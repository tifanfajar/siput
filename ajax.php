<?php 
//General Controller
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();

//Model Ajax
include "model/ajax.php";
$gen_ajax      = new Ajax();

//Model Notif
include "model/notif.php";
$md_notif      = new notif();
session_start();



$act="";
if(isset($_GET['do_act'])){
    $act = $_GET['do_act'];
}

$id_parameter="";
if(isset($_GET['id_parameter'])){
        $id_parameter =$_GET['id_parameter'];
}

if($act=="" or $act==null) {
  echo "404 Not Found";
}
else if($act=="form_pwd"){
   include "view/default_style.php"; 
   include "view/ajax_form/change_pwd.php";
}
else if($act=="reqPassword"){
  if(!empty($_SESSION['kode_user'])){
        $old_pwd   = hash('crc32b',$_POST['old_pwd']);;
        $new_pwd   = hash('crc32b',$_POST['new_pwd']);;
        $k_new_pwd = hash('crc32b',$_POST['k_new_pwd']);;

        $update_data = array();
        $update_data['password']       = $new_pwd;

          //Paramater
        $where_data = array();
        $where_data['kode_user']       = $_SESSION['kode_user'];

        //Check Old Password
        $data_old_pwd = $gen_model->getOne("password","ms_user",array('kode_user'=>$_SESSION['kode_user']));

        if($data_old_pwd!=$old_pwd){
          echo "Password lama anda salah";
        }      
        else if($new_pwd!=$k_new_pwd){
          echo "Password baru anda tidak sama";
        }
        else {
           echo $gen_model->Update('ms_user',$update_data,$where_data);
        }
 } else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="reqCompanyLoket"){
  $data = array();
  $data_usr = $gen_ajax->getDataCompanyLoket($_POST['query']);
    while($list = $data_usr->FetchRow()){
    foreach($list as $key=>$val){
      $key=strtolower($key);
      $$key=$val;
      } 
      array_push($data,array('label'=>$company_name,'value'=>$company_id,'company_id'=>$company_id,'company_name'=>$company_name,'company_address'=>$alamat)); 
    }
    echo json_encode($data); 
}
else if($act=="reqCompany"){
	$data = array();
	$data_usr = $gen_ajax->getDataCompany($_POST['query']);
		while($list = $data_usr->FetchRow()){
		foreach($list as $key=>$val){
			$key=strtolower($key);
			$$key=$val;
		  } 
		  array_push($data,array('label'=>$company_name,'value'=>$company_id,'company_id'=>$company_id,'company_name'=>$company_name,'company_address'=>$alamat)); 
		}
    echo json_encode($data); 
}
else if($act=="reqDataSims"){
	$data = array();
	$data_usr = $gen_ajax->getDataSims($_POST['query']);
		while($list = $data_usr->FetchRow()){
		foreach($list as $key=>$val){
			$key=strtolower($key);
			$$key=$val;
		  } 
		  $my_tgh = int_to_rp($tagihan);	
		  array_push($data,array('label'=>$no_spp,'value'=>$no_spp,'no_spp'=>$no_spp,'company_name'=>$company_name,'service'=>$service,'no_aplikasi'=>$no_aplikasi,'no_client'=>$no_klien_licence,'tagihan'=>$my_tgh)); 
		}
    echo json_encode($data); 
}
else if($act=="reqDataRencanaSosialisasi"){
	$data = array();
    //Kepala UPT atau Operator UPT
   if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){  
	      $data_usr = $gen_ajax->getDataRencanaSosialisasi_upt($_POST['query'],$_SESSION['kode_upt']);
    }
    else {
      $data_usr = $gen_ajax->getDataRencanaSosialisasi($_POST['query']);
    }
		while($list = $data_usr->FetchRow()){
		foreach($list as $key=>$val){
			$key=strtolower($key);
			$$key=$val;
		  } 
		  $label  =  $gen_controller->get_date_indonesia($tgl_pelaksanaan)." - ".$tema." - ".$kegiatan;
		  array_push($data,array('label'=>$label,'value'=>$label,'tgl_pelaksanaan'=>$gen_controller->get_date_indonesia($tgl_pelaksanaan),'tema'=>$tema,'jenis_kegiatan'=>$jenis_kegiatan,'tempat'=>$tempat,'kode_upt'=>$kode_upt,'id_prov'=>$id_prov,'jumlah_peserta'=>int_to_rp($jumlah_peserta),'anggaran'=>int_to_rp($anggaran),'target_peserta'=>$target_peserta,'narasumber'=>$narasumber,'keterangan'=>$keterangan)); 
		}
    echo json_encode($data); 
}else if($act=="backup_database"){
	$gen_controller->EXPORT_DATABASE('localhost','root','','pelaporan_pelayanan');
}
else if($act=="notifikasi_read"){

    $update_data = array();
      $update_data['status']          = "1";

       //Paramater
      $where_data = array();
      $where_data['kode_notif']       = base64_decode($_POST['kode_notif']);
      $where_data['jenis']            = $_POST['jenis'];

      $gen_model->Update('tr_notif',$update_data,$where_data);
        //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){ 
             $not_read_by_jenis = $db->getOne("select count(*) as total_pesan from tr_notif 
                              where to_group_user='".$_SESSION['group_user']."' 
                                  and id_prov='".$_SESSION['upt_provinsi']."' 
                                  and kode_upt='".$_SESSION['kode_upt']."'  and status='0' and jenis='".$_POST['jenis']."'
                              ");   
        }
        else {
           $not_read_by_jenis = $db->getOne("select count(*) as total_pesan from tr_notif 
                              where to_group_user='".$_SESSION['group_user']."'   and status='0' and jenis='".$_POST['jenis']."'
                              ");   
        }
       
        if(!empty($not_read_by_jenis)){
          echo $not_read_by_jenis;
        }
}
else if($act=="notifikasi_read_all"){
         //Kepala UPT atau Operator UPT
      if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){ 
          $not_read_by_jenis = $db->getOne("select count(*) as total_pesan from tr_notif 
                            where to_group_user='".$_SESSION['group_user']."' 
                                and id_prov='".$_SESSION['upt_provinsi']."' 
                                and kode_upt='".$_SESSION['kode_upt']."'  and status='0'
                            "); 
      }
      else {
             

          $not_read_by_jenis = $db->getOne("select count(*) as total_pesan from tr_notif 
                            where to_group_user='".$_SESSION['group_user']."'   and status='0' 
                            ");   
      }
      if(!empty($not_read_by_jenis)){
      	echo $not_read_by_jenis;
      }
}
else if($act=="notifikasi"){
    if(!empty($_SESSION['kode_user'])){ ?>
    		  <table id="notif_table" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
                    <th>Notification</th>
                    <th>Created Date</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody></tbody>
                </table>
                <script type="text/javascript">
                	var dTable;
		                $(document).ready(function() {
		                  dTable = $('#notif_table').DataTable( {
		                    "bProcessing": true,
		                    "bServerSide": true,
		                    "bJQueryUI": false,
		                    "responsive": false,
		                    "autoWidth": false,
		                    "sAjaxSource": "<?php echo $basepath ?>ajax/rest_notif&jenis=<?php echo $_REQUEST['jenis'] ?>&id_prov=<?php echo $_SESSION['upt_provinsi'] ?>&upt=<?php echo $_SESSION['kode_upt'] ?>", 
		                    "sServerMethod": "POST",
		                    "scrollX": true,
		                    "scrollY": "350px",
		                     "scrollCollapse": true,
		                    "columnDefs": [
		                    { "orderable": true, "targets": 0, "searchable": true},
		                    { "orderable": true, "targets": 1, "searchable": true},
		                    { "orderable": false, "targets": 2, "searchable": false}
		                    ]
		                  } );
		                });
                </script>
    <?php 
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_notif"){

  $aColumns = array('ntf.judul','ntf.created_date','ntf.kode_notif'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);

  $prov   = $_REQUEST['id_prov'];
  $upt    = $_REQUEST['upt'];
  $jenis  = $_REQUEST['jenis'];

  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_notif->getNotif($sWhere,$sOrder,$sLimit,$prov,$upt,$jenis,$_SESSION['group_user']);
  $rResultFilterTotal   = $md_notif->getCountNotif($sWhere,$prov,$upt,$jenis,$_SESSION['group_user']);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    //$wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    
    $detail = '<button title="Detail" onclick="table_notif(\''.$jenis.'\',\''.base64_encode($aRow['kode_notif']).'\',\''.base64_encode($aRow['data_notif']).'\')"  type="button"  class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
    $style1=""; 
    $style2="";
    if($aRow['status']=="0"){
    	 $style1="<b>"; 
   		 $style2="</b>";
    } 
    $edit_delete = $detail; 
    $row = array();
    $row = array($style1.$aRow['judul'].$style1,$style1.$gen_controller->get_date_indonesia($aRow['created_date']).$style2,"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_notif_pelayanan_table"){ //Pelayanan
	 if(!empty($_SESSION['kode_user'])){ ?>
    		  <table id="tb_notif_table_data" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
					<th class="text-center">Tanggal</th>
					<th class="text-center">Status</th>
					<th class="text-center">Nama</th>
					<th class="text-center">Jabatan</th>
					<th class="text-center">Perusahaan</th>
					<th class="text-center">No.Tlp</th>
					<th class="text-center">No.Hp</th>
					<th class="text-center">Email</th>
					<th class="text-center">Tujuan</th>
					<th class="text-center">Provinsi</th>
					<th class="text-center">UPT</th>
					<th class="text-center">Keterangan</th>
					<th class="text-center">Aksi</th>
				</tr>
                </thead>
                <tbody></tbody>
                </table>
                <script type="text/javascript">
                	 var dTable_notif;
				            dTable_notif = $('#tb_notif_table_data').DataTable( {
				              "bProcessing": true,
				              "bServerSide": true,
				              "bJQueryUI": false,
				              "responsive": false,
				              "autoWidth": false,
				              "retrieve": true,
				              "bDestroy":true,
				              "sAjaxSource": "<?php echo $basepath ?>ajax/rest_notif_pelayanan_data&data_notif=<?php echo $_REQUEST['data_notif'] ?>", 
				              "sServerMethod": "POST",
				              "scrollX": true,
				              "scrollY": "350px",
				                "scrollCollapse": true,
				                "order": [[ 0, "desc" ]],
				              "columnDefs": [
				              { "orderable": false, "targets": 0, "searchable": true},
				              { "orderable": true, "targets": 1, "searchable": true},
				              { "orderable": true, "targets": 2, "searchable": true},
				              { "orderable": true, "targets": 3, "searchable": true},
				              { "orderable": true, "targets": 4, "searchable": true },
				              { "orderable": true, "targets": 5, "searchable": true },
				              { "orderable": true, "targets": 6, "searchable": true },
				              { "orderable": true, "targets": 7, "searchable": true },
				              { "orderable": true, "targets": 8, "searchable": true },
				              { "orderable": true, "targets": 9, "searchable": true },
				              { "orderable": true, "targets": 10, "searchable": true },
				              { "orderable": true, "targets": 11, "searchable": true },
				              { "orderable": false, "targets": 12, "searchable": false, "width":170}
				              ]
				            } );
                </script>
    <?php 
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_notif_pelayanan_data"){ //Pelayanan Data
  $aColumns = array('pl.tgl_pelayanan','pl.status','pl.nama_pengunjung','cp.company_name','pl.jabatan_pengunjung','pl.no_tlp','pl.no_hp','pl.email','pl.tujuan','pv.nama_prov','upt.nama_upt','pl.keterangan','pl.kode_pelayanan'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);
  $data_notif = "";

    $variableAry=explode(",",base64_decode($_REQUEST['data_notif']));
        foreach($variableAry as $var) {
            $data_notif .= "'".$var."',";
        }

    $data_notif = rtrim($data_notif,',');
    $data_notif = base64_encode($data_notif);

  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_notif->getLoketPelayananNotif($sWhere,$sOrder,$sLimit,$data_notif);
  $rResultFilterTotal   = $md_notif->getCountLoketPelayananNotif($sWhere,$data_notif);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_pelayanan'];
    
    $detail = '<button title="Detail"  type="button"  onclick="do_detail_pl_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
 
    $sts = "";
    if($aRow['status']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }

    $edit_delete = $detail;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['tgl_pelayanan']),$sts,$aRow['nama_pengunjung'],$aRow['jabatan_pengunjung'],$aRow['company_name'],$aRow['no_tlp'],$aRow['no_hp'],$aRow['email'],$aRow['tujuan'],$aRow['nama_prov'],$aRow['nama_upt'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_notif_spp_table"){ //SPP
	 if(!empty($_SESSION['kode_user'])){ ?>
    		  <table id="tb_notif_table_data" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
					<th class="text-center">Tanggal Buat</th>
					<th class="text-center">Status</th>
					<th class="text-center">No. Client</th>
					<th class="text-center">Perusahaan</th>
					<th class="text-center">Service</th>
					<th class="text-center">No. Spp</th>
					<th class="text-center">No. Aplikasi</th>
					<th class="text-center">Jenis</th>
					<th class="text-center">Nilai BHP (Rp)</th>
					<th class="text-center">Tanggal Terima Waba</th>
					<th class="text-center">Provinsi</th>
					<th class="text-center">UPT</th>
					<th class="text-center">Metode</th>
					<th class="text-center">Aksi</th>
				</tr>
                </thead>
                <tbody></tbody>
                </table>
                <script type="text/javascript">
                	 var dTable_notif;
				            dTable_notif = $('#tb_notif_table_data').DataTable( {
				              "bProcessing": true,
				              "bServerSide": true,
				              "bJQueryUI": false,
				              "responsive": false,
				              "autoWidth": false,
				              "retrieve": true,
				              "bDestroy":true,
				              "sAjaxSource": "<?php echo $basepath ?>ajax/rest_notif_spp_data&data_notif=<?php echo $_REQUEST['data_notif'] ?>", 
				              "sServerMethod": "POST",
				              "scrollX": true,
				              "scrollY": "350px",
				                "scrollCollapse": true,
				                "order": [[ 0, "desc" ]],
				              "columnDefs": [
				              { "orderable": false, "targets": 0, "searchable": true},
				              { "orderable": true, "targets": 1, "searchable": true},
				              { "orderable": true, "targets": 2, "searchable": true},
				              { "orderable": true, "targets": 3, "searchable": true},
				              { "orderable": true, "targets": 4, "searchable": true },
				              { "orderable": true, "targets": 5, "searchable": true },
				              { "orderable": true, "targets": 6, "searchable": true },
				              { "orderable": true, "targets": 7, "searchable": true },
				              { "orderable": true, "targets": 8, "searchable": true },
				              { "orderable": true, "targets": 9, "searchable": true },
				              { "orderable": true, "targets": 10, "searchable": true },
				              { "orderable": true, "targets": 11, "searchable": true },
				              { "orderable": true, "targets": 12, "searchable": true },
				              { "orderable": false, "targets": 13, "searchable": false, "width":170}
				              ]
				            } );
                </script>
    <?php 
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_notif_spp_data"){ //SPP Data
$aColumns = array('spp.created_date','spp.no_klien_licence','cp.company_name','spp.service','spp.no_spp','spp.no_aplikasi','spp.kategori_spp','spp.tagihan','spp.tgl_terima_waba','pv.nama_prov','upt.nama_upt','mtd.metode','spp.kode_spp'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);
  $data_notif = "";

    $variableAry=explode(",",base64_decode($_REQUEST['data_notif']));
        foreach($variableAry as $var) {
            $data_notif .= "'".$var."',";
        }

    $data_notif = rtrim($data_notif,',');
    $data_notif = base64_encode($data_notif);

  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_notif->getSPPNotif($sWhere,$sOrder,$sLimit,$data_notif);
  $rResultFilterTotal   = $md_notif->getCountSPPNotif($sWhere,$data_notif);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_spp'];
    
    $detail = '<button title="Detail"  type="button"  onclick="do_detail_spp_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
 
    $sts = "";
    if($aRow['status']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }

    $edit_delete = $detail;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['tgl_buat']),$sts,$aRow['no_klien_licence'],$aRow['company_name'],$aRow['service'],$aRow['no_spp'],$aRow['no_aplikasi'],$aRow['kategori_spp'],int_to_rp($aRow['tagihan']),$gen_controller->get_date_indonesia($aRow['tgl_terima_waba']),$aRow['nama_prov'],$aRow['nama_upt'],$aRow['nama_metode'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_notif_isr_table"){ //SPP
	 if(!empty($_SESSION['kode_user'])){ ?>
    		  <table id="tb_notif_table_data" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
					<th class="text-center">Tanggal Buat</th>
					<th class="text-center">Status</th>
                    <th class="text-center">No. Spp</th>
                    <th class="text-center">No. Client</th>
                    <th class="text-center">Perusahaan</th>
                    <th class="text-center">Service</th>
                    <th class="text-center">No. Aplikasi</th>
                    <th class="text-center">Tanggal Terima Waba</th>
                    <th class="text-center">Provinsi</th>
                    <th class="text-center">UPT</th>
                    <th class="text-center">Metode</th>
                    <th class="text-center">Aksi</th>
				</tr>
                </thead>
                <tbody></tbody>
                </table>
                <script type="text/javascript">
                	 var dTable_notif;
				            dTable_notif = $('#tb_notif_table_data').DataTable( {
				              "bProcessing": true,
				              "bServerSide": true,
				              "bJQueryUI": false,
				              "responsive": false,
				              "autoWidth": false,
				              "retrieve": true,
				              "bDestroy":true,
				              "sAjaxSource": "<?php echo $basepath ?>ajax/rest_notif_isr_data&data_notif=<?php echo $_REQUEST['data_notif'] ?>", 
				              "sServerMethod": "POST",
				              "scrollX": true,
				              "scrollY": "350px",
				                "scrollCollapse": true,
				                "order": [[ 0, "desc" ]],
				              "columnDefs": [
				              	{ "orderable": false, "targets": 0, "searchable": false},
								{ "orderable": true, "targets": 1, "searchable": true},
								{ "orderable": true, "targets": 2, "searchable": true},
								{ "orderable": true, "targets": 3, "searchable": true},
								{ "orderable": true, "targets": 4, "searchable": true },
								{ "orderable": true, "targets": 5, "searchable": true },
								{ "orderable": true, "targets": 6, "searchable": true },
								{ "orderable": true, "targets": 7, "searchable": true },
								{ "orderable": true, "targets": 8, "searchable": true },
								{ "orderable": true, "targets": 9, "searchable": true },
								{ "orderable": true, "targets": 10, "searchable": true },
								{ "orderable": false, "targets": 11, "searchable": false, "width":170}
				              ]
				            } );
                </script>
    <?php 
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_notif_isr_data"){ //ISR Data
$aColumns = array('isr.created_date','isr.status','isr.no_spp','isr.no_client','cp.company_name','isr.service','isr.no_aplikasi','isr.tgl_terima_waba','pv.nama_prov','upt.nama_upt','mtd.metode','isr.kode_isr'); //Kolom Pada Tabel
    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);
  $data_notif = "";

    $variableAry=explode(",",base64_decode($_REQUEST['data_notif']));
        foreach($variableAry as $var) {
            $data_notif .= "'".$var."',";
        }

    $data_notif = rtrim($data_notif,',');
    $data_notif = base64_encode($data_notif);

  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_notif->getISRNotif($sWhere,$sOrder,$sLimit,$data_notif);
  $rResultFilterTotal   = $md_notif->getCountISRNotif($sWhere,$data_notif);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_isr'];
    
    $detail = '<button title="Detail"  type="button"  onclick="do_detail_isr_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
 
    $sts = "";
    if($aRow['status']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }

    $edit_delete = $detail;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$sts,$aRow['no_spp'],$aRow['no_client'],$aRow['company_name'],$aRow['service'],$aRow['no_aplikasi'],$gen_controller->get_date_indonesia($aRow['tgl_terima_waba']),$aRow['nama_prov'],$aRow['nama_upt'],$aRow['nama_metode'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_notif_revoke_table"){ //Revoke
	 if(!empty($_SESSION['kode_user'])){ ?>
    		  <table id="tb_notif_table_data" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
					<th class="text-center">Tanggal Buat</th>
					<th class="text-center">Status</th>
                    <th class="text-center">No. Spp</th>
                    <th class="text-center">No. Client</th>
                    <th class="text-center">Perusahaan</th>
                    <th class="text-center">Service</th>
                    <th class="text-center">No. Aplikasi</th>
                    <th class="text-center">Tanggal ISR di cabut</th>
                    <th class="text-center">Tanggal Terima Waba</th>
                    <th class="text-center">Provinsi</th>
                    <th class="text-center">UPT</th>
                    <th class="text-center">Metode</th>
                    <th class="text-center">Keterangan</th>
                    <th class="text-center">Aksi</th>
				</tr>
                </thead>
                <tbody></tbody>
                </table>
                <script type="text/javascript">
                	 var dTable_notif;
				            dTable_notif = $('#tb_notif_table_data').DataTable( {
				              "bProcessing": true,
				              "bServerSide": true,
				              "bJQueryUI": false,
				              "responsive": false,
				              "autoWidth": false,
				              "retrieve": true,
				              "bDestroy":true,
				              "sAjaxSource": "<?php echo $basepath ?>ajax/rest_notif_revoke_data&data_notif=<?php echo $_REQUEST['data_notif'] ?>", 
				              "sServerMethod": "POST",
				              "scrollX": true,
				              "scrollY": "350px",
				                "scrollCollapse": true,
				                "order": [[ 0, "desc" ]],
				              "columnDefs": [
				              	{ "orderable": false, "targets": 0, "searchable": false},
								{ "orderable": true, "targets": 1, "searchable": true},
								{ "orderable": true, "targets": 2, "searchable": true},
								{ "orderable": true, "targets": 3, "searchable": true},
								{ "orderable": true, "targets": 4, "searchable": true },
								{ "orderable": true, "targets": 5, "searchable": true },
								{ "orderable": true, "targets": 6, "searchable": true },
								{ "orderable": true, "targets": 7, "searchable": true },
								{ "orderable": true, "targets": 8, "searchable": true },
								{ "orderable": true, "targets": 9, "searchable": true },
								{ "orderable": true, "targets": 10, "searchable": true },
								{ "orderable": false, "targets": 11, "searchable": false, "width":170}
				              ]
				            } );
                </script>
    <?php 
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_notif_revoke_data"){ //Revoke Data
$aColumns = array('rvk.created_date','rvk.status','rvk.no_spp','rvk.no_client','cp.company_name','rvk.service','rvk.no_aplikasi','rvk.tgl_isr_dicabut','rvk.tgl_terima_waba','pv.nama_prov','upt.nama_upt','mtd.metode','rvk.keterangan','rvk.kode_revoke'); //Kolom Pada Tabel
    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);
  $data_notif = "";

    $variableAry=explode(",",base64_decode($_REQUEST['data_notif']));
        foreach($variableAry as $var) {
            $data_notif .= "'".$var."',";
        }

    $data_notif = rtrim($data_notif,',');
    $data_notif = base64_encode($data_notif);

  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_notif->getRevokeNotif($sWhere,$sOrder,$sLimit,$data_notif);
  $rResultFilterTotal   = $md_notif->getCountRevokeNotif($sWhere,$data_notif);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_revoke'];
    
    $detail = '<button title="Detail"  type="button"  onclick="do_detail_revoke_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
 
    $sts = "";
    if($aRow['status']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }

    $edit_delete = $detail;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$sts,$aRow['no_spp'],$aRow['no_client'],$aRow['company_name'],$aRow['service'],$aRow['no_aplikasi'],$gen_controller->get_date_indonesia($aRow['tgl_isr_dicabut']),$gen_controller->get_date_indonesia($aRow['tgl_terima_waba']),$aRow['nama_prov'],$aRow['nama_upt'],$aRow['nama_metode'],$aRow['keterangan'],"<center>".$edit_delete."</center>");

    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_notif_piutang_table"){ //Piutang
	 if(!empty($_SESSION['kode_user'])){ ?>
    		  <table id="tb_notif_table_data" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
					<th class="text-center" rowspan="2">Tanggal Buat</th>
                    <th class="text-center" rowspan="2">Status</th>
                    <th class="text-center" rowspan="2">Wajib Bayar</th>
                    <th class="text-center" rowspan="2">No. Client</th>
                    <th class="text-center" rowspan="2">Nilai Penyerahan</th>
                    <th class="text-center" rowspan="2">Tahun Pelimpahan</th>
                    <th class="text-center" rowspan="2">Nama KPKNL</th>
                    <th class="text-center" rowspan="2">Tahapan Pengurusan</th>
                    <th class="text-center" colspan="3">Pemindah Bukuan</th>
                    <th class="text-center" rowspan="2">PSBDT</th>
                    <th class="text-center" rowspan="2">Pembatalan</th>
                    <th class="text-center" rowspan="2">Sisa Piutang</th>
                    <th class="text-center" rowspan="2">Provinsi</th>
                    <th class="text-center" rowspan="2">UPT</th>
                    <th class="text-center" rowspan="2">Ket</th>
                    <th class="text-center" rowspan="2">Aksi</th>
				</tr>
				<tr>
                    <th class="text-center">Lunas</th>
                    <th class="text-center">Angsuran</th>
                    <th class="text-center">Tanggal</th>
                  </tr>
                  <tr>
                    <th class="text-center">1</th>
                    <th class="text-center">2</th>
                    <th class="text-center">3</th>
                    <th class="text-center">4</th>
                    <th class="text-center">5</th>
                    <th class="text-center">6</th>
                    <th class="text-center">7</th>
                    <th class="text-center">8</th>
                    <th class="text-center">9</th>
                    <th class="text-center">10</th>
                    <th class="text-center">11</th>
                    <th class="text-center">12</th>
                    <th class="text-center">13</th>
                    <th class="text-center">14</th>
                    <th class="text-center">15</th>
                    <th class="text-center">16</th>
                    <th class="text-center">17</th>
                    <th class="text-center">18</th>
                  </tr>
                </thead>
                <tbody></tbody>
                </table>
                <script type="text/javascript">
                	 var dTable_notif;
				            dTable_notif = $('#tb_notif_table_data').DataTable( {
				              "bProcessing": true,
				              "bServerSide": true,
				              "bJQueryUI": false,
				              "responsive": false,
				              "autoWidth": false,
				              "retrieve": true,
				              "bDestroy":true,
				              "sAjaxSource": "<?php echo $basepath ?>ajax/rest_notif_piutang_data&data_notif=<?php echo $_REQUEST['data_notif'] ?>", 
				              "sServerMethod": "POST",
				              "scrollX": true,
				              "scrollY": "350px",
				                "scrollCollapse": true,
				                "order": [[ 0, "desc" ]],
				              "columnDefs": [
				              	{ "orderable": false, "targets": 0, "searchable": false},
								{ "orderable": true, "targets": 1, "searchable": true},
								{ "orderable": true, "targets": 2, "searchable": true},
								{ "orderable": true, "targets": 3, "searchable": true},
								{ "orderable": true, "targets": 4, "searchable": true },
								{ "orderable": true, "targets": 5, "searchable": true },
								{ "orderable": true, "targets": 6, "searchable": true },
								{ "orderable": true, "targets": 7, "searchable": true },
								{ "orderable": true, "targets": 8, "searchable": true },
								{ "orderable": true, "targets": 9, "searchable": true },
								{ "orderable": true, "targets": 10, "searchable": true },
								{ "orderable": true, "targets": 11, "searchable": true },
								{ "orderable": true, "targets": 12, "searchable": true },
								{ "orderable": false, "targets": 13, "searchable": false, "width":170}
				              ]
				            } );
                </script>
    <?php 
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_notif_piutang_data"){ //Piutang Data
$aColumns = array('pt.created_date','cp.company_name','pt.no_client','pt.nilai_penyerahan','pt.thn_pelimpahan','pt.nama_kpknl','pt.tahapan_pengurusan','pt.lunas','pt.angsuran','pt.tanggal','pt.psbdt','pt.pembatalan','pt.sisa_piutang','pv.nama_prov','upt.nama_upt','pt.keterangan','pt.kode_piutang'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);
  $data_notif = "";

    $variableAry=explode(",",base64_decode($_REQUEST['data_notif']));
        foreach($variableAry as $var) {
            $data_notif .= "'".$var."',";
        }

    $data_notif = rtrim($data_notif,',');
    $data_notif = base64_encode($data_notif);

  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_notif->getPiutangNotif($sWhere,$sOrder,$sLimit,$data_notif);
  $rResultFilterTotal   = $md_notif->getCountPiutangNotif($sWhere,$data_notif);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_piutang'];
    
    $detail = '<button title="Detail"  type="button"  onclick="do_detail_piutang_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
 
    $sts = "";
    if($aRow['status']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }

    $edit_delete = $detail;
    $row = array();
     $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$sts,$aRow['company_name'],$aRow['no_client'],int_to_rp($aRow['nilai_penyerahan']),$aRow['thn_pelimpahan'],$aRow['nama_kpknl'],$aRow['tahapan_pengurusan'],int_to_rp($aRow['lunas']),int_to_rp($aRow['angsuran']),$gen_controller->get_date_indonesia($aRow['tanggal']),int_to_rp($aRow['psbdt']),int_to_rp($aRow['pembatalan']),int_to_rp($aRow['sisa_piutang']),$aRow['nama_prov'],$aRow['nama_upt'],$aRow['keterangan'],"<center>".$edit_delete."</center>");


    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_notif_unar_table"){ //UNAR
   if(!empty($_SESSION['kode_user'])){ ?>
          <table id="tb_notif_table_data" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
          <th class="text-center" rowspan="2">Tanggal Buat</th>
                    <th class="text-center" rowspan="2">Status</th>
                    <th class="text-center" rowspan="2">Wajib Bayar</th>
                    <th class="text-center" rowspan="2">No. Client</th>
                    <th class="text-center" rowspan="2">Nilai Penyerahan</th>
                    <th class="text-center" rowspan="2">Tahun Pelimpahan</th>
                    <th class="text-center" rowspan="2">Nama KPKNL</th>
                    <th class="text-center" rowspan="2">Tahapan Pengurusan</th>
                    <th class="text-center" colspan="3">Pemindah Bukuan</th>
                    <th class="text-center" rowspan="2">PSBDT</th>
                    <th class="text-center" rowspan="2">Pembatalan</th>
                    <th class="text-center" rowspan="2">Sisa Piutang</th>
                    <th class="text-center" rowspan="2">Provinsi</th>
                    <th class="text-center" rowspan="2">UPT</th>
                    <th class="text-center" rowspan="2">Ket</th>
                    <th class="text-center" rowspan="2">Aksi</th>
        </tr>
        <tr>
                    <th class="text-center">Lunas</th>
                    <th class="text-center">Angsuran</th>
                    <th class="text-center">Tanggal</th>
                  </tr>
                  <tr>
                    <th class="text-center">1</th>
                    <th class="text-center">2</th>
                    <th class="text-center">3</th>
                    <th class="text-center">4</th>
                    <th class="text-center">5</th>
                    <th class="text-center">6</th>
                    <th class="text-center">7</th>
                    <th class="text-center">8</th>
                    <th class="text-center">9</th>
                    <th class="text-center">10</th>
                    <th class="text-center">11</th>
                    <th class="text-center">12</th>
                    <th class="text-center">13</th>
                    <th class="text-center">14</th>
                    <th class="text-center">15</th>
                    <th class="text-center">16</th>
                    <th class="text-center">17</th>
                    <th class="text-center">18</th>
                  </tr>
                </thead>
                <tbody></tbody>
                </table>
                <script type="text/javascript">
                   var dTable_notif;
                    dTable_notif = $('#tb_notif_table_data').DataTable( {
                      "bProcessing": true,
                      "bServerSide": true,
                      "bJQueryUI": false,
                      "responsive": false,
                      "autoWidth": false,
                      "retrieve": true,
                      "bDestroy":true,
                      "sAjaxSource": "<?php echo $basepath ?>ajax/rest_notif_unar_data&data_notif=<?php echo $_REQUEST['data_notif'] ?>", 
                      "sServerMethod": "POST",
                      "scrollX": true,
                      "scrollY": "350px",
                        "scrollCollapse": true,
                        "order": [[ 0, "desc" ]],
                      "columnDefs": [
                        { "orderable": false, "targets": 0, "searchable": false},
                { "orderable": true, "targets": 1, "searchable": true},
                { "orderable": true, "targets": 2, "searchable": true},
                { "orderable": true, "targets": 3, "searchable": true},
                { "orderable": true, "targets": 4, "searchable": true },
                { "orderable": true, "targets": 5, "searchable": true },
                { "orderable": true, "targets": 6, "searchable": true },
                { "orderable": true, "targets": 7, "searchable": true },
                { "orderable": true, "targets": 8, "searchable": true },
                { "orderable": true, "targets": 9, "searchable": true },
                { "orderable": true, "targets": 10, "searchable": true },
                { "orderable": true, "targets": 11, "searchable": true },
                { "orderable": true, "targets": 12, "searchable": true },
                { "orderable": false, "targets": 13, "searchable": false, "width":170}
                      ]
                    } );
                </script>
    <?php 
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_notif_unar_data"){ //Unar Data
 $aColumns = array('un.created_date','un.status','un.tgl_ujian','un.lokasi_ujian','kb.nama_kab_kot','un.jumlah_yd','un.jumlah_yc','un.jumlah_yb','un.lulus_yd','un.lulus_yc','un.lulus_yb','un.tdk_lulus_yd','un.tdk_lulus_yb','un.tdk_lulus_yc','pv.nama_prov','upt.nama_upt','un.keterangan','un.kode_unar'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);
  $data_notif = "";

    $variableAry=explode(",",base64_decode($_REQUEST['data_notif']));
        foreach($variableAry as $var) {
            $data_notif .= "'".$var."',";
        }

    $data_notif = rtrim($data_notif,',');
    $data_notif = base64_encode($data_notif);

  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_notif->getUnarNotif($sWhere,$sOrder,$sLimit,$data_notif);
  $rResultFilterTotal   = $md_notif->getCountUnarNotif($sWhere,$data_notif);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_unar'];
    
    $detail = '<button title="Detail"  type="button"  onclick="do_detail_unar_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
 
    $sts = "";
    if($aRow['status']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }

    $edit_delete = $detail;
    $row = array();
 $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$sts,$gen_controller->get_date_indonesia($aRow['tgl_ujian']),$aRow['lokasi_ujian'],$aRow['nama_kab_kot'],$aRow['jumlah_yd'],$aRow['jumlah_yc'],$aRow['jumlah_yb'],$aRow['lulus_yd'],$aRow['lulus_yc'],$aRow['lulus_yb'],$aRow['tdk_lulus_yd'],$aRow['tdk_lulus_yc'],$aRow['tdk_lulus_yb'],$aRow['nama_prov'],$aRow['nama_upt'],$aRow['keterangan'],"<center>".$edit_delete."</center>");

    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_notif_bahan_sosialisasi_table"){ //Bahan Sosialisasi
   if(!empty($_SESSION['kode_user'])){ ?>
          <table id="tb_notif_table_data" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Materi</th>
                    <th class="text-center">Judul</th>
                    <th class="text-center">Author</th>
                    <th class="text-center">Lampiran</th>
                    <th class="text-center">Provinsi</th>
                    <th class="text-center">UPT</th>
                    <th class="text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody></tbody>
                </table>
                <script type="text/javascript">
                   var dTable_notif;
                    dTable_notif = $('#tb_notif_table_data').DataTable( {
                      "bProcessing": true,
                      "bServerSide": true,
                      "bJQueryUI": false,
                      "responsive": false,
                      "autoWidth": false,
                      "retrieve": true,
                      "bDestroy":true,
                      "sAjaxSource": "<?php echo $basepath ?>ajax/rest_notif_bahan_sosialisasi_data&data_notif=<?php echo $_REQUEST['data_notif'] ?>", 
                      "sServerMethod": "POST",
                      "scrollX": true,
                      "scrollY": "350px",
                        "scrollCollapse": true,
                        "order": [[ 0, "desc" ]],
                      "columnDefs": [
                        { "orderable": false, "targets": 0, "searchable": false},
                        { "orderable": true, "targets": 1, "searchable": true},
                        { "orderable": true, "targets": 2, "searchable": true},
                        { "orderable": true, "targets": 3, "searchable": true},
                        { "orderable": true, "targets": 4, "searchable": true },
                        { "orderable": true, "targets": 5, "searchable": true },
                        { "orderable": true, "targets": 6, "searchable": true },
                        { "orderable": true, "targets": 7, "searchable": true },
                        { "orderable": false, "targets": 8, "searchable": false, "width":170}
                      ]
                    } );
                </script>
    <?php 
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_notif_bahan_sosialisasi_data"){ //Bahan Sosialisasi Data
 $aColumns = array('bhn.created_date','bhn.status','mtr.jenis_materi','bhn.judul','bhn.author','bhn.lampiran','pv.nama_prov','upt.nama_upt','bhn.kode_bhn_sosialisasi'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);
  $data_notif = "";

    $variableAry=explode(",",base64_decode($_REQUEST['data_notif']));
        foreach($variableAry as $var) {
            $data_notif .= "'".$var."',";
        }

    $data_notif = rtrim($data_notif,',');
    $data_notif = base64_encode($data_notif);

  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_notif->getBahanSosialisasiNotif($sWhere,$sOrder,$sLimit,$data_notif);
  $rResultFilterTotal   = $md_notif->getCountBahanSosialisasiNotif($sWhere,$data_notif);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_bhn_sosialisasi'];
    
    $detail = '<button title="Detail"  type="button"  onclick="do_detail_bhn_so_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
 
    $sts = "";
    if($aRow['status']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }
    $lampiran ="";
    if(!empty($aRow['lampiran'])){
       $lampiran = '<center><a   title="Download File" style="cursor:pointer;color:blue" href="'.$basepath.'assets/attachment/sosialisasi_bimtek/bahan_sosialisasi/'.$aRow['lampiran'].'" download><i class="fa fa-download fa-2x"></i></a></center>';
    }   
    $edit_delete = $detail;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$sts,$aRow['jenis_materi'],$aRow['judul'],$aRow['author'],$lampiran,$aRow['nama_prov'],$aRow['nama_upt'],"<center>".$edit_delete."</center>");

    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_notif_rencana_sosialisasi_table"){ //Rencana Sosialisasi
   if(!empty($_SESSION['kode_user'])){ ?>
          <table id="tb_notif_table_data" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Jenis Kegiatan</th>
                    <th class="text-center">Tanggal Pelaksanaan</th>
                    <th class="text-center">Tempat</th>
                    <th class="text-center">Tema</th>
                    <th class="text-center">Lampiran</th>
                    <th class="text-center">Provinsi</th>
                    <th class="text-center">UPT</th>
                    <th class="text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody></tbody>
                </table>
                <script type="text/javascript">
                   var dTable_notif;
                    dTable_notif = $('#tb_notif_table_data').DataTable( {
                      "bProcessing": true,
                      "bServerSide": true,
                      "bJQueryUI": false,
                      "responsive": false,
                      "autoWidth": false,
                      "retrieve": true,
                      "bDestroy":true,
                      "sAjaxSource": "<?php echo $basepath ?>ajax/rest_notif_rencana_sosialisasi_data&data_notif=<?php echo $_REQUEST['data_notif'] ?>", 
                      "sServerMethod": "POST",
                      "scrollX": true,
                      "scrollY": "350px",
                        "scrollCollapse": true,
                        "order": [[ 0, "desc" ]],
                      "columnDefs": [
                        { "orderable": false, "targets": 0, "searchable": false},
                        { "orderable": true, "targets": 1, "searchable": true},
                        { "orderable": true, "targets": 2, "searchable": true},
                        { "orderable": true, "targets": 3, "searchable": true},
                        { "orderable": true, "targets": 4, "searchable": true },
                        { "orderable": true, "targets": 5, "searchable": true },
                        { "orderable": true, "targets": 6, "searchable": true },
                        { "orderable": true, "targets": 7, "searchable": true },
                        { "orderable": true, "targets": 8, "searchable": true },
                        { "orderable": false, "targets": 9, "searchable": false, "width":170}
                      ]
                    } );
                </script>
    <?php 
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_notif_rencana_sosialisasi_data"){ //Rencana Sosialisasi Data
  $aColumns = array('rcn.created_date','kgt.kegiatan','rcn.tgl_pelaksanaan','rcn.tempat','rcn.tema','rcn.lampiran','pv.nama_prov','upt.nama_upt','rcn.kode_rencana_sosialisasi'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);
  $data_notif = "";

    $variableAry=explode(",",base64_decode($_REQUEST['data_notif']));
        foreach($variableAry as $var) {
            $data_notif .= "'".$var."',";
        }

    $data_notif = rtrim($data_notif,',');
    $data_notif = base64_encode($data_notif);

  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_notif->getRencanaSosialisasiNotif($sWhere,$sOrder,$sLimit,$data_notif);
  $rResultFilterTotal   = $md_notif->getCountRencanaSosialisasiNotif($sWhere,$data_notif);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_rencana_sosialisasi'];
    
    $detail = '<button title="Detail"  type="button"  onclick="do_detail_rencana_so_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
 
    $sts = "";
    if($aRow['status']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }
    $lampiran ="";
    if(!empty($aRow['lampiran'])){
       $lampiran = '<center><a   title="Download File" style="cursor:pointer;color:blue" href="'.$basepath.'assets/attachment/sosialisasi_bimtek/rencana_sosialisasi/'.$aRow['lampiran'].'" download><i class="fa fa-download fa-2x"></i></a></center>';
    }
    $edit_delete = $detail;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$sts,$aRow['kegiatan'],$gen_controller->get_date_indonesia($aRow['tgl_pelaksanaan']),$aRow['tempat'],$aRow['tema'],$lampiran,$aRow['nama_prov'],$aRow['nama_upt'],"<center>".$edit_delete."</center>");

    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_notif_monev_sosialisasi_table"){ //Monev Sosialisasi
   if(!empty($_SESSION['kode_user'])){ ?>
          <table id="tb_notif_table_data" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Jenis Kegiatan</th>
                    <th class="text-center">Tanggal Pelaksanaan</th>
                    <th class="text-center">Tempat</th>
                    <th class="text-center">Tema</th>
                    <th class="text-center">Lampiran</th>
                    <th class="text-center">Provinsi</th>
                    <th class="text-center">UPT</th>
                    <th class="text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody></tbody>
                </table>
                <script type="text/javascript">
                   var dTable_notif;
                    dTable_notif = $('#tb_notif_table_data').DataTable( {
                      "bProcessing": true,
                      "bServerSide": true,
                      "bJQueryUI": false,
                      "responsive": false,
                      "autoWidth": false,
                      "retrieve": true,
                      "bDestroy":true,
                      "sAjaxSource": "<?php echo $basepath ?>ajax/rest_notif_monev_sosialisasi_data&data_notif=<?php echo $_REQUEST['data_notif'] ?>", 
                      "sServerMethod": "POST",
                      "scrollX": true,
                      "scrollY": "350px",
                        "scrollCollapse": true,
                        "order": [[ 0, "desc" ]],
                      "columnDefs": [
                        { "orderable": false, "targets": 0, "searchable": false},
                        { "orderable": true, "targets": 1, "searchable": true},
                        { "orderable": true, "targets": 2, "searchable": true},
                        { "orderable": true, "targets": 3, "searchable": true},
                        { "orderable": true, "targets": 4, "searchable": true },
                        { "orderable": true, "targets": 5, "searchable": true },
                        { "orderable": true, "targets": 6, "searchable": true },
                        { "orderable": true, "targets": 7, "searchable": true },
                        { "orderable": true, "targets": 8, "searchable": true },
                        { "orderable": false, "targets": 9, "searchable": false, "width":170}
                      ]
                    } );
                </script>
    <?php 
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_notif_monev_sosialisasi_data"){ //Monev Sosialisasi Data
   $aColumns = array('mnv.created_date','kgt.kegiatan','mnv.tgl_pelaksanaan','mnv.tempat','mnv.tema','mnv.lampiran','pv.nama_prov','upt.nama_upt','mnv.kode_monev_sosialisasi'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);
  $data_notif = "";

    $variableAry=explode(",",base64_decode($_REQUEST['data_notif']));
        foreach($variableAry as $var) {
            $data_notif .= "'".$var."',";
        }

    $data_notif = rtrim($data_notif,',');
    $data_notif = base64_encode($data_notif);

  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_notif->getMonevSosialisasiNotif($sWhere,$sOrder,$sLimit,$data_notif);
  $rResultFilterTotal   = $md_notif->getCountMonevSosialisasiNotif($sWhere,$data_notif);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_monev_sosialisasi'];
    
    $detail = '<button title="Detail"  type="button"  onclick="do_detail_monev_so_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
 
    $sts = "";
    if($aRow['status']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }
    $lampiran ="";
    if(!empty($aRow['lampiran'])){
       $lampiran = '<center><a   title="Download File" style="cursor:pointer;color:blue" href="'.$basepath.'assets/attachment/sosialisasi_bimtek/monev_sosialisasi/'.$aRow['lampiran'].'" download><i class="fa fa-download fa-2x"></i></a></center>';
    }
    $edit_delete = $detail;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$sts,$aRow['kegiatan'],$gen_controller->get_date_indonesia($aRow['tgl_pelaksanaan']),$aRow['tempat'],$aRow['tema'],$lampiran,$aRow['nama_prov'],$aRow['nama_upt'],"<center>".$edit_delete."</center>");

    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_notif_galeri_table"){ //Galeri
   if(!empty($_SESSION['kode_user'])){ ?>
          <table id="tb_notif_table_data" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Kegiatan</th>
                    <th class="text-center">Lampiran</th>
                    <th class="text-center">Provinsi</th>
                    <th class="text-center">UPT</th>
                    <th class="text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody></tbody>
                </table>
                <script type="text/javascript">
                   var dTable_notif;
                    dTable_notif = $('#tb_notif_table_data').DataTable( {
                      "bProcessing": true,
                      "bServerSide": true,
                      "bJQueryUI": false,
                      "responsive": false,
                      "autoWidth": false,
                      "retrieve": true,
                      "bDestroy":true,
                      "sAjaxSource": "<?php echo $basepath ?>ajax/rest_notif_galeri_data&data_notif=<?php echo $_REQUEST['data_notif'] ?>", 
                      "sServerMethod": "POST",
                      "scrollX": true,
                      "scrollY": "350px",
                        "scrollCollapse": true,
                        "order": [[ 0, "desc" ]],
                      "columnDefs": [
                        { "orderable": false, "targets": 0, "searchable": false},
                        { "orderable": true, "targets": 1, "searchable": true},
                        { "orderable": true, "targets": 2, "searchable": true},
                        { "orderable": true, "targets": 3, "searchable": true},
                        { "orderable": true, "targets": 4, "searchable": true },
                        { "orderable": true, "targets": 5, "searchable": true },
                        { "orderable": false, "targets": 6, "searchable": false, "width":170}
                      ]
                    } );
                </script>
    <?php 
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_notif_galeri_data"){ //Galeri Data
   $aColumns = array('gl.created_date','gl.status','gl.kegiatan','gl.lampiran','pv.nama_prov','upt.nama_upt','gl.kode_galeri'); //Kolom Pada Tabel


    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);
  $data_notif = "";

    $variableAry=explode(",",base64_decode($_REQUEST['data_notif']));
        foreach($variableAry as $var) {
            $data_notif .= "'".$var."',";
        }

    $data_notif = rtrim($data_notif,',');
    $data_notif = base64_encode($data_notif);

  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_notif->getGaleriNotif($sWhere,$sOrder,$sLimit,$data_notif);
  $rResultFilterTotal   = $md_notif->getCountGaleriNotif($sWhere,$data_notif);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_galeri'];
    
    $detail = '<button title="Detail"  type="button"  onclick="do_detail_galeri_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
 
    $sts = "";
    if($aRow['status']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }
    $lampiran ="";
    if(!empty($aRow['lampiran'])){
       $lampiran = '<center><a   title="Download File" style="cursor:pointer;color:blue" href="'.$basepath.'assets/attachment/sosialisasi_bimtek/galeri/'.$aRow['lampiran'].'" download><i class="fa fa-download fa-2x"></i></a></center>';
    }
    $edit_delete = $detail;
    $row = array();
     $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$sts,$aRow['kegiatan'],$lampiran,$aRow['nama_prov'],$aRow['nama_upt'],"<center>".$edit_delete."</center>");

    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_notif_validasi_data_table"){ //Validasi Data
   if(!empty($_SESSION['kode_user'])){ ?>
          <table id="tb_notif_table_data" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
                    <th class="text-center" rowspan="2">Tanggal Buat</th>
                    <th class="text-center" rowspan="2">Status</th>
                    <th class="text-center" rowspan="2">Data SIMS</th>
                    <th class="text-center" rowspan="2">Data Sampling</th>
                    <th class="text-center" colspan="3">Data Hasil Inspeksi</th>
                    <th class="text-center" colspan="3">Tindak Lanjut Hasil Inspeksi</th>
                    <th class="text-center" rowspan="2">Capaian (% Valid)</th>
                    <th class="text-center" rowspan="2">Lampiran</th>
                    <th class="text-center" rowspan="2">Provinsi</th>
                    <th class="text-center" rowspan="2">UPT</th>
                    <th class="text-center" rowspan="2">Keterangan</th>
                    <th class="text-center" rowspan="2">Aksi</th>
                  </tr>
                  <tr>
                    <th  class="text-center">Sesuai ISR</th>
                    <th  class="text-center">Tidak Sesuai</th>
                    <th  class="text-center">Tidak Aktif</th> 
                    <th  class="text-center">Proses ISR</th>
                    <th  class="text-center">Sudah di tindaklanjuti</th>
                    <th  class="text-center">Belum di tindaklanjuti</th>
                  </tr> 
                  <tr>
                    <th  class="text-center">1</th>
                    <th  class="text-center">2</th>
                    <th  class="text-center">3</th> 
                    <th  class="text-center">4</th>
                    <th  class="text-center">5</th>
                    <th  class="text-center">6</th>
                    <th  class="text-center">7</th>
                    <th  class="text-center">8</th>
                    <th  class="text-center">9</th>
                    <th  class="text-center">10</th>
                    <th  class="text-center">11</th>
                    <th  class="text-center">12</th>
                    <th  class="text-center">13</th>
                    <th  class="text-center">14</th>
                    <th  class="text-center">15</th>
                    <th  class="text-center">16</th>
                  </tr>
                </thead>
                <tbody></tbody>
                </table>
                <script type="text/javascript">
                   var dTable_notif;
                    dTable_notif = $('#tb_notif_table_data').DataTable( {
                      "bProcessing": true,
                      "bServerSide": true,
                      "bJQueryUI": false,
                      "responsive": false,
                      "autoWidth": false,
                      "retrieve": true,
                      "bDestroy":true,
                      "sAjaxSource": "<?php echo $basepath ?>ajax/rest_notif_validasi_data&data_notif=<?php echo $_REQUEST['data_notif'] ?>", 
                      "sServerMethod": "POST",
                      "scrollX": true,
                      "scrollY": "350px",
                        "scrollCollapse": true,
                        "order": [[ 0, "desc" ]],
                      "columnDefs": [
                        { "orderable": false, "targets": 0, "searchable": false},
                        { "orderable": true, "targets": 1, "searchable": true},
                        { "orderable": true, "targets": 2, "searchable": true},
                        { "orderable": true, "targets": 3, "searchable": true},
                        { "orderable": true, "targets": 4, "searchable": true },
                        { "orderable": true, "targets": 5, "searchable": true },
                        { "orderable": true, "targets": 6, "searchable": true },
                        { "orderable": true, "targets": 7, "searchable": true },
                        { "orderable": true, "targets": 8, "searchable": true },
                        { "orderable": true, "targets": 9, "searchable": true },
                        { "orderable": true, "targets": 10, "searchable": true },
                        { "orderable": true, "targets": 11, "searchable": true },
                        { "orderable": true, "targets": 12, "searchable": true },
                        { "orderable": true, "targets": 13, "searchable": true },
                        { "orderable": true, "targets": 14, "searchable": true },
                        { "orderable": false, "targets": 15, "searchable": false, "width":170}
                      ]
                    } );
                </script>
    <?php 
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_notif_validasi_data"){ //Validasi Data
   $aColumns = array('vld.created_date','vld.status','vld.data_sims','vld.data_sampling','vld.sesuai_isr','vld.tdk_sesuai_isr','vld.tidak_aktif','vld.proses_isr','vld.sudah_ditindaklanjuti','vld.belum_ditindaklanjuti','vld.capaian','vld.lampiran','pv.nama_prov','upt.nama_upt','vld.keterangan','vld.kode_validasi_data'); //Kolom Pada Tabel



    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);
  $data_notif = "";

    $variableAry=explode(",",base64_decode($_REQUEST['data_notif']));
        foreach($variableAry as $var) {
            $data_notif .= "'".$var."',";
        }

    $data_notif = rtrim($data_notif,',');
    $data_notif = base64_encode($data_notif);

  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_notif->getValidasiDataNotif($sWhere,$sOrder,$sLimit,$data_notif);
  $rResultFilterTotal   = $md_notif->getCountValidasiDataNotif($sWhere,$data_notif);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_validasi_data'];
    
    $detail = '<button title="Detail"  type="button"  onclick="do_detail_validasi_data_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
 
    $sts = "";
    if($aRow['status']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }
    $lampiran ="";
    if(!empty($aRow['lampiran'])){
       $lampiran = '<center><a   title="Download File" style="cursor:pointer;color:blue" href="'.$basepath.'assets/attachment/validasi_data/'.$aRow['lampiran'].'" download><i class="fa fa-download fa-2x"></i></a></center>';
    }
    $edit_delete = $detail;
    $row = array();
      $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$sts,$aRow['data_sims'],$aRow['data_sampling'],$aRow['sesuai_isr'],$aRow['tdk_sesuai_isr'],$aRow['tidak_aktif'],$aRow['proses_isr'],$aRow['sudah_ditindaklanjuti'],$aRow['belum_ditindaklanjuti'],$aRow['capaian'],$lampiran,$aRow['nama_prov'],$aRow['nama_upt'],$aRow['keterangan'],"<center>".$edit_delete."</center>");


    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_notif_monev_kinerja_table"){ //Monev Kinerja
	 if(!empty($_SESSION['kode_user'])){ ?>
    		  <table id="tb_notif_table_data" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
                    <th class="text-center">Tanggal Buat</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Kinerja</th>
                    <th class="text-center">Indikator Kinerja Kegiatan</th>
                    <th class="text-center">Target</th>
                    <th class="text-center">Renaksi Kinerja Target Uraian</th>
                    <th class="text-center">Pagu Anggaran</th>
                    <th class="text-center">Realisasi</th>
                    <th class="text-center">Data Dukung</th>
                    <th class="text-center">Provinsi</th>
                    <th class="text-center">UPT</th>
                    <th class="text-center">Keterangan</th>
                    <th class="text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody></tbody>
                </table>
                <script type="text/javascript">
                	 var dTable_notif;
				            dTable_notif = $('#tb_notif_table_data').DataTable( {
				              "bProcessing": true,
				              "bServerSide": true,
				              "bJQueryUI": false,
				              "responsive": false,
				              "autoWidth": false,
				              "retrieve": true,
				              "bDestroy":true,
				              "sAjaxSource": "<?php echo $basepath ?>ajax/rest_notif_monev_kinerja_data&data_notif=<?php echo $_REQUEST['data_notif'] ?>", 
				              "sServerMethod": "POST",
				              "scrollX": true,
				              "scrollY": "350px",
				                "scrollCollapse": true,
				                "order": [[ 0, "desc" ]],
				              "columnDefs": [
				              	 { "orderable": false, "targets": 0, "searchable": false},
                      { "orderable": true, "targets": 1, "searchable": true},
                      { "orderable": true, "targets": 2, "searchable": true},
                      { "orderable": true, "targets": 3, "searchable": true},
                      { "orderable": true, "targets": 4, "searchable": true },
                      { "orderable": true, "targets": 5, "searchable": true },
                      { "orderable": true, "targets": 6, "searchable": true },
                      { "orderable": true, "targets": 7, "searchable": true },
                      { "orderable": true, "targets": 8, "searchable": true },
                      { "orderable": true, "targets": 9, "searchable": true },
                      { "orderable": true, "targets": 10, "searchable": true },
                      { "orderable": true, "targets": 11, "searchable": true },
                      { "orderable": false, "targets": 12, "searchable": false, "width":200}
				              ]
				            } );
                </script>
    <?php 
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_notif_monev_kinerja_data"){ //Monev Kinerja
  $aColumns = array('mnv.created_date','mnv.status','mnv.kinerja','mnv.indikator_kinerja','mnv.target','mnv.renaksi_kinerja','mnv.pagu_anggaran','mnv.realisasi','mnv.data_dukung','pv.nama_prov','upt.nama_upt','mnv.keterangan','mnv.kode_monev_kinerja'); //Kolom Pada Tabel


    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);
  $data_notif = "";

    $variableAry=explode(",",base64_decode($_REQUEST['data_notif']));
        foreach($variableAry as $var) {
            $data_notif .= "'".$var."',";
        }

    $data_notif = rtrim($data_notif,',');
    $data_notif = base64_encode($data_notif);

  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_notif->getMonevKinerjaNotif($sWhere,$sOrder,$sLimit,$data_notif);
  $rResultFilterTotal   = $md_notif->getCountMonevKinerjaNotif($sWhere,$data_notif);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_monev_kinerja'];
    
    $detail = '<button title="Detail"  type="button"  onclick="do_detail_monev_kinerja_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
 
    $sts = "";
    if($aRow['status']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }
    
     //Renaksi
     $renaksi   = "";
     $renaksi_exp=explode("|",$aRow['renaksi_kinerja']);
     $i = 1;
     foreach($renaksi_exp as $var) {
      $zz = $i;
      if($i<10){
        $zz = "0".$i; 
      }
      if($i<=12){
        $renaksi .= "B".$zz." : ".$var. "<br/>";
          $i++;
      }
     } 
     
     //Realisasi
     $realisasi = "";
     $realisasi_exp=explode("|",$aRow['realisasi']);
     $iz = 1;
     foreach($realisasi_exp as $var) {
      $zz = $iz;
      if($iz<10){
        $zz = "0".$iz; 
      }
      if($iz<=12){
        $realisasi .= "B".$zz." : ".$var. "<br/>";
         $iz++;
      }
     }


    $edit_delete = $detail;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$sts,$aRow['kinerja'],$aRow['indikator_kinerja'],$aRow['target'],$renaksi,$aRow['pagu_anggaran'],$realisasi,$aRow['data_dukung'],$aRow['nama_prov'],$aRow['nama_upt'],$aRow['keterangan'],"<center>".$edit_delete."</center>");

    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
?>