<?php 
//General Controller
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();


//Model User
include "model/user.php";
$md_user      = new user();
session_start();

$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}

$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //Check Session
  $gen_controller->check_session();

  //View
  include "view/header.php";
  include "view/setting_filter_import.php";
  include "view/default_style.php";
  include "view/setting.php";
  include "view/footer.php";
}
else if($act=="update_web"){
    if(!empty($_SESSION['kode_user'])){
      //Proses
      $update_data = array();
      $update_data['judul']                  = get($_POST['judul']);
      $update_data['no_tlp']                 = get($_POST['no_tlp']);
      $update_data['no_hp']                  = get($_POST['no_hp']);
      $update_data['no_fax']                 = get($_POST['no_fax']);
      $update_data['email']                  = get($_POST['email']);
      $update_data['alamat']                 = get($_POST['alamat']);
      $update_data['running_text']           = get($_POST['running_text']);
      $update_data['running_text_color']     = get($_POST['running_text_color']);
      $update_data['running_text_color_bg']  = get($_POST['running_text_color_bg']);
      $update_data['running_text_size']      = get($_POST['running_text_size']);
      $update_data['note_pengisian']         = get($_POST['note_pengisian']);
      $update_data['last_update']            = $date_now_indo_full;
      $update_data['last_update_by']         = $_SESSION['kode_user'];

       //Validation
      if(!empty($_POST['judul'])){
            echo $gen_model->Update('ms_web',$update_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else {
  $gen_controller->response_code(http_response_code());
}
?>