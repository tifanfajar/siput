<?php 
//General Controller
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();

//Model User
include "model/user.php";
$md_user      = new user();

session_start();
//Check Session
$gen_controller->check_session();


$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}

$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //View
	include "view/header.php";
	include "view/dashboard.php";
	include "view/footer.php";
}
else if($act=="rest_filter"){ 
    $prov = $_REQUEST['provinsi'];
    $upt  = $_REQUEST['upt'];
    $tgl_awal   = $_REQUEST['tgl_awal'];
    $tgl_akhir  = $_REQUEST['tgl_akhir'];

    $qry_prov = "";
    $qry_upt  = "";
    $qry_tgl  = "";
    if(!empty($prov)){
        $qry_prov = " and pv.id_prov='".$prov."' ";
    }
    if(!empty($upt)){
        $qry_upt = " and upt.kode_upt='".$upt."' ";
    }

    if(!empty($tgl_awal) && !empty($tgl_akhir)){
      $qry_tgl  = " and created_date between '".$gen_controller->date_indo_default($tgl_awal)."' and  '".$gen_controller->date_indo_default($tgl_akhir)."' ";
    }
    else if(!empty($tgl_awal)){
      $qry_tgl  = " and created_date like '".$gen_controller->date_indo_default($tgl_awal)."%' ";
    }
    else if(!empty($tgl_akhir)){
      $qry_tgl  = " and created_date like '".$gen_controller->date_indo_default($tgl_awal)."%' ";
    } 
  ?>
   <table id="tb_dashboard" class="table table-bordered table-cont">
                      <thead>
                        <tr>
                          <th rowspan="2" class="align-middle">Provinsi</th>
                          <th rowspan="2" class="align-middle">Nama UPT</th>
                          <?php 
                               $sql_men="select * from ms_menu_tab where tab_id between '1' and '12' order by urutan asc";
                                $result_men= $db->Execute($sql_men);
                                while($men=$result_men->FetchRow()){ ?>
                                   <th colspan="4"><?php echo $men['tab'] ?></th>
                           <?php } ?>
                        </tr>
                        <tr>
                           <?php 
                               $sql_men="select * from ms_menu_tab where tab_id  between '1' and '12'  order by urutan asc";
                                $result_men= $db->Execute($sql_men);
                                while($men=$result_men->FetchRow()){ ?>
                                    <th>Approve</th>
                                    <th>Reject</th>
                                    <th>Waiting Verification</th>
                                    <th>Waiting Update</th>
                           <?php } ?>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                          $data_prov_upt = $db->SelectLimit("select pv.id_prov,pv.nama_prov,upt.kode_upt,upt.nama_upt
                                                              from ms_provinsi as pv
                                                                  inner join ms_upt as upt
                                                                      on pv.id_prov=upt.id_prov
                                                                        where 1=1 ".$qry_prov."  ".$qry_upt."
                                                                       order by pv.id_prov asc "); 
                          while($aRow = $data_prov_upt->FetchRow()){ ?>
                        <tr>
                            <td><?php echo $aRow['nama_prov'] ?></td>
                            <td><?php echo $aRow['nama_upt'] ?></td>
                            <?php 
                               $sql_men="select * from ms_menu_tab where tab_id  between '1' and '12'  order by urutan asc";
                                $result_men= $db->Execute($sql_men);
                                while($men=$result_men->FetchRow()){ 

                                $approve =  $db->GetOne("select count(*) as total from ".$men['table']."  where  status='3' and id_prov='".$aRow['id_prov']."' and kode_upt='".$aRow['kode_upt']."' ".$qry_tgl." ");


                                $reject =  $db->GetOne("select count(*) as total from ".$men['table']."  where status='1' and id_prov='".$aRow['id_prov']."' and kode_upt='".$aRow['kode_upt']."' ".$qry_tgl." ");

                                $verif = $db->GetOne("select count(*) as total from ".$men['table']."  where status='0' and id_prov='".$aRow['id_prov']."' and kode_upt='".$aRow['kode_upt']."' ".$qry_tgl." ");

                                $update = $db->GetOne("select count(*) as total from ".$men['table']."  where status='4' and id_prov='".$aRow['id_prov']."' and kode_upt='".$aRow['kode_upt']."' ".$qry_tgl." ");

                                      $approve = (!empty($approve) ? $approve : '0');
                                      $reject =  (!empty($reject) ? $reject : '0');
                                      $verif =   (!empty($verif) ? $verif : '0');
                                      $update =  (!empty($update) ? $update : '0');
                                  ?>
                                    <td align="right"><?php echo $approve ?></td>
                                    <td align="right"><?php echo $reject ?></td>
                                    <td align="right"><?php echo $verif ?></td>
                                    <td align="right"><?php echo $update ?></td>
                           <?php } ?>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table> 
<?php } ?>