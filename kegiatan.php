<?php 
//General Controller
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();

//Model Materi
include "model/kegiatan.php";
$md_kgt  = new kegiatan();


//Model User
include "model/user.php";
$md_user      = new user();
session_start();

//Check Access Tab
$akses_tab = $gen_model->GetOneRow("tr_function_access_tab",array('id_tab'=>19,'kode_function_access'=>$_SESSION['group_user']));
if(empty($akses_tab['fua_edit'])){
  $akses_tab['fua_edit'] = 0;
}
if(empty($akses_tab['fua_delete'])){
  $akses_tab['fua_delete'] = 0;
}


$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}

$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //Check Session
  $gen_controller->check_session();

  //View
  include "view/header.php";
  include "view/kegiatan.php";
  include "view/footer.php";
}
else if($act=="form"){
   include "view/default_style.php"; 
   $form_url  = $_REQUEST['form_url'];
   $activity  = $_REQUEST['activity'];
   if($form_url=="Form_Kegiatan"){
      include "view/ajax_form/kegiatan_form_data.php";
   }
}
else if($act=="add"){
    if(!empty($_SESSION['kode_user'])){
      //Proses
      $insert_data = array();
      $insert_data['kode_kegiatan']       = "kgt_".date("ymdhis")."_".rand(1000,9999);
      $insert_data['kegiatan']            = get($_POST['kegiatan']);
      $insert_data['created_date']        = $date_now_indo_full;
      $insert_data['last_update']         = $date_now_indo_full;
      $insert_data['created_by']          = $_SESSION['kode_user'];
      $insert_data['last_update_by']      = $_SESSION['kode_user'];
 
       //Validation
      if($insert_data['kegiatan']!=""){
           echo $gen_model->Insert('ms_kegiatan',$insert_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="delete"){
    if(!empty($_SESSION['kode_user'])){

       $update_data['status']              = "0"; //Delete

       //Paramater
      $where_data = array();
      $where_data['kode_kegiatan']       = $_POST['kode_kegiatan'];


       //Validation
      if(!empty($_POST['kode_kegiatan'])){
            echo $gen_model->Update('ms_kegiatan',$update_data,$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="edit" and $id_parameter!=""){
    $edit  = $gen_model->GetOneRow("ms_kegiatan",array('kode_kegiatan'=>$id_parameter)); 
    foreach($edit as $key=>$val){
                  $key=strtolower($key);
                  $$key=$val;
    }
    $data = array(
      'kode_kegiatan'=>$kode_kegiatan,
      'kegiatan'=>$kegiatan
    );
    echo json_encode($data); 
}
else if($act=="update"){
    if(!empty($_SESSION['kode_user'])){
      //Proses
      $update_data = array();
      $update_data['kegiatan']        = get($_POST['kegiatan']);
      $update_data['last_update']         = $date_now_indo_full;
      $update_data['last_update_by']      = $_SESSION['kode_user'];

       //Paramater
      $where_data = array();
      $where_data['kode_kegiatan']       = $_POST['kode_kegiatan'];


       //Validation
      if(!empty($_POST['kode_kegiatan'])){
           echo $gen_model->Update('ms_kegiatan',$update_data,$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="rest"){
  $aColumns = array('kgt.kode_kegiatan','kgt.kegiatan','kgt.kode_kegiatan'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_kgt->getKegiatan($sWhere,$sOrder,$sLimit);
  $rResultFilterTotal   = $md_kgt->getCountKegiatan($sWhere);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $param_id = $aRow['kode_kegiatan'];
    $edit="";
    $delete="";

    if($akses_tab['fua_edit']=="1"){ 
       //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($aRow['created_by']==$_SESSION['kode_user']){
               $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_kgt(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
        }
        else {
           $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_kgt(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
        //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($aRow['created_by']==$_SESSION['kode_user']){
               $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_kgt(\''.base64_encode('kode_kegiatan').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
        }
        else {
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_kgt(\''.base64_encode('kode_kegiatan').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }

    $edit_delete = $edit.$delete;
    $row = array();
    $row = array($aRow['kode_kegiatan'],$aRow['kegiatan'],$edit_delete);
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else {
  $gen_controller->response_code(http_response_code());
}
?>