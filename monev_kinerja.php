<?php 
//General Controller
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();


//Model User
include "model/user.php";
$md_user      = new user();


//Model Monev Kinerja
include "model/monev_kinerja.php";
$md_monev  = new monev_kinerja();
session_start();

//Check Acces Menu and Tab For Notification
$id_tab  = 12;
$id_menu = 5; 
$grp_menu_ntf = get_grp_notif($id_menu,$id_tab);
$group_ntf    = array();
if(!empty($grp_menu_ntf)){
  $variableAry=explode(",",$grp_menu_ntf); 
  foreach($variableAry as $grp) {
    if(!empty($grp)){
     array_push($group_ntf,$grp);
    }
  }
}

//Check Access Tab
$akses_tab = $gen_model->GetOneRow("tr_function_access_tab",array('id_tab'=>$id_tab,'kode_function_access'=>$_SESSION['group_user'])); 
if(empty($akses_tab['fua_edit'])){
  $akses_tab['fua_edit'] = 0;
}
if(empty($akses_tab['fua_delete'])){
  $akses_tab['fua_delete'] = 0;
}

$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}

$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //Check Session
  $gen_controller->check_session();

  //View
  include "view/header.php";
  include "view/monev_kinerja_filter_import.php";
  include "view/monev_kinerja.php";
  include "view/footer.php";
}
else if($act=="form_all"){
   include "view/default_style.php"; 
   $activity  = $_REQUEST['activity'];
   $form_url  = $_REQUEST['form_url'];
   if($form_url=="Form_Monev_Kinerja_ALL"){
      include "view/ajax_form/monev_kinerja_form_all.php";
   }
}
else if($act=="form"){
   include "view/default_style.php"; 
   $form_url  = $_REQUEST['form_url'];
   $map_id    = $_REQUEST['map_id'];
   $upt       = $_REQUEST['upt'];
   $activity  = $_REQUEST['activity'];
   $wilayah   = $gen_model->GetOneRow("ms_provinsi",array('id_map'=>$map_id)); 
   if($form_url=="Form_Monev_Kinerja"){
      include "view/ajax_form/monev_kinerja_form.php";
   }
}
else if($act=="table"){
   $upt     = $_REQUEST['upt'];
   $div_id  = $_REQUEST['div_id'];
   $map_id  = $_REQUEST['map_id'];
   $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_map'=>$map_id)); 
   if($div_id=="monev_kinerja_table_data_utama"){
      include "view/ajax_table/monev_kinerja.php";
   }
}
else if($act=="pilih_upt"){
   $map_id  = $_REQUEST['map_id'];
   $div_id  = $_REQUEST['div_id'];
   $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_map'=>$map_id));  
   $total_upt = $gen_model->GetOne(' count(*) as total ','ms_upt',array('id_prov'=>$wilayah['id_prov']));
   if($total_upt==0){
    echo '<label class="loc-lbl">UPT '.$wilayah['nama_prov'].'</label>';
    echo '<center>Tidak ada UPT di provinsi ini</center>';
   }
   else if($total_upt==1){
     echo "<script>";
     $upt = $gen_model->GetOne('kode_upt','ms_upt',array('id_prov'=>$wilayah['id_prov']));
      if($div_id=="monev_kinerja_table_data_utama"){ 
          echo " detail_monev_kinerja('".$map_id."','".$upt."') ";
       }
       echo "</script>";
   }  
   else { ?>
   <div style="background-color:white">
   <label class="loc-lbl">UPT <?php echo $wilayah['nama_prov'] ?></label>
     <div class="row">
        <div class="col-md-12">
           <select class="form_control" onchange="get_detail()" id="data_upt" style="width: 100%;">
              <?php 
                 $data_upt = $gen_model->GetWhere('ms_upt',array('id_prov'=>$wilayah['id_prov']));
                 echo '<option value="">Pilih UPT</option>';
                  while($list = $data_upt->FetchRow()){
                    foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
                              }  
                ?>
              <option value="<?php echo $kode_upt ?>"><?php echo $nama_upt ?></option>
              <?php }  ?>
           </select>
        </div>
     </div>
   </div>
   <script type="text/javascript">
     function get_detail(){
        var  map_id = "<?php echo $map_id ?>";
        var  upt    = $("#data_upt").val();
        if(upt){
            <?php 
                if($div_id=="monev_kinerja_table_data_utama"){
                    echo " detail_monev_kinerja(map_id,upt) ";
                 }
            ?>
        }
     }
   </script>
   <?php
   }
}
else if($act=="get_group_perizinan"){
    echo "<option value=''>Pilih Group Perizinan</option>";
    $get_grp  = $gen_model->GetWhere("ms_group_layanan",array('jenis'=>$_POST['jenis'])); 
    while($list = $get_grp->FetchRow()){
      foreach($list as $key=>$val){
                        $key=strtolower($key);
                        $$key=$val;
      }  
      echo "<option value='".$id_group_layanan."'>".$group_layanan."</option>";
    }
}
else if($act=="cetak_detail"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/monev_kinerja.php";
}
else if($act=="cetak_all"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/monev_kinerja_all.php";
}
else if($act=="table_detail_filter"){
   $param  = $_REQUEST['param'];
   include "view/ajax_table/monev_kinerja_filter.php";
}
else if($act=="table_all_filter"){
   $param  = $_REQUEST['param'];
   include "view/ajax_table/monev_kinerja_filter_all.php";
}
else if($act=="import"){
   //File
    $target = basename($_FILES['lampiran']['name']) ;
    move_uploaded_file($_FILES['lampiran']['tmp_name'], $target);
    $path_info = pathinfo($target);
    $ext =  strtolower($path_info['extension']); 
    
    if($ext!="xls"){
      echo "Format file salah";
      die();
    }

    require_once 'lib/excel_reader.php';
    $data = new Spreadsheet_Excel_Reader($_FILES['lampiran']['name'],false);
    $baris = $data->rowcount($sheet_index=0);
    $kode="";
    $hitung_files=0;
    for ($i=2; $i<=$baris; $i++) {
        $insert_data = array();
        $insert_data['kode_monev_kinerja']      = "mnk_".date("ymdhis")."_".rand(1000,9999);
        $insert_data['id_prov']                 = get($_POST['id_prov']);
        $insert_data['kode_upt']                = get($_POST['id_upt']);
        $insert_data['kinerja']                 = $data->val($i, 1);
        $insert_data['indikator_kinerja']       = $data->val($i, 2);
        $insert_data['target']                  = $data->val($i, 3);
        $insert_data['renaksi_kinerja']         = $data->val($i, 4);
        $insert_data['pagu_anggaran']           = only_number($data->val($i, 5));
        $insert_data['realisasi']               = $data->val($i, 6);
        $insert_data['data_dukung']             = $data->val($i, 7);
        $insert_data['keterangan']              = $data->val($i, 8); 
        $insert_data['created_date']            = $date_now_indo_full;
        $insert_data['last_update']             = $date_now_indo_full;
        $insert_data['created_by']              = $_SESSION['kode_user'];
        $insert_data['last_update_by']          = $_SESSION['kode_user'];

        if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
          $insert_data['status']              = "0";
        }
        else { 
          $insert_data['status']              = "3";
        }

        if($insert_data['kinerja']!=""){
          $gen_model->Insert('tr_monev_kinerja',$insert_data);
          $kode .= $insert_data['kode_monev_kinerja'].",";
          $hitung_files++;
        }
    }
    $kode = rtrim($kode,",");
    //Notification
    if($hitung_files!=0){
              if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                  notifikasi('2','12',$_POST['id_prov'],$_POST['id_upt'],'grp_171026194411_1143','Monev Kinerja','Ada '.$hitung_files.' data baru pada Monev Kinerja yang harus di tinjau',$kode);
              }
              else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                notifikasi('4','12',$_POST['id_prov'],$_POST['id_upt'],'grp_171026194411_1142','Monev Kinerja','Ada '.$hitung_files.' data baru pada Monev Kinerja yang sudah di setujui',$kode);
              }
              else {
                foreach ($group_ntf as $id_group) {
                   notifikasi('1','12',$_POST['id_prov'],$_POST['id_upt'],$id_group,'Monev Kinerja','Ada '.$hitung_files.' data baru pada Monev Kinerja',$kode);
                }
              }
    }
    echo "OK";
    
    unlink($_FILES['lampiran']['name']);
}
else if($act=="import_all"){
   //File
    $target = basename($_FILES['lampiran']['name']);
    move_uploaded_file($_FILES['lampiran']['tmp_name'], $target);
    $path_info = pathinfo($target);
    $ext =  strtolower($path_info['extension']); 
    
    if($ext!="xls"){
      echo "Format file salah";
      die();
    }

    require_once 'lib/excel_reader.php';
    $data = new Spreadsheet_Excel_Reader($_FILES['lampiran']['name'],false);
    $baris = $data->rowcount($sheet_index=0);
    for ($i=2; $i<=$baris; $i++) {
        $insert_data = array();
        $insert_data['kode_monev_kinerja']      = "mnk_".date("ymdhis")."_".rand(1000,9999);
        $insert_data['id_prov']                 = $data->val($i, 1);
        $insert_data['kode_upt']                = $data->val($i, 2);
        $insert_data['kinerja']                 = $data->val($i, 3);
        $insert_data['indikator_kinerja']       = $data->val($i, 4);
        $insert_data['target']                  = $data->val($i, 5);
        $insert_data['renaksi_kinerja']         = $data->val($i, 6);
        $insert_data['pagu_anggaran']           = only_number($data->val($i, 7));
        $insert_data['realisasi']               = $data->val($i, 8);
        $insert_data['data_dukung']             = $data->val($i, 9);
        $insert_data['keterangan']              = $data->val($i, 10); 
        $insert_data['created_date']            = $date_now_indo_full;
        $insert_data['last_update']             = $date_now_indo_full;
        $insert_data['created_by']              = $_SESSION['kode_user'];
        $insert_data['last_update_by']          = $_SESSION['kode_user'];

        if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
          $insert_data['status']              = "0";
        }
        else { 
          $insert_data['status']              = "3";
        }

        if( !empty($data->val($i, 1)) && !empty($data->val($i, 2)) ){ //Provinsi && UPT
            $gen_model->Insert('tr_monev_kinerja',$insert_data);
            
              //For Notification All
            $upt=$insert_data['kode_upt'];
            $upt_array[$upt][] =  array (
                                    'kode' => $insert_data['kode_monev_kinerja'],
                                    'prov' => $insert_data['id_prov'],
                                    'upt'  => $upt
                                  );

        }
    }

    $keys = array_keys($upt_array);
      for($i = 0; $i < count($upt_array); $i++) {
          $hitung_files = 0;
          $kode  = "";
          foreach($upt_array[$keys[$i]] as $key => $value) {
             $kode .= $value['kode'].",";
              $upt   = $value['upt'];
              $prov  = $value['prov'];
              $hitung_files++;
          }
          $kode = trim($kode,',');
          // echo "Id Prov : ".$prov."<br/>";
          // echo "UPT : ".$upt."<br/>";
          // echo "Kode : ".$kode."<br/>";
          // echo "Total : ".$hitung_files."<br/><br/>";

          //Notification
           if($hitung_files!=0){
                    if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                      notifikasi('2','12',$prov,$upt,'grp_171026194411_1143','Monev Kinerja','Ada '.$hitung_files.' data baru pada Monev Kinerja yang harus di tinjau',$kode);
                    }
                    else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                      notifikasi('4','12',$prov,$upt,'grp_171026194411_1142','Monev Kinerja','Ada '.$hitung_files.' data baru pada Monev Kinerja yang sudah di setujui',$kode);
                    }
                    else {
                      foreach ($group_ntf as $id_group) {
                         notifikasi('1','12',$prov,$upt,$id_group,'Monev Kinerja','Ada '.$hitung_files.' data baru pada Monev Kinerja',$kode);
                      }
                    }
          }
      }
    echo "OK";
    
    unlink($_FILES['lampiran']['name']);
}
else if($act=="add"){


    if(!empty($_SESSION['kode_user'])){
      //Proses
      $insert_data = array();
      $insert_data['kode_monev_kinerja']     = "mnk_".date("ymdhis")."_".rand(1000,9999);
      $insert_data['kinerja']                = get($_POST['kinerja']);
      $insert_data['indikator_kinerja']      = get($_POST['indikator_kinerja']);
      $insert_data['target']                 = get($_POST['target']);
      $insert_data['data_dukung']            = get($_POST['data_dukung']);
      $insert_data['pagu_anggaran']          = only_number(get($_POST['pagu_anggaran']));
      $insert_data['kode_upt']               = get($_POST['loket_upt']);
      $insert_data['id_prov']                = get($_POST['id_prov']);
      $insert_data['keterangan']             = get($_POST['keterangan']);
      $insert_data['created_date']           = $date_now_indo_full;
      $insert_data['last_update']            = $date_now_indo_full;
      $insert_data['created_by']             = $_SESSION['kode_user'];
      $insert_data['last_update_by']         = $_SESSION['kode_user'];


        //Renaksi
        $renaksi = "";
        $i = 1;
        foreach($_POST['renaksi'] as $check) {
            if(empty($check)){
              $check = "0";
            }
            if($i<=12){
              $renaksi .= $check."|";
              $i++;
            }
        }
        $renaksi = rtrim($renaksi,'|');
        $insert_data['renaksi_kinerja']           = $renaksi;

         //Realisasi
        $realisasi = "";
        $iz = 1;
        foreach($_POST['realisasi'] as $check2) {
            if(empty($check2)){
              $check2 = "";
            }
            if($iz<=12){
              $realisasi .= $check2."|";
              $iz++;
            }
        }
        $realisasi = rtrim($realisasi,'|');
        $insert_data['realisasi']           = $realisasi;

       
        
        if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
          $insert_data['status']              = "0";
        }
        else { 
          $insert_data['status']              = "3";
        }


        if($_POST['id_prov']=="All"){
          $insert_data['id_prov'] ="";
        }
        if($_POST['loket_upt']=="All"){
          $insert_data['kode_upt']="";
       }

       //Validation
      if($insert_data['kinerja']!=""){
           if($gen_model->Insert('tr_monev_kinerja',$insert_data)=="OK"){
               //Notification
                if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                  notifikasi('2','12',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1143','Monev Kinerja','Ada 1 data baru pada Monev Kinerja yang harus di tinjau',$insert_data['kode_monev_kinerja']);
                }
                else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                  notifikasi('4','12',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1142','Monev Kinerja','Ada 1 data baru pada Monev Kinerja yang sudah di setujui',$insert_data['kode_monev_kinerja']);
                }
                else {
                  foreach ($group_ntf as $id_group) {
                     notifikasi('1','12',$_POST['id_prov'],$_POST['loket_upt'],$id_group,'Monev Kinerja','Ada 1 data baru pada Monev Kinerja',$insert_data['kode_monev_kinerja']);
                  }
                }
                echo "OK";
           }
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="edit" and $id_parameter!=""){
    $edit  = $gen_model->GetOneRow("tr_monev_kinerja",array('kode_monev_kinerja'=>$id_parameter)); 
    foreach($edit as $key=>$val){
                  $key=strtolower($key);
                  $$key=$val;
    }
    $data = array(
      'kode_monev_kinerja'=>$kode_monev_kinerja,
      'created_date'=>$gen_controller->get_date_indonesia($created_date),
      'kinerja'=>$kinerja,
      'indikator_kinerja'=>$indikator_kinerja,
      'target'=>$target,
      'renaksi_kinerja'=>$renaksi_kinerja,
      'realisasi'=>$realisasi,
      'data_dukung'=>$data_dukung,
      'pagu_anggaran'=>int_to_rp($pagu_anggaran),
      'kode_upt'=>$kode_upt,
      'id_prov'=>$id_prov,
      'keterangan'=>$keterangan
    );
    echo json_encode($data); 
}
else if($act=="update"){
    if(!empty($_SESSION['kode_user'])){
      //Proses
      $update_data = array();
      $update_data['kinerja']                = get($_POST['kinerja']);
      $update_data['indikator_kinerja']      = get($_POST['indikator_kinerja']);
      $update_data['target']                 = get($_POST['target']);
      $update_data['data_dukung']            = get($_POST['data_dukung']);
      $update_data['pagu_anggaran']          = only_number(get($_POST['pagu_anggaran']));
      $update_data['kode_upt']               = get($_POST['loket_upt']);
      $update_data['id_prov']                = get($_POST['id_prov']);
      $update_data['keterangan']             = get($_POST['keterangan']);
      $update_data['last_update']            = $date_now_indo_full;
      $update_data['last_update_by']         = $_SESSION['kode_user'];

      if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
        $update_data['status']              = "0";
      }
      else {
        //$update_data['status']              = "3";
      }

        //Renaksi
        $renaksi = "";
        $i = 1;
        foreach($_POST['renaksi'] as $check) {
            if(empty($check)){
              $check = "";
            }
            if($i<=12){
              $renaksi .= $check."|";
              $i++;
            }
        }
        $renaksi = rtrim($renaksi,'|');
        $update_data['renaksi_kinerja']           = $renaksi;

         //Realisasi
        $realisasi = "";
        $iz = 1;
        foreach($_POST['realisasi'] as $check2) {
            if(empty($check2)){
              $check2 = "";
            }
            if($iz<=12){
              $realisasi .= $check2."|";
              $iz++;
            }
        }
        $realisasi = rtrim($realisasi,'|');
        $update_data['realisasi']           = $realisasi;


        if($_POST['id_prov']=="All"){
          $update_data['id_prov'] ="";
        }
        if($_POST['loket_upt']=="All"){
          $update_data['kode_upt']="";
        }

       //Paramater
      $where_data = array();
      $where_data['kode_monev_kinerja']       = $_POST['kode_monev_kinerja'];

       //Validation
      if(!empty($_POST['kode_monev_kinerja'])){
            if($gen_model->Update('tr_monev_kinerja',$update_data,$where_data)=="OK"){
                    //Notification
                    if($_SESSION['group_user']!="grp_171026194411_1143" or $_SESSION['group_user']!="grp_171026194411_1144"){  //Not Operator or Not Kepala UPT
                        notifikasi('2','12',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1143','Monev Kinerja','Ada 1 data baru pada Monev Kinerja yang harus di tinjau',$_POST['kode_monev_kinerja']);
                    }
              echo "OK";
          }
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="delete"){
    if(!empty($_SESSION['kode_user'])){
       //Paramater
      $where_data = array();
      $where_data['kode_monev_kinerja']       = $_POST['kode_monev_kinerja'];

       $update_data['status']              = "2"; //Delete
       
       //Validation
      if(!empty($_POST['kode_monev_kinerja'])){
            echo $gen_model->Update('tr_monev_kinerja',$update_data,$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="verifikasi"){
    if(!empty($_SESSION['kode_user'])){
       $db->Execute("update tr_monev_kinerja set status='3' where kode_monev_kinerja in(".$_REQUEST['kode'].") ");
       $total = $_REQUEST['total'];
        if(!empty($total)){
        //Notification
          if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
            $kode=str_replace("'","",$_REQUEST['kode']);
            notifikasi('4','12',$_SESSION['upt_provinsi'],$_SESSION['kode_upt'],'grp_171026194411_1142','Monev Kinerja','Ada '.$total.' data baru pada Monev Kinerja yang sudah di setujui',$kode);
          }
        }
       echo "OK";
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="reject"){
    if(!empty($_SESSION['kode_user'])){
       $db->Execute("update tr_monev_kinerja set status='1',ket_sts_apr_reject='".$_REQUEST['ket']."' where kode_monev_kinerja in(".$_REQUEST['kode'].") ");
        $total = $_REQUEST['total'];
        if(!empty($total)){
        //Notification
          if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
            $kode=str_replace("'","",$_REQUEST['kode']);
            notifikasi('3','12',$_SESSION['upt_provinsi'],$_SESSION['kode_upt'],'grp_171026194411_1143','Monev Kinerja','Ada '.$total.' data baru pada Monev Kinerja yang sudah di reject',$kode);
          }
        }
       echo "OK";
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_waiting"){
  $prov = $_REQUEST['id_prov'];
  $upt = $_REQUEST['upt'];
  $aColumns = array('mnv.kode_monev_kinerja','mnv.created_date','mnv.status','mnv.kinerja','mnv.indikator_kinerja','mnv.target','mnv.renaksi_kinerja','mnv.pagu_anggaran','mnv.realisasi','mnv.data_dukung','mnv.keterangan','mnv.kode_monev_kinerja'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_monev->getMonevKinerjaWaiting($sWhere,$sOrder,$sLimit,$prov,$upt);
  $rResultFilterTotal   = $md_monev->getCountMonevKinerjaWaiting($sWhere,$prov,$upt);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
      $wilayah ="";
      if(!empty($aRow['id_prov'])){
       $wilayah = $gen_model->GetOne("id_map","ms_provinsi",array('id_prov'=>$aRow['id_prov']));
      }

    $param_id = $aRow['kode_monev_kinerja'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
    
     $checkbox="";
    if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
         if($aRow['status']!="4"){
             $checkbox = '<input type="checkbox" value="'.$param_id.'" name="kode_monev_kinerja">';
          }
    }
   
    $sts = "";
    if($aRow['status']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }
      //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
        //Operator & Kepala Upt
        if($_SESSION['group_user']=="grp_171026194411_1144" or $_SESSION['group_user']=="grp_171026194411_1143"){ 
            //Kepala Upt
            if($_SESSION['group_user']=="grp_171026194411_1143"){
               $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user'] ){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
        }
        else {
             $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
      //Operator & Kepala UPT
        if($_SESSION['group_user']=="grp_171026194411_1144" or $_SESSION['group_user']=="grp_171026194411_1143"){ 
            if($_SESSION['group_user']=="grp_171026194411_1143"){
               $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete(\''.base64_encode('kode_monev_kinerja').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user'] ){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete(\''.base64_encode('kode_monev_kinerja').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
        }
        else {
             $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete(\''.base64_encode('kode_monev_kinerja').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }

     //Renaksi
     $renaksi   = "";
     $renaksi_exp=explode("|",$aRow['renaksi_kinerja']);
     $i = 1;
     foreach($renaksi_exp as $var) {
      $zz = $i;
      if($i<10){
        $zz = "0".$i; 
      }
      if($i<=12){
        $renaksi .= "B".$zz." : ".$var. "<br/>";
          $i++;
      }
     } 
     
     //Realisasi
     $realisasi = "";
     $realisasi_exp=explode("|",$aRow['realisasi']);
     $iz = 1;
     foreach($realisasi_exp as $var) {
      $zz = $iz;
      if($iz<10){
        $zz = "0".$iz; 
      }
      if($iz<=12){
        $realisasi .= "B".$zz." : ".$var. "<br/>";
         $iz++;
      }
     }


    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($checkbox,$gen_controller->get_date_indonesia($aRow['created_date']),$sts,$aRow['kinerja'],$aRow['indikator_kinerja'],$aRow['target'],$renaksi,int_to_rp($aRow['pagu_anggaran']),$realisasi,$aRow['data_dukung'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest"){
   $aColumns = array('mnv.created_date','pv.nama_prov','upt.nama_upt','mnv.kinerja','mnv.indikator_kinerja','mnv.target','mnv.renaksi_kinerja','mnv.pagu_anggaran','mnv.realisasi','mnv.data_dukung','mnv.keterangan','mnv.kode_monev_kinerja'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_monev->getMonevKinerja($sWhere,$sOrder,$sLimit);
  $rResultFilterTotal   = $md_monev->getCountMonevKinerja($sWhere);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $nama_prov = $aRow['id_prov'];
    $nama_upt  = $aRow['kode_upt'];

    if($nama_prov=="Only_Admin"){
      $nama_prov = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_prov)){
      $nama_prov = "<center>Semua Provinsi</center>";
    } 
    else {
      $nama_prov  = $aRow['nama_prov'];
    } 

    if($nama_upt=="Only_Admin"){
      $nama_upt = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_upt)){
      $nama_upt = "<center>Semua UPT</center>";
    } 
    else {
      $nama_upt  = $aRow['nama_upt'];
    }   

    $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    $param_id = $aRow['kode_monev_kinerja'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_monev_kinerja_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

    $edit="";
    $delete="";

        if($akses_tab['fua_edit']=="1"){ 
           //Kepala UPT atau Operator UPT
            if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
                if($_SESSION['group_user']=="grp_171026194411_1143"){
                  $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
                }
                else if($aRow['created_by']==$_SESSION['kode_user']){
                   $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
                }
            }
            else {
               $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
        }
        if($akses_tab['fua_delete']=="1"){ 
            //Kepala UPT atau Operator UPT
            if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
                if($aRow['created_by']==$_SESSION['kode_user']){
                   $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_all(\''.base64_encode('kode_monev_kinerja').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                }
            }
            else {
               $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_all(\''.base64_encode('kode_monev_kinerja').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
        }


    $edit_delete = $detail.$edit.$delete;

     //Renaksi
     $renaksi   = "";
     $renaksi_exp=explode("|",$aRow['renaksi_kinerja']);
     $i = 1;
     foreach($renaksi_exp as $var) {
      $zz = $i;
      if($i<10){
        $zz = "0".$i; 
      }
      if($i<=12){
        $renaksi .= "B".$zz." : ".$var. "<br/>";
          $i++;
      }
     } 
     
     //Realisasi
     $realisasi = "";
     $realisasi_exp=explode("|",$aRow['realisasi']);
     $iz = 1;
     foreach($realisasi_exp as $var) {
      $zz = $iz;
      if($iz<10){
        $zz = "0".$iz; 
      }
      if($iz<=12){
        $realisasi .= "B".$zz." : ".$var. "<br/>";
         $iz++;
      }
     }

    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$nama_prov,$nama_upt,$aRow['kinerja'],$aRow['indikator_kinerja'],$aRow['target'],$renaksi,int_to_rp($aRow['pagu_anggaran']),$realisasi,$aRow['data_dukung'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_filter"){
  $param = $_REQUEST['param'];
  $aColumns = array('mnv.created_date','pv.nama_prov','upt.nama_upt','mnv.kinerja','mnv.indikator_kinerja','mnv.target','mnv.renaksi_kinerja','mnv.pagu_anggaran','mnv.realisasi','mnv.data_dukung','mnv.keterangan','mnv.kode_monev_kinerja'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_monev->getMonevKinerjaFilter($sWhere,$sOrder,$sLimit,$param);
  $rResultFilterTotal   = $md_monev->getCountMonevKinerjaFilter($sWhere,$param);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

   while($aRow = $rResult->FetchRow()){
    $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    $param_id = $aRow['kode_monev_kinerja'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_monev_kinerja_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
              $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
              $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
             }
        }
        else {
          $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
              $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_all(\''.base64_encode('kode_monev_kinerja').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_all(\''.base64_encode('kode_monev_kinerja').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
             }
        }
        else {
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_all(\''.base64_encode('kode_monev_kinerja').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }
    $edit_delete = $detail.$edit.$delete;

     //Renaksi
     $renaksi   = "";
     $renaksi_exp=explode("|",$aRow['renaksi_kinerja']);
     $i = 1;
     foreach($renaksi_exp as $var) {
      $zz = $i;
      if($i<10){
        $zz = "0".$i; 
      }
      if($i<=12){
        $renaksi .= "B".$zz." : ".$var. "<br/>";
          $i++;
      }
     } 
     
     //Realisasi
     $realisasi = "";
     $realisasi_exp=explode("|",$aRow['realisasi']);
     $iz = 1;
     foreach($realisasi_exp as $var) {
      $zz = $iz;
      if($iz<10){
        $zz = "0".$iz; 
      }
      if($iz<=12){
        $realisasi .= "B".$zz." : ".$var. "<br/>";
         $iz++;
      }
     }

    $nama_prov = $aRow['id_prov'];
    $nama_upt  = $aRow['kode_upt'];

    if($nama_prov=="Only_Admin"){
      $nama_prov = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_prov)){
      $nama_prov = "<center>Semua Provinsi</center>";
    } 
    else {
      $nama_prov  = $aRow['nama_prov'];
    } 

    if($nama_upt=="Only_Admin"){
      $nama_upt = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_upt)){
      $nama_upt = "<center>Semua UPT</center>";
    } 
    else {
      $nama_upt  = $aRow['nama_upt'];
    }   

    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$nama_prov,$nama_upt,$aRow['kinerja'],$aRow['indikator_kinerja'],$aRow['target'],$renaksi,int_to_rp($aRow['pagu_anggaran']),$realisasi,$aRow['data_dukung'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_provinsi_filter"){
  $param = $_REQUEST['param'];
    $aColumns = array('mnv.created_date','mnv.kinerja','mnv.indikator_kinerja','mnv.target','mnv.renaksi_kinerja','mnv.pagu_anggaran','mnv.realisasi','mnv.data_dukung','mnv.keterangan','mnv.kode_monev_kinerja'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_monev->getMonevKinerjaFilter($sWhere,$sOrder,$sLimit,$param);
  $rResultFilterTotal   = $md_monev->getCountMonevKinerjaFilter($sWhere,$param);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

 while($aRow = $rResult->FetchRow()){
    $wilayah ="";
    if(!empty($aRow['id_prov'])){
     $wilayah = $gen_model->GetOne("id_map","ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    }
    $param_id = $aRow['kode_monev_kinerja'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

      //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));

    $edit="";
    $delete="";
   

    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['kode_upt']==$aRow['kode_upt']){
                if($_SESSION['group_user']=="grp_171026194411_1143"){
                     $edit = '&nbsp; <button title="Edit"  type="button"  onclick="do_edit(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';      
                }
                else if($aRow['created_by']==$_SESSION['kode_user']){
                    $edit = '&nbsp; <button title="Edit"  type="button"  onclick="do_edit(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
                 }
             }
          }
          else {
              $edit = '&nbsp; <button title="Edit"  type="button"  onclick="do_edit(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
          }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['kode_upt']==$aRow['kode_upt']){
                if($_SESSION['group_user']=="grp_171026194411_1143"){
                    $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete(\''.base64_encode('kode_monev_kinerja').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';       
                }
                else if($aRow['created_by']==$_SESSION['kode_user']){
                    $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete(\''.base64_encode('kode_monev_kinerja').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                 }
             }
          }
          else {
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete(\''.base64_encode('kode_monev_kinerja').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
          }
    }
    
     //Renaksi
     $renaksi   = "";
     $renaksi_exp=explode("|",$aRow['renaksi_kinerja']);
     $i = 1;
     foreach($renaksi_exp as $var) {
      $zz = $i;
      if($i<10){
        $zz = "0".$i; 
      }
      if($i<=12){
        $renaksi .= "B".$zz." : ".$var. "<br/>";
          $i++;
      }
     } 
     
     //Realisasi
     $realisasi = "";
     $realisasi_exp=explode("|",$aRow['realisasi']);
     $iz = 1;
     foreach($realisasi_exp as $var) {
      $zz = $iz;
      if($iz<10){
        $zz = "0".$iz; 
      }
      if($iz<=12){
        $realisasi .= "B".$zz." : ".$var. "<br/>";
         $iz++;
      }
     }

    $edit_delete = $detail.$edit.$delete;
    $nama_prov = $aRow['id_prov'];
    $nama_upt  = $aRow['kode_upt'];

    if($nama_prov=="Only_Admin"){
      $nama_prov = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_prov)){
      $nama_prov = "<center>Semua Provinsi</center>";
    } 
    else {
      $nama_prov  = $aRow['nama_prov'];
    } 

    if($nama_upt=="Only_Admin"){
      $nama_upt = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_upt)){
      $nama_upt = "<center>Semua UPT</center>";
    } 
    else {
      $nama_upt  = $aRow['nama_upt'];
    }   

    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$nama_prov,$nama_upt,$aRow['kinerja'],$aRow['indikator_kinerja'],$aRow['target'],$renaksi,int_to_rp($aRow['pagu_anggaran']),$realisasi,$aRow['data_dukung'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_provinsi"){
  $prov = $_REQUEST['id_prov'];
  $upt  = $_REQUEST['upt'];
  

     $aColumns = array('mnv.created_date','mnv.kinerja','mnv.indikator_kinerja','mnv.target','mnv.renaksi_kinerja','mnv.pagu_anggaran','mnv.realisasi','mnv.data_dukung','mnv.keterangan','mnv.kode_monev_kinerja'); //Kolom Pada Tabel


    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_monev->getMonevKinerja($sWhere,$sOrder,$sLimit,$prov,$upt);
  $rResultFilterTotal   = $md_monev->getCountMonevKinerja($sWhere,$prov,$upt);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $wilayah ="";
    if(!empty($aRow['id_prov'])){
     $wilayah = $gen_model->GetOne("id_map","ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    }
    $param_id = $aRow['kode_monev_kinerja'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

     //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));


    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['kode_upt']==$aRow['kode_upt']){
                if($_SESSION['group_user']=="grp_171026194411_1143"){
                     $edit = '&nbsp; <button title="Edit"  type="button"  onclick="do_edit(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';      
                }
                else if($aRow['created_by']==$_SESSION['kode_user']){
                    $edit = '&nbsp; <button title="Edit"  type="button"  onclick="do_edit(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
                 }
             }
          }
          else {
              $edit = '&nbsp; <button title="Edit"  type="button"  onclick="do_edit(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
          }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['kode_upt']==$aRow['kode_upt']){
                if($_SESSION['group_user']=="grp_171026194411_1143"){
                      $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete(\''.base64_encode('kode_monev_kinerja').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';      
                }
                else if($aRow['created_by']==$_SESSION['kode_user']){
                     $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete(\''.base64_encode('kode_monev_kinerja').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                 }
             }
          }
          else {
               $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete(\''.base64_encode('kode_monev_kinerja').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
          }
    }

     //Renaksi
     $renaksi   = "";
     $renaksi_exp=explode("|",$aRow['renaksi_kinerja']);
     $i = 1;
     foreach($renaksi_exp as $var) {
      $zz = $i;
      if($i<10){
        $zz = "0".$i; 
      }
      if($i<=12){
        $renaksi .= "B".$zz." : ".$var. "<br/>";
          $i++;
      }
     } 
     
     //Realisasi
     $realisasi = "";
     $realisasi_exp=explode("|",$aRow['realisasi']);
     $iz = 1;
     foreach($realisasi_exp as $var) {
      $zz = $iz;
      if($iz<10){
        $zz = "0".$iz; 
      }
      if($iz<=12){
        $realisasi .= "B".$zz." : ".$var. "<br/>";
         $iz++;
      }
     }

   $edit_delete = $detail.$edit.$delete;
    $nama_prov = $aRow['id_prov'];
    $nama_upt  = $aRow['kode_upt'];

    if($nama_prov=="Only_Admin"){
      $nama_prov = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_prov)){
      $nama_prov = "<center>Semua Provinsi</center>";
    } 
    else {
      $nama_prov  = $aRow['nama_prov'];
    } 

    if($nama_upt=="Only_Admin"){
      $nama_upt = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_upt)){
      $nama_upt = "<center>Semua UPT</center>";
    } 
    else {
      $nama_upt  = $aRow['nama_upt'];
    }   

    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$nama_prov,$nama_upt,$aRow['kinerja'],$aRow['indikator_kinerja'],$aRow['target'],$renaksi,int_to_rp($aRow['pagu_anggaran']),$realisasi,$aRow['data_dukung'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}

else {
  $gen_controller->response_code(http_response_code());
}
?>