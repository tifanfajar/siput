<?php 
//General Controller
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();

//Model Hak Akses
include "model/hak_akses.php";
$md_hak      = new hak_akses();

//Model User
include "model/user.php";
$md_user      = new user();


session_start();

$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}

$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //Check Session
  $gen_controller->check_session();
  //View
  $gen_controller->response_code('404');
}
else if($act=="edit" and $id_parameter!=""){
  //Check Session
  $gen_controller->check_session();
  //View
  include "view/header.php";
  include "view/akses_tab_edit.php";
  include "view/footer.php";
}
else if($act=="do_update"){
  //Check Session
  $gen_controller->check_session();
  $hak_akses    = $_POST['hak_akses_text'];
  $kode_fua_name = $gen_controller->decrypt($_POST['kode_function_access']);
  //Paramater
    $where_data = array();
    $where_data['kode_function_access']    = $kode_fua_name;
    $gen_model->Delete('tr_function_access_tab',$where_data);

    foreach($_POST as $key=>$val){
        if(preg_match("/read|edit|add|delete/",$key)){
          $$key = $val;
          foreach($$key as $key1 => $val1){

            $whr = array();
            $whr['kode_function_access'] = $kode_fua_name;
            $whr['id_tab'] = $val1;
            $men_nomorurutexist = $gen_model->GetOneRow("tr_function_access_tab",$whr); 
            if(!empty($men_nomorurutexist['id_tab'])){
              $sql="update tr_function_access_tab set function_access='".$hak_akses."',fua_$key='1' where id_tab='".$val1."' and kode_function_access='".$kode_fua_name."'";
              $db->Execute($sql);
            }else{
              $sql="insert into tr_function_access_tab (fua_$key,id_tab,function_access,kode_function_access) values ('1','".$val1."','".$hak_akses."','".$kode_fua_name."') ";
             
              $db->Execute($sql);
            }
              
          }
        }
    }
    echo "OK";
}
else if($act=="update" and $id_parameter!=""){
  error_reporting(null);
  //Check Session
  $gen_controller->check_session();
  $fua_name =  $gen_controller->decrypt($id_parameter);
  $id       = $_REQUEST['id'];
  //@ = isset
  if($id!=""){
    $sql="select * from ms_menu_tab where reference='".$id."' order by men_id,urutan asc";
  }
  else {
    $sql="select * from ms_menu_tab where tab_level='1' order by men_id,urutan asc";
  }
  
        $sql2=" select * from tr_function_access_tab where kode_function_access='".$fua_name."' ";
        $result2= $db->Execute($sql2);
        while($row2=$result2->FetchRow()){
          foreach($row2 as $key2 => $val){
            $key2=strtolower($key2);
            $$key2=$val;
          }
        }
    

  
        $result= $db->Execute($sql);
        $menu = "";
        while($row=$result->FetchRow()){
          { 
            $men_judul    = $row['tab'];
            $men_id       = $row['tab_id']; 

            $state = "";  
            $hitung = $gen_model->GetOne("count(*)","ms_menu_tab",array('reference'=>$men_id));
            $check_data = $hitung;

            
            if($check_data>0){
              $state = "closed";
            }
            else {
              $state = "open";
            }
            
            $read_check  ="";
            $add_check   ="";
            $edit_check  ="";
            $delete_check ="";

              $whr = array();
              $whr['kode_function_access'] = $fua_name;
              $whr['id_tab'] = $men_id;
              $row_check = $gen_model->GetOneRow("tr_function_access_tab",$whr); 

              $read_check   = ($row_check['fua_read']=="1" ? 'checked' : '');
              $add_check    = ($row_check['fua_add']=="1" ? 'checked' : '');
              $edit_check   = ($row_check['fua_edit']=="1" ? 'checked' : '');
              $delete_check = ($row_check['fua_delete']=="1" ? 'checked' : '');
              
              $data_indk = $gen_model->GetOne("menu","ms_menu",array('men_id'=>$row['men_id']));
              $induk = "<b>".$data_indk."</b> - ";
            
            $menu .= '{"id":"'.$men_id.'","title":"'.$induk.$men_judul.'","state":"'.$state.'","read":"<input id=\"read_'.$men_id.'\" '.$read_check.' type=\"checkbox\" name=\"read[]\" value=\"'.$men_id.'\"","add":"<input id=\"add_'.$men_id.'\" '.$add_check.' type=\"checkbox\" name=\"add[]\" value=\"'.$men_id.'\" ","edit":"<input id=\"edit_'.$men_id.'\"  '.$edit_check.' type=\"checkbox\" name=\"edit[]\" value=\"'.$men_id.'\" >","delete":"<input id=\"delete_'.$men_id.'\" '.$delete_check.' type=\"checkbox\" name=\"delete[]\" value=\"'.$men_id.'\"","checkrow":"<a style=color:#03C9A9;  href=javascript:; onClick=\"check_row(\''.$men_id.'\')\"><i class=\"fa fa-check\"><\/i><\/a>","uncheckrow":"<a style=color:#03C9A9; href=javascript:; onClick=\"uncheck_row(\''.$men_id.'\')\"><i class=\"fa fa-times\"><\/i><\/a>","show":"<input type=\"hidden\" name=show[] value=\"'.$men_id.'\""},';
          }
        }
        $data = trim($menu, ",");
    echo "[".$data."]";
  
}


else {
  $gen_controller->response_code(http_response_code());
}
?>