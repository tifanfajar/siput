<?php 
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();

//Model Pelayanan
include "model/pelayanan.php";
$md_pelayanan  = new pelayanan();


//Model User
include "model/user.php";
$md_user      = new user();
session_start();

//Check Acces Menu and Tab For Notification
$id_tab  = 1;
$id_menu = 2; 
$grp_menu_ntf = get_grp_notif($id_menu,$id_tab);
$group_ntf    = array();
if(!empty($grp_menu_ntf)){
  $variableAry=explode(",",$grp_menu_ntf); 
  foreach($variableAry as $grp) {
    if(!empty($grp)){
     array_push($group_ntf,$grp);
    }
  }
}

$akses_tab = $gen_model->GetOneRow("tr_function_access_tab",array('id_tab'=>$id_tab,'kode_function_access'=>$_SESSION['group_user'])); 
if(empty($akses_tab['fua_edit'])){
  $akses_tab['fua_edit'] = 0;
}
if(empty($akses_tab['fua_delete'])){
  $akses_tab['fua_delete'] = 0;
}


$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}



$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //Check Session
  $gen_controller->check_session();
  //View
  $gen_controller->response_code('404');
}
else if($act=="cetak_detail"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/pelayanan_loket.php";
}
else if($act=="cetak_all"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/pelayanan_loket_all.php";
}
else if($act=="table_detail_filter"){
   $param  = $_REQUEST['param'];
   include "view/ajax_table/pelayanan_table_data_pengunjung_filter.php";
}
else if($act=="table_all_filter"){
   $param  = $_REQUEST['param'];
   include "view/ajax_table/pelayanan_table_data_pengunjung_filter_all.php";
}
else if($act=="import"){
   //File
    $target = basename($_FILES['lampiran']['name']) ;
    move_uploaded_file($_FILES['lampiran']['tmp_name'], $target);
    $path_info = pathinfo($target);
    $ext =  strtolower($path_info['extension']); 
    
    if($ext!="xls"){
      echo "Format file salah";
      die();
    }

    require_once 'lib/excel_reader.php';
    $data = new Spreadsheet_Excel_Reader($_FILES['lampiran']['name'],false);
    $baris = $data->rowcount($sheet_index=0);
    $kode="";
    $hitung_files=0;
    for ($i=2; $i<=$baris; $i++) {
        $insert_data = array();
        $insert_data['kode_pelayanan']      = "pl_".date("ymdhis")."_".rand(1000,9999);
        $insert_data['kode_perusahaan']     = $data->val($i, 1);
        $insert_data['tgl_pelayanan']       = $data->val($i, 2);
        $insert_data['nama_pengunjung']     = $data->val($i, 3);
        $insert_data['jabatan_pengunjung']  = $data->val($i, 4);
        $insert_data['no_tlp']              = $data->val($i, 5);
        $insert_data['no_hp']               = $data->val($i, 6);
        $insert_data['email']               = $data->val($i, 7);
        $insert_data['tujuan']              = $data->val($i, 8);
        $insert_data['jenis_perizinan']     = $data->val($i, 9);
        //$insert_data['sub_group_perizinan'] = $data->val($i, 10);
        $insert_data['keterangan']          = $data->val($i, 10);
        $insert_data['id_prov']             = get($_POST['id_prov']);
        $insert_data['kode_upt']            = get($_POST['id_upt']);
        $insert_data['created_date']        = $date_now_indo_full;
        $insert_data['last_update']         = $date_now_indo_full;
        $insert_data['created_by']          = $_SESSION['kode_user'];
        $insert_data['last_update_by']      = $_SESSION['kode_user'];

        $insert_data['status']              = "3";

         if(!empty($insert_data['id_prov']) && !empty($insert_data['kode_upt'])){ //Provinsi && UPT
            $gen_model->Insert('tr_pelayanan',$insert_data);
            $kode .= $insert_data['kode_pelayanan'].",";
            $hitung_files++;
        }
    }

    $kode = rtrim($kode,",");
    //Notification
    if($hitung_files!=0){
         if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator UPT
                 notifikasi('4','1',$_POST['id_prov'],$_POST['id_upt'],'grp_171026194411_1142','Pelayanan','Ada '.$hitung_files.' data baru pada loket pelayanan yang sudah di setujui',$kode);
              }
              else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                notifikasi('4','1',$_POST['id_prov'],$_POST['id_upt'],'grp_171026194411_1142','Pelayanan','Ada '.$hitung_files.' data baru pada loket pelayanan yang sudah di setujui',$kode);
              }
              
    }
    echo "OK";
    
    unlink($_FILES['lampiran']['name']);
}
else if($act=="import_all"){
   //File
    $target = basename($_FILES['lampiran']['name']);
    move_uploaded_file($_FILES['lampiran']['tmp_name'], $target);
    $path_info = pathinfo($target);
    $ext =  strtolower($path_info['extension']); 
    
    if($ext!="xls"){
      echo "Format file salah";
      die();
    }

    require_once 'lib/excel_reader.php';
    $data = new Spreadsheet_Excel_Reader($_FILES['lampiran']['name'],false);
    $baris = $data->rowcount($sheet_index=0);
    
   
    for ($i=2; $i<=$baris; $i++) {
        $insert_data = array();
        $insert_data['kode_pelayanan']      = "pl_".date("ymdhis")."_".rand(1000,9999);
        $insert_data['kode_perusahaan']     = $data->val($i, 1);
        $insert_data['tgl_pelayanan']       = $data->val($i, 2);
        $insert_data['nama_pengunjung']     = $data->val($i, 3);
        $insert_data['jabatan_pengunjung']  = $data->val($i, 4);
        $insert_data['no_tlp']              = $data->val($i, 5);
        $insert_data['no_hp']               = $data->val($i, 6);
        $insert_data['email']               = $data->val($i, 7);
        $insert_data['tujuan']              = $data->val($i, 8);
        $insert_data['jenis_perizinan']     = $data->val($i, 9);
        //$insert_data['sub_group_perizinan'] = $data->val($i, 10);
        $insert_data['keterangan']          = $data->val($i, 10); 
        $insert_data['id_prov']             = $data->val($i, 11);
        $insert_data['kode_upt']            = $data->val($i, 12);
        $insert_data['created_date']        = $date_now_indo_full;
        $insert_data['last_update']         = $date_now_indo_full;
        $insert_data['created_by']          = $_SESSION['kode_user'];
        $insert_data['last_update_by']      = $_SESSION['kode_user'];

        $insert_data['status']              = "3";

        if( !empty($data->val($i, 11)) && !empty($data->val($i, 12)) ){ //Provinsi && UPT
            $gen_model->Insert('tr_pelayanan',$insert_data);

            //For Notification All
            $upt=$insert_data['kode_upt'];
            $upt_array[$upt][] =  array (
                                    'kode' => $insert_data['kode_pelayanan'],
                                    'prov' => $insert_data['id_prov'],
                                    'upt'  => $upt
                                  );
        }
    }

     $keys = array_keys($upt_array);
      for($i = 0; $i < count($upt_array); $i++) {
          $hitung_files = 0;
          $kode  = "";
          foreach($upt_array[$keys[$i]] as $key => $value) {
             $kode .= $value['kode'].",";
              $upt   = $value['upt'];
              $prov  = $value['prov'];
              $hitung_files++;
          }
          $kode = trim($kode,',');
          // echo "Id Prov : ".$prov."<br/>";
          // echo "UPT : ".$upt."<br/>";
          // echo "Kode : ".$kode."<br/>";
          // echo "Total : ".$hitung_files."<br/><br/>";

          //Notification
           if($hitung_files!=0){
                    if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator UPT
                      notifikasi('4','1',$prov,$upt,'grp_171026194411_1142','Pelayanan','Ada '.$hitung_files.' data baru pada loket pelayanan yang sudah di setujui',$kode);
                    }
                    else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                      notifikasi('4','1',$prov,$upt,'grp_171026194411_1142','Pelayanan','Ada '.$hitung_files.' data baru pada loket pelayanan yang sudah di setujui',$kode);
                    }
          }
      }
    echo "OK";
    
    unlink($_FILES['lampiran']['name']);
}
else if($act=="add"){
    if(!empty($_SESSION['kode_user'])){
      //Proses
      $insert_data = array();
      $insert_data['kode_pelayanan']      = "pl_".date("ymdhis")."_".rand(1000,9999);
      $insert_data['tgl_pelayanan']       = $gen_controller->date_indo_default($_POST['tgl_pelayanan']);
      $insert_data['nama_pengunjung']     = get($_POST['nama_pengunjung']);
      $insert_data['jenis_perizinan']     = get($_POST['perizinan']);
      //$insert_data['sub_group_perizinan'] = get($_POST['group_perizinan']);
      $insert_data['kode_perusahaan']     = get($_POST['kode_perusahaan']);
      $insert_data['kode_upt']            = get($_POST['loket_upt']);
      $insert_data['id_prov']             = get($_POST['id_prov']);
      $insert_data['jabatan_pengunjung']  = get($_POST['jabatan_pengunjung']);
      $insert_data['no_tlp']              = get($_POST['no_tlp']);
      $insert_data['no_hp']               = get($_POST['no_hp']);
      $insert_data['email']               = get($_POST['email']);
      $insert_data['tujuan']              = get($_POST['tujuan']);
      $insert_data['keterangan']          = get($_POST['keterangan']);
      $insert_data['created_date']        = $date_now_indo_full;
      $insert_data['last_update']         = $date_now_indo_full;
      $insert_data['created_by']          = $_SESSION['kode_user'];
      $insert_data['last_update_by']      = $_SESSION['kode_user'];

      $cmp = $gen_model->GetOne('company_name','ms_company_loket',array('company_id'=>$insert_data['kode_perusahaan']));
      $insert_data['company_name']        = $cmp;

     
      $insert_data['status']              = "3";

       //Validation
      if($insert_data['nama_pengunjung']!=""){
           if($gen_model->Insert('tr_pelayanan',$insert_data)=="OK"){
                   //Notification
                  if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator UPT
                     notifikasi('4','1',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1142','Pelayanan','Ada 1 data baru pada loket pelayanan yang sudah di setujui',$insert_data['kode_pelayanan']);
                  }
                  else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                    notifikasi('4','1',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1142','Pelayanan','Ada 1 data baru pada loket pelayanan yang sudah di setujui',$insert_data['kode_pelayanan']);
                  }
               echo "OK";
          }
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="edit" and $id_parameter!=""){
    $edit  = $gen_model->GetOneRow("tr_pelayanan",array('kode_pelayanan'=>$id_parameter)); 
    foreach($edit as $key=>$val){
                  $key=strtolower($key);
                  $$key=$val;
    }
    $cp  = $gen_model->GetOneRow("ms_company_loket",array('company_id'=>$kode_perusahaan)); 
    $data = array(
      'kode_pelayanan'=>$kode_pelayanan,
      'tgl_pelayanan'=>$gen_controller->get_date_indonesia($tgl_pelayanan),
      'kode_perusahaan'=>$kode_perusahaan,
      'nama_perusahaan'=>(!empty($cp['company_name']) ? $cp['company_name'] : '' ),
      'alamat_perusahaan'=>(!empty($cp['alamat']) ? $cp['alamat'] : '' ),
      'jabatan_pengunjung'=>$jabatan_pengunjung,
      'nama_pengunjung'=>$nama_pengunjung,
      'kode_upt'=>$kode_upt,
      'id_prov'=>$id_prov,
      'no_tlp'=>$no_tlp,
      'no_hp'=>$no_hp,
      'email'=>$email,
      'tujuan'=>$tujuan,
      'keterangan'=>$keterangan,
      'jenis_perizinan'=>$jenis_perizinan,
      'group_perizinan'=>$sub_group_perizinan
    );
    echo json_encode($data); 
}
else if($act=="update"){
    if(!empty($_SESSION['kode_user'])){
      //Proses
      $update_data = array();
      $update_data['tgl_pelayanan']       = $gen_controller->date_indo_default($_POST['tgl_pelayanan']);
      $update_data['nama_pengunjung']     = get($_POST['nama_pengunjung']);
      $update_data['jenis_perizinan']     = get($_POST['perizinan']);
      //$update_data['sub_group_perizinan'] = get($_POST['group_perizinan']);
      $update_data['kode_perusahaan']     = get($_POST['kode_perusahaan']);
      $update_data['kode_upt']            = get($_POST['loket_upt']);
      $update_data['id_prov']             = get($_POST['id_prov']);
      $update_data['jabatan_pengunjung']  = get($_POST['jabatan_pengunjung']);
      $update_data['no_tlp']              = get($_POST['no_tlp']);
      $update_data['no_hp']               = get($_POST['no_hp']);
      $update_data['email']               = get($_POST['email']);
      $update_data['tujuan']              = get($_POST['tujuan']);
      $update_data['keterangan']          = get($_POST['keterangan']);
      $update_data['last_update']         = $date_now_indo_full;
      $update_data['last_update_by']      = $_SESSION['kode_user'];

      

       //Paramater
      $where_data = array();
      $where_data['kode_pelayanan']       = $_POST['kode_pelayanan'];

       //Validation
      if(!empty($_POST['kode_pelayanan'])){
           if($gen_model->Update('tr_pelayanan',$update_data,$where_data)=="OK"){
                   //Notification
                  if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                     //notifikasi('2','1',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1143','Pelayanan','Ada 1 data baru pada loket pelayanan yang harus di tinjau',$_POST['kode_pelayanan']);
                  }
                  echo "OK";
          }

      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="delete"){
    if(!empty($_SESSION['kode_user'])){

       $update_data['status']              = "2"; //Delete

       //Paramater
      $where_data = array();
      $where_data['kode_pelayanan']       = $_POST['kode_pelayanan'];


       //Validation
      if(!empty($_POST['kode_pelayanan'])){
            echo $gen_model->Update('tr_pelayanan',$update_data,$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="verifikasi"){
    if(!empty($_SESSION['kode_user'])){
       $db->Execute("update tr_pelayanan set status='3' where kode_pelayanan in(".$_REQUEST['kode'].") ");
        $total = $_REQUEST['total'];
        if(!empty($total)){
        //Notification
          if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
            $kode=str_replace("'","",$_REQUEST['kode']);
            notifikasi('4','1',$_SESSION['upt_provinsi'],$_SESSION['kode_upt'],'grp_171026194411_1142','Pelayanan','Ada '.$total.' data baru pada loket pelayanan yang sudah di setujui',$kode);
          }
        }

       echo "OK";
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="reject"){
    if(!empty($_SESSION['kode_user'])){
       $db->Execute("update tr_pelayanan set status='1',ket_sts_apr_reject='".$_REQUEST['ket']."' where kode_pelayanan in(".$_REQUEST['kode'].") ");

       $total = $_REQUEST['total'];
        if(!empty($total)){
        //Notification
          if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
            $kode=str_replace("'","",$_REQUEST['kode']);
              foreach ($group_ntf as $id_group) {
                      notifikasi('3','1',$_SESSION['upt_provinsi'],$_SESSION['kode_upt'],$id_group,'Pelayanan','Ada '.$total.' data baru pada loket pelayanan yang di reject',$kode);
               }
          }
        }

       echo "OK";
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_waiting"){
  $prov = $_REQUEST['id_prov'];
  $upt = $_REQUEST['upt'];
  $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$prov)); 
  $aColumns = array('pl.kode_pelayanan','pl.tgl_pelayanan','pl.status','pl.nama_pengunjung','cp.company_name','pl.jabatan_pengunjung','pl.no_tlp','pl.no_hp','pl.email','pl.tujuan','pl.keterangan','pl.kode_pelayanan'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getLoketPelayananWaiting($sWhere,$sOrder,$sLimit,$prov,$upt);
  $rResultFilterTotal   = $md_pelayanan->getCountLoketPelayananWaiting($sWhere,$prov,$upt);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_pelayanan'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_pl(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
    
    $checkbox="";
    if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
         if($aRow['status']!="4"){
             $checkbox = '<input type="checkbox" value="'.$param_id.'" name="kode_pelayanan">';
          }
    }
   
    $sts = "";
    if($aRow['status']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }

    //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));
    
    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
      //Operator & Kepala UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_pl(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else {
                if($aRow['created_by']==$_SESSION['kode_user']){
                  $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_pl(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
               }  
            }
         
        }
        else {
           $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_pl(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
      //Operator & Kepala UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_pl(\''.base64_encode('kode_pelayanan').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else {
                if($aRow['created_by']==$_SESSION['kode_user']){
                  $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_pl(\''.base64_encode('kode_pelayanan').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
               }  
            }
         
        }
        else {
           $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_pl(\''.base64_encode('kode_pelayanan').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($checkbox,$gen_controller->get_date_indonesia($aRow['tgl_pelayanan']),$sts,$aRow['nama_pengunjung'],$aRow['jabatan_pengunjung'],$aRow['company_name'],$aRow['no_tlp'],$aRow['no_hp'],$aRow['email'],$aRow['tujuan'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest"){
  $aColumns = array('pl.tgl_pelayanan','pv.nama_prov','upt.nama_upt','pl.nama_pengunjung','cp.company_name','pl.jabatan_pengunjung','pl.no_tlp','pl.no_hp','pl.email','tj.tujuan','pl.jenis_perizinan','pl.keterangan','pl.kode_pelayanan'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getLoketPelayanan($sWhere,$sOrder,$sLimit);
  $rResultFilterTotal   = $md_pelayanan->getCountLoketPelayanan($sWhere);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    $param_id = $aRow['kode_pelayanan'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_pl_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
    


    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
       //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_pl_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_pl_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
        }
        else {
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_pl_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
        //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
              $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_pl_all(\''.base64_encode('kode_pelayanan').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }  
            else if($aRow['created_by']==$_SESSION['kode_user']){
               $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_pl_all(\''.base64_encode('kode_pelayanan').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
        }
        else {
           $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_pl_all(\''.base64_encode('kode_pelayanan').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['tgl_pelayanan']),$aRow['nama_prov'],$aRow['nama_upt'],$aRow['nama_pengunjung'],$aRow['jabatan_pengunjung'],$aRow['company_name'],$aRow['no_tlp'],$aRow['no_hp'],$aRow['email'],$aRow['nama_tujuan'],$aRow['jenis'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_filter"){
  $param = $_REQUEST['param'];
  $aColumns = array('pl.tgl_pelayanan','pv.nama_prov','pl.nama_pengunjung','cp.company_name','pl.jabatan_pengunjung','pl.no_tlp','pl.no_hp','pl.email','pl.tujuan','pl.jenis_perizinan','pl.keterangan','pl.kode_pelayanan'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getLoketPelayananFilter($sWhere,$sOrder,$sLimit,$param);
  $rResultFilterTotal   = $md_pelayanan->getCountLoketPelayananFilter($sWhere,$param);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    $param_id = $aRow['kode_pelayanan'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_pl(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
    
  

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
        //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
               $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_pl(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_pl(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
        }
        else {
           $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_pl(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_pl(\''.base64_encode('kode_pelayanan').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_pl(\''.base64_encode('kode_pelayanan').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
        }
        else {
           $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_pl(\''.base64_encode('kode_pelayanan').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
        
    }

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['tgl_pelayanan']),$aRow['nama_prov'],$aRow['nama_pengunjung'],$aRow['jabatan_pengunjung'],$aRow['company_name'],$aRow['no_tlp'],$aRow['no_hp'],$aRow['email'],$aRow['nama_tj'],$aRow['jenis'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_provinsi_filter"){
  $param = $_REQUEST['param'];
  $aColumns = array('pl.tgl_pelayanan','pl.nama_pengunjung','cp.company_name','pl.jabatan_pengunjung','pl.no_tlp','pl.no_hp','pl.email','pl.tujuan','pl.jenis_perizinan','pl.keterangan','pl.kode_pelayanan'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getLoketPelayananFilter($sWhere,$sOrder,$sLimit,$param);
  $rResultFilterTotal   = $md_pelayanan->getCountLoketPelayananFilter($sWhere,$param);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    $param_id = $aRow['kode_pelayanan'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_pl(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
  
     //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
        if($_SESSION['group_user']=="grp_171026194411_1142" or $_SESSION['group_user']=="grp_171026194411_1143"){ 
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_pl(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
        else {
            if($aRow['created_by']==$_SESSION['kode_user']){
               $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_pl(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']!=$_SESSION['kode_user'] and $grp_usr=="grp_171026194411_1142"){
               $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_pl(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
        }
        
    }
    if($akses_tab['fua_delete']=="1"){ 
        if($_SESSION['group_user']=="grp_171026194411_1142" or $_SESSION['group_user']=="grp_171026194411_1143"){ 
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_pl(\''.base64_encode('kode_pelayanan').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
        else {
            if($aRow['created_by']==$_SESSION['kode_user']){
              $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_pl(\''.base64_encode('kode_pelayanan').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
        }
         
    }

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['tgl_pelayanan']),$aRow['nama_pengunjung'],$aRow['jabatan_pengunjung'],$aRow['company_name'],$aRow['no_tlp'],$aRow['no_hp'],$aRow['email'],$aRow['nama_tj'],$aRow['jenis'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_provinsi"){
  $prov = $_REQUEST['id_prov'];
  $upt  = $_REQUEST['upt'];
  $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$prov));

 
  $aColumns = array('pl.tgl_pelayanan','pl.nama_pengunjung','cp.company_name','pl.jabatan_pengunjung','pl.no_tlp','pl.no_hp','pl.email','pl.tujuan','pl.jenis_perizinan','pl.keterangan','pl.kode_pelayanan'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getLoketPelayanan($sWhere,$sOrder,$sLimit,$prov,$upt);
  $rResultFilterTotal   = $md_pelayanan->getCountLoketPelayanan($sWhere,$prov,$upt);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_pelayanan'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_pl(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

    //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));

    $edit="";
    $delete="";

    if($akses_tab['fua_edit']=="1"){ 
       //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
              $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_pl(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
               $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_pl(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
        }
        else {
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_pl(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
        //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
              $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_pl(\''.base64_encode('kode_pelayanan').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
               $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_pl(\''.base64_encode('kode_pelayanan').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
        }
        else {
           $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_pl(\''.base64_encode('kode_pelayanan').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }



    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['tgl_pelayanan']),$aRow['nama_pengunjung'],$aRow['jabatan_pengunjung'],$aRow['company_name'],$aRow['no_tlp'],$aRow['no_hp'],$aRow['email'],$aRow['nama_tujuan'],$aRow['jenis'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="akumulasi"){
  $aColumns = array('pv.nama_prov','upt.nama_upt'); //Kolom Pada Tabel

    $data_tj = $db->SelectLimit("select * from ms_tujuan"); 
    while($list = $data_tj->FetchRow()){
        foreach($list as $key=>$val){
              $key=strtolower($key);
              $$key=$val;
            }  
               array_push($aColumns,$kode_tujuan);
         }
  array_push($aColumns,'total'); 
    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getAkumulasiPelayananAll($sWhere,$sOrder,$sLimit);
  $rResultFilterTotal   = $md_pelayanan->getCountAkumulasiPelayananAll($sWhere);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );
  while($aRow = $rResult->FetchRow()){

  //    $list_jns="";
  //   $data_jns = $db->SelectLimit("select DISTINCT(grp.jenis) from ms_group_layanan grp 
  //                               inner join tr_pelayanan as pl on pl.jenis_perizinan=grp.id_jenis where pl.id_prov='".$aRow['id_prov']."' "); 
  // while($list = $data_jns->FetchRow()){
  //       foreach($list as $key=>$val){
  //                 $key=strtolower($key);
  //                 $$key=$val;
  //               }  
  //        $list_jns .= $jenis.", ";
  //     }  

    $row = array();
    //$row = array($aRow['nama_prov'],trim($list_jns,', '));
    $row = array($aRow['nama_prov'],$aRow['nama_upt']);

    $data_tj = $db->SelectLimit("select * from ms_tujuan"); 
    while($list = $data_tj->FetchRow()){
        foreach($list as $key=>$val){
              $key=strtolower($key);
              $$key=$val;
            }  
               array_push($row,"<center>".$aRow[$kode_tujuan]."</center>");
         }
    array_push($row,"<center>".$aRow['total']."</center>");      
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="akumulasi_detail"){
  $prov = $_REQUEST['id_prov'];
  $upt  = $_REQUEST['upt'];
  $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$prov)); 
  $aColumns = array(); //Kolom Pada Tabel

   $data_tj = $db->SelectLimit("select * from ms_tujuan"); 
    while($list = $data_tj->FetchRow()){
        foreach($list as $key=>$val){
              $key=strtolower($key);
              $$key=$val;
            }  
               array_push($aColumns,$kode_tujuan);
         }
   array_push($aColumns,'total'); 
    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getAkumulasiPelayanan($sWhere,$sOrder,$sLimit,$prov,$upt);
  $rResultFilterTotal   = $md_pelayanan->getCountAkumulasiPelayanan($sWhere,$prov,$upt);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );
      

  while($aRow = $rResult->FetchRow()){
    $row = array();
    

    $data_tj = $db->SelectLimit("select * from ms_tujuan"); 
    while($list = $data_tj->FetchRow()){
        foreach($list as $key=>$val){
              $key=strtolower($key);
              $$key=$val;
            }  
               array_push($row,"<center>".$aRow[$kode_tujuan]."</center>");
         }
     array_push($row,"<center>".$aRow['total']."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else {
  $gen_controller->response_code(http_response_code());
}
?>