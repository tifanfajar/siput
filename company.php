<?php 
//General Controller
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();

//Model Pelayanan
include "model/company.php";
$md_company  = new company();


//Model User
include "model/user.php";
$md_user      = new user();
session_start();

//Check Access Tab
$akses_tab = $gen_model->GetOneRow("tr_function_access_tab",array('id_tab'=>16,'kode_function_access'=>$_SESSION['group_user'])); 
if(empty($akses_tab['fua_edit'])){
  $akses_tab['fua_edit'] = 0;
}
if(empty($akses_tab['fua_delete'])){
  $akses_tab['fua_delete'] = 0;
}

$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}

$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //Check Session
  $gen_controller->check_session();

  //View
  include "view/header.php";
  include "view/company.php";
  include "view/footer.php";
}
else if($act=="cetak"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/company.php";
}
else if($act=="form"){
   include "view/default_style.php"; 
   $form_url  = $_REQUEST['form_url'];
   $activity  = $_REQUEST['activity'];
   if($form_url=="Form_Perusahaan"){
      include "view/ajax_form/company_form_data.php";
   }
}
else if($act=="table_filter"){
   $param  = $_REQUEST['param'];
   include "view/ajax_table/company_filter.php";
}
else if($act=="add_loket"){

    if(!empty($_SESSION['kode_user'])){
      //Proses 
      $insert_data = array();
      $insert_data['company_id']          = "cmp_".date("ymdhis")."_".rand(1000,9999);
      $insert_data['company_name']        = get($_POST['nama_perusahaan']);
      $insert_data['no_tlp_company']      = get($_POST['no_tlp']);
      $insert_data['no_hp_company']       = get($_POST['no_hp']);
      $insert_data['id_provinsi']         = get($_POST['id_prov']);
      $insert_data['id_kabkot']           = get($_POST['kabkot']);
      $insert_data['alamat']              = get($_POST['alamat']);
      $insert_data['created_date']        = $date_now_indo_full;
      $insert_data['last_update']         = $date_now_indo_full;
      $insert_data['created_by']          = $_SESSION['kode_user'];
      $insert_data['last_update_by']      = $_SESSION['kode_user'];


       //Validation
      if($insert_data['company_id']!=""){
          echo $gen_model->Insert('ms_company_loket',$insert_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="update_loket"){

    if(!empty($_SESSION['kode_user'])){

      $cmp_id_temp = get($_POST['company_id_temp']);

      //Proses 
      $update_data = array();
      $update_data['company_name']        = get($_POST['nama_perusahaan']);
      $update_data['no_tlp_company']      = get($_POST['no_tlp']);
      $update_data['no_hp_company']       = get($_POST['no_hp']);
      $update_data['id_provinsi']         = get($_POST['id_prov']);
      $update_data['id_kabkot']           = get($_POST['kabkot']);
      $update_data['alamat']              = get($_POST['alamat']);
      $update_data['last_update']         = $date_now_indo_full;
      $update_data['last_update_by']      = $_SESSION['kode_user'];

      $where_data = array();
      $where_data['company_id']           = $cmp_id_temp;

       //Validation
      if($update_data['company_name']!=""){
            echo $gen_model->Update('ms_company_loket',$update_data,$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="edit_loket" and $id_parameter!=""){
 $edit  = $gen_model->GetOneRow("ms_company_loket",array('company_id'=>$id_parameter)); 
    foreach($edit as $key=>$val){
                  $key=strtolower($key);
                  $$key=$val;
    }
    $data = array(
      'company_id'=>$company_id,
      'company_name'=>$company_name,
      'id_provinsi'=>$id_provinsi,
      'id_kabkot'=>$id_kabkot,
      'no_tlp'=>$no_tlp_company,
      'no_hp'=>$no_hp_company,
      'alamat'=>$alamat
    );
    echo json_encode($data); 
}
else if($act=="add"){

    if(!empty($_SESSION['kode_user'])){
      //Proses 
      $insert_data = array();
      $insert_data['company_id']          = get($_POST['company_id']);
      $insert_data['company_name']        = get($_POST['nama_perusahaan']);
      $insert_data['no_tlp_company']      = get($_POST['no_tlp']);
      $insert_data['no_hp_company']       = get($_POST['no_hp']);
      $insert_data['id_provinsi']         = get($_POST['id_prov']);
      $insert_data['id_kabkot']           = get($_POST['kabkot']);
      $insert_data['alamat']              = get($_POST['alamat']);
      $insert_data['created_date']        = $date_now_indo_full;
      $insert_data['last_update']         = $date_now_indo_full;
      $insert_data['created_by']          = $_SESSION['kode_user'];
      $insert_data['last_update_by']      = $_SESSION['kode_user'];

     

       //Validation
      if($insert_data['company_id']!=""){
            $count_company = $db->GetOne("select count(*) as total from ms_company where company_id='".$_POST['company_id']."'"); 
            if($count_company>0){
              echo 'Kode perusahaan telah terdaftar di database';
            }
            else {
              echo $gen_model->Insert('ms_company',$insert_data);
            }
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="update"){

    if(!empty($_SESSION['kode_user'])){

      $cmp_id_temp = get($_POST['company_id_temp']);

      //Proses 
      $update_data = array();
      $update_data['company_id']          = get($_POST['company_id']);
      $update_data['company_name']        = get($_POST['nama_perusahaan']);
      $update_data['no_tlp_company']      = get($_POST['no_tlp']);
      $update_data['no_hp_company']       = get($_POST['no_hp']);
      $update_data['id_provinsi']         = get($_POST['id_prov']);
      $update_data['id_kabkot']           = get($_POST['kabkot']);
      $update_data['alamat']              = get($_POST['alamat']);
      $update_data['last_update']         = $date_now_indo_full;
      $update_data['last_update_by']      = $_SESSION['kode_user'];

      $where_data = array();
      $where_data['company_id']           = $cmp_id_temp;

       //Validation
      if($update_data['company_id']!=""){
            if($cmp_id_temp==$_POST['company_id']){
              echo $gen_model->Update('ms_company',$update_data,$where_data);
            }
            else {
               $count_company = $db->GetOne("select count(*) as total from ms_company where company_id='".$_POST['company_id']."'");
               if($count_company>0){
                  echo 'Kode perusahaan telah terdaftar di database';
                }
                else {
                  echo $gen_model->Update('ms_company',$update_data,$where_data);
                } 
            }
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="update"){

    if(!empty($_SESSION['kode_user'])){

      $cmp_id_temp = get($_POST['company_id_temp']);

      //Proses 
      $update_data = array();
      $update_data['company_id']          = get($_POST['company_id']);
      $update_data['company_name']        = get($_POST['nama_perusahaan']);
      $update_data['no_tlp_company']      = get($_POST['no_tlp']);
      $update_data['no_hp_company']       = get($_POST['no_hp']);
      $update_data['id_provinsi']         = get($_POST['id_prov']);
      $update_data['id_kabkot']           = get($_POST['kabkot']);
      $update_data['alamat']              = get($_POST['alamat']);
      $update_data['last_update']         = $date_now_indo_full;
      $update_data['last_update_by']      = $_SESSION['kode_user'];

      $where_data = array();
      $where_data['company_id']           = $cmp_id_temp;

       //Validation
      if($update_data['company_id']!=""){
            if($cmp_id_temp==$_POST['company_id']){
              echo $gen_model->Update('ms_company',$update_data,$where_data);
            }
            else {
               $count_company = $db->GetOne("select count(*) as total from ms_company where company_id='".$_POST['company_id']."'");
               if($count_company>0){
                  echo 'Kode perusahaan telah terdaftar di database';
                }
                else {
                  echo $gen_model->Update('ms_company',$update_data,$where_data);
                } 
            }
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="edit" and $id_parameter!=""){
 $edit  = $gen_model->GetOneRow("ms_company",array('company_id'=>$id_parameter)); 
    foreach($edit as $key=>$val){
                  $key=strtolower($key);
                  $$key=$val;
    }
    $data = array(
      'company_id'=>$company_id,
      'company_name'=>$company_name,
      'id_provinsi'=>$id_provinsi,
      'id_kabkot'=>$id_kabkot,
      'no_tlp'=>$no_tlp_company,
      'no_hp'=>$no_hp_company,
      'alamat'=>$alamat
    );
    echo json_encode($data); 
}
else if($act=="edit_cp_loket" and $id_parameter!=""){
 $edit  = $gen_model->GetOneRow("ms_company_loket",array('company_id'=>$id_parameter)); 
    foreach($edit as $key=>$val){
                  $key=strtolower($key);
                  $$key=$val;
    }
    $data = array(
      'company_id'=>$company_id,
      'company_name'=>$company_name,
      'id_provinsi'=>$id_provinsi,
      'id_kabkot'=>$id_kabkot,
      'no_tlp'=>$no_tlp_company,
      'no_hp'=>$no_hp_company,
      'alamat'=>$alamat
    );
    echo json_encode($data); 
}
else if($act=="delete"){
    if(!empty($_SESSION['kode_user'])){
      $update_data = array();
      $update_data['status_delete']  = "1";

       //Paramater
      $where_data = array();
      $where_data['company_id']       = $_POST['company_id'];

      //Validation
      if(!empty($_POST['company_id'])){
           echo $gen_model->Update('ms_company',$update_data,$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="delete_cp_loket"){
    if(!empty($_SESSION['kode_user'])){
      $update_data = array();
      $update_data['status_delete']  = "1";

       //Paramater
      $where_data = array();
      $where_data['company_id']       = $_POST['company_id'];

      //Validation
      if(!empty($_POST['company_id'])){
           echo $gen_model->Update('ms_company_loket',$update_data,$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_loket"){
  $aColumns = array('cp.company_id','cp.company_name','cp.no_tlp_company','cp.no_hp_company','pv.nama_prov','kb.nama_kab_kot','cp.alamat','cp.company_id'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_company->getCompany_Loket($sWhere,$sOrder,$sLimit);
  $rResultFilterTotal   = $md_company->getCountCompany_Loket($sWhere);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $param_id = $aRow['company_id'];
    
    $detail = '<button title="Detail"  type="button" onclick="company_loket_detail(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
 
    $edit="";
    $delete="";
    //if($akses_tab['fua_edit']=="1"){ 
       $edit = '&nbsp; <button title="Edit"  type="button" onclick="company_loket_edit(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
   // }
   // if($akses_tab['fua_delete']=="1"){ 
       $delete = '&nbsp; <button title="Hapus" type="button" onclick="company_loket_delete(\''.base64_encode('company_id').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
    // }
       
    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($aRow['company_id'],$aRow['company_name'],$aRow['no_tlp_company'],$aRow['no_hp_company'],$aRow['nama_prov'],$aRow['nama_kab_kot'],$aRow['alamat'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest"){
  $aColumns = array('cp.company_id','cp.company_name','cp.no_tlp_company','cp.no_hp_company','pv.nama_prov','kb.nama_kab_kot','cp.alamat','cp.company_id'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_company->getCompany($sWhere,$sOrder,$sLimit);
  $rResultFilterTotal   = $md_company->getCountCompany($sWhere);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $param_id = $aRow['company_id'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_company(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
 
    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
       if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
          if($_SESSION['kode_user']==$aRow['created_by']){
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_company(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
          }
       }
       else if($_SESSION['group_user']=="grp_171026194411_1143"){
          $upt = $gen_model->GetOne('kode_upt','ms_user',array('kode_user'=>$aRow['created_by']));
          if($_SESSION['kode_upt']==$upt){
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_company(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
          }
       }
       else {
        $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_company(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
       }
    }
    if($akses_tab['fua_delete']=="1"){ 

      if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
         if($_SESSION['kode_user']==$aRow['created_by']){
             $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_company(\''.base64_encode('company_id').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
          }
       }
       else if($_SESSION['group_user']=="grp_171026194411_1143"){
          $upt = $gen_model->GetOne('kode_upt','ms_user',array('kode_user'=>$aRow['created_by']));
          if($_SESSION['kode_upt']==$upt){
           $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_company(\''.base64_encode('company_id').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
          }
       }
       else {
        $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_company(\''.base64_encode('company_id').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
       }
     }
       
    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($aRow['company_id'],$aRow['company_name'],$aRow['no_tlp_company'],$aRow['no_hp_company'],$aRow['nama_prov'],$aRow['nama_kab_kot'],$aRow['alamat'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_filter"){
   $param  = $_REQUEST['param'];
  $aColumns = array('cp.company_id','cp.company_name','cp.no_tlp_company','cp.no_hp_company','pv.nama_prov','kb.nama_kab_kot','cp.alamat','cp.company_id'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_company->getCompanyFilter($sWhere,$sOrder,$sLimit,$param);
  $rResultFilterTotal   = $md_company->getCountCompanyFilter($sWhere,$param);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $param_id = $aRow['company_id'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_company(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
 
    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
       if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
          if($_SESSION['kode_user']==$aRow['created_by']){
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_company(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
          }
       }
       else if($_SESSION['group_user']=="grp_171026194411_1143"){
          $upt = $gen_model->GetOne('kode_upt','ms_user',array('kode_user'=>$aRow['created_by']));
          if($_SESSION['kode_upt']==$upt){
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_company(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
          }
       }
       else {
        $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_company(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
       }
    }
    if($akses_tab['fua_delete']=="1"){ 

      if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
         if($_SESSION['kode_user']==$aRow['created_by']){
             $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_company(\''.base64_encode('company_id').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
          }
       }
       else if($_SESSION['group_user']=="grp_171026194411_1143"){
          $upt = $gen_model->GetOne('kode_upt','ms_user',array('kode_user'=>$aRow['created_by']));
          if($_SESSION['kode_upt']==$upt){
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_company(\''.base64_encode('company_id').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
          }
       }
       else {
        $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_company(\''.base64_encode('company_id').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
       }
     }
       
    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($aRow['company_id'],$aRow['company_name'],$aRow['no_tlp_company'],$aRow['no_hp_company'],$aRow['nama_prov'],$aRow['nama_kab_kot'],$aRow['alamat'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else {
  $gen_controller->response_code(http_response_code());
}
?>