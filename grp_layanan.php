<?php 
//General Controller
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();

//Model Pelayanan
include "model/grp_layanan.php";
$md_grp_layanan  = new grp_layanan();


//Model User
include "model/user.php";
$md_user      = new user();
session_start();

//Check Access Tab
$akses_tab = $gen_model->GetOneRow("tr_function_access_tab",array('id_tab'=>19,'kode_function_access'=>$_SESSION['group_user']));
if(empty($akses_tab['fua_edit'])){
  $akses_tab['fua_edit'] = 0;
}
if(empty($akses_tab['fua_delete'])){
  $akses_tab['fua_delete'] = 0;
}

$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}

$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //Check Session
  $gen_controller->check_session();

  //View
  include "view/header.php";
  include "view/grp_layanan.php";
  include "view/footer.php";
}
else if($act=="form"){
   include "view/default_style.php"; 
   $form_url  = $_REQUEST['form_url'];
   $activity  = $_REQUEST['activity'];
   if($form_url=="Form_Perizinan"){
      include "view/ajax_form/grp_form_data.php";
   }
   else if($form_url=="Form_Sub_Perizinan"){
      include "view/ajax_form/grp_sub_form_data.php";
   }
}
else if($act=="add"){
    if(!empty($_SESSION['kode_user'])){
      //Proses
      $insert_data = array();
      $insert_data['id_jenis']            = "gp_".date("ymdhis")."_".rand(1000,9999);
      $insert_data['jenis']               = get($_POST['jenis']);
      $insert_data['created_date']        = $date_now_indo_full;
      $insert_data['last_update']         = $date_now_indo_full;
      $insert_data['created_by']          = $_SESSION['kode_user'];
      $insert_data['last_update_by']      = $_SESSION['kode_user'];
 
       //Validation
      if($insert_data['jenis']!=""){
           echo $gen_model->Insert('ms_group_layanan',$insert_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="delete"){
    if(!empty($_SESSION['kode_user'])){

       $update_data['status']              = "0"; //Delete

       //Paramater
      $where_data = array();
      $where_data['id_jenis']       = $_POST['id_jenis'];


       //Validation
      if(!empty($_POST['id_jenis'])){
            echo $gen_model->Update('ms_group_layanan',$update_data,$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="edit" and $id_parameter!=""){
    $edit  = $gen_model->GetOneRow("ms_group_layanan",array('id_jenis'=>$id_parameter)); 
    foreach($edit as $key=>$val){
                  $key=strtolower($key);
                  $$key=$val;
    }
    $data = array(
      'id_jenis'=>$id_jenis,
      'jenis'=>$jenis
    );
    echo json_encode($data); 
}
else if($act=="update"){
    if(!empty($_SESSION['kode_user'])){
      //Proses
      $update_data = array();
      $update_data['jenis']               = get($_POST['jenis']);
      $update_data['last_update']         = $date_now_indo_full;
      $update_data['last_update_by']      = $_SESSION['kode_user'];

       //Paramater
      $where_data = array();
      $where_data['id_jenis']       = $_POST['id_jenis'];


       //Validation
      if(!empty($_POST['id_jenis'])){
           echo $gen_model->Update('ms_group_layanan',$update_data,$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="add_sb"){
    if(!empty($_SESSION['kode_user'])){
      //Proses
      $insert_data = array();
      $insert_data['id_sub_group_layanan']    = "sb_".date("ymdhis")."_".rand(1000,9999);
      $insert_data['sub_group_layanan']       = get($_POST['jenis_sb']);
      $insert_data['id_jenis']                = get($_POST['jenis']);
      $insert_data['created_date']            = $date_now_indo_full;
      $insert_data['last_update']             = $date_now_indo_full;
      $insert_data['created_by']              = $_SESSION['kode_user'];
      $insert_data['last_update_by']          = $_SESSION['kode_user'];
 
       //Validation
      if($insert_data['sub_group_layanan']!=""){
           echo $gen_model->Insert('ms_sub_group_layanan',$insert_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="delete_sb"){
    if(!empty($_SESSION['kode_user'])){

       $update_data['status']              = "0"; //Delete

       //Paramater
      $where_data = array();
      $where_data['id_sub_group_layanan']       = $_POST['id_sub_group_layanan'];


       //Validation
      if(!empty($_POST['id_sub_group_layanan'])){
            echo $gen_model->Update('ms_sub_group_layanan',$update_data,$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="edit_sb" and $id_parameter!=""){
    $edit  = $gen_model->GetOneRow("ms_sub_group_layanan",array('id_sub_group_layanan'=>$id_parameter)); 
    foreach($edit as $key=>$val){
                  $key=strtolower($key);
                  $$key=$val;
    }
    $data = array(
      'id_jenis'=>$id_jenis,
      'id_sub_group_layanan'=>$id_sub_group_layanan,
      'sub_group_layanan'=>$sub_group_layanan
    );
    echo json_encode($data); 
}
else if($act=="update_sb"){
    if(!empty($_SESSION['kode_user'])){
      //Proses
      $update_data = array();
      $update_data['id_jenis']               = get($_POST['jenis']);
      $update_data['sub_group_layanan']      = get($_POST['jenis_sb']);
      $update_data['last_update']            = $date_now_indo_full;
      $update_data['last_update_by']         = $_SESSION['kode_user'];

       //Paramater
      $where_data = array();
      $where_data['id_sub_group_layanan']       = $_POST['id_jenis_sb'];


       //Validation
      if(!empty($_POST['id_jenis_sb'])){
           echo $gen_model->Update('ms_sub_group_layanan',$update_data,$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="rest"){
  $aColumns = array('grp.id_jenis','grp.jenis','sb.id_sub_group_layanan','sb.sub_group_layanan','sb.id_sub_group_layanan'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_grp_layanan->getGroupLayaan($sWhere,$sOrder,$sLimit);
  $rResultFilterTotal   = $md_grp_layanan->getCountGroupLayaan($sWhere);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['id_sub_group_layanan'];
    $edit="";
    $delete="";

    if($akses_tab['fua_edit']=="1"){ 
       //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($aRow['created_by']==$_SESSION['kode_user']){
               $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_grp(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
        }
        else {
           $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_grp(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
        //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($aRow['created_by']==$_SESSION['kode_user']){
               $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_grp(\''.base64_encode('id_jenis').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
        }
        else {
           $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_grp(\''.base64_encode('id_jenis').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }
    $edit_delete = $edit.$delete;

    $row = array();
    $row = array($aRow['id_jenis'],$aRow['jenis'],$aRow['id_sub_group_layanan'],$aRow['sub_group_layanan'],$edit_delete);
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_grp"){
  $aColumns = array('grp.id_jenis','grp.jenis','grp.id_jenis'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_grp_layanan->getGroupLayaan2($sWhere,$sOrder,$sLimit);
  $rResultFilterTotal   = $md_grp_layanan->getCountGroupLayaan2($sWhere);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $param_id = $aRow['id_jenis'];
    $edit="";
    $delete="";

    if($akses_tab['fua_edit']=="1"){ 
       //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($aRow['created_by']==$_SESSION['kode_user']){
               $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_grp(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
        }
        else {
           $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_grp(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
        //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($aRow['created_by']==$_SESSION['kode_user']){
               $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_grp(\''.base64_encode('id_jenis').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
        }
        else {
           $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_grp(\''.base64_encode('id_jenis').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }
    $edit_delete = $edit.$delete;

    $row = array();
    $row = array($aRow['id_jenis'],$aRow['jenis'],$edit_delete);
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else {
  $gen_controller->response_code(http_response_code());
}
?>