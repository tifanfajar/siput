<?php 
//General Controller
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();

//Model Pelayanan
include "model/pelayanan.php";
$md_pelayanan  = new pelayanan();

//Model User
include "model/user.php";
$md_user      = new user();
session_start();

//Check Acces Menu and Tab For Notification
$id_tab  = 3;
$id_menu = 2; 
$grp_menu_ntf = get_grp_notif($id_menu,$id_tab);
$group_ntf    = array();
if(!empty($grp_menu_ntf)){
  $variableAry=explode(",",$grp_menu_ntf); 
  foreach($variableAry as $grp) {
    if(!empty($grp)){
     array_push($group_ntf,$grp);
    }
  }
}

//Check Access Tab
$akses_tab = $gen_model->GetOneRow("tr_function_access_tab",array('id_tab'=>$id_tab,'kode_function_access'=>$_SESSION['group_user'])); 
if(empty($akses_tab['fua_edit'])){
  $akses_tab['fua_edit'] = 0;
}
if(empty($akses_tab['fua_delete'])){
  $akses_tab['fua_delete'] = 0;
}


$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}

$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //Check Session
  $gen_controller->check_session();
  //View
  $gen_controller->response_code('404');
}
else if($act=="cetak_detail"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/isr.php";
}
else if($act=="cetak_all"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/isr_all.php";
}
else if($act=="table_detail_filter"){
   $param  = $_REQUEST['param'];
   include "view/ajax_table/pelayanan_table_data_isr_filter.php";
}
else if($act=="table_all_filter"){
   $param  = $_REQUEST['param'];
   include "view/ajax_table/pelayanan_table_data_isr_filter_all.php";
}
else if($act=="import_all"){
   //File
    $target = basename($_FILES['lampiran']['name']);
    move_uploaded_file($_FILES['lampiran']['tmp_name'], $target);
    $path_info = pathinfo($target);
    $ext =  strtolower($path_info['extension']); 
    
    if($ext!="xls"){
      echo "Format file salah";
      die();
    }

    require_once 'lib/excel_reader.php';
    $data = new Spreadsheet_Excel_Reader($_FILES['lampiran']['name'],false);
    $baris = $data->rowcount($sheet_index=0);
    $data_masuk=0;
    for ($i=2; $i<=$baris; $i++) {
        $insert_data = array();
        $insert_data['id_prov']             = $data->val($i, 2);
        $insert_data['kode_upt']            = $data->val($i, 3);
        $insert_data['service']             = $data->val($i, 4);
        $insert_data['no_aplikasi']         = $data->val($i, 5);
        $insert_data['tgl_terima_waba']     = $data->val($i, 6);
        $insert_data['metode']              = $data->val($i, 7);
        $insert_data['keterangan']          = $data->val($i, 8);
        $insert_data['created_date']        = $date_now_indo_full;
        $insert_data['last_update']         = $date_now_indo_full;
        $insert_data['last_update_by']      = $_SESSION['kode_user'];
     
        if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
          $insert_data['status']              = "0";
        }
        else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
          $insert_data['status']              = "3";
        }
        else {
          $insert_data['status']              = "3";
        }

        $check_spp = $db->GetOne("select count(*) as total from tr_spp where no_spp='".$data->val($i, 1)."' and status ='3' "); 
        if($check_spp>0){
            if(!empty($data->val($i, 1))){
               $count_spp = $db->GetOne("select count(*) as total from tr_isr where no_spp='".$data->val($i, 1)."' and status!='2' "); 
              if($count_spp>0){
                    //Paramater
                  $where_data = array();
                  $where_data['no_spp']       = $data->val($i, 1);
                  $gen_model->Update('tr_isr',$insert_data,$where_data);

                  $kode_isr = $db->GetOne("select kode_isr from tr_isr where no_spp='".$data->val($i, 1)."' and status!='2' "); 

                   //For Notification All
                 $upt=$insert_data['kode_upt'];
                 $upt_array[$upt][] =  array (
                                        'kode' => $kode_isr,
                                        'prov' => $insert_data['id_prov'],
                                        'upt'  => $upt
                                      );

              }
              else {
                 $insert_data['created_by']          = $_SESSION['kode_user'];
                 $insert_data['no_spp']        = $data->val($i, 1);
                 $insert_data['kode_isr']      = "isr_".date("ymdhis")."_".rand(1000,9999);
                 $gen_model->Insert('tr_isr',$insert_data);
                 
                 //For Notification All
                 $upt=$insert_data['kode_upt'];
                 $upt_array[$upt][] =  array (
                                        'kode' => $insert_data['kode_isr'],
                                        'prov' => $insert_data['id_prov'],
                                        'upt'  => $upt
                                      );
              }
              $data_masuk++;
            }
        }
       
    }

      if($data_masuk>0){
        $keys = array_keys($upt_array);
        for($i = 0; $i < count($upt_array); $i++) {
            $hitung_files = 0;
            $kode  = "";
            foreach($upt_array[$keys[$i]] as $key => $value) {
               $kode .= $value['kode'].",";
                $upt   = $value['upt'];
                $prov  = $value['prov'];
                $hitung_files++;
            }
            $kode = trim($kode,',');
            // echo "Id Prov : ".$prov."<br/>";
            // echo "UPT : ".$upt."<br/>";
            // echo "Kode : ".$kode."<br/>";
            // echo "Total : ".$hitung_files."<br/><br/>";

            //Notification
             if($hitung_files!=0){
                 if($_SESSION['group_user']=="grp_171026194411_1142"){  //Operator
                         notifikasi('2','3',$prov,$upt,'grp_171026194411_1143','ISR','Ada '.$hitung_files.' data baru pada ISR yang harus di tinjau',$kode);
                      }
                      else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                        notifikasi('4','3',$prov,$upt,'grp_171026194411_1142','ISR','Ada '.$hitung_files.' data baru pada ISR yang sudah di setujui',$kode);
                      }
                      else {
                       foreach ($group_ntf as $id_group) {
                           notifikasi('1','3',$prov,$upt,$id_group,'ISR','Ada '.$hitung_files.' data baru pada ISR',$kode);
                        }
                      }
            }
        }
        echo "OK";
      }
      else {
        echo "No SPP tidak terdaftar";
      }
    
    
    unlink($_FILES['lampiran']['name']);
}
else if($act=="import_detail"){
   //File
    $target = basename($_FILES['lampiran']['name']);
    move_uploaded_file($_FILES['lampiran']['tmp_name'], $target);
    $path_info = pathinfo($target);
    $ext =  strtolower($path_info['extension']); 
    
    if($ext!="xls"){
      echo "Format file salah";
      die();
    }

    require_once 'lib/excel_reader.php';
    $data = new Spreadsheet_Excel_Reader($_FILES['lampiran']['name'],false);
    $baris = $data->rowcount($sheet_index=0);
    
    $kode="";
    $hitung_files=0;
    $data_masuk=0;
    for ($i=2; $i<=$baris; $i++) {
        $insert_data = array();
        $insert_data['id_prov']             = $_POST['id_prov'];
        $insert_data['kode_upt']            = $_POST['id_upt'];
        $insert_data['service']             = $data->val($i, 2);
        $insert_data['no_aplikasi']         = $data->val($i, 3);
        $insert_data['tgl_terima_waba']     = $data->val($i, 4);
        $insert_data['metode']              = $data->val($i, 5);
        $insert_data['keterangan']          = $data->val($i, 6);
        $insert_data['last_update']         = $date_now_indo_full;
        $insert_data['last_update_by']      = $_SESSION['kode_user'];

        if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
          $insert_data['status']              = "0";
        }
        else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
          $insert_data['status']              = "3";
        }
        else {
          $insert_data['status']              = "3";
        }
        $check_spp = $db->GetOne("select count(*) as total from tr_spp where no_spp='".$data->val($i, 1)."' and status ='3' "); 
        if($check_spp>0){
            if(!empty($data->val($i, 1))){
               $count_spp = $db->GetOne("select count(*) as total from tr_isr where no_spp='".$data->val($i, 1)."'"); 
              if($count_spp>0){
                    //Paramater
                  $where_data = array();
                  $where_data['no_spp']       = $data->val($i, 1);
                  $gen_model->Update('tr_isr',$insert_data,$where_data);
              }
              else {
                 $insert_data['created_by']    = $_SESSION['kode_user'];
                 $insert_data['created_date']  = $date_now_indo_full;
                 $insert_data['no_spp']        = $data->val($i, 1);
                 $insert_data['kode_isr']      = "isr_".date("ymdhis")."_".rand(1000,9999);
                 $gen_model->Insert('tr_isr',$insert_data);
                 $kode .= $insert_data['kode_isr'].",";
                 $hitung_files++;
              }
              $data_masuk++;
            }
        }
       
    }

    if($data_masuk>0){ 
      $kode = rtrim($kode,",");
      //Notification
      if($hitung_files!=0){
           if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                  notifikasi('2','3',$_POST['id_prov'],$_POST['id_upt'],'grp_171026194411_1143','ISR','Ada '.$hitung_files.' data baru pada ISR yang harus di tinjau',$kode);
                }
                else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                  notifikasi('4','3',$_POST['id_prov'],$_POST['id_upt'],'grp_171026194411_1142','ISR','Ada '.$hitung_files.' data baru pada ISR yang sudah di setujui',$kode);
                }
                else {
                  foreach ($group_ntf as $id_group) {
                     notifikasi('1','3',$_POST['id_prov'],$_POST['id_upt'],$id_group,'ISR','Ada '.$hitung_files.' data baru pada ISR',$kode);
                  }
                }
      }
      echo "OK";
    }
    else {
      echo "No SPP tidak terdaftar";
    }
    
    unlink($_FILES['lampiran']['name']);
}
else if($act=="add"){

    if(!empty($_SESSION['kode_user'])){
      //Proses
      $insert_data = array();
      $insert_data['kode_isr']            = "isr_".date("ymdhis")."_".rand(1000,9999);
      $insert_data['tgl_terima_waba']     = $gen_controller->date_indo_default($_POST['tgl_terima_waba']);
      $insert_data['metode']              = get($_POST['metode']);
      $insert_data['id_prov']             = get($_POST['id_prov']);
      $insert_data['kode_upt']            = get($_POST['loket_upt']);
      $insert_data['keterangan']          = get($_POST['keterangan']);
      $insert_data['no_spp']              = get($_POST['no_spp']);
      $insert_data['service']             = get($_POST['service']);
      $insert_data['no_aplikasi']         = get($_POST['no_aplikasi']);
      $insert_data['created_date']        = $date_now_indo_full;
      $insert_data['last_update']         = $date_now_indo_full;
      $insert_data['created_by']          = $_SESSION['kode_user'];
      $insert_data['last_update_by']      = $_SESSION['kode_user'];

      if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
        $insert_data['status']              = "0";
      }
      else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
        $insert_data['status']              = "3";
      }
      else {
        $insert_data['status']              = "3";
      }

       //Validation
      if($insert_data['no_spp']!=""){
           if($gen_model->Insert('tr_isr',$insert_data)=="OK"){
                   //Notification
                  if($_SESSION['group_user']=="grp_171026194411_1144"){ // Operator
                    notifikasi('2','3',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1143','ISR','Ada 1 data baru pada ISR yang harus di tinjau',$insert_data['kode_isr']);
                  }
                  else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                    notifikasi('4','3',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1142','ISR','Ada 1 data baru pada ISR yang sudah di setujui',$insert_data['kode_isr']);
                  }
                  else {
                    foreach ($group_ntf as $id_group) {
                       notifikasi('1','3',$_POST['id_prov'],$_POST['loket_upt'],$id_group,'ISR','Ada 1 data baru pada ISR',$insert_data['kode_isr']);
                    }
                  }
               echo "OK";
          }
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="edit" and $id_parameter!=""){
    $edit  = $gen_model->GetOneRow("tr_isr",array('kode_isr'=>$id_parameter)); 
    foreach($edit as $key=>$val){
                  $key=strtolower($key);
                  $$key=$val;
    }
    $kode_client  = $gen_model->GetOne("no_klien_licence","tr_spp",array('no_spp'=>$no_spp)); 
    $cp       = $gen_model->GetOneRow("ms_company",array('company_id'=>$kode_client)); 
    $data = array(
      'kode_isr'=>$kode_isr,
      'created_date'=>$gen_controller->get_date_indonesia($created_date),
      'tgl_terima_waba'=>$gen_controller->get_date_indonesia($tgl_terima_waba),
      'no_spp'=>$no_spp,
      'client_name'=>$cp['company_name'],
      'service'=>$service,
      'no_client'=>$kode_client,
      'no_aplikasi'=>$no_aplikasi,
      'metode'=>$metode,
      'kode_upt'=>$kode_upt,
      'id_prov'=>$id_prov,
      'keterangan'=>$keterangan
    );
    echo json_encode($data); 
}
else if($act=="update"){
    if(!empty($_SESSION['kode_user'])){
      //Proses
      $update_data = array();
      $update_data['tgl_terima_waba']     = $gen_controller->date_indo_default($_POST['tgl_terima_waba']);
      $update_data['metode']              = get($_POST['metode']);
      $update_data['id_prov']             = get($_POST['id_prov']);
      $update_data['kode_upt']            = get($_POST['loket_upt']);
      $update_data['keterangan']          = get($_POST['keterangan']);
      $update_data['no_spp']              = get($_POST['no_spp']);
      $update_data['service']             = get($_POST['service']);
      $update_data['no_aplikasi']         = get($_POST['no_aplikasi']);
      $update_data['last_update']         = $date_now_indo_full;
      $update_data['last_update_by']      = $_SESSION['kode_user'];

      if($_SESSION['group_user']=="grp_171026194411_1144"){   //Operator
       $update_data['status']              = "0";
      }

       //Paramater
      $where_data = array();
      $where_data['kode_isr']       = $_POST['kode_isr'];


       //Validation
      if(!empty($_POST['kode_isr'])){
           if($gen_model->Update('tr_isr',$update_data,$where_data)=="OK"){
                   //Notification
                  if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                     notifikasi('2','3',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1143','ISR','Ada 1 data baru pada ISR yang harus di tinjau',$_POST['kode_isr']);
                  }
                  echo "OK";
          }
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="delete"){
    if(!empty($_SESSION['kode_user'])){

       $update_data['status']              = "2"; //Delete

       //Paramater
      $where_data = array();
      $where_data['kode_isr']       = $_POST['kode_isr'];


       //Validation
      if(!empty($_POST['kode_isr'])){
            echo $gen_model->Update('tr_isr',$update_data,$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="verifikasi"){
    if(!empty($_SESSION['kode_user'])){
       $db->Execute("update tr_isr set status='3' where kode_isr in(".$_REQUEST['kode'].") ");
        $total = $_REQUEST['total'];
        if(!empty($total)){
        //Notification
          if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
            $kode=str_replace("'","",$_REQUEST['kode']);
            notifikasi('4','3',$_SESSION['upt_provinsi'],$_SESSION['kode_upt'],'grp_171026194411_1142','ISR','Ada '.$total.' data baru pada ISR yang sudah di setujui',$kode);
          }
        }
         echo "OK";
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="reject"){
    if(!empty($_SESSION['kode_user'])){
       $db->Execute("update tr_isr set status='1',ket_sts_apr_reject='".$_REQUEST['ket']."' where kode_isr in(".$_REQUEST['kode'].") ");
        $total = $_REQUEST['total'];
        if(!empty($total)){
        //Notification
          if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
            $kode=str_replace("'","",$_REQUEST['kode']);
              foreach ($group_ntf as $id_group) {
                      notifikasi('3','3',$_SESSION['upt_provinsi'],$_SESSION['kode_upt'],$id_group,'ISR','Ada '.$total.' data baru pada ISR yang di reject',$kode);
               }
          }
        }
       echo "OK";
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_waiting"){
  $prov = $_REQUEST['id_prov'];
  $upt  = $_REQUEST['upt'];
  $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$prov)); 
   $aColumns = array('isr.kode_isr','isr.created_date','isr.status','spp.no_klien_licence','cp.company_name','isr.service','isr.no_aplikasi','isr.tgl_terima_waba','mtd.metode','isr.keterangan','isr.kode_isr'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

   $rResult              = $md_pelayanan->getISRWaiting($sWhere,$sOrder,$sLimit,$prov,$upt);
   $rResultFilterTotal   = $md_pelayanan->getCountISRWaiting($sWhere,$prov,$upt);




  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_isr'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_isr(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
    
    $checkbox="";
    if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
         if($aRow['status']!="4"){
             $checkbox = '<input type="checkbox" value="'.$param_id.'" name="kode_isr">';
          }
    }
   
    $sts = "";
    if($aRow['status']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }

    //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));


    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
              $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_isr(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
               $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_isr(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
             }
        }
        else {
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_isr(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_isr(\''.base64_encode('kode_isr').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_isr(\''.base64_encode('kode_isr').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
             }
        }
        else {
          $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_isr(\''.base64_encode('kode_isr').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($checkbox,$gen_controller->get_date_indonesia($aRow['created_date']),$sts,$aRow['no_klien_licence'],$aRow['company_name'],$aRow['service'],$aRow['no_aplikasi'],$gen_controller->get_date_indonesia($aRow['tgl_terima_waba']),$aRow['nama_metode'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest"){
  $aColumns = array('pv.nama_prov','upt.nama_upt','spp.no_klien_licence','cp.company_name','isr.service','isr.no_aplikasi','isr.tgl_terima_waba','mtd.metode','isr.keterangan','isr.kode_isr'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getISR($sWhere,$sOrder,$sLimit);
  $rResultFilterTotal   = $md_pelayanan->getCountISR($sWhere);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    $param_id = $aRow['kode_isr'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_isr_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_isr_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_isr_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
             }
        }
        else {
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_isr_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                  $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_isr_all(\''.base64_encode('kode_isr').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                 $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_isr_all(\''.base64_encode('kode_isr').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
             }
        }
        else {
             $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_isr_all(\''.base64_encode('kode_isr').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }


    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($aRow['nama_prov'],$aRow['nama_upt'],$aRow['no_klien_licence'],$aRow['company_name'],$aRow['service'],$aRow['no_aplikasi'],$gen_controller->get_date_indonesia($aRow['tgl_terima_waba']),$aRow['nama_metode'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_filter"){
  $param = $_REQUEST['param'];
    $aColumns = array('pv.nama_prov','upt.nama_upt','spp.no_klien_licence','cp.company_name','isr.service','isr.no_aplikasi','isr.tgl_terima_waba','mtd.metode','isr.keterangan','isr.kode_isr'); //Kolom Pada Tabel



    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getISRFilter($sWhere,$sOrder,$sLimit,$param);
  $rResultFilterTotal   = $md_pelayanan->getCountISRFilter($sWhere,$param);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    $param_id = $aRow['kode_isr'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_isr_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
              $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_isr_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
              $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_isr_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
        }
        else {
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_isr_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_isr_all(\''.base64_encode('kode_isr').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_isr_all(\''.base64_encode('kode_isr').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
             }
        }
        else {
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_isr_all(\''.base64_encode('kode_isr').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($aRow['nama_prov'],$aRow['nama_upt'],$aRow['no_klien_licence'],$aRow['company_name'],$aRow['service'],$aRow['no_aplikasi'],$gen_controller->get_date_indonesia($aRow['tgl_terima_waba']),$aRow['nama_metode'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_provinsi_filter"){
  $param = $_REQUEST['param'];
 
  $aColumns = array('spp.no_klien_licence','cp.company_name','isr.service','isr.no_aplikasi','isr.tgl_terima_waba','mtd.metode','isr.keterangan','isr.kode_isr'); //Kolom Pada Tabel


    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getISRFilter($sWhere,$sOrder,$sLimit,$param);
  $rResultFilterTotal   = $md_pelayanan->getCountISRFilter($sWhere,$param);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    $param_id = $aRow['kode_isr'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_isr(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
    
    //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                 $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_isr(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                 $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_isr(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
             }
        }
        else {
             $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_isr(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_isr(\''.base64_encode('kode_isr').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_isr(\''.base64_encode('kode_isr').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
             }
        }
        else {
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_isr(\''.base64_encode('kode_isr').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }


    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($aRow['no_klien_licence'],$aRow['company_name'],$aRow['service'],$aRow['no_aplikasi'],$gen_controller->get_date_indonesia($aRow['tgl_terima_waba']),$aRow['nama_metode'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_provinsi"){
  $prov = $_REQUEST['id_prov'];
  $upt = $_REQUEST['upt'];
  $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$prov));
  $aColumns = array('spp.no_klien_licence','cp.company_name','isr.service','isr.no_aplikasi','isr.tgl_terima_waba','mtd.metode','isr.keterangan','isr.kode_isr'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getISR($sWhere,$sOrder,$sLimit,$prov,$upt);
  $rResultFilterTotal   = $md_pelayanan->getCountISR($sWhere,$prov,$upt);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    
    $param_id = $aRow['kode_isr'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_isr(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
    
     //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_isr(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_isr(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
             }
        }
        else {
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_isr(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_isr(\''.base64_encode('kode_isr').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_isr(\''.base64_encode('kode_isr').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
             }
        }
        else {
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_isr(\''.base64_encode('kode_isr').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }


    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($aRow['no_klien_licence'],$aRow['company_name'],$aRow['service'],$aRow['no_aplikasi'],$gen_controller->get_date_indonesia($aRow['tgl_terima_waba']),$aRow['nama_metode'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else {
  $gen_controller->response_code(http_response_code());
}
?>