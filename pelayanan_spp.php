<?php 
//General Controller
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();

//Model Pelayanan
include "model/pelayanan.php";
$md_pelayanan  = new pelayanan();

//Model User
include "model/user.php";
$md_user      = new user();
session_start();

//Check Acces Menu and Tab For Notification
$id_tab  = 2;
$id_menu = 2; 
$grp_menu_ntf = get_grp_notif($id_menu,$id_tab);
$group_ntf    = array();
if(!empty($grp_menu_ntf)){
  $variableAry=explode(",",$grp_menu_ntf); 
  foreach($variableAry as $grp) {
    if(!empty($grp)){
     array_push($group_ntf,$grp);
    }
  }
}


//Check Access Tab
$akses_tab = $gen_model->GetOneRow("tr_function_access_tab",array('id_tab'=>$id_tab,'kode_function_access'=>$_SESSION['group_user']));
if(empty($akses_tab['fua_edit'])){
  $akses_tab['fua_edit'] = 0;
}
if(empty($akses_tab['fua_delete'])){
  $akses_tab['fua_delete'] = 0;
}


$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}

$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //Check Session
  $gen_controller->check_session();
  //View
  $gen_controller->response_code('404');
}
else if($act=="cetak_detail"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/spp.php";
}
else if($act=="cetak_all"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/spp_all.php";
}
else if($act=="detail"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/loket_pelayanan.php";
}
else if($act=="table_detail_filter"){
   $param  = $_REQUEST['param'];
   include "view/ajax_table/pelayanan_table_data_spp_filter.php";
}
else if($act=="table_all_filter"){
   $param  = $_REQUEST['param'];
   include "view/ajax_table/pelayanan_table_data_spp_filter_all.php";
}
else if($act=="import_all_check"){
   //File
    $target = basename($_FILES['lampiran']['name']);
    move_uploaded_file($_FILES['lampiran']['tmp_name'], $target);
    $path_info = pathinfo($target);
    $ext =  strtolower($path_info['extension']); 
    
    if($ext!="xls"){
      echo "Format file salah";
      die();
    }
    $hasil = 0;
    require_once 'lib/excel_reader.php';
    $data = new Spreadsheet_Excel_Reader($_FILES['lampiran']['name'],false);
    $baris = $data->rowcount($sheet_index=0);
    $total = 1;
    for ($i=2; $i<=$baris; $i++) {
        $insert_data = array();
        $insert_data['kode_upt']  = $data->val($i, 7);
        $get_pv = $gen_model->GetOne("id_prov","ms_upt",array('kode_upt'=>$insert_data['kode_upt']));
        if(!empty($get_pv)){
            if(!empty($data->val($i, 1))){
               $count_spp = $db->GetOne("select count(*) as total from tr_spp where no_spp='".$data->val($i, 1)."'"); 
              if($count_spp>0){
                 $hasil = 1;
              }
              else {
                
              }
            }
        }
        $total++;
    }
    if($total==$baris){
      echo $hasil;
    }
    unlink($_FILES['lampiran']['name']);
}
else if($act=="import_all"){
   //File
    $target = basename($_FILES['lampiran']['name']);
    move_uploaded_file($_FILES['lampiran']['tmp_name'], $target);
    $path_info = pathinfo($target);
    $ext =  strtolower($path_info['extension']); 
    
    if($ext!="xls"){
      echo "Format file salah";
      die();
    }

    $status_replace = $_REQUEST['replace_import_spp_all'];
    require_once 'lib/excel_reader.php';
    $data = new Spreadsheet_Excel_Reader($_FILES['lampiran']['name'],false);
    $baris = $data->rowcount($sheet_index=0);
    for ($i=2; $i<=$baris; $i++) {

         // $crt_date        = $gen_controller->date_indo_default($data->val($i, 9));
         // $tgl_terima      = $gen_controller->date_indo_default($data->val($i, 11));
         // $tgl_jatuh_tempo = $gen_controller->date_indo_default($data->val($i, 10));

         $crt_date        = $data->val($i, 9);
         $tgl_terima      = $data->val($i, 11);
         $tgl_jatuh_tempo = $data->val($i, 10);


        $insert_data = array();
        $insert_data['kode_upt']            = $data->val($i, 7);
        $insert_data['no_klien_licence']    = $data->val($i, 2);
        $insert_data['no_aplikasi']         = $data->val($i, 3);
        $insert_data['tagihan']             = rp_int($data->val($i, 5));
        $insert_data['kategori_spp']        = $data->val($i, 6);
        $insert_data['service']             = $data->val($i, 8);

        $insert_data['tgl_terima_waba']     = $tgl_terima;
        $insert_data['tgl_jatuh_tempo']     = $tgl_jatuh_tempo;
        $insert_data['metode']              = $data->val($i, 12);
        $insert_data['keterangan']          = $data->val($i, 13);

        $insert_data['created_date']        = $crt_date;
        $insert_data['last_update']         = $crt_date;
        $insert_data['last_update_by']      = $_SESSION['kode_user'];

        if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
           $insert_data['status']              = "0";
        }
        else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
          $insert_data['status']              = "3";
        }
        else {
          $insert_data['status']              = "3";
        }

        $get_pv = $gen_model->GetOne("id_prov","ms_upt",array('kode_upt'=>$insert_data['kode_upt']));

        if(!empty($get_pv)){
            $insert_data['id_prov']             = $get_pv;
            if(!empty($data->val($i, 1))){

                //Cek Perusahaan
              $check_company = $db->GetOne("select count(*) as total from ms_company where company_id='".$data->val($i, 2)."'"); 
               if($check_company==0){
                     $insert_data_cmp = array();
                      $insert_data_cmp['company_id']          = $data->val($i, 2);
                      $insert_data_cmp['company_name']        = $data->val($i, 4);
                      $insert_data_cmp['created_date']        = $date_now_indo_full;
                      $insert_data_cmp['last_update']         = $date_now_indo_full;
                      $insert_data_cmp['created_by']          = $_SESSION['kode_user'];
                      $insert_data_cmp['last_update_by']      = $_SESSION['kode_user'];

                      if($insert_data_cmp['company_id']!=""){
                          $gen_model->Insert('ms_company',$insert_data_cmp);
                      }
                }

              $count_spp = $db->GetOne("select count(*) as total from tr_spp where no_spp='".$data->val($i, 1)."' and status!='2' "); 
              if($count_spp > 0){
                  if($status_replace=="1"){
                    //Paramater
                    $where_data = array();
                    $where_data['no_spp']       = $data->val($i, 1);
                    $gen_model->Update('tr_spp',$insert_data,$where_data);
                  }
              }
              else {
                 $insert_data['created_by']          = $_SESSION['kode_user'];
                 $insert_data['no_spp']        = $data->val($i, 1);
                 $insert_data['kode_spp']      = "spp_".date("ymdhis")."_".rand(1000,9999);
                 $gen_model->Insert('tr_spp',$insert_data);

                 //For Notification All
                 $upt=$insert_data['kode_upt'];
                 $upt_array[$upt][] =  array (
                                        'kode' => $insert_data['kode_spp'],
                                        'prov' => $insert_data['id_prov'],
                                        'upt'  => $upt
                                      );
              }
            }
        }
    }

      $keys = array_keys($upt_array);
      for($i = 0; $i < count($upt_array); $i++) {
          $hitung_files = 0;
          $kode  = "";
          foreach($upt_array[$keys[$i]] as $key => $value) {
             $kode .= $value['kode'].",";
              $upt   = $value['upt'];
              $prov  = $value['prov'];
              $hitung_files++;
          }
          $kode = trim($kode,',');
          // echo "Id Prov : ".$prov."<br/>";
          // echo "UPT : ".$upt."<br/>";
          // echo "Kode : ".$kode."<br/>";
          // echo "Total : ".$hitung_files."<br/><br/>";

          //Notification
           if($hitung_files!=0){
               if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                      notifikasi('2','2',$prov,$upt,'grp_171026194411_1143','SPP','Ada '.$hitung_files.' data baru pada SPP yang harus di tinjau',$kode);
                    }
                    else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                      notifikasi('4','2',$prov,$upt,'grp_171026194411_1142','SPP','Ada '.$hitung_files.' data baru pada SPP yang sudah di setujui',$kode);
                    }
                    else {
                       foreach ($group_ntf as $id_group) {
                         notifikasi('1','2',$prov,$upt,$id_group,'SPP','Ada '.$hitung_files.' data baru pada SPP',$kode);
                      }
                    }
          }
      }

    echo "OK";
    
    unlink($_FILES['lampiran']['name']);
}
else if($act=="import_detail_check"){
   //File
    $target = basename($_FILES['lampiran']['name']);
    move_uploaded_file($_FILES['lampiran']['tmp_name'], $target);
    $path_info = pathinfo($target);
    $ext =  strtolower($path_info['extension']); 
    
    if($ext!="xls"){
      echo "Format file salah";
      die();
    }
    $hasil = 0;
    require_once 'lib/excel_reader.php';
    $data = new Spreadsheet_Excel_Reader($_FILES['lampiran']['name'],false);
    $baris = $data->rowcount($sheet_index=0);
    $total = 1;
    for ($i=2; $i<=$baris; $i++) {
        $insert_data = array();
        $insert_data['kode_upt']  = $_POST['loket_upt'];
        $get_pv = $gen_model->GetOne("id_prov","ms_upt",array('kode_upt'=>$insert_data['kode_upt']));
        if(!empty($get_pv)){
            if(!empty($data->val($i, 1))){
               $count_spp = $db->GetOne("select count(*) as total from tr_spp where no_spp='".$data->val($i, 1)."' and status!='2' "); 
              if($count_spp>0){
                 $hasil = 1;
              }
              else {
                
              }
            }
        }
        $total++;
    }
    if($total==$baris){
      echo $hasil;
    }
    unlink($_FILES['lampiran']['name']);
}
else if($act=="import_detail"){
	$status_replace = $_REQUEST['replace_import_spp_detail_all'];
   //File
    $target = basename($_FILES['lampiran']['name']);
    move_uploaded_file($_FILES['lampiran']['tmp_name'], $target);
    $path_info = pathinfo($target);
    $ext =  strtolower($path_info['extension']); 
    
    if($ext!="xls"){
      echo "Format file salah";
      die();
    }

    require_once 'lib/excel_reader.php';
    $data = new Spreadsheet_Excel_Reader($_FILES['lampiran']['name'],false);
    $baris = $data->rowcount($sheet_index=0);
    
    $hitung_files=0;
    $kode="";
    for ($i=2; $i<=$baris; $i++) {
        $insert_data = array();
         //$tgl_terima_arr  = (explode("/",$data->val($i, 9)));
         //$tgl_terima = $tgl_terima_arr[2]."-".$tgl_terima_arr[1]."-".$tgl_terima_arr[0];

         // $crt_date   = $gen_controller->date_indo_default($data->val($i, 8));
         // $tgl_terima = $gen_controller->date_indo_default($data->val($i, 10));
         // $tgl_jatuh_tempo = $gen_controller->date_indo_default($data->val($i, 9)); 

         $crt_date        = $data->val($i, 8);
         $tgl_terima      = $data->val($i, 10);
         $tgl_jatuh_tempo = $data->val($i, 9);


        $insert_data = array();
        $insert_data['id_prov']             = $_POST['id_prov'];
        $insert_data['kode_upt']            = $_POST['loket_upt'];
        $insert_data['no_klien_licence']    = $data->val($i, 2);
        $insert_data['no_aplikasi']         = $data->val($i, 3);
        $insert_data['tagihan']             = rp_int($data->val($i, 5));
        $insert_data['kategori_spp']        = $data->val($i, 6);
        $insert_data['service']             = $data->val($i, 7);

        $insert_data['tgl_jatuh_tempo']     = $tgl_jatuh_tempo;
        $insert_data['tgl_terima_waba']     = $tgl_terima;
        $insert_data['metode']              = $data->val($i, 11);
        $insert_data['keterangan']          = $data->val($i, 12);

        $insert_data['created_date']        = $crt_date;
        $insert_data['last_update']         = $crt_date;
        $insert_data['last_update_by']      = $_SESSION['kode_user'];

        
        if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
          $insert_data['status']              = "0";
        }
        else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
          $insert_data['status']              = "3";
        }
        else {
          $insert_data['status']              = "0";
        }
        if(!empty($data->val($i, 1))){

            //Cek Perusahaan
              $check_company = $db->GetOne("select count(*) as total from ms_company where company_id='".$data->val($i, 2)."'"); 
               if($check_company==0){
                     $insert_data_cmp = array();
                      $insert_data_cmp['company_id']          = $data->val($i, 2);
                      $insert_data_cmp['company_name']        = $data->val($i, 4);
                      $insert_data_cmp['created_date']        = $date_now_indo_full;
                      $insert_data_cmp['last_update']         = $date_now_indo_full;
                      $insert_data_cmp['created_by']          = $_SESSION['kode_user'];
                      $insert_data_cmp['last_update_by']      = $_SESSION['kode_user'];

                      if($insert_data_cmp['company_id']!=""){
                            $gen_model->Insert('ms_company',$insert_data_cmp);
                      }
                } 

           $count_spp = $db->GetOne("select count(*) as total from tr_spp where no_spp='".$data->val($i, 1)."' and status!='2'"); 
          if($count_spp>0){
    			  if($status_replace=="1"){
    					  $where_data = array();
    					  $where_data['no_spp']       = $data->val($i, 1);
    					  $gen_model->Update('tr_spp',$insert_data,$where_data);
             }
          }
          else {
             $insert_data['created_by']          = $_SESSION['kode_user'];
             $insert_data['no_spp']        = $data->val($i, 1);
             $insert_data['kode_spp']      = "spp_".date("ymdhis")."_".rand(1000,9999);
             $gen_model->Insert('tr_spp',$insert_data);
             $kode .= $insert_data['kode_spp'].",";
             $hitung_files++;
          }
        }
       
    }
    $kode = rtrim($kode,",");
    //Notification
    if($hitung_files!=0){
         if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                notifikasi('2','2',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1143','SPP','Ada '.$hitung_files.' data baru pada SPP yang harus di tinjau',$kode);
              }
              else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                notifikasi('4','2',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1142','SPP','Ada '.$hitung_files.' data baru pada SPP yang sudah di setujui',$kode);
              }
              else {
                foreach ($group_ntf as $id_group) {
                   notifikasi('1','2',$_POST['id_prov'],$_POST['loket_upt'],$id_group,'SPP','Ada '.$hitung_files.' data baru pada SPP',$kode);
                }
              }
    }
    echo "OK";
    
    unlink($_FILES['lampiran']['name']);
}
else if($act=="add"){

    //Upload Single File
    // $tmp              = $_FILES["lampiran"]["tmp_name"];
    // $origin_file_name = $_FILES['lampiran']['name'];
    // if(!empty($origin_file_name)){
    //   $ext              = strtolower(pathinfo($_FILES["lampiran"]["name"])['extension']);
    //   $file_name        = date("ymdhis")."_".rand(1000,9999).".".$ext;
    //   $path             = "assets/attachment/pelayanan/spp/";
    // }

    //Multiple File
    $path             = "assets/attachment/pelayanan/spp/";
    $count            = 0;

                 

    if(!empty($_SESSION['kode_user'])){
      //Proses
      $kode_spp = "spp_".date("ymdhis")."_".rand(1000,9999);
      $insert_data = array();
      $insert_data['kode_spp']            = $kode_spp;
      $insert_data['tgl_terima_waba']     = $gen_controller->date_indo_default($_POST['tgl_terima_waba']);
      $insert_data['tgl_jatuh_tempo']     = $gen_controller->date_indo_default($_POST['tgl_jatuh_tempo']);
      $insert_data['metode']              = get($_POST['metode']);
      $insert_data['id_prov']             = get($_POST['id_prov']);
      $insert_data['kode_upt']            = get($_POST['loket_upt']);
      $insert_data['keterangan']          = get($_POST['keterangan']);
      $insert_data['no_spp']              = get($_POST['no_spp']);
      $insert_data['no_klien_licence']    = get($_POST['kode_perusahaan']);
      $insert_data['service']             = get($_POST['service']);
      $insert_data['no_aplikasi']         = get($_POST['no_aplikasi']);
      $insert_data['kategori_spp']        = get($_POST['kategori_spp']);
      $insert_data['tagihan']             = rp_int(get($_POST['nilai_bhp']));
      $insert_data['created_date']        = $date_now_indo_full;
      $insert_data['last_update']         = $date_now_indo_full;
      $insert_data['created_by']          = $_SESSION['kode_user'];
      $insert_data['last_update_by']      = $_SESSION['kode_user'];

         

      if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
        $insert_data['status']              = "0";
      }
      else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
        $insert_data['status']              = "3";
      }
      else {
        $insert_data['status']              = "3";
      }

   
       //Validation
      if($insert_data['no_spp']!=""){
           if($gen_model->Insert('tr_spp',$insert_data)=="OK"){

                  //Upload Files
                  foreach ($_FILES['lampiran']['name'] as $f => $name) {     
                      if ($_FILES['lampiran']['error'][$f] == 4) {
                          continue; // Skip file if any error found
                      }        
                      if ($_FILES['lampiran']['error'][$f] == 0) { 
                           $ext              = strtolower(pathinfo($_FILES["lampiran"]["name"][$f])['extension']);
                           $file_name        = date("ymdhis")."_".rand(1000,9999).".".$ext;           
                          if(move_uploaded_file($_FILES["lampiran"]["tmp_name"][$f],$path.$file_name))
                            $insert_data2 = array();
                            $insert_data2['kode_spp_files']     = "spp_fls_".date("ymdhis")."_".rand(1000,9999);
                            $insert_data2['kode_spp']           = $kode_spp;
                            $insert_data2['lampiran']           = $file_name;
                            $insert_data2['created_date']       = $date_now_indo_full;
                            $insert_data2['last_update']        = $date_now_indo_full;
                            $insert_data2['created_by']         = $_SESSION['kode_user'];
                            $insert_data2['last_update_by']     = $_SESSION['kode_user'];
                            $gen_model->Insert('tr_spp_files',$insert_data2);
                            $count++; // Number of successfully uploaded file
                      }
                  }

                   //Notification
                  if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                    notifikasi('2','2',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1143','SPP','Ada 1 data baru pada SPP yang harus di tinjau',$insert_data['kode_spp']);
                  }
                  else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                    notifikasi('4','2',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1142','SPP','Ada 1 data baru pada SPP yang sudah di setujui',$insert_data['kode_spp']);
                  }
                  else {
                    foreach ($group_ntf as $id_group) {
                       notifikasi('1','2',$_POST['id_prov'],$_POST['loket_upt'],$id_group,'SPP','Ada 1 data baru pada SPP',$insert_data['kode_spp']);
                    }
                  }
               echo "OK";
          }
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="edit" and $id_parameter!=""){

     $sql = "SELECT  spp.*,pv.nama_prov,us_crt.username as pembuat ,us_updt.username as perubah,cp.company_name
          FROM tr_spp as spp
          INNER JOIN ms_company AS cp 
            on cp.company_id = spp.no_klien_licence
          left outer join ms_user as us_crt
            on us_crt.kode_user=spp.created_by
          left outer join ms_user as us_updt
            on us_updt.kode_user=spp.last_update_by
          left outer join ms_provinsi as pv
            on pv.id_prov=spp.id_prov
          where kode_spp='".$id_parameter."' ";
    $edit  = $db->getRow($sql);
    foreach($edit as $key=>$val){
                  $key=strtolower($key);
                  $$key=$val;
    }
    $data = array(
      'tgl_terima_waba'=>$gen_controller->get_date_indonesia($tgl_terima_waba),
      'tgl_jatuh_tempo'=>$gen_controller->get_date_indonesia($tgl_jatuh_tempo),
      'metode'=>$metode,
      'kode_spp'=>$kode_spp,
      'no_spp'=>$no_spp,
      'id_prov'=>$id_prov,
      'lampiran'=>$basepath.'assets/attachment/pelayanan/spp/'.$lampiran,
      'kode_upt'=>$kode_upt,
      'no_klien_licence'=>$no_klien_licence,
      'no_aplikasi'=>$no_aplikasi,
      'company_name'=>$company_name,
      'service'=>$service,
      'kategori_spp'=>$kategori_spp,
      'tagihan'=>int_to_rp($tagihan),
      'keterangan'=>$keterangan
    );
    echo json_encode($data); 
}
else if($act=="get_files"){
    $activity=$_POST['aksi'];
    $readonly="";
    if($activity=="do_detail"){ 
      $readonly=" readonly ";
     }
    $data_fls = $gen_model->GetWhere('tr_spp_files',array('kode_spp'=>$_POST['kode_spp']));
    $count=1;
    while ($fls = $data_fls->FetchRow()) {  ?>
        <div class="class_up_<?php echo $count ?> col-md-4 form-group form-box col-xs-12"></div>
        <div class="class_up_<?php echo $count ?> col-md-4 form-group form-box col-xs-12">
          <span class="class_up_<?php echo $count ?> label">Bukti Kirim</span>  
          <?php if($activity!="do_detail"){  ?>
            <input  class="class_up_<?php echo $count ?> form-control"  id="lampiran" name="lampiran[]"  type="file" <?php echo $readonly ?>  >
          <?php } 
          if($activity=="do_detail" or  $activity=="do_edit"){ 
            echo '<br/><a class="class_up_'.$count.'" style="text-decoration:none" id="lampiran_spp" href="'.$basepath.'assets/attachment/pelayanan/spp/'.$fls['lampiran'].'" download>Download<a/>'; 
              } ?>
        </div>
        <div class="class_up_<?php echo $count ?> col-md-1 form-group form-box col-xs-12">
        <button type="button" onClick="hapus_file_id('<?php echo $fls['kode_spp_files'] ?>','<?php echo $count ?>');" class="btn btn-danger btn-sm btn-batal">Hapus</button>
        </div><div class="class_up_<?php echo $count ?> col-md-3 form-group form-box col-xs-12"></div>
   <?php $count++;
  }
}
else if($act=="update"){
    if(!empty($_SESSION['kode_user'])){

      //Single File
      // $tmp              = $_FILES["lampiran"]["tmp_name"];
      // $origin_file_name = $_FILES['lampiran']['name'];

      // if(!empty($origin_file_name)){
      //   $ext              = strtolower(pathinfo($_FILES["lampiran"]["name"])['extension']);
      //   $file_name        = date("ymdhis")."_".rand(1000,9999).".".$ext;
      //   $path             = "assets/attachment/pelayanan/spp/";
      // }
      
      //Multiple File
      $path             = "assets/attachment/pelayanan/spp/";
      $count            = 0;


      //Proses
      $update_data = array();
      $update_data['tgl_terima_waba']     = $gen_controller->date_indo_default($_POST['tgl_terima_waba']);
      $update_data['tgl_jatuh_tempo']     = $gen_controller->date_indo_default($_POST['tgl_jatuh_tempo']);
      $update_data['metode']              = get($_POST['metode']);
      $update_data['id_prov']             = get($_POST['id_prov']);
      $update_data['kode_upt']            = get($_POST['loket_upt']);
      $update_data['keterangan']          = get($_POST['keterangan']);
      $update_data['no_spp']              = get($_POST['no_spp']);
      $update_data['no_klien_licence']    = get($_POST['kode_perusahaan']);
      $update_data['service']             = get($_POST['service']);
      $update_data['no_aplikasi']         = get($_POST['no_aplikasi']);
      $update_data['kategori_spp']        = get($_POST['kategori_spp']);
      $update_data['tagihan']             = rp_int(get($_POST['nilai_bhp']));
      $update_data['last_update']         = $date_now_indo_full;
      $update_data['last_update_by']      = $_SESSION['kode_user'];
      

      if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
         $update_data['status']              = "0";
      }
      

       //Paramater
      $where_data = array();
      $where_data['kode_spp']       = $_POST['kode_spp'];


       //Validation
      if(!empty($_POST['kode_spp'])){
           if($gen_model->Update('tr_spp',$update_data,$where_data)=="OK"){

                 // if(!empty($origin_file_name)){
                    //Delete File
                    //$old_file = $gen_model->GetOne('lampiran','tr_spp',$where_data);
                   // $gen_controller->delete_file($path,$old_file);

                    
                   // $update_data['lampiran']              = $file_name;
                   // $gen_model->Update('tr_spp',$update_data,$where_data);
                    //$gen_controller->upload_file($tmp,$path,$file_name);
                 // } 

                   //Upload Files
                  foreach ($_FILES['lampiran']['name'] as $f => $name) {     
                      if ($_FILES['lampiran']['error'][$f] == 4) {
                          continue; // Skip file if any error found
                      }        
                      if ($_FILES['lampiran']['error'][$f] == 0) { 
                           $ext              = strtolower(pathinfo($_FILES["lampiran"]["name"][$f])['extension']);
                           $file_name        = date("ymdhis")."_".rand(1000,9999).".".$ext;           
                          if(move_uploaded_file($_FILES["lampiran"]["tmp_name"][$f],$path.$file_name))
                            $insert_data2 = array();
                            $insert_data2['kode_spp_files']     = "spp_fls_".date("ymdhis")."_".rand(1000,9999);
                            $insert_data2['kode_spp']           = $_POST['kode_spp'];
                            $insert_data2['lampiran']           = $file_name;
                            $insert_data2['created_date']       = $date_now_indo_full;
                            $insert_data2['last_update']        = $date_now_indo_full;
                            $insert_data2['created_by']         = $_SESSION['kode_user'];
                            $insert_data2['last_update_by']     = $_SESSION['kode_user'];
                            $gen_model->Insert('tr_spp_files',$insert_data2);
                            $count++; // Number of successfully uploaded file
                      }
                  }

                   //Notification
                  if($_SESSION['group_user']=="grp_171026194411_1142"){  //Operator
                     notifikasi('2','2',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1143','SPP','Ada 1 data baru pada SPP yang harus di tinjau',$_POST['kode_spp']);
                  }
                  echo "OK";
          }

      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="delete_file"){
    $path  = "assets/attachment/pelayanan/spp/";
    if(!empty($_SESSION['kode_user'])){
       //Paramater
      $where_data = array();
      $where_data['kode_spp_files']       = $_POST['kode'];
       //Validation
      if(!empty($_POST['kode'])){
          @$foto_name = $gen_model->GetOne('lampiran','tr_spp_files',$where_data);
          @$gen_controller->delete_file($path,$foto_name);
          echo $gen_model->Delete('tr_spp_files',$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="delete"){
    if(!empty($_SESSION['kode_user'])){

       $update_data['status']              = "2"; //Delete

       //Paramater
      $where_data = array();
      $where_data['kode_spp']       = $_POST['kode_spp'];


       //Validation
      if(!empty($_POST['kode_spp'])){
            echo $gen_model->Update('tr_spp',$update_data,$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="verifikasi"){
    
    if(!empty($_SESSION['kode_user'])){
       $db->Execute("update tr_spp set status='3' where kode_spp in(".$_REQUEST['kode'].") ");
       $total = $_REQUEST['total'];
        if(!empty($total)){
        //Notification
          if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
            $kode=str_replace("'","",$_REQUEST['kode']);
            notifikasi('4','2',$_SESSION['upt_provinsi'],$_SESSION['kode_upt'],'grp_171026194411_1142','SPP','Ada '.$total.' data baru pada SPP yang sudah di setujui',$kode);
          }
        }
       echo "OK";
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="reject"){
    if(!empty($_SESSION['kode_user'])){
       $db->Execute("update tr_spp set status='1',ket_sts_apr_reject='".$_REQUEST['ket']."' where kode_spp in(".$_REQUEST['kode'].") ");
        $total = $_REQUEST['total'];
        if(!empty($total)){
        //Notification
          if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
            $kode=str_replace("'","",$_REQUEST['kode']);
              foreach ($group_ntf as $id_group) {
                      notifikasi('3','2',$_SESSION['upt_provinsi'],$_SESSION['kode_upt'],$id_group,'SPP','Ada '.$total.' data baru pada SPP yang di reject',$kode);
               }
          }
        }
       echo "OK";
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_waiting"){
  $prov = $_REQUEST['id_prov'];
  $upt  = $_REQUEST['upt'];
  $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$prov)); 
    $aColumns = array('spp.kode_spp','spp.created_date','spp.status','spp.no_klien_licence','cp.company_name','spp.service','spp.no_aplikasi','spp.no_spp','kgs.kode_kategori_spp','spp.tagihan','spp.tgl_jatuh_tempo','spp.tgl_terima_waba','mtd.metode','spp.keterangan','spp.kode_spp'); 

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

   $rResult              = $md_pelayanan->getSPPWaiting($sWhere,$sOrder,$sLimit,$prov,$upt);
   $rResultFilterTotal   = $md_pelayanan->getCountSPPWaiting($sWhere,$prov,$upt);




  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_spp'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_spp(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
    

    $lampiran ="";
    if(!empty($aRow['lampiran'])){
       $lampiran = '<center><a   title="Download File" style="cursor:pointer;color:blue" href="'.$basepath.'assets/attachment/pelayanan/spp/'.$aRow['lampiran'].'" download><i class="fa fa-download fa-2x"></i></a></center>';
    }   


     $checkbox="";
    if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
         if($aRow['status_verif']!="4"){
             $checkbox = '<input type="checkbox" value="'.$param_id.'" name="kode_spp">';
          }
    }
   
     $sts = "";
    if($aRow['status_verif']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status_verif']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status_verif']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }

    //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));



    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
               $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_spp(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
               $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_spp(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
             }
        }
        else {
           $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_spp(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_spp(\''.base64_encode('kode_spp').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_spp(\''.base64_encode('kode_spp').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
             }
        }
        else {
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_spp(\''.base64_encode('kode_spp').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }

     

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($checkbox,$gen_controller->get_date_indonesia($aRow['tgl_buat']),$sts,$aRow['no_klien_licence'],$aRow['company_name'],$aRow['service'],$aRow['no_aplikasi'],$aRow['no_spp'],$aRow['kgs_data'],int_to_rp($aRow['tagihan']),$gen_controller->get_date_indonesia($aRow['tgl_jatuh_tempo']),$gen_controller->get_date_indonesia($aRow['tgl_terima_waba']),$aRow['nama_metode'],$aRow['keterangan'],"<center>".$edit_delete."</center>");

    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest"){
  //Kolom Pada Tabel
    $aColumns = array('pv.nama_prov','upt.nama_upt','spp.no_klien_licence','cp.company_name','spp.service','spp.no_aplikasi','spp.no_spp','kgs.kode_kategori_spp','spp.tagihan','spp.tgl_jatuh_tempo','spp.tgl_terima_waba','mtd.metode','spp.keterangan','spp.kode_spp'); 

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getSPP($sWhere,$sOrder,$sLimit);
  $rResultFilterTotal   = $md_pelayanan->getCountSPP($sWhere);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    $param_id = $aRow['kode_spp'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_spp_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

    $lampiran ="";
    if(!empty($aRow['lampiran'])){
       $lampiran = '<center><a   title="Download File" style="cursor:pointer;color:blue" href="'.$basepath.'assets/attachment/pelayanan/spp/'.$aRow['lampiran'].'" download><i class="fa fa-download fa-2x"></i></a></center>';
    }   

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){
        //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
               $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_spp_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
               $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_spp_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
        }
        else {
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_spp_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
        //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
               $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_spp_all(\''.base64_encode('kode_spp').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
               $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_spp_all(\''.base64_encode('kode_spp').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
        }
        else {
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_spp_all(\''.base64_encode('kode_spp').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($aRow['nama_prov'],$aRow['nama_upt'],$aRow['no_klien_licence'],$aRow['company_name'],$aRow['service'],$aRow['no_aplikasi'],$aRow['no_spp'],$aRow['kgs_data'],int_to_rp($aRow['tagihan']),$gen_controller->get_date_indonesia($aRow['tgl_jatuh_tempo']),$gen_controller->get_date_indonesia($aRow['tgl_terima_waba']),$aRow['nama_metode'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_filter"){
  $param = $_REQUEST['param'];
    //Kolom Pada Tabel
    $aColumns = array('pv.nama_prov','upt.nama_upt','spp.no_klien_licence','cp.company_name','spp.service','spp.no_aplikasi','spp.no_spp','kgs.kode_kategori_spp','spp.tagihan','spp.tgl_jatuh_tempo','spp.tgl_terima_waba','mtd.metode','spp.keterangan','spp.kode_spp'); 



    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getSPPFilter($sWhere,$sOrder,$sLimit,$param);
  $rResultFilterTotal   = $md_pelayanan->getCountSPPFilter($sWhere,$param);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    $param_id = $aRow['kode_spp'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_spp_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

    $lampiran ="";
    if(!empty($aRow['lampiran'])){
       $lampiran = '<center><a   title="Download File" style="cursor:pointer;color:blue" href="'.$basepath.'assets/attachment/pelayanan/spp/'.$aRow['lampiran'].'" download><i class="fa fa-download fa-2x"></i></a></center>';
    }  

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
               $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_spp_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                 $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_spp_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
             }
        }
        else {
                 $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_spp_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 

         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_spp_all(\''.base64_encode('kode_spp').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_spp_all(\''.base64_encode('kode_spp').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
             }
        }
        else {
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_spp_all(\''.base64_encode('kode_spp').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($aRow['nama_prov'],$aRow['nama_upt'],$aRow['no_klien_licence'],$aRow['company_name'],$aRow['service'],$aRow['no_aplikasi'],$aRow['no_spp'],$aRow['kgs_data'],int_to_rp($aRow['tagihan']),$gen_controller->get_date_indonesia($aRow['tgl_jatuh_tempo']),$gen_controller->get_date_indonesia($aRow['tgl_terima_waba']),$aRow['nama_metode'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_provinsi_filter"){
  $param = $_REQUEST['param'];
   //Kolom Pada Tabel
    $aColumns = array('spp.no_klien_licence','cp.company_name','spp.service','spp.no_aplikasi','spp.no_spp','kgs.kode_kategori_spp','spp.tagihan','spp.tgl_jatuh_tempo','spp.tgl_terima_waba','mtd.metode','spp.keterangan','spp.kode_spp'); 

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getSPPFilter($sWhere,$sOrder,$sLimit,$param);
  $rResultFilterTotal   = $md_pelayanan->getCountSPPFilter($sWhere,$param);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    $param_id = $aRow['kode_spp'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_spp_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';


    $lampiran ="";
    if(!empty($aRow['lampiran'])){
       $lampiran = '<center><a   title="Download File" style="cursor:pointer;color:blue" href="'.$basepath.'assets/attachment/pelayanan/spp/'.$aRow['lampiran'].'" download><i class="fa fa-download fa-2x"></i></a></center>';
    }   


    //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));


    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 

         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
               $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_spp_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                 $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_spp_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
             }
        }
        else {
                 $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_spp_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
         }
    }
    if($akses_tab['fua_delete']=="1"){ 

         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                  $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_spp_all(\''.base64_encode('kode_spp').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                 $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_spp_all(\''.base64_encode('kode_spp').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
             }
        }
        else {
             $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_spp_all(\''.base64_encode('kode_spp').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($aRow['no_klien_licence'],$aRow['company_name'],$aRow['service'],$aRow['no_aplikasi'],$aRow['no_spp'],$aRow['kgs_data'],int_to_rp($aRow['tagihan']),$gen_controller->get_date_indonesia($aRow['tgl_jatuh_tempo']),$gen_controller->get_date_indonesia($aRow['tgl_terima_waba']),$aRow['nama_metode'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_provinsi"){
  $prov = $_REQUEST['id_prov'];
  $upt = $_REQUEST['upt'];
  $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$prov));

    //Kolom Pada Tabel
    $aColumns = array('spp.no_klien_licence','cp.company_name','spp.service','spp.no_aplikasi','spp.no_spp','kgs.kode_kategori_spp','spp.tagihan','spp.tgl_jatuh_tempo','spp.tgl_terima_waba','mtd.metode','spp.keterangan','spp.kode_spp'); 

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getSPP($sWhere,$sOrder,$sLimit,$prov,$upt);
  $rResultFilterTotal   = $md_pelayanan->getCountSPP($sWhere,$prov,$upt);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_spp'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_spp(\''.$param_id.'\',\''.$wilayah['id_map'].'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';


    $lampiran ="";
    if(!empty($aRow['lampiran'])){
       $lampiran = '<center><a   title="Download File" style="cursor:pointer;color:blue" href="'.$basepath.'assets/attachment/pelayanan/spp/'.$aRow['lampiran'].'" download><i class="fa fa-download fa-2x"></i></a></center>';
    }   

    
    //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
          //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                   $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_spp(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                  $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_spp(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
             }
        }
        else {
                 $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_spp(\''.$param_id.'\',\''.$wilayah['id_map'].'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
          } 
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                  $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_spp(\''.base64_encode('kode_spp').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                 $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_spp(\''.base64_encode('kode_spp').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
             }
        }
        else {
           $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_spp(\''.base64_encode('kode_spp').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
       }
    }

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($aRow['no_klien_licence'],$aRow['company_name'],$aRow['service'],$aRow['no_aplikasi'],$aRow['no_spp'],$aRow['kgs_data'],int_to_rp($aRow['tagihan']),$gen_controller->get_date_indonesia($aRow['tgl_jatuh_tempo']),$gen_controller->get_date_indonesia($aRow['tgl_terima_waba']),$aRow['nama_metode'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="akumulasi"){
  $aColumns = array('pv.nama_prov','pl.jenis_perizinan','total','gangguan','konsultasi','lain'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getAkumulasiPelayananAll($sWhere,$sOrder,$sLimit);
  $rResultFilterTotal   = $md_pelayanan->getCountAkumulasiPelayananAll($sWhere);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  $list_jns="";
  $data_jns = $db->SelectLimit("select DISTINCT(jenis_perizinan) from tr_pelayanan where status='3' "); 
  while($list = $data_jns->FetchRow()){
        foreach($list as $key=>$val){
                  $key=strtolower($key);
                  $$key=$val;
                }  
         $list_jns .= $jenis_perizinan.", ";
      }  


  while($aRow = $rResult->FetchRow()){
    $row = array();
    $row = array($aRow['nama_prov'],trim($list_jns,', '),"<center>".$aRow['total']."</center>","<center>".$aRow['gangguan']."</center>","<center>".$aRow['konsultasi']."</center>","<center>".$aRow['aksestensi']."</center>","<center>".$aRow['lain']."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="akumulasi_detail"){
  $prov = $_REQUEST['id_prov'];
  $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$prov)); 
  $aColumns = array('pl.jenis_perizinan','total','gangguan','konsultasi','lain'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getAkumulasiPelayanan($sWhere,$sOrder,$sLimit,$prov);
  $rResultFilterTotal   = $md_pelayanan->getCountAkumulasiPelayanan($sWhere,$prov);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );
      

  while($aRow = $rResult->FetchRow()){
    $row = array();
    $row = array($aRow['jenis_perizinan'],"<center>".$aRow['total']."</center>","<center>".$aRow['gangguan']."</center>","<center>".$aRow['konsultasi']."</center>","<center>".$aRow['lain']."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else {
  $gen_controller->response_code(http_response_code());
}
?>