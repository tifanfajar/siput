<?php

//AUTH
Auth::routes();
Route::get('login', 'Auth\LoginController@getLogin')->name('login');
Route::post('postLogin', 'Auth\LoginController@postLogin')->name('postLogin');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/images/user/{filename}', function ($filename)
{
	$path = public_path('images/user') . '/' . $filename;
	$file = File::get($path);
	$type = File::mimeType($path);
	$response = Response::make($file);
	$response->header("Content-Type", $type);
	return $response;
})->name('loginImages');


//WEB
Route::group(['middleware' => ['web']], function () {
	Route::group(['middleware' => ['auth']], function () {

		//CORE
		Route::get('/','Core\DashboardController@index');
		Route::get('provinsi/kabupaten/{id}', 'Dev\CountryController@getKabupaten')->name('admin.cities.get_by_country');
		Route::get('getUpt/{id}', 'Dev\GetUptController@getUpt')->name('admin.upt.getUpt');
		Route::get('getNotification/{id_module}', 'Dev\Notification\NotificationController@getNotification')->name('get-notification');
		Route::get('updateToRead/{id}', 'Dev\Notification\NotificationController@updateToRead')->name('update-to-read-notification');

		//PENGATURAN
		Route::prefix('pengaturan')->group(function(){

			//HAK AKSES
			Route::prefix('hak-akses')->group(function(){
				Route::group(['middleware' => 'roler:pengaturan/hak-akses'], function () {
					Route::get('/','Core\Setting\AccessRights\AccessRightsController@index')->name('pengaturan/hak-akses');
					Route::get('edit/{id}','Core\Setting\AccessRights\AccessRightsController@edit')->name('hak-akses-edit');
				});
				Route::group(['middleware' => 'rolec:pengaturan/hak-akses'], function () {
					Route::get('create','Core\Setting\AccessRights\AccessRightsController@create')->name('hak-akses-create');
					Route::post('save','Core\Setting\AccessRights\AccessRightsController@save')->name('hak-akses-save');
				});
				Route::group(['middleware' => 'roleu:pengaturan/hak-akses'], function () {
					Route::post('update/{id}','Core\Setting\AccessRights\AccessRightsController@update')->name('hak-akses-update');
				});
				Route::group(['middleware' => 'roled:pengaturan/hak-akses'], function () {
					Route::get('delete/{id}','Core\Setting\AccessRights\AccessRightsController@delete')->name('hak-akses-delete');
				});	
			});

			Route::prefix('perusahaan')->group(function(){
				Route::group(['middleware' => 'roler:pengaturan/perusahaan'], function () {
					Route::get('/','Core\Setting\Company\CompanyController@index')->name('pengaturan/perusahaan');
					Route::get('print','Core\Setting\Company\CompanyController@print')->name('company-print');
					Route::get('download','Core\Setting\Company\CompanyController@export')->name('company-export');
					Route::post('import','Core\Setting\Company\CompanyController@importPost')->name('company-import');
					Route::post('import/previews','Core\Setting\Company\CompanyController@importPreview')->name('company-preview-import');
					Route::get('sampel','Core\Setting\Company\CompanyController@downloadSampel')->name('company-sampel');
				});
				Route::group(['middleware' => 'rolec:pengaturan/perusahaan'], function () {
					Route::post('save','Core\Setting\Company\CompanyController@save')->name('company-save');
				});
				Route::group(['middleware' => 'roleu:pengaturan/perusahaan'], function () {
					Route::post('update','Core\Setting\Company\CompanyController@update')->name('company-update');
				});
				Route::group(['middleware' => 'roled:pengaturan/perusahaan'], function () {
					Route::get('delete','Core\Setting\Company\CompanyController@delete')->name('company-delete');
				});		
			});

			//DAFTAR PENGGUNA
			Route::prefix('daftar-pengguna')->group(function()
			{
				Route::group(['middleware' => 'roler:pengaturan/daftar-pengguna'], function () {
					Route::get('/','Core\Setting\Users\UsersController@index')->name('pengaturan/daftar-pengguna');
					Route::get('print','Core\Setting\Users\UsersController@print')->name('users-print');
					Route::get('download','Core\Setting\Users\UsersController@export')->name('users-export');
					
					Route::post('import','Core\Setting\Users\UsersController@importPost')->name('users-import-post');
					Route::post('import/previews','Core\Setting\Users\UsersController@importPreview')->name('users-preview-import');
					Route::get('sampel','Core\Setting\Users\UsersController@downloadSampel')->name('users-sampel');
				});
				Route::group(['middleware' => 'rolec:pengaturan/daftar-pengguna'], function () {
					Route::post('save','Core\Setting\Users\UsersController@save')->name('users-save');
				});
				Route::group(['middleware' => 'roleu:pengaturan/daftar-pengguna'], function () {
					Route::post('update','Core\Setting\Users\UsersController@update')->name('users-update');
				});
				Route::group(['middleware' => 'roled:pengaturan/daftar-pengguna'], function () {
					Route::get('delete','Core\Setting\Users\UsersController@delete')->name('users-delete');
				});		
			});

			//REFRENSI
			Route::prefix('refrensi')->group(function(){

				Route::group(['middleware' => 'roler:pengaturan/refrensi'], function () {
					Route::get('/','Core\Setting\Reference\ReferenceController@index')->name('pengaturan/refrensi');
					Route::get('print','Core\Setting\Reference\ReferenceController@print')->name('kategori-spp-print');
					Route::get('download','Core\Setting\Reference\ReferenceController@export')->name('kategori-spp-export');
				});
				// KATEGORI
				Route::group(['middleware' => 'rolec:pengaturan/refrensi'], function () {
					Route::post('save','Core\Setting\Reference\ReferenceController@save')->name('kategori-spp-save');
				});
				Route::group(['middleware' => 'roleu:pengaturan/refrensi'], function () {
					Route::post('update','Core\Setting\Reference\ReferenceController@update')->name('kategori-spp-update');
				});
				Route::group(['middleware' => 'roled:pengaturan/refrensi'], function () {
					Route::get('delete','Core\Setting\Reference\ReferenceController@delete')->name('kategori-spp-delete');
				});
				Route::get('download', 'Core\Setting\Reference\ReferenceController@export')->name('refrensi-export');

				//MATERI
				Route::group(['middleware' => 'rolec:pengaturan/refrensi'], function () {
					Route::post('save-materi','Core\Setting\Reference\TheoryController@save')->name('materi-save');
				});
				Route::group(['middleware' => 'roleu:pengaturan/refrensi'], function () {
					Route::post('update-materi','Core\Setting\Reference\TheoryController@update')->name('materi-update');
				});
				Route::group(['middleware' => 'roled:pengaturan/refrensi'], function () {
					Route::get('delete-materi','Core\Setting\Reference\TheoryController@delete')->name('materi-delete');
				});
				//KEGIATAN
				Route::group(['middleware' => 'rolec:pengaturan/refrensi'], function () {
					Route::post('save-kegiatan','Core\Setting\Reference\ActivityController@save')->name('kegiatan-save');
				});
				Route::group(['middleware' => 'roleu:pengaturan/refrensi'], function () {
					Route::post('update-kegiatan','Core\Setting\Reference\ActivityController@update')->name('kegiatan-update');
				});
				Route::group(['middleware' => 'roled:pengaturan/refrensi'], function () {
					Route::get('delete-kegiatan','Core\Setting\Reference\ActivityController@delete')->name('kegiatan-delete');
				});
				//METODE
				Route::group(['middleware' => 'rolec:pengaturan/refrensi'], function () {
					Route::post('save-metode','Core\Setting\Reference\MethodController@save')->name('metode-save');
				});
				Route::group(['middleware' => 'roleu:pengaturan/refrensi'], function () {
					Route::post('update-metode','Core\Setting\Reference\MethodController@update')->name('metode-update');
				});
				Route::group(['middleware' => 'roled:pengaturan/refrensi'], function () {
					Route::get('delete-metode','Core\Setting\Reference\MethodController@delete')->name('metode-delete');
				});
				//TUJUAN
				Route::group(['middleware' => 'rolec:pengaturan/refrensi'], function () {
					Route::post('save-tujuan','Core\Setting\Reference\PurposeController@save')->name('tujuan-save');
				});
				Route::group(['middleware' => 'roleu:pengaturan/refrensi'], function () {
					Route::post('update-tujuan','Core\Setting\Reference\PurposeController@update')->name('tujuan-update');
				});
				Route::group(['middleware' => 'roled:pengaturan/refrensi'], function () {
					Route::get('delete-tujuan','Core\Setting\Reference\PurposeController@delete')->name('tujuan-delete');
				});
				//GRUP PERIZINAN
				Route::group(['middleware' => 'rolec:pengaturan/refrensi'], function () {
					Route::post('save-group-perizinan','Core\Setting\Reference\LicensingGroupController@save')->name('group-perizinan-save');
				});
				Route::group(['middleware' => 'roleu:pengaturan/refrensi'], function () {
					Route::post('update-group-perizinan','Core\Setting\Reference\LicensingGroupController@update')->name('group-perizinan-update');
				});
				Route::group(['middleware' => 'roled:pengaturan/refrensi'], function () {
					Route::get('delete-group-perizinan','Core\Setting\Reference\LicensingGroupController@delete')->name('group-perizinan-delete');
				});
			});
			Route::prefix('upt')->group(function(){
				Route::group(['middleware' => 'roler:pengaturan/upt'], function () {
					Route::get('/','Core\Setting\Upt\UptController@index')->name('pengaturan/upt');
					Route::get('print','Core\Setting\Upt\UptController@print')->name('upt-print');
					Route::get('download','Core\Setting\Upt\UptController@export')->name('upt-export');
				});
			});

		});

		Route::prefix('pelayanan')->group(function(){
			Route::prefix('unar')->group(function(){
				Route::group(['middleware' => 'roler:pelayanan/unar'], function () {
					Route::get('/','Core\Pelayanan\UNAR\UNARController@index')->name('pelayanan/unar');
					Route::get('search','Core\Pelayanan\UNAR\UNARController@search')->name('unar-search');
					Route::get('searchMultiple/{id}','Core\Pelayanan\UNAR\UNARController@searchMultiple')->name('unar-search-multiple');
					Route::get('print','Core\Pelayanan\UNAR\UNARController@print')->name('unar-print');
					Route::get('print/{id}','Core\Pelayanan\UNAR\UNARController@print_search')->name('unar-print-search');
					Route::get('download','Core\Pelayanan\UNAR\UNARController@export')->name('unar-export');
					Route::get('download/{id}','Core\Pelayanan\UNAR\UNARController@export_search')->name('unar-export-search');

					Route::get('sampel','Core\Pelayanan\UNAR\UNARController@downloadSampel')->name('unar-sampel');
					Route::post('import/previews','Core\Pelayanan\UNAR\UNARController@importPreview')->name('unar-import-preview');
					Route::post('import','Core\Pelayanan\UNAR\UNARController@importPost')->name('unar-import-post');
				});
				Route::group(['middleware' => 'rolec:pelayanan/unar'], function () {
					Route::post('save','Core\Pelayanan\UNAR\UNARController@save')->name('unar-save');	
				});
				Route::group(['middleware' => 'roleu:pelayanan/unar'], function () {
					Route::post('update','Core\Pelayanan\UNAR\UNARController@update')->name('unar-update');
					Route::post('update-rejected','Core\Pelayanan\UNAR\UNARController@updateRejected')->name('unar-reject-update');
					Route::post('approved','Core\Pelayanan\UNAR\UNARController@approved')->name('unar-approved');
					Route::get('reupload/{id}','Core\Pelayanan\UNAR\UNARController@reupload')->name('unar-reupload');
				});	
				Route::group(['middleware' => 'roled:pelayanan/unar'], function () {
					Route::get('delete','Core\Pelayanan\UNAR\UNARController@delete')->name('unar-delete');
				});
			});

			Route::prefix('penanganan-piutang')->group(function(){
				Route::group(['middleware' => 'roler:pelayanan/penanganan-piutang'], function () {
					Route::get('/','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@index')->name('pelayanan/penanganan-piutang');
					Route::get('search','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@search')->name('pelayanan/penanganan-piutang');
					Route::get('searchMultiple/{id}','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@searchMultiple')->name('penanganan-piutang-search-multiple');
					Route::get('print','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@print')->name('penanganan-piutang-print');
					Route::get('print/{id}','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@print_search')->name('penanganan-piutang-print-search');
					Route::get('download','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@export')->name('penanganan-piutang-export');
					Route::get('download/{id}','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@export_search')->name('penanganan-piutang-export-search');
					Route::post('import/previews','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@importPreview')->name('penanganan-piutang-import-preview');
					Route::post('import','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@importPost')->name('penanganan-piutang-import-post');
					Route::get('sampel','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@downloadSampel')->name('penanganan-piutang-sampel');
				});
				Route::group(['middleware' => 'rolec:pelayanan/penanganan-piutang'], function () {
					Route::post('save','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@save')->name('penanganan-piutang-save');	
				});
				Route::group(['middleware' => 'roleu:pelayanan/penanganan-piutang'], function () {
					Route::post('update','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@update')->name('penanganan-piutang-update');
					Route::post('update-rejected','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@updateRejected')->name('penanganan-piutang-reject-update');
					Route::post('approved','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@approved')->name('penanganan-piutang-approved');
					Route::get('reupload/{id}','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@reupload')->name('penanganan-piutang-reupload');
				});	
				Route::group(['middleware' => 'roled:pelayanan/penanganan-piutang'], function () {
					Route::get('delete','Core\Pelayanan\PenangananPiutang\PenangananPiutangController@delete')->name('penanganan-piutang-delete');
				});
			});

		});

		Route::prefix('sosialisasi-bimtek')->group(function(){
			Route::prefix('bahan-sosialisasi')->group(function(){
				Route::group(['middleware' => 'roler:sosialisasi-bimtek/bahan-sosialisasi'], function () {
					Route::get('/','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@index')->name('sosialisasi-bimtek/bahan-sosialisasi');
					Route::get('search','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@search')->name('sosialisasi-bimtek/bahan-sosialisasi');
					Route::get('searchMultiple/{id}','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@searchMultiple')->name('bahsos-search-multiple');
					Route::get('/lampiran/{filename}', function ($filename)
					{
						$path = public_path('lampiran/bahansosialisasi') . '/' . $filename;
						$extension = $extension = pathinfo(public_path('lampiran/bahansosialisasi') . '/' . $filename, PATHINFO_EXTENSION);
						$file = File::get($path);
						$type = File::mimeType($path);
						$response = Response::make($file);
						$headers = ["Content-Type" => $type];
						$response->header("Content-Type", $type);
						return Response::download($path, 'Lampiran Bahan Sosialisasi.'.$extension, $headers);
					})->name('lampiran-bahansosialisasi');
					Route::get('print','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@print')->name('bahsos-print');
					Route::get('print/{id}','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@print_search')->name('bahsos-print-search');
					Route::get('download','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@export')->name('bahsos-export');
					Route::get('sampel','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@downloadSampel')->name('bahsos-sampel');
					Route::post('import/previews','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@importPreview')->name('bahsos-import-preview');
					Route::post('import','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@importPost')->name('bahsos-import-post');
					Route::get('download/{id}','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@export_search')->name('bahsos-export-search');
				});
				Route::group(['middleware' => 'rolec:sosialisasi-bimtek/bahan-sosialisasi'], function () {
					Route::post('save','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@save')->name('bahsos-save');
				});
				Route::group(['middleware' => 'roleu:sosialisasi-bimtek/bahan-sosialisasi'], function () {
					Route::post('update','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@update')->name('bahsos-update');
				});
				Route::group(['middleware' => 'roled:sosialisasi-bimtek/bahan-sosialisasi'], function () {
					Route::get('delete','Core\TeknologiBimbinganSosialisasi\BahanSosialisasi\BahanSosialisasiController@delete')->name('bahsos-delete');
				});
			});
		});	

		Route::prefix('spp')->group(function(){
			Route::prefix('rt')->group(function(){
				Route::group(['middleware' => 'roler:spp/rt'], function () {
					Route::get('/','Core\SPP\RincianTagihanController@index')->name('spp/rt');
					Route::get('search_query','Core\SPP\RincianTagihanController@search_query')->name('spp/rt');
					Route::get('search','Core\SPP\RincianTagihanController@search')->name('spp-rt-search');	
					Route::get('searchMultiple/{id}','Core\SPP\RincianTagihanController@searchMultiple')->name('spp-rt-search-multiple');	
					Route::get('/lampiran/{filename}', function ($filename)
					{
						$path = public_path('lampiran/spp/rt') . '/' . $filename;
						$file = File::get($path);
						$type = File::mimeType($path);
						$response = Response::make($file);
						$response->header("Content-Type", $type);
						return $response;
					})->name('bukti-dukung-spp-rt');

					Route::post('preview','Core\SPP\RincianTagihanController@preview')->name('rt-preview');						
					Route::post('preview_search','Core\SPP\RincianTagihanController@preview_search')->name('rt-preview-search');
					Route::post('print','Core\SPP\RincianTagihanController@print')->name('rt-print');						
					Route::post('print_search','Core\SPP\RincianTagihanController@print_search')->name('rt-print-search');
					Route::post('download','Core\SPP\RincianTagihanController@export')->name('rt-export');
					Route::post('download_search','Core\SPP\RincianTagihanController@export_search')->name('rt-export-search');

					Route::post('import/previews','Core\SPP\RincianTagihanController@importPreview')->name('rt-import-preview');
					Route::post('import','Core\SPP\RincianTagihanController@importPost')->name('rt-import-post');
					Route::get('sampel','Core\SPP\RincianTagihanController@downloadSampel')->name('rt-sampel');
				});
				Route::group(['middleware' => 'rolec:spp/rt'], function () {
					Route::post('postORupdate','Core\SPP\RincianTagihanController@postORupdate')->name('spp-rt-postORupdate');	
				});
				Route::group(['middleware' => 'roleu:spp/rt'], function () {
					Route::post('approved','Core\SPP\RincianTagihanController@approved')->name('spp-rt-approved');
					Route::post('ORUpdate','Core\SPP\RincianTagihanController@ORUpdate')->name('spp-rt-orupdate');
				});
				
			});

			Route::prefix('st')->group(function(){
			Route::group(['middleware' => 'roler:spp/st'], function () {
				Route::get('/','Core\SPP\StatusTagihan@index')->name('spp/st');
				Route::get('search_query','Core\SPP\StatusTagihan@search_query')->name('spp/st');
				Route::get('search','Core\SPP\StatusTagihan@search')->name('spp-rt-search');	
				Route::get('searchMultiple/{id}','Core\SPP\StatusTagihan@searchMultiple')->name('spp-st-search-multiple');	
				Route::get('/lampiran/{filename}', function ($filename)
				{
					$path = public_path('lampiran/spp/st') . '/' . $filename;
					$file = File::get($path);
					$type = File::mimeType($path);
					$response = Response::make($file);
					$response->header("Content-Type", $type);
					return $response;
				})->name('bukti-dukung-spp-st');

				Route::post('preview','Core\SPP\StatusTagihan@preview')->name('st-preview');						
				Route::post('preview_search','Core\SPP\StatusTagihan@preview_search')->name('st-preview-search');
				Route::post('print','Core\SPP\StatusTagihan@print')->name('st-print');
				Route::post('print_search','Core\SPP\StatusTagihan@print_search')->name('st-print-search');
				Route::post('download','Core\SPP\StatusTagihan@export')->name('st-export');
				Route::post('download_search','Core\SPP\StatusTagihan@export_search')->name('st-export-search');

				Route::post('import/previews','Core\SPP\StatusTagihan@importPreview')->name('st-import-preview');
				Route::post('import','Core\SPP\StatusTagihan@importPost')->name('st-import-post');
				Route::get('sampel','Core\SPP\StatusTagihan@downloadSampel')->name('st-sampel');
			});
			Route::group(['middleware' => 'rolec:spp/st'], function () {
				Route::post('postORupdate','Core\SPP\StatusTagihan@postORupdate')->name('spp-st-postORupdate');	
			});
			Route::group(['middleware' => 'roleu:spp/st'], function () {
				Route::post('approved','Core\SPP\StatusTagihan@approved')->name('spp-st-approved');
				Route::post('ORUpdate','Core\SPP\StatusTagihan@ORUpdate')->name('spp-st-orupdate');
			});

			});

		});


	});
});
