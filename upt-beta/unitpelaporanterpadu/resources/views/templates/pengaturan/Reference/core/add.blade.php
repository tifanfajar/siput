<div class="modal fade" id="addRefrensi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">TAMBAH KATEGORI SPP</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('kategori-spp-save')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<div class="col-md-12 form-group">
						<label for="kategori_spp">Kategori SPP </label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="kategori_spp" name="kategori_spp" placeholder="Masukan kategori SPP" required>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>		
	</div>
</div>

<div class="modal fade" id="addMateri" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<!-- Materi -->
		<div class="modal-content" style="width: 700px;">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">TAMBAH MATERI</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('materi-save')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<div class="col-md-12 form-group">
						<label for="jenis_materi">Jenis Materi </label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="jenis_materi" name="jenis_materi" placeholder="Masukan Materi" required>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="addMetode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<!-- Metode -->
		<div class="modal-content" style="width: 700px;">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">TAMBAH METODE</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('metode-save')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<div class="col-md-12 form-group">
						<label for="metode">Metode </label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="metode" name="metode" placeholder="Masukan Metode" required>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="addKegiatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<!-- Kegiatan -->
		<div class="modal-content" style="width: 700px;">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">TAMBAH KEGIATAN</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('kegiatan-save')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<div class="col-md-12 form-group">
						<label for="kegiatan">Kegiatan </label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="kegiatan" name="kegiatan" placeholder="Masukan Kegiatan" required>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="addGroupPerizinan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<!-- Group Perizinan -->
		<div class="modal-content" style="width: 700px;">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">TAMBAH JENIS</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('group-perizinan-save')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<div class="col-md-12 form-group">
						<label for="jenis">Jenis </label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="jenis" name="jenis" placeholder="Masukan Jenis" required>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="addTujuan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<!-- Tujuan -->
		<div class="modal-content" style="width: 700px;">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">TAMBAH TUJUAN</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('tujuan-save')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<div class="col-md-12 form-group">
						<label for="tujuan">Kategori SPP </label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="tujuan" name="tujuan" placeholder="Masukan Tujuan" required>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
