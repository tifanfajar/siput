@extends('dev.core.using')
@section('content')

@include('templates.pengaturan.Reference.core.add')
@include('templates.pengaturan.Reference.core.detail')
@include('templates.pengaturan.Reference.core.edit')
@include('templates.helpers.delete')
@include('templates.helpers.export')
@include('templates.helpers.import')
@include('templates.pengaturan.Reference.dev.data')


<style>
	#example_filter{position: absolute; right: 20px;}
</style>
@include('templates.pengaturan.Reference.core.kategori')
<br>
@include('templates.pengaturan.Reference.core.materi')
<br>
@include('templates.pengaturan.Reference.core.kegiatan')
<br>
@include('templates.pengaturan.Reference.core.metode')
<br>
@include('templates.pengaturan.Reference.core.tujuan')
<br>
@include('templates.pengaturan.Reference.core.group-perizinan')

@endsection