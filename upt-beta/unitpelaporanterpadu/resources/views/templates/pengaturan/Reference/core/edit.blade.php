<!-- Kategori -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header">
				<img src="http://localhost:8000/templates/assets/img/edit.svg" alt="" style="width: 30px; height: 30px; margin-bottom: 2px; padding-right: 2px; ">
				<h5 class="modal-title" id="exampleModalLabel" style="margin-top: 10px; padding-left: 10px;">Ubah Kategori SPP</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('kategori-spp-update')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="eid">
					<div class="col-md-12 form-group">
						<label for="kategori_spp">Kategori SPP *</label>
						<input type="text" id="ekategori_spp" class="form-control" name="kategori_spp" required>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Group Perizinan -->
<div class="modal fade" id="eGroupPerizinan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="http://localhost:8000/templates/assets/img/edit.svg" alt="" style="width: 30px; height: 30px; margin-bottom: 2px; padding-right: 2px; ">
				<h5 class="modal-title" id="exampleModalLabel" style="margin-top: 10px; padding-left: 10px;">Ubah Jenis</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('group-perizinan-update')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="eidGroupPerizinan">
					<div class="col-md-12 form-group">
						<label for="jenis">Jenis *</label>
						<input type="text" id="ejenis" class="form-control" name="jenis" placeholder="Masukan Jenis" required>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Kegiatan -->
<div class="modal fade" id="eKegiatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="http://localhost:8000/templates/assets/img/edit.svg" alt="" style="width: 30px; height: 30px; margin-bottom: 2px; padding-right: 2px; ">
				<h5 class="modal-title" id="exampleModalLabel" style="margin-top: 10px; padding-left: 10px;">Ubah Kegiatan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('kegiatan-update')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="eidkegiatan">
					<div class="col-md-12 form-group">
						<label for="kegiatan">Kegiatan *</label>
						<input type="text" id="ekegiatan" class="form-control" name="kegiatan" placeholder="Masukan Kegiatan" required>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Materi -->
<div class="modal fade" id="emateri" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="http://localhost:8000/templates/assets/img/edit.svg" alt="" style="width: 30px; height: 30px; margin-bottom: 2px; padding-right: 2px; ">
				<h5 class="modal-title" id="exampleModalLabel" style="margin-top: 10px; padding-left: 10px;">Ubah Jenis Materi</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('materi-update')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="eidjenis_materi">
					<div class="col-md-12 form-group">
						<label for="jenis_materi">Jenis Materi *</label>
						<input type="text" id="ejenis_materi" class="form-control" name="jenis_materi" placeholder="Masukan Jenis Materi" required>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Metode -->
<div class="modal fade" id="eMetode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="http://localhost:8000/templates/assets/img/edit.svg" alt="" style="width: 30px; height: 30px; margin-bottom: 2px; padding-right: 2px; ">
				<h5 class="modal-title" id="exampleModalLabel" style="margin-top: 10px; padding-left: 10px;">Ubah Metode</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('metode-update')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="eidmetode">
					<div class="col-md-12 form-group">
						<label for="metode">Metode *</label>
						<input type="text" id="emetode" class="form-control" name="metode" placeholder="Masukan Jenis" required>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- Tujuan -->
<div class="modal fade" id="eTujuan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="http://localhost:8000/templates/assets/img/edit.svg" alt="" style="width: 30px; height: 30px; margin-bottom: 2px; padding-right: 2px; ">
				<h5 class="modal-title" id="exampleModalLabel" style="margin-top: 10px; padding-left: 10px;">Ubah Tujuan</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('tujuan-update')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="eid">
					<div class="col-md-12 form-group">
						<label for="tujuan">Tujuan *</label>
						<input type="text" id="etujuan" class="form-control" name="tujuan" placeholder="Masukan Jenis" required>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>