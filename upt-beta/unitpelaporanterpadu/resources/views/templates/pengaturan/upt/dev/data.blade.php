@include('dev.helpers.jquery')
<script type="text/javascript">
	$(document).ready(function(){
		$('#detailModal').on('show.bs.modal', function (e) {
			var nama=$(e.relatedTarget).attr('data-nama');
			$('#dnama').val(nama);
			var telp=$(e.relatedTarget).attr('data-telp');
			$('#dtelp').val(telp); 
			var fax=$(e.relatedTarget).attr('data-fax');
			$('#dfax').val(fax); 
			var email=$(e.relatedTarget).attr('data-email');
			$('#demail').val(email);
			var alamat=$(e.relatedTarget).attr('data-alamat');
			$('#dalamat').val(alamat);
			var created=$(e.relatedTarget).attr('data-created');
			$('#dcreated').val(created);
			var updated=$(e.relatedTarget).attr('data-updated');
			$('#dupdated').val(updated);
			var provinsi=$(e.relatedTarget).attr('data-provinsi');
			$('#dprovinsi').val(provinsi);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#editModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#eid').val(id);
			var nama=$(e.relatedTarget).attr('data-nama');
			$('#enama').val(nama);
			var telp=$(e.relatedTarget).attr('data-telp');
			$('#etelp').val(telp); 
			var fax=$(e.relatedTarget).attr('data-fax');
			$('#efax').val(fax); 
			var email=$(e.relatedTarget).attr('data-email');
			$('#eemail').val(email);
			var alamat=$(e.relatedTarget).attr('data-alamat');
			$('#ealamat').val(alamat);
			var created=$(e.relatedTarget).attr('data-created');
			$('#ecreated').val(created);
			var updated=$(e.relatedTarget).attr('data-updated');
			$('#eupdated').val(updated);
			var provinsi=$(e.relatedTarget).attr('data-provinsi');
			var idprovinsi=$(e.relatedTarget).attr('data-provinsiid');
			$('#eprovinsi').append('<option value="'+ idprovinsi +'" selected>'+ provinsi +'</option>');
		});
	})
</script>