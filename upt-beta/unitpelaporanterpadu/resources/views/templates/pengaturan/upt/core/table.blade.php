@include('templates.helpers.data')
<table id="example" class="table table-striped table-dark align-items-center table-flush data-table dataTable no-footer">
	<thead class="thead-light">
		<tr>
			<th scope="col">Office ID</th>
			<th scope="col">Office Name</th>
			<th scope="col">District Name</th>
			<th scope="col">Province Name</th>
			<th scope="col">Zone</th>
		</tr>
	</thead>
	<tbody>
		@foreach($upt as $upts)
		<tr>
			<td>{{$upts->office_id}}</td>
			<td>{{$upts->office_name}}</td>
			<td>{{$upts->district_name}}</td>
			<td>{{$upts->province_name}}</td>
			<td>{{$upts->zone}}</td>
		</tr>
		@endforeach
	</tbody>
</table>