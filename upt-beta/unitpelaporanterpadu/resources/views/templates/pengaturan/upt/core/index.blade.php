@extends('dev.core.using')
@section('content')

@include('templates.pengaturan.upt.core.detail')
@include('templates.helpers.delete')
@include('templates.helpers.import')
@include('templates.helpers.export')
@include('templates.pengaturan.upt.dev.data')

<style>
	#example_filter{position: absolute; right: 20px;}
</style>
<div class="row mt-5">
	<div class="col text-left" style="padding-bottom: 10px;">
		<a class="btn btn-sm btn-info" style="color: white;" id="getPrint" data-toggle="modal" data-target="#printModal"><i class="ni ni-single-copy-04"></i>&nbsp;DOWNLOAD AS PDF</a>
		<a class="btn btn-sm btn-danger" style="color: white;" id="getDownload" data-toggle="modal" data-target="#downloadModal"><i class="ni ni-cloud-download-95"></i>&nbsp;DOWNLOAD AS XLS </a>
	</div>


	<div class="col-xl-12 mb-5 mb-xl-0">
		<div class="card shadow" style="border:1px solid #dedede;">
			<div class="card-header border-0" style="background-color: #5f5f5f;">
				<div class="row align-items-center">
					<div class="col">
						<h3 class="mb-0" style="color: #fff;"><i class="ni ni-archive-2"></i>&nbsp;&nbsp;Daftar UPT</h3>
					</div>
				</div>
			</div>
			<div class="table-responsive">
				<!-- Projects table -->
				<div class="widest">
					@include('templates.pengaturan.upt.core.table')
				</div>
			</div>
		</div>
	</div>

	
</div>




@endsection