<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Detail UPT</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="#" method="POST" class="row" enctype="multipart/enctype">
					@csrf
					<div class="col-md-6 form-group">
						<label for="namaupt">Nama UPT *</label>
						<input type="text" id="dnama" class="form-control" name="nama" placeholder="Masukan Nama UPT" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="fax">Fax</label>
						<input type="text" class="form-control" id="dfax" name="fax" placeholder="Masukan Fax UPT" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="telp">No. Telp</label>
						<input type="text" class="form-control" id="dtelp" name="telp" placeholder="Masukan No.Telp UPT" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="Email">Email</label>
						<input type="email" class="form-control" id="demail" name="email" placeholder="Masukan Email UPT" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="provinsi">Provinsi</label>
						<input type="text" class="form-control" id="dprovinsi" name="provinsi" placeholder="Masukan Provinsi UPT" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="alamat">Alamat *</label>
						<textarea class="form-control" id="dalamat" name="alamat" rows="3" placeholder="Masukan Alamat UPT" disabled></textarea>
					</div>
					<div class="col-md-6 form-group">
						<label for="created">Dibuat Pada</label>
						<input type="text" class="form-control" id="dcreated" name="provinsi" placeholder="Masukan Tanggal Buat UPT" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="updated">Di Perbaharui Pada</label>
						<input type="text" class="form-control" id="dupdated" name="provinsi" placeholder="Masukan Tanggal Update UPT" disabled>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>