@extends('dev.core.using')
@section('content')
<br>
<br>
<form action="{{route('hak-akses-save')}}" class="row" method="POST" enctype="multipart/form-data">
	@csrf
	<div class="col-md-4 form-group">
		<label for="role_name">Hak Akses </label><label style="color: red;">*</label>
		<input type="text" class="form-control" id="role_name" name="role_name" placeholder="Masukan Nama Hak Akses" required>
	</div>
	<div class="col-md-4 form-group">
		<label for="description">Deskripsi </label><label style="color: red;">*</label>
		<input type="text" class="form-control" id="description" name="description" placeholder="Masukan Deskripsi" required>
	</div>
	<div class="col-md-4 form-group">
		<label for="description">Tipe Akses </label><label style="color: red;">*</label>
		<select class="form-control" name="akses" required>
			<option selected disabled>Pilih salah satu</option>
			<option value="administrator">Administrator</option>
			<option value="kepala-upt">Kepala UPT</option>
			<option value="operator">Operator</option>
		</select>
	</div>
	
	<table class="table  align-items-center">
		<thead class="thead-light">
			<tr>
				<th scope="col">Module</th>
				<th scope="col">Create</th>
				<th scope="col">Read</th>
				<th scope="col">Update</th>
				<th scope="col">Delete</th>
				<th scope="col">Check All</th>
			</tr>
		</thead>
		<tbody>
			<tr id='minimal-checkbox-10001'>
				<td>Pelayanan - Loket Pengaduan</td>
				<td><input name="10001_create" type="checkbox" id="minimal-checkbox-10001" value="10001"></td>
				<td><input name="10001_read" type="checkbox" id="minimal-checkbox-10001" value="10001"></td>
				<td><input name="10001_update" type="checkbox" id="minimal-checkbox-999999999" value="10001"></td>
				<td><input name="10001_delete" type="checkbox" id="minimal-checkbox-999999999" value="10001"></td>
				<td><input type="checkbox" id="all-10001" value="all" name="all" onChange="check(10001)" /></td>
			</tr>
			<tr id='minimal-checkbox-10006'>
				<td>Pelayanan - UNAR</td>
				<td><input name="10006_create" type="checkbox" id="minimal-checkbox-10006" value="10006"></td>
				<td><input name="10006_read" type="checkbox" id="minimal-checkbox-10006" value="10006"></td>
				<td><input name="10006_update" type="checkbox" id="minimal-checkbox-10006" value="10006"></td>
				<td><input name="10006_delete" type="checkbox" id="minimal-checkbox-10006" value="10006"></td>
				<td><input type="checkbox" id="all-10006" value="all" name="all" onChange="check(10006)" /></td>
			</tr>
			<tr id='minimal-checkbox-10004'>
				<td>SPP - ST</td>
				<td><input name="10004_create" type="checkbox" id="minimal-checkbox-10004" value="10004"></td>
				<td><input name="10004_read" type="checkbox" id="minimal-checkbox-10004" value="10004"></td>
				<td><input name="10004_update" type="checkbox" id="minimal-checkbox-10004" value="10004"></td>
				<td><input name="10004_delete" type="checkbox" id="minimal-checkbox-10004" value="10004"></td>
				<td><input type="checkbox" id="all-10004" value="all" name="all" onChange="check(10004)" /></td>
			</tr>
			<tr id='minimal-checkbox-10003'>
				<td>SPP - RT</td>
				<td><input name="10003_create" type="checkbox" id="minimal-checkbox-10003" value="10003"></td>
				<td><input name="10003_read" type="checkbox" id="minimal-checkbox-10003" value="10003"></td>
				<td><input name="10003_update" type="checkbox" id="minimal-checkbox-10003" value="10003"></td>
				<td><input name="10003_delete" type="checkbox" id="minimal-checkbox-10003" value="10003"></td>
				<td><input type="checkbox" id="all-10003" value="all" name="all" onChange="check(10003)" /></td>
			</tr>
			<tr id='minimal-checkbox-10005'>
				<td>Pelayanan - Penanganan Piutang</td>
				<td><input name="10005_create" type="checkbox" id="minimal-checkbox-10005" value="10005"></td>
				<td><input name="10005_read" type="checkbox" id="minimal-checkbox-10005" value="10005"></td>
				<td><input name="10005_update" type="checkbox" id="minimal-checkbox-10005" value="10005"></td>
				<td><input name="10005_delete" type="checkbox" id="minimal-checkbox-10005" value="10005"></td>
				<td><input type="checkbox" id="all-10005" value="all" name="all" onChange="check(10005)" /></td>
			</tr>
			<tr id='minimal-checkbox-10007'>
				<td>Sosialisasi & Bimtek - Bahan Sosialisasi</td>
				<td><input name="10007_create" type="checkbox" id="minimal-checkbox-10007" value="10007"></td>
				<td><input name="10007_read" type="checkbox" id="minimal-checkbox-10007" value="10007"></td>
				<td><input name="10007_update" type="checkbox" id="minimal-checkbox-10007" value="10007"></td>
				<td><input name="10007_delete" type="checkbox" id="minimal-checkbox-10007" value="10007"></td>
				<td><input type="checkbox" id="all-10007" value="all" name="all" onChange="check(10007)" /></td>
			</tr>
			<tr id='minimal-checkbox-10008'>
				<td>Sosialisasi & Bimtek - Rencana Sosialisasi</td>
				<td><input name="10008_create" type="checkbox" id="minimal-checkbox-10008" value="10008"></td>
				<td><input name="10008_read" type="checkbox" id="minimal-checkbox-10008" value="10008"></td>
				<td><input name="10008_update" type="checkbox" id="minimal-checkbox-10008" value="10008"></td>
				<td><input name="10008_delete" type="checkbox" id="minimal-checkbox-10008" value="10008"></td>
				<td><input type="checkbox" id="all-10008" value="all" name="all" onChange="check(10008)" /></td>
			</tr>
			<tr id='minimal-checkbox-10009'>
				<td>Sosialisasi & Bimtek - Monev Sosialisasi</td>
				<td><input name="10009_create" type="checkbox" id="minimal-checkbox-10009" value="10009"></td>
				<td><input name="10009_read" type="checkbox" id="minimal-checkbox-10009" value="10009"></td>
				<td><input name="10009_update" type="checkbox" id="minimal-checkbox-10009" value="10009"></td>
				<td><input name="10009_delete" type="checkbox" id="minimal-checkbox-10009" value="10009"></td>
				<td><input type="checkbox" id="all-10009" value="all" name="all" onChange="check(10009)" /></td>
			</tr>
			<tr id='minimal-checkbox-100010'>
				<td>Sosialisasi & Bimtek - Galeri</td>
				<td><input name="100010_create" type="checkbox" id="minimal-checkbox-100010" value="100010"></td>
				<td><input name="100010_read" type="checkbox" id="minimal-checkbox-100010" value="100010"></td>
				<td><input name="100010_update" type="checkbox" id="minimal-checkbox-100010" value="100010"></td>
				<td><input name="100010_delete" type="checkbox" id="minimal-checkbox-100010" value="100010"></td>
				<td><input type="checkbox" id="all-100010" value="all" name="all" onChange="check(100010)" /></td>
			</tr>
			<tr id='minimal-checkbox-100011'>
				<td>Inspeksi - Inspeksi Data</td>
				<td><input name="100011_create" type="checkbox" id="minimal-checkbox-100011" value="100011"></td>
				<td><input name="100011_read" type="checkbox" id="minimal-checkbox-100011" value="100011"></td>
				<td><input name="100011_update" type="checkbox" id="minimal-checkbox-100011" value="100011"></td>
				<td><input name="100011_delete" type="checkbox" id="minimal-checkbox-100011" value="100011"></td>
				<td><input type="checkbox" id="all-100011" value="all" name="all" onChange="check(100011)" /></td>
			</tr>
			<tr id='minimal-checkbox-100012'>
				<td>Pengaturan - Daftar Pengguna</td>
				<td><input name="100012_create" type="checkbox" id="minimal-checkbox-100012" value="100012"></td>
				<td><input name="100012_read" type="checkbox" id="minimal-checkbox-100012" value="100012"></td>
				<td><input name="100012_update" type="checkbox" id="minimal-checkbox-100012" value="100012"></td>
				<td><input name="100012_delete" type="checkbox" id="minimal-checkbox-100012" value="100012"></td>
				<td><input type="checkbox" id="all-100012" value="all" name="all" onChange="check(100012)" /></td>
			</tr>
			<tr id='minimal-checkbox-100013'>
				<td>Pengaturan - Hak Akses</td>
				<td><input name="100013_create" type="checkbox" id="minimal-checkbox-100013" value="100013"></td>
				<td><input name="100013_read" type="checkbox" id="minimal-checkbox-100013" value="100013"></td>
				<td><input name="100013_update" type="checkbox" id="minimal-checkbox-100013" value="100013"></td>
				<td><input name="100013_delete" type="checkbox" id="minimal-checkbox-100013" value="100013"></td>
				<td><input type="checkbox" id="all-100013" value="all" name="all" onChange="check(100013)" /></td>
			</tr>
			<tr id='minimal-checkbox-100014'>
				<td>Pengaturan - UPT</td>
				<td><input name="100014_create" type="checkbox" id="minimal-checkbox-100014" value="100014"></td>
				<td><input name="100014_read" type="checkbox" id="minimal-checkbox-100014" value="100014"></td>
				<td><input name="100014_update" type="checkbox" id="minimal-checkbox-100014" value="100014"></td>
				<td><input name="100014_delete" type="checkbox" id="minimal-checkbox-100014" value="100014"></td>
				<td><input type="checkbox" id="all-100014" value="all" name="all" onChange="check(100014)" /></td>
			</tr>
			<tr id='minimal-checkbox-100015'>
				<td>Pengaturan - Refrensi</td>
				<td><input name="100015_create" type="checkbox" id="minimal-checkbox-100015" value="100015"></td>
				<td><input name="100015_read" type="checkbox" id="minimal-checkbox-100015" value="100015"></td>
				<td><input name="100015_update" type="checkbox" id="minimal-checkbox-100015" value="100015"></td>
				<td><input name="100015_delete" type="checkbox" id="minimal-checkbox-100015" value="100015"></td>
				<td><input type="checkbox" id="all-100015" value="all" name="all" onChange="check(100015)" /></td>
			</tr>
			<tr id='minimal-checkbox-100016'>
				<td>Pengaturan - Backup Database</td>
				<td><input name="100016_create" type="checkbox" id="minimal-checkbox-100016" value="100016"></td>
				<td><input name="100016_read" type="checkbox" id="minimal-checkbox-100016" value="100016"></td>
				<td><input name="100016_update" type="checkbox" id="minimal-checkbox-100016" value="100016"></td>
				<td><input name="100016_delete" type="checkbox" id="minimal-checkbox-100016" value="100016"></td>
				<td><input type="checkbox" id="all-100016" value="all" name="all" onChange="check(100016)" /></td>
			</tr>
			<tr id='minimal-checkbox-100017'>
				<td>Pengaturan - Perusahaan</td>
				<td><input name="100017_create" type="checkbox" id="minimal-checkbox-100017" value="100017"></td>
				<td><input name="100017_read" type="checkbox" id="minimal-checkbox-100017" value="100017"></td>
				<td><input name="100017_update" type="checkbox" id="minimal-checkbox-100017" value="100017"></td>
				<td><input name="100017_delete" type="checkbox" id="minimal-checkbox-100017" value="100017"></td>
				<td><input type="checkbox" id="all-100017" value="all" name="all" onChange="check(100017)" /></td>
			</tr>
		</tbody>
	</table>
	@include('dev.helpers.button.btnGroupFormAction')
</form>
<script>

$('input[type="checkbox"]').css('cursor','pointer');

function check(no) {
            document.getElementById('minimal-checkbox-'+no).addEventListener('change', function(e) {
                var el = e.target;
                var inputs = document.getElementById('minimal-checkbox-'+no).getElementsByTagName('input');

                if (el.id === 'all-'+no) {
                    for (var i = 0, input; input = inputs[i++]; ) {
                        input.checked = el.checked;
                    }
                } else {
                    var numChecked = 0;

                    for (var i = 1, input; input = inputs[i++]; ) {
                        if (input.checked) {
                            numChecked++;
                        }
                    }
                    inputs[0].checked = numChecked === inputs.length - 1;
                }
            }, false);
        }
</script>
@endsection