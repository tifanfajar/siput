@include('templates.helpers.data')
<table id="example" class="table table-striped table-dark align-items-center table-flush data-table dataTable no-footer">
	<thead class="thead-light">
		<tr>
			<th scope="col">No.Lisensi</th>
			<th scope="col">Nama</th>
			<th scope="col">No.Telp</th>
			<th scope="col">No.HP</th>
			<th scope="col">Provinsi</th>
			<th scope="col">Kota/Kabupaten</th>
			<th scope="col">Alamat</th>
			<th scope="col">Tindakan</th>
		</tr>
	</thead>
	<tbody>
		@foreach($company as $companys)
		<tr>
			<td>{{$companys->company_id}}</td>
			<td>{{$companys->name}}</td>
			<td>{{$companys->no_telp}}</td>
			<td>{{$companys->no_hp}}</td>
			<td>{{\App\Model\Region\Provinsi::where('id',$companys->id_prov)->value('nama')}}</td>
			<td>{{\App\Model\Region\KabupatenKota::where('id',$companys->id_kabkot)->value('nama')}}</td>
			<td>{{$companys->alamat}}</td>
			<td>
				<?php $id = Crypt::encryptString($companys->id) ?>
				<?php
				$url = \Request::route()->getName();
				$getKdModule = \App\Model\Menu\Module::where('menu_path',$url)->value('kdModule');
				$getEdit = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
				->value('update_acl');
				$getDelete = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
				->value('delete_acl');
				?>
				<a class="btn btn-success btn-sm" id="getDetail" data-toggle="modal" data-target="#detailModal" data-name="{{$companys->name}}" data-telp="{{$companys->no_telp}}" data-lisensi="{{$companys->company_id}}" data-hp="{{$companys->no_hp}}" data-alamat="{{$companys->alamat}}" data-provinsi="{{\App\Model\Region\Provinsi::where('id',$companys->id_prov)->value('nama')}}" data-kabupaten="{{\App\Model\Region\KabupatenKota::where('id',$companys->id_kabkot)->value('nama')}}"><i class="fa fa-file"></i></a>
				@if($getEdit == $getKdModule)
				<a  class="btn btn-warning btn-sm" id="getEdit" data-toggle="modal" data-target="#editModal" data-id="{{$id}}" data-name="{{$companys->name}}" data-lisensi="{{$companys->company_id}}" data-telp="{{$companys->no_telp}}" data-hp="{{$companys->no_hp}}" data-alamat="{{$companys->alamat}}" data-provinsi="{{\App\Model\Region\Provinsi::where('id',$companys->id_prov)->value('nama')}}" data-provinsiid="{{$companys->id_prov}}" data-kabupatenid="{{$companys->id_kabkot}}" data-kabupaten="{{\App\Model\Region\KabupatenKota::where('id',$companys->id_kabkot)->value('nama')}}"><i class="fa fa-edit"></i></a>
				@endif
				@if($getDelete == $getKdModule)
				<a  class="btn  btn-danger btn-sm" id="getDelete" data-toggle="modal" data-target="#deleteModal" data-id="{{$id}}"><i class="fa fa-trash"></i></a>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>