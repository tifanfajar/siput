@include('dev.helpers.jquery')
<script type="text/javascript">
	$(document).ready(function(){
		$('#detailModal').on('show.bs.modal', function (e) {
			var lisensi=$(e.relatedTarget).attr('data-lisensi');
			$('#dlisensi').val(lisensi);
			var name=$(e.relatedTarget).attr('data-name');
			$('#dname').val(name);
			var telp=$(e.relatedTarget).attr('data-telp');
			$('#dtelp').val(telp); 
			var fax=$(e.relatedTarget).attr('data-hp');
			$('#dhp').val(fax); 
			var alamat=$(e.relatedTarget).attr('data-alamat');
			$('#dalamat').val(alamat);
			var provinsi=$(e.relatedTarget).attr('data-provinsi');
			$('#dprovinsi').val(provinsi);
			var kabupaten=$(e.relatedTarget).attr('data-kabupaten');
			$('#dkabupaten').val(kabupaten);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#editModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#eid').val(id);
			var lisensi=$(e.relatedTarget).attr('data-lisensi');
			$('#elisensi').val(lisensi);
			var name=$(e.relatedTarget).attr('data-name');
			$('#ename').val(name);
			var telp=$(e.relatedTarget).attr('data-telp');
			$('#etelp').val(telp); 
			var fax=$(e.relatedTarget).attr('data-hp');
			$('#ehp').val(fax); 
			var alamat=$(e.relatedTarget).attr('data-alamat');
			$('#ealamat').val(alamat);
			var provinsi=$(e.relatedTarget).attr('data-provinsi');
			var idprovinsi=$(e.relatedTarget).attr('data-provinsiid');
			$('#eprovinsi').append('<option value="'+ idprovinsi +'" selected>'+ provinsi +'</option>');
			var kabupaten=$(e.relatedTarget).attr('data-kabupaten');
			var idkabupaten=$(e.relatedTarget).attr('data-kabupatenid');
			$('#ekabupaten').append('<option value="'+ idkabupaten +'" selected>'+ kabupaten +'</option>');
			
			
		});
	})
</script>



<script type="text/javascript">
	$("#provinsi").change(function(){
		$.ajax({
			url: "{{ url('provinsi/kabupaten/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#kabupaten').html(data.html);
			}
		});
	});
</script>

<script type="text/javascript">
	$("#eprovinsi").change(function(){
		$.ajax({
			url: "{{ url('provinsi/kabupaten/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#ekabupaten').html(data.html);
			}
		});
	});
</script>
