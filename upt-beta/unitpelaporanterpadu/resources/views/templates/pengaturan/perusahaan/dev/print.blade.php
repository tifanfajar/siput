@include('templates.helpers.data')
<table id="example" class="table table-striped table-dark align-items-center table-flush data-table dataTable no-footer">
	<thead class="thead-light">
		<tr>
			<th scope="col">No.Lisensi</th>
			<th scope="col">Nama</th>
			<th scope="col">No.Telp</th>
			<th scope="col">No.HP</th>
			<th scope="col">Provinsi</th>
			<th scope="col">Kota/Kabupaten</th>
			<th scope="col">Alamat</th>
		</tr>
	</thead>
	<tbody>
		@foreach($company as $companys)
		<tr>
			<td>{{$companys->company_id}}</td>
			<td>{{$companys->name}}</td>
			<td>{{$companys->no_telp}}</td>
			<td>{{$companys->no_hp}}</td>
			<td>{{\App\Model\Region\Provinsi::where('id',$companys->id_prov)->value('nama')}}</td>
			<td>{{\App\Model\Region\KabupatenKota::where('id',$companys->id_kabkot)->value('nama')}}</td>
			<td>{{$companys->alamat}}</td>
		</tr>
		@endforeach
	</tbody>
</table>