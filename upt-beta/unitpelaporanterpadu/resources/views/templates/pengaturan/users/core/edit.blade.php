<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Ubah Pengguna</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('users-update')}}" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<input type="hidden" name="id" id="eid">
					<div class="col-md-6 form-group">
						<label for="email">Nama Pengguna</label>
						<input type="text" class="form-control" id="eemail" name="email" placeholder="Masukan Nama Pengguna" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="name">Nama Lengkap</label>
						<input type="text" class="form-control" id="ename" name="name" placeholder="Masukan Nama Lengkap" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="role_id">Pilih Level</label>
						<select class="form-control" id="erole_id" name="role_id" required>
							<option selected disabled>Pilih salah satu</option>
							@foreach(DB::table('roles')->get() as $level)
                            <option value="{{$level->id}}">{{$level->role_name}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6  form-group">
						<label for="telp">No. Telp</label>
						<input type="number" class="form-control" id="etelp" name="telp" placeholder="Masukan No.Telp">
					</div>
					<div class="col-md-6  form-group">
						<label for="nohp">No. HP</label>
						<input type="number" class="form-control" id="enohp" name="nohp" placeholder="Masukan No.HP">
					</div>
					<div class="col-md-6  form-group">
						<label for="nofax">No. Fax</label>
						<input type="number" class="form-control" id="enofax" name="nofax" placeholder="Masukan No.Fax">
					</div>
					<div class="col-md-6  form-group">
						<label for="profilephoto">Photo Profile</label>
						<div id="ephoto"></div>
					</div>
					<div class="col-md-6  form-group">
						<label for="profilephoto">Pilih Photo</label>
						<input type="file" class="form-control" id="eprofilephoto" name="profilephoto" placeholder="Masukan No.Fax">
					</div>
					<div class="col-md-12 form-group">
						<label for="alamat">Alamat *</label>
						<textarea class="form-control" id="ealamat" name="alamat" rows="3" placeholder="Masukan Alamat UPT" required></textarea>
					</div>
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi *</label>
						<select class="form-control" name="upt_provinsi" id="eprovinsi">
							@foreach(\App\Model\Region\Provinsi::orderBy('id')->get() as $provinsi)
							<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="upt">UPT *</label>
						<select name="id_upt" id="eupt" class="form-control">
						</select>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>