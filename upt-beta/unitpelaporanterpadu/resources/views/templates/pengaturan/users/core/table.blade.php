@include('templates.helpers.data')
<table id="example" class="table table-striped table-dark align-items-center table-flush data-table dataTable no-footer">
	<thead class="thead-light">
		<tr>
			<th scope="col">Nama Pengguna / Email</th>
			<th scope="col">Nama</th>
			<th scope="col">Level</th>
			<th scope="col">Provinsi</th>
			<th scope="col">UPT</th>
			<th scope="col">No. Telp</th>
			<th scope="col">No. HP</th>
			<th scope="col">No. Fax</th>
			<th scope="col">Alamat</th>
			<th scope="col">Tanggal Buat</th>
			<th scope="col">Tanggal Diperbaharui</th>
			<th scope="col">Tindakan</th>
		</tr>
	</thead>
	<tbody>
		@foreach($users as $user)
		<tr>
			<?php  
			$upts = \App\Model\Setting\UPT::where('office_id',$user->upt)->select('office_name')->distinct()->value('office_name');
			?>
			<td>{{$user->email}}</td>
			<td>{{$user->name}}</td>
			<td>{{\App\Model\Privillage\Role::where('id',$user->role_id)->value('role_name')}}</td>
			<td>{{\App\Model\Region\Provinsi::where('id',$user->province_code)->value('nama')}}</td>
			<td>{{$upts}}</td>
			<td>{{$user->no_telp}}</td>
			<td>{{$user->no_hp}}</td>
			<td>{{$user->no_fax}}</td>
			<td>{{$user->alamat}}</td>
			<td>{{$user->created_at}}</td>
			<td>{{$user->updated_at}}</td>
			<td>
				<?php $id = Crypt::encryptString($user->id) ?>
				<?php
				$url = \Request::route()->getName();
				$getKdModule = \App\Model\Menu\Module::where('menu_path',$url)->value('kdModule');
				$getEdit = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
				->value('update_acl');
				$getDelete = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
				->value('delete_acl');
				?>
				<a class="btn btn-success btn-sm" id="getDetail" data-toggle="modal" data-target="#detailModal" data-name="{{$user->name}}" data-email="{{$user->email}}" data-role="{{\App\Model\Privillage\Role::where('id',$user->role_id)->value('role_name')}}" data-telp="{{$user->no_telp}}" data-nohp="{{$user->no_hp}}" data-nofax="{{$user->no_fax}}" data-alamat="{{$user->alamat}}" data-photo="{{$user->profile_photo}}" data-created="{{$user->created_at}}" data-provinsi="{{\App\Model\Region\Provinsi::where('id',$user->province_code)->value('nama')}}"
					data-upt="{{$upts}}" data-updated="{{$user->updated_at}}"><i class="fa fa-file"></i></a>
					@if($getEdit == $getKdModule)
					<a  class="btn btn-warning btn-sm" id="getEdit" data-toggle="modal" data-target="#editModal" data-id="{{$id}}" data-name="{{$user->name}}" data-email="{{$user->email}}" data-id-role="{{$user->role_id}}" data-role="{{\App\Model\Privillage\Role::where('id',$user->role_id)->value('role_name')}}" data-telp="{{$user->no_telp}}" data-nohp="{{$user->no_hp}}" data-nofax="{{$user->no_fax}}" data-alamat="{{$user->alamat}}" data-photo="{{$user->profile_photo}}" data-created="{{$user->created_at}}" data-provinsi="{{\App\Model\Region\Provinsi::where('id',$user->province_code)->value('nama')}}"
						data-upt="{{$upts}}" data-updated="{{$user->updated_at}}" data-provinsiid="{{$user->province_code}}" data-idupt="{{$user->upt}}"><i class="fa fa-edit"></i></a>
						@endif
						@if($getDelete == $getKdModule)
						<a  class="btn  btn-danger btn-sm" 
						id="getDelete" data-toggle="modal" data-target="#deleteModal" data-id="{{$id}}"><i class="fa fa-trash"></i></a>
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>