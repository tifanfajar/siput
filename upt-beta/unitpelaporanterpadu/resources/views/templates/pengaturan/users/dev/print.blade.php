@include('templates.helpers.data')
<table id="example" class="table table-striped table-dark align-items-center table-flush data-table dataTable no-footer">
	<thead class="thead-light">
		<tr>
			<th scope="col">Nama Pengguna / Email</th>
			<th scope="col">Nama</th>
			<th scope="col">Level</th>
			<th scope="col">Provinsi</th>
			<th scope="col">UPT</th>
			<th scope="col">No. Telp</th>
			<th scope="col">No. HP</th>
			<th scope="col">No. Fax</th>
			<th scope="col">Alamat</th>
			<th scope="col">Tanggal Buat</th>
			<th scope="col">Tanggal Diperbaharui</th>
		</tr>
	</thead>
	<tbody>
		@foreach($users as $user)
		<tr>
			<?php  
			$upts = \App\Model\Setting\UPT::where('office_id',$user->upt)->select('office_name')->distinct()->value('office_name');
			?>
			<td>{{$user->email}}</td>
			<td>{{$user->name}}</td>
			<td>{{\App\Model\Privillage\Role::where('id',$user->role_id)->value('role_name')}}</td>
			<td>{{\App\Model\Region\Provinsi::where('id',$user->province_code)->value('nama')}}</td>
			<td>{{$upts}}</td>
			<td>{{$user->no_telp}}</td>
			<td>{{$user->no_hp}}</td>
			<td>{{$user->no_fax}}</td>
			<td>{{$user->alamat}}</td>
			<td>{{$user->created_at}}</td>
			<td>{{$user->updated_at}}</td>
		</tr>
		@endforeach
	</tbody>
</table>