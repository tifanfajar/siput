<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">Ubah Bahan Sosialisasi</h5>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('bahsos-update')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="eid">
					<div class="col-md-12 form-group">
						<label for="judul">Judul *</label>
						<input type="text" class="form-control" id="ejudul" name="judul" placeholder="Masukan Judul" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="id_materi">Materi *</label>
						<select id="emateri" class="form-control select2" name="id_materi" required>
							<option selected disabled>Pilih salah satu</option>
							@foreach(\App\Model\Refrension\Materi::all() as $value)
                            <option value="{{$value->id}}">{{$value->jenis_materi}}</option>
                            @endforeach
						</select>
					</div>
					<div class="col-md-6  form-group">
						<label for="kontributor">Kontributor *</label>
						<input type="text" class="form-control" id="eauthor" name="kontributor" placeholder="Masukan Kontributor" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="lampiran">Lampiran *</label>
						<input type="file" class="form-control" name="lampiran">
					</div>
					<div class="col-md-6 form-group">
						<label>Lampiran sebelumnya</label>
						<input type="text" class="form-control" id="elampiran" disabled="">
					</div>

					<div class="col-md-6 form-group">
						<label for="deskripsi">Deskripsi</label>
						<textarea class="form-control" id="edeskripsi" name="deskripsi" rows="3" placeholder="Masukan Deskripsi"></textarea>

					</div>
					<div class="col-md-6 form-group">
						<label for="keterangan">Keterangan</label>
						<textarea class="form-control" id="eketerangan" name="keterangan" rows="3" placeholder="Masukan Keterangan"></textarea>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>