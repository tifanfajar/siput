@extends('dev.core.using')
@section('content')

@include('templates.teknologi-bimbingan-sosialisasi.bahan-sosialisasi.core.add')
@include('templates.teknologi-bimbingan-sosialisasi.bahan-sosialisasi.core.detail')
@include('templates.teknologi-bimbingan-sosialisasi.bahan-sosialisasi.core.edit')
@include('templates.teknologi-bimbingan-sosialisasi.bahan-sosialisasi.helpers.delete')
@include('templates.helpers.export')
@include('templates.helpers.import')
@include('templates.teknologi-bimbingan-sosialisasi.bahan-sosialisasi.dev.data')

<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<style>
	#example_filter{position: absolute; right: 20px;}
</style>
@if($getStatus != 'administator')
<?php  
$upts = \App\Model\Setting\UPT::where('office_id',Auth::user()->id_upt)->select('office_name')->distinct()->value('office_name');
?>
<br>
<br>
<center>
	<h2>{{$upts}}</h2>
	<h2>{{\App\Model\Region\Provinsi::where('id',Auth::user()->upt_provinsi)->value('nama')}}</h2>
</center>
@endif
@include('templates.helpers.teknologi-bimbingan-sosialisasi.bahan-sosialisasi.maps')
<div class="row mt-5">
	<div class="col text-left" style="padding-bottom: 10px;">
		<?php
		$url = \Request::route()->getName();
		$getKdModule = \App\Model\Menu\Module::where('menu_path',$url)->value('kdModule');
		$getCreate = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('create_acl');
		$getRead = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('read_acl');
		?>
		@if($getCreate == $getKdModule)
		<a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addBahanSosialisasi"><i class="ni ni-fat-add"></i>&nbsp;TAMBAH DATA</a>
		@endif
		@include('dev.helpers.action')
	</div>


	<div class="col-xl-12 mb-5 mb-xl-0">
		<div class="card shadow" style="border:1px solid #dedede;">
			<div class="card-header border-0" style="background-color: #5f5f5f;">
				<div class="row align-items-center">
					<div class="col">
						<h3 class="mb-0" style="color: #fff;"><i class="ni ni-archive-2"></i>&nbsp;&nbsp;Bahan Dan Materi Sosialisasi Dan Bimtek</h3>
					</div>
					<!-- <div class="col text-right">
						<a href="#!" class="btn btn-sm btn-primary">See all</a>
					</div> -->
				</div>
			</div>
			<div class="table-responsive">
				<!-- Projects table -->
				<div class="widest">
					@include('templates.teknologi-bimbingan-sosialisasi.bahan-sosialisasi.core.table')
				</div>
			</div>
		</div>
	</div>

	
</div>

@include('templates.helpers.teknologi-bimbingan-sosialisasi.bahan-sosialisasi.js')

@endsection