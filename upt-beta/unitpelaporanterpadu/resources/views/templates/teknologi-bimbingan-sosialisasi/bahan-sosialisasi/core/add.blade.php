<div class="modal fade" id="addBahanSosialisasi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">TAMBAH BAHAN SOSIALISASI</h5>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('bahsos-save')}}" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="col-md-6 form-group">
						<label for="id_materi">Materi *</label><label style="color: red;">*</label>
						<select class="form-control select2" name="id_materi" required>
							<option selected disabled>Pilih salah satu</option>
							@foreach(\App\Model\Refrension\Materi::all() as $value)
                            <option value="{{$value->id}}">{{$value->jenis_materi}}</option>
                            @endforeach
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="judul">Judul *</label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="judul" name="judul" placeholder="Masukan Judul" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="kontributor">Kontributor *</label><label style="color: red;">*</label>
						<input type="text" class="form-control" id="kontributor" name="kontributor" placeholder="Masukan Kontributor" required>
					</div>
					<div class="col-md-6  form-group">
						<label for="lampiran">Lampiran *</label><label style="color: red;">*</label>
						<input type="file" class="form-control" id="lampiran" name="lampiran" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="deskripsi">Deskripsi</label>
						<textarea class="form-control" id="deskripsi" name="deskripsi" rows="3" placeholder="Masukan Deskripsi"></textarea>
					</div>
					<div class="col-md-6 form-group">
						<label for="keterangan">Keterangan</label>
						<textarea class="form-control" id="keterangan" name="keterangan" rows="3" placeholder="Masukan Keterangan"></textarea>
					</div>
					@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi</label>
						<select class="form-control" id="upt_provinsi" name="id_prov">
							<option selected disabled>Khusus Kepala UPT & Operator</option>
							@foreach(\App\Model\Region\Provinsi::orderBy('id')->get() as $provinsi)
							<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="city">UPT</label>
						<select name="kode_upt" id="id_upt" class="form-control">
							<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
						</select>
					</div>
					@endif
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>