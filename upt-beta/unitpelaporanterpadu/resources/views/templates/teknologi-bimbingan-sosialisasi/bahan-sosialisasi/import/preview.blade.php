@extends('dev.core.using')
@section('content')
<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<div class="row mt-5">
	<div class="container">
	<form class="row" action="{{route('bahsos-import-post')}}" method="POST" id="form-validate" enctype="multipart/form-data">
	@csrf
	<input type="hidden" name="route" id="route" value="{{url('sosialisasi-bimtek/bahan-sosialisasi')}}">    
		<label for="id_materi">Materi *</label>
		<select class="form-control" name="id_materi" style="margin-bottom: 20px; border: 2; border-radius: 6px;" required>
			<option selected disabled>Pilih salah satu</option>
			@foreach(\App\Model\Refrension\Materi::all() as $value)
            <option value="{{$value->id}}">{{$value->jenis_materi}}</option>
            @endforeach
		</select>
		@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')			
			<label for="provinsi">Provinsi</label>
			<select class="form-control" style="margin-bottom: 20px" id="upt_provinsi" name="upt_provinsi" required>
				<option selected disabled>Provinsi</option>
				@foreach(\App\Model\Region\Provinsi::all() as $provinsi)
				<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
				@endforeach
			</select>
			
			<label for="city">UPT</label>
			<select name="id_upt" id="id_upt" style="margin-bottom: 20px" class="form-control">
				<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
			</select>
		@endif
	<table class="table">

	  <thead>
	    <tr>
	      <th scope="col">Judul *</th>
	      <th scope="col">Dekripsi *</th>
	      <th scope="col">Kontributor</th>
	      <th scope="col">Keterangan</th>	      
	      <th scope="col">Lampiran</th>	      
	    </tr>
	  </thead>
	  <tbody>
		@foreach($result as $company => $companys)
	    <tr>
	      <td><input type="text" class="form-control" id="judul" name="judul[]" placeholder="Masukan Judul" value="{{$companys[0]}}" data-validation="message"></td>
	      <td><input type="text" class="form-control" id="deskripsi" name="deskripsi[]" placeholder="Masukan Deksripsi" value="{{$companys[1]}}" data-validation="message"></td>
	      <td><input type="text" class="form-control" id="author" name="author[]" value="{{$companys[2]}}" placeholder="Masukan Kontributor" data-validation="message"></td>
	      <td><input type="text" class="form-control" id="keterangan" name="keterangan[]" value="{{$companys[3]}}" placeholder="Masukan keterangan" data-validation="message"></td>
	      <td><input type="file" id="lampiran" name="lampiran[]" required></td>
	    </tr>
	    @endforeach
	  </tbody>
	</table>
	<button type="submit" id="btn-submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Import Data</button>
	<a class="btn btn-danger" href="{{ URL::previous() }}"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Kembali</a>	
    </form>

</div>	
</div>
@include('dev.helpers.jquery')
@include('dev.helpers.validation')
<script>
	$("#upt_provinsi").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#id_upt').html(data.html);
				console.log(data.html);
			}
		});
	});
</script>

@endsection

