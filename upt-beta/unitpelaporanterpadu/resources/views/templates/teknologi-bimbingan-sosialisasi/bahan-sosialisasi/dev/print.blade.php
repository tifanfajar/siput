<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Lampiran Data Bahan Sosialisasi</h2>
			<hr>
			<table id="example" class="table table-bordered align-items-center" style="width: 100%; ">
				<thead class="thead-light">
					<tr>
						<th scope="col">No. </th>
						<th scope="col">Tanggal</th>
						<th scope="col">Provinsi</th>
						<th scope="col">UPT</th>
						<th scope="col">Kategori</th>
						<th scope="col">Judul</th>
						<th scope="col">Deskripsi</th>
						<th scope="col">Keterangan</th>
						<th scope="col">Kontributor</th>
					</tr>
				</thead>
				<tbody>
					@foreach($bahsos as $key => $value)
					<tr>
						<td scope="col">{{$key+1}}</td>
						<td scope="col">{{$value->created_at}}</td>
						@if(\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama'))
						<td scope="col">{{\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama')}}</td>
						@else
						<td scope="col">Admin</td>
						@endif
						<?php  
						$upts = \App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
						?>
						@if($upts)
						<td scope="col">{{$upts}}</td>
						@else
						<td scope="col">Admin</td>
						@endif
						<td scope="col">{{\App\Model\Refrension\Materi::where('id',$value->id_materi)->value('jenis_materi')}}</td>
						<td scope="col">{{$value->judul}}</td>
						<td scope="col">{{$value->deskripsi}}</td>
						<td scope="col">{{$value->keterangan}}</td>
						<td scope="col">{{$value->author}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<hr>
		</div>
	</center>
</body>
</html>
