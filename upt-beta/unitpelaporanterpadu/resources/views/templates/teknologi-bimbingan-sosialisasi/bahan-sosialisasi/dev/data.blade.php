@include('dev.helpers.jquery')
<script type="text/javascript">
	$("#upt_provinsi").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#id_upt').html(data.html);
			}
		});
	});
</script>
<script type="text/javascript">
var bla = $('#txt_name').val();
$('#txt_name').val(bla);
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#detailModal').on('show.bs.modal', function (e) {
			var tanggal=$(e.relatedTarget).attr('data-tanggal');
			$('#dtanggal').val(tanggal);
			var provinsi=$(e.relatedTarget).attr('data-provinsi');
			$('#dprovinsi').val(provinsi); 
			var upt=$(e.relatedTarget).attr('data-upt');
			$('#dupt').val(upt); 
			var materi=$(e.relatedTarget).attr('data-materi');
			$('#dmateri').val(materi);
			var judul=$(e.relatedTarget).attr('data-judul');
			$('#djudul').val(judul);
			var deskripsi=$(e.relatedTarget).attr('data-deskripsi');
			$('#ddeskripsi').val(deskripsi);
			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#dketerangan').val(keterangan);
			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#dlampiran').val(lampiran);
			var author=$(e.relatedTarget).attr('data-author');
			$('#dauthor').val(author);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#editModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#eid').val(id);

			var tanggal=$(e.relatedTarget).attr('data-tanggal');
			$('#etanggal').val(tanggal);

			var provinsi=$(e.relatedTarget).attr('data-provinsi');
			$('#eprovinsi').val(provinsi);

			var upt=$(e.relatedTarget).attr('data-upt');
			$('#eupt').val(upt);

			var materi=$(e.relatedTarget).attr('data-materi');
			$('#emateri').val(materi);

			var judul=$(e.relatedTarget).attr('data-judul');
			$('#ejudul').val(judul);

			var deskripsi=$(e.relatedTarget).attr('data-deskripsi');
			$('#edeskripsi').val(deskripsi);

			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#eketerangan').val(keterangan);

			var lampiran=$(e.relatedTarget).attr('data-lampiran');
			$('#elampiran').val(lampiran);

			var author=$(e.relatedTarget).attr('data-author');
			$('#eauthor').val(author);

			var materi=$(e.relatedTarget).attr('data-materi');
			var idmateri=$(e.relatedTarget).attr('data-id-materi');
			$('#emateri').append('<option value="'+ idmateri +'" selected>'+ materi +'</option>');
		});
	});
</script>