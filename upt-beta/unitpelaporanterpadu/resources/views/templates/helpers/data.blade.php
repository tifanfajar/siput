<script type="text/javascript">
	$(document).ready(function(){
		$('#deleteModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#deid').val(id);
		});
		$('#deleteMateri').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id-materi');
			$('#deidMateri').val(id);
		});
		$('#deleteTujuan').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id-tujuan');
			$('#deidTujuan').val(id);
		});
		$('#deleteGroupPerizinan').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id-group-perizinan');
			$('#deidPerizinan').val(id);
		});
		$('#deleteMetode').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id-metode');
			$('#deidMetode').val(id);
		});
		$('#deleteKegiatan').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id-kegiatan');
			$('#deidKegiatan').val(id);
		});
	})
</script>
