<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="http://localhost:8000/templates/assets/img/trash.jpg" alt="" style="width: 50px; height: 30px; margin-bottom: 2px; padding-right: 10px; ">
				<h5 class="modal-title" id="exampleModalLabel" style="margin-top: 6px;">Hapus Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
				<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>
			<div class="modal-body" style="overflow: hidden;">
				<form action="{{url()->full()}}/delete" method="GET" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="deid">
					<center>
					@include('dev.helpers.button.btnGroupHapus')
					</center>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Materi -->
<div class="modal fade" id="deleteMateri" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="http://localhost:8000/templates/assets/img/trash.jpg" alt="" style="width: 50px; height: 30px; margin-bottom: 2px; padding-right: 10px; ">
				<h5 class="modal-title" id="exampleModalLabel" style="margin-top: 6px;">Hapus Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
				<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>
			<div class="modal-body" style="overflow: hidden;">
				<form action="{{url()->full()}}/delete-materi" method="GET" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="deidMateri">
					<center>
					@include('dev.helpers.button.btnGroupHapus')
					</center>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Tujuan -->
<div class="modal fade" id="deleteTujuan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="http://localhost:8000/templates/assets/img/trash.jpg" alt="" style="width: 50px; height: 30px; margin-bottom: 2px; padding-right: 10px; ">
				<h5 class="modal-title" id="exampleModalLabel" style="margin-top: 6px;">Hapus Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
				<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>
			<div class="modal-body" style="overflow: hidden;">
				<form action="{{url()->full()}}/delete-tujuan" method="GET" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="deidTujuan">
					<center>
					@include('dev.helpers.button.btnGroupHapus')
					</center>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Metode -->
<div class="modal fade" id="deleteMetode" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="http://localhost:8000/templates/assets/img/trash.jpg" alt="" style="width: 50px; height: 30px; margin-bottom: 2px; padding-right: 10px; ">
				<h5 class="modal-title" id="exampleModalLabel" style="margin-top: 6px;">Hapus Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
				<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>
			<div class="modal-body" style="overflow: hidden;">
				<form action="{{url()->full()}}/delete-metode" method="GET" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="deidMetode">
					<center>
					@include('dev.helpers.button.btnGroupHapus')
					</center>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Group Perizinan -->
<div class="modal fade" id="deleteGroupPerizinan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="http://localhost:8000/templates/assets/img/trash.jpg" alt="" style="width: 50px; height: 30px; margin-bottom: 2px; padding-right: 10px; ">
				<h5 class="modal-title" id="exampleModalLabel" style="margin-top: 6px;">Hapus Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
				<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>
			<div class="modal-body" style="overflow: hidden;">
				<form action="{{url()->full()}}/delete-group-perizinan" method="GET" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="deidPerizinan">
					<center>
					@include('dev.helpers.button.btnGroupHapus')
					</center>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Kegiatan -->
<div class="modal fade" id="deleteKegiatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="http://localhost:8000/templates/assets/img/trash.jpg" alt="" style="width: 50px; height: 30px; margin-bottom: 2px; padding-right: 10px; ">
				<h5 class="modal-title" id="exampleModalLabel" style="margin-top: 6px;">Hapus Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
				<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>
			<div class="modal-body" style="overflow: hidden;">
				<form action="{{url()->full()}}/delete-kegiatan" method="GET" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="deidKegiatan">
					<center>
					@include('dev.helpers.button.btnGroupHapus')
					</center>
				</form>
			</div>
		</div>
	</div>
</div>
