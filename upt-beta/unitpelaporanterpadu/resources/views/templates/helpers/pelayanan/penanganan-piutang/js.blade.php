<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>
<script>
	Highcharts.chart('penanganan-piutang-operator', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		$kabupaten = \App\Model\Region\KabupatenKota::all();
		?>
		series: [{
			name: 'Total',
			colorByPoint: true,
			data: [
			@foreach($kabupaten as $key => $value)
			{	
				name: '{{$value->nama}}',
				y: {{\App\Model\Service\PenangananPiutang::where('nama_kpknl',$value->id)->where('kode_upt',Auth::user()->id_upt)->count()}}
			},
			@endforeach
			]
		}]
	});
</script>
<script>
	Highcharts.chart('penanganan-piutang-tujuan', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		$kabupaten = \App\Model\Region\KabupatenKota::all();
		?>
		series: [{
			name: 'Total',
			colorByPoint: true,
			data: [
			@foreach($kabupaten as $key => $value)
			{	
				name: '{{$value->nama}}',
				y: {{\App\Model\Service\PenangananPiutang::where('nama_kpknl',$value->id_row)->count()}}
			},
			@endforeach
			]
		}]
	});
</script>

<script>
	Highcharts.chart('penanganan-piutang', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		$provinsi = \App\Model\Region\Provinsi::all();
		?>
		series: [{
			name: 'Total',
			colorByPoint: true,
			data: [
			@foreach($provinsi as $key => $value)
			{	
				name: '{{$value->nama}}',
				y: {{\App\Model\Service\PenangananPiutang::where('id_prov',$value->id_row)->count()}}
			},
			@endforeach
			]
		}]
	});
</script>

<script type="text/javascript">
	var map = am4core.create("chartdiv", am4maps.MapChart);
</script>