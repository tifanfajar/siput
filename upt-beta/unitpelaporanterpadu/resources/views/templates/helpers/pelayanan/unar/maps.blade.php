<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@if($getStatus != 'kepala-upt' && $getStatus != 'operator')
@include('dev.helpers.jquery')
<br>
<br>
<label><i class="fa fa-map-marked-alt"></i>&nbsp;Peta Lokasi UPT</label>
<div class="col">
	<div class="card shadow border-0" style="height: 380px;">
		<div id="maps"></div>
	</div>
</div>
<div id="info">
	<span class="f32"><span id="flag"></span></span>
	<form id="searchForm" name="searchForm" action="{{route('unar-search')}}" method="GET">
		<input type="hidden" name="id_map" id="map" class="form-control">
	</form>
</div>
<script type="text/javascript">
	$( "#maps" ).click(function() {
		$( "#searchForm" ).submit();
	});
</script>
@endif