<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>

<script>
	Highcharts.chart('unar-operator', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		$lulus_yd = \App\Model\Service\Unar::where('kode_upt',Auth::user()->id_upt)->sum('lulus_yd');
		$lulus_yc = \App\Model\Service\Unar::where('kode_upt',Auth::user()->id_upt)->sum('lulus_yc');
		$lulus_yb = \App\Model\Service\Unar::where('kode_upt',Auth::user()->id_upt)->sum('lulus_yb');
		$lulus = $lulus_yb + $lulus_yc + $lulus_yd;
		$tdk_lulus_yd = \App\Model\Service\Unar::where('kode_upt',Auth::user()->id_upt)->sum('tdk_lulus_yd');
		$tdk_lulus_yc = \App\Model\Service\Unar::where('kode_upt',Auth::user()->id_upt)->sum('tdk_lulus_yc');
		$tdk_lulus_yb = \App\Model\Service\Unar::where('kode_upt',Auth::user()->id_upt)->sum('tdk_lulus_yb');
		$tdk_lulus = $tdk_lulus_yb + $tdk_lulus_yc + $tdk_lulus_yd;
		?>
		series: [{
			name: 'Brands',
			colorByPoint: true,
			data: [
			{	
				name: 'Lulus',
				y: {{$lulus}}
			},
			{	
				name: 'Tidak Lulus',
				y: {{$tdk_lulus}}
			},
			]
		}]
	});
</script>
<script>
	Highcharts.chart('unar-admin', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		$lulus_yd = \App\Model\Service\Unar::sum('lulus_yd');
		$lulus_yc = \App\Model\Service\Unar::sum('lulus_yc');
		$lulus_yb = \App\Model\Service\Unar::sum('lulus_yb');
		$lulus = $lulus_yb + $lulus_yc + $lulus_yd;
		$tdk_lulus_yd = \App\Model\Service\Unar::sum('tdk_lulus_yd');
		$tdk_lulus_yc = \App\Model\Service\Unar::sum('tdk_lulus_yc');
		$tdk_lulus_yb = \App\Model\Service\Unar::sum('tdk_lulus_yb');
		$tdk_lulus = $tdk_lulus_yb + $tdk_lulus_yc + $tdk_lulus_yd;
		?>
		series: [{
			name: 'Brands',
			colorByPoint: true,
			data: [
			{	
				name: 'Lulus',
				y: {{$lulus}}
			},
			{	
				name: 'Tidak Lulus',
				y: {{$tdk_lulus}}
			},
			]
		}]
	});
</script>

<script>
	Highcharts.chart('unar-admin2', {
		chart: {
			type: 'bar'
		},
		title:false,
		<?php 
		$upt = \App\Model\Setting\UPT::distinct('office_id')->get();
		?>
		xAxis: {
			categories: [
			@foreach($upt as $key => $value)
			'{{$value->office_name}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'Total Unar',
			data: [
			@foreach($upt as $key => $value)
			{{\App\Model\Service\Unar::where('kode_upt',$value->office_id)->count()}},
			@endforeach
			]
		}, 
		]
	});
</script>

<script type="text/javascript">
	var map = am4core.create("chartdiv", am4maps.MapChart);
</script>