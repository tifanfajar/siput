<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>
<script>
	Highcharts.chart('unar-search', {
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		title:false,
		tooltip: {
			pointFormat: '{series.name}: <b>{point.y}</b>'
		},
		plotOptions: {
			pie: {
				size:'70%',
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.y}'
				}
			}
		},
		<?php
		$lulus_yd = \App\Model\Service\Unar::where('kode_upt',$changeUPT)->sum('lulus_yd');
		$lulus_yc = \App\Model\Service\Unar::where('kode_upt',$changeUPT)->sum('lulus_yc');
		$lulus_yb = \App\Model\Service\Unar::where('kode_upt',$changeUPT)->sum('lulus_yb');
		$lulus = $lulus_yb + $lulus_yc + $lulus_yd;
		$tdk_lulus_yd = \App\Model\Service\Unar::where('kode_upt',$changeUPT)->sum('tdk_lulus_yd');
		$tdk_lulus_yc = \App\Model\Service\Unar::where('kode_upt',$changeUPT)->sum('tdk_lulus_yc');
		$tdk_lulus_yb = \App\Model\Service\Unar::where('kode_upt',$changeUPT)->sum('tdk_lulus_yb');
		$tdk_lulus = $tdk_lulus_yb + $tdk_lulus_yc + $tdk_lulus_yd;
		?>
		series: [{
			name: 'Brands',
			colorByPoint: true,
			data: [
			{	
				name: 'Lulus',
				y: {{$lulus}}
			},
			{	
				name: 'Tidak Lulus',
				y: {{$tdk_lulus}}
			},
			]
		}]
	});
</script>
<script type="text/javascript">
	var map = am4core.create("chartdiv", am4maps.MapChart);
</script>