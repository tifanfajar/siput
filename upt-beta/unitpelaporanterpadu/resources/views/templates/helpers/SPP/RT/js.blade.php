<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>

<?php 
$myTime = Carbon\Carbon::now();
$fixYears = '20'.$myTime->format('y');
$getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses');
?>
@if($getStatus == 'administrator')
<script>	
	Highcharts.chart('sppRT-admin', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'Total RT Terbit Diterbitkan',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php 
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan;
				$minYears = $fixYears;
			}
			$pay_rt = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$minMonth)->whereYear('bi_begin',$minYears)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
			$not_pay_rt = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$minMonth)->whereYear('bi_begin',$minYears)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
			?>
			{{$pay_rt + $not_pay_rt}},
			@endforeach
			]
		},
		{
			name: 'RT Terbayar ( H-60 s.d H-30 )',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			} 
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$minMonth)->whereYear('bi_begin',$minYears)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count()}},
			@endforeach
			]
		},
		{
			name: 'RT Terbayar ( H-30 s.d H ) Hasil TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			@if(\App\Model\SPP\RincianTagihan::where('status','paid')->get() == null)
			0,
			@else
			{{\App\Model\SPP\RincianTagihan::where('status','paid')->whereMonth('bi_begin',$minMonth)->whereYear('bi_begin',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count()}},
			@endif
			@endforeach
			]
		},
		{
			name: 'RT Belum Terbayar ( Belum Jatuh Tempo )',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$minMonth)->whereYear('bi_begin',$minYears)->where('status_remainder',null)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count()}},
			@endforeach
			]
		},
		{
			name: 'RT Tidak Membayar ( Menjadi Reminder )',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$minMonth)->whereYear('bi_begin',$minYears)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',null)->count()}},
			@endforeach
			]
		},
		]
	});
</script>
@endif

<script>	
	Highcharts.chart('sppRT', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'Total RT Terbit Diterbitkan',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php 
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			$pay_rt = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$minMonth)->whereYear('bi_begin',$minYears)->where('status','paid')->where('upt',$getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
			$not_pay_rt = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$minMonth)->whereYear('bi_begin',$minYears)->where('status','not paid')->where('upt',$getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
			?>
			{{$pay_rt + $not_pay_rt}},
			@endforeach
			]
		},
		{
			name: 'RT Terbayar ( H-60 s.d H-30 )',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$minMonth)->whereYear('bi_begin',$minYears)->where('status','paid')->where('upt',$getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count()}},
			@endforeach
			]
		},
		{
			name: 'RT Terbayar ( H-30 s.d H ) Hasil TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			@if(\App\Model\SPP\RincianTagihan::where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->whereMonth('bi_begin',$minMonth)->whereYear('bi_begin',$minYears)->get() == null)
			0,
			@else
			{{\App\Model\SPP\RincianTagihan::where('active',1)->whereMonth('bi_begin',$minMonth)->whereYear('bi_begin',$minYears)->where('upt',$getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count()}},
			@endif
			@endforeach
			]
		},
		{
			name: 'RT Belum Terbayar ( Belum Jatuh Tempo )',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$minMonth)->whereYear('bi_begin',$minYears)->where('status_remainder','null')->where('upt',$getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count()}},
			@endforeach
			]
		},
		{
			name: 'RT Tidak Membayar ( Menjadi Reminder )',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$minMonth)->whereYear('bi_begin',$minYears)->where('status','not paid')->where('upt',$getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',null)->count()}},
			@endforeach
			]
		},
		]
	});
</script>
<script type="text/javascript">
	var map = am4core.create("chartdiv", am4maps.MapChart);
</script>