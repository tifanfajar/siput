<div class="row">
	<!-- Grafik -->
	<div class="col-xl-12 mb-6 mb-xl-0">
		<div class="card shadow">
			<div class="card-header bg-transparent">
				<div class="row align-items-center">
					<div class="col">
						<h6 class="text-uppercase text-light ls-1 mb-1"><i class="ni ni-chart-pie-35"></i>&nbsp;&nbsp;Grafik Aksi Pencegahan Piutang oleh {{$getUptName}}
						</h6>
					</div>
				</div>
			</div>
			<div class="card-body" style="overflow-y: scroll;">
				<div class="chart">	
					<div id="sppRT" style="min-width: 350px; height: 2000px; max-width: 600px; margin: 0 auto"></div>
				</div>
			</div>
		</div>
	</div>
</div>