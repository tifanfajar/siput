<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/maps/modules/map.js"></script>
<script src="http://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="http://code.highcharts.com/mapdata/countries/us/us-all.js"></script>

<?php 
$myTime = Carbon\Carbon::now();
$fixYears = '20'.$myTime->format('y');
$getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses');
?>

@if($getStatus == 'administrator')
<script>	
	Highcharts.chart('sppRT-admin', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'ST-1 Terbit Sudah di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp','First Reminder')->where('active',1)->count()}},
			@endforeach
			]
		},
		{
			name: 'ST-1 Terbit Belum di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp','First Reminder')->where('active',null)->count()}},
			@endforeach
			]
		},
		]
	});
</script>

<script>	
	Highcharts.chart('sppRT-admin1', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'ST-2 Terbit Sudah di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp','Second Reminder')->where('active',1)->count()}},
			@endforeach
			]
		},
		{
			name: 'ST-2 Terbit Belum di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp','Second Reminder')->where('active',null)->count()}},
			@endforeach
			]
		},
		]
	});
</script>


<script>	
	Highcharts.chart('sppRT-admin2', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'ST-3 Terbit Sudah di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp','Third Reminder')->where('active',1)->count()}},
			@endforeach
			]
		},
		{
			name: 'ST-3 Terbit Belum di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp','Third Reminder')->where('active',null)->count()}},
			@endforeach
			]
		},
		]
	});
</script>

<script>	
	Highcharts.chart('sppRT-admin3', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'STT Terbit Sudah di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp','Last Reminder')->where('active',1)->count()}},
			@endforeach
			]
		},
		{
			name: 'STT Terbit Belum di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp','Last Reminder')->where('active',null)->count()}},
			@endforeach
			]
		},
		]
	});
</script>
@endif

<script>	
	Highcharts.chart('sppRT-operator', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'ST-1 Terbit Sudah di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp','First Reminder')->where('active',1)->where('upt',$getUptName)->count()}},
			@endforeach
			]
		},
		{
			name: 'ST-1 Terbit Belum di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp','First Reminder')->where('active',null)->where('upt',$getUptName)->count()}},
			@endforeach
			]
		},
		]
	});
</script>

<script>	
	Highcharts.chart('sppRT-operator1', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'ST-2 Terbit Sudah di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp','Second Reminder')->where('active',1)->where('upt',$getUptName)->count()}},
			@endforeach
			]
		},
		{
			name: 'ST-2 Terbit Belum di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp','Second Reminder')->where('active',null)->where('upt',$getUptName)->count()}},
			@endforeach
			]
		},
		]
	});
</script>


<script>	
	Highcharts.chart('sppRT-operator2', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'ST-3 Terbit Sudah di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp','Third Reminder')->where('active',1)->where('upt',$getUptName)->count()}},
			@endforeach
			]
		},
		{
			name: 'ST-3 Terbit Belum di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp','Third Reminder')->where('active',null)->where('upt',$getUptName)->count()}},
			@endforeach
			]
		},
		]
	});
</script>

<script>	
	Highcharts.chart('sppRT-operator3', {
		chart: {
			type: 'bar'
		},
		title:false,
		xAxis: {
			categories: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			'{{$value->nama}}',
			@endforeach
			],
			title: {
				text: null
			}
		},
		yAxis: {
			min: 0,
			title: {
				text: 'UPT',
				align: 'high'
			},
			labels: {
				overflow: 'justify'
			}
		},
		tooltip: {
			valueSuffix: ' Jumlah'
		},
		plotOptions: {
			bar: {
				dataLabels: {
					enabled: true
				}
			}
		},
		legend: {
			layout: 'vertical',
			align: 'right',
			verticalAlign: 'top',
			x: -40,
			y: 80,
			floating: true,
			borderWidth: 1,
			backgroundColor:
			Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
			shadow: true
		},
		credits: {
			enabled: false
		},
		series: [
		{
			name: 'STT Terbit Sudah di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp','Last Reminder')->where('active',1)->where('upt',$getUptName)->count()}},
			@endforeach
			]
		},
		{
			name: 'STT Terbit Belum di-TL UPT',
			data: [
			@foreach(\App\Model\Date\ListMonth::all() as $key => $value)
			<?php
			if ($value->id_bulan == 1) {
				$minMonth = 12;
				$minYears = $fixYears - 1;
			}
			else{
				$minMonth = $value->id_bulan - 1;
				$minYears = $fixYears;
			}
			?>
			{{\App\Model\SPP\RincianTagihan::whereMonth('bi_create_date',$minMonth)->whereYear('bi_create_date',$minYears)->where('status_izin','Perpanjangan')->where('katagori_spp','Last Reminder')->where('active',null)->where('upt',$getUptName)->count()}},
			@endforeach
			]
		},
		]
	});
</script>
<script type="text/javascript">
	var map = am4core.create("chartdiv", am4maps.MapChart);
</script>