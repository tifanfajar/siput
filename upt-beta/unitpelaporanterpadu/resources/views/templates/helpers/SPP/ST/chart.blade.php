@if($getStatus == 'administrator')
<div class="row">
	<!-- Grafik -->
	<div class="col-xl-6 mb-6 mb-xl-0">
		<div class="card shadow">
			<div class="card-header bg-transparent">
				<div class="row align-items-center">
					<div class="col">
						<h6 class="text-uppercase text-light ls-1 mb-1"><i class="ni ni-chart-pie-35"></i>&nbsp;&nbsp;ST-1 (first reminder)
						</h6>
					</div>
				</div>
			</div>
			<div class="card-body" style="overflow-y: scroll;">
				<div class="chart">	
					<div id="sppRT-admin" style="height: 2000px; width: 600px; margin: 0 auto"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Grafik -->
	<div class="col-xl-6 mb-6 mb-xl-0">
		<div class="card shadow">
			<div class="card-header bg-transparent">
				<div class="row align-items-center">
					<div class="col">
						<h6 class="text-uppercase text-light ls-1 mb-1"><i class="ni ni-chart-pie-35"></i>&nbsp;&nbsp;ST-2 (second reminder)

						</h6>
					</div>
				</div>
			</div>
			<div class="card-body" style="overflow-y: scroll;">
				<div class="chart">	
					<div id="sppRT-admin1" style="height: 2000px; width: 600px; margin: 0 auto"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<br>
<br>
<br>
<div class="row">
	<!-- Grafik -->
	<div class="col-xl-6 mb-6 mb-xl-0">
		<div class="card shadow">
			<div class="card-header bg-transparent">
				<div class="row align-items-center">
					<div class="col">
						<h6 class="text-uppercase text-light ls-1 mb-1"><i class="ni ni-chart-pie-35"></i>&nbsp;&nbsp;ST-3 (third reminder)
						</h6>
					</div>
				</div>
			</div>
			<div class="card-body" style="overflow-y: scroll;">
				<div class="chart">	
					<div id="sppRT-admin2" style="height: 2000px; width: 600px; margin: 0 auto"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- Grafik -->
	<div class="col-xl-6 mb-6 mb-xl-0">
		<div class="card shadow">
			<div class="card-header bg-transparent">
				<div class="row align-items-center">
					<div class="col">
						<h6 class="text-uppercase text-light ls-1 mb-1"><i class="ni ni-chart-pie-35"></i>&nbsp;&nbsp;STT (last reminder)
						</h6>
					</div>
				</div>
			</div>
			<div class="card-body" style="overflow-y: scroll;">
				<div class="chart">	
					<div id="sppRT-admin3" style="height: 2000px; width: 600px; margin: 0 auto"></div>
				</div>
			</div>
		</div>
	</div>
</div>
@endif