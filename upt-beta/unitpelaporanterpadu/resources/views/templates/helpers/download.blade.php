<!-- Modal -->
<div class="modal fade" id="downloadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DOWNLOAD AS XLS</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>
			<div class="modal-body" style="overflow: hidden;">			
				<form action="{{url()->full()}}/download" method="GET" enctype="multipart/enctype">
					<center>
					@csrf
					<center>
						<label>Filter Data :</label>
						<hr>
						<div class="row">
						<div class="col-md-4 form-group">
							<label for="date">Tanggal</label>
							<input type="number" class="form-control" id="date" name="date">
						</div>
						<div class="col-md-4 form-group">
							<label for="date">Bulan</label>
							<input type="number" class="form-control" id="date" name="month">
						</div>
						<div class="col-md-4 form-group">
							<label for="date">Tahun</label>
							<input type="number" class="form-control" id="date" name="year">
						</div>
						</div>
						<hr>
					</center>
					<br>
					<center>
					@include('dev.helpers.button.btnGroupCetak')
					</center>
				</center>
			</form>
		</div>
	</div>
</div>
</div>