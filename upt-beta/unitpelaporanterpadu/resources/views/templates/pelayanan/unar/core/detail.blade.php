<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL UNAR</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi</label>
						<input type="text" id="dprovinsi" class="form-control" name="provinsi" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="upt">UPT</label>
						<input type="text" id="dupt" class="form-control" name="upt" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="lampiran">Tanggal Pelaksanaan Ujian</label>
						<input type="date" class="form-control" id="dtanggal_ujian" name="tanggal_ujian" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="lokasi">Lokasi Ujian</label>
						<textarea class="form-control" id="dlokasi" name="deskripsi" rows="3" disabled></textarea>
					</div>
					<div class="col-md-12  form-group">
						<label for="kontributor">Kab/Kota</label>
						<input class="form-control" id="dkota" name="kota" rows="3" disabled>
					</div>
					<div style="text-align: center;" class="col-md-4 form-group">
						<label>Jumlah Peserta</label>
					</div>
					<div style="text-align: center;" class="col-md-4 form-group">
						<label>Lulus</label>
					</div>
					<div style="text-align: center;" class="col-md-4 form-group">
						<label>Tidak Lulus</label>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="djumlah_yd" name="jumlah_yd" rows="3" disabled>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="dlulus_yd" name="lulus_yd" rows="3" disabled>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="dtdk_lulus_yd" name="tdk_lulus_yd" rows="3" disabled>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="djumlah_yc" name="jumlah_yc" rows="3" disabled>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="dlulus_yc" name="lulus_yc" rows="3" disabled>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="dtdk_lulus_yc" name="tdk_lulus_yc" rows="3" disabled>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="djumlah_yb" name="jumlah_yb" rows="3" disabled>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="dlulus_yb" name="lulus_yb" rows="3" disabled>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="dtdk_lulus_yb" name="tdk_lulus_yb" rows="3" disabled>
					</div>
					<div class="col-md-6  form-group">
						<label>Dibuat Oleh</label>
						<input type="text" class="form-control" id="dcreated_by" name="created_by" rows="3" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label>Dibuat Pada Tanggal</label>
						<input type="text" class="form-control" id="dcreated_at" name="created_at" rows="3" disabled>
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>