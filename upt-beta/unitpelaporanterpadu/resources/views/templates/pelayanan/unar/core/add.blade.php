<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">TAMBAH UNAR</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('unar-save')}}" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi </label><label style="color: red;">*</label>
						<select class="form-control" name="id_prov" id="upt_provinsi" required>
							<option selected disabled>Pilih Salah Satu</option>
							@foreach(\App\Model\Region\Provinsi::all() as $provinsis)
							<option value="{{ $provinsis->id_row }}">
								{{ $provinsis->nama }}
							</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="city">UPT</label>
						<select name="id_upt" id="id_upt" class="form-control">
							<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="city">Kabupaten/Kota</label>
						<select name="id_kabkot" id="kabupaten" class="form-control" required>
							<option value="" selected disabled>Pilih Salah Satu</option>
						</select>
					</div>
					@else
					<?php $notAdminKabkot = \App\Model\Region\KabupatenKota::where('id_prov',Auth::user()->upt_provinsi)->get(); ?>
					<div class="col-md-6 form-group">
						<label for="city">Kabupaten/Kota</label>
						<select name="id_kabkot"  class="form-control" required>
							<option value="" selected disabled>Pilih Salah Satu</option>
							@foreach($notAdminKabkot as $key)
							<option value="{{$key->id}}">{{$key->nama}}</option>
							@endforeach
						</select>
					</div>
					@endif
					<div class="col-md-6 form-group">
						<label for="tanggal_ujian">Tanggal Pelaksanaan </label><label style="color: red;">*</label>
						<input type="date" class="form-control" id="tanggal_ujian" name="tanggal_ujian" required>
					</div>
					<div class="col-md-12 form-group">
						<label for="lokasi_ujian">Lokasi Ujian </label><label style="color: red;">*</label>
						<input class="form-control" id="lokasi_ujian" name="lokasi_ujian" rows="3" placeholder="Masukan Lokasi" required>
					</div>
					<div style="text-align: center;" class="col-md-4 form-group">
						<label>Jumlah Peserta</label>
					</div>
					<div style="text-align: center;" class="col-md-4 form-group">
						<label>Lulus</label>
					</div>
					<div style="text-align: center;" class="col-md-4 form-group">
						<label>Tidak Lulus</label>
					</div>
					<div class="col-md-4  form-group">
						<input type="number" class="form-control" id="jumlah_yd" name="jumlah_yd" rows="3" placeholder="YD">
					</div>
					<div class="col-md-4  form-group">
						<input type="number" class="form-control" id="lulus_yd" name="lulus_yd" rows="3" placeholder="YD">
					</div>
					<div class="col-md-4  form-group">
						<input type="number" class="form-control" id="tdk_lulus_yd" name="tdk_lulus_yd" rows="3" placeholder="YD">
					</div>
					<div class="col-md-4  form-group">
						<input type="number" class="form-control" id="jumlah_yc" name="jumlah_yc" rows="3" placeholder="YC">
					</div>
					<div class="col-md-4  form-group">
						<input type="number" class="form-control" id="lulus_yc" name="lulus_yc" rows="3" placeholder="YC">
					</div>
					<div class="col-md-4  form-group">
						<input type="number" class="form-control" id="tdk_lulus_yc" name="tdk_lulus_yc" rows="3" placeholder="YC">
					</div>
					<div class="col-md-4  form-group">
						<input type="number" class="form-control" id="jumlah_yb" name="jumlah_yb" rows="3" placeholder="YB">
					</div>
					<div class="col-md-4  form-group">
						<input type="number" class="form-control" id="lulus_yb" name="lulus_yb" rows="3" placeholder="YB">
					</div>
					<div class="col-md-4  form-group">
						<input type="number" class="form-control" id="tdk_lulus_yb" name="tdk_lulus_yb" rows="3" placeholder="YB">
					</div>
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>