<table id="example" class="table table-striped table-dark align-items-center table-flush data-table dataTable no-footer">
	<thead class="thead-light">
		<tr>
			<th rowspan="2" scope="col">TANGGAL DIBUAT</th>
			<th rowspan="2" scope="col">PROVINSI</th>
			<th rowspan="2" scope="col">UPT</th>
			<th rowspan="2" scope="col">TANGGAL PELAKSANAAN UJIAN</th>
			<th rowspan="2" scope="col">LOKASI UJIAN</th>
			<th rowspan="2" scope="col">KAB/KOTA</th>
			<th style="text-align: center;" colspan="3" scope="col">JUMLAH PESERTA</th>
			<th style="text-align: center;" colspan="3" scope="col">LULUS</th>
			<th style="text-align: center;" colspan="3" scope="col">TIDAK LULUS</th>
			<th rowspan="2" scope="col">TINDAKAN</th>
		</tr>
		<tr>
			<th scope="col">YD</th>
			<th scope="col">YC</th>
			<th scope="col">YB</th>
			<th scope="col">YD</th>
			<th scope="col">YC</th>
			<th scope="col">YB</th>
			<th scope="col">YD</th>
			<th scope="col">YC</th>
			<th scope="col">YB</th>
			
		</tr>
	</thead>
	<tbody>
		@foreach($unar as $value)
		<tr>
			<td>{{$value->created_at}}</td>
			<td>{{\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama')}}</td>
			<?php $id = Crypt::encryptString($value->id) ?>
			<?php  
			$upts = \App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
			?>
			<td>{{$upts}}</td>
			<td>{{$value->tanggal_ujian}}</td>
			<td>{{$value->lokasi_ujian}}</td>
			<td>{{\App\Model\Region\KabupatenKota::where('id',$value->id_kabkot)->value('nama')}}</td>
			<td>{{$value->jumlah_yd}}</td>
			<td>{{$value->jumlah_yc}}</td>
			<td>{{$value->jumlah_yb}}</td>
			<td>{{$value->lulus_yd}}</td>
			<td>{{$value->lulus_yc}}</td>
			<td>{{$value->lulus_yb}}</td>
			<td>{{$value->tdk_lulus_yd}}</i></td>
			<td>{{$value->tdk_lulus_yc}}</td>
			<td>{{$value->tdk_lulus_yb}}</td>
			<td>
				<a href="#" data-toggle="modal" data-target="#detailModal" class="btn btn-success btn-sm"
				data-id="{{$id}}"
				data-provinsi="{{\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama')}}"
				data-upt="{{$upts}}"
				data-tanggal-ujian="{{$value->tanggal_ujian}}"
				data-lokasi="{{$value->lokasi_ujian}}"
				data-jumlah-yd="{{$value->jumlah_yd}}"
				data-jumlah-yc="{{$value->jumlah_yc}}"
				data-jumlah-yb="{{$value->jumlah_yb}}"
				data-lulus-yd="{{$value->lulus_yd}}"
				data-lulus-yc="{{$value->lulus_yc}}"
				data-lulus-yb="{{$value->lulus_yb}}"
				data-tidak-lulus-yd="{{$value->tdk_lulus_yd}}"
				data-tidak-lulus-yc="{{$value->tdk_lulus_yc}}"
				data-tidak-lulus-yb="{{$value->tdk_lulus_yb}}"
				data-pembuat="{{Auth::user()->where('id',$value->created_by)->value('name')}}"
				data-dibuat="{{$value->created_at}}"
				data-kota="{{\App\Model\Region\KabupatenKota::where('id',$value->id_kabkot)->value('nama')}}"
				><i class="fa fa-file"></i></a>

				<a href="#" data-toggle="modal" data-target="#editModal" class="btn btn-warning btn-sm" 
				data-id="{{$id}}"
				data-prov="{{\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama')}}"
				data-upt="{{$upts}}"
				data-tanggal-ujian="{{$value->tanggal_ujian}}"
				data-lokasi="{{$value->lokasi_ujian}}"
				data-jumlah-yd="{{$value->jumlah_yd}}"
				data-jumlah-yc="{{$value->jumlah_yc}}"
				data-jumlah-yb="{{$value->jumlah_yb}}"
				data-lulus-yd="{{$value->lulus_yd}}"
				data-lulus-yc="{{$value->lulus_yc}}"
				data-lulus-yb="{{$value->lulus_yb}}"
				data-tidak-lulus-yd="{{$value->tdk_lulus_yd}}"
				data-tidak-lulus-yc="{{$value->tdk_lulus_yc}}"
				data-tidak-lulus-yb="{{$value->tdk_lulus_yb}}"
				data-kota="{{\App\Model\Region\KabupatenKota::where('id',$value->id_kabkot)->value('nama')}}"
				><i class="fa fa-edit"></i></a>
				<a  class="btn btn-danger btn-sm" 
					id="getDelete" 
					data-toggle="modal" 
					data-target="#deleteModal" 
					data-id="{{$id}}"><i class="fa fa-trash"></i>
				</a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
