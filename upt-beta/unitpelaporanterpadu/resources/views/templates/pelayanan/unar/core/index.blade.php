@extends('dev.core.using')
@section('content')

@include('templates.pelayanan.unar.core.add')
@include('templates.pelayanan.unar.core.detail')
@include('templates.pelayanan.unar.core.edit')
@include('templates.pelayanan.unar.helpers.delete')
@include('templates.helpers.export')
@include('templates.helpers.import')
@include('templates.pelayanan.unar.dev.data')

<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>

<style>
	#example_filter{position: absolute; right: 20px;}
</style>
@if($getStatus != 'administator')
<?php  
$upts = \App\Model\Setting\UPT::where('office_id',Auth::user()->id_upt)->select('office_name')->distinct()->value('office_name');
?>
<br>
<br>
<center>
	<h2>{{$upts}}</h2>
	<h2>{{\App\Model\Region\Provinsi::where('id',Auth::user()->upt_provinsi)->value('nama')}}</h2>
</center>
@endif
@include('templates.helpers.pelayanan.unar.maps')
<div class="container" style="margin-top: 40px;">
	<br>
@if($getStatus == 'administrator')
	@include('templates.helpers.pelayanan.unar.chart')
	@else
	@include('templates.helpers.pelayanan.unar.chart-no-admin')
@endif
</div>

<div class="row mt-5">
	<div class="col text-left" style="padding-bottom: 10px;">
		<?php
		$url = \Request::route()->getName();
		$getKdModule = \App\Model\Menu\Module::where('menu_path',$url)->value('kdModule');
		$getCreate = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('create_acl');
		?>
		@if($getCreate == $getKdModule)
		<a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addModal"><i class="ni ni-fat-add"></i>&nbsp;TAMBAH DATA</a>
		@endif
		@include('dev.helpers.action')
	</div>

	<div class="col-xl-12 mb-5 mb-xl-0">
		<div class="card shadow" style="border:1px solid #dedede;">
			<div class="card-header border-0" style="background-color: #5f5f5f;">
				<div class="row align-items-center">
					<div class="col">
						<h3 class="mb-0" style="color: #fff;"><i class="ni ni-archive-2"></i>&nbsp;&nbsp;UNAR</h3>
					</div>
				</div>
			</div>
			<div class="table-responsive">
				<div class="widest">
					@include('templates.pelayanan.unar.core.table')
				</div>
			</div>
		</div>
	</div>

	
</div>

@include('templates.helpers.pelayanan.unar.js')

@endsection