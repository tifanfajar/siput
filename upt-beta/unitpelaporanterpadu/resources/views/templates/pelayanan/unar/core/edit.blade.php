<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">UBAH UNAR</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('unar-update')}}" class="row" method="POST" enctype="multipart/form-data">
					@csrf
					<input type="hidden" name="getStatus" value="1">
					<div class="col-md-6 form-group">
						<label for="id_prov">Provinsi</label>
						<input type="text" class="form-control" name="id_prov" id="eprovinsi" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="kode_upt">UPT</label>
						<input type="text" class="form-control" id="eupt" name="kode_upt" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="city">Kabupaten/Kota</label>
						<input type="text" class="form-control" name="id_kabkot" id="ekota" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="tanggal_ujian">Tanggal Pelaksanaan *</label>
						<input type="date" class="form-control" id="etanggal_ujian" name="tanggal_ujian" required>
					</div>
					<div class="col-md-12 form-group">
						<label for="lokasi">Lokasi Ujian</label>
						<textarea class="form-control" id="elokasi" name="lokasi_ujian" rows="3"></textarea>
					</div>
					<div style="text-align: center;" class="col-md-4 form-group">
						<label>Jumlah Peserta</label>
					</div>
					<div style="text-align: center;" class="col-md-4 form-group">
						<label>Lulus</label>
					</div>
					<div style="text-align: center;" class="col-md-4 form-group">
						<label>Tidak Lulus</label>
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="ejumlah_yd" name="jumlah_yd" rows="3" >
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="elulus_yd" name="lulus_yd" rows="3" >
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="etdk_lulus_yd" name="tdk_lulus_yd" rows="3" >
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="ejumlah_yc" name="jumlah_yc" rows="3" >
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="elulus_yc" name="lulus_yc" rows="3" >
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="etdk_lulus_yc" name="tdk_lulus_yc" rows="3" >
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="ejumlah_yb" name="jumlah_yb" rows="3" >
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="elulus_yb" name="lulus_yb" rows="3" >
					</div>
					<div class="col-md-4  form-group">
						<input type="text" class="form-control" id="etdk_lulus_yb" name="tdk_lulus_yb" rows="3" >
					</div>
					<input type="hidden" class="form-control" id="eid" name="id" placeholder="Admin">
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
						@include('dev.helpers.button.btnSimpan')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>