@include('dev.helpers.jquery')
<script type="text/javascript">
	$("#upt_provinsi").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#id_upt').html(data.html);
			}
		});
	});
</script>
<script type="text/javascript">
	$("#upt_provinsi").change(function(){
		$.ajax({
			url: "{{ url('provinsi/kabupaten/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#kabupaten').html(data.html);
			}
		});
	});
	$("#eprovinsi").change(function(){
		$.ajax({
			url: "{{ url('provinsi/kabupaten/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#ekabupaten').html(data.html);
			}
		});
	});
</script>

<script type="text/javascript">
var bla = $('#txt_name').val();
$('#txt_name').val(bla);
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#deleteModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#dmid').val(id);			
		});
	})
</script>

<!-- EDIT -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#editModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#eid').val(id);

			var prov=$(e.relatedTarget).attr('data-prov');
			$('#eprovinsi').val(prov);

			var nama_upt=$(e.relatedTarget).attr('data-upt');
			$('#eupt').val(nama_upt);

			var kota=$(e.relatedTarget).attr('data-kota');
			$('#ekota').val(kota);

			var tanggal_ujian=$(e.relatedTarget).attr('data-tanggal-ujian');
			$('#etanggal_ujian').val(tanggal_ujian);

			var lokasi=$(e.relatedTarget).attr('data-lokasi');
			$('#elokasi').val(lokasi);

			var tanggal_ujian=$(e.relatedTarget).attr('data-tanggal-ujian');
			$('#etanggal_ujian').val(tanggal_ujian);

			var jumlah_yd=$(e.relatedTarget).attr('data-jumlah-yd');
			$('#ejumlah_yd').val(jumlah_yd);

			var jumlah_yc=$(e.relatedTarget).attr('data-jumlah-yc');
			$('#ejumlah_yc').val(jumlah_yc);

			var jumlah_yb=$(e.relatedTarget).attr('data-jumlah-yb');
			$('#ejumlah_yb').val(jumlah_yb);

			var lulus_yd=$(e.relatedTarget).attr('data-lulus-yd');
			$('#elulus_yd').val(lulus_yd);

			var lulus_yc=$(e.relatedTarget).attr('data-lulus-yc');
			$('#elulus_yc').val(lulus_yc);

			var lulus_yb=$(e.relatedTarget).attr('data-lulus-yb');
			$('#elulus_yb').val(lulus_yb);

			var tdk_lulus_yd=$(e.relatedTarget).attr('data-tidak-lulus-yd');
			$('#etdk_lulus_yd').val(tdk_lulus_yd);

			var tdk_lulus_yc=$(e.relatedTarget).attr('data-tidak-lulus-yc');
			$('#etdk_lulus_yc').val(tdk_lulus_yc);

			var tdk_lulus_yb=$(e.relatedTarget).attr('data-tidak-lulus-yb');
			$('#etdk_lulus_yb').val(tdk_lulus_yb);
			
		});
	})
</script>

<!-- DETAIL -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#detailModal').on('show.bs.modal', function (e) {
			var provinsi=$(e.relatedTarget).attr('data-provinsi');
			$('#dprovinsi').val(provinsi);

			var upt=$(e.relatedTarget).attr('data-upt');
			$('#dupt').val(upt);			

			var tanggal_ujian=$(e.relatedTarget).attr('data-tanggal-ujian');
			$('#dtanggal_ujian').val(tanggal_ujian);

			var lokasi=$(e.relatedTarget).attr('data-lokasi');
			$('#dlokasi').val(lokasi);

			var jumlah_yd=$(e.relatedTarget).attr('data-jumlah-yd');
			$('#djumlah_yd').val(jumlah_yd);

			var jumlah_yc=$(e.relatedTarget).attr('data-jumlah-yc');
			$('#djumlah_yc').val(jumlah_yc);

			var jumlah_yb=$(e.relatedTarget).attr('data-jumlah-yb');
			$('#djumlah_yb').val(jumlah_yb);

			var lulus_yd=$(e.relatedTarget).attr('data-lulus-yd');
			$('#dlulus_yd').val(lulus_yd);

			var lulus_yc=$(e.relatedTarget).attr('data-lulus-yc');
			$('#dlulus_yc').val(lulus_yc);

			var lulus_yb=$(e.relatedTarget).attr('data-lulus-yb');
			$('#dlulus_yb').val(lulus_yb);

			var tdk_lulus_yd=$(e.relatedTarget).attr('data-tidak-lulus-yd');
			$('#dtdk_lulus_yd').val(tdk_lulus_yd);

			var tdk_lulus_yc=$(e.relatedTarget).attr('data-tidak-lulus-yc');
			$('#dtdk_lulus_yc').val(tdk_lulus_yc);

			var tdk_lulus_yb=$(e.relatedTarget).attr('data-tidak-lulus-yb');
			$('#dtdk_lulus_yb').val(tdk_lulus_yb);

			var created_by=$(e.relatedTarget).attr('data-pembuat');
			$('#dcreated_by').val(created_by);

			var created_at=$(e.relatedTarget).attr('data-dibuat');
			$('#dcreated_at').val(created_at);

			var kota=$(e.relatedTarget).attr('data-kota');
			$('#dkota').val(kota);

		});
	})
</script>
<!-- END DETAIL -->
