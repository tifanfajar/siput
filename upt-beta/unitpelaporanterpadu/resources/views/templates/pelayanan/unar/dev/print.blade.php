<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Lampiran Data Unar</h2>
			<hr>
			<table id="example" class="table table-bordered align-items-center" style="width: 100%; font-size: 12px;">
				<thead class="thead-light">
					<tr>
						<th rowspan="2" scope="col">NO.</th>
						<th rowspan="2" scope="col">TANGGAL DIBUAT</th>
						<th rowspan="2" scope="col">PROVINSI</th>
						<th rowspan="2" scope="col">UPT</th>
						<th rowspan="2" scope="col">TANGGAL PELAKSANAAN UJIAN</th>
						<th rowspan="2" scope="col">LOKASI UJIAN</th>
						<th rowspan="2" scope="col">KAB/KOTA</th>
						<th style="text-align: center;" colspan="3" scope="col">JUMLAH PESERTA</th>
						<th style="text-align: center;" colspan="3" scope="col">LULUS</th>
						<th style="text-align: center;" colspan="3" scope="col">TIDAK LULUS</th>
					</tr>
					<tr>
						<th scope="col">YD</th>
						<th scope="col">YC</th>
						<th scope="col">YB</th>
						<th scope="col">YD</th>
						<th scope="col">YC</th>
						<th scope="col">YB</th>
						<th scope="col">YD</th>
						<th scope="col">YC</th>
						<th scope="col">YB</th>
						
					</tr>
				</thead>
				<tbody>
					@foreach($unar as $key => $value)
					@if($value->status == 1)
					<tr>
						<td>{{$key+1}}</td>
						<td>{{$value->created_at}}</td>
						<td>{{\App\Provinsi::where('id',$value->id_prov)->value('nama')}}</td>
						<?php $id = Crypt::encryptString($value->id) ?>
						<?php  
						$connection = \DB::connection('oracle');
						$upts = $connection->table('dwh.upt_vw')->where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
						?>
						<td>{{$upts}}</td>
						<td>{{$value->tanggal_ujian}}</td>
						<td>{{$value->lokasi_ujian}}</td>
						<td>{{\App\KabupatenKota::where('id',$value->id_kabkot)->value('nama')}}</td>
						<td>{{$value->jumlah_yd}}</td>
						<td>{{$value->jumlah_yc}}</td>
						<td>{{$value->jumlah_yb}}</td>
						<td>{{$value->lulus_yd}}</td>
						<td>{{$value->lulus_yc}}</td>
						<td>{{$value->lulus_yb}}</td>
						<td>{{$value->tdk_lulus_yd}}</i></td>
						<td>{{$value->tdk_lulus_yc}}</td>
						<td>{{$value->tdk_lulus_yb}}</td>
					</tr>
					@endif
					@endforeach
				</tbody>
			</table>
			<hr>
		</div>
	</center>
</body>
</html>