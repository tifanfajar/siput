<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Lampiran Data Penanganan Piutang</h2>
			<hr>
			<table id="example" class="table table-bordered align-items-center" style="width: 100%; font-size: 8px;">
				<thead class="thead-light">
					<tr>
						<th scope="col">NO.</th>
						<th scope="col">TANGGAL DIBUAT</th>
						<th scope="col">PROVINSI</th>
						<th scope="col">UPT</th>
						<th scope="col">WAJIB BAYAR</th>
						<th scope="col">NILAI PENYERAHAN</th>
						<th scope="col">TAHUN PELIMPAHAN</th>
						<th scope="col">NAMA KPKNL</th>
						<th scope="col">TAHAPAN PENGURUSAN</th>
						<th scope="col">LUNAS</th>
						<th scope="col">ANGSURAN</th>
						<th scope="col">TANGGAL</th>
						<th scope="col">PSBDT</th>
						<th scope="col">PEMBATALAN</th>
						<th scope="col">SISA UTANG</th>
						<th scope="col">KET</th>
					</tr>
				</thead>
				<tbody>
					@foreach($petang as $key => $value)
					@if($value->status == 1)
					<tr>
						<td>{{$key+1}}</td>
						<td>{{$value->created_at}}</td>
						<td>{{\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama')}}</td>
						<?php $id = Crypt::encryptString($value->id) ?>
						<?php  
						$upts = \App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
						?>
						<td>{{$upts}}</td>
						<td>{{\App\Model\Setting\Perusahaan::where('id',$value->no_client)->value('name')}}</td>
						<td>{{$value->nilai_penyerahan}}</td>
						<td>{{$value->tahun_pelimpahan}}</td>
						<td>{{\App\Model\Region\KabupatenKota::where('id',$value->nama_kpknl)->value('nama')}}</td>
						<td>{{$value->tahapan_pengurusan}}</td>
						<td>{{$value->lunas}}</td>
						<td>{{$value->angsuran}}</td>
						<td>{{$value->tanggal}}</td>
						<td>{{$value->psbdt}}</td>
						<td>{{$value->pembatalan}}</td>
						<td>{{$value->sisa_piutang}}</td>
						<td>{{$value->keterangan}}</td>
					</tr>
					@endif
					@endforeach
				</tbody>
			</table>
			<hr>
		</div>
	</center>
</body>
</html>