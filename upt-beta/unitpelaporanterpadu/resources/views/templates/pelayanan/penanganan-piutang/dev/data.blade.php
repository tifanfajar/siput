@include('dev.helpers.jquery')
<script type="text/javascript">
	$("#id_prov").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#id_upt').html(data.html);
			}
		});
	});
</script>
<script type="text/javascript">
	$("#id_prov").change(function(){
		$.ajax({
			url: "{{ url('provinsi/kabupaten/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#nama_kpknl').html(data.html);
			}
		});
	});
	$("#eprovinsi").change(function(){
		$.ajax({
			url: "{{ url('provinsi/kabupaten/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#ekabupaten').html(data.html);
			}
		});
	});
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#deleteModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#dmid').val(id);			
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#detailApproval').on('show.bs.modal', function (e) {
			var perusahaan=$(e.relatedTarget).attr('data-perusahaan');
			$('#dano_client').val(perusahaan);		

			var nilai_penyerahan=$(e.relatedTarget).attr('data-nilai-penyerahan');
			$('#danilai_penyerahan').val(nilai_penyerahan);

			var tema=$(e.relatedTarget).attr('data-tema');
			$('#datema').val(tema);

			var tahun_pelimpahan=$(e.relatedTarget).attr('data-tahun-pelimpahan');
			$('#datahun_pelimpahan').val(tahun_pelimpahan);

			var tahapan_pengurusan=$(e.relatedTarget).attr('data-tahapan-pengurusan');
			$('#datahapan_pengurusan').val(tahapan_pengurusan);

			var lunas=$(e.relatedTarget).attr('data-lunas');
			$('#dalunas').val(lunas);

			var angsuran=$(e.relatedTarget).attr('data-angsuran');
			$('#daangsuran').val(angsuran);

			var tanggal=$(e.relatedTarget).attr('data-tanggal');
			$('#datanggal').val(tanggal);

			var psbdt=$(e.relatedTarget).attr('data-psbdt');
			$('#dapsbdt').val(psbdt);

			var pembatalan=$(e.relatedTarget).attr('data-pembatalan');
			$('#dapembatalan').val(pembatalan);

			var sisa_piutang=$(e.relatedTarget).attr('data-sisa-piutang');
			$('#dasisa_piutang').val(sisa_piutang);

			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#daketerangan').val(keterangan);

			var id_prov=$(e.relatedTarget).attr('data-id-prov');
			$('#daid_prov').val(id_prov);

			var nama_kpknl=$(e.relatedTarget).attr('data-nama-kpknl');
			$('#danama_kpknl').val(nama_kpknl);

			var id_upt=$(e.relatedTarget).attr('data-id-upt');
			$('#daid_upt').val(id_upt);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#detailReject').on('show.bs.modal', function (e) {
			var perusahaan=$(e.relatedTarget).attr('data-perusahaan');
			$('#drno_client').val(perusahaan);		

			var nilai_penyerahan=$(e.relatedTarget).attr('data-nilai-penyerahan');
			$('#drnilai_penyerahan').val(nilai_penyerahan);

			var tema=$(e.relatedTarget).attr('data-tema');
			$('#drtema').val(tema);

			var tahun_pelimpahan=$(e.relatedTarget).attr('data-tahun-pelimpahan');
			$('#drtahun_pelimpahan').val(tahun_pelimpahan);

			var tahapan_pengurusan=$(e.relatedTarget).attr('data-tahapan-pengurusan');
			$('#drtahapan_pengurusan').val(tahapan_pengurusan);

			var lunas=$(e.relatedTarget).attr('data-lunas');
			$('#drlunas').val(lunas);

			var angsuran=$(e.relatedTarget).attr('data-angsuran');
			$('#drangsuran').val(angsuran);

			var tanggal=$(e.relatedTarget).attr('data-tanggal');
			$('#drtanggal').val(tanggal);

			var psbdt=$(e.relatedTarget).attr('data-psbdt');
			$('#drpsbdt').val(psbdt);

			var pembatalan=$(e.relatedTarget).attr('data-pembatalan');
			$('#drpembatalan').val(pembatalan);

			var sisa_piutang=$(e.relatedTarget).attr('data-sisa-piutang');
			$('#drsisa_piutang').val(sisa_piutang);

			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#drketerangan').val(keterangan);

			var id_prov=$(e.relatedTarget).attr('data-id-prov');
			$('#drid_prov').val(id_prov);

			var nama_kpknl=$(e.relatedTarget).attr('data-nama-kpknl');
			$('#drnama_kpknl').val(nama_kpknl);

			var id_upt=$(e.relatedTarget).attr('data-id-upt');
			$('#drid_upt').val(id_upt);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#detailModal').on('show.bs.modal', function (e) {
			var perusahaan=$(e.relatedTarget).attr('data-perusahaan');
			$('#dno_client').val(perusahaan);		

			var nilai_penyerahan=$(e.relatedTarget).attr('data-nilai-penyerahan');
			$('#dnilai_penyerahan').val(nilai_penyerahan);

			var tema=$(e.relatedTarget).attr('data-tema');
			$('#dtema').val(tema);

			var tahun_pelimpahan=$(e.relatedTarget).attr('data-tahun-pelimpahan');
			$('#dtahun_pelimpahan').val(tahun_pelimpahan);

			var tahapan_pengurusan=$(e.relatedTarget).attr('data-tahapan-pengurusan');
			$('#dtahapan_pengurusan').val(tahapan_pengurusan);

			var lunas=$(e.relatedTarget).attr('data-lunas');
			$('#dlunas').val(lunas);

			var angsuran=$(e.relatedTarget).attr('data-angsuran');
			$('#dangsuran').val(angsuran);

			var tanggal=$(e.relatedTarget).attr('data-tanggal');
			$('#dtanggal').val(tanggal);

			var psbdt=$(e.relatedTarget).attr('data-psbdt');
			$('#dpsbdt').val(psbdt);

			var pembatalan=$(e.relatedTarget).attr('data-pembatalan');
			$('#dpembatalan').val(pembatalan);

			var sisa_piutang=$(e.relatedTarget).attr('data-sisa-piutang');
			$('#dsisa_piutang').val(sisa_piutang);

			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#dketerangan').val(keterangan);

			var id_prov=$(e.relatedTarget).attr('data-id-prov');
			$('#did_prov').val(id_prov);

			var nama_kpknl=$(e.relatedTarget).attr('data-nama-kpknl');
			$('#dnama_kpknl').val(nama_kpknl);

			var id_upt=$(e.relatedTarget).attr('data-id-upt');
			$('#did_upt').val(id_upt);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#editModal').on('show.bs.modal', function (e) {

			var perusahaan=$(e.relatedTarget).attr('data-perusahaan');
			var idperusahaan=$(e.relatedTarget).attr('data-perusahaan-id');
			var noperusahaan=$(e.relatedTarget).attr('data-perusahaan-no');
			$('#eno_client').append('<option value="'+ idperusahaan +'" selected>'+ perusahaan +'&nbsp;('+ noperusahaan +')</option>');

			var nilai_penyerahan=$(e.relatedTarget).attr('data-nilai-penyerahan');
			$('#editnilai_penyerahan').val(nilai_penyerahan);

			var tahun_pelimpahan=$(e.relatedTarget).attr('data-tahun-pelimpahan');
			$('#etahun_pelimpahan').val(tahun_pelimpahan);

			var tahapan_pengurusan=$(e.relatedTarget).attr('data-tahapan-pengurusan');
			$('#edittahapan_pengurusan').val(tahapan_pengurusan);

			var lunas=$(e.relatedTarget).attr('data-lunas');
			$('#editlunas').val(lunas);

			var angsuran=$(e.relatedTarget).attr('data-angsuran');
			$('#editangsuran').val(angsuran);

			var tanggal=$(e.relatedTarget).attr('data-tanggal');
			$('#edittanggal').val(tanggal);

			var psbdt=$(e.relatedTarget).attr('data-psbdt');
			$('#editpsbdt').val(psbdt);

			var pembatalan=$(e.relatedTarget).attr('data-pembatalan');
			$('#editpembatalan').val(pembatalan);

			var sisa_piutang=$(e.relatedTarget).attr('data-sisa-piutang');
			$('#editsisa_piutang').val(sisa_piutang);

			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#editketerangan').val(keterangan);

			var nama_upt=$(e.relatedTarget).attr('data-nama-upt');
			$('#editid_upt').val(nama_upt);

			var nama_prov=$(e.relatedTarget).attr('data-id-prov');
			$('#editid_prov').val(nama_prov);

			var nama_kpknl=$(e.relatedTarget).attr('data-nama-kpknl');
			$('#editnama_kpknl').val(nama_kpknl);

			var id=$(e.relatedTarget).attr('data-id');
			$('#editid').val(id);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#editRejectModal').on('show.bs.modal', function (e) {

			var perusahaan=$(e.relatedTarget).attr('data-perusahaan');
			var idperusahaan=$(e.relatedTarget).attr('data-perusahaan-id');
			var noperusahaan=$(e.relatedTarget).attr('data-perusahaan-no');
			$('#erno_client').append('<option value="'+ idperusahaan +'" selected>'+ perusahaan +'&nbsp;('+ noperusahaan +')</option>');

			var nilai_penyerahan=$(e.relatedTarget).attr('data-nilai-penyerahan');
			$('#erditnilai_penyerahan').val(nilai_penyerahan);

			var tahun_pelimpahan=$(e.relatedTarget).attr('data-tahun-pelimpahan');
			$('#ertahun_pelimpahan').val(tahun_pelimpahan);

			var tahapan_pengurusan=$(e.relatedTarget).attr('data-tahapan-pengurusan');
			$('#erdittahapan_pengurusan').val(tahapan_pengurusan);

			var lunas=$(e.relatedTarget).attr('data-lunas');
			$('#erditlunas').val(lunas);

			var angsuran=$(e.relatedTarget).attr('data-angsuran');
			$('#erditangsuran').val(angsuran);

			var tanggal=$(e.relatedTarget).attr('data-tanggal');
			$('#erdittanggal').val(tanggal);

			var psbdt=$(e.relatedTarget).attr('data-psbdt');
			$('#erditpsbdt').val(psbdt);

			var pembatalan=$(e.relatedTarget).attr('data-pembatalan');
			$('#erditpembatalan').val(pembatalan);

			var sisa_piutang=$(e.relatedTarget).attr('data-sisa-piutang');
			$('#erditsisa_piutang').val(sisa_piutang);

			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#erditketerangan').val(keterangan);

			var nama_upt=$(e.relatedTarget).attr('data-nama-upt');
			$('#erditid_upt').val(nama_upt);

			var nama_prov=$(e.relatedTarget).attr('data-id-prov');
			$('#erditid_prov').val(nama_prov);

			var nama_kpknl=$(e.relatedTarget).attr('data-nama-kpknl');
			$('#erditnama_kpknl').val(nama_kpknl);

			var id=$(e.relatedTarget).attr('data-id');
			$('#erditid').val(id);
		});
	})
</script>