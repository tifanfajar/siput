@extends('dev.core.using')
@section('content')

@include('templates.pelayanan.penanganan-piutang.core.add')
@include('templates.pelayanan.penanganan-piutang.core.detail')
@include('templates.pelayanan.penanganan-piutang.core.edit')
@include('templates.pelayanan.penanganan-piutang.rejected.detail')
@include('templates.pelayanan.penanganan-piutang.rejected.edit')
@include('templates.pelayanan.penanganan-piutang.approval.detail')
@include('templates.pelayanan.penanganan-piutang.helpers.delete')

@include('templates.helpers.export')
@include('templates.helpers.import')
@include('templates.pelayanan.penanganan-piutang.dev.data')

<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>

<style>
	#example_filter{position: absolute; right: 20px;}
</style>
@if($getStatus != 'administator')
<?php  
$upts = \App\Model\Setting\UPT::where('office_id',Auth::user()->id_upt)->select('office_name')->distinct()->value('office_name');
?>
<br>
<br>
<center>
	<h2>{{$upts}}</h2>
	<h2>{{\App\Model\Region\Provinsi::where('id',Auth::user()->upt_provinsi)->value('nama')}}</h2>
</center>
@endif
@include('templates.helpers.pelayanan.penanganan-piutang.maps')
<div class="container" style="margin-top: 40px;">
	<br>
	@if($getStatus == 'administrator')
	@include('templates.helpers.pelayanan.penanganan-piutang.chart')
	@else
	@include('templates.helpers.pelayanan.penanganan-piutang.chart-no-admin')
	@endif
</div>

<div class="row mt-5">
	<div class="col text-left" style="padding-bottom: 10px;">
		<?php
		$url = \Request::route()->getName();
		$getKdModule = \App\Model\Menu\Module::where('menu_path',$url)->value('kdModule');
		$getCreate = \App\Model\Privillage\Roleacl::where('module_id', $getKdModule)->where('role_id',Auth::user()->role_id)
		->value('create_acl');
		?>
		@if($getCreate == $getKdModule)
		<a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addModal"><i class="ni ni-fat-add"></i>&nbsp;TAMBAH DATA</a>
		@endif
		@include('dev.helpers.action')
	</div>
	<div class="col-xl-12 mb-5 mb-xl-0">
		<form method="POST" action="{{route('penanganan-piutang-approved')}}" enctype="multipart/form-data">
			@csrf
			<div class="card shadow" style="border:1px solid #dedede;">
				<div class="card-header border-0" style="background-color: #5f5f5f;">
					<div class="row align-items-center">
						<div class="col">
							<h3 class="mb-0" style="color: #fff;"><i class="fa fa-check-circle"></i>&nbsp;&nbsp;Menunggu Persetujuan</h3>
						</div>
				</div>
			</div>
			<div class="table-responsive">
				<div class="widest">
					@include('templates.pelayanan.penanganan-piutang.core.approval-table')
				</div>
			</div>
		</div>
		@if($getStatus == 'kepala-upt')
		<div class="col text-right" style="padding-top: 10px;">
			<button value="1" name="action" class="btn btn-success btn-sm"><i class="fa fa-check-circle"></i>&nbsp;Approved</button>
			&nbsp;
			<button value="3" name="action" class="btn btn-danger btn-sm"><i class="fa fa-times-circle"></i>&nbsp;Reject</button>
		</div>
		@endif
	</form>
</div>

@if($getStatus == 'operator')
<div class="col-xl-12 mb-5 mb-xl-0">
	<br>
	<br>
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header border-0" style="background-color: #5f5f5f;">
			<div class="row align-items-center">
				<div class="col">
					<h3 class="mb-0" style="color: #fff;"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Berkas Ditolak</h3>
				</div>
					<!-- <div class="col text-right">
						<a href="#!" class="btn btn-sm btn-primary">See all</a>
					</div> -->
				</div>
			</div>
			<div class="table-responsive">
				<!-- Projects table -->
				<div class="widest">
					@include('templates.pelayanan.penanganan-piutang.core.reject-table')
				</div>
			</div>
		</div>
	</div>
@endif

	<div class="col-xl-12 mb-5 mb-xl-0">
		<div class="card shadow" style="border:1px solid #dedede;">
			<div class="card-header border-0" style="background-color: #5f5f5f;">
				<div class="row align-items-center">
					<div class="col">
						<h3 class="mb-0" style="color: #fff;"><i class="ni ni-archive-2"></i>&nbsp;&nbsp;PENANGANAN PIUTANG</h3>
					</div>
					<!-- <div class="col text-right">
						<a href="#!" class="btn btn-sm btn-primary">See all</a>
					</div> -->
				</div>
			</div>
			<div class="table-responsive">
				<!-- Projects table -->
				<div class="widest">
					@include('templates.pelayanan.penanganan-piutang.core.table')
				</div>
			</div>
		</div>
	</div>

	
</div>

@include('templates.helpers.pelayanan.penanganan-piutang.js')

@endsection