<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">TAMBAH PENANGANAN PIUTANG</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('penanganan-piutang-save')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="eid">
					<div class="col-md-12 form-group">
						<label for="company">Perusahaan </label><label style="color: red;">*</label>
						<select name="no_client" id="no_client" class="form-control select2" required>
							<option value="" selected disabled>Pilih Salah Satu</option>
							@foreach(\App\Model\Setting\Perusahaan::all() as $value)
							<option value="{{$value->id}}">{{$value->name}}&nbsp;( NO.KLIEN : {{$value->company_id}} )</option>
							@endforeach
						</select>
						@if($getStatus !='kepala-upt')
						<a href="{{route('pengaturan/perusahaan')}}">➤ Daftar Perusahaan disini</a>
						@endif
					</div>
					<div class="col-md-6 form-group">
						<label for="nilai_penyerahan">Nilai Penyerahan </label><label style="color: red;">*</label>
						<input type="number" class="form-control" name="nilai_penyerahan" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="tahun_pelimpahan">Tahun Pelimpahan </label><label style="color: red;">*</label>
						<input id="datepicker" type="year" class="form-control" name="tahun_pelimpahan" required>
					</div>
					<div class="col-md-12 form-group">
						<label for="tahapan_pengurusan">Tahapan Pengurusan </label><label style="color: red;">*</label>
						<input type="text" class="form-control" name="tahapan_pengurusan" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="lunas">Lunas</label>
						<input type="text" class="form-control" name="lunas">
					</div>
					<div class="col-md-6 form-group">
						<label for="angsuran">Angsuran</label>
						<input type="text" class="form-control" name="angsuran">
					</div>
					<div class="col-md-12 form-group">
						<label for="tanggal">Tanggal</label>
						<input type="date" class="form-control" name="tanggal">
					</div>
					<div class="col-md-4 form-group">
						<label for="psbdt">PSBDT </label>
						<input type="text" class="form-control" name="psbdt" required>
					</div>
					<div class="col-md-4 form-group">
						<label for="pembatalan">Pembatalan </label>
						<input type="text" class="form-control" name="pembatalan" required>
					</div>
					<div class="col-md-4 form-group">
						<label for="sisa_piutang">Sisa Piutang </label>
						<input type="number" class="form-control" name="sisa_piutang" required>
					</div>
					<div class="col-md-12 form-group">
						<label for="keterangan">Keterangan </label>
						<textarea type="text" class="form-control" name="keterangan" rows="3" required></textarea>
					</div>
					@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi *</label>
						<select class="form-control" id="id_prov" name="id_prov" required>
							<option selected disabled>Khusus Kepala UPT & Operator</option>
							@foreach(\App\Model\Region\Provinsi::orderBy('id')->get() as $provinsi)
							<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="city">Kabupaten/Kota </label><label style="color: red;">*</label>
						<select name="nama_kpknl" id="nama_kpknl" class="form-control" required>
							<option value="" selected disabled>Pilih Salah Satu</option>
						</select>
					</div>
					<div class="col-md-12 form-group">
						<label for="city">UPT</label><label style="color: red;">*</label>
						<select name="id_upt" id="id_upt" class="form-control" required>
							<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
						</select>
					</div>
					@endif
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>