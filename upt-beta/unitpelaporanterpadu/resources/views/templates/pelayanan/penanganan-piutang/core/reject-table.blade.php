<table id="example" class="table table-striped table-dark align-items-center table-flush data-table dataTable no-footer">
	<thead class="thead-light">
		<tr>
			<th scope="col">TANGGAL DIBUAT</th>
			<th scope="col">PROVINSI</th>
			<th scope="col">UPT</th>
			<th scope="col">WAJIB BAYAR</th>
			<th scope="col">NILAI PENYERAHAN</th>
			<th scope="col">TAHUN PELIMPAHAN</th>
			<th scope="col">NAMA KPKNL</th>
			<th scope="col">TAHAPAN PENGURUSAN</th>
			<th scope="col">LUNAS</th>
			<th scope="col">ANGSURAN</th>
			<th scope="col">TANGGAL</th>
			<th scope="col">PSBDT</th>
			<th scope="col">PEMBATALAN</th>
			<th scope="col">SISA UTANG</th>
			<th scope="col">KET</th>
			<th>Tindakan</th>
		</tr>
	</thead>
	<tbody>
		@foreach($petang as $value)
		@if($value->status == 3)
		<tr>
			<td>{{$value->created_at}}</td>
			<td>{{\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama')}}</td>
			<?php $id = Crypt::encryptString($value->id) ?>
			<?php  
			$upts = \App\Model\Setting\UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
			?>
			<td>{{$upts}}</td>
			<td>{{\App\Company::where('id',$value->no_client)->value('name')}}</td>
			<td>{{$value->nilai_penyerahan}}</td>
			<td>{{$value->tahun_pelimpahan}}</td>
			<td>{{\App\Model\Region\KabupatenKota::where('id',$value->nama_kpknl)->value('nama')}}</td>
			<td>{{$value->tahapan_pengurusan}}</td>
			<td>{{$value->lunas}}</td>
			<td>{{$value->angsuran}}</td>
			<td>{{$value->tanggal}}</td>
			<td>{{$value->psbdt}}</td>
			<td>{{$value->pembatalan}}</td>
			<td>{{$value->sisa_piutang}}</td>
			<td>{{$value->keterangan}}</td>
			<td>
				<a href="#" data-toggle="modal" data-target="#detailReject" class="btn btn-success btn-sm"
				data-perusahaan="{{\App\Company::where('id',$value->no_client)->value('name')}}"
				data-nilai-penyerahan="{{$value->nilai_penyerahan}}"
				data-tahun-pelimpahan="{{$value->tahun_pelimpahan}}"
				data-tahapan-pengurusan="{{$value->tahapan_pengurusan}}"
				data-lunas="{{$value->lunas}}"
				data-angsuran="{{$value->angsuran}}"
				data-tanggal="{{$value->tanggal}}"
				data-psbdt="{{$value->psbdt}}"
				data-pembatalan="{{$value->pembatalan}}"
				data-sisa-piutang="{{$value->sisa_piutang}}"
				data-keterangan="{{$value->keterangan}}"
				data-id-prov="{{\App\Model\Region\Provinsi::where('id',$value->id_prov)->value('nama')}}"
				data-nama-kpknl="{{\App\Model\Region\KabupatenKota::where('id',$value->nama_kpknl)->value('nama')}}"
				data-id-upt="{{$upts}}"
				><i class="fa fa-file"></i></a>
				<a href="#" data-toggle="modal" data-target="#editReject" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
				<a  class="btn btn-danger btn-sm" 
					id="getDelete" 
					data-toggle="modal" 
					data-target="#deleteModal" 
					data-id="{{$id}}"><i class="fa fa-trash"></i>
				</a>
				<a href="{{route('penanganan-piutang-reupload',$value->id)}}" class="btn btn-primary btn-sm text-white"  onclick="return confirm('Yakin Mengupload Ulang?');"><i class="fa fa-check-circle"></i>&nbsp;Ajukan Ulang</a>
			</td>
		</tr>
		@endif
		@endforeach
	</tbody>
</table>
