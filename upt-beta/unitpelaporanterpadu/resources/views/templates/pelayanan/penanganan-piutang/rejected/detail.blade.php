<div class="modal fade" id="detailReject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">DETAIL PENANGANAN PIUTANG</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('penanganan-piutang-update')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="eid">
					<div class="col-md-12 form-group">
						<label for="company">Perusahaan *</label>
						<input type="text" class="form-control" id="drno_client" name="no_client" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="nilai_penyerahan">Nilai Penyerahan *</label>
						<input type="number" class="form-control" id="drnilai_penyerahan" name="nilai_penyerahan" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="tahun_pelimpahan">Tahun Pelimpahan *</label>
						<input id="datepicker" type="year" class="form-control" id="drtahun_pelimpahan" name="tahun_pelimpahan" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="tahapan_pengurusan">Tahapan Pengurusan *</label>
						<input type="text" class="form-control" id="drtahapan_pengurusan" name="tahapan_pengurusan" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="lunas">Lunas</label>
						<input type="text" class="form-control" id="drlunas" name="lunas" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="angsuran">Angsuran</label>
						<input type="text" class="form-control" id="drangsuran" name="angsuran" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="tanggal">Tanggal</label>
						<input type="date" class="form-control" id="drtanggal" name="tanggal" disabled>
					</div>
					<div class="col-md-4 form-group">
						<label for="psbdt">PSBDT </label>
						<input type="text" class="form-control" id="drpsbdt" name="psbdt" disabled>
					</div>
					<div class="col-md-4 form-group">
						<label for="pembatalan">Pembatalan </label>
						<input type="text" class="form-control" id="drpembatalan" name="pembatalan" disabled>
					</div>
					<div class="col-md-4 form-group">
						<label for="sisa_piutang">Sisa Piutang </label>
						<input type="text" class="form-control" id="drsisa_piutang" name="sisa_piutang" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="keterangan">Keterangan </label>
						<textarea type="text" class="form-control" id="drketerangan" name="keterangan" rows="3" disabled></textarea>
					</div>
					@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi *</label>
						<input type="text" class="form-control" id="drid_prov" name="id_prov" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="city">Kabupaten/Kota *</label>
						<input type="text" class="form-control" id="drnama_kpknl" name="nama_kpknl" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="city">UPT*</label>
						<input type="text" class="form-control" id="drid_upt" name="id_upt" disabled>
					</div>
					@endif
					<div class="modal-footer">
						@include('dev.helpers.button.btnBatal')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>