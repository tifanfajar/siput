<div class="modal fade" id="editReject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg " role="document">
		<div class="modal-content modal-lg">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="exampleModalLabel">TAMBAH PENANGANAN PIUTANG</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="{{route('penanganan-piutang-update')}}" class="row" method="POST" enctype="multipart/enctype">
					@csrf
					<input type="hidden" name="id" id="eid">
					<div class="col-md-12 form-group">
						<label for="company">Perusahaan *</label>
						<select name="no_client" id="no_client" class="form-control" required>
							<option value="" selected disabled>Pilih Salah Satu</option>
							@foreach(\App\Model\Setting\Perusahaan::all() as $value)
							<option value="{{$value->id}}">{{$value->name}}&nbsp;({{$value->company_id}})</option>
							@endforeach
						</select>
					</div>
					<div class="col-md-6 form-group">
						<label for="nilai_penyerahan">Nilai Penyerahan *</label>
						<input type="number" class="form-control" id="enilai_penyerahan" name="nilai_penyerahan" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="tahun_pelimpahan">Tahun Pelimpahan *</label>
						<input id="datepicker" type="year" class="form-control" id="etahun_pelimpahan" name="tahun_pelimpahan" required>
					</div>
					<div class="col-md-12 form-group">
						<label for="tahapan_pengurusan">Tahapan Pengurusan *</label>
						<input type="text" class="form-control" id="etahapan_pengurusan" name="tahapan_pengurusan" required>
					</div>
					<div class="col-md-6 form-group">
						<label for="lunas">Lunas</label>
						<input type="text" class="form-control" id="elunas" name="lunas">
					</div>
					<div class="col-md-6 form-group">
						<label for="angsuran">Angsuran</label>
						<input type="text" class="form-control" id="eangsuran" name="angsuran">
					</div>
					<div class="col-md-12 form-group">
						<label for="tanggal">Tanggal</label>
						<input type="date" class="form-control" id="etanggal" name="tanggal">
					</div>
					<div class="col-md-4 form-group">
						<label for="psbdt">PSBDT </label>
						<input type="text" class="form-control" id="epsbdt" name="psbdt" required>
					</div>
					<div class="col-md-4 form-group">
						<label for="pembatalan">Pembatalan </label>
						<input type="text" class="form-control" id="epembatalan" name="pembatalan" required>
					</div>
					<div class="col-md-4 form-group">
						<label for="sisa_piutang">Sisa Piutang </label>
						<input type="number" class="form-control" id="esisa_piutang" name="sisa_piutang" required>
					</div>
					<div class="col-md-12 form-group">
						<label for="keterangan">Keterangan </label>
						<textarea type="text" class="form-control" id="eketerangan" name="keterangan" rows="3" required></textarea>
					</div>
					@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
					<div class="col-md-6 form-group">
						<label for="provinsi">Provinsi *</label>
						<input type="text" class="form-control" id="eid_prov" name="id_prov" disabled>
					</div>
					<div class="col-md-6 form-group">
						<label for="city">Kabupaten/Kota *</label>
						<input type="text" class="form-control" id="enama_kpknl" name="nama_kpknl" disabled>
					</div>
					<div class="col-md-12 form-group">
						<label for="city">UPT*</label>
						<input type="text" class="form-control" id="eid_upt" name="id_upt" disabled>
					</div>
					@endif
					<div class="modal-footer">
						@include('dev.helpers.button.btnGroupForm')
					</div>
				</form>
			</div>
		</div>
	</div>
</div>