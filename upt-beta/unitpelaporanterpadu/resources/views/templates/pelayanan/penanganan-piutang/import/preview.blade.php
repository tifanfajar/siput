@extends('dev.core.using')
@section('content')
<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<div class="row mt-5">
	<div class="container">
	<form class="row" action="{{route('penanganan-piutang-import-post')}}" method="POST" id="form-validate" enctype="multipart/form-data">
	@csrf
	<input type="hidden" name="route" id="route" value="{{url('pelayanan/penanganan-piutang')}}">    
		@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
			<label for="provinsi">Provinsi</label>
			<select class="form-control" style="margin-bottom: 20px" id="upt_provinsi" name="upt_provinsi" required>
				<option selected disabled>Provinsi</option>
				@foreach(\App\Model\Region\Provinsi::all() as $provinsi)
				<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
				@endforeach
			</select>
			
			<label for="city">UPT</label>
			<select name="id_upt" id="id_upt" style="margin-bottom: 20px" class="form-control">
				<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
			</select>			
		@endif
		<label for="city">Kabupaten/Kota</label>
		<select name="id_kabkot" id="kabupaten" class="form-control" style="margin-bottom: 20px">
			<option value="" selected disabled>Pilih Kabupaten/Kota</option>
			@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
			@foreach(\App\Model\Region\KabupatenKota::where('id_prov', Auth::user()->upt_provinsi)->get() as $data)
			<option value="{{$data->id}}">{{$data->nama}}</option>
			@endforeach
			@endif
		</select>	

		<label for="city">Nama Perusahaan</label>
		<select name="nama_perusahaan" id="nama_perusahaan" class="form-control" style="margin-bottom: 20px">
			<option value="" selected disabled>Pilih Perusahaan</option>			
			@foreach(\App\Model\Setting\Perusahaan::all() as $data)
			<option value="{{$data->id}}">{{$data->name}}</option>
			@endforeach			
		</select>


	<table class="table table-responsive">

	  <thead>
	    <tr>
	      <th scope="col">Nilai Penyerahan *</th>
	      <th scope="col">Tanggal Pelimpahan *</th>
	      <th scope="col">Nama KPKNL *</th>
	      <th scope="col">Tahapanan Pengurusan *</th>
	      <th scope="col">Lunas *</th>
	      <th scope="col">Angsuran *</th>
	      <th scope="col">Tanggal *</th>
	      <th scope="col">PSBDT *</th>
	      <th scope="col">Pembatalan *</th>
	      <th scope="col">Sisa Piutang *</th>
	      <th scope="col">Keterangan *</th>
	    </tr>
	  </thead>
	  <tbody>
		@foreach($result as $piutang => $piutangs)
	    <tr>
	      <td>
	      	<input type="text" class="form-control" id="nilai_penyerahan" name="nilai_penyerahan[]" value="{{$piutangs[0]}}" placeholder="Masukan Data Nama KPKNL" data-validation="number">
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="tahun_pelimpahan" name="tahun_pelimpahan[]" value="{{$piutangs[1]}}" placeholder="Masukan Data Tahun Pelimpahan" data-validation="number">
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="nama_kpknl" name="nama_kpknl[]" value="{{$piutangs[2]}}" placeholder="Masukan Data Nama KPKNL" data-validation="name">
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="tahapanan_pengurusan" name="tahapanan_pengurusan[]" value="{{$piutangs[3]}}" placeholder="Masukan Data Nama KPKNL" data-validation="message">
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="lunas" name="lunas[]" value="{{$piutangs[4]}}" placeholder="Masukan Data" data-validation="number">
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="angsuran" name="angsuran[]" value="{{$piutangs[5]}}" placeholder="Masukan Data" data-validation="number">
	      </td>
	      <td>
	      	<input type="date" class="form-control" id="tanggal" name="tanggal[]" value="{{$piutangs[6]}}" placeholder="Masukan Data Tanggal" data-validation="date">
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="psbdt" name="psbdt[]" value="{{$piutangs[7]}}" placeholder="Masukan Data" data-validation="message">
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="pembatalan" name="pembatalan[]" value="{{$piutangs[8]}}" placeholder="Masukan Data" data-validation="message">
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="sisa_piutang" name="sisa_piutang[]" value="{{$piutangs[9]}}" placeholder="Masukan Data" data-validation="number">
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="keterangan" name="keterangan[]" value="{{$piutangs[10]}}" placeholder="Masukan Data" data-validation="message">
	      </td>
	    </tr>
	    @endforeach
	  </tbody>
	</table>
	<button type="submit" id="btn-submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Import Data</button>
	<a class="btn btn-danger" href="{{ URL::previous() }}"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Kembali</a>	
    </form>

</div>	
</div>
@include('dev.helpers.jquery')
@include('dev.helpers.validation')
<script>
	$("#upt_provinsi").change(function(){
		$.ajax({
			url: "{{ url('getUpt/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#id_upt').html(data.html);
				console.log(data.html);
			}
		});
		$.ajax({
			url: "{{ url('provinsi/kabupaten/') }}/" + $(this).val(),
			method: 'GET',
			success: function(data) {
				$('#kabupaten').html(data.html);
			}
		});
	});
</script>

@endsection

