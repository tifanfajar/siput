<style>
	.form-check {
	    position: relative;
	    display: block;
	    padding-left: 1.25rem;
	    float: left !important;
	    width: 100%;
	    margin-bottom: 7px !important;
	}
	.form-check-label {
	    margin-bottom: 0;
	    float: left;
	}
	.calBox{
		border-radius: 5px;
		border:1px solid #dedede;
	}
</style>
<div class="modal fade" id="downloadIndexSearch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h5 class="modal-title text-white" id="searchIndexPrint">DOWNLOAD AS XLS</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>
			<div class="modal-body" style="overflow: hidden; padding: 10px;">
				<center>
				<form action="{{route('penanganan-piutang-export-search',$changeUPT)}}" method="GET" enctype="multipart/enctype">
					@csrf
					<center>
						<label>Filter Data :</label>
						<hr>
						<div class="row">
						<div class="col-md-4 form-group">
							<label for="date">Tanggal</label>
							<input type="number" class="form-control" id="date" name="date">
						</div>
						<div class="col-md-4 form-group">
							<label for="date">Bulan</label>
							<input type="number" class="form-control" id="date" name="month">
						</div>
						<div class="col-md-4 form-group">
							<label for="date">Tahun</label>
							<input type="number" class="form-control" id="date" name="year">
						</div>
						</div>
						<hr>
					</center>
					<br>
					<center>
						@include('dev.helpers.button.btnGroupCetak')
					</center>
				</form>
			</center>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="downloadIndexSearch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="http://localhost:8000/templates/assets/img/excel.png" alt="" style="width: 60px; height: 50px; padding-right: 10px; ">
				<h5 class="modal-title" id="downloadIndexSearch" style="margin-top: 15px;">Unduh Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>			
			<div class="modal-body">
				<center>
				<a class="btn btn-success" style="color: white;" href="{{route('penanganan-piutang-export-search',$changeUPT)}}"><i class="ni ni-cloud-download-95"></i>&nbsp;Unduh</a>
				<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Batal</button>
				</center>
		</div>
	</div>
</div>
</div>