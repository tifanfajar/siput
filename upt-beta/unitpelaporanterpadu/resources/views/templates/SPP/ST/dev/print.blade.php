<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Lampiran Data RT</h2>
			<hr>
			<table id="example" class="table table-bordered align-items-center" style="width: 100%; font-size: 12px;">
				<thead class="thead-light">
					<tr>
						<th scope="col">#</th>
						<th scope="col">No.Tagihan</th>
						<th scope="col">No. Klien</th>
						<th scope="col">Nama Klien</th>
						<th scope="col">BHP (Rp)</th>
						<th scope="col">Tanggal ( BI CREATE DATE )</th>
						<th scope="col">Status Pembayaran</th>
						<th scope="col">Tgl Jatuh Tempo</th>
						<th scope="col">Tanggal Bayar</th>
						<th scope="col1">Tanggal Upload</th>
						<th scope="col">Service</th>
  					</tr>
				</thead>
				<tbody>
					@foreach($rt as $key => $value)
					<tr>
						<td scope="col">{{$key+1}}</td>
						<th scope="row">{{$value->no_spp}}</th>
						<td>{{$value->no_klien}}</td>
						<td>{{$value->nama_perusahaan}}</td>
						<td>Rp. {{number_format($value->tagihan, 2)}}</td>
						<td>{{\Carbon\Carbon::parse($value->bi_create_date)
						->format('d, M Y')}}</td>
						<td><center><a class="btn btn-success btn-sm text-white">Telah Bayar</a></center></td>
						<td>{{\Carbon\Carbon::parse($value->bi_end)
						->format('d, M Y')}}</td>
						<td>{{\Carbon\Carbon::parse($value->bi_money_received)
						->format('d, M Y')}}</td>
						<td>19/06/2019</td>
						<td>{{$value->service_name}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<hr>
		</div>
	</center>
</body>
</html>