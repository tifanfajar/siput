@extends('dev.core.using')
@section('content')
<?php $getStatus = \App\Model\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<div class="row mt-5">
	<div class="container">
  	<form action="{{route('rt-import-post')}}" method="POST" enctype="multipart/form-data">
	@csrf
	<input type="hidden" name="route" id="route" value="{{url('spp/rt')}}">
	@if(\App\Model\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
		<label for="provinsi">Provinsi</label>
		<select class="form-control" style="margin-bottom: 20px" id="upt_provinsi" name="upt_provinsi" required>
			<option selected disabled>Provinsi</option>
			@foreach(\App\Provinsi::all() as $provinsi)
			<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
			@endforeach
		</select>
		
		<label for="city">UPT</label>
		<select name="id_upt" id="id_upt" style="margin-bottom: 20px" class="form-control">
			<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
		</select>			
	@endif
	<table class="table table-responsive">

	  <thead>
	    <tr>
	        <th rowspan="2" scope="col">Provinsi</th>
	        <th rowspan="2" scope="col">Nama UPT</th>
	        <th rowspan="2" scope="col">No.Tagihan</th>
	        <th rowspan="2" scope="col">No.Klien</th>
	        <th rowspan="2" scope="col">Nama Klien</th>
	        <th rowspan="2" scope="col">BHP (Rp)</th>
	        <th rowspan="2" scope="col">Tanggal ( BI BEGIN )</th>
	        <th rowspan="2" scope="col">Status Pembayaran</th>
	        <th colspan="5" scope="col" style="text-align: center; background-color: #ffc107; color: #fff;">TL UPT</th>
	    </tr>
	    <tr>
	        <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Upaya / Methode</td>
	        <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Tgl Upaya</td>
	        <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Bukti Dukung</td>
	        <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Keterangan</td>
	    </tr>
	  </thead>
	  <tbody>
		@foreach($result as $rt => $rts)
		<?php 
		$connection = \DB::connection('oracle'); 
		?>
		<?php 
          $tanggal_jatuh_tempo = date("Y-m-d", strtotime($rts[6] . "- 1days"));
      	?>
	    <tr>
	      <td>
	      	{{$rts[0]}}
	      </td>
	      <td>
	      	{{$rts[1]}}
	      </td>
	      <td>
	      	{{$rts[2]}}
	      </td>
	      <td>
	      	{{$rts[3]}}
	      </td>
	      <td>
	      	{{$rts[4]}}
	      </td>
	      <td>
	      	Rp. {{number_format($rts[5], 2)}}
	      </td>
	      <td>
			{{\Carbon\Carbon::parse($rts[6])->format('d, M Y')}}
	      </td>
	      <td>
	      	{{$rts[7]}}
	      </td>
	      <td>
      		<select class="form-control metode" style="border: 2; border-radius: 6px;" id="upayametode[]" name="upayametode" data-validation="message">
				<option selected value="0" disabled>Pilih Data</option>
				@foreach(\App\Model\Metode::all() as $kabupaten)
				<option value="{{$kabupaten->id}}">{{$kabupaten->metode}}</option>
				@endforeach
			</select>
	      </td>
	      <td>
	      	<input type="date" class="form-control" id="tanggal_upaya" name="tanggal_upaya[]" placeholder="Format (yyyy-mm-dd)" required>
	      </td>
	      <td>
	      	<input type="file" name="bukti_dukung[]" required>
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="keterangan" name="keterangan[]" placeholder="Masukan Keterangan" required>
	      </td>
	    </tr>
	    <input type="hidden" name="kode_upt[]" value="{{Auth::user()->id_upt}}">
		<input type="hidden" name="id_prov[]" value="{{Auth::user()->upt_provinsi}}">
	  	<input type="hidden" name="no_tagihan[]" value="{{$rts[2]}}">
		<input type="hidden" name="no_klien[]" value="{{$rts[3]}}">
		<input type="hidden" name="nama_klien[]" value="{{$rts[4]}}">
		<input type="hidden" name="bhp[]" value="{{$rts[5]}}">
		<input type="hidden" name="status_pembayaran[]" value="{{$rts[6]}}">
		<input type="hidden" name="bi_begin[]" value="{{$rts[7]}}">
		<input type="hidden" name="tanggal_jatuh_tempo[]" value="{{$tanggal_jatuh_tempo}}">
		<input type="hidden" name="status[]" value="0">
	    @endforeach
	  </tbody>
	</table>
	<button type="submit" id="btn-submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Import Data</button>
	<a class="btn btn-danger" href="{{ URL::previous() }}"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Kembali</a>	
    </form>

</div>	
</div>
@include('dev.helpers.jquery')
@include('dev.helpers.validation')

@endsection

