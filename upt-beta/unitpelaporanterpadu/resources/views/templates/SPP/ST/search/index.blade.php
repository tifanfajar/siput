<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')

@include('templates.SPP.ST.core.detail')
@include('templates.SPP.ST.core.edit')
@include('templates.SPP.ST.import.import')
@include('templates.helpers.delete')
@include('templates.helpers.export')
@include('templates.SPP.ST.dev.data')

<style>
	#example_filter{position: absolute; right: 20px;}
</style>
<br>
<br>
<center>
	<h2>{{$getUptName}}</h2>
</center>
<br>
@include('templates.helpers.SPP.ST.maps')
<div class="container" style="margin-top: 40px;">
	<br>
	@if($getStatus == 'administrator')
	@include('templates.helpers.SPP.ST.chart')
	@else
	@include('templates.helpers.SPP.ST.chart-no-admin')
	@endif
</div>

<div class="row mt-5">
	<div class="col-md-12 mb-4 mb-xl-0" style="margin-top: 10px;">
		<div class="table-responsive" >
			<!-- Projects table -->
			@include('templates.SPP.ST.core.table')
		</div>
	</div>

	
</div>
@include('templates.helpers.SPP.ST.js-search')
@endsection