<!-- Modal -->
<div class="modal fade" id="downloadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="http://localhost:8000/templates/assets/img/excel.png" alt="" style="width: 60px; height: 50px;
				margin-bottom: 2px; padding-right: 2px; ">
				<h5 class="modal-title" id="exampleModalLabel" style="margin-top: 17px; padding-left: 10px">Unduh Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>
			<div class="modal-body" style="overflow: hidden;">
				<form action="{{route('st-export')}}" method="POST" enctype="multipart/enctype">
					<input type="hidden" name="status_pembayaran" id="status-pembayaran">
					<center>
					<br>
					@csrf
					@include('dev.helpers.button.btnGroupCetak')
					</center>
				</center>
			</form>
		</div>
	</div>
</div>
</div>