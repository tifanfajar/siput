<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')

@include('templates.SPP.ST.core.detail')
@include('templates.SPP.ST.core.detailModalAdmin')
@include('templates.SPP.ST.core.edit')
@include('templates.SPP.ST.export.preview_search')
@include('templates.SPP.ST.export.print_search')
@include('templates.SPP.ST.export.export_search')
@include('templates.SPP.ST.import.import')
@include('templates.helpers.delete')
@include('templates.SPP.ST.dev.data')

<style>
	#example_filter{position: absolute; right: 20px;}
</style>
<br>
<br>
<div style="margin-bottom: 20px;">
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
				<div class="col">
					<center>
						<h4 class="mb-0"><i class="fas fa-building"></i>&nbsp;&nbsp;{{strtoupper($getStatus)}} {{$getUptName}}</h4>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>
@include('templates.helpers.SPP.ST.maps')
<div class="container" style="margin-top: 40px;">
	@if($getStatus == 'administrator')
	@include('templates.helpers.SPP.ST.chart')
	@else
	@include('templates.helpers.SPP.ST.chart-no-admin')
	@endif
</div>

<div class="row mt-5">
	<div class="col-md-12 mb-4 mb-xl-0" style="margin-top: 10px;">
		<div class="table-responsive" >
			@include('templates.SPP.ST.core.table')
			@if($getStatus == 'administrator')


			<div class="form-group mt-5">

				<div style="margin-bottom: 20px;">
					<div class="card shadow" style="border:1px solid #dedede;">
						<div class="card-header bg-transparent">
							<div class="row align-items-center">
								<div class="col">
									<h4 class="mb-0">Catatan Petugas Pelaporan {{$getUptName}} (Apabila ada) :</h4>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="card-body col-xl-12" style="margin-left: 30px;">
								@if($catatanPetugas)
								➤ {{$catatanPetugas}}
								@else
								➤ Tidak Ada Catatan
								@endif
							</div>
						</div>
					</div>
				</div>

				<div style="margin-bottom: 20px;">
					<div class="card shadow" style="border:1px solid #dedede;">
						<div class="card-header bg-transparent">
							<div class="row align-items-center">
								<div class="col">
									<h4 class="mb-0">Catatan Kasi {{$getUptName}} (Apabila ada) :</h4>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="card-body col-xl-12" style="margin-left: 30px;">
								@if($catatanKasi)
								➤ {{$catatanKasi}}
								@else
								➤ Tidak Ada Catatan
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
			@endif
		</div>
	</div>

	
</div>

@include('templates.helpers.SPP.ST.js')
@endsection