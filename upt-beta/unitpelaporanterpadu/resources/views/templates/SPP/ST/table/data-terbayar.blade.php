<div class="col text-left" style="margin-top: 10px;margin-bottom: -20px;">
  <a class="btn btn-sm btn-success" style="color: white;" id="getPrint" data-toggle="modal" data-target="#previewModal" data-status-pembayaran="paid"><i class="ni ni-single-copy-04"></i>&nbsp;PRINT</a>
  <a class="btn btn-sm btn-info" style="color: white;" id="getPrint" data-toggle="modal" data-target="#printModal"><i class="ni ni-single-copy-04"></i>&nbsp;DOWNLOAD AS PDF</a>
  <a class="btn btn-sm btn-danger" style="color: white;" id="getDownload" data-toggle="modal" data-target="#downloadModal"><i class="ni ni-cloud-download-95"></i>&nbsp;DOWNLOAD AS XLS</a>
</div>
<div class="col-xl-12 mt-5">
  <div class="card shadow">

    <div class="card-header border-0" style="background-color: #5f5f5f;">
      <div class="row align-items-center">
        <div class="col">
          <h3 class="mb-0" style="color: #fff;">DATA @if($jenis_st == 'First Reminder')  ST-1 @elseif($jenis_st == 'Second Reminder') ST-2 @elseif($jenis_st == 'Third Reminder') ST-3 @elseif($jenis_st == 'Last Reminder') STT @endif TERBAYAR (H-60 s.d. H-30)</h3>
        </div>
      </div>
    </div>

    <div class="table-responsive">
      <!-- asdas -->
      <div class="widest">
        <table id="example" class="table table-dark table-striped align-items-center table-flush data-table ">
          <thead class="thead-light">
            <tr>
              <th scope="col">No.Tagihan</th>
              <th scope="col">No. Klien</th>
              <th scope="col">Nama Klien</th>
              <th scope="col">BHP (Rp)</th>
              <th scope="col">Tanggal ( BI CREATE DATE )</th>
              <th scope="col">Status Pembayaran</th>
              <th scope="col">Tgl Jatuh Tempo</th>
              <th scope="col">Tanggal Bayar</th>
              <th scope="col">Service</th>
            </tr>
          </thead>
          <tbody>
            @foreach($rts_paid as $rt => $value)
            <?php 
            $tanggal_jatuh_tempo = date("Y-m-d", strtotime($value->bi_create_date . "- 1days"));
            ?>
            <tr>
              <th scope="row">{{$value->no_spp}}</th>
              <td>{{$value->no_klien}}</td>
              <td>{{$value->nama_perusahaan}}</td>
              <td>Rp. {{number_format($value->tagihan, 2)}}</td>
              <td>{{\Carbon\Carbon::parse($value->bi_create_date)
               ->format('d, M Y')}}</td>
               <td><center><a class="btn btn-success btn-sm text-white">Telah Bayar</a></center></td>
               <td>{{\Carbon\Carbon::parse($tanggal_jatuh_tempo)
                   ->format('d, M Y')}}</td>
                 <td>{{\Carbon\Carbon::parse($value->bi_money_received)
                   ->format('d, M Y')}}</td>
                   <td>{{$value->service_name}}</td>
                 </tr>
                 @endforeach
               </tbody>
             </table>
           </div>
         </div>
       </div>


     </div>