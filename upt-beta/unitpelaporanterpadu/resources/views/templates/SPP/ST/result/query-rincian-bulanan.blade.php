@if($getStatus == 'administrator')
<div class="col-xl-12" style="margin-bottom: 20px;">
  <div class="card shadow" style="border:1px solid #dedede;">
    <div class="card-header bg-transparent">
      <div class="row align-items-center">
        <div class="col">
          <h4 class="mb-0">LAPORAN AKSI PENGURANGAN PIUTANG @if($jenis_st == 'First Reminder')  ST-1 @elseif($jenis_st == 'Second Reminder') ST-2 @elseif($jenis_st == 'Third Reminder') ST-3 @elseif($jenis_st == 'Last Reminder') STT @endif  WILAYAH {{$getUptName}} BULAN {{strtoupper(\App\Model\Date\ListMonth::where('id_bulan',$month)->value('nama'))}} TAHUN {{$year}}</h4>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="card-body col-xl-6">
        <table class="tab-info">
          <tr>
            <td>TOTAL @if($jenis_st == 'First Reminder')  ST-1 @elseif($jenis_st == 'Second Reminder') ST-2 @elseif($jenis_st == 'Third Reminder') ST-3 @elseif($jenis_st == 'Last Reminder') STT @endif TERBIT</td>
            <td>:</td>
            <td>{{$total}}</td>
          </tr>
          <tr>
            <td>SUDAH DI TL UPT</td>
            <td>:</td>
            <td>{{$sudah_tl_upt}}</td>
          </tr>
          <tr>
            <td>BELUM DI TL UPT</td>
            <td>:</td>
            <td>{{$belum_tl_upt}}</td>
          </tr>
        </table>
      </div>

    </div>
  </div>
</div> 
@else
<div class="col-xl-12" style="margin-bottom: 20px;">
  <div class="card shadow" style="border:1px solid #dedede;">
    <div class="card-header bg-transparent">
      <div class="row align-items-center">
        <div class="col">
          <h4 class="mb-0">HASIL QUERY RINCIAN @if($jenis_st == 'First Reminder')  ST-1 @elseif($jenis_st == 'Second Reminder') ST-2 @elseif($jenis_st == 'Third Reminder') ST-3 @elseif($jenis_st == 'Last Reminder') STT @endif {{strtoupper(\App\Model\Date\ListMonth::where('id_bulan',$month)->value('nama'))}} TAHUN {{$year}} WILAYAH {{$getUptName}}</h4>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="card-body col-xl-6">
        <table class="tab-info">
          <tr>
            <td>TOTAL ST {{$jenis_st}} TERBIT</td>
            <td>:</td>
            <td>{{$total}}</td>
          </tr>
          <tr>
            <td>SUDAH DI TL UPT</td>
            <td>:</td>
            <td>{{$sudah_tl_upt}}</td>
          </tr>
          <tr>
            <td>BELUM DI TL UPT</td>
            <td>:</td>
            <td>{{$belum_tl_upt}}</td>
          </tr>
        </table>
      </div>

    </div>
  </div>
</div>  
@endif