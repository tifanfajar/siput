<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
@extends('dev.core.using')
@section('content')

@include('templates.SPP.RT.core.detail')
@include('templates.SPP.RT.core.detailModalAdmin')
@include('templates.SPP.RT.core.edit')
@include('templates.SPP.RT.import.import')
@include('templates.helpers.delete')
@include('templates.SPP.RT.export.preview')
@include('templates.SPP.RT.export.export')
@include('templates.SPP.RT.export.print')
@include('templates.SPP.RT.dev.data')

<style>
	#example_filter{position: absolute; right: 20px;}
</style>
@if($getStatus != 'administrator')
<br>
<br>
<div style="margin-bottom: 20px;">
	<div class="card shadow" style="border:1px solid #dedede;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
				<div class="col">
					<center>
						<h4 class="mb-0"><i class="fas fa-building"></i>&nbsp;&nbsp;{{strtoupper($getStatus)}} {{$getUptName}}</h4>
					</center>
				</div>
			</div>
		</div>
	</div>
</div>
@if($statusReminder != $checkStatusReminder)
<div class="col-xl-12" style="margin-bottom: 20px;">
	<div class="card text-white bg-danger" style="border:1px;">
		<div class="card-header bg-transparent">
			<div class="row align-items-center">
				<div class="col">
					<marquee>
						<h4 class="mb-0" style="color: white;"><i class="fa fa-info-circle"></i>&nbsp;&nbsp;PERHATIAN !!! ANDA BELUM MENGIRIMKAN LAPORAN PENCEGAHAN PIUTANG {{strtoupper(\App\Model\Date\ListMonth::where('id_bulan',$month)->value('nama'))}}  ( BATAS WAKTU PENGIRIMAN TANGGAL 10 {{strtoupper(\App\Model\Date\ListMonth::where('id_bulan',$month)->value('nama'))}} {{$year}} )</h4>
					</marquee>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
@endif
@include('templates.helpers.SPP.RT.maps')
<div class="container" style="margin-top: 40px;">
	<br>
	@if($getStatus == 'administrator')
	@include('templates.helpers.SPP.RT.chart')
	@else
	@include('templates.helpers.SPP.RT.chart-no-admin')
	@endif
</div>

<div class="row mt-5">
	<div class="col-md-12 mb-4 mb-xl-0" style="margin-top: 10px;">
		<div class="table-responsive" >
			@include('templates.SPP.RT.core.table')
		</div>
	</div>

	
</div>
@include('templates.helpers.SPP.RT.js')
@endsection