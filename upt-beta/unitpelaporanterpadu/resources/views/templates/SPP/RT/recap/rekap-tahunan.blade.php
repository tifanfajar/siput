 @if($getStatus == 'administrator')
 <div class="col-xl-12 mb-5 mb-xl-0">
  <div class="card shadow">
    <div class="card-header border-0" style="background-color: #5f5f5f;">
      <div class="row align-items-center">
        <div class="col">
          <h3 class="mb-0" style="color: #fff;">REKAP PENGIRIMAN LAPORAN</h3>
        </div>
      </div>
    </div>
    <div class="table-responsive" style="overflow: hidden; width: 100%;">
      <!-- Projects table -->
      <table id="example" class="table table-dark table-striped align-items-center table-flush data-table">
        <thead class="thead-light">
          <tr>
            <th scope="col">UPT</th>
            <th scope="col">Jan</th>
            <th scope="col">Feb</th>
            <th scope="col">Mar</th>

            <th scope="col">Apr</th>
            <th scope="col">Mei</th>
            <th scope="col">Jun</th>
            <th scope="col">Jul</th>

            <th scope="col">Aug</th>
            <th scope="col">Sep</th>
            <th scope="col">Okt</th>
            <th scope="col">Nov</th>
            <th scope="col">Des</th>
            <th scope="col">Tanggal Upload</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">
              Balmon Kelas II Aceh
            </th>
            <td>Ya</td>
            <td id="backn">Belum</td>
            <td>Ya</td>
            <td id="backn">Belum</td>
            <td id="backn">Belum</td>
            <td>Ya</td>
            <td>Ya</td>
            <td id="backn">Belum</td>
            <td id="backn">Belum</td>
            <td>Ya</td>
            <td id="backn">Belum</td>
            <td>Ya</td>
            <td>12/9/2019</td>
          </tr>
        </tbody>
      </table>
      <label>Total Kirim :</label>
      <label>Total Tidak Kirim :</label>
    </div>
  </div>
</div>
@endif
@if($getStatus == 'administrator')
 <div class="col-xl-12 mt-5">
  <div class="card shadow">

    <div class="card-header border-0">
      <div class="row align-items-center">
        <div class="col">
          <h3 class="mb-0" style="color: #ffff;">REKAPITULASI DAN PERSENTASE AKSI PENCEGAHAN PIUTANG</h3>
        </div>
      </div>
    </div>

    <div class="table-responsive">
      <table id="example" class="table table-dark table-striped align-items-center table-flush data-table">
        <thead class="thead-light">
          <tr>
            <th class="tg-0pky" colspan="3"></th>
            <th class="tg-0lax" colspan="4" style="background-color: #63a53a; color: #fff; text-align: center;">RT BELUM TERBAYAR (H-30 s.d H)</th>
            <th class="tg-0lax" colspan="3" style="background-color: #ff1228; color: #fff;">RT TIDAK TERBAYAR (MENJADI REMINDER)</th>
          </tr>
          <tr>
            <td class="tg-0lax">UPT</td>
            <td class="tg-0lax">TOTAL RT TERBIT</td>
            <td class="tg-0lax">RT TERBAYAR</td>
            <td class="tg-0lax">SUDAH DI-TL UPT</td>
            <td class="tg-0lax">%</td>
            <td class="tg-0lax">BELUM DI-TL UPT</td>
            <td class="tg-0lax">%</td>
            <td class="tg-0lax">JUMLAH RT</td>
            <td class="tg-0lax">%</td>
            <td class="tg-0lax">Tanggal Upload</td>
          </tr>
        </thead>
        <tr>
          <td class="tg-0lax">JAN</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">FEB</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">MAR</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">APR</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">MEI</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">JUN</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">JUL</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">AUG</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">SEP</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">OKT</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">NOV</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
        <tr>
          <td class="tg-0lax">JAN</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>...</td>
          <td>12/9/2019</td>
        </tr>
      </table>
    </div>

  </div>

  <div class="col text-right" style="margin-top: 20px;">
    <div class="" role="group" aria-label="Basic example">
      <button type="button" class="btn btn-sm btn-primary">Print</button>
      <button type="button" class="btn btn-sm btn-primary">Download PDF</button>
      <button type="button" class="btn btn-sm btn-primary">Download XLS</button>
    </div>
  </div>

</div>  
@endif