@extends('dev.core.using')
@section('content')
<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); ?>
<div class="row mt-5">
	<div class="container">
  	<form action="{{route('rt-import-post')}}" method="POST" id="form-validate" enctype="multipart/form-data">
	@csrf
	<input type="hidden" name="route" id="route" value="{{route('rt-import-post')}}">
	@if(\App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator')
		<label for="provinsi">Provinsi</label>
		<select class="form-control" style="margin-bottom: 20px" id="upt_provinsi" name="upt_provinsi" required>
			<option selected disabled>Provinsi</option>
			@foreach(\App\Model\Region\Provinsi::all() as $provinsi)
			<option value="{{$provinsi->id_row}}">{{$provinsi->nama}}</option>
			@endforeach
		</select>
		
		<label for="city">UPT</label>
		<select name="id_upt" id="id_upt" style="margin-bottom: 20px" class="form-control">
			<option value="" selected disabled>Khusus Kepala UPT & Operator</option>
		</select>			
	@endif
		<label for="ket_operator">Keterangan Operator</label>
		<textarea class="form-control" name="ket_operator"></textarea>
	<table class="table table-responsive">

	  <thead>
	    <tr>
	        <th rowspan="2" scope="col">Provinsi</th>
	        <th rowspan="2" scope="col">Nama UPT</th>
	        <th rowspan="2" scope="col">No.Tagihan</th>
	        <th rowspan="2" scope="col">No.Klien</th>
	        <th rowspan="2" scope="col">Nama Klien</th>
	        <th rowspan="2" scope="col">BHP (Rp)</th>
	        <th rowspan="2" scope="col">Tanggal ( BI BEGIN )</th>
	        <th rowspan="2" scope="col">Status Pembayaran</th>
	        <th colspan="5" scope="col" style="text-align: center; background-color: #ffc107; color: #fff;">TL UPT</th>
	    </tr>
	    <tr>
	        <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Upaya / Methode</td>
	        <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Tgl Upaya</td>
	        <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Bukti Dukung</td>
	        <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Keterangan</td>
	    </tr>
	  </thead>
	  <tbody>
		@foreach($result as $rt => $rts)
		<?php 
          $tanggal_jatuh_tempo = date("Y-m-d", strtotime($rts[6] . "- 1days"));
      	?>
	    <tr>
	      <td>
	      	{{$rts[0]}}
	      </td>
	      <td>
	      	{{$rts[1]}}
	      </td>
	      <td>
	      	{{$rts[2]}}
	      </td>
	      <td>
	      	{{$rts[3]}}
	      </td>
	      <td>
	      	{{$rts[4]}}
	      </td>
	      <td>
	      	Rp. {{number_format($rts[5], 2)}}
	      </td>
	      <td>
			{{\Carbon\Carbon::parse($rts[6])->format('d, M Y')}}
	      </td>
	      <td>
	      	{{$rts[7]}}
	      </td>
	      <td>
      		<select class="form-control metode" style="border: 2; border-radius: 6px;" id="upayametode[]" name="upayametode[]" required>
				<option selected value="0" disabled>Pilih Data</option>
				@foreach(\App\Model\Refrension\Metode::all() as $metode)
					<option value="{{$metode->id}}" {{ $metode->metode == ucfirst($rts[8]) ? 'selected' : " "}}>{{$metode->metode}}</option>
				@endforeach
			</select>
	      </td>
	      <td>
	      	<input type="date" class="form-control" id="tanggal_upaya" name="tanggal_upaya[]" placeholder="Format (yyyy-mm-dd)" data-validation="date" value="{{$rts[9]}}" required>
	      </td>
	      <td>
	      	<input type="file" name="bukti_dukung[]" required>
	      </td>
	      <td>
	      	<input type="text" class="form-control" id="keterangan" name="keterangan[]" value="{{$rts[10]}}" placeholder="Masukan Keterangan" data-validation="message" required>
	      </td>
	    </tr>
	  	<input type="hidden" name="no_spp[]" value="{{$rts[2]}}">
	    @endforeach
	  </tbody>
	</table>
	<button type="submit" id="btn-submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Import Data</button>
	<a class="btn btn-danger" href="{{ URL::previous() }}"><i class="fa fa-times-circle"></i>&nbsp;&nbsp;Kembali</a>	
    </form>

</div>	
</div>
@include('dev.helpers.jquery')
@include('templates.SPP.RT.import.validation')
@endsection

