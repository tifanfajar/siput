<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Lampiran Data RT</h2>
			<hr>
			<table id="example" class="table table-bordered align-items-center" style="width: 100%; font-size: 12px;">
				<thead class="thead-light">
					<tr>
						<th scope="col">#</th>
						<th scope="col">No.Tagihan</th>
						<th scope="col">No. Klien</th>
						<th scope="col">Nama Klien</th>
						<th scope="col">BHP (Rp)</th>
						<th scope="col">Tanggal ( BI BEGIN )</th>
						<th scope="col">Status Pembayaran</th>
						<th scope="col">Tgl Jatuh Tempo</th>
						<th scope="col">Tanggal Bayar</th>
						<th scope="col1">Tanggal Upload</th>
						<th scope="col">Service</th>
  					</tr>
				</thead>
				<tbody>
					@foreach($rt as $key => $value)
					<tr>
						<td scope="col">{{$key+1}}</td>
						<th scope="row">{{$value->no_tagihan}}</th>
						<td>{{$value->no_klien}}</td>
						<td>{{$value->nama_klien}}</td>
						<td>Rp. {{number_format($value->bhp, 2)}}</td>
						<td>{{\Carbon\Carbon::parse($value->bi_begin)
						->format('d, M Y')}}</td>
						@if($value->status_pembayaran == 'paid')
							<td><center><a class="btn btn-success btn-sm text-white">Telah Bayar</a></center></td>
						@else
							<td><center><a class="btn btn-danger btn-sm text-white">Belum Bayar</a></center></td>
						@endif
						<td>{{\Carbon\Carbon::parse($value->tanggal_jatuh_tempo)
						->format('d, M Y')}}</td>
						<td>{{\Carbon\Carbon::parse($value->updated_at)
						->format('d, M Y')}}</td>
						<td>19/06/2019</td>
						<td>Land Mobile (private)</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<hr>
		</div>
	</center>
</body>
</html>