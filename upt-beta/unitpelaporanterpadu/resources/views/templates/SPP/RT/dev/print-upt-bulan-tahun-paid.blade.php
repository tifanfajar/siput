<!DOCTYPE html>
<?php 
  $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses');
  if($getStatus == 'administrator'){
      $listUpt = \App\Model\Setting\UPT::select('office_id','office_name','province_name')->distinct()->get();
  }else{
      $listUpt = \App\Model\Setting\UPT::where('office_id', Auth::user()->upt)->select('office_id','office_name','province_name')->distinct()->get();
  }
  if($month == 1){
    $fixMonth = 12;
    $fixYear = $year - 1;
  } else{
    $fixMonth = $month - 1;
    $fixYear = $year;
  }
?>
<html>
<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
  <center>
    <div>
      <hr>
      <h2 class="font-weight-bolder">REKAP PENGIRIMAN LAPORAN</h2>
      <hr>
      <table id="example" class="table table-bordered align-items-center">
          <thead class="thead-light">
            <tr>
              <th scope="col">UPT</th>
              <th scope="col">Pengiriman Laporan</th>
            </tr>
          </thead>
          <tbody>
            @foreach($listUpt as $key => $value)
            <tr>
              <td scope="row">
                {{$value->office_name}}
              </td>
              <?php
              $checkingPengiriman = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('upt',$value->office_name)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
              ?>
              <td>
                @if($checkingPengiriman == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      <hr>
    </div>
  </center>
</body>
</html>