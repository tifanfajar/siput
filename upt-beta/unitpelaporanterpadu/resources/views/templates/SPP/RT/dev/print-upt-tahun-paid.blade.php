<!DOCTYPE html>
<?php $getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses');
  if($getStatus == 'administrator'){
      $upt = \App\Model\Setting\UPT::select('office_name','office_id')->distinct()->get();
  }else{
      $upt = \App\Model\Setting\UPT::where('office_id', Auth::user()->upt)->select('office_name','office_id')->distinct()->get();
  }
?>
<html>
<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
  <center>
    <div>
      <hr>
      <h2 class="font-weight-bolder">REKAP PENGIRIMAN LAPORAN</h2>
      <hr>
      <table id="example" class="table table-bordered align-items-center">
          <thead class="thead-light">
            <tr>
              <th scope="col">UPT</th>
              <th scope="col">Jan</th>
              <th scope="col">Feb</th>
              <th scope="col">Mar</th>

              <th scope="col">Apr</th>
              <th scope="col">Mei</th>
              <th scope="col">Jun</th>
              <th scope="col">Jul</th>

              <th scope="col">Aug</th>
              <th scope="col">Sep</th>
              <th scope="col">Okt</th>
              <th scope="col">Nov</th>
              <th scope="col">Des</th>
            </tr>
          </thead>
          <tbody>
            @foreach($upt as $key => $value)
            <tr>
              <th scope="row">
                {{$value->office_name}}
              </th>
              <?php
              $fixYear = $year - 1; 
              $januari = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('upt', $value->office_name)->count();
              $februari = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
              $maret = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
              $april = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
              $mei = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
              $juni = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
              $juli = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
              $agustus = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
              $september = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
              $oktober = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
              $november = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
              $desember = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
              ?>
              <td>
                @if($januari == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($februari == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($maret == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($april == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($mei == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($juni == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($juli == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($agustus == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($september == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($oktober == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($november == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($desember == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      <hr>
    </div>
  </center>
</body>
</html>