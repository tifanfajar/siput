<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<center>
		<div>
			<hr>
			<h2 class="font-weight-bolder">Lampiran Data RT</h2>
			<hr>
			<table id="example" class="table table-bordered align-items-center" style="width: 100%; font-size: 12px;">
				<thead class="thead-light">
					<tr>
						<th scope="col">#</th>
						<th scope="col">No.Tagihan</th>
						<th scope="col">No. Klien</th>
						<th scope="col">Nama Klien</th>
						<th scope="col">BHP (Rp)</th>
						<th scope="col">Tanggal ( BI BEGIN )</th>
						<th scope="col">Status Pembayaran</th>
						<th scope="col">Tgl Jatuh Tempo</th>
						<th scope="col">Tanggal Bayar</th>
						<th scope="col">Tanggal Upload</th>
						<th scope="col">Service</th>
						<th scope="col">Upaya / Methode</th>
						<th scope="col">Tanggal Upaya</th>
						<th scope="col">Keterangan</th>
  					</tr>
				</thead>
				<tbody>
					@foreach($rt as $key => $value)
					<tr>
						<td scope="col">{{$key+1}}</td>
						<th scope="row">{{$value->no_spp}}</th>
						<td>{{$value->no_klien}}</td>
						<td>{{$value->nama_perusahaan}}</td>
						<td>Rp. {{number_format($value->tagihan, 2)}}</td>
						<td>{{\Carbon\Carbon::parse($value->bi_begin)
						->format('d, M Y')}}</td>
						<td>
							@if($value->status == 'paid')
							<center><a class="btn btn-success btn-sm text-white">Telah Bayar</a></center>
							@else
							<center><a class="btn btn-danger btn-sm text-white">Belum Bayar</a></center>
							@endif
						</td>
						<td>{{$value->service_name}}</td>
						<td>{{\Carbon\Carbon::parse($value->bi_end)
						->format('d, M Y')}}</td>
						<td>{{\Carbon\Carbon::parse($value->bi_money_received)
						->format('d, M Y')}}</td>
						<td>{{\Carbon\Carbon::parse($value->created_at)
						->format('d, M Y')}}</td>
						<td>{{\App\Model\Refrension\Metode::where('id', $value->upaya_metode)->value('metode')}}</td>
						<td>{{\Carbon\Carbon::parse($value->tanggal_upaya)
						->format('d, M Y')}}</td>
						<td>{{$value->keterangan}}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<hr>
		</div>
	</center>
</body>
</html>