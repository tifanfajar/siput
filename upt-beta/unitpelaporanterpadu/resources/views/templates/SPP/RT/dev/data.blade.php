@include('dev.helpers.jquery')
<script type="text/javascript">
	$(document).ready(function(){
		$('#editModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#eid').val(id);

			var no_tagihan=$(e.relatedTarget).attr('data-no-tagihan');
			$('#eno_tagihan').val(no_tagihan);		

			var no_client=$(e.relatedTarget).attr('data-no-client');
			$('#eno_client').val(no_client);

			var nama_client=$(e.relatedTarget).attr('data-nama-client');
			$('#enama_client').val(nama_client);

			var bhp=$(e.relatedTarget).attr('data-bhp');
			$('#ebhp').val(bhp);

			var bi_begin=$(e.relatedTarget).attr('data-bi-begin');
			$('#ebi_begin').val(bi_begin);

			var status_pembayaran=$(e.relatedTarget).attr('data-status-pembayaran');
			if(status_pembayaran === 'not paid'){
				$('#estatus_pembayaran').val('Belum Dibayar');
			}else{
				$('#estatus_pembayaran').val('Telah Dibayar');
			}

			var tanggal_jatuh_tempo=$(e.relatedTarget).attr('data-tanggal-jatuh-tempo');
			$('#etanggal_jatuh_tempo').val(tanggal_jatuh_tempo);

			var upaya_metode=$(e.relatedTarget).attr('data-upaya-metode');
			var upaya_metode_id=$(e.relatedTarget).attr('data-upaya-metode-id');
			$('#eupayametode').append('<option value="'+ upaya_metode_id +'" selected>'+ upaya_metode +'(sebelumnya)</option>');

			var tanggal_upaya=$(e.relatedTarget).attr('data-tanggal-upaya');
			$('#etanggal_upaya').val(tanggal_upaya);

			var bukti_dukung=$(e.relatedTarget).attr('data-bukti-dukung');
			$('#ebukti_dukung').val(bukti_dukung);

			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#eketerangan').val(keterangan);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#detailModal').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#did').val(id);

			var no_tagihan=$(e.relatedTarget).attr('data-no-tagihan');
			$('#dno_tagihan').val(no_tagihan);		

			var no_client=$(e.relatedTarget).attr('data-no-client');
			$('#dno_client').val(no_client);

			var nama_client=$(e.relatedTarget).attr('data-nama-client');
			$('#dnama_client').val(nama_client);

			var bhp=$(e.relatedTarget).attr('data-bhp');
			$('#dbhp').val(bhp);

			var bi_begin=$(e.relatedTarget).attr('data-bi-begin');
			$('#dbi_begin').val(bi_begin);

			var status_pembayaran=$(e.relatedTarget).attr('data-status-pembayaran');
			if(status_pembayaran === 'not paid'){
				$('#dstatus_pembayaran').val('Belum Dibayar');
			}else{
				$('#dstatus_pembayaran').val('Telah Dibayar');
			}

			var tanggal_jatuh_tempo=$(e.relatedTarget).attr('data-tanggal-jatuh-tempo');
			$('#dtanggal_jatuh_tempo').val(tanggal_jatuh_tempo);

			var upaya_metode=$(e.relatedTarget).attr('data-upaya-metode');
			var upaya_metode_id=$(e.relatedTarget).attr('data-upaya-metode-id');
			$('#dupayametode').append('<option value="'+ upaya_metode_id +'" selected>'+ upaya_metode +'(sebelumnya)</option>');

			var tanggal_upaya=$(e.relatedTarget).attr('data-tanggal-upaya');
			$('#dtanggal_upaya').val(tanggal_upaya);

			var bukti_dukung=$(e.relatedTarget).attr('data-bukti-dukung');
			$('#dbukti_dukung').val(bukti_dukung);

			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#dketerangan').val(keterangan);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#detailModalAdmin').on('show.bs.modal', function (e) {
			var id=$(e.relatedTarget).attr('data-id');
			$('#dida').val(id);

			var no_tagihan=$(e.relatedTarget).attr('data-no-tagihan');
			$('#dno_tagihana').val(no_tagihan);		

			var no_client=$(e.relatedTarget).attr('data-no-client');
			$('#dno_clienta').val(no_client);

			var nama_client=$(e.relatedTarget).attr('data-nama-client');
			$('#dnama_clienta').val(nama_client);

			var bhp=$(e.relatedTarget).attr('data-bhp');
			$('#dbhpa').val(bhp);

			var bi_begin=$(e.relatedTarget).attr('data-bi-begin');
			$('#dbi_begina').val(bi_begin);

			var status_pembayaran=$(e.relatedTarget).attr('data-status-pembayaran');
			if(status_pembayaran === 'not paid'){
				$('#dstatus_pembayarana').val('Belum Dibayar');
			}else{
				$('#dstatus_pembayarana').val('Telah Dibayar');
			}

			var tanggal_jatuh_tempo=$(e.relatedTarget).attr('data-tanggal-jatuh-tempo');
			$('#dtanggal_jatuh_tempoa').val(tanggal_jatuh_tempo);

			var upaya_metode=$(e.relatedTarget).attr('data-upaya-metode');
			$('#dupayametode').val(tanggal_jatuh_tempo);

			// var upaya_metode_id=$(e.relatedTarget).attr('data-upaya-metode-id');
			// $('#dupayametode').append('<option value="'+ upaya_metode_id +'" selected>'+ upaya_metode +'(sebelumnya)</option>');

			var tanggal_upaya=$(e.relatedTarget).attr('data-tanggal-upaya');
			$('#dtanggal_upayaa').val(tanggal_upaya);

			var bukti_dukung=$(e.relatedTarget).attr('data-bukti-dukung');
			$('#dbukti_dukunga').val(bukti_dukung);

			var keterangan=$(e.relatedTarget).attr('data-keterangan');
			$('#dketerangana').val(keterangan);
		});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$('#downloadModal').on('show.bs.modal', function (e) {
			var statusPembayaran=$(e.relatedTarget).attr('data-status-pembayaran');
			$('#status-pembayaran').val(statusPembayaran);
		});
	})
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#printModal').on('show.bs.modal', function (e) {
			var statusPembayaran=$(e.relatedTarget).attr('data-status-pembayaran');
			$('#pstatus-pembayaran').val(statusPembayaran);
		});
	})
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#previewModal').on('show.bs.modal', function (e) {
			var statusPembayaran=$(e.relatedTarget).attr('data-status-pembayaran');
			$('#prstatus-pembayaran').val(statusPembayaran);
		});
	})
</script>