@if($getStatus == 'administrator')
<div class="col-xl-12" style="margin-bottom: 20px;">
  <div class="card shadow" style="border:1px solid #dedede;">
    <div class="card-header bg-transparent">
      <div class="row align-items-center">
        <div class="col">
          <h4 class="mb-0">LAPORAN AKSI PENCEGAHAN PIUTANG WILAYAH {{$getUptName}} BULAN {{strtoupper(\App\Model\Date\ListMonth::where('id_bulan',$month)->value('nama'))}} TAHUN {{$year}}</h4>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="card-body col-xl-6">
        <table class="tab-info">
          <tr>
            <td>TANGGAL PENGIRIMAN LAPORAN</td>
            <td>:</td>
              @if($sendReport && $sendReport != 'INDEX')
              <td>
              {{\Carbon\Carbon::parse($sendReport)
               ->format('d, M Y')}}
             </td>
              @elseif($sendReport == 'INDEX')
              <td>TIDAK ADA LAPORAN</td>
               @else
               <td style="color: red;">
                BELUM MENGIRIM LAPORAN
                </td>
              </label>
              @endif
          </tr>
          <tr>
            <td>TOTAL RT TERBIT</td>
            <td>:</td>
            <td>{{$total}}</td>
          </tr>
          <tr>
            <td>RT TERBAYAR (H-60 s.d. H-30)</td>
            <td>:</td>
            <?php 
            if ($paid_rts > 0) {
              $paid_percent = $paid_rts / $total * 100;
            }
            else{
             $paid_percent = 0;
           }
           ?>
           <td>{{$paid_rts}} ( {!!  substr(strip_tags($paid_percent), 0, 5) !!} % )</td>
         </tr>
         <tr>
          <td>RT BELUM TERBAYAR (H-30 s.d H)</td>
          <td>:</td>
          <?php 
          if ($paid_rts > 0) {
            $nopaid_percent = $nopaid_rts / $total * 100;
          }
          else{
           $nopaid_percent = 0;
         }
         ?>
         <td>{{$nopaid_rts}} ( {!!  substr(strip_tags($nopaid_percent), 0, 5) !!} % )</td>
       </tr>
     </table>
   </div>

 </div>
</div>
</div> 
@else
<div class="col-xl-12" style="margin-bottom: 20px;">
  <div class="card shadow" style="border:1px solid #dedede;">
    <div class="card-header bg-transparent">
      <div class="row align-items-center">
        <div class="col">
          <h4 class="mb-0"><i class="fas fa-history"></i>&nbsp;&nbsp;HASIL QUERY RINCIAN TAGIHAN BULAN {{strtoupper(\App\Model\Date\ListMonth::where('id_bulan',$month)->value('nama'))}} TAHUN {{$year}} WILAYAH {{$getUptName}}</h4>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="card-body col-xl-6">
        <table class="tab-info">
          <tr>
            <td>TOTAL RT TERBIT</td>
            <td>:</td>
            <td>{{$total}}</td>
          </tr>
          <tr>
            <td>RT TERBAYAR (H-60 s.d. H-30)</td>
            <td>:</td>
            <td>{{$paid_rts}}</td>
          </tr>
          <tr>
            <td>RT BELUM TERBAYAR (H-30 s.d H)</td>
            <td>:</td>
            <td>{{$nopaid_rts}}</td>
          </tr>
        </table>
      </div>

    </div>
  </div>
</div>  
@endif