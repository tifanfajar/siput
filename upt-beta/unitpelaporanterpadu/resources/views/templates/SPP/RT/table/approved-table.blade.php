<div class="col-xl-12 mt-5">
  <div class="card shadow">

    <div class="card-header border-0" style="background-color: #5f5f5f;">
      <div class="row align-items-center">
        <div class="col">
          <h3 class="mb-0" style="color: #fff;"><i class="fa fa-pause"></i>&nbsp;&nbsp;MENUNGGU PERSETUJUAN</h3>
        </div>
      </div>
    </div>

    <div class="table-responsive">
      <!-- asdas -->
      <div class="widest">
        <table id="example" class="table table-dark table-striped align-items-center table-flush data-table ">
          <thead class="thead-light">
            <tr>
              @if($getStatus == 'kepala-upt')
              @if($freezeTime <= 10 && $freezeMonth == $month)
              <th  rowspan="2" scope="col"><input type="checkbox" id="selectAll" /></th>
              @endif
              @endif
              <th rowspan="2" scope="col">No.Tagihan</th>
              <th rowspan="2" scope="col">No.Klien</th>
              <th rowspan="2" scope="col">Nama Klien</th>
              <th rowspan="2" scope="col">BHP (Rp)</th>
              <th rowspan="2" scope="col">Tanggal ( BI BEGIN )</th>
              <th rowspan="2" scope="col">Status Pembayaran</th>
              <th rowspan="2" scope="col">Tanggal Jatuh Tempo</th>  
              <th colspan="5" scope="col" style="text-align: center; background-color: #ffc107; color: #fff;">TL UPT</th>
              <th rowspan="2" scope="col">Tindakan</th>
            </tr>
            <tr>
              <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Ya / Belum</td>
              <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Upaya / Methode</td>
              <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Tgl Upaya</td>
              <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Bukti Dukung</td>
              <td scope="col" bgcolor="#ffc107" style="border-top: 0px;">Ket</td>
            </tr>
          </thead>
          <tbody>
            @foreach($approved as $rt => $value)
            <?php 
            $tanggal_jatuh_tempo = date("Y-m-d", strtotime($value->bi_begin . "- 1days"));
            ?>
            <input type="hidden" name="getStatus" value="{{$value->active}}">
            <tr>
              @if($getStatus == 'kepala-upt')
              @if($freezeTime <= 10 && $freezeMonth == $month)
              <td><input type="checkbox" name="id[]" value="{{ $value->no_spp }}"></td>
              @endif
              @endif
              <th scope="row">{{$value->no_spp}}</th>
              <td>{{$value->no_klien}}</td>
              <td>{{$value->nama_perusahaan}}</td>
              <td>Rp. {{number_format($value->tagihan, 2)}}</td>
              <td>{{\Carbon\Carbon::parse($value->bi_begin)
               ->format('d, M Y')}}</td>
               <td style="text-align: center;"><a class="btn btn-danger btn-sm text-white">Belum Bayar</a></span></td>
               <td>{{\Carbon\Carbon::parse($tanggal_jatuh_tempo)
                 ->format('d, M Y')}}</td>
                 <td>@if(\App\Model\SPP\RincianTagihan::where('no_spp',$value->no_spp)->where('active',1)->first()) Sudah Disetujui @else Belum Disetujui @endif</td>
                  @if($getStatus == 'kepala-upt')
                  <td>{{\App\Model\Refrension\Metode::where('id',$value->upaya_metode)->value('metode')}}</td>
                  <td>{{$value->tanggal_upaya}}</td>
                  <td><a href="{{route('bukti-dukung-spp-rt',$value->bukti_dukung)}}" class="btn btn-info btn-sm"><i  class="fa fa-download"></i></a></td>
                  <td>{{$value->keterangan}}</td>
                  @endif
                </td>
                <td>-</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
    $('#selectAll').click(function(e){
      var table= $(e.target).closest('table');
      $('td input:checkbox',table).prop('checked',this.checked);
    });
  </script>