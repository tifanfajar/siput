<?php 
$getStatus = \App\Model\Privillage\Role::where('id',Auth::user()->role_id)->value('akses'); 
?>
@include('templates.SPP.RT.export.export-search')
@include('templates.SPP.RT.export.preview-search')
@include('templates.SPP.RT.export.print-search')
@include('templates.SPP.RT.dev.data')

 <style>
  #example_filter {width: 990px !important;}
  .dataTables_length{width: 560px !important;}
</style>
 @if($getStatus == 'administrator')
 <div class="col text-left" style="margin-top: 10px;margin-bottom: -20px;">
  <a class="btn btn-sm btn-success" style="color: white;" id="getPreview" data-toggle="modal" data-target="#previewModal" data-status-pembayaran="paid"><i class="ni ni-single-copy-04"></i>&nbsp;PRINT</a>
  <a class="btn btn-sm btn-info" style="color: white;" id="getPrint" data-toggle="modal" data-target="#printModal" data-status-pembayaran="paid"><i class="ni ni-single-copy-04"></i>&nbsp;DOWNLOAD AS PDF</a>
  <a class="btn btn-sm btn-danger" style="color: white;" id="getDownload" data-toggle="modal" data-target="#downloadModal" data-status-pembayaran="paid"><i class="ni ni-cloud-download-95"></i>&nbsp;DOWNLOAD AS XLS</a>
</div>
<div class="col-xl-12 mt-5">
  <div class="card shadow">

    <div class="card-header border-0" style="background-color: #5f5f5f;">
      <div class="row align-items-center">
        <div class="col">
          <h3 class="mb-0" style="color: #fff;">REKAP PENGIRIMAN LAPORAN</h3>
        </div>
      </div>
    </div>
    
    <div class="table-responsive">
      <div class="">
        <table id="example" class="table table-dark table-striped align-items-center table-flush data-table">
          <thead class="thead-light">
            <tr>
              <th scope="col">UPT</th>
              <th scope="col">Jan</th>
              <th scope="col">Feb</th>
              <th scope="col">Mar</th>

              <th scope="col">Apr</th>
              <th scope="col">Mei</th>
              <th scope="col">Jun</th>
              <th scope="col">Jul</th>

              <th scope="col">Aug</th>
              <th scope="col">Sep</th>
              <th scope="col">Okt</th>
              <th scope="col">Nov</th>
              <th scope="col">Des</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">
                {{$getUptName}}
              </th>
              <?php
              $fixYear = $year - 1; 
              $januari = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
              $februari = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
              $maret = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
              $april = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
              $mei = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
              $juni = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
              $juli = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
              $agustus = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
              $september = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
              $oktober = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
              $november = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
              $desember = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->where('upt', $getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
              ?>
              <td>
                @if($januari == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($februari == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($maret == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($april == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($mei == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($juni == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($juli == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($agustus == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($september == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($oktober == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($november == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
              <td>
                @if($desember == 0)
                <a class="btn btn-danger btn-sm text-white">BELUM</a>
                @else
                <a class="btn btn-success btn-sm text-white">YA</a>
                @endif
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endif
<br>
@if($getStatus == 'administrator')
<div class="col text-left" style="margin-top: 10px;margin-bottom: -20px;">
  <a class="btn btn-sm btn-success" style="color: white;" id="getPreview" data-toggle="modal" data-target="#previewModal" data-status-pembayaran="not_paid"><i class="ni ni-single-copy-04"></i>&nbsp;PRINT</a>
  <a class="btn btn-sm btn-info" style="color: white;" id="getPrint" data-toggle="modal" data-target="#printModal" data-status-pembayaran="not_paid"><i class="ni ni-single-copy-04"></i>&nbsp;DOWNLOAD AS PDF</a>
  <a class="btn btn-sm btn-danger" style="color: white;" id="getDownload" data-toggle="modal" data-target="#downloadModal" data-status-pembayaran="not_paid"><i class="ni ni-cloud-download-95"></i>&nbsp;DOWNLOAD AS XLS</a>
</div>
<div class="col-xl-12 mt-5">
  <div class="card shadow">

    <div class="card-header border-0">
      <div class="row align-items-center">
        <div class="col">
          <h3 class="mb-0" style="color: #ffff;">REKAPITULASI DAN PERSENTASE AKSI PENCEGAHAN PIUTANG</h3>
        </div>
      </div>
    </div>

    <div class="table-responsive">
      <table id="example" class="table table-dark table-striped align-items-center table-flush data-table">
        <thead class="thead-light">
          <tr>
            <th class="tg-0pky" colspan="3"></th>
            <th class="tg-0lax" colspan="4" style="background-color: #63a53a; color: #fff; text-align: center;">RT BELUM TERBAYAR (H-30 s.d H)</th>
            <th class="tg-0lax" colspan="3" style="background-color: #ff1228; color: #fff;">RT TIDAK TERBAYAR (MENJADI REMINDER)</th>
          </tr>
          <tr>
            <td class="tg-0lax">BULAN</td>
            <td class="tg-0lax">TOTAL RT TERBIT</td>
            <td class="tg-0lax">RT TERBAYAR</td>
            <td class="tg-0lax">SUDAH DI-TL UPT</td>
            <td class="tg-0lax">%</td>
            <td class="tg-0lax">BELUM DI-TL UPT</td>
            <td class="tg-0lax">%</td>
            <td class="tg-0lax">JUMLAH RT</td>
            <td class="tg-0lax">%</td>
            <td class="tg-0lax">Tanggal Upload</td>
          </tr>
        </thead>
        <?php
        $fixYear = $year - 1;

        //Januari
        $paid_rts_januari = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $nopaid_rts_januari = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $total_rts_januari = $paid_rts_januari + $nopaid_rts_januari;
        $sudah_tl_januari = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->where('active',1)->count();
        $belum_tl_januari = $total_rts_januari - $sudah_tl_januari;
        $percent_sudah_tl_januari = $sudah_tl_januari / $total_rts_januari * 100;
        $percent_belum_tl_januari = $belum_tl_januari / $total_rts_januari * 100;
        $jumlah_rt_januari = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        if ($jumlah_rt_januari != 0) {
          $percent_jumlah_rt_januari = $jumlah_rt_januari / $total_rts_januari * 100;
        }else{
          $percent_jumlah_rt_januari = 0;
        }
        $tanggal_upload_januari = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->select('updated_at')->distinct()->value('updated_at');

        //Februari
        $paid_rts_februari = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $nopaid_rts_februari = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $total_rts_februari = $paid_rts_februari + $nopaid_rts_februari;
        $sudah_tl_februari = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->where('active',1)->count();
        $belum_tl_februari = $total_rts_februari - $sudah_tl_februari;
        $percent_sudah_tl_februari = $sudah_tl_februari / $total_rts_februari * 100;
        $percent_belum_tl_februari = $belum_tl_februari / $total_rts_februari * 100;
        $jumlah_rt_februari = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        if ($jumlah_rt_februari != 0) {
          $percent_jumlah_rt_februari = $jumlah_rt_februari / $total_rts_februari * 100;
        }else{
          $percent_jumlah_rt_februari = 0;
        }
        $tanggal_upload_februari = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->where('active',1)->select('updated_at')->distinct()->value('updated_at');

        //Maret
        $paid_rts_maret = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $nopaid_rts_maret = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $total_rts_maret = $paid_rts_maret + $nopaid_rts_maret;
        $sudah_tl_maret = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->where('active',1)->count();
        $belum_tl_maret = $total_rts_maret - $sudah_tl_maret;
        $percent_sudah_tl_maret = $sudah_tl_maret / $total_rts_maret * 100;
        $percent_belum_tl_maret = $belum_tl_maret / $total_rts_maret * 100;
        $jumlah_rt_maret = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        if ($jumlah_rt_maret != 0) {
          $percent_jumlah_rt_maret =  $jumlah_rt_maret / $total_rts_maret * 100;
        }else{
          $percent_jumlah_rt_maret = 0;
        }
        $tanggal_upload_maret = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->distinct()->value('updated_at');

        //April
        $paid_rts_april = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $nopaid_rts_april = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $total_rts_april = $paid_rts_april + $nopaid_rts_april;
        $sudah_tl_april = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->where('active',1)->count();
        $belum_tl_april = $total_rts_april - $sudah_tl_april;
        $percent_sudah_tl_april = $sudah_tl_april / $total_rts_april * 100;
        $percent_belum_tl_april = $belum_tl_april / $total_rts_april * 100;
        $jumlah_rt_april = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        if ($jumlah_rt_april != 0) {
          $percent_jumlah_rt_april = $jumlah_rt_april/ $total_rts_april * 100;
        }else{
          $percent_jumlah_rt_april = 0;
        }
        $tanggal_upload_april = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->distinct()->value('updated_at');

        //Mei

        $paid_rts_mei = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $nopaid_rts_mei = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $total_rts_mei = $paid_rts_mei + $nopaid_rts_mei;
        $sudah_tl_mei = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->where('active',1)->count();
        $belum_tl_mei = $total_rts_mei - $sudah_tl_mei;
        $percent_sudah_tl_mei = $sudah_tl_mei / $total_rts_mei * 100;
        $percent_belum_tl_mei = $belum_tl_mei / $total_rts_mei * 100;
        $jumlah_rt_mei = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        if ($jumlah_rt_mei != 0) {
          $percent_jumlah_rt_mei =  $jumlah_rt_mei / $total_rts_mei * 100;
        }else{
          $percent_jumlah_rt_mei = 0;
        }
        $tanggal_upload_mei = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->distinct()->value('updated_at');

        //Juni
        $paid_rts_juni = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $nopaid_rts_juni = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $total_rts_juni = $paid_rts_juni + $nopaid_rts_juni;
        $sudah_tl_juni = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->where('active',1)->count();
        $belum_tl_juni = $total_rts_juni - $sudah_tl_juni;
        $percent_sudah_tl_juni = $sudah_tl_juni / $total_rts_juni * 100;
        $percent_belum_tl_juni = $belum_tl_juni / $total_rts_juni * 100;
        $jumlah_rt_juni = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        if ($jumlah_rt_juni != 0) {
          $percent_jumlah_rt_juni =  $jumlah_rt_juni / $total_rts_juni * 100;
        }else{
          $percent_jumlah_rt_juni = 0;
        }
        $tanggal_upload_juni = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->distinct()->value('updated_at');

        //Juli
        $paid_rts_juli = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $nopaid_rts_juli = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $total_rts_juli = $paid_rts_juli + $nopaid_rts_juli;
        $sudah_tl_juli = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->where('status',1)->count();
        $belum_tl_juli = $total_rts_juli - $sudah_tl_juli;
        $percent_sudah_tl_juli = $sudah_tl_juli / $total_rts_juli * 100;
        $percent_belum_tl_juli = $belum_tl_juli / $total_rts_juli * 100;
        $jumlah_rt_juli = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        if ($jumlah_rt_juli != 0) {
          $percent_jumlah_rt_juli = $jumlah_rt_juli / $total_rts_juli * 100;
        }else{
          $percent_jumlah_rt_juli = 0;
        }
        $tanggal_upload_juli = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->distinct()->value('updated_at');

        //Agustus
        $paid_rts_agustus = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $nopaid_rts_agustus = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $total_rts_agustus = $paid_rts_agustus + $nopaid_rts_agustus;
        $sudah_tl_agustus = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->where('active',1)->count();
        $belum_tl_agustus = $total_rts_agustus - $sudah_tl_agustus;
        $percent_sudah_tl_agustus = $sudah_tl_agustus / $total_rts_agustus * 100;
        $percent_belum_tl_agustus = $belum_tl_agustus / $total_rts_agustus * 100;
        $jumlah_rt_agustus = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        if ($jumlah_rt_agustus != 0) {
          $percent_jumlah_rt_agustus =  $jumlah_rt_agustus / $total_rts_agustus * 100;
        }else{
          $percent_jumlah_rt_agustus = 0;
        }
        $tanggal_upload_agustus = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->distinct()->value('updated_at');

        //September
        $paid_rts_september = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $nopaid_rts_september = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $total_rts_september = $paid_rts_september + $nopaid_rts_september;
        $sudah_tl_september = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->where('active',1)->count();
        $belum_tl_september = $total_rts_september - $sudah_tl_september;
        $percent_sudah_tl_september = $sudah_tl_september / $total_rts_september * 100;
        $percent_belum_tl_september = $belum_tl_september / $total_rts_september * 100;
        $jumlah_rt_september = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        if ($jumlah_rt_september != 0) {
          $percent_jumlah_rt_september =  $jumlah_rt_september / $total_rts_september * 100;
        }else{
          $percent_jumlah_rt_september = 0;
        }
        $tanggal_upload_september = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->distinct()->value('updated_at');

        //Oktober
        $paid_rts_oktober = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $nopaid_rts_oktober = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $total_rts_oktober = $paid_rts_oktober + $nopaid_rts_oktober;
        $sudah_tl_oktober = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->where('active',1)->count();
        $belum_tl_oktober = $total_rts_oktober - $sudah_tl_oktober;
        $percent_sudah_tl_oktober = $sudah_tl_oktober / $total_rts_oktober * 100;
        $percent_belum_tl_oktober = $belum_tl_oktober / $total_rts_oktober * 100;
        $jumlah_rt_oktober = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        if ($jumlah_rt_oktober != 0) {
          $percent_jumlah_rt_oktober = $jumlah_rt_oktober / $total_rts_oktober * 100;
        }else{
          $percent_jumlah_rt_oktober = 0;
        }
        $tanggal_upload_oktober = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->distinct()->value('updated_at');

        //November
        $paid_rts_november = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $nopaid_rts_november = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $total_rts_november = $paid_rts_november + $nopaid_rts_november;
        $sudah_tl_november = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->where('active',1)->count();
        $belum_tl_november = $total_rts_november - $sudah_tl_november;
        $percent_sudah_tl_november = $sudah_tl_november / $total_rts_november * 100;
        $percent_belum_tl_november = $belum_tl_november / $total_rts_november * 100;
        $jumlah_rt_november = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        if ($jumlah_rt_november != 0) {
          $percent_jumlah_rt_november =  $jumlah_rt_november / $total_rts_november * 100;
        }else{
          $percent_jumlah_rt_november = 0;
        }
        $tanggal_upload_november = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->distinct()->value('updated_at');

        //Desember
        $paid_rts_desember = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $nopaid_rts_desember = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        $total_rts_desember = $paid_rts_desember + $nopaid_rts_desember;
        $sudah_tl_desember = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->where('active',1)->count();
        $belum_tl_desember = $total_rts_desember - $sudah_tl_desember;
        $percent_sudah_tl_desember = $sudah_tl_desember / $total_rts_desember * 100;
        $percent_belum_tl_desember = $belum_tl_desember / $total_rts_desember * 100;
        $jumlah_rt_desember = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
        if ($jumlah_rt_desember != 0) {
          $percent_jumlah_rt_desember =  $jumlah_rt_desember / $total_rts_desember * 100;
        }else{
          $percent_jumlah_rt_desember = 0;
        }
        $tanggal_upload_desember = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->distinct()->value('updated_at');
        ?>
        <tr>
          <td class="tg-0lax">JAN</td>
          <td>{{$total_rts_januari}}</td>
          <td>{{$paid_rts_januari}}</td>
          <td>{{$sudah_tl_januari}}</td>
          <td>{!!  substr(strip_tags($percent_sudah_tl_januari), 0, 5) !!} %</td>
          <td>{{$belum_tl_januari}}</td>
          <td>{!!  substr(strip_tags($percent_belum_tl_januari), 0, 5) !!} %</td>
          <td>{{$jumlah_rt_januari}}</td>
          <td>{!!  substr(strip_tags($percent_jumlah_rt_januari), 0, 5) !!} %</td>
          <td>
            @if($tanggal_upload_januari)
            {{\Carbon\Carbon::parse($tanggal_upload_januari)
                   ->format('d, M Y')}}
            @else
            Belum Mengirim
            @endif
          </td>
        </tr>
        <tr>
          <td class="tg-0lax">FEB</td>
          <td>{{$total_rts_februari}}</td>
          <td>{{$paid_rts_februari}}</td>
          <td>{{$sudah_tl_februari}}</td>
          <td>{!!  substr(strip_tags($percent_sudah_tl_februari), 0, 5) !!} %</td>
          <td>{{$belum_tl_februari}}</td>
          <td>{!!  substr(strip_tags($percent_belum_tl_februari), 0, 5) !!} %</td>
          <td>{{$jumlah_rt_februari}}</td>
          <td>{!!  substr(strip_tags($percent_jumlah_rt_februari), 0, 5) !!} %</td>
          <td>
            @if($tanggal_upload_februari)
           {{\Carbon\Carbon::parse($tanggal_upload_februari)
                   ->format('d, M Y')}}
            @else
            Belum Mengirim
            @endif
          </td>
        </tr>
        <tr>
          <td class="tg-0lax">MAR</td>
          <td>{{$total_rts_maret}}</td>
          <td>{{$paid_rts_maret}}</td>
          <td>{{$sudah_tl_maret}}</td>
          <td>{!!  substr(strip_tags($percent_sudah_tl_maret), 0, 5) !!} %</td>
          <td>{{$belum_tl_maret}}</td>
          <td>{!!  substr(strip_tags($percent_belum_tl_maret), 0, 5) !!} %</td>
          <td>{{$jumlah_rt_maret}}</td>
          <td>{!!  substr(strip_tags($percent_jumlah_rt_maret), 0, 5) !!} %</td>
          <td>
            @if($tanggal_upload_maret)
            {{\Carbon\Carbon::parse($tanggal_upload_maret)
               ->format('d, M Y')}}
            @else
            Belum Mengirim
            @endif
          </td>
        </tr>
        <tr>
          <td class="tg-0lax">APR</td>
          <td>{{$total_rts_april}}</td>
          <td>{{$paid_rts_april}}</td>
          <td>{{$sudah_tl_april}}</td>
          <td>{!!  substr(strip_tags($percent_sudah_tl_april), 0, 5) !!} %</td>
          <td>{{$belum_tl_april}}</td>
          <td>{!!  substr(strip_tags($percent_belum_tl_april), 0, 5) !!} %</td>
          <td>{{$jumlah_rt_april}}</td>
          <td>{!!  substr(strip_tags($percent_jumlah_rt_april), 0, 5) !!} %</td>
          <td>
            @if($tanggal_upload_april)
            {{\Carbon\Carbon::parse($tanggal_upload_april)
                   ->format('d, M Y')}}
            @else
            Belum Mengirim
            @endif
          </td>
        </tr>
        <tr>
          <td class="tg-0lax">MEI</td>
          <td>{{$total_rts_mei}}</td>
          <td>{{$paid_rts_mei}}</td>
          <td>{{$sudah_tl_mei}}</td>
          <td>{!!  substr(strip_tags($percent_sudah_tl_mei), 0, 5) !!} %</td>
          <td>{{$belum_tl_mei}}</td>
          <td>{!!  substr(strip_tags($percent_belum_tl_mei), 0, 5) !!} %</td>
          <td>{{$jumlah_rt_mei}}</td>
          <td>{!!  substr(strip_tags($percent_jumlah_rt_mei), 0, 5) !!} %</td>
          <td>
            @if($tanggal_upload_mei)
            {{\Carbon\Carbon::parse($tanggal_upload_mei)
                   ->format('d, M Y')}}
            @else
            Belum Mengirim
            @endif
          </td>
        </tr>
        <tr>
          <td class="tg-0lax">JUN</td>
          <td>{{$total_rts_juni}}</td>
          <td>{{$paid_rts_juni}}</td>
          <td>{{$sudah_tl_juni}}</td>
          <td>{!!  substr(strip_tags($percent_sudah_tl_juni), 0, 5) !!} %</td>
          <td>{{$belum_tl_juni}}</td>
          <td>{!!  substr(strip_tags($percent_belum_tl_juni), 0, 5) !!} %</td>
          <td>{{$jumlah_rt_juni}}</td>
          <td>{!!  substr(strip_tags($percent_jumlah_rt_juni), 0, 5) !!} %</td>
          <td>
            @if($tanggal_upload_juni)
            {{\Carbon\Carbon::parse($tanggal_upload_juni)
                   ->format('d, M Y')}}
            @else
            Belum Mengirim
            @endif
          </td>
        </tr>
        <tr>
          <td class="tg-0lax">JUL</td>
          <td>{{$total_rts_juli}}</td>
          <td>{{$paid_rts_juli}}</td>
          <td>{{$sudah_tl_juli}}</td>
          <td>{!!  substr(strip_tags($percent_sudah_tl_juli), 0, 5) !!} %</td>
          <td>{{$belum_tl_juli}}</td>
          <td>{!!  substr(strip_tags($percent_belum_tl_juli), 0, 5) !!} %</td>
          <td>{{$jumlah_rt_juli}}</td>
          <td>{!!  substr(strip_tags($percent_jumlah_rt_juli), 0, 5) !!} %</td>
          <td>
            @if($tanggal_upload_juli)
            {{\Carbon\Carbon::parse($tanggal_upload_juli)
                   ->format('d, M Y')}}
            @else
            Belum Mengirim
            @endif
          </td>
        </tr>
        <tr>
          <td class="tg-0lax">AUG</td>
          <td>{{$total_rts_agustus}}</td>
          <td>{{$paid_rts_agustus}}</td>
          <td>{{$sudah_tl_agustus}}</td>
          <td>{!!  substr(strip_tags($percent_sudah_tl_agustus), 0, 5) !!} %</td>
          <td>{{$belum_tl_agustus}}</td>
          <td>{!!  substr(strip_tags($percent_belum_tl_agustus), 0, 5) !!} %</td>
          <td>{{$jumlah_rt_agustus}}</td>
          <td>{!!  substr(strip_tags($percent_jumlah_rt_agustus), 0, 5) !!} %</td>
          <td>
            @if($tanggal_upload_agustus)
            {{\Carbon\Carbon::parse($tanggal_upload_agustus)
                   ->format('d, M Y')}}
            @else
            Belum Mengirim
            @endif
          </td>
        </tr>
        <tr>
          <td class="tg-0lax">SEP</td>
          <td>{{$total_rts_september}}</td>
          <td>{{$paid_rts_september}}</td>
          <td>{{$sudah_tl_september}}</td>
          <td>{!!  substr(strip_tags($percent_sudah_tl_september), 0, 5) !!} %</td>
          <td>{{$belum_tl_september}}</td>
          <td>{!!  substr(strip_tags($percent_belum_tl_september), 0, 5) !!} %</td>
          <td>{{$jumlah_rt_september}}</td>
          <td>{!!  substr(strip_tags($percent_jumlah_rt_september), 0, 5) !!} %</td>
          <td>
            @if($tanggal_upload_september)
            {{\Carbon\Carbon::parse($tanggal_upload_september)
                   ->format('d, M Y')}}
            @else
            Belum Mengirim
            @endif
          </td>
        </tr>
        <tr>
          <td class="tg-0lax">OKT</td>
          <td>{{$total_rts_oktober}}</td>
          <td>{{$paid_rts_oktober}}</td>
          <td>{{$sudah_tl_oktober}}</td>
          <td>{!!  substr(strip_tags($percent_sudah_tl_oktober), 0, 5) !!} %</td>
          <td>{{$belum_tl_oktober}}</td>
          <td>{!!  substr(strip_tags($percent_belum_tl_oktober), 0, 5) !!} %</td>
          <td>{{$jumlah_rt_oktober}}</td>
          <td>{!!  substr(strip_tags($percent_jumlah_rt_oktober), 0, 5) !!} %</td>
          <td>
            @if($tanggal_upload_oktober)
            {{\Carbon\Carbon::parse($tanggal_upload_oktober)
                   ->format('d, M Y')}}
            @else
            Belum Mengirim
            @endif
          </td>
        </tr>
        <tr>
          <td class="tg-0lax">NOV</td>
          <td>{{$total_rts_november}}</td>
          <td>{{$paid_rts_november}}</td>
          <td>{{$sudah_tl_november}}</td>
          <td>{!!  substr(strip_tags($percent_sudah_tl_november), 0, 5) !!} %</td>
          <td>{{$belum_tl_november}}</td>
          <td>{!!  substr(strip_tags($percent_belum_tl_november), 0, 5) !!} %</td>
          <td>{{$jumlah_rt_november}}</td>
          <td>{!!  substr(strip_tags($percent_jumlah_rt_november), 0, 5) !!} %</td>
          <td>
            @if($tanggal_upload_november)
            {{\Carbon\Carbon::parse($tanggal_upload_november)
                   ->format('d, M Y')}}
            @else
            Belum Mengirim
            @endif
          </td>
        </tr>
        <tr>
          <td class="tg-0lax">DES</td>
          <td>{{$total_rts_desember}}</td>
          <td>{{$paid_rts_desember}}</td>
          <td>{{$sudah_tl_desember}}</td>
          <td>{!!  substr(strip_tags($percent_sudah_tl_desember), 0, 5) !!} %</td>
          <td>{{$belum_tl_desember}}</td>
          <td>{!!  substr(strip_tags($percent_belum_tl_desember), 0, 5) !!} %</td>
          <td>{{$jumlah_rt_desember}}</td>
          <td>{!!  substr(strip_tags($percent_jumlah_rt_desember), 0, 5) !!} %</td>
          <td>
            @if($tanggal_upload_desember)
            {{\Carbon\Carbon::parse($tanggal_upload_desember)
                   ->format('d, M Y')}}
            @else
            Belum Mengirim
            @endif
          </td>
        </tr>
      </table>
    </div>

  </div>
  @endif