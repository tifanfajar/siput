<!-- Modal -->
<div class="modal fade" id="previewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<img src="http://localhost:8000/templates/assets/img/pdf.ico" alt="" style="width: 60px; height: 50px; margin-bottom: 2px; padding-right: 2px; ">
				<h5 class="modal-title" id="exampleModalLabel" style="margin-top: 17px; padding-left: 10px">Cetak Data</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div style="width: 100%; display: block; height: 1px; background-color: #dedede;"></div>
			<div class="modal-body" style="overflow: hidden;">
				<form action="{{route('rt-preview-search')}}" method="POST" enctype="multipart/enctype">
					<input type="hidden" name="status_pembayaran" id="prstatus-pembayaran">					
					<input type="hidden" name="month" value="{{$month}}">
					<input type="hidden" name="year" value="{{$year}}">
					<input type="hidden" name="getUptName" value="{{$getUptName}}">
					<center>
					@csrf							
						<button type="submit" class="btn btn-success" target="_blank"><i class="ni ni-single-copy-04"></i>&nbsp;&nbsp;Preview AS PDF</button>
						@include('dev.helpers.button.btnBatal')
					</center>
				</center>
			</form>
		</div>
	</div>
</div>
</div>