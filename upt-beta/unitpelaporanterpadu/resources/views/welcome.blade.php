@extends('dev.core.using')
@section('content')
<br>
<br>
<div class="row">

	<div class="col-xl-4 col-lg-6 mb-xl-3">
		<div class="card card-stats mb-4 mb-xl-0">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<h5 class="card-title text-uppercase text-muted mb-0">Layanan Konsultasi dan Pengaduan</h5>

						<span class="h2 font-weight-bold mb-0">0 %</span>
					</div>
					<div class="col-auto">
						<div class="icon icon-shape bg-info text-white rounded-circle shadow">
							<i class="ni ni-support-16"></i>
						</div>
					</div>
				</div>
				<p class="mt-3 mb-0 text-muted text-sm">
					<span class="text-nowrap">Layanan Konsultasi dan Pengaduan</span>
				</p>
			</div>
		</div>
	</div>

	<div class="col-xl-4 col-lg-6 mb-xl-3">
		<div class="card card-stats mb-4 mb-xl-0">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<h5 class="card-title text-uppercase text-muted mb-0">Sosialisasi dan Bimtek</h5>
						<span class="h2 font-weight-bold mb-0">0 %</span>
					</div>
					<div class="col-auto">
						<div class="icon icon-shape bg-info text-white rounded-circle shadow">
							<i class="ni ni-send"></i>
						</div>
					</div>
				</div>
				<p class="mt-3 mb-0 text-muted text-sm">
					<span class="text-nowrap">Sosialisasi dan Bimtek</span>
				</p>
			</div>
		</div>
	</div>

	<div class="col-xl-4 col-lg-6 mb-xl-3">
		<div class="card card-stats mb-4 mb-xl-0">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<h5 class="card-title text-uppercase text-muted mb-0">Penanganan Tagihan dan Piutang BHP Frekuensi Radio</h5>
						<span class="h2 font-weight-bold mb-0">0 %</span>
					</div>
					<div class="col-auto">
						<div class="icon icon-shape bg-info text-white rounded-circle shadow">
							<i class="ni ni-money-coins"></i>
						</div>
					</div>
				</div>
				<p class="mt-3 mb-0 text-muted text-sm">
					<span class="text-nowrap">Penanganan Tagihan dan Piutang BHP Frekuensi Radio</span>
				</p>
			</div>
		</div>
	</div>

	<div class="col-xl-4 col-lg-6 mb-xl-3">
		<div class="card card-stats mb-4 mb-xl-0">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<h5 class="card-title text-uppercase text-muted mb-0">Penanganan Piutang Yang Telah Dilimpahkan</h5>
						<span class="h2 font-weight-bold mb-0">0%</span>
					</div>
					<div class="col-auto">
						<div class="icon icon-shape bg-info text-white rounded-circle shadow">
							<i class="ni ni-money-coins"></i>
						</div>
					</div>
				</div>
				<p class="mt-3 mb-0 text-muted text-sm">
					<span class="text-nowrap">Penanganan Piutang Yang Telah Dilimpahkan</span>
				</p>
			</div>
		</div>
	</div>

	<div class="col-xl-4 col-lg-6 mb-xl-3">
		<div class="card card-stats mb-4 mb-xl-0">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<h5 class="card-title text-uppercase text-muted mb-0">Pelaksanaan UNAR</h5>
						<span class="h2 font-weight-bold mb-0">0 %</span>
					</div>
					<div class="col-auto">
						<div class="icon icon-shape bg-info text-white rounded-circle shadow">
							<i class="fas fa-percent"></i>
						</div>
					</div>
				</div>
				<p class="mt-3 mb-0 text-muted text-sm">
					<span class="text-nowrap">Pelaksanaan UNAR</span>
				</p>
			</div>
		</div>
	</div>

	<div class="col-xl-4 col-lg-6 mb-xl-3">
		<div class="card card-stats mb-4 mb-xl-0">
			<div class="card-body">
				<div class="row">
					<div class="col">
						<h5 class="card-title text-uppercase text-muted mb-0">Validasi dan Inspeksi</h5>
						<span class="h2 font-weight-bold mb-0">0 %</span>
					</div>
					<div class="col-auto">
						<div class="icon icon-shape bg-info text-white rounded-circle shadow">
							<i class="ni ni-app"></i>
						</div>
					</div>
				</div>
				<p class="mt-3 mb-0 text-muted text-sm">
					<span class="text-nowrap">Validasi dan Inspeksi</span>
				</p>
			</div>
		</div>
	</div>


</div>

<hr>

<div class="btn-group" role="group" aria-label="Basic example">
	<button type="button" class="btn btn-sm btn-info">Lihat Rincian Per UPT</button>
	<button type="button" class="btn btn-sm btn-secondary">Lihat Capaian Per Bulan</button>
</div>
@endsection