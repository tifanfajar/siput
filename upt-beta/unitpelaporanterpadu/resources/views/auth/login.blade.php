<!DOCTYPE html>
<html lang="pt-BR">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>APLIKASI PELAPORAN PELAYANAN</title>
  @include('auth.asset.css')
  @include('dev.helpers.css')
</head>
<div class="login-wrapper">
    <div class="login-form">
        <h2 style="color: white;margin-top: -100px;">APLIKASI PELAPORAN PELAYANAN<br>
            SPEKTRUM FREKUENSI RADIO & SERTIFIKASI OPERATOR RADIO<br>
        DI UPT MONITOR SPEKTRUM FREKUENSI RADIO</h2>
        <form class="form" method="POST" action="{{ route('postLogin') }}">
            @csrf
            <span class="info" style="color: white;">Silahkan masukan informasi nama pengguna dan <br> password anda untuk login</span>
            <input type="text" placeholder="Nama Pengguna" name="email"  required autofocus>
            @if ($errors->has('username') || $errors->has('email'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('username') ?: $errors->first('email') }}</strong>
          </span>
          @endif
          <input type="password" placeholder="Kata Sandi" name="password" required autocomplete="current-password">
          @error('password')
          <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
          </span>
          @enderror
          <button type="submit">MASUK</button>
      </form>
  </div>

  <ul class="bg-circles">
      <li></li>
      <li></li>
      <li></li>
      <li></li>
      <li></li>
      <li></li>
      <li></li>
      <li></li>
      <li></li>
      <li></li>
  </ul>
  <span class="copyright" style="color: white;">&copy 2019 Direktorat Operasi Sumber Daya [OPS11]</span>
</div>
@include('auth.asset.js')
@include('dev.helpers.js')
</body>
</html>
