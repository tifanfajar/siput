 <style>
  @font-face {
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 300;
    src: local('Roboto Light'), local('Roboto-Light'), url(https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmSU5fBBc9.ttf) format('truetype');
  }
  @font-face {
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 400;
    src: local('Roboto'), local('Roboto-Regular'), url(https://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Mu4mxP.ttf) format('truetype');
  }
  * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    font-weight: 300;
  }
  body {
    font-family: 'Roboto', sans-serif;
    color: white;
    font-weight: 300;
  }
  body ::-webkit-input-placeholder {
    /* WebKit browsers */
    font-family: 'Roboto', sans-serif;
    color: white;
    font-weight: 300;
  }
  body :-moz-placeholder {
    /* Mozilla Firefox 4 to 18 */
    font-family: 'Roboto', sans-serif;
    color: white;
    opacity: 1;
    font-weight: 300;
  }
  body ::-moz-placeholder {
    /* Mozilla Firefox 19+ */
    font-family: 'Roboto', sans-serif;
    color: white;
    opacity: 1;
    font-weight: 300;
  }
  body :-ms-input-placeholder {
    /* Internet Explorer 10+ */
    font-family: 'Roboto', sans-serif;
    color: white;
    font-weight: 300;
  }
  .login-wrapper {
  /*background: #e1eec3;
  background: -webkit-linear-gradient(top left, #e1eec3 0%, #f05053 100%);
  background: -moz-linear-gradient(top left, #e1eec3 0%, #f05053 100%);
  background: -o-linear-gradient(top left, #e1eec3 0%, #f05053 100%);
  background: linear-gradient(to bottom right, #e1eec3 0%, #f05053 100%);*/
  background: rgba(147,206,222,1);
  background: -moz-linear-gradient(-45deg, rgba(147,206,222,1) 0%, rgba(117,189,209,1) 41%, rgba(2,82,179,1) 100%);
  background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(147,206,222,1)), color-stop(41%, rgba(117,189,209,1)), color-stop(100%, rgba(2,82,179,1)));
  background: -webkit-linear-gradient(-45deg, rgba(147,206,222,1) 0%, rgba(117,189,209,1) 41%, rgba(2,82,179,1) 100%);
  background: -o-linear-gradient(-45deg, rgba(147,206,222,1) 0%, rgba(117,189,209,1) 41%, rgba(2,82,179,1) 100%);
  background: -ms-linear-gradient(-45deg, rgba(147,206,222,1) 0%, rgba(117,189,209,1) 41%, rgba(2,82,179,1) 100%);
  background: linear-gradient(135deg, rgba(147,206,222,1) 0%, rgba(117,189,209,1) 41%, rgba(2,82,179,1) 100%);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#93cede', endColorstr='#0252b3', GradientType=1 );
  position: absolute;
  width: 100%;
  height: 100%;
  align-items: center;
  display: flex;
  justify-content: center;
  overflow: hidden;
}
.login-wrapper.form-success .login-form h1 {
  transform: translateY(85px);
}
.login-form {
  min-width: 600px;
  margin: 0 auto;
  padding: 80px 0;
  height: 400px;
  text-align: center;
}
.login-form h1 {
  font-size: 40px;
  transition-duration: 1s;
  transition-timing-function: ease-in-put;
  font-weight: 400;
}
form {
  padding: 20px 0;
  position: relative;
  z-index: 2;
}
form input {
  appearance: none;
  outline: 0;
  border: 1px solid rgba(255, 255, 255, 0.4);
  background-color: rgba(255, 255, 255, 0.2);
  width: 250px;
  border-radius: 3px;
  padding: 10px 15px;
  margin: 0 auto 10px auto;
  display: block;
  text-align: center;
  font-size: 17px;
  color: white;
  transition-duration: 0.25s;
  font-weight: 300;
}
form input:hover {
  background-color: rgba(255, 255, 255, 0.4);
}
form input:focus {
  background-color: white;
  width: 300px;
  color: #f05053;
}
form button {
  appearance: none;
  outline: 0;
  background-color: white;
  border: 0;
  padding: 10px 15px;
  color: #0252b3;
  border-radius: 3px;
  width: 250px;
  cursor: pointer;
  font-size: 18px;
  transition-duration: 0.25s;
}
form button:hover {
  background-color: #f5f7f9;
}
.bg-circles {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 1;
}
.bg-circles li {
  position: absolute;
  list-style: none;
  display: block;
  width: 40px;
  height: 40px;
  background-color: rgba(255, 255, 255, 0.15);
  bottom: -160px;
  border-radius: 50%;
  -webkit-animation: square 25s infinite;
  animation: square 25s infinite;
  -webkit-transition-timing-function: linear;
  transition-timing-function: linear;
}
.bg-circles li:nth-child(1) {
  left: 10%;
}
.bg-circles li:nth-child(2) {
  left: 20%;
  width: 80px;
  height: 80px;
  animation-delay: 2s;
  animation-duration: 17s;
}
.bg-circles li:nth-child(3) {
  left: 25%;
  animation-delay: 4s;
}
.bg-circles li:nth-child(4) {
  left: 40%;
  width: 60px;
  height: 60px;
  animation-duration: 22s;
  background-color: rgba(255, 255, 255, 0.25);
}
.bg-circles li:nth-child(5) {
  left: 70%;
}
.bg-circles li:nth-child(6) {
  left: 80%;
  width: 120px;
  height: 120px;
  animation-delay: 3s;
  background-color: rgba(255, 255, 255, 0.2);
}
.bg-circles li:nth-child(7) {
  left: 32%;
  width: 160px;
  height: 160px;
  animation-delay: 7s;
}
.bg-circles li:nth-child(8) {
  left: 55%;
  width: 20px;
  height: 20px;
  animation-delay: 15s;
  animation-duration: 40s;
}
.bg-circles li:nth-child(9) {
  left: 25%;
  width: 10px;
  height: 10px;
  animation-delay: 2s;
  animation-duration: 40s;
  background-color: rgba(255, 255, 255, 0.3);
}
.bg-circles li:nth-child(10) {
  left: 90%;
  width: 160px;
  height: 160px;
  animation-delay: 11s;
}
.info{
  font-size: 12px;
  margin-bottom: 10px;
  display: block;
}
.copyright{
  position: absolute;
  text-align: center;
  font-size: 12px;
  bottom: 25px;
}
@-webkit-keyframes square {
  0% {
    transform: translateY(0);
  }
  100% {
    transform: translateY(-1350px) rotate(600deg);
  }
}
@keyframes square {
  0% {
    transform: translateY(0);
  }
  100% {
    transform: translateY(-1350px) rotate(600deg);
  }
}

</style>