<script>

var mapChart;

var data = [
@foreach(\App\Model\Region\Provinsi::all() as $provinsi => $value)
['{{$value->id_map}}',1],
@endforeach
];

Highcharts.wrap(Highcharts.Point.prototype, 'select', function (proceed) {

    proceed.apply(this, Array.prototype.slice.call(arguments, 1));

    var points = mapChart.getSelectedPoints();
    if (points.length) {
        if (points.length === 1) {
            $('#info-spp #flag-spp').attr('class', 'flag ' + points[0].flag);
            // $('#info-spp h2').html(points[0].name);
            $('#info-spp #map-spp').attr('value', points[0].options['hc-key']);
        } else {
            $('#info-spp #flag-spp').attr('class', 'flag');
            // $('#info-spp h2').html('Comparing countries');
            $('#info-spp #map-spp').attr('value', '');

        }

    } else {
        $('#info-spp #flag-spp').attr('class', '');
    }
});

// Create the chart
mapChart = Highcharts.mapChart('maps-spp', {
	chart: {
		map: 'countries/id/id-all'
	},

	title: {
		enabled: false,
		text: ''
	},

	mapNavigation: {
		enabled: true,
		buttonOptions: {
			verticalAlign: 'top'
		}
	},

	colorAxis: {
        min: 1,
        type: 'logarithmic',
        minColor: '#0000FF',
        maxColor: '#FF0000',
        stops: [
        [0, '#0d00f2'],
        [0.67, '#800080'],
        [1, '#FF0000']
        ]
    },

    series: [{
      data: data,
      name: 'UPT',
      allowPointSelect: true,
      cursor: 'pointer',
      states: {
         hover: {
            color: '#BADA55'
        }
    }
}]
});
</script>