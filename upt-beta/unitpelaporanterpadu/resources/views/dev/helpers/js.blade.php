<!--   Core   -->
@include('dev.helpers.jquery')
<script src="{{asset('templates/assets/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<!--   Optional JS   -->
<script src="{{asset('templates/assets/js/plugins/chart.js/dist/Chart.min.js')}}"></script>
<script src="{{asset('templates/assets/js/plugins/chart.js/dist/Chart.extension.js')}}"></script>
<script src="{{asset('templates/assets/js/jquery-confirm.js') }}"></script>

<!--   Optional JS   -->
<script src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap"></script>
<!--   Argon JS   -->
<script src="{{asset('templates/assets/js/argon-dashboard.min.js?v=1.1.0')}}"></script>
<script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>

<!-- Map -->
<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="https://code.highcharts.com/mapdata/countries/id/id-all.js"></script>

<!-- Datatables -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script src="{{ asset('js/datepicker.js') }}"></script>
<script src="{{asset('js/moment.js')}}"></script>
  <script src="{{asset('js/moment-locale.js')}}"></script>
  <script src="{{asset('jquery-ui/jquery-ui.min.js')}}"></script>
<!-- <link rel="stylesheet" type="text/css" href="{{asset('datetimepicker/jquery.datetimepicker.js')}}"/> -->
@include('dev.helpers.maps')
@include('dev.helpers.maps-spp')
<script>	
	$(document).ready(function(){
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
		$('table.data-table').DataTable({
			"pagingType": "numbers"
		});	
		$('.select2').select2({
		    theme: "bootstrap"
		});
	});
</script>

<script>
	var myBarChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options
	});
</script>

<script>
	function addMarker(lat, lng, info) {
		var pt = new google.maps.LatLng(lat, lng);
		map.setCenter(pt);
		map.setZoom(100);
	}
	var myBarChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options
	});
</script>

<script>
	$('.drop-down-show-hide').hide();
	$('#dropDown').change(function () {
		$('.drop-down-show-hide').hide()
		$('#' + this.value).show();
	});
	$('#dropDown').change(function () {
		$('.slowws').hide()
    // $('#' + this.value).show();
});
</script>

<script>
	$('.drop-down-show-hide').hide();
	$('#dropDown2').change(function () {
		$('.drop-down-show-hide').hide()
		$('#' + this.value).show();
	});
	$('#dropDown2').change(function () {
		$('.slowws').hide()
	});
</script>
<script>
	@if(count($errors) > 0 || Session::has('success') || Session::has('info') || Session::has('warning'))
	$.confirm({
		title: '{{Session::get('info')}}',
		content: '{{Session::get('alert')}}',
		type: '{{Session::get('colors')}}',
		icon: '{{Session::get('icons')}}',
		typeAnimated: true,
		buttons: {
			close: function () {
			}
		}
	});
	@elseif(count($errors) == 0)
	return false;
	@else
	$.confirm({
		title: '{{Session::has('info')}}',
		content: '{{Session::get('alert')}}',
		type: 'red',
		typeAnimated: true,
		icon: 'fas fa-exclamation-triangle',
		buttons: {
			close: function () {
			}
		}
	});
	@endif
</script>