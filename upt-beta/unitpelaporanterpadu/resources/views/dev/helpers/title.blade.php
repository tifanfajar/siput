<?php 
$title = strtoupper(Request::segment(1));
?>
@if(\Request::getRequestUri() == "/")
<title>
	DASHBOARD - APLIKASI PELAPORAN PELAYANAN
</title>
@else
<title>
	{{$title}} - APLIKASI PELAPORAN PELAYANAN
</title>
@endif