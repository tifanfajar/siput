<link href="{{asset('templates/assets/img/brand/favicon.jpg')}}" rel="icon" type="image/png">
<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<!-- Icons -->
<link href="{{asset('templates/assets/js/plugins/nucleo/css/nucleo.css')}}" rel="stylesheet" />
<link href="{{asset('templates/assets/js/plugins/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet" />
<!-- CSS Files -->
<link href="{{asset('templates/assets/css/argon-dashboard.css?v=1.1.0')}}" rel="stylesheet" />
<link href="{{asset('templates/assets/css/animate.css')}}" rel="stylesheet" />

<link href="{{asset('select2/dist/css/select2.min.css')}}" rel="stylesheet" />
<link href="{{asset('css/select2-bootstrap.min.css')}}" rel="stylesheet" />

<link rel="stylesheet" href="{{asset('templates/assets/css/jquery-confirm.css') }}">


<link rel="stylesheet" type="text/css" href="{{asset('templates/assets/css/datatables.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('validation/validate.css')}}"/>
<!-- <link rel="stylesheet" type="text/css" href="{{asset('datetimepicker/jquery.datetimepicker.css')}}"/> -->
<link rel="stylesheet" href="{{ asset('css/datepicker.css') }}">
<link rel="stylesheet" href="{{ asset('jquery-ui/jquery-ui.min.css') }}">

<style type="text/css">
	.select2-container--bootstrap {z-index: 1073}
</style>
