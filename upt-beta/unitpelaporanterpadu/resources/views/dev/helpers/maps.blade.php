<script>

    var mapChart;

var data = [
@foreach(\App\Model\Region\Provinsi::all() as $provinsi => $value)
['{{$value->id_map}}',{{$value->region_color}}],
@endforeach
];

Highcharts.wrap(Highcharts.Point.prototype, 'select', function (proceed) {

    proceed.apply(this, Array.prototype.slice.call(arguments, 1));

    var points = mapChart.getSelectedPoints();
    if (points.length) {
        if (points.length === 1) {
            $('#info #flag').attr('class', 'flag ' + points[0].flag);
            // $('#info h2').html(points[0].name);
            $('#info #map').attr('value', points[0].options['hc-key']);
        } else {
            $('#info #flag').attr('class', 'flag');
            // $('#info h2').html('Comparing countries');
            $('#info #map').attr('value', '');

        }

    } else {
        $('#info #flag').attr('class', '');
    }
});

// Create the chart
mapChart = Highcharts.mapChart('maps', {
	chart: {
		map: 'countries/id/id-all'
	},

	title: {
		enabled: false,
		text: ''
	},

	mapNavigation: {
		enabled: true,
		buttonOptions: {
			verticalAlign: 'top'
		}
	},

	colorAxis: {
      dataClasses: [{
        to: 4,
        color: "#FF0087"
    }, {
        from: 5,
        to: 10,
        color: "#E9FB06"
    }, {
        from: 11,
        to: 16,
        color: "#FB0606"
    }, {
        from: 17,
        color: "#06FBE7"
    }, {
        from: 18,
        to: 21,
        color: "#10A421"
    },{
        from: 23,
        to: 28,
        color: "#FF7B00"
    },
    {
        from: 29,
        to: 30,
        color: "#6309B1"
    },
    {
        from: 31,
        to: 32,
        color: "#69390D"
    },
    {
        from: 33,
        to: 34,
        color: "#001DFB"
    },
    {
        from: 1000,
        color: "black"
    }]
},
series: [{
  data: data,
  name: 'UPT',
  allowPointSelect: true,
  cursor: 'pointer',
  states: {
   hover: {
    color: '#BADA55'
}
}
}]
});
</script>