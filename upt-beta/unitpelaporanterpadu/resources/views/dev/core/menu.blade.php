<?php
$module_parent = DB::table('role_acl')
->select('module_parent','module_name','pathParent','menu_icon')
->join('modules','role_acl.module_parent','=','modules.id')
->where('role_id', Auth::user()->role_id)
->where(function ($query){
  $query->where('create_acl','<>',0)
  ->orWhere('read_acl','<>',0)
  ->orWhere('update_acl','<>',0)
  ->orWhere('delete_acl','<>',0);
})
->groupBy('module_parent')
->groupBy('module_name')
->groupBy('pathParent')
->groupBy('menu_icon')
->orderBy('module_parent','asc')
->get();
?>

<li class="nav-item  class=" active>
  <a class=" nav-link curved-btn {{ (Request::url() === url('/')) ? 'active' : '' }} " href=" {{url('/')}}"> <i class="ni ni-tv-2 text-whiteX"></i> Dashboard
  </a>
</li>

@foreach($module_parent as $parent)
<li class="nav-item">
  <a class="nav-link curved-btn {{ Request::is($parent->pathParent.'/*') ? 'active' : '' }}" data-toggle="collapse" href="#col{{$parent->module_parent}}" role="button" aria-expanded="false" aria-controls="col2">
    <i class="{{$parent->menu_icon}} text-whiteX"></i> {{$parent->module_name}}
  </a>
</li>
<?php
$module_childs = DB::table('role_acl')
->join('modules','role_acl.module_id','=','modules.kdModule')
->where('module_parent',$parent->module_parent)
->where('role_id', Auth::user()->role_id)
->where(function ($query) {
  $query->where('create_acl','<>',0)
  ->orWhere('read_acl','<>',0)
  ->orWhere('update_acl','<>',0)
  ->orWhere('delete_acl','<>',0);
})
->orderBy('menu_order','asc')
->get();
?>
<div class="collapse {{ Request::is($parent->pathParent.'/*') ? 'show' : '' }} multi-collapse" id="col{{$parent->module_parent}}">
  @foreach($module_childs as $child)
  <div class="card card-body list-menus">
    <ul style="padding-right: 0px;">
      <li class="nav-item"><a class="nav-link {{ Request::is($child->menu_path) ? 'active curved-btn' : '' }} lk-subs" href="{{url($child->menu_path)}}">{{$child->module_name}}</a></li>
    </ul>
  </div>
  @endforeach
</div>
@endforeach