<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tanggal_ujian')->nullable();
            $table->string('lokasi_ujian')->nullable();
            $table->string('id_prov')->nullable();
            $table->string('id_kabkot')->nullable();
            $table->string('kode_upt')->nullable();
            $table->integer('jumlah_yd')->nullable();
            $table->integer('jumlah_yc')->nullable();
            $table->integer('jumlah_yb')->nullable();
            $table->integer('lulus_yd')->nullable();
            $table->integer('lulus_yc')->nullable();
            $table->integer('lulus_yb')->nullable();
            $table->integer('tdk_lulus_yd')->nullable();
            $table->integer('tdk_lulus_yc')->nullable();
            $table->integer('tdk_lulus_yb')->nullable();
            $table->text('ket_reject')->nullable();
            $table->string('created_by')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unars');
    }
}
