<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Model\Privillage\Role;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    protected $loginPath = 'login';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('guest')->except('logout');
    }

    public function getLogin(){
      return view('auth.login');
    }

    public function logout(){
      Auth::logout();
      return redirect(url('login'));
    }

    public function postLogin(Request $r){
      $username = $r->input('email');
      $password = $r->input('password');
      if (Auth::attempt(['email' => $username, 'password' => $password])) {
        $getAkses = Role::where('id',Auth::user()->role_id)->value('akses');
        $capitalizeAkses = ucfirst($getAkses);
        $getUptName = \App\Model\Setting\UPT::where('office_id',Auth::user()->upt)->distinct()->value('office_name');
        Session::flash('colors', 'green');
        Session::flash('icons', 'fas fa-check-circle');
        if ($getAkses == 'administrator') {
          Session::flash('info', 'Selamat Datang!');
          Session::flash('alert', 'Hallo '.$capitalizeAkses);
        }
        else{
          if ($getAkses == 'kasi') {
            Session::flash('info', 'Penjabat UPT');
          }
          else{
            Session::flash('info', $capitalizeAkses);
          }
          Session::flash('alert', $getUptName); 
        }
        return redirect(url('/'));
      }
      Session::flash('info', 'Terjadi Kesalahan');
      Session::flash('colors', 'red');
      Session::flash('icons', 'fas fa-times');
      Session::flash('alert', 'Username/Password salah');
      return redirect()->back();
    }


  }


