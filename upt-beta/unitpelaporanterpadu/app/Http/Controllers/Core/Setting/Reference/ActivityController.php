<?php

namespace App\Http\Controllers\Core\Setting\Reference;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Model\Refrension\Kegiatan;
use PDF;
use Auth;
use Session;
use Maatwebsite\Excel\Facades\Excel;

class ActivityController extends Controller
{
     public function save(Request $request){
		$kegiatan = new Kegiatan;
		$kegiatan->kegiatan = $request->input('kegiatan');
		$kegiatan->created_by = Auth::user()->id;
		$kegiatan->updated_by = Auth::user()->id;
		$kegiatan->save();
		Session::flash('info', 'Kegiatan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-check');
		Session::flash('alert', 'Berhasil Menambah Data!');
		return redirect()->back();
	}

	public function delete(Request $request){
		$id = Crypt::decryptString($request->id);
		Kegiatan::where('id',$id)->delete();
		Session::flash('info', 'Kegiatan');
		Session::flash('colors', 'red');
		Session::flash('icons', 'fas fa-trash');
		Session::flash('alert', 'Berhasil Menghapus Data!');
		return redirect()->back();
	}
	public function update(Request $request){
		$id = Crypt::decryptString($request->id);
		$kegiatan = Kegiatan::find($id);
		$kegiatan->kegiatan = $request->input('Kegiatan');
		$kegiatan->updated_by = Auth::user()->id;
		$kegiatan->save();
		Session::flash('info', 'kegiatan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Memperbaharui Data!');
		return redirect()->back();

	}
}
