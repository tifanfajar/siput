<?php

namespace App\Http\Controllers\Core\Setting\Reference;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Model\Refrension\Metode;
use PDF;
use Auth;
use Session;
use Maatwebsite\Excel\Facades\Excel;

class MethodController extends Controller
{
     public function save(Request $request){
		$metode = new Metode;
		$metode->metode = $request->input('metode');
		$metode->created_by = Auth::user()->id;
		$metode->updated_by = Auth::user()->id;
		$metode->save();
		Session::flash('info', 'Metode');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-check');
		Session::flash('alert', 'Berhasil Menambah Data!');
		return redirect()->back();
	}

	public function delete(Request $request){
		$id = Crypt::decryptString($request->id);
		Metode::where('id',$id)->delete();
		Session::flash('info', 'Metode');
		Session::flash('colors', 'red');
		Session::flash('icons', 'fas fa-trash');
		Session::flash('alert', 'Berhasil Menghapus Data!');
		return redirect()->back();
	}
	public function update(Request $request){
		$id = Crypt::decryptString($request->id);
		$metode = Metode::find($id);
		$metode->metode = $request->input('metode');
		$metode->updated_by = Auth::user()->id;
		$metode->save();
		Session::flash('info', 'Metode');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Memperbaharui Data!');
		return redirect()->back();

	}
}
