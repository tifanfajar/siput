<?php

namespace App\Http\Controllers\Core\Setting\Reference;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Model\Refrension\Tujuan;
use PDF;
use Auth;
use Session;

class PurposeController extends Controller
{
     public function save(Request $request){
		$tujuan = new Tujuan;
		$tujuan->tujuan = $request->input('tujuan');
		$tujuan->created_by = Auth::user()->id;
		$tujuan->updated_by = Auth::user()->id;
		$tujuan->save();
		Session::flash('info', 'Tujuan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-check');
		Session::flash('alert', 'Berhasil Menambah Data!');
		return redirect()->back();
	}

	public function delete(Request $request){
		$id = Crypt::decryptString($request->id);
		Tujuan::where('id',$id)->delete();
		Session::flash('info', 'Tujuan');
		Session::flash('colors', 'red');
		Session::flash('icons', 'fas fa-trash');
		Session::flash('alert', 'Berhasil Menghapus Data!');
		return redirect()->back();
	}
	public function update(Request $request){
		$id = Crypt::decryptString($request->id);
		$tujuan = Tujuan::find($id);
		$tujuan->tujuan = $request->input('tujuan');
		$tujuan->updated_by = Auth::user()->id;
		$tujuan->save();
		Session::flash('info', 'tujuan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Memperbaharui Data!');
		return redirect()->back();

	}

}
