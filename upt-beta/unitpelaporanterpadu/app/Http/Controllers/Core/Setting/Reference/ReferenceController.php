<?php

namespace App\Http\Controllers\Core\Setting\Reference;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Model\Refrension\Refrensi;
use App\Model\Refrension\Materi;
use App\Model\Refrension\Kegiatan;
use App\Model\Refrension\Metode;
use App\Model\Refrension\Tujuan;
use App\Model\Refrension\GroupPerizinan;
use PDF;
use Auth;
use Session;
use Maatwebsite\Excel\Facades\Excel;



class ReferenceController extends Controller
{
    public function index()
    {
    	$refrensi['refrensi'] = Refrensi::all();
    	$materi['materi'] = Materi::all();
    	$kegiatan['kegiatan'] = Kegiatan::all();
    	$metode['metode'] = Metode::all();
    	$tujuan['tujuan'] = Tujuan::all();
    	$group_perizinan['group_perizinan'] = GroupPerizinan::all();
    	return view('templates.pengaturan.Reference.core.index')->with($refrensi)->with($materi)->with($kegiatan)->with($metode)->with($tujuan)->with($group_perizinan);
    }

    //Kategori
    public function save(Request $request){
		$refrensi = new Refrensi;
		$refrensi->kategori_spp = $request->input('kategori_spp');
		$refrensi->created_by = Auth::user()->id;
		$refrensi->updated_by = Auth::user()->id;
		$refrensi->save();
		Session::flash('info', 'Refrensi');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-check');
		Session::flash('alert', 'Berhasil Menambah Data!');
		return redirect()->back();
	}

	public function delete(Request $request){
		$id = Crypt::decryptString($request->id);
		Refrensi::where('id',$id)->delete();
		Session::flash('info', 'Refrensi');
		Session::flash('colors', 'red');
		Session::flash('icons', 'fas fa-trash');
		Session::flash('alert', 'Berhasil Menghapus Data!');
		return redirect()->back();
	}
	public function update(Request $request){
		$id = Crypt::decryptString($request->id);
		$refrensi = Refrensi::find($id);
		$refrensi->kategori_spp = $request->input('kategori_spp');
		$refrensi->updated_by = Auth::user()->id;
		$refrensi->save();
		Session::flash('info', 'Refrensi');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Memperbaharui Data!');
		return redirect()->back();

	}
		public function print(){
		$refrensi['refrensi'] = Refrensi::all();
		$pdf = PDF::loadView('templates.pengaturan.Reference.dev.print', $refrensi);
		return $pdf->download('Data-Reference.pdf');
	}

	 public function export() 
    {
        return Excel::download(new RefrensiExport, 'refrensi.xlsx');
    }

    public function import() 
    {
        Excel::import(new RefrensiImport, 'Refrensi.xlsx');
        
        return redirect('/')->with('success', 'All good!');
    }
    //End Kategori
}
