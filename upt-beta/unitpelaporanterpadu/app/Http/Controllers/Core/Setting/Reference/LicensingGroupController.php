<?php

namespace App\Http\Controllers\Core\Setting\Reference;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Model\Refrension\GroupPerizinan;
use PDF;
use Auth;
use Session;
use Maatwebsite\Excel\Facades\Excel;

class LicensingGroupController extends Controller
{
     public function save(Request $request){
		$group_perizinan = new GroupPerizinan;
		$group_perizinan->jenis = $request->input('jenis');
		$group_perizinan->created_by = Auth::user()->id;
		$group_perizinan->updated_by = Auth::user()->id;
		$group_perizinan->save();
		Session::flash('info', 'Group Perizinan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-check');
		Session::flash('alert', 'Berhasil Menambah Data!');
		return redirect()->back();
	}

	public function delete(Request $request){
		$id = Crypt::decryptString($request->id);
		GroupPerizinan::where('id',$id)->delete();
		Session::flash('info', 'Group Perizinan');
		Session::flash('colors', 'red');
		Session::flash('icons', 'fas fa-trash');
		Session::flash('alert', 'Berhasil Menghapus Data!');
		return redirect()->back();
	}
	public function update(Request $request){
		$id = Crypt::decryptString($request->id);
		$group_perizinan = GroupPerizinan::find($id);
		$group_perizinan->jenis = $request->input('jenis');
		$group_perizinan->updated_by = Auth::user()->id;
		$group_perizinan->save();
		Session::flash('info', 'Group Perizinan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Memperbaharui Data!');
		return redirect()->back();

	}
}
