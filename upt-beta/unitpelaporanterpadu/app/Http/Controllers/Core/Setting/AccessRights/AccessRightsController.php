<?php

namespace App\Http\Controllers\Core\Setting\AccessRights;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use Validator;
use Session;
use DB;
use Auth;
use App\Model\Privillage\Role;
use App\Model\Menu\Module;
use App\Model\Privillage\RoleAcl;
use App\Model\Notification\Notification;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AccessRightsController extends Controller
{
	public function index(){
		$roles = Role::all();
		return view('templates.pengaturan.hak-akses.core.index',compact('roles'));
	}
	public function create(){
		$modules = Module::where('menu_parent', '0')->get();
		return view('templates.pengaturan.hak-akses.core.add')
		->with('module', $modules);
	}
	public function save(Request $request){
		$validator = Validator::make($request->all(), [
			'role_name' => 'required',
			'description' => 'required'
		]);

		if($validator->fails()) {
			return redirect()->back();
		}

		$role = new Role;
		$role->role_name = $request->input('role_name');
		$role->description = $request->input('description');
		$role->akses = $request->input('akses');
		$role->save();

		$notification = new Notification;
		$nama_user = Auth::user()->name;
		$notification->id_module = 17;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = 0;
			$notification->id_upt = 0;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Menambahkan Data !';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		$roles = Module::where('menu_parent', '!=', 0)->get();
		foreach ($roles as $module) {
			Roleacl::create([
				'module_id' => $module->kdModule,
				'role_id' => $role->id,
				'create_acl' => $request->input($module->kdModule.'_create'),
				'read_acl' => $request->input($module->kdModule.'_read'),
				'update_acl' => $request->input($module->kdModule.'_update'),
				'delete_acl' => $request->input($module->kdModule.'_delete'),
				'module_parent' =>  $module->menu_parent,
			]);
		}

		Session::flash('info', 'Hak Akses!');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-check-circle');
		Session::flash('alert', 'Berhasil disimpan');

		return redirect(url('pengaturan/hak-akses'));
	}

	public function edit($id){
		$idrole = Crypt::decryptString($id);
		$role = Role::find($idrole);
		$module = Module::where('menu_parent', '0')->get();

		return view('templates.pengaturan.hak-akses.core.edit')
		->with('role', $role)->with('module', $module);
	}

	public function update($id, Request $r)
	{
		$role_name = $r->input('role_name');
		$description = $r->input('description');
		$akses = $r->input('akses');

		$role = Role::find($id)->update([
			'role_name' => $role_name,
			'description' => $description,
			'akses' => $akses
		]);

		$roles = Module::where('menu_parent', '!=', 0)->get();
		foreach ($roles as $modules) {
			$data = [
				'module_id' => $modules->kdModule,
				'role_id' => $id,
				'create_acl' => $r->input($modules->kdModule.'_create'),
				'read_acl' => $r->input($modules->kdModule.'_read'),
				'update_acl' => $r->input($modules->kdModule.'_update'),
				'delete_acl' => $r->input($modules->kdModule.'_delete'),
				'module_parent' => $modules->menu_parent,
			];
			$cek = RoleAcl::where('module_id', $modules->kdModule)->where('role_id', $id)->first();
			if ($cek == null) {
				$rolenya = RoleAcl::create($data);
			} else {
				$rolenya = RoleAcl::find($cek->id);
				$rolenya->update($data);
			}
		}

		$notification = new Notification;
		$nama_user = Auth::user()->name;
		$notification->id_module = 17;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = 0;
			$notification->id_upt = 0;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Mengubah Data !';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Hak Akses');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-check-circle');
		Session::flash('alert', 'Berhasil disimpan');
		return redirect(url('pengaturan/hak-akses'));

	}

	public function delete($id)
	{
		$idrole = Crypt::decryptString($id);
		$getAkses = Role::where('id',$idrole)->value('akses');
		Role::where('id', $idrole)->delete();
		Roleacl::where('role_id', $idrole)->delete();

		$notification = new Notification;
		$nama_user = Auth::user()->name;
		$notification->id_module = 17;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = 0;
			$notification->id_upt = 0;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Menghapus Data !';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Hak Akses');
		Session::flash('colors', 'red');
		Session::flash('icons', 'fas fa-times-circle');
		Session::flash('alert', 'Berhasil dihapus');
		return redirect()->back();
		
	}
}
