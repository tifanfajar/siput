<?php

namespace App\Http\Controllers\Core\Setting\Upt;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Http\Request;
use App\Model\Setting\UPT;
use PDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UptExport;
use Session;

class UptController extends Controller
{
	public function index(){
		$upt = UPT::all();
		return view('templates.pengaturan.upt.core.index',compact('upt'));
	}
	public function print(){
		$upt['upt'] = UPT::all();
		$pdf = PDF::loadView('templates.pengaturan.upt.dev.print', $upt)->setPaper('a4','landscape');;
		return $pdf->download('Data UPT.pdf');
	}
	public function export(){
		return Excel::download(new UptExport, 'Data Upt.xlsx');
	}

}
