<?php

namespace App\Http\Controllers\Core\SPP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Exports\RTExport;
use App\Exports\RTUptBulanTahunExport;
use App\Exports\RTUptTahunExport;
use App\Exports\RTUptTahunExportNotPaid;
use App\Imports\RtImport;
use App\Model\Refrension\Metode;
use Carbon\Carbon;
use Auth;
use App\Model\Privillage\Role;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use App\Model\SPP\RincianTagihan;
use App\Model\Notification\Notification;
use App\Model\Date\ListMonth;
use App\Model\Setting\UPT;
use DB;
use Validator;
use App\Model\Region\Provinsi;

class RincianTagihanController extends Controller
{
	public function __construct()
	{
		$this->myTime = Carbon::now();
		$this->day = $this->myTime->format('d');
		$this->month = $this->myTime->format('m');
		$this->years = '20'.$this->myTime->format('y');
	}

	public function index(){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$getUptName = 'SELURUH UPT';
			$month = $this->month;
			$year = $this->years;
			if ($month == 1) {
				$dataTerbayar = 12;
				$dataBelumTerbayar = 12;
				$fixYear = $year - 1;
			}
			else{
				$dataTerbayar = $month - 1;
				$dataBelumTerbayar = $month - 1; 
				$fixYear = $year;
			}
			$year = $this->years;
			$rts_paid = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
			$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
			$paid_rts = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('status','paid')->where('katagori_spp','Pokok')->count();
			$approved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',0)->get();
			$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('katagori_spp','Pokok')->count();
			$total = $paid_rts + $nopaid_rts;
			$sendReport = 'INDEX';
			$statusReminder = '';
			$checkStatusReminder = '';
			$checkStatusApproved = '';
			$freezeTime = $this->myTime->format('d');
			$freezeMonth = $this->myTime->format('m');

		}
		else{
			$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
			$getUptId = UPT::where('office_id',Auth::user()->upt)->select('office_id')->distinct()->value('office_id');
			$month = $this->month;
			$year = $this->years;
			if ($month == 1) {
				$dataTerbayar = 12;
				$dataBelumTerbayar = 12;
				$fixYear = $year - 1;
			}
			else{
				$dataTerbayar = $month - 1;
				$dataBelumTerbayar = $month - 1; 
				$fixYear = $year;
			}
			$rts_paid = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
			$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
			$paid_rts = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
			$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
			$total = $paid_rts + $nopaid_rts;
			$approved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
			$statusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
			$checkStatusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
			$checkStatusApproved = RincianTagihan::where('active',0)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
			$freezeTime = $this->myTime->format('d');
			$freezeMonth = $this->myTime->format('m');
		}		
		return view('templates.SPP.RT.core.index',compact('rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','freezeMonth','checkStatusApproved','sendReport'));
	}

	public function search(Request $request){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeID = Provinsi::where('id_map',$request->id_map)->value('id_row');
			$changeUPT = UPT::where('province_code',$changeID)->select('office_id','office_name')->distinct()->value('office_id');
			$getUPT = UPT::where('province_code',$changeID)->select('office_id','office_name')->distinct()->get();
			$getUptName = UPT::where('office_id',$changeUPT)->select('office_name')->distinct()->value('office_name');
			$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			if ($changeUPT) {
				$count = $getUPT->count();
				if ($count > 1) {
					return view('templates.spp.rt.search.multiple',compact('getUPT'));
				}
				else{
					$month = $this->month;
					$year = $this->years;
					if ($month == 1) {
						$dataTerbayar = 12;
						$dataBelumTerbayar = 12;
						$fixYear = $year - 1;
					}
					else{
						$dataTerbayar = $month - 1;
						$dataBelumTerbayar = $month - 1; 
						$fixYear = $year;
					}
					$rts_paid = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->get();$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->get();
					
					$paid_rts = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
					$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
					$approved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('active',0)->where('upt',$getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
					$catatanPetugas = RincianTagihan::where('upt',$getUptName)->whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->select('ket_operator')->distinct()->value('ket_operator');
					$catatanKasi = RincianTagihan::where('upt',$getUptName)->whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->select('ket_reject')->distinct()->value('ket_reject');
					$sendReport = RincianTagihan::where('upt',$getUptId)->whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->select('updated_at')->distinct()->value('updated_at');
					$total = $paid_rts + $nopaid_rts;
					$statusReminder = '';
					$checkStatusReminder = '';
					$checkStatusApproved = '';
					$freezeTime = $this->myTime->format('d');
					$freezeMonth = $this->myTime->format('m');
				}
				return view('templates.SPP.RT.search.index',compact('rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','freezeMonth','checkStatusApproved','changeUPT','catatanPetugas','catatanKasi','sendReport'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Loket Pengaduan');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function searchMultiple($id,Request $request){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeUPT = $id;
			$getUptName = UPT::where('office_id',$changeUPT)->select('office_name')->distinct()->value('office_name');
			$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			if ($changeUPT) {
				$month = $this->month;
				$year = $this->years;
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $year;
				}
				$rts_paid = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->get();
				$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->get();
				$paid_rts = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
				$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
				$approved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status',0)->where('kode_upt',$changeUPT)->get();
				$catatanPetugas = RincianTagihan::where('upt',$getUptName)->whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = RincianTagihan::where('upt',$getUptName)->whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->select('ket_reject')->distinct()->value('ket_reject');
				$sendReport = RincianTagihan::where('upt',$getUptName)->whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->select('updated_at')->distinct()->value('updated_at');
				$total = $paid_rts + $nopaid_rts;
				$statusReminder = '';
				$checkStatusReminder = '';
				$checkStatusApproved = '';
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
				return view('templates.SPP.RT.search.index',compact('rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','freezeMonth','checkStatusApproved','changeUPT','catatanPetugas','catatanKasi','sendReport'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Loket Pengaduan');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}
	public function search_query(Request $request){
		$month = $request->month;
		$year = $request->year;
		$getUptName = $request->upt;
		$getStatus = Role::where('id',Auth::user()->role_id)->value('akses');
		$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			if ($getUptName != 'SELURUH UPT' && $month && $year) {
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $year;
				}
				$rts_paid = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
				$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
				$paid_rts = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
				$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->where('active',0)->get();
				$statusReminder = '';
				$checkStatusReminder = '';
				$checkStatusApproved = '';
				$catatanPetugas = RincianTagihan::where('upt',$getUptName)->whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->select('ket_operator')->distinct()->value('ket_operator');
				$catatanKasi = RincianTagihan::where('upt',$getUptName)->whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->select('ket_reject')->distinct()->value('ket_reject');
				$sendReport = RincianTagihan::where('upt',$getUptName)->whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->select('updated_at')->distinct()->value('updated_at');
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
				return view('templates.SPP.RT.table.upt-bulan-tahun',compact('getUptName','getStatus','rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth','catatanPetugas','catatanKasi','sendReport'));
			}elseif($getUptName != 'SELURUH UPT' && $year){
				if ($month == 1) {
					$month = 12;
					$year = $year - 1;
				}
				else{
					$month = $month - 1;
					$year = $year;
				}
				return view('templates.SPP.RT.search.upt-tahun',compact('getUptName','year','month','getUptId'));
			}elseif($getUptName == 'SELURUH UPT' && $month && $year){
				return view('templates.SPP.RT.search.seluruh-upt-bulan-tahun',compact('getUptName','year','month','getUptId'));
			}
		}else{
			if ($year) {
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $year;
				}
				$rts_paid = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
				$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
				$paid_rts = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
				$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('active',0)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
				$statusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
				$checkStatusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
				$checkStatusApproved = RincianTagihan::where('active',0)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$getUptName)->count();
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
			}else{
				if ($month == 1) {
					$dataTerbayar = 12;
					$dataBelumTerbayar = 12;
					$fixYear = $this->years - 1;
				}
				else{
					$dataTerbayar = $month - 1;
					$dataBelumTerbayar = $month - 1; 
					$fixYear = $this->years;
				}
				$rts_paid = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
				$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
				$paid_rts = RincianTagihan::whereMonth('bi_begin',$dataTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
				$nopaid_rts = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
				$total = $paid_rts + $nopaid_rts;
				$approved = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('active',0)->get();
				$statusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
				$checkStatusReminder = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
				$checkStatusApproved = RincianTagihan::where('active',0)->where('upt',$getUptName)->count();
				$freezeTime = $this->myTime->format('d');
				$freezeMonth = $this->myTime->format('m');
			}			
			return view('templates.SPP.RT.table.upt-bulan-tahun',compact('rts_paid','rts_nopaid','getUptName','month','year','paid_rts','nopaid_rts','total','approved','statusReminder','checkStatusReminder','freezeTime','checkStatusApproved','freezeMonth'));
		}
		// if ($getUptName != 'SELURUH UPT' && $year) {
		// 	return view('templates.SPP.RT.search.admin-rekap-upt-tahun',compact('getUptName','year','month'));
		// }elseif($getUptName == 'SELURUH UPT' && $month & $year){
		// 	return view('templates.SPP.RT.search.admin-rekap-seluruh-upt-tanggal',compact('getUptName','year','month'));
		// }elseif($getUptName == 'SELURUH UPT' && $year){
		// 	return view('templates.SPP.RT.search.admin-rekap-seluruh-upt-tahun',compact('getUptName','year','month'));
		// }
	}

	public function postORupdate(Request $request){
		$data = $request->all();
		$no_spp = $data['no_spp'];
		$upayametode = $data['upayametode'];
		$tanggal_upaya = $data['tanggal_upaya'];
		$keterangan = $data['keterangan'];
		$filename = $request->file('bukti_dukung');
		$created_at = date('Y-m-d H:m:s');
		$updated_at = date('Y-m-d H:m:s');
		foreach ($no_spp as $key => $value) {
			$rt = RincianTagihan::where('no_spp',$value)->first();
			$uniq = uniqid();
			$filename[$key]->move(public_path('lampiran/spp/rt'),'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension());
			$rt->upaya_metode = isset($upayametode[$key]) ? $upayametode[$key] : '';
			$rt->tanggal_upaya = isset($tanggal_upaya[$key]) ? $tanggal_upaya[$key] : '';
			$rt->keterangan = isset($keterangan[$key]) ? $keterangan[$key] : '';
			$rt->ket_operator = $request->ket_operator;
			$rt->created_by = Auth::user()->id;
			$rt->bukti_dukung = 'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension();
			$rt->active = 0;
			$rt->save();
		}
		// $rows = [];
		// $variables = array_filter($request->tanggal_upaya);
		// foreach($variables as $key => $input) {
		// 	$uniq = uniqid();
		// 	$filename[$key]->move(public_path('lampiran/spp/rt'),'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension());
		// 	array_push($rows, 
		// 		[
		// 			'upaya_metode' => isset($upayametode[$key]) ? $upayametode[$key] : '',
		// 			'tanggal_upaya' => isset($tanggal_upaya[$key]) ? $tanggal_upaya[$key] : '',
		// 			'keterangan' => isset($keterangan[$key]) ? $keterangan[$key] : '',
		// 			'ket_operator' => $request->ket_operator,
		// 			'bukti_dukung' => 'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension(),
		// 			'created_by' => Auth::user()->id,
		// 			'created_at' => $created_at,
		// 			'updated_at' => $updated_at,
		// 		]);
		// }
		// $hasil = RincianTagihan::whereIn('no_spp',$data_spp)->update($rows);

		$notification = new Notification;
		$nama_user = Auth::user()->name;
		$notification->id_module = 8;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}else{
			$notification->id_prov = Auth::user()->province_code;
			$notification->id_upt = Auth::user()->upt;
		}
		$notification->readable = 0;
		$notification->message = $nama_user.' Telah Berhasil Mengupload Data Rincian Tagihan';
		$notification->created_at = date('Y-m-d H:m:s');
		$notification->updated_at = date('Y-m-d H:m:s');
		$notification->save();

		Session::flash('info', 'Rincian Tagihan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengupload Data!');		
		return redirect()->back();
	}

	public function preview(Request $request){
		ini_set('max_execution_time',3600);

		$myTime = Carbon::now();
		$month = $myTime->format('m');
		$year = '20'.$myTime->format('y');
		if ($month == 1) {			
			$dataBulan = 12;
			$fixYear = $year - 1;
		}
		else{			
			$dataBulan = $month - 1; 
			$fixYear = $year;
		}
		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
			$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
		}else{

			$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
			$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
		}		
		$rt['rt'] = $rts_data; 
		$pdf = PDF::loadView('templates.SPP.RT.dev.print', $rt)->setPaper('a4','landscape');		
		return $pdf->stream('RT.pdf');
	}

	public function preview_search(Request $request)
	{	
		ini_set('max_execution_time',3600);

		$myTime = Carbon::now();
		$status_pembayaran = $request->status_pembayaran;		
		$month = $request->month;
		$year = $request->year;					
		$myTime = Carbon::now();

		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){		
			$getUptName = $request->getUptName;
			$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			if ($getUptName != 'SELURUH UPT' && $month != -1 && $year) {
				if ($month == 1) {					
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1; 
					$fixYear = $year;
				}	
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();

				$rt['rt'] = $rts_data; 
				$pdf = PDF::loadView('templates.SPP.RT.dev.print', $rt)->setPaper('a4','landscape');		
				return $pdf->stream($getUptName.'_RT.pdf');				

			}elseif($getUptName != 'SELURUH UPT' && $month == -1 && $year){	
				if($request->status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-tahun-paid', compact('getUptName','year','month','getUptId'))->setPaper('a4','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-tahun-not-paid', compact('getUptName','year','month','getUptId'))->setPaper('a4','landscape');					
				}
			// $pdf = PDF::loadView('templates.SPP.RT.dev.print', $rt)->setPaper('a4','landscape');
				return $pdf->stream('RT-UPT-TAHUN.pdf');
			}elseif($getUptName == 'SELURUH UPT' && $month && $year){
				if ($month == 1) {					
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1; 
					$fixYear = $year;
				}
				if($request->status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-bulan-tahun-paid', compact('getUptName','year','month','getUptId'))->setPaper('a4','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-bulan-tahun-not-paid', compact('getUptName','year','month','getUptId'))->setPaper('a4','landscape');					
				}
				return $pdf->stream('RT-UPT-BULAN-TAHUN.pdf');
			}


		}else{

			$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');

			if ($month != '' && $year != '') {
				$dataBulan = $month;
				$fixYear = $year;
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();

				$pdf = PDF::loadView('templates.SPP.RT.dev.print', $rt)->setPaper('a4','landscape');
				return $pdf->stream($getUptName.'_RT.pdf');
			}elseif($getUptName != 'SELURUH UPT' && $month == -1 && $year){	
				if($request->status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-tahun-paid', compact('getUptName','year','month','getUptId'))->setPaper('a4','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-tahun-not-paid', compact('getUptName','year','month','getUptId'))->setPaper('a4','landscape');					
				}
				return $pdf->stream('RT-UPT-TAHUN.pdf');
			}elseif($getUptName == 'SELURUH UPT' && $month && $year){
				if($request->status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-bulan-tahun-paid', compact('getUptName','year','month','getUptId'))->setPaper('a4','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-bulan-tahun-not-paid', compact('getUptName','year','month','getUptId'))->setPaper('a4','landscape');					
				}
				return $pdf->stream('RT-UPT-BULAN-TAHUN.pdf');
			}
		}
	}

	public function print(Request $request){
		ini_set('max_execution_time',3600);

		$myTime = Carbon::now();
		$month = $myTime->format('m');
		$year = '20'.$myTime->format('y');
		if ($month == 1) {			
			$dataBulan = 12;
			$fixYear = $year - 1;
		}
		else{			
			$dataBulan = $month - 1; 
			$fixYear = $year;
		}
		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
			$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
		}else{

			$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
			$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
		}		
		$rt['rt'] = $rts_data; 
		$pdf = PDF::loadView('templates.SPP.RT.dev.print', $rt)->setPaper('a4','landscape');		
		return $pdf->download('RT.pdf');
	}

	public function print_search(Request $request)
	{	
		ini_set('max_execution_time',3600);

		$myTime = Carbon::now();
		$status_pembayaran = $request->status_pembayaran;		
		$month = $request->month;
		$year = $request->year;					
		$myTime = Carbon::now();

		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){		
			$getUptName = $request->getUptName;
			$getUptId = UPT::where('office_name',$getUptName)->select('office_id')->distinct()->value('office_id');
			if ($getUptName != 'SELURUH UPT' && $month != -1 && $year) {
				if ($month == 1) {					
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1; 
					$fixYear = $year;
				}	
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();

				$rt['rt'] = $rts_data; 
				$pdf = PDF::loadView('templates.SPP.RT.dev.print', $rt)->setPaper('a4','landscape');		
				return $pdf->download($getUptName.'_RT.pdf');				

			}elseif($getUptName != 'SELURUH UPT' && $month == -1 && $year){	
				if($request->status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-tahun-paid', compact('getUptName','year','month','getUptId'))->setPaper('a4','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-tahun-not-paid', compact('getUptName','year','month','getUptId'))->setPaper('a4','landscape');					
				}
			// $pdf = PDF::loadView('templates.SPP.RT.dev.print', $rt)->setPaper('a4','landscape');
				return $pdf->download('RT-UPT-TAHUN.pdf');
			}elseif($getUptName == 'SELURUH UPT' && $month && $year){
				if ($month == 1) {					
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1; 
					$fixYear = $year;
				}
				if($request->status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-bulan-tahun-paid', compact('getUptName','year','month','getUptId'))->setPaper('a4','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-bulan-tahun-not-paid', compact('getUptName','year','month','getUptId'))->setPaper('a4','landscape');					
				}
				return $pdf->download('RT-UPT-BULAN-TAHUN.pdf');
			}


		}else{

			$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');

			if ($month != '' && $year != '') {
				$dataBulan = $month;
				$fixYear = $year;
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();

				$pdf = PDF::loadView('templates.SPP.RT.dev.print', $rt)->setPaper('a4','landscape');
				return $pdf->download($getUptName.'_RT.pdf');
			}elseif($getUptName != 'SELURUH UPT' && $month == -1 && $year){	
				if($request->status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-tahun-paid', compact('getUptName','year','month','getUptId'))->setPaper('a4','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-tahun-not-paid', compact('getUptName','year','month','getUptId'))->setPaper('a4','landscape');					
				}
				return $pdf->download('RT-UPT-TAHUN.pdf');
			}elseif($getUptName == 'SELURUH UPT' && $month && $year){
				if($request->status_pembayaran == 'paid'){
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-bulan-tahun-paid', compact('getUptName','year','month','getUptId'))->setPaper('a4','landscape');

				}else{
					$pdf = PDF::loadView('templates.SPP.RT.dev.print-upt-bulan-tahun-not-paid', compact('getUptName','year','month','getUptId'))->setPaper('a4','landscape');					
				}
				return $pdf->download('RT-UPT-BULAN-TAHUN.pdf');
			}
		}
	}
	public function ORUpdate(Request $request){
		$id = Crypt::decryptString($request->id);
		$rt = RincianTagihan::find($id);		
		$rt->keterangan = $request->keterangan;
		if ($request->hasFile('bukti_dukung')) {
			$files = $request->file('bukti_dukung');
			$cover = 'Bukti Dukung-'.uniqid().$id."."
			.$files->getClientOriginalExtension();
			$files->move(public_path('lampiran/spp/rt'), $cover);
			$rt->bukti_dukung = $cover;	
		}
		$rt->upaya_metode = $request->upaya_metode;
		$rt->tanggal_upaya = $request->tanggal_upaya;
		$rt->bukti_dukung = $request->bukti_dukung;
		$rt->created_by = Auth::user()->id;
		$rt->status = 0;
		$rt->save();
		Session::flash('info', 'RT');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengubah Data!');
		return redirect()->back();
	}


	public function export(Request $request){
		$push = [];				
		$getUptName = 'SELURUH UPT';
		$month = $this->month;
		$year = $this->years;
		if ($month == 1) {
			$dataBulan = 12;
			$fixYear = $year - 1;
		}
		else{
			$dataBulan = $month - 1;
			$fixYear = $year;
		}

		// return $dataBulan;

		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
			$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
			// return $rts;
		}else{

			$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
			$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status',$request->status_pembayaran)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
			// return $rts_data;
		}

		// $tes = json_decode($rts_data);		
		$export = new RTExport([          
			$push,
		]);
		foreach ($rts_data as $key => $value) {
			if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
				$provinsi = $value->province;
				$nama_upt = $value->upt;				
			}else{
				$id_prov = Auth::user()->province_code;
				$kode_upt = Auth::user()->upt;

				$provinsi = Provinsi::where('id', $id_prov)->value('nama');
				$nama_upt = $upts =  UPT::where('office_id',$kode_upt)->select('office_name')->distinct()->value('office_name');				
			}

			$tl = RincianTagihan::where('no_spp', $value->no_spp)->first();

			if($value->keterangan != '' && $value->upaya_metode != '' || $value->tanggal_upaya != ''){
				$metode = Metode::where('id', $value->upaya_metode)->value('metode');
				$tanggal_upaya = $value->tanggal_upaya;
				$keterangan = $value->keterangan;
			}else{
				$metode = '';
				$tanggal_upaya = '';
				$keterangan = '';
			}

			if($request->status_pembayaran == 'paid'){
				$status_pembayaran = 'Telah Bayar';
			}else{
				$status_pembayaran = 'Belum Bayar';
			}


			array_push($push, array(
				$provinsi,
				$nama_upt,
				$value->no_spp,
				$value->no_klien,
				$value->nama_perusahaan,
				$value->tagihan,
				$value->bi_begin,
				$status_pembayaran,
				$value->service_name,
				$metode,
				$tanggal_upaya,
				$keterangan,
			));

			$export = new RTExport([          
				$push,
			]);
		}
		
		return Excel::download($export, 'RT.xlsx');		
	}

	public function export_search(Request $request){
		$push = [];		
		$status_pembayaran = $request->status_pembayaran;		
		$month = $request->month;
		$year = $request->year;
		$myTime = Carbon::now();	
		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){				
			$getUptName = $request->getUptName;
			if ($getUptName != 'SELURUH UPT' && $month != -1 && $year != '') {
				if ($month == 1) {					
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1; 
					$fixYear = $year;
				}	
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();


			}elseif($getUptName != 'SELURUH UPT' && $month == -1 && $year != ''){
				$rts_data = RincianTagihan::whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('status', $status_pembayaran)->where('upt', $getUptName)->get();
				return $this->exportPerYear($rts_data,$year,$status_pembayaran,$getUptName);

			}elseif($getUptName == 'SELURUH UPT' && $month == -1 && $year != ''){
				$rts_data = RincianTagihan::whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('status', $status_pembayaran)->get();
				return $this->exportPerYear($rts_data,$year,$status_pembayaran,$getUptName);

			}elseif($getUptName == 'SELURUH UPT' && $month != -1 && $year == ''){
				$rts = RincianTagihan::whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->paginate(10);
				$rts_data = $rts->items();
			}elseif($getUptName == 'SELURUH UPT' && $month != -1 && $year != ''){
				if ($month == 1) {					
					$dataBulan = 12;
					$fixYear = $year - 1;
				}
				else{
					$dataBulan = $month - 1; 
					$fixYear = $year;
				}
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
				return $this->exportPerMonthYear($rts_data,$month,$year,$status_pembayaran,$getUptName);
			}


		}else{

			$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');

			if ($month != '' && $year != '') {
				// if ($month == 1) {					
				// 	$dataBulan = 12;
				// 	$fixYear = $year - 1;
				// }
				// else{
				// 	$dataBulan = $month - 1; 
				// $fixYear = $year;
				// }	
				$dataBulan = $month;
				$fixYear = $year;
				$rts_data = RincianTagihan::whereMonth('bi_begin',$dataBulan)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();				
			}elseif($month == '' && $year != ''){
				$rts_data = RincianTagihan::whereYear('bi_begin',$year)->where('upt',$getUptName)->where('status', $status_pembayaran)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
			}
		}
		
		if(!empty($rts_data) || isset($rts_data[0]) || $rts_data == '' || $rts_data == null){
			$export = new RTExport([          
				$push,
			]);
			foreach ($rts_data as $key => $value) {
				if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){				
					$getUptId = UPT::where('office_name',$getUptName)->select('province_code')->distinct()->value('province_code');				
					$provinsi = Provinsi::where('id', $getUptId)->value('nama');				
					$nama_upt = $request->getUptName;		
				}else{
					$id_prov = Auth::user()->upt_provinsi;
					$kode_upt = Auth::user()->upt;

					$provinsi = Provinsi::where('id', $id_prov)->value('nama');
					$nama_upt = $upts =  UPT::where('office_id',$kode_upt)->select('office_name')->distinct()->value('office_name');				
				}

				$tl = RincianTagihan::where('no_spp', $value->no_spp)->first();

				if($value->keterangan != '' && $value->upaya_metode != '' || $value->tanggal_upaya != ''){
					$metode = Metode::where('id', $value->upaya_metode)->value('metode');
					$tanggal_upaya = $value->tanggal_upaya;
					$keterangan = $value->keterangan;
				}else{
					$metode = '';
					$tanggal_upaya = '';
					$keterangan = '';
				}

				if($request->status_pembayaran == 'paid'){
					$status_pembayaran = 'Telah Bayar';
				}else{
					$status_pembayaran = 'Belum Bayar';
				}


				array_push($push, array(
					$provinsi,
					$nama_upt,
					$value->no_spp,
					$value->no_klien,
					$value->nama_perusahaan,
					$value->tagihan,
					$value->bi_begin,
					$status_pembayaran,
					$value->service_name,
					$metode,
					$tanggal_upaya,
					$keterangan,
				));

				$export = new RTExport([          
					$push,
				]);

			}
			
			return Excel::download($export, 'RT.xlsx');
		}else{			
			Session::flash('info', 'RT');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Data Kosong!');
			return redirect()->back();
		}
	}

	public function exportPerYear($rts_data,$year,$status_pembayaran,$getUptName)
	{	
		if(Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator'){
			$upt = UPT::select('office_name','office_id')->distinct()->get();
		}else{
			$upt = UPT::where('upt', Auth::user()->upt)->select('office_name','office_id')->distinct()->get();
		}
		$push = [];
		$fixYear = $year - 1; 
		$list_month = ListMonth::all();
		// return $status_pembayaran;


		$export = new RTExport([          
			$push,
		]);
		if($status_pembayaran == 'paid'){
			if($getUptName != ''){
				$januari = RincianTagihan::whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('upt', $getUptName)->count();
				$februari = RincianTagihan::whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('upt', $getUptName)->count();
				$maret = RincianTagihan::whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->where('upt', $getUptName)->count();
				$april = RincianTagihan::whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->where('upt', $getUptName)->count();
				$mei = RincianTagihan::whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->where('upt', $getUptName)->count();
				$juni = RincianTagihan::whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->where('upt', $getUptName)->count();
				$juli = RincianTagihan::whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->where('upt', $getUptName)->count();
				$agustus = RincianTagihan::whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->where('upt', $getUptName)->count();
				$september = RincianTagihan::whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->where('upt', $getUptName)->count();
				$oktober = RincianTagihan::whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('upt', $getUptName)->count();
				$november = RincianTagihan::whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('upt', $getUptName)->count();
				$desember = RincianTagihan::whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->where('upt', $getUptName)->count();

				if($januari == 0){
					$countjanuari = 'BELUM';
				}else{
					$countjanuari = 'YA';
				}

				if($februari == 0){
					$countfebruari = 'BELUM';
				}else{
					$countfebruari = 'YA';
				}

				if($maret == 0){
					$countmaret = 'BELUM';
				}else{
					$countmaret = 'YA';
				}

				if($april == 0){
					$countapril = 'BELUM';
				}else{
					$countapril = 'YA';
				}

				if($mei == 0){
					$countmei = 'BELUM';
				}else{
					$countmei = 'YA';
				}

				if($juni == 0){
					$countjuni = 'BELUM';
				}else{
					$countjuni = 'YA';
				}

				if($juli == 0){
					$countjuli = 'BELUM';
				}else{
					$countjuli = 'YA';
				}

				if($agustus == 0){
					$countagustus = 'BELUM';
				}else{
					$countagustus = 'YA';
				}

				if($september == 0){
					$countseptember = 'BELUM';
				}else{
					$countseptember = 'YA';
				}

				if($oktober == 0){
					$countoktober = 'BELUM';
				}else{
					$countoktober = 'YA';
				}

				if($november == 0){
					$countnovember = 'BELUM';
				}else{
					$countnovember = 'YA';
				}

				if($desember == 0){
					$countdesember = 'BELUM';
				}else{
					$countdesember = 'YA';
				}

				array_push($push, array(
					$getUptName,
					$countjanuari,
					$countfebruari,
					$countmaret,
					$countapril,
					$countmei,
					$countjuni,
					$countjuli,
					$countagustus,
					$countseptember,
					$countoktober,
					$countnovember,
					$countdesember,
				));

				$export = new RTUptTahunExport([          
					$push,
				]);
			}else{	
				foreach ($upt as $key => $value) {
					// return $upt;
					$januari = RincianTagihan::whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('upt', $value->office_name)->count();
					$februari = RincianTagihan::whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
					$maret = RincianTagihan::whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
					$april = RincianTagihan::whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
					$mei = RincianTagihan::whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
					$juni = RincianTagihan::whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
					$juli = RincianTagihan::whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
					$agustus = RincianTagihan::whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
					$september = RincianTagihan::whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
					$oktober = RincianTagihan::whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
					$november = RincianTagihan::whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();
					$desember = RincianTagihan::whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->where('upt', $value->office_name)->count();

					if($januari == 0){
						$countjanuari = 'BELUM';
					}else{
						$countjanuari = 'YA';
					}

					if($februari == 0){
						$countfebruari = 'BELUM';
					}else{
						$countfebruari = 'YA';
					}

					if($maret == 0){
						$countmaret = 'BELUM';
					}else{
						$countmaret = 'YA';
					}

					if($april == 0){
						$countapril = 'BELUM';
					}else{
						$countapril = 'YA';
					}

					if($mei == 0){
						$countmei = 'BELUM';
					}else{
						$countmei = 'YA';
					}

					if($juni == 0){
						$countjuni = 'BELUM';
					}else{
						$countjuni = 'YA';
					}

					if($juli == 0){
						$countjuli = 'BELUM';
					}else{
						$countjuli = 'YA';
					}

					if($agustus == 0){
						$countagustus = 'BELUM';
					}else{
						$countagustus = 'YA';
					}

					if($september == 0){
						$countseptember = 'BELUM';
					}else{
						$countseptember = 'YA';
					}

					if($oktober == 0){
						$countoktober = 'BELUM';
					}else{
						$countoktober = 'YA';
					}

					if($november == 0){
						$countnovember = 'BELUM';
					}else{
						$countnovember = 'YA';
					}

					if($desember == 0){
						$countdesember = 'BELUM';
					}else{
						$countdesember = 'YA';
					}

					array_push($push, array(
						$value->office_name,
						$countjanuari,
						$countfebruari,
						$countmaret,
						$countapril,
						$countmei,
						$countjuni,
						$countjuli,
						$countagustus,
						$countseptember,
						$countoktober,
						$countnovember,
						$countdesember,
					));

					$export = new RTUptTahunExport([          
						$push,
					]);
			}

			}
		}else{

			$paid_rts_januari = RincianTagihan::whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $nopaid_rts_januari = RincianTagihan::whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $total_rts_januari = $paid_rts_januari + $nopaid_rts_januari;
	        $sudah_tl_januari = RincianTagihan::whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
	        $belum_tl_januari = $total_rts_januari - $sudah_tl_januari;
	        $percent_sudah_tl_januari = $sudah_tl_januari / $total_rts_januari * 100;
	        $percent_belum_tl_januari = $belum_tl_januari / $total_rts_januari * 100;
	        $jumlah_rt_januari = RincianTagihan::whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        if ($jumlah_rt_januari != 0) {
	          $percent_jumlah_rt_januari = $jumlah_rt_januari / $total_rts_januari * 100;
	        }else{
	          $percent_jumlah_rt_januari = 0;
	        }
	        $tanggal_upload_januari = RincianTagihan::whereMonth('bi_begin',12)->whereYear('bi_begin',$fixYear)->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->select('updated_at')->distinct()->value('updated_at');

	        //Februari
	        $paid_rts_februari = RincianTagihan::whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $nopaid_rts_februari = RincianTagihan::whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $total_rts_februari = $paid_rts_februari + $nopaid_rts_februari;
	        $sudah_tl_februari = RincianTagihan::whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
	        $belum_tl_februari = $total_rts_februari - $sudah_tl_februari;
	        $percent_sudah_tl_februari = $sudah_tl_februari / $total_rts_februari * 100;
	        $percent_belum_tl_februari = $belum_tl_februari / $total_rts_februari * 100;
	        $jumlah_rt_februari = RincianTagihan::whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        if ($jumlah_rt_februari != 0) {
	          $percent_jumlah_rt_februari = $jumlah_rt_februari / $total_rts_februari * 100;
	        }else{
	          $percent_jumlah_rt_februari = 0;
	        }
	        $tanggal_upload_februari = RincianTagihan::whereMonth('bi_begin',1)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->select('updated_at')->distinct()->value('updated_at');

	        //Maret
	        $paid_rts_maret = RincianTagihan::whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $nopaid_rts_maret = RincianTagihan::whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $total_rts_maret = $paid_rts_maret + $nopaid_rts_maret;
	        $sudah_tl_maret = RincianTagihan::whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
	        $belum_tl_maret = $total_rts_maret - $sudah_tl_maret;
	        $percent_sudah_tl_maret = $sudah_tl_maret / $total_rts_maret * 100;
	        $percent_belum_tl_maret = $belum_tl_maret / $total_rts_maret * 100;
	        $jumlah_rt_maret = RincianTagihan::whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        if ($jumlah_rt_maret != 0) {
	          $percent_jumlah_rt_maret =  $jumlah_rt_maret / $total_rts_maret * 100;
	        }else{
	          $percent_jumlah_rt_maret = 0;
	        }
	        $tanggal_upload_maret = RincianTagihan::whereMonth('bi_begin',2)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->distinct()->value('updated_at');

	        //April
	        $paid_rts_april = RincianTagihan::whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $nopaid_rts_april = RincianTagihan::whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $total_rts_april = $paid_rts_april + $nopaid_rts_april;
	        $sudah_tl_april = RincianTagihan::whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
	        $belum_tl_april = $total_rts_april - $sudah_tl_april;
	        $percent_sudah_tl_april = $sudah_tl_april / $total_rts_april * 100;
	        $percent_belum_tl_april = $belum_tl_april / $total_rts_april * 100;
	        $jumlah_rt_april = RincianTagihan::whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        if ($jumlah_rt_april != 0) {
	          $percent_jumlah_rt_april = $jumlah_rt_april/ $total_rts_april * 100;
	        }else{
	          $percent_jumlah_rt_april = 0;
	        }
	        $tanggal_upload_april = RincianTagihan::whereMonth('bi_begin',3)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->distinct()->value('updated_at');

	        //Mei

	        $paid_rts_mei = RincianTagihan::whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $nopaid_rts_mei = RincianTagihan::whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $total_rts_mei = $paid_rts_mei + $nopaid_rts_mei;
	        $sudah_tl_mei = RincianTagihan::whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
	        $belum_tl_mei = $total_rts_mei - $sudah_tl_mei;
	        $percent_sudah_tl_mei = $sudah_tl_mei / $total_rts_mei * 100;
	        $percent_belum_tl_mei = $belum_tl_mei / $total_rts_mei * 100;
	        $jumlah_rt_mei = RincianTagihan::whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        if ($jumlah_rt_mei != 0) {
	          $percent_jumlah_rt_mei =  $jumlah_rt_mei / $total_rts_mei * 100;
	        }else{
	          $percent_jumlah_rt_mei = 0;
	        }
	        $tanggal_upload_mei = RincianTagihan::whereMonth('bi_begin',4)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->distinct()->value('updated_at');

	        //Juni
	        $paid_rts_juni = RincianTagihan::whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $nopaid_rts_juni = RincianTagihan::whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $total_rts_juni = $paid_rts_juni + $nopaid_rts_juni;
	        $sudah_tl_juni = RincianTagihan::whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
	        $belum_tl_juni = $total_rts_juni - $sudah_tl_juni;
	        $percent_sudah_tl_juni = $sudah_tl_juni / $total_rts_juni * 100;
	        $percent_belum_tl_juni = $belum_tl_juni / $total_rts_juni * 100;
	        $jumlah_rt_juni = RincianTagihan::whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        if ($jumlah_rt_juni != 0) {
	          $percent_jumlah_rt_juni =  $jumlah_rt_juni / $total_rts_juni * 100;
	        }else{
	          $percent_jumlah_rt_juni = 0;
	        }
	        $tanggal_upload_juni = RincianTagihan::whereMonth('bi_begin',5)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->distinct()->value('updated_at');

	        //Juli
	        $paid_rts_juli = RincianTagihan::whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $nopaid_rts_juli = RincianTagihan::whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $total_rts_juli = $paid_rts_juli + $nopaid_rts_juli;
	        $sudah_tl_juli = RincianTagihan::whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('status',1)->count();
	        $belum_tl_juli = $total_rts_juli - $sudah_tl_juli;
	        $percent_sudah_tl_juli = $sudah_tl_juli / $total_rts_juli * 100;
	        $percent_belum_tl_juli = $belum_tl_juli / $total_rts_juli * 100;
	        $jumlah_rt_juli = RincianTagihan::whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        if ($jumlah_rt_juli != 0) {
	          $percent_jumlah_rt_juli = $jumlah_rt_juli / $total_rts_juli * 100;
	        }else{
	          $percent_jumlah_rt_juli = 0;
	        }
	        $tanggal_upload_juli = RincianTagihan::whereMonth('bi_begin',6)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->distinct()->value('updated_at');

	        //Agustus
	        $paid_rts_agustus = RincianTagihan::whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $nopaid_rts_agustus = RincianTagihan::whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $total_rts_agustus = $paid_rts_agustus + $nopaid_rts_agustus;
	        $sudah_tl_agustus = RincianTagihan::whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
	        $belum_tl_agustus = $total_rts_agustus - $sudah_tl_agustus;
	        $percent_sudah_tl_agustus = $sudah_tl_agustus / $total_rts_agustus * 100;
	        $percent_belum_tl_agustus = $belum_tl_agustus / $total_rts_agustus * 100;
	        $jumlah_rt_agustus = RincianTagihan::whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        if ($jumlah_rt_agustus != 0) {
	          $percent_jumlah_rt_agustus =  $jumlah_rt_agustus / $total_rts_agustus * 100;
	        }else{
	          $percent_jumlah_rt_agustus = 0;
	        }
	        $tanggal_upload_agustus = RincianTagihan::whereMonth('bi_begin',7)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->distinct()->value('updated_at');

	        //September
	        $paid_rts_september = RincianTagihan::whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $nopaid_rts_september = RincianTagihan::whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $total_rts_september = $paid_rts_september + $nopaid_rts_september;
	        $sudah_tl_september = RincianTagihan::whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
	        $belum_tl_september = $total_rts_september - $sudah_tl_september;
	        $percent_sudah_tl_september = $sudah_tl_september / $total_rts_september * 100;
	        $percent_belum_tl_september = $belum_tl_september / $total_rts_september * 100;
	        $jumlah_rt_september = RincianTagihan::whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        if ($jumlah_rt_september != 0) {
	          $percent_jumlah_rt_september =  $jumlah_rt_september / $total_rts_september * 100;
	        }else{
	          $percent_jumlah_rt_september = 0;
	        }
	        $tanggal_upload_september = RincianTagihan::whereMonth('bi_begin',8)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->distinct()->value('updated_at');

	        //Oktober
	        $paid_rts_oktober = RincianTagihan::whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $nopaid_rts_oktober = RincianTagihan::whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $total_rts_oktober = $paid_rts_oktober + $nopaid_rts_oktober;
	        $sudah_tl_oktober = RincianTagihan::whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
	        $belum_tl_oktober = $total_rts_oktober - $sudah_tl_oktober;
	        $percent_sudah_tl_oktober = $sudah_tl_oktober / $total_rts_oktober * 100;
	        $percent_belum_tl_oktober = $belum_tl_oktober / $total_rts_oktober * 100;
	        $jumlah_rt_oktober = RincianTagihan::whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        if ($jumlah_rt_oktober != 0) {
	          $percent_jumlah_rt_oktober = $jumlah_rt_oktober / $total_rts_oktober * 100;
	        }else{
	          $percent_jumlah_rt_oktober = 0;
	        }
	        $tanggal_upload_oktober = RincianTagihan::whereMonth('bi_begin',9)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->distinct()->value('updated_at');

	        //November
	        $paid_rts_november = RincianTagihan::whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $nopaid_rts_november = RincianTagihan::whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $total_rts_november = $paid_rts_november + $nopaid_rts_november;
	        $sudah_tl_november = RincianTagihan::whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
	        $belum_tl_november = $total_rts_november - $sudah_tl_november;
	        $percent_sudah_tl_november = $sudah_tl_november / $total_rts_november * 100;
	        $percent_belum_tl_november = $belum_tl_november / $total_rts_november * 100;
	        $jumlah_rt_november = RincianTagihan::whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        if ($jumlah_rt_november != 0) {
	          $percent_jumlah_rt_november =  $jumlah_rt_november / $total_rts_november * 100;
	        }else{
	          $percent_jumlah_rt_november = 0;
	        }
	        $tanggal_upload_november = RincianTagihan::whereMonth('bi_begin',10)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->distinct()->value('updated_at');

	        //Desember
	        $paid_rts_desember = RincianTagihan::whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $nopaid_rts_desember = RincianTagihan::whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        $total_rts_desember = $paid_rts_desember + $nopaid_rts_desember;
	        $sudah_tl_desember = RincianTagihan::whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
	        $belum_tl_desember = $total_rts_desember - $sudah_tl_desember;
	        $percent_sudah_tl_desember = $sudah_tl_desember / $total_rts_desember * 100;
	        $percent_belum_tl_desember = $belum_tl_desember / $total_rts_desember * 100;
	        $jumlah_rt_desember = RincianTagihan::whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();
	        if ($jumlah_rt_desember != 0) {
	          $percent_jumlah_rt_desember =  $jumlah_rt_desember / $total_rts_desember * 100;
	        }else{
	          $percent_jumlah_rt_desember = 0;
	        }
	        $tanggal_upload_desember = RincianTagihan::whereMonth('bi_begin',11)->whereYear('bi_begin',$year)->select('updated_at')->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->distinct()->value('updated_at');
		        // return $total_rts_januari;
			foreach($list_month as $bulan){

				if($bulan->id_bulan == 1){
					$total = $total_rts_januari; 
					$paid = $paid_rts_januari; 
					$sudah_tl = $sudah_tl_januari; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_januari), 0, 5).'%'; 
					$belum_tl = $belum_tl_januari; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_januari), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_januari;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_januari), 0, 5).'%';
					if($tanggal_upload_januari ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_januari)->format('d, M Y');
					}else{
						$tanggalUpload = 'Belum Mengirim';
					}
				}

				if($bulan->id_bulan == 2){
					$total = $total_rts_februari; 
					$paid = $paid_rts_februari; 
					$sudah_tl = $sudah_tl_februari; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_februari), 0, 5).'%'; 
					$belum_tl = $belum_tl_februari; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_februari), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_februari;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_februari), 0, 5).'%';
					if($tanggal_upload_februari ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_februari)->format('d, M Y');
					}else{
						$tanggalUpload = 'Belum Mengirim';
					}
				}
				if($bulan->id_bulan == 3){
					$total = $total_rts_maret; 
					$paid = $paid_rts_maret; 
					$sudah_tl = $sudah_tl_maret; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_maret), 0, 5).'%'; 
					$belum_tl = $belum_tl_maret; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_maret), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_maret;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_maret), 0, 5).'%';
					if($tanggal_upload_maret ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_maret)->format('d, M Y');
					}else{
						$tanggalUpload = 'Belum Mengirim';
					}
				}
				if($bulan->id_bulan == 4){
					$total = $total_rts_april; 
					$paid = $paid_rts_april; 
					$sudah_tl = $sudah_tl_april; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_april), 0, 5).'%'; 
					$belum_tl = $belum_tl_april; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_april), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_april;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_april), 0, 5).'%';
					if($tanggal_upload_april ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_april)->format('d, M Y');
					}else{
						$tanggalUpload = 'Belum Mengirim';
					}
				}
				if($bulan->id_bulan == 5){
					$total = $total_rts_mei; 
					$paid = $paid_rts_mei; 
					$sudah_tl = $sudah_tl_mei; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_mei), 0, 5).'%'; 
					$belum_tl = $belum_tl_mei; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_mei), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_mei;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_mei), 0, 5).'%';
					if($tanggal_upload_mei ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_mei)->format('d, M Y');
					}else{
						$tanggalUpload = 'Belum Mengirim';
					}
				}
				if($bulan->id_bulan == 6){
					$total = $total_rts_juni; 
					$paid = $paid_rts_juni; 
					$sudah_tl = $sudah_tl_juni; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_juni), 0, 5).'%'; 
					$belum_tl = $belum_tl_juni; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_juni), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_juni;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_juni), 0, 5).'%';
					if($tanggal_upload_juni ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_juni)->format('d, M Y');
					}else{
						$tanggalUpload = 'Belum Mengirim';
					}
				}
				if($bulan->id_bulan == 7){
					$total = $total_rts_juli; 
					$paid = $paid_rts_juli; 
					$sudah_tl = $sudah_tl_juli; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_juli), 0, 5).'%'; 
					$belum_tl = $belum_tl_juli; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_juli), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_juli;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_juli), 0, 5).'%';
					if($tanggal_upload_juli ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_juli)->format('d, M Y');
					}else{
						$tanggalUpload = 'Belum Mengirim';
					}
				}
				if($bulan->id_bulan == 8){
					$total = $total_rts_agustus; 
					$paid = $paid_rts_agustus; 
					$sudah_tl = $sudah_tl_agustus; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_agustus), 0, 5).'%'; 
					$belum_tl = $belum_tl_agustus; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_agustus), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_agustus;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_agustus), 0, 5).'%';
					if($tanggal_upload_agustus ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_agustus)->format('d, M Y');
					}else{
						$tanggalUpload = 'Belum Mengirim';
					}
				}
				if($bulan->id_bulan == 9){
					$total = $total_rts_september; 
					$paid = $paid_rts_september; 
					$sudah_tl = $sudah_tl_september; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_september), 0, 5).'%'; 
					$belum_tl = $belum_tl_september; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_september), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_september;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_september), 0, 5).'%';
					if($tanggal_upload_september ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_september)->format('d, M Y');
					}else{
						$tanggalUpload = 'Belum Mengirim';
					}
				}
				if($bulan->id_bulan == 10){
					$total = $total_rts_oktober; 
					$paid = $paid_rts_oktober; 
					$sudah_tl = $sudah_tl_oktober; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_oktober), 0, 5).'%'; 
					$belum_tl = $belum_tl_oktober; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_oktober), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_oktober;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_oktober), 0, 5).'%';
					if($tanggal_upload_oktober ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_oktober)->format('d, M Y');
					}else{
						$tanggalUpload = 'Belum Mengirim';
					}
				}
				if($bulan->id_bulan == 11){
					$total = $total_rts_november; 
					$paid = $paid_rts_november; 
					$sudah_tl = $sudah_tl_november; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_november), 0, 5).'%'; 
					$belum_tl = $belum_tl_november; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_november), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_november;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_november), 0, 5).'%';
					if($tanggal_upload_november ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_november)->format('d, M Y');
					}else{
						$tanggalUpload = 'Belum Mengirim';
					}
				}
				if($bulan->id_bulan == 12){
					$total = $total_rts_desember; 
					$paid = $paid_rts_desember; 
					$sudah_tl = $sudah_tl_desember; 
					$percent_sudah_tl = substr(strip_tags($percent_sudah_tl_desember), 0, 5).'%'; 
					$belum_tl = $belum_tl_desember; 
					$percent_belum_tl = substr(strip_tags($percent_belum_tl_desember), 0, 5).'%'; 
					$jumlah_tl = $jumlah_rt_desember;
					$percent_jumlah = substr(strip_tags($percent_jumlah_rt_desember), 0, 5).'%';
					if($tanggal_upload_desember ){
						$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_desember)->format('d, M Y');
					}else{
						$tanggalUpload = 'Belum Mengirim';
					}
				}
				array_push($push, array(
					$bulan->nama,
					$total,
					$paid,
					$sudah_tl,
					$percent_sudah_tl,
					$belum_tl,
					$percent_belum_tl,
					$jumlah_tl,
					$percent_jumlah,
					$tanggalUpload,
				));

				$export = new RTUptTahunExportNotPaid([          
					$push,
				]);
			}
		}

		return Excel::download($export, 'RT-UPT-TAHUN.xlsx');
	}

	public function exportPerMonthYear($rts_data,$month,$year,$status_pembayaran,$getUptName)
	{	
		$upt = UPT::select('office_id','office_name','province_name')->distinct()->get();
		$push = [];
		if($month == 1){
		  $fixMonth = 12;
		  $fixYear = $year - 1;
		} else{
		  $fixMonth = $month - 1;
		  $fixYear = $year;
		}
		$list_month = ListMonth::all();


		$export = new RTExport([          
			$push,
		]);
		if($status_pembayaran == 'paid'){
			foreach ($upt as $key => $value) {
				$checkingPengiriman = RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('upt',$value->office_name)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->count();

			if($checkingPengiriman == 0){
				$countCheckingPengiriman = 'BELUM';
			}else{
				$countCheckingPengiriman = 'YA';
			}

			array_push($push, array(
				$value->office_name,
				$countCheckingPengiriman,
			));

			$export = new RTUptBulanTahunExport([          
				$push,
			]);

			}
			return Excel::download($export, 'RT-UPT-TAHUN.xlsx');
		}else{
          $paid_rts = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('status','paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$value->office_name)->count();
          $nopaid_rts = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$value->office_name)->count();
          $total_rts = $paid_rts + $nopaid_rts;
          $sudah_tl = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('upt',$value->office_name)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('active',1)->count();
          $belum_tl = $total_rts - $sudah_tl;
          $tanggal_upload = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('upt',$value->office_name)->where('active',1)->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->select('updated_at')->distinct()->value('updated_at');
          $jumlah_rt = \App\Model\SPP\RincianTagihan::whereMonth('bi_begin',$fixMonth)->whereYear('bi_begin',$fixYear)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->where('upt',$value->office_name)->count();
          if($tanggal_upload_november ){
				$tanggalUpload = \Carbon\Carbon::parse($tanggal_upload_november)->format('d, M Y');
			}else{
				$tanggalUpload = 'Belum Mengirim';
			}
			foreach($upt as $value){
				array_push($push, array(
					$value->office_name,
					$total_rts,
					$paid_rts,
					$sudah_tl,
					'0 %',
					$belum_tl,
					'0 %',
					$jumlah_rt,
					'0 %',
					$tanggalUpload,
				));

				$export = new RTUptTahunExportNotPaid([          
					$push,
				]);
			}
		}

		return Excel::download($export, 'RT-UPT-TAHUN.xlsx');
	}

	public function approved(Request $request){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'kepala-upt') {
			if ($request->getStatus == 0) {
				$ids = $request->id;
				if ($request->id) {
					$action = $request->action;
					DB::table('rincian_tagihans')->whereIn('no_spp', $ids)
					->update(['active' => $action,'ket_reject' => $request->catatankasi]);
					Session::flash('info', 'Rincian Tagihan');
					Session::flash('colors', 'green');
					Session::flash('icons', 'fas fa-clipboard-check');
					Session::flash('alert', 'Belum Mengapprove Data!');
					return redirect()->back();
				}
				else{
					Session::flash('info', 'Rincian Tagihan');
					Session::flash('colors', 'red');
					Session::flash('icons', 'fas fa-clipboard-check');
					Session::flash('alert', 'Belum Memilih Data!');
					return redirect()->back();
				}	
			}
			else{
				Session::flash('info', 'Rincian Tagihan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-clipboard-check');
				Session::flash('alert', 'Status Salah!');
				return redirect()->back();
			}
			
		}else{
			Session::flash('info', 'Rincian Tagihan');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function importPreview(Request $request)
	{
		$this->validate($request,[
			'Import' => 'required|file|mimes:xls,xlsx',
		]);

	    // Excel::import(new SettleImport, request()->file('Import'));    
		$collection = (new RtImport)->toArray(request()->file('Import'));

		$body = $collection[0];

		$header = $body[0];

		$result = array_splice($body, 1);
		if($header[0] == 'Provinsi' && $header[1] == 'Nama UPT' && $header[2] == 'No. Tagihan' && $header[3] == 'No. Klien' && $header[4] == 'Nama Klien' && $header[5] == 'BHP (Rp)' && $header[6] == 'Tanggal (BI Begin)' && $header[7] == 'Status Pembayaran' && $header[8] == 'Upaya / Methode' && $header[9] == 'Tanggal Upaya' && $header[10] == 'Keterangan'){

			return view('templates.SPP.RT.import.preview')
			->with('result', $result);
		}else{
			Session::flash('info', 'RT');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Data header tidak cocok!');		
			return redirect()->back();
		}
	}

	public function importPost(Request $request)
	{
		ini_set('max_execution_time',3600);

		$data = $request->all();
		$no_spp = $data['no_spp'];
		$upayametode = $data['upayametode'];
		$tanggal_upaya = $data['tanggal_upaya'];
		$keterangan = $data['keterangan'];
		$filename = $request->file('bukti_dukung');
		$created_at = date('Y-m-d H:m:s');
		$updated_at = date('Y-m-d H:m:s');
		foreach ($no_spp as $key => $value) {
			$rt = RincianTagihan::where('no_spp',$value)->first();
			$uniq = uniqid();
			$filename[$key]->move(public_path('lampiran/spp/rt'),'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension());
			$rt->upaya_metode = isset($upayametode[$key]) ? $upayametode[$key] : '';
			$rt->tanggal_upaya = isset($tanggal_upaya[$key]) ? $tanggal_upaya[$key] : '';
			$rt->keterangan = isset($keterangan[$key]) ? $keterangan[$key] : '';
			$rt->ket_operator = $request->ket_operator;
			$rt->created_by = Auth::user()->id;
			$rt->bukti_dukung = 'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension();
			$rt->active = 0;
			$rt->save();
		}
		Session::flash('info', 'Rincian Tagihan');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengupload Data!');		
		return redirect(url('spp/rt'));
		
		// $data = $request->all();		
		// $no_spp = $data['no_tagihan'];
		// $upayametode = $data['upayametode'];
		// $tanggal_upaya = $data['tanggal_upaya'];
		// $keterangan = $data['keterangan'];
		// $bi_begin = $data['bi_begin'];
		// $filename = $request->file('bukti_dukung');
		// $created_at = date('Y-m-d H:m:s');
		// $updated_at = date('Y-m-d H:m:s');		
		// $rows = [];
		// $variables = array_filter($request->tanggal_upaya);
		// return $variables;
		// foreach($variables as $key => $input) {
		// 	$uniq = uniqid();
			// $filename[$key]->move(public_path('lampiran/spp/rt'),'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension());
			// if($status_pembayaran[$key]  == 'Belum Bayar' || $status_pembayaran[$key]  == 'belum bayar'){
			// 	$pembayaran = 'not paid';
			// }else{
			// 	$pembayaran = 'paid';
			// }

			// dd((int)$no_spp[$key]);
		// 	array_push($rows, 
		// 		[
		// 			'active' => 0,
		// 			'upaya_metode' => isset($upayametode[$key]) ? $upayametode[$key] : '',
		// 			'tanggal_upaya' => isset($tanggal_upaya[$key]) ? $tanggal_upaya[$key] : '',
		// 			'keterangan' => isset($keterangan[$key]) ? $keterangan[$key] : '',
		// 			'ket_operator' => $request->ket_operator,
		// 			'bukti_dukung' => 'Bukti Dukung-'.$uniq.'-'.'.'.$filename[$key]->getClientOriginalExtension(),
		// 			'created_by' => Auth::user()->id,
		// 			'created_at' => $created_at,
		// 			'updated_at' => $updated_at,
		// 		]);
		// }
		// $hasil = RincianTagihan::insert($rows);
		// Session::flash('info', 'RT');
		// Session::flash('colors', 'green');
		// Session::flash('icons', 'fas fa-clipboard-check');
		// Session::flash('alert', 'Berhasil Mengupload Data!');
		// return redirect(url('spp/rt'));

	}

	public function downloadSampel(){

		$push = [];
		$myTime = Carbon::now();
		$month = $myTime->format('m');
		$year = '20'.$myTime->format('y');
		if ($month == 1) {			
			$dataBelumTerbayar = 12;
			$fixYear = $year - 1;
		}
		else{			
			$dataBelumTerbayar = $month - 1; 
			$fixYear = $year;
		}

		$tanggal_upaya = Date('Y-m-d');
		$getUptName = UPT::where('office_id',Auth::user()->upt)->select('office_name')->distinct()->value('office_name');
		$getUptId = UPT::where('office_id',Auth::user()->upt)->select('office_id')->distinct()->value('office_id');
		$rts_nopaid = RincianTagihan::whereMonth('bi_begin',$dataBelumTerbayar)->whereYear('bi_begin',$fixYear)->where('upt',$getUptName)->where('status','not paid')->where('status_izin','Perpanjangan')->where('katagori_spp','Pokok')->get();
		$tes = json_decode($rts_nopaid);
		foreach ($tes as $key => $value) {
			if(Auth::user()->upt_provinsi == 0 || Auth::user()->kode_upt == 0 || Auth::user()->upt_provinsi == null || Auth::user()->kode_upt == null){
				$id_prov = Auth::user()->upt_provinsi;
				$kode_upt = Auth::user()->upt;

				$provinsi = Provinsi::where('id', $id_prov)->value('nama');
				$nama_upt = $upts =  UPT::where('office_id',$kode_upt)->select('office_name')->distinct()->value('office_name');
			}else{
				$id_prov = Auth::user()->upt_provinsi;
				$kode_upt = Auth::user()->kode_upt;

				$provinsi = Provinsi::where('id', $id_prov)->value('nama');
				$nama_upt = $upts =  UPT::where('office_id',$kode_upt)->select('office_name')->distinct()->value('office_name');
			}


			array_push($push, array(
				$provinsi,
				$nama_upt,
				$value->no_spp,
				$value->no_klien,
				$value->nama_perusahaan,
				$value->tagihan,
				$value->bi_begin,
				'Belum Bayar',
				'',
				$tanggal_upaya,
				'',

			));

		}
		$export = new RTExport([          
			$push,
		]);

		return Excel::download($export, 'RT Import.xlsx');

	}
}
