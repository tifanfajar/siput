<?php

namespace App\Http\Controllers\Core\Pelayanan\UNAR;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Service\Unar;
use Illuminate\Support\Facades\Crypt;
use App\Imports\UnarImport;
use App\Exports\UnarExport;
use App\Exports\UnarExportId;
use App\Model\Region\Provinsi;
use App\Model\Region\KabupatenKota;
use App\Model\Setting\UPT;
use App\Model\Privillage\Role;
use Auth;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class UNARController extends Controller
{
	public function index(){
		if (Auth::user()->upt_provinsi == 0 && Auth::user()->id_upt == 0) {
			$unar = Unar::all();
		}
		else{
			$unar = Unar::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->get();
		}
		return view('templates.pelayanan.unar.core.index',compact('unar'));
	}

	public function print(Request $request){
		$date = $request->date;
		$month = $request->month;
		$year = $request->year;
		if ($date && $month && $year) {
			$unar['unar'] = Unar::whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
		}elseif($date && $month){
			$unar['unar'] = Unar::whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->get();
		}elseif($date){
			$unar['unar'] = Unar::whereDay('tanggal_ujian',$date)->get();
		}elseif($month && $year){
			$unar['unar'] = Unar::whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
		}elseif($date && $year){
			$unar['unar'] = Unar::whereDay('tanggal_ujian',$date)->whereYear('tanggal_ujian',$year)->get();
		}elseif($month){
			$unar['unar'] = Unar::whereMonth('tanggal_ujian',$month)->get();
		}elseif($year){
			$unar['unar'] = Unar::whereYear('tanggal_ujian',$year)->get();
		}else{
			$unar['unar'] = Unar::all();
		}
		$pdf = PDF::loadView('templates.pelayanan.unar.dev.print-search', $unar)->setPaper('a4','landscape');
		return $pdf->download('Data-Unar.pdf');
	}

	public function print_search($id, Request $request){
		if($id == null){
			return redirect()->back();
		}
		else{
			$date = $request->date;
			$month = $request->month;
			$year = $request->year;
			if ($date && $month && $year) {
				$unar['unar'] = Unar::where('kode_upt',$id)->whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
			}elseif($date && $month){
				$unar['unar'] = Unar::where('kode_upt',$id)->whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->get();
			}elseif($date){
				$unar['unar'] = Unar::where('kode_upt',$id)->whereDay('tanggal_ujian',$date)->get();
			}elseif($month && $year){
				$unar['unar'] = Unar::where('kode_upt',$id)->whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
			}elseif($date && $year){
				$unar['unar'] = Unar::where('kode_upt',$id)->whereDay('tanggal_ujian',$date)->whereYear('tanggal_ujian',$year)->get();
			}elseif($month){
				$unar['unar'] = Unar::where('kode_upt',$id)->whereMonth('tanggal_ujian',$month)->get();
			}elseif($year){
				$unar['unar'] = Unar::where('kode_upt',$id)->whereYear('tanggal_ujian',$year)->get();
			}else{
				$unar['unar'] = Unar::where('kode_upt',$id)->get();
			}
			$getUptName = UPT::where('office_id',$id)->select('office_name')->distinct()->value('office_name');
			$pdf = PDF::loadView('templates.pelayanan.unar.dev.print', $unar)->setPaper('a4','landscape');
			return $pdf->download('UNAR '.$getUptName.'.pdf');
		}
	}
	public function export()
	{
		$push = [];
		if (Auth::user()->upt_provinsi == 0 && Auth::user()->id_upt == 0) {
            $date = request()->date;
            $month = request()->month;
            $year = request()->year;
            if ($date && $month && $year) {
                $unar = Unar::whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
            }elseif($date && $month){
                $unar = Unar::whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->get();
            }elseif($date){
                $unar = Unar::whereDay('tanggal_ujian',$date)->get();
            }elseif($month && $year){
                $unar = Unar::whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
            }elseif($date && $year){
                $unar = Unar::whereDay('tanggal_ujian',$date)->whereYear('tanggal_ujian',$year)->get();
            }elseif($month){
                $unar = Unar::whereMonth('tanggal_ujian',$month)->get();
            }elseif($year){
                $unar = Unar::whereYear('tanggal_ujian',$year)->get();
            }else{
                $unar = Unar::get();;
            }
		}
		else{
            $date = request()->date;
            $month = request()->month;
            $year = request()->year;
            if ($date && $month && $year) {
                $unar = Unar::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)::whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
            }elseif($date && $month){
                $unar = Unar::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)::whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->get();
            }elseif($date){
                $unar = Unar::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)::whereDay('tanggal_ujian',$date)->get();
            }elseif($month && $year){
                $unar = Unar::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)::whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
            }elseif($date && $year){
                $unar = Unar::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)::whereDay('tanggal_ujian',$date)->whereYear('tanggal_ujian',$year)->get();
            }elseif($month){
                $unar = Unar::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)::whereMonth('tanggal_ujian',$month)->get();
            }elseif($year){
                $unar = Unar::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)::whereYear('tanggal_ujian',$year)->get();
            }else{
                $unar = Unar::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)::get();;
            }
		}
		$export = new UnarExport([          
			$push,
		]);

		foreach ($unar as $key => $value) {			
			$provinsi = Provinsi::where('id', $value->id_prov)->value('nama');
			$nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
			$kabupaten = KabupatenKota::where('id', $value->id_kabkot)->value('nama');

			// return $kabupaten;

			array_push($push, array(
				$value->id,
				$value->tanggal_ujian,
				$value->lokasi_ujian,
				$provinsi,
				$kabupaten,
				$nama_upt,
				$value->jumlah_yd,
				$value->jumlah_yc,
				$value->jumlah_yb,
				$value->lulus_yd,
				$value->lulus_yc,
				$value->lulus_yb,
				$value->tdk_lulus_yd,
				$value->tdk_lulus_yc,
				$value->tdk_lulus_yb,
			));

			$export = new UnarExport([          
				$push,
			]);
		}


		return Excel::download($export, 'Unar.xlsx');
	}
	public function export_search($id){
		$changeName = UPT::where('office_id',$id)->select('office_name')->distinct()->value('office_name');
		if($id == null){
			return redirect()->back();
		}
		else{
			$push = [];
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
            $date = request()->date;
            $month = request()->month;
            $year = request()->year;
            if ($date && $month && $year) {
                $unar = Unar::where('kode_upt', $id)->whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
            }elseif($date && $month){
                $unar = Unar::where('kode_upt', $id)->whereDay('tanggal_ujian',$date)->whereMonth('tanggal_ujian',$month)->get();
            }elseif($date){
                $unar = Unar::where('kode_upt', $id)->whereDay('tanggal_ujian',$date)->get();
            }elseif($month && $year){
                $unar = Unar::where('kode_upt', $id)->whereMonth('tanggal_ujian',$month)->whereYear('tanggal_ujian',$year)->get();
            }elseif($date && $year){
                $unar = Unar::where('kode_upt', $id)->whereDay('tanggal_ujian',$date)->whereYear('tanggal_ujian',$year)->get();
            }elseif($month){
                $unar = Unar::where('kode_upt', $id)->whereMonth('tanggal_ujian',$month)->get();
            }elseif($year){
                $unar = Unar::where('kode_upt', $id)->whereYear('tanggal_ujian',$year)->get();
            }else{
                $unar = Unar::where('kode_upt', $id)->get();;
            }
		}
		$export = new UnarExport([          
			$push,
		]);

		foreach ($unar as $key => $value) {			
			$provinsi = Provinsi::where('id', $value->id_prov)->value('nama');
			$nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
			$kabupaten = KabupatenKota::where('id', $value->id_kabkot)->value('nama');

			// return $kabupaten;

			array_push($push, array(
				$value->id,
				$value->tanggal_ujian,
				$value->lokasi_ujian,
				$provinsi,
				$kabupaten,
				$nama_upt,
				$value->jumlah_yd,
				$value->jumlah_yc,
				$value->jumlah_yb,
				$value->lulus_yd,
				$value->lulus_yc,
				$value->lulus_yb,
				$value->tdk_lulus_yd,
				$value->tdk_lulus_yc,
				$value->tdk_lulus_yb,
			));

			$export = new UnarExport([          
				$push,
			]);
		}

			return Excel::download($export, 'UNAR '.$changeName.'.xlsx');
		}
	}

	public function searchMultiple($id,Request $request){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeUPT = $id;
			if ($changeUPT) {
				$unar = Unar::where('kode_upt',$changeUPT)->get();
				return view('templates.pelayanan.unar.search.index',compact('unar','changeUPT'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Unar');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function save(Request $request){
		$unar = new Unar;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$unar->id_prov = $request->id_prov;
			$unar->kode_upt = $request->id_upt;
			$unar->id_kabkot = $request->id_kabkot;
		}else{
			$unar->id_prov = Auth::user()->upt_provinsi;
			$unar->kode_upt = Auth::user()->id_upt;
			$unar->id_kabkot = $request->id_kabkot;
		}
		$unar->tanggal_ujian = $request->tanggal_ujian;
		$unar->lokasi_ujian = $request->lokasi_ujian;
		$unar->jumlah_yd = $request->jumlah_yd;
		$unar->jumlah_yc = $request->jumlah_yc;
		$unar->jumlah_yb = $request->jumlah_yb;
		$unar->lulus_yd = $request->lulus_yd;
		$unar->lulus_yc = $request->lulus_yc;
		$unar->lulus_yb = $request->lulus_yb;
		$unar->tdk_lulus_yd = $request->tdk_lulus_yd;
		$unar->tdk_lulus_yc = $request->tdk_lulus_yc;
		$unar->tdk_lulus_yb = $request->tdk_lulus_yb;
		$unar->created_by = Auth::user()->id;
		$unar->save();
		Session::flash('info', 'UNAR');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Menambah Data!');
		return redirect()->back();
	}

	public function update(Request $request)
	{
		$id = Crypt::decryptString($request->id);
		$unar = Unar::find($id);
		$unar->tanggal_ujian = $request->tanggal_ujian;
		$unar->lokasi_ujian = $request->lokasi_ujian;
		$unar->tanggal_ujian = $request->tanggal_ujian;
		$unar->jumlah_yd = $request->jumlah_yd;
		$unar->jumlah_yc = $request->jumlah_yc;
		$unar->jumlah_yb = $request->jumlah_yb;
		$unar->lulus_yd = $request->lulus_yd;
		$unar->lulus_yc = $request->lulus_yc;
		$unar->lulus_yb = $request->lulus_yb;
		$unar->tdk_lulus_yd = $request->tdk_lulus_yd;
		$unar->tdk_lulus_yc = $request->tdk_lulus_yc;
		$unar->tdk_lulus_yb = $request->tdk_lulus_yb;
		$unar->created_by = Auth::user()->id;
		$unar->save();
		Session::flash('info', 'Unar');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengubah Data!');
		return redirect()->back();
	}

	public function delete(Request $request){
		$id = Crypt::decryptString($request->id);
		Unar::where('id',$id)->delete();
		Session::flash('info', 'Unar');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-trash');
		Session::flash('alert', 'Berhasil Menghapus Data!');
		return redirect()->back();
	}
	public function search(Request $request){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeID = Provinsi::where('id_map',$request->id_map)->value('id_row');
			$changeUPT = UPT::where('province_code',$changeID)->select('office_id','office_name')->distinct()->value('office_id');
			$getUPT = UPT::where('province_code',$changeID)->select('office_name','office_id')->distinct()->get();
			if ($changeUPT) {
				$count = $getUPT->count();
				if ($count > 1) {
					return view('templates.pelayanan.unar.search.multiple',compact('getUPT'));
				}
				else{
					$unar = Unar::where('kode_upt',$changeUPT)->get();
				}
				return view('templates.pelayanan.unar.search.index',compact('unar','changeUPT'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Unar');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function importPreview(Request $request)
	{
		$this->validate($request,[
			'Import' => 'required|file|mimes:xls,xlsx',
		]);

        // Excel::import(new SettleImport, request()->file('Import'));    
		$collection = (new UnarImport)->toArray(request()->file('Import'));

		$body = $collection[0];

		$header = $body[0];

		$result = array_splice($body, 1);

        // return $header;

        if($header[0] == 'tanggal_ujian' && $header[1] == 'lokasi_ujian' && $header[2] == 'jumlah_yd' && $header[3] == 'jumlah_yc' && $header[4] == 'jumlah_yb' && $header[5] == 'lulus_yd' && $header[6] == 'lulus_yc' && $header[7] == 'lulus_yb' && $header[8] == 'tdk_lulus_yd' && $header[9] == 'tdk_lulus_yc' && $header[10] == 'tdk_lulus_yb'){
            return view('templates.pelayanan.unar.import.preview')
            ->with('result', $result);
        }else{
            Session::flash('info', 'UNAR');
            Session::flash('colors', 'red');
            Session::flash('icons', 'fas fa-clipboard-check');
            Session::flash('alert', 'Data header tidak cocok!');        
            return redirect()->back();
        }
    }

    public function importPost(Request $request)
    {
            $data = $request->all();
            $tanggal_ujian = $data['tanggal_ujian'];
            $lokasi_ujian = $data['lokasi_ujian'];            
            $jumlah_yd = $data['jumlah_yd'];            
            $jumlah_yc = $data['jumlah_yc'];            
            $jumlah_yb = $data['jumlah_yb'];            
            $lulus_yd = $data['lulus_yd'];            
            $lulus_yc = $data['lulus_yc'];
            $lulus_yb = $data['lulus_yb'];
            $tdk_lulus_yd = $data['tdk_lulus_yd'];
            $tdk_lulus_yc = $data['tdk_lulus_yc'];
            $tdk_lulus_yb = $data['tdk_lulus_yb'];
			$id_kabkot = $request->id_kabkot;
            $created_at = date('Y-m-d H:m');
            $updated_at = date('Y-m-d H:m');
            if (Auth::user()->upt_provinsi == 0 && Auth::user()->id_upt == 0) {
                $id_prov = $request->upt_provinsi;
                $kode_upt = $request->id_upt;
            }
            else{
                $id_prov = Auth::user()->upt_provinsi;  
                $kode_upt = Auth::user()->id_upt;                                
            }
        $rows = [];
         foreach($tanggal_ujian as $key => $input) {
            array_push($rows, [
            'tanggal_ujian' => isset($tanggal_ujian[$key]) ? $tanggal_ujian[$key] : '',
            'lokasi_ujian' => isset($lokasi_ujian[$key]) ? $lokasi_ujian[$key] : '',            
            'jumlah_yd' => isset($jumlah_yd[$key]) ? $jumlah_yd[$key] : '',            
            'jumlah_yc' => isset($jumlah_yc[$key]) ? $jumlah_yc[$key] : '',            
            'jumlah_yb' => isset($jumlah_yb[$key]) ? $jumlah_yb[$key] : '',            
            'lulus_yd' => isset($lulus_yd[$key]) ? $lulus_yd[$key] : '',            
            'lulus_yc' => isset($lulus_yc[$key]) ? $lulus_yc[$key] : '',            
            'lulus_yb' => isset($lulus_yb[$key]) ? $lulus_yb[$key] : '',            
            'tdk_lulus_yd' => isset($tdk_lulus_yd[$key]) ? $tdk_lulus_yd[$key] : '',            
            'tdk_lulus_yc' => isset($tdk_lulus_yc[$key]) ? $tdk_lulus_yc[$key] : '',            
            'tdk_lulus_yb' => isset($tdk_lulus_yb[$key]) ? $tdk_lulus_yb[$key] : '',            
            'created_by' => Auth::user()->id,            
            'id_prov' => $id_prov,
            'kode_upt' => $kode_upt,
            'id_kabkot' => $request->id_kabkot,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
            
         ]);
         }
        $hasil = Unar::insert($rows);
        Session::flash('info', 'UNAR');
        Session::flash('colors', 'green');
        Session::flash('icons', 'fas fa-clipboard-check');
        Session::flash('alert', 'Berhasil Mengimport Data!');       
        return redirect(url('pelayanan/unar'));
        
    }
    public function downloadSampel(){

		$responses = response()->download(storage_path("app/public/sampelUnar.xlsx"));
		ob_end_clean();
		return $responses;

	}	
}
