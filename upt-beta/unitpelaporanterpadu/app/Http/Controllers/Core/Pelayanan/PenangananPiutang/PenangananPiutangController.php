<?php

namespace App\Http\Controllers\Core\Pelayanan\PenangananPiutang;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Service\PenangananPiutang;
use Illuminate\Support\Facades\Crypt;
use App\Imports\PenangananPiutangImport;
use App\Exports\PenangananPiutangExport;
use App\Exports\PenangananPiutangExportId;
use App\Model\Region\Provinsi;
use App\Model\Region\KabupatenKota;
use App\Model\Setting\UPT;
use App\Model\Setting\Perusahaan;
use App\Model\Privillage\Role;
use Auth;
use Session;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use DB;

class PenangananPiutangController extends Controller
{
    public function index(){
		if (Auth::user()->upt_provinsi == 0 && Auth::user()->id_upt == 0) {
			$petang = PenangananPiutang::all();
		}
		else{
			$petang = PenangananPiutang::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->get();
		}
		return view('templates.pelayanan.penanganan-piutang.core.index',compact('petang'));
	}

	public function print_search($id, Request $request){
		if($id == null){
			return redirect()->back();
		}
		else{
			$date = $request->date;
			$month = $request->month;
			$year = $request->year;
			if ($date && $month && $year) {
				$petang['petang'] = PenangananPiutang::where('kode_upt',$id)->whereDay('tanggal',$date)->whereMonth('tanggal',$month)->whereYear('tanggal',$year)->get();
			}elseif($date && $month){
				$petang['petang'] = PenangananPiutang::where('kode_upt',$id)->whereDay('tanggal',$date)->whereMonth('tanggal',$month)->get();
			}elseif($date){
				$petang['petang'] = PenangananPiutang::where('kode_upt',$id)->whereDay('tanggal',$date)->get();
			}elseif($month && $year){
				$petang['petang'] = PenangananPiutang::where('kode_upt',$id)->whereMonth('tanggal',$month)->whereYear('tanggal',$year)->get();
			}elseif($date && $year){
				$petang['petang'] = PenangananPiutang::where('kode_upt',$id)->whereDay('tanggal',$date)->whereYear('tanggal',$year)->get();
			}elseif($month){
				$petang['petang'] = PenangananPiutang::where('kode_upt',$id)->whereMonth('tanggal',$month)->get();
			}elseif($year){
				$petang['petang'] = PenangananPiutang::where('kode_upt',$id)->whereYear('tanggal',$year)->get();
			}else{
				$petang['petang'] = PenangananPiutang::where('kode_upt',$id)->get();
			}
			$getUptName = UPT::where('office_id',$id)->select('office_name')->distinct()->value('office_name');
			$pdf = PDF::loadView('templates.pelayanan.penanganan-piutang.dev.print-search', $petang)->setPaper('a4','landscape');
			return $pdf->download('Penanganan Piutang '.$getUptName.'.pdf');
		}
	}
	public function export_search($id){
		$changeName = UPT::where('office_id',$id)->select('office_name')->distinct()->value('office_name');
		if($id == null){
			return redirect()->back();
		}
		else{
			$push = [];

			$date = request()->date;
	        $month = request()->month;
	        $year = request()->year;
	        if ($date && $month && $year) {
	            return PenangananPiutang::where('kode_upt',$id)->where('status',1)->whereDay('tanggal',$date)->whereMonth('tanggal',$month)->whereYear('tanggal',$year)->get();
	        }elseif($date && $month){
	            return PenangananPiutang::where('kode_upt',$id)->where('status',1)->whereDay('tanggal',$date)->whereMonth('tanggal',$month)->get();
	        }elseif($date){
	            return PenangananPiutang::where('kode_upt',$id)->where('status',1)->whereDay('tanggal',$date)->get();
	        }elseif($month && $year){
	            return PenangananPiutang::where('kode_upt',$id)->where('status',1)->whereMonth('tanggal',$month)->whereYear('tanggal',$year)->get();
	        }elseif($date && $year){
	            return PenangananPiutang::where('kode_upt',$id)->where('status',1)->whereDay('tanggal',$date)->whereYear('tanggal',$year)->get();
	        }elseif($month){
	            return PenangananPiutang::where('kode_upt',$id)->where('status',1)->whereMonth('tanggal',$month)->get();
	        }elseif($year){
	            return PenangananPiutang::where('kode_upt',$id)->where('status',1)->whereYear('tanggal',$year)->get();
	        }else{
	            return PenangananPiutang::where('kode_upt',$id)->where('status',1)->get();;
	        }

	        $export = new PenangananPiutangExport([          
				$push,
			]);

			foreach ($petang as $key => $value) {			
				$provinsi = Provinsi::where('id', $value->id_prov)->value('nama');
				$nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
				$kabupaten = KabupatenKota::where('id',$value->nama_kpknl)->value('nama');
				$perusahaan = Perusahaan::where('id', $value->no_client)->value('name');

				// return $perusahaan;

				array_push($push, array(
					$value->id,
					$provinsi,
					$nama_upt,
					$perusahaan,
					$value->nilai_penyerahan,
					$kabupaten,
					$value->tahapan_pengurusan,
					$value->lunas,
					$value->angsuran,
					$value->tanggal,
					$value->psbdt,
					$value->pembatalan,
					$value->sisa_piutang,
					$value->keterangan,
				));

				$export = new PenangananPiutangExport([          
					$push,
				]);

			}
			return Excel::download($export, 'Penanganan Piutang '.$changeName.'.xlsx');
		}
	}

	public function searchMultiple($id,Request $request){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeUPT = $id;
			$provinceCode = PenangananPiutang::where('kode_upt',$changeUPT)->value('id_prov');
			if ($changeUPT) {
				$petang = PenangananPiutang::where('kode_upt',$changeUPT)->get();
				return view('templates.pelayanan.penanganan-piutang.search.index',compact('petang','changeUPT','provinceCode'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Penanganan Piutang');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function save(Request $request){
		$petang = new PenangananPiutang;
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$petang->id_prov = $request->id_prov;
			$petang->kode_upt = $request->id_upt;
			$petang->nama_kpknl = $request->nama_kpknl;
		}else{
			$petang->id_prov = Auth::user()->upt_provinsi;
			$petang->kode_upt = Auth::user()->id_upt;
			$petang->nama_kpknl = $request->nama_kpknl;
		}
		$petang->no_client = $request->no_client;
		$petang->nilai_penyerahan = $request->nilai_penyerahan;
		$petang->tahun_pelimpahan = $request->tahun_pelimpahan;
		$petang->nama_kpknl = $request->nama_kpknl;
		$petang->tahapan_pengurusan = $request->tahapan_pengurusan;
		$petang->lunas = $request->lunas;
		$petang->angsuran = $request->angsuran;
		$petang->tanggal = $request->tanggal;
		$petang->psbdt = $request->psbdt;
		$petang->pembatalan = $request->pembatalan;
		$petang->sisa_piutang = $request->sisa_piutang;
		$petang->keterangan = $request->keterangan;
		$petang->created_by = Auth::user()->id;
		$petang->save();
		Session::flash('info', 'Penanganan Piutang');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Menambah Data!');
		return redirect()->back();
	}

	public function update(Request $request)
	{
		$id = Crypt::decryptString($request->id);
		$petang = PenangananPiutang::find($id);
		$petang->no_client = $request->no_client;
		$petang->nilai_penyerahan = $request->nilai_penyerahan;
		$petang->tahun_pelimpahan = $request->tahun_pelimpahan;
		$petang->nama_kpknl = $request->nama_kpknl;
		$petang->tahapan_pengurusan = $request->tahapan_pengurusan;
		$petang->lunas = $request->lunas;
		$petang->angsuran = $request->angsuran;
		$petang->tanggal = $request->tanggal;
		$petang->psbdt = $request->psbdt;
		$petang->pembatalan = $request->pembatalan;
		$petang->sisa_piutang = $request->sisa_piutang;
		$petang->keterangan = $request->keterangan;
		$petang->created_by = Auth::user()->id;
		$petang->save();
		Session::flash('info', 'Penanganan Piutang');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengubah Data!');
		return redirect()->back();
	}

	public function delete(Request $request){
		$id = Crypt::decryptString($request->id);
		PenangananPiutang::where('id',$id)->delete();
		Session::flash('info', 'Penanganan Piutang');
		Session::flash('colors', 'red');
		Session::flash('icons', 'fas fa-trash');
		Session::flash('alert', 'Berhasil Menghapus Data!');
		return redirect()->back();
	}

	public function updateRejected(Request $request)
	{
		$id = Crypt::decryptString($request->id);
		$petang = PenangananPiutang::find($id);
		$petang->no_client = $request->no_client;
		$petang->nilai_penyerahan = $request->nilai_penyerahan;
		$petang->tahun_pelimpahan = $request->tahun_pelimpahan;
		$petang->nama_kpknl = $request->nama_kpknl;
		$petang->tahapan_pengurusan = $request->tahapanan_pengurusan;
		$petang->lunas = $request->lunas;
		$petang->angsuran = $request->angsuran;
		$petang->tanggal = $request->tanggal;
		$petang->psbdt = $request->psbdt;
		$petang->pembatalan = $request->pembatalan;
		$petang->sisa_piutang = $request->sisa_piutang;
		$petang->keterangan = $request->keterangan;;
		$petang->created_by = Auth::user()->id;
		$petang->status = 3;
		$petang->save();
		Session::flash('info', 'Penanganan Piutang');
		Session::flash('colors', 'green');
		Session::flash('icons', 'fas fa-clipboard-check');
		Session::flash('alert', 'Berhasil Mengubah Data!');
		return redirect()->back();
	}

	public function approved(Request $request){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'kepala-upt') {
			$ids = $request->id;
			if ($request->id) {
				$action = $request->action;
				DB::table('penanganan_piutangs')->whereIn('id', $ids)
				->update(['status' => $action]);
				if ($action == 1) {
					Session::flash('info', 'Penanganan Piutang');
					Session::flash('colors', 'green');
					Session::flash('icons', 'fas fa-clipboard-check');
					Session::flash('alert', 'Berhasil Mengapprove Data!');
				}else{
					Session::flash('info', 'Penanganan Piutang');
					Session::flash('colors', 'red');
					Session::flash('icons', 'fas fa-clipboard-check');
					Session::flash('alert', 'Berhasil Mereject Data!');
				}
				return redirect()->back();
			}
			else{
				Session::flash('info', 'Penanganan Piutang');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-clipboard-check');
				Session::flash('alert', 'Belum Memilih Data!');
				return redirect()->back();
			}
			
		}else{
			Session::flash('info', 'Penanganan Piutang');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function reupload($id){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'operator') {
			$petang = PenangananPiutang::find($id);
			$petang->save();
			Session::flash('info', 'Penanganan Piutang');
			Session::flash('colors', 'green');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Berhasil Mengirim Ulang');
			return redirect()->back();
		}
		else{
			Session::flash('info', 'Penanganan Piutang');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}

	public function search(Request $request){
		if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
			$changeID = Provinsi::where('id_map',$request->id_map)->value('id_row');
			$changeUPT = UPT::where('province_code',$changeID)->select('office_id','office_name')->distinct()->value('office_id');
			$provinceCode = PenangananPiutang::where('kode_upt',$changeUPT)->value('id_prov');
			$getUPT = UPT::where('province_code',$changeID)->select('office_name','office_id')->distinct()->get();
			if ($changeUPT) {
				$count = $getUPT->count();
				if ($count > 1) {
					return view('templates.pelayanan.penanganan-piutang.search.multiple',compact('getUPT'));
				}
				else{
					$petang = PenangananPiutang::where('kode_upt',$changeUPT)->get();
				}
				return view('templates.pelayanan.penanganan-piutang.search.index',compact('petang','changeUPT','provinceCode'));
			}
			else{
				Session::flash('info', 'Terjadi Kesalahan');
				Session::flash('colors', 'red');
				Session::flash('icons', 'fas fa-times');
				Session::flash('alert', 'Tidak ada UPT di Provinsi tersebut!');
				return redirect()->back();

			}
		}
		else{
			Session::flash('info', 'Penanganan Piutang');
			Session::flash('colors', 'red');
			Session::flash('icons', 'fas fa-clipboard-check');
			Session::flash('alert', 'Tidak Memiliki Akses!');
			return redirect()->back();
		}
	}
	public function print(Request $request){
		$date = $request->date;
		$month = $request->month;
		$year = $request->year;
		if ($date && $month && $year) {
			$petang['petang'] = PenangananPiutang::whereDay('tanggal',$date)->whereMonth('tanggal',$month)->whereYear('tanggal',$year)->get();
		}elseif($date && $month){
			$petang['petang'] = PenangananPiutang::whereDay('tanggal',$date)->whereMonth('tanggal',$month)->get();
		}elseif($date){
			$petang['petang'] = PenangananPiutang::whereDay('tanggal',$date)->get();
		}elseif($month && $year){
			$petang['petang'] = PenangananPiutang::whereMonth('tanggal',$month)->whereYear('tanggal',$year)->get();
		}elseif($date && $year){
			$petang['petang'] = PenangananPiutang::whereDay('tanggal',$date)->whereYear('tanggal',$year)->get();
		}elseif($month){
			$petang['petang'] = PenangananPiutang::whereMonth('tanggal',$month)->get();
		}elseif($year){
			$petang['petang'] = PenangananPiutang::whereYear('tanggal',$year)->get();
		}else{
			$petang['petang'] = PenangananPiutang::all();
		}
		$pdf = PDF::loadView('templates.pelayanan.penanganan-piutang.dev.print', $petang)->setPaper('a4','landscape');
		return $pdf->download('Data-Penanganan-Piutang.pdf');
	}
	public function export()
	{
		$push = [];
		if (Auth::user()->upt_provinsi == 0 && Auth::user()->id_upt == 0) {
            $date = request()->date;
            $month = request()->month;
            $year = request()->year;
            if ($date && $month && $year) {
                $petang = PenangananPiutang::where('status',1)->whereDay('tanggal',$date)->whereMonth('tanggal',$month)->whereYear('tanggal',$year)->get();
            }elseif($date && $month){
                $petang = PenangananPiutang::where('status',1)->whereDay('tanggal',$date)->whereMonth('tanggal',$month)->get();
            }elseif($date){
                $petang = PenangananPiutang::where('status',1)->whereDay('tanggal',$date)->get();
            }elseif($month && $year){
                $petang = PenangananPiutang::where('status',1)->whereMonth('tanggal',$month)->whereYear('tanggal',$year)->get();
            }elseif($date && $year){
                $petang = PenangananPiutang::where('status',1)->whereDay('tanggal',$date)->whereYear('tanggal',$year)->get();
            }elseif($month){
                $petang = PenangananPiutang::where('status',1)->whereMonth('tanggal',$month)->get();
            }elseif($year){
                $petang = PenangananPiutang::where('status',1)->whereYear('tanggal',$year)->get();
            }else{
                $petang = PenangananPiutang::where('status',1)->get();;
            }
		}
		else{
            $date = request()->date;
            $month = request()->month;
            $year = request()->year;
            if ($date && $month && $year) {
                $petang = PenangananPiutang::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->where('status',1)->whereDay('tanggal',$date)->whereMonth('tanggal',$month)->whereYear('tanggal',$year)->get();
            }elseif($date && $month){
                $petang = PenangananPiutang::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->where('status',1)->whereDay('tanggal',$date)->whereMonth('tanggal',$month)->get();
            }elseif($date){
                $petang = PenangananPiutang::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->where('status',1)->whereDay('tanggal',$date)->get();
            }elseif($month && $year){
                $petang = PenangananPiutang::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->where('status',1)->whereMonth('tanggal',$month)->whereYear('tanggal',$year)->get();
            }elseif($date && $year){
                $petang = PenangananPiutang::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->where('status',1)->whereDay('tanggal',$date)->whereYear('tanggal',$year)->get();
            }elseif($month){
                $petang = PenangananPiutang::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->where('status',1)->whereMonth('tanggal',$month)->get();
            }elseif($year){
                $petang = PenangananPiutang::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->where('status',1)->whereYear('tanggal',$year)->get();
            }else{
                $petang = PenangananPiutang::where('kode_upt',Auth::user()->id_upt)->where('id_prov',Auth::user()->upt_provinsi)->where('status',1)->get();;
            }
		}
		$export = new PenangananPiutangExport([          
			$push,
		]);

		foreach ($petang as $key => $value) {			
			$provinsi = Provinsi::where('id', $value->id_prov)->value('nama');
			$nama_upt = UPT::where('office_id',$value->kode_upt)->select('office_name')->distinct()->value('office_name');
			$kabupaten = KabupatenKota::where('id',$value->nama_kpknl)->value('nama');
			$perusahaan = Perusahaan::where('id', $value->no_client)->value('name');

			// return $perusahaan;

			array_push($push, array(
				$value->id,
				$provinsi,
				$nama_upt,
				$perusahaan,
				$value->nilai_penyerahan,
				$kabupaten,
				$value->tahapan_pengurusan,
				$value->lunas,
				$value->angsuran,
				$value->tanggal,
				$value->psbdt,
				$value->pembatalan,
				$value->sisa_piutang,
				$value->keterangan,
			));

			$export = new PenangananPiutangExport([          
				$push,
			]);
		}
		return Excel::download($export, 'PenangananPiutang.xlsx');
	}

	public function importPreview(Request $request)
    {
        $this->validate($request,[
            'Import' => 'required|file|mimes:xls,xlsx',
        ]);

        // Excel::import(new SettleImport, request()->file('Import'));    
        $collection = (new PenangananPiutangImport)->toArray(request()->file('Import'));

        $body = $collection[0];

        $header = $body[0];

        $result = array_splice($body, 1);

        // return $result;

        if($header[0] == 'nilai_penyerahan' && $header[1] == 'tahun_pelimpahan' && $header[2] == 'nama_kpknl' && $header[3] == 'tahapanan_pengurusan' && $header[4] == 'lunas' && $header[5] == 'angsuran' && $header[6] == 'tanggal' && $header[7] == 'psbdt' && $header[8] == 'pembatalan' && $header[9] == 'sisa_piutang' && $header[10] == 'keterangan'){

            return view('templates.pelayanan.penanganan-piutang.import.preview')
            ->with('result', $result);
        }else{
            Session::flash('info', 'Penanganan Piutang');
            Session::flash('colors', 'red');
            Session::flash('icons', 'fas fa-clipboard-check');
            Session::flash('alert', 'Data header tidak cocok!');        
            return redirect()->back();
        }
    }

    public function importPost(Request $request)
    {
            $data = $request->all();
            $nilai_penyerahan = $data['nilai_penyerahan'];
            $tahun_pelimpahan = $data['tahun_pelimpahan'];            
            $tahapanan_pengurusan = $data['tahapanan_pengurusan'];
            $lunas = $data['lunas'];
            $angsuran = $data['angsuran'];
            $tanggal = $data['tanggal'];
            $psbdt = $data['psbdt'];
            $pembatalan = $data['pembatalan'];
            $sisa_piutang = $data['sisa_piutang'];
            $keterangan = $data['keterangan'];
            $created_at = date('Y-m-d H:m');
            $updated_at = date('Y-m-d H:m');
            if (Auth::user()->upt_provinsi == 0 && Auth::user()->id_upt == 0) {
                $id_prov = $request->upt_provinsi;
                $kode_upt = $request->id_upt;                
            }
            else{
                $id_prov = Auth::user()->upt_provinsi;  
                $kode_upt = Auth::user()->id_upt;                
            }
        $rows = [];
         foreach($tanggal as $key => $input) {
            array_push($rows, [            
            'nilai_penyerahan' => isset($nilai_penyerahan[$key]) ? $nilai_penyerahan[$key] : '',
            'tahun_pelimpahan' => isset($tahun_pelimpahan[$key]) ? $tahun_pelimpahan[$key] : '',
            'nama_kpknl' => $request->id_kabkot,
            'tahapanan_pengurusan' => isset($tahapanan_pengurusan[$key]) ? $tahapanan_pengurusan[$key] : '',
            'lunas' => isset($lunas[$key]) ? $lunas[$key] : '',
            'angsuran' => isset($angsuran[$key]) ? $angsuran[$key] : '',
            'tanggal' => isset($tanggal[$key]) ? $tanggal[$key] : '',
            'psbdt' => isset($psbdt[$key]) ? $psbdt[$key] : '',
            'pembatalan' => isset($pembatalan[$key]) ? $pembatalan[$key] : '',
            'sisa_piutang' => isset($sisa_piutang[$key]) ? $sisa_piutang[$key] : '',
            'keterangan' => isset($keterangan[$key]) ? $keterangan[$key] : '',
            'created_by' => Auth::user()->id,            
            'no_client' => $request->nama_perusahaan,
            'id_prov' => $id_prov,
            'kode_upt' => $kode_upt,
            'status' => 0,
            'created_at' => $created_at,
            'updated_at' => $updated_at,
            
         ]);
         }
        $hasil = PenangananPiutang::insert($rows);
        Session::flash('info', 'Penanganan Piutang');
        Session::flash('colors', 'green');
        Session::flash('icons', 'fas fa-clipboard-check');
        Session::flash('alert', 'Berhasil Mengimport Data!');       
        return json_encode($hasil);
        
    }

	public function downloadSampel(){

		$responses = response()->download(storage_path("app/public/sampelPenangananPiutang.xlsx"));
		ob_end_clean();
		return $responses;

	}
}
