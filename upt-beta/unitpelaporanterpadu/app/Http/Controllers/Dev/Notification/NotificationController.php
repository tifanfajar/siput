<?php

namespace App\Http\Controllers\Dev\Notification;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Notification\Notification;
use App\Model\Menu\Module;
use App\Model\Privillage\Role;
use App\Model\Region\Provinsi;
use App\Model\Setting\UPT;
use Auth;

class NotificationController extends Controller
{
    public function getNotification($id_module)
    {
    	if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
    		$getNotification = Notification::where('id_module', $id_module)->get();
    	}else{
    		$getNotification = Notification::where('id_module', $id_module)->where('id_upt', Auth::user()->upt)->get();
    	}
    	$html = '';
    	$id = '';
    	foreach ($getNotification as $notification) {
    		$module = Module::where('id', $notification->id_module)->value('module_name');
			if ($notification != '') {
				$html .= '<tr><td>'.$module.'</td><td>'.$notification->message.'</td><td>'.$notification->created_at.'</td></tr>';
				$id .= $notification->id.',';
			}
			else{
				$html = '<tr><td>Tidak Ada Pemberitahuan</td></tr>';
			}
		}

    	return response()->json(['html' => $html,'id' => $id]);
    }

    public function updateToRead($id)
    {
    	$idReal = substr($id,0,-1);
    	$explode=explode(",",$idReal);
    	$jml_notip = 0;
    	foreach($explode as $value){
    	$notip = Notification::where('id', $value)->count();
    	$jml_notip += $notip;
			$hasil = Notification::where('id', $value)
			->update(['readable' => 1]);
    		
    	}
    	return $jml_notip;

    }
}
