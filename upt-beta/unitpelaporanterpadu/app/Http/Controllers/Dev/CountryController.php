<?php

namespace App\Http\Controllers\Dev;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Model\Region\KabupatenKota;
use App\Model\Privillage\Role;
use Auth;

class CountryController extends Controller
{
	public function getKabupaten($id,Request $request)
	{
		if (!$id) {
			$html = '<option value="">Tidak Tersedia</option>';
		} else {
			$html = '';
			if (Role::where('id',Auth::user()->role_id)->value('akses') == 'administrator') {
				$cities = KabupatenKota::where('id_prov', $id)->get();
				foreach ($cities as $city) {
					if ($city) {
						$html .= '<option value="'.$city->id.'">'.$city->nama.'</option>';
					}
					else{
						$html = '<option value="">Tidak Tersedia</option>';
					}
				}
			}
			else{
				$cities = KabupatenKota::where('id_prov', Auth::user()->upt_provinsi)->get();
				foreach ($cities as $city) {
					if ($city) {
						$html .= '<option value="'.$city->id.'">'.$city->nama.'</option>';
					}
					else{
						$html = '<option value="">Tidak Tersedia</option>';
					}
				}
			}
			
		}

		return response()->json(['html' => $html]);
	}
}
