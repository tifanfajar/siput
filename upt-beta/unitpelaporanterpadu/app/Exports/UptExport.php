<?php

namespace App\Exports;

use App\Model\Setting\UPT;
use Maatwebsite\Excel\Concerns\FromCollection;

class UptExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return UPT::all();
    }
}
