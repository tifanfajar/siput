<?php

namespace App\Exports;

use App\Model\Service\PenangananPiutang;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromArray;
use Auth;


class PenangananPiutangExport implements FromArray, WithHeadings, WithCustomStartCell, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(array $invoices)
    {
        $this->invoices = $invoices;
    }

    public function array(): array
    {
        return $this->invoices;
    }

    public function headings(): array
    {
        return ['ID', 'Provinsi', 'Nama UPT', 'Nomor Client','Nilai Penyerahan', 'Nama KPKNL', 'Tahapan Pengurusan', 'Lunas', 'Angsuran', 'Tanggal',
         'PSBDT', 'Pembatalan', 'Sisa Piutang', 'Keterangan'];
    }

    public function startCell(): string
    {
        return 'A1';
    }
}
