<?php

namespace App\Exports;

use App\Model\SPP\RT;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromArray;

use Maatwebsite\Excel\Concerns\Exportable;
use Auth;
use Carbon\Carbon;

class RTExport implements FromArray, WithHeadings, WithCustomStartCell, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct(array $invoices)
    {
        $this->invoices = $invoices;
    }

    public function array(): array
    {
        return $this->invoices;
    }

    public function headings(): array
    {
        return ['Provinsi', 'Nama UPT', 'No. Tagihan', 'No. Klien','Nama Klien', 'BHP (Rp)', 'Tanggal (BI Begin)', 'Status Pembayaran','Service','Upaya / Methode','Tanggal Upaya', 'Keterangan'];
    }

    public function startCell(): string
    {
        return 'A1';
    }

}
