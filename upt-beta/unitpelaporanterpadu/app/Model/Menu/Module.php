<?php

namespace App\Model\Menu;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
  protected $table = 'modules';
  protected $primaryKey = 'id';

  protected $fillable = [
    'MENU_PARENT',
    'MODULE_NAME',
    'MENU_MASK',
    'MENU_PATH',
    'MENU_ICON',
    'MENU_ORDER',
    'DIVIDER'
  ];
}
