<?php 
//General Controller
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();

//Model User
include "model/user.php";
$md_user      = new user();
session_start();

//Check Access Tab
$akses_tab = $gen_model->GetOneRow("tr_function_access_tab",array('id_tab'=>13,'kode_function_access'=>$_SESSION['group_user'])); 
if(empty($akses_tab['fua_edit'])){
  $akses_tab['fua_edit'] = 0;
}
if(empty($akses_tab['fua_delete'])){
  $akses_tab['fua_delete'] = 0;
}


$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}

$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //Check Session
  $gen_controller->check_session();
  //View
  $gen_controller->response_code('404');
}
else if($act=="form"){
   include "view/default_style.php"; 
   $form_url  = $_REQUEST['form_url'];
   $activity  = $_REQUEST['activity'];
   if($form_url=="Form_Pengguna"){
      include "view/ajax_form/pengguna_form_data.php";
   }
}
else if($act=="cetak"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/daftar_pengguna.php";
}
else if($act=="table_filter"){
   $param  = $_REQUEST['param'];
   include "view/ajax_table/daftar_pengguna_filter.php";
}
else if($act=="do_add"){

    //File
    $tmp              = $_FILES["photo"]["tmp_name"];
    $origin_file_name = $_FILES['photo']['name'];
    if(!empty($origin_file_name)){
      $ext              = strtolower(pathinfo($_FILES["photo"]["name"])['extension']);
      $file_name        = date("ymdhis")."_".rand(1000,9999).".".$ext;
      $path             = "assets/attachment/daftar_pengguna/";
    }

    if(!empty($_SESSION['kode_user'])){
      //Proses
      $insert_data = array();
      $insert_data['kode_user']           = "us_".date("ymdhis")."_".rand(1000,9999);
      $insert_data['username']            = get($_POST['username']);
      $insert_data['password']            = hash('crc32b',$_POST['password']);
      $insert_data['nama_lengkap']        = get($_POST['nama_lengkap']);
      $insert_data['group_user']          = get($_POST['group_user']);
      $insert_data['no_fax']              = get($_POST['no_fax']);
      $insert_data['no_tlp']              = get($_POST['no_tlp']);
      $insert_data['no_hp']               = get($_POST['no_hp']);
      $insert_data['email']               = get($_POST['email']);
      $insert_data['alamat']              = get($_POST['alamat']);
      $insert_data['created_date']        = $date_now_indo_full;
      $insert_data['last_update']         = $date_now_indo_full;
      $insert_data['created_by']          = $_SESSION['kode_user'];
      $insert_data['last_update_by']      = $_SESSION['kode_user'];
      $insert_data['status']              = "1";
      $insert_data['is_deleted']          = "0";

      if(!empty($origin_file_name)){
         $insert_data['pas_foto']         = $file_name;
      }

      $grp     = $gen_model->GetOne("group_name","ms_group",array('kode_group'=>$_POST['group_user'])); 
      $fa      = $gen_model->GetOne("kode_function_access","tr_function_access",array('function_access'=>$grp)); 
      $insert_data['level']          = $fa;
       //Validation
      $count_username = $db->GetOne("select count(*) as total from ms_user where username='".$_POST['username']."'"); 
      $count_email = $db->GetOne("select count(*) as total from ms_user where email='".$_POST['email']."'"); 

      if($count_username>0){
        echo 'Username sudah terdaftar di database';
      }
      // else if($count_email>0){
      //   echo 'Email sudah terdaftar di database';
      // }
      else {
        if($_POST['group_user']=="grp_171026194411_1143" or $_POST['group_user']=="grp_171026194411_1144"){
            $insert_data['upt_provinsi']          = get($_POST['upt_provinsi']);
            $insert_data['kode_upt']              = get($_POST['kode_upt']);
            if(empty($_POST['upt_provinsi'])){
              echo "Provinsi belum dipilih";
            }  
            else if(empty($_POST['kode_upt'])){
              echo "Nama UPT belum dipilih";
            } 
            else {
             echo $gen_model->Insert('ms_user',$insert_data);
              if(!empty($origin_file_name)){
                $gen_controller->upload_file($tmp,$path,$file_name);
              }
            } 
        }
        else {
          echo $gen_model->Insert('ms_user',$insert_data);
          if(!empty($origin_file_name)){
            $gen_controller->upload_file($tmp,$path,$file_name);
          }
        }
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="edit" and $id_parameter!=""){
    $edit  = $gen_model->GetOneRow("ms_user",array('kode_user'=>$id_parameter)); 
    foreach($edit as $key=>$val){
                  $key=strtolower($key);
                  $$key=$val;
    }
    $data = array(
      'kode_user'=>$kode_user,
      'username'=>$username,
      'status'=>$status,
      'level'=>$level,
      'group_user'=>$group_user,
      'upt_provinsi'=>$upt_provinsi,
      'kode_upt'=>$kode_upt,
      'nama_lengkap'=>$nama_lengkap,
      'no_fax'=>$no_fax,
      'no_tlp'=>$no_tlp,
      'no_hp'=>$no_hp,
      'email'=>$email,
      'alamat'=>$alamat
    );
    echo json_encode($data); 
}
else if($act=="do_update"){

    //File
    $tmp              = $_FILES["photo"]["tmp_name"];
    $origin_file_name = $_FILES['photo']['name'];
    if(!empty($origin_file_name)){
      $ext              = strtolower(pathinfo($_FILES["photo"]["name"])['extension']);
      $file_name        = date("ymdhis")."_".rand(1000,9999).".".$ext;
      $path             = "assets/attachment/daftar_pengguna/";
    }

    if(!empty($_SESSION['kode_user'])){
      //Proses
      $update_data = array();
      $update_data['username']            = get($_POST['username']);
      $update_data['nama_lengkap']        = get($_POST['nama_lengkap']);
      $update_data['group_user']          = get($_POST['group_user']);
      $update_data['no_fax']              = get($_POST['no_fax']);
      $update_data['no_tlp']              = get($_POST['no_tlp']);
      $update_data['no_hp']               = get($_POST['no_hp']);
      $update_data['email']               = get($_POST['email']);
      $update_data['alamat']              = get($_POST['alamat']);
      $update_data['last_update']         = $date_now_indo_full;
      $update_data['last_update_by']      = $_SESSION['kode_user'];
      $update_data['status']              = get($_POST['status']);
      $grp     = $gen_model->GetOne("group_name","ms_group",array('kode_group'=>$_POST['group_user'])); 
      $fa      = $gen_model->GetOne("kode_function_access","tr_function_access",array('function_access'=>$grp)); 
      $update_data['level']          = $fa;

      if(!empty($_POST['password'])){
          $update_data['password']       = hash('crc32b',$_POST['password']);
      }

      if(!empty($origin_file_name)){
         $update_data['pas_foto']         = $file_name;
      }

       //Paramater
      $where_data = array();
      $where_data['kode_user']       = $_POST['kode_user'];

     
       //Validation
      $count_username = $db->GetOne("select count(*) as total from ms_user where username='".$_POST['username']."' and kode_user!='".$_POST['kode_user']."' "); 
      $count_email = $db->GetOne("select count(*) as total from ms_user where email='".$_POST['email']."' and kode_user!='".$_POST['kode_user']."' "); 

      if($count_username>0){
        echo 'Username sudah terdaftar di database';
      }
      // else if($count_email>0){
      //   echo 'Email sudah terdaftar di database';
      // }
      else {
        if($_POST['group_user']=="grp_171026194411_1143" or $_POST['group_user']=="grp_171026194411_1144"){
            $update_data['upt_provinsi']          = get($_POST['upt_provinsi']);
            $update_data['kode_upt']              = get($_POST['kode_upt']);
            if(empty($_POST['upt_provinsi'])){
              echo "Provinsi belum dipilih";
            } 
            else if(empty($_POST['kode_upt'])){
              echo "Nama UPT belum dipilih";
            } 
            else {
             echo $gen_model->Update('ms_user',$update_data,$where_data);
              if(!empty($origin_file_name)){
                $gen_controller->upload_file($tmp,$path,$file_name);
              }
            } 
        }
        else {
            echo $gen_model->Update('ms_user',$update_data,$where_data);
            if(!empty($origin_file_name)){
              $gen_controller->upload_file($tmp,$path,$file_name);
            }
        }
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="delete"){
    if(!empty($_SESSION['kode_user'])){
    

      $update_data = array();
      $update_data['is_deleted']            = "1";
      $update_data['status']                = "2";

       //Paramater
      $where_data = array();
      $where_data['kode_user']       = $_POST['kode_user'];


       //Validation
      if(!empty($_POST['kode_user'])){
           echo $gen_model->Update('ms_user',$update_data,$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="get_upt"){
    if(!empty($_SESSION['kode_user'])){
      $id_parameter = $_REQUEST['provinsi'];

          $data_upt = $gen_model->GetWhere('ms_upt',array('id_prov'=>$id_parameter));
          echo '<option value="">Pilih UPT</option>';

          if($id_parameter=="All"){
            echo '<option value="All">Semua UPT</option>';
          }
          else if($id_parameter=="Only_Admin"){
            echo '<option value="Only_Admin">Hanya Admin</option>';
          }
            while($list = $data_upt->FetchRow()){
              foreach($list as $key=>$val){
                          $key=strtolower($key);
                          $$key=$val;
                        }  
          ?>
        <option value="<?php echo $kode_upt ?>"><?php echo $nama_upt ?> - <?php echo $alamat ?></option>
        <?php }

    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="get_kpknl"){
    if(!empty($_SESSION['kode_user'])){
      $id_parameter = $_REQUEST['provinsi'];
          $data_upt = $gen_model->GetWhere('ms_kabupaten_kota',array('id_provinsi'=>$id_parameter));
          echo '<option value="">Pilih KPKNL</option>';
          if($id_parameter=="All"){
            echo '<option value="All">Semua KPKNL</option>';
          }
          else if($id_parameter=="Only_Admin"){
            echo '<option value="Only_Admin">Hanya Admin</option>';
          }
            while($list = $data_upt->FetchRow()){
              foreach($list as $key=>$val){
                          $key=strtolower($key);
                          $$key=$val;
                        }  
          ?>
        <option value="<?php echo $id_kabkot ?>"><?php echo $nama_kab_kot ?></option>
        <?php }

    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="get_kabkot"){
    if(!empty($_SESSION['kode_user'])){
      $id_parameter = $_REQUEST['provinsi'];

          $data_upt = $gen_model->GetWhere('ms_kabupaten_kota',array('id_provinsi'=>$id_parameter));
          echo '<option value="">Pilih Kabupaten/Kota</option>';
          if($id_parameter=="All"){
            echo '<option value="All">Semua Kabupaten/Kota</option>';
          }
          else if($id_parameter=="Only_Admin"){
            echo '<option value="Only_Admin">Hanya Admin</option>';
          }
            while($list = $data_upt->FetchRow()){
              foreach($list as $key=>$val){
                          $key=strtolower($key);
                          $$key=$val;
                        }  
          ?>
        <option value="<?php echo $id_kabkot ?>"><?php echo $nama_kab_kot ?></option>
        <?php }

    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="get_kabkot_with_code"){
    if(!empty($_SESSION['kode_user'])){
      $id_parameter = $_REQUEST['provinsi'];
      $with_act     = $_REQUEST['with_act'];
          $data_upt = $gen_model->GetWhere('ms_kabupaten_kota',array('id_provinsi'=>$id_parameter));
          echo '<table class="table table-bordered" style="display: block;max-height: 200px;overflow-y: auto;">
                  <tr>
                    <th>Kode Kabupaten/Kota</th>
                    <th>Kabupaten/Kota</th>
                  </tr>';
            while($list = $data_upt->FetchRow()){
              foreach($list as $key=>$val){
                          $key=strtolower($key);
                          $$key=$val;
                        }  
          ?>
            <tr>
                <td><?php echo $id_kabkot ?></td>
                <!--<td><?php //echo ($with_act=="piutang" ? 'KPKNL ' : '').$nama_kab_kot ?></td>-->
                <td><?php echo ($with_act=="piutang" ? '' : '').$nama_kab_kot ?></td>
              </tr>
        <?php }
        echo '</table>';
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest"){
  $aColumns = array('us.username','us.nama_lengkap','grp.group_name','us.status','pv.nama_prov','upt.nama_upt','us.no_tlp','us.no_hp','us.email','us.created_date','us.last_update','us.kode_user'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_user->getDataUser($sWhere,$sOrder,$sLimit);
  $rResultFilterTotal   = $md_user->getCountUser($sWhere);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_user'];

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
       $edit = '<button  data-toggle="modal" data-target="#edit_modal" type="button" onclick="do_edit_user(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>';
    }
    if($akses_tab['fua_delete']=="1"){ 
       $delete = '&nbsp; <button type="button" onclick="do_delete_user(\''.base64_encode('kode_user').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
     }

        
    $sts = "";
    if($aRow['status']=="0"){
      $sts = "Non Aktif";
    }
    else if($aRow['status']=="1"){
      $sts = "Aktif";
    }
    else if($aRow['status']=="2"){
      $sts = "Blokir";
    }

    $edit_delete = $edit.$delete;
    $row = array();
    $row = array($aRow['username'],$aRow['nama_lengkap'],$aRow['group_name'],$sts,$aRow['nama_prov'],$aRow['nama_upt'],$aRow['no_tlp'],$aRow['no_hp'],$aRow['email'],$gen_controller->get_date_indonesia($aRow['created_date'])." ".substr($aRow['created_date'],11,8),$gen_controller->get_date_indonesia($aRow['last_update'])." ".substr($aRow['last_update'],11,8),"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_filter"){
  $param = $_REQUEST['param'];
  $aColumns = array('us.username','us.nama_lengkap','grp.group_name','us.status','pv.nama_prov','upt.nama_upt','us.no_tlp','us.no_hp','us.email','us.created_date','us.last_update','us.kode_user'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_user->getDataUserFilter($sWhere,$sOrder,$sLimit,$param);
  $rResultFilterTotal   = $md_user->getCountUserFilter($sWhere,$param);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_user'];
    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
       $edit = '<button  data-toggle="modal" data-target="#edit_modal" type="button" onclick="do_edit_user(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button>';
    }
    if($akses_tab['fua_delete']=="1"){ 
       $delete = '&nbsp; <button type="button" onclick="do_delete_user(\''.base64_encode('kode_user').'\',\''.base64_encode($param_id).'\')"class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
     }
     
    $sts = "";
    if($aRow['status']=="0"){
      $sts = "Non Aktif";
    }
    else if($aRow['status']=="1"){
      $sts = "Aktif";
    }
    else if($aRow['status']=="2"){
      $sts = "Blokir";
    }

    $edit_delete = $edit.$delete;
    $row = array();
    $row = array($aRow['username'],$aRow['nama_lengkap'],$aRow['group_name'],$sts,$aRow['nama_prov'],$aRow['nama_upt'],$aRow['no_tlp'],$aRow['no_hp'],$aRow['email'],$gen_controller->get_date_indonesia($aRow['created_date'])." ".substr($aRow['created_date'],11,8),$gen_controller->get_date_indonesia($aRow['last_update'])." ".substr($aRow['last_update'],11,8),"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}

else {
  $gen_controller->response_code(http_response_code());
}
?>