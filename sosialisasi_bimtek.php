<?php 
//General Controller
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();


//Model User
include "model/user.php";
$md_user      = new user();
session_start();

$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}

$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //Check Session
  $gen_controller->check_session();

  //View
  include "view/header.php";
  include "view/sosialisasi_bimtek_filter_import.php";
  include "view/sosialisasi_bimtek.php";
  include "view/footer.php";
}
else if($act=="form_all"){
   include "view/default_style.php"; 
   $activity  = $_REQUEST['activity'];
   $form_url  = $_REQUEST['form_url'];
   if($form_url=="Form_Bahan_Sosialisasi_ALL"){
      include "view/ajax_form/sosialisasi_bimtek_form_data_bhn_sosialisasi_all.php";
   }
   else if($form_url=="Form_Rencana_Sosialisasi_ALL"){
      include "view/ajax_form/sosialisasi_bimtek_form_data_rencana_sosialisasi_all.php";
   } 
   else if($form_url=="Form_Monev_Sosialisasi_ALL"){
      include "view/ajax_form/sosialisasi_bimtek_form_data_monev_sosialisasi_all.php";
   } 
   else if($form_url=="Form_Galeri_ALL"){
      include "view/ajax_form/sosialisasi_bimtek_form_data_galeri_all.php";
   }
}
else if($act=="form"){
   include "view/default_style.php"; 
   $form_url  = $_REQUEST['form_url'];
   $map_id    = $_REQUEST['map_id'];
   $upt       = $_REQUEST['upt'];
   $activity  = $_REQUEST['activity'];
   $wilayah   = $gen_model->GetOneRow("ms_provinsi",array('id_map'=>$map_id)); 
   if($form_url=="Form_Bahan_Sosialisasi"){
      include "view/ajax_form/sosialisasi_bimtek_form_data_bhn_sosialisasi.php";
   }
   else if($form_url=="Form_Rencana_Sosialisasi"){
      include "view/ajax_form/sosialisasi_bimtek_form_data_rencana_sosialisasi.php";
   } 
   else if($form_url=="Form_Monev_Sosialisasi"){
      include "view/ajax_form/sosialisasi_bimtek_form_data_monev_sosialisasi.php";
   } 
    else if($form_url=="Form_Galeri"){
      include "view/ajax_form/sosialisasi_bimtek_form_data_galeri.php";
   }  
}
else if($act=="table"){
   $upt     = $_REQUEST['upt'];
   $div_id  = $_REQUEST['div_id'];
   $map_id  = $_REQUEST['map_id'];
   $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_map'=>$map_id)); 
   if($div_id=="bahan_sosialisasi_table_data_utama"){
      include "view/ajax_table/sosialisasi_table_data_bahan_sosialisasi.php";
   }
   else if($div_id=="rencana_sosialisasi_table_data_utama"){
      include "view/ajax_table/sosialisasi_table_data_rencana_sosialisasi.php";
   }  
   else if($div_id=="monev_sosialisasi_table_data_utama"){
      include "view/ajax_table/sosialisasi_table_data_monev_sosialisasi.php";
   }  
   else if($div_id=="galeri_table_data_utama"){
      include "view/ajax_table/sosialisasi_table_data_galeri.php";
   }  
}
else if($act=="pilih_upt"){
   $map_id  = $_REQUEST['map_id'];
   $div_id  = $_REQUEST['div_id'];
   $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_map'=>$map_id));  
   $total_upt = $gen_model->GetOne(' count(*) as total ','ms_upt',array('id_prov'=>$wilayah['id_prov']));
   if($total_upt==0){
    echo '<label class="loc-lbl">UPT '.$wilayah['nama_prov'].'</label>';
    echo '<center>Tidak ada UPT di provinsi ini</center>';
   }
   else if($total_upt==1){
     echo "<script>";
     $upt = $gen_model->GetOne('kode_upt','ms_upt',array('id_prov'=>$wilayah['id_prov']));
      if($div_id=="bahan_sosialisasi_table_data_utama"){ 
          echo " detail_bhn_sosialisasi('".$map_id."','".$upt."') ";
       }
       else if($div_id=="rencana_sosialisasi_table_data_utama"){
         echo " detail_rencana_sosialisasi('".$map_id."','".$upt."') ";
       }  
       else if($div_id=="monev_sosialisasi_table_data_utama"){
         echo " detail_monev_sosialisasi('".$map_id."','".$upt."') ";
       }  
       else if($div_id=="galeri_table_data_utama"){
         echo " detail_galeri('".$map_id."','".$upt."') ";
       }  
       echo "</script>";
   }  
   else { ?>
   <div style="background-color:white">
   <label class="loc-lbl">UPT <?php echo $wilayah['nama_prov'] ?></label>
     <div class="row">
        <div class="col-md-12">
           <select class="form_control" onchange="get_detail()" id="data_upt" style="width: 100%;">
              <?php 
                 $data_upt = $gen_model->GetWhere('ms_upt',array('id_prov'=>$wilayah['id_prov']));
                 echo '<option value="">Pilih UPT</option>';
                  while($list = $data_upt->FetchRow()){
                    foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
                              }  
                ?>
              <option value="<?php echo $kode_upt ?>"><?php echo $nama_upt ?></option>
              <?php }  ?>
           </select>
        </div>
     </div>
   </div>
   <script type="text/javascript">
     function get_detail(){
        var  map_id = "<?php echo $map_id ?>";
        var  upt    = $("#data_upt").val();
        if(upt){
            <?php 
                if($div_id=="bahan_sosialisasi_table_data_utama"){
                    echo " detail_bhn_sosialisasi(map_id,upt) ";
                 }
                 else if($div_id=="rencana_sosialisasi_table_data_utama"){
                   echo " detail_rencana_sosialisasi(map_id,upt) ";
                 }   
                 else if($div_id=="monev_sosialisasi_table_data_utama"){
                   echo " detail_monev_sosialisasi(map_id,upt) ";
                 }  
                 else if($div_id=="galeri_table_data_utama"){
                   echo " detail_galeri(map_id,upt) ";
                 }  
            ?>
        }
     }
   </script>
   <?php
   }
}
else if($act=="get_group_perizinan"){
    echo "<option value=''>Pilih Group Perizinan</option>";
    $get_grp  = $gen_model->GetWhere("ms_group_layanan",array('jenis'=>$_POST['jenis'])); 
    while($list = $get_grp->FetchRow()){
      foreach($list as $key=>$val){
                        $key=strtolower($key);
                        $$key=$val;
      }  
      echo "<option value='".$id_group_layanan."'>".$group_layanan."</option>";
    }
}


else {
  $gen_controller->response_code(http_response_code());
}
?>