<?php 
//General Controller
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();

//Model Hak Akses
include "model/hak_akses.php";
$md_hak      = new hak_akses();

//Model User
include "model/user.php";
$md_user      = new user();
session_start();

//Check Access Tab
$akses_tab = $gen_model->GetOneRow("tr_function_access_tab",array('id_tab'=>14,'kode_function_access'=>$_SESSION['group_user'])); 
if(empty($akses_tab['fua_edit'])){
  $akses_tab['fua_edit'] = 0;
}
if(empty($akses_tab['fua_delete'])){
  $akses_tab['fua_delete'] = 0;
}


$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}

$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //Check Session
  $gen_controller->check_session();
  //View
  $gen_controller->response_code('404');
}
else if($act=="form"){
   include "view/default_style.php"; 
   $form_url  = $_REQUEST['form_url'];
   $activity  = $_REQUEST['activity'];
   if($form_url=="Form_Hak_Akses"){
      include "view/ajax_form/hak_akses_form_data.php";
   }
}
else if($act=="edit" and $id_parameter!=""){
  //Check Session
  $gen_controller->check_session();
  //View
  include "view/header.php";
  include "view/hak_akses_edit.php";
  include "view/footer.php";
}
else if($act=="add"){
    if(!empty($_SESSION['kode_user'])){
      //Proses
      $insert_data = array();
      $insert_data['kode_group']          = "grp_".date("ymdhis")."_".rand(1000,9999);
      $insert_data['group_name']          = get($_POST['hak_akses']);
      $insert_data['created_date']        = $date_now_indo_full;
      $insert_data['last_update']         = $date_now_indo_full;
      $insert_data['created_by']          = $_SESSION['kode_user'];
      $insert_data['last_update_by']      = $_SESSION['kode_user'];
 
       //Validation
      if($insert_data['group_name']!=""){
           $count_grp = $db->GetOne("select count(*) as total from ms_group where group_name='".$_POST['hak_akses']."'"); 
            if($count_grp>0){
               echo 'Hak Akses sudah terdaftar di database';
            }
            else {
               echo $gen_model->Insert('ms_group',$insert_data); 
            }
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="delete"){
    if(!empty($_SESSION['kode_user'])){
 
       //Paramater
      $where_data = array();
      $where_data['kode_group']       = $_POST['kode_group'];

       //Validation
      if(!empty($_POST['kode_group'])){
            echo $gen_model->Delete('ms_group',$where_data);

            //Remove Function Acces and Function Acces Tab
            $where_data2 = array();
            $where_data2['kode_function_access']    = $_POST['kode_group'];
            $gen_model->Delete('tr_function_access',$where_data2);
            $gen_model->Delete('tr_function_access_tab',$where_data2);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="do_update"){
  //Check Session
  $gen_controller->check_session();
  $hak_akses    = $_POST['hak_akses_text'];
  $kode_fua_name = $gen_controller->decrypt($_POST['kode_function_access']);
  //Paramater
    $where_data = array();
    $where_data['kode_function_access']    = $kode_fua_name;
    $gen_model->Delete('tr_function_access',$where_data);

    foreach($_POST as $key=>$val){
        if(preg_match("/read/",$key)){
          $$key = $val;
          foreach($$key as $key1 => $val1){

            $whr = array();
            $whr['kode_function_access'] = $kode_fua_name;
            $whr['id_menu'] = $val1;
            $men_nomorurutexist = $gen_model->GetOneRow("tr_function_access",$whr); 
            if(!empty($men_nomorurutexist['id_menu'])){
              $sql="update tr_function_access set function_access='".$hak_akses."',fua_$key='1' where id_menu='".$val1."' and kode_function_access='".$kode_fua_name."'";
              $db->Execute($sql);
            }else{
              $sql="insert into tr_function_access (fua_$key,id_menu,function_access,kode_function_access) values ('1','".$val1."','".$hak_akses."','".$kode_fua_name."') ";
              $db->Execute($sql);
            }
              
          }
        }
    }
    echo "OK";
}
else if($act=="update" and $id_parameter!=""){
  error_reporting(null);
  //Check Session
  $gen_controller->check_session();
  $fua_name =  $gen_controller->decrypt($id_parameter);
  $id       = $_REQUEST['id'];
  //@ = isset
  if($id!=""){
    $sql="select * from ms_menu where reference='".$id."' and men_id!='1' order by urutan asc";
  }
  else {
    $sql="select * from ms_menu where menu_level='1' and men_id!='1' order by urutan asc";
  }
  
        $sql2=" select * from tr_function_access where kode_function_access='".$fua_name."' ";
        $result2= $db->Execute($sql2);
        while($row2=$result2->FetchRow()){
          foreach($row2 as $key2 => $val){
            $key2=strtolower($key2);
            $$key2=$val;
          }
        }
    

  
        $result= $db->Execute($sql);
        $menu = "";
        while($row=$result->FetchRow()){
          { 
            $men_judul    = $row['menu'];
            $men_id       = $row['men_id']; 

            $state = "";  
            $hitung = $gen_model->GetOne("count(*)","ms_menu",array('reference'=>$men_id));
            $check_data = $hitung;

            
            if($check_data>0){
              $state = "closed";
            }
            else {
              $state = "open";
            }
            
            $read_check  ="";
            // $add_check   ="";
            // $edit_check  ="";
            // $delete_check ="";

              $whr = array();
              $whr['kode_function_access'] = $fua_name;
              $whr['id_menu'] = $men_id;
              $row_check = $gen_model->GetOneRow("tr_function_access",$whr); 

              $read_check   = ($row_check['fua_read']=="1" ? 'checked' : '');
            
            $menu .= '{"id":"'.$men_id.'","title":"'.$men_judul.'","state":"'.$state.'","read":"<center><input id=\"read_'.$men_id.'\" '.$read_check.' type=\"checkbox\" name=\"read[]\" value=\"'.$men_id.'\"</center>","show":"<input type=\"hidden\" name=show[] value=\"'.$men_id.'\""},';
          }
        }
        $data = trim($menu, ",");
    echo "[".$data."]";
  
}
else if($act=="rest"){
  $aColumns = array('grp.group_name','grp.created_date','grp.last_update','grp.kode_group'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_hak->getHakAkses($sWhere,$sOrder,$sLimit);
  $rResultFilterTotal   = $md_hak->getCountHakAkses($sWhere);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_group'];

    $edit="";
    $edit2="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
      $edit = '<a href="'.$basepath.'hak_akses/edit/'.$gen_controller->encrypt($param_id).'"><button  type="button"  class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Akses Menu</button></a>';
      $edit2 = '&nbsp; <a href="'.$basepath.'akses_tab/edit/'.$gen_controller->encrypt($param_id).'"><button  type="button"  class="btn btn-warning btn-sm"><i class="fa fa-edit"></i> Akses Tab</button></a>';
    } 
    if($akses_tab['fua_delete']=="1"){ 
      if($aRow['kode_group']=="grp_171026194411_1142" OR $aRow['kode_group']=="grp_171026194411_1143"){

      }
      else {
        $delete = '&nbsp; <button onclick="do_delete_akses(\''.base64_encode('kode_group').'\',\''.base64_encode($param_id).'\')" type="button"  class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus</button>';
      }
    }

   


    $edit_delete = $edit.$edit2.$delete;
    $row = array();
    $row = array($aRow['group_name'],"<center>".$gen_controller->get_date_indonesia($aRow['created_date'])."</center>","<center>".$gen_controller->get_date_indonesia($aRow['last_update'])."</center>","<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}

else {
  $gen_controller->response_code(http_response_code());
}
?>