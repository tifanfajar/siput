<?php 
class upt { 

	function getUPT($where,$order,$limit){
		  global $db;
		  $sql = "SELECT  upt.*,pv.nama_prov
					FROM ms_upt as upt
					left outer join ms_provinsi as pv  
						on pv.id_prov=upt.id_prov
					".$where."  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountUPT($where){
		  global $db;
		  $sql = "SELECT  upt.*,pv.nama_prov
					FROM ms_upt as upt
					left outer join ms_provinsi as pv  
						on pv.id_prov=upt.id_prov
					".$where;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getUPTFilter($where,$order,$limit,$param){
		  global $db;
		  $sql = "SELECT  upt.*,pv.nama_prov
					FROM ms_upt as upt
					left outer join ms_provinsi as pv  
						on pv.id_prov=upt.id_prov
					".$where." ".base64_decode($param)." ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountUPTFilter($where,$param){
		  global $db;
		  $sql = "SELECT  upt.*,pv.nama_prov
					FROM ms_upt as upt
					left outer join ms_provinsi as pv  
						on pv.id_prov=upt.id_prov
					".$where." ".base64_decode($param)."  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
}
?>