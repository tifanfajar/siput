<?php 
class kabkot { 

	function getKabkot($where,$order,$limit){
		  global $db;
		  $sql = "SELECT  pv.*,kb.*
					FROM ms_provinsi as pv
					inner join ms_kabupaten_kota as kb 
						on kb.id_provinsi=pv.id_prov
					".$where."  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountKabkot($where){
		  global $db;
		  $sql = "SELECT  pv.id_prov
					FROM ms_provinsi as pv
					inner join ms_kabupaten_kota as kb 
						on kb.id_provinsi=pv.id_prov
					".$where;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
}
?>