<?php 
class user { 
	function login($username,$pwd){
		  global $db;
    // echo $pwd;
    // die;
		  $sql = "SELECT * FROM ms_user WHERE
						(
							username = '".$username."'
						)
					AND password = '".$pwd."' and status='1' ";
	      $result=$db->getRow($sql);
			if (!$result){
				return $db->ErrorMsg();
			}
			else {
				return $result;
			}
	}
	function getDetailDataUser($col_param='',$id=''){
		  global $db;

		  $where ="";
		  if(!empty($id)){
		  	 $where =" where us.".$col_param."='".$id."' ";
		  }

		  $sql = "SELECT us.*,grp.group_name,grp.group_name as level
		  		FROM ms_user as us 
		  		left outer join ms_group as grp
						on grp.kode_group=us.group_user
		  		".$where;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getDataUser($where,$order,$limit){
		  global $db;
		  $sql = "SELECT  us.*,grp.group_name,upt.nama_upt,pv.nama_prov
					FROM ms_user as us
					left outer join ms_group as grp
						on grp.kode_group=us.group_user
					left outer join ms_provinsi as pv
						on pv.id_prov=us.upt_provinsi
					left outer join ms_upt as upt
						on upt.kode_upt=us.kode_upt
					".$where." and us.is_deleted='0'  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountUser($where){
		  global $db;
		  $sql = "SELECT  us.kode_user
					FROM ms_user as us
					left outer join ms_group as grp
						on grp.kode_group=us.group_user
					left outer join ms_provinsi as pv
						on pv.id_prov=us.upt_provinsi
					left outer join ms_upt as upt
						on upt.kode_upt=us.kode_upt
					".$where." and us.is_deleted='0'  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getDataUserFilter($where,$order,$limit,$param){
		  global $db;
		  $sql = "SELECT  us.*,grp.group_name,upt.nama_upt,pv.nama_prov
					FROM ms_user as us
					left outer join ms_group as grp
						on grp.kode_group=us.group_user
					left outer join ms_provinsi as pv
						on pv.id_prov=us.upt_provinsi
					left outer join ms_upt as upt
						on upt.kode_upt=us.kode_upt
					".$where." ".base64_decode($param)."  and us.is_deleted='0'  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountUserFilter($where,$param){
		  global $db;
		  $sql = "SELECT  us.kode_user
					FROM ms_user as us
					left outer join ms_group as grp
						on grp.kode_group=us.group_user
					left outer join ms_provinsi as pv
						on pv.id_prov=us.upt_provinsi
					left outer join ms_upt as upt
						on upt.kode_upt=us.kode_upt
					".$where." ".base64_decode($param)."  and us.is_deleted='0'  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
}
?>