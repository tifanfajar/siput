<?php 
class sims { 
	//Start Sims
	function getSims($where,$order,$limit){
		  global $db;
		  $sql = "SELECT  sm.*,cp.company_name
					FROM tr_data_sims as sm
					INNER JOIN ms_company AS cp 
						on cp.company_id = sm.no_klien_licence
					".$where.$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountSims($where){
		  global $db;
		  $sql = "SELECT  sm.no_spp
					FROM tr_data_sims as sm
					INNER JOIN ms_company AS cp 
						on cp.company_id = sm.no_klien_licence
					".$where;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getSimsFilter($where,$order,$limit,$param=''){
		  global $db;
		  $sql = "SELECT  sm.*,cp.company_name
					FROM tr_data_sims as sm
					INNER JOIN ms_company AS cp 
						on cp.company_id = sm.no_klien_licence
					".$where."  ".base64_decode($param)." ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountSimsFilter($where,$param=''){
		  global $db;
		  $sql = "SELECT  sm.no_spp
					FROM tr_data_sims as sm
					INNER JOIN ms_company AS cp 
						on cp.company_id = sm.no_klien_licence
					".$where." ".base64_decode($param);
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	//End Sims
}
?>