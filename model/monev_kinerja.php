<?php 
class monev_kinerja { 
	//Start Monev Kinerja
	function getMonevKinerja($where,$order,$limit,$prov='',$upt=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		$query_add=" and (mnv.id_prov='".$prov."' or mnv.id_prov is null) ";
		  }	 
		  if(!empty($upt)){
		  		$query_add=" and (mnv.kode_upt='".$upt."' or mnv.kode_upt is null) ";
		  }	
		  $sql = "SELECT  mnv.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_monev_kinerja as mnv
					left outer join ms_user as us_crt
						on us_crt.kode_user=mnv.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=mnv.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=mnv.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=mnv.kode_upt
					".$where." ".$query_add." and mnv.status='3'  ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountMonevKinerja($where,$prov='',$upt=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		 $query_add=" and mnv.id_prov='".$prov."' ";
		  }	 
		  if(!empty($upt)){
		  		 $query_add=" and mnv.kode_upt='".$upt."' ";
		  }	
		  $sql = "SELECT mnv.kode_monev_kinerja
					FROM tr_monev_kinerja as mnv
					left outer join ms_user as us_crt
						on us_crt.kode_user=mnv.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=mnv.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=mnv.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=mnv.kode_upt
					".$where."  ".$query_add." and mnv.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getMonevKinerjaWaiting($where,$order,$limit,$prov='',$upt=''){
		  global $db;
		  $sql = "SELECT  mnv.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_monev_kinerja as mnv
					left outer join ms_user as us_crt
						on us_crt.kode_user=mnv.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=mnv.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=mnv.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=mnv.kode_upt
					".$where." and mnv.id_prov='".$prov."' and mnv.kode_upt='".$upt."' and mnv.status in ('0','1','4')  ".$order.$limit;
					//echo $sql;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountMonevKinerjaWaiting($where,$prov='',$upt=''){
		  global $db;
		  $sql = "SELECT mnv.kode_monev_kinerja
					FROM tr_monev_kinerja as mnv
					left outer join ms_user as us_crt
						on us_crt.kode_user=mnv.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=mnv.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=mnv.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=mnv.kode_upt
					".$where." and mnv.id_prov='".$prov."'  and mnv.kode_upt='".$upt."' and mnv.status in ('0','1','4') ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getMonevKinerjaFilter($where,$order,$limit,$param){
		  global $db;
		  $sql = "SELECT  mnv.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_monev_kinerja as mnv
					left outer join ms_user as us_crt
						on us_crt.kode_user=mnv.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=mnv.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=mnv.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=mnv.kode_upt
					".$where." ".base64_decode($param)."  and mnv.status='3'  ".$order.$limit;
				
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountMonevKinerjaFilter($where,$param){
		  global $db;
		  $sql = "SELECT mnv.kode_monev_kinerja
					FROM tr_monev_kinerja as mnv
					left outer join ms_user as us_crt
						on us_crt.kode_user=mnv.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=mnv.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=mnv.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=mnv.kode_upt
					".$where." ".base64_decode($param)."   and mnv.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	//End Monev Kinerja
}
?>