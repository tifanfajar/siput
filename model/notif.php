<?php 
class notif { 
	function getNotif($where,$order,$limit,$prov='',$upt='',$jenis='',$grp=''){
		  global $db;
		  $query_prov="";
		  $query_upt="";
		  if(!empty($prov)){
		  		 $query_prov=" and ntf.id_prov='".$prov."' ";
		  }	
		  if(!empty($upt)){
		  		 $query_upt=" and ntf.kode_upt='".$upt."' ";
		  }	
		  $sql = "SELECT  ntf.* FROM tr_notif as ntf
					".$where." ".$query_prov." ".$query_upt."  and ntf.jenis='".$jenis."' and ntf.to_group_user='".$grp."' order by created_date desc ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountNotif($where,$prov='',$upt='',$jenis='',$grp=''){
		  global $db;
		  $query_prov="";
		  $query_upt="";
		  if(!empty($prov)){
		  		 $query_prov=" and ntf.id_prov='".$prov."' ";
		  }	
		  if(!empty($upt)){
		  		 $query_upt=" and ntf.kode_upt='".$upt."' ";
		  }	
		  $sql = "SELECT  ntf.kode_notif
					FROM tr_notif as ntf
					".$where." ".$query_prov." ".$query_upt."  and ntf.jenis='".$jenis."' and ntf.to_group_user='".$grp."' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getLoketPelayananNotif($where,$order,$limit,$data_notif=''){
		  global $db;
		  $sql = "SELECT  pl.*,cp.company_name,cp.alamat,us_crt.username as pembuat ,us_updt.username as perubah,pv.nama_prov,upt.nama_upt
					FROM tr_pelayanan as pl
					left outer join ms_company as cp
						on cp.company_id=pl.kode_perusahaan
					left outer join ms_user as us_crt
						on us_crt.kode_user=pl.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=pl.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=pl.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=pl.kode_upt
					left outer join ms_group_layanan as  grp
						on grp.id_jenis=pl.jenis_perizinan
					left outer join ms_sub_group_layanan as  sb_grp
						on sb_grp.id_sub_group_layanan=pl.sub_group_perizinan
					".$where." and pl.kode_pelayanan  in (".base64_decode($data_notif).")   and pl.status not in ('2')  ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountLoketPelayananNotif($where,$data_notif=''){
		  global $db;
		  $sql = "SELECT  pl.kode_pelayanan
					FROM tr_pelayanan as pl
					left outer join ms_company as cp
						on cp.company_id=pl.kode_perusahaan
					left outer join ms_user as us_crt
						on us_crt.kode_user=pl.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=pl.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=pl.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=pl.kode_upt
					left outer join ms_group_layanan as  grp
						on grp.id_jenis=pl.jenis_perizinan
					left outer join ms_sub_group_layanan as  sb_grp
						on sb_grp.id_sub_group_layanan=pl.sub_group_perizinan
					".$where." and pl.kode_pelayanan  in (".base64_decode($data_notif).")   and pl.status not in ('2')  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getSPPNotif($where,$order,$limit,$data_notif=''){
		  global $db;
		  $sql = "SELECT  spp.*,spp.created_date as tgl_buat,spp.status as status_verif,pv.nama_prov,us_crt.username as pembuat ,us_updt.username as perubah,cp.company_name,upt.nama_upt,mtd.metode as nama_metode,pv.nama_prov,upt.nama_upt
					FROM tr_spp as spp
					left outer join ms_company AS cp 
						on cp.company_id = spp.no_klien_licence
					left outer join ms_user as us_crt
						on us_crt.kode_user=spp.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=spp.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=spp.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=spp.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=spp.metode	
					".$where." and spp.kode_spp  in (".base64_decode($data_notif).")   and spp.status not in ('2')  ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountSPPNotif($where,$data_notif=''){
		  global $db;
		  $sql = "SELECT  spp.kode_spp
					FROM tr_spp as spp
					left outer join ms_company AS cp 
						on cp.company_id = spp.no_klien_licence
					left outer join ms_user as us_crt
						on us_crt.kode_user=spp.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=spp.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=spp.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=spp.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=spp.metode	
					".$where." and spp.kode_spp  in (".base64_decode($data_notif).")   and spp.status not in ('2')  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getISRNotif($where,$order,$limit,$data_notif=''){
		  global $db;
		  $sql = "SELECT  isr.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah,cp.company_name,mtd.metode as nama_metode
					FROM tr_isr as isr
					left outer join ms_company AS cp 
						on cp.company_id = isr.no_client
					left outer join ms_user as us_crt
						on us_crt.kode_user=isr.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=isr.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=isr.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=isr.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=isr.metode	
					".$where." and isr.kode_isr  in (".base64_decode($data_notif).")   and isr.status not in ('2')  ".$order.$limit;
					
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountISRNotif($where,$data_notif=''){
		  global $db;
		  $sql = "SELECT  isr.kode_isr
					FROM tr_isr as isr
					left outer join ms_company AS cp 
						on cp.company_id = isr.no_client
					left outer join ms_user as us_crt
						on us_crt.kode_user=isr.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=isr.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=isr.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=isr.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=isr.metode	
					".$where." and isr.kode_isr  in (".base64_decode($data_notif).")   and isr.status not in ('2')  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getRevokeNotif($where,$order,$limit,$data_notif=''){
		  global $db;
		  $sql = "SELECT  rvk.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah,cp.company_name,mtd.metode as nama_metode 
					FROM tr_revoke as rvk
					left outer join ms_company AS cp 
						on cp.company_id = rvk.no_client
					left outer join ms_user as us_crt
						on us_crt.kode_user=rvk.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=rvk.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=rvk.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=rvk.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=rvk.metode		
					".$where." and rvk.kode_revoke  in (".base64_decode($data_notif).")   and rvk.status not in ('2')  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountRevokeNotif($where,$data_notif=''){
		  global $db;
		  $sql = "SELECT  rvk.kode_revoke
					FROM tr_revoke as rvk
					left outer join ms_company AS cp 
						on cp.company_id = rvk.no_client
					left outer join ms_user as us_crt
						on us_crt.kode_user=rvk.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=rvk.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=rvk.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=rvk.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=rvk.metode	
					".$where." and rvk.kode_revoke  in (".base64_decode($data_notif).")   and rvk.status not in ('2')  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getPiutangNotif($where,$order,$limit,$data_notif=''){
		  global $db;
		  $sql = "SELECT  pt.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah,cp.company_name
					FROM tr_penanganan_piutang as pt
					left outer join ms_company AS cp 
						on cp.company_id = pt.no_client
					left outer join ms_user as us_crt
						on us_crt.kode_user=pt.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=pt.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=pt.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=pt.kode_upt
					".$where." and pt.kode_piutang  in (".base64_decode($data_notif).")   and pt.status not in ('2')  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountPiutangNotif($where,$data_notif=''){
		  global $db;
		  $sql = "SELECT  pt.kode_piutang
					FROM tr_penanganan_piutang as pt
					left outer join ms_company AS cp 
						on cp.company_id = pt.no_client
					left outer join ms_user as us_crt
						on us_crt.kode_user=pt.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=pt.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=pt.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=pt.kode_upt
					".$where." and pt.kode_piutang  in (".base64_decode($data_notif).")   and pt.status not in ('2')  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getUnarNotif($where,$order,$limit,$data_notif=''){
		  global $db;
		  $sql = "SELECT  un.*,kb.nama_kab_kot,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_unar as un
					left outer join ms_user as us_crt
						on us_crt.kode_user=un.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=un.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=un.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=un.kode_upt	
					left outer join ms_kabupaten_kota as kb
						on kb.id_kabkot=un.id_kabkot
					".$where." and un.kode_unar  in (".base64_decode($data_notif).")   and un.status not in ('2')  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountUnarNotif($where,$data_notif=''){
		  global $db;
		  $sql = "SELECT  un.kode_unar
					FROM tr_unar as un
					left outer join ms_user as us_crt
						on us_crt.kode_user=un.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=un.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=un.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=un.kode_upt	
					left outer join ms_kabupaten_kota as kb
						on kb.id_kabkot=un.id_kabkot
					".$where." and un.kode_unar  in (".base64_decode($data_notif).")   and un.status not in ('2')  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getBahanSosialisasiNotif($where,$order,$limit,$data_notif=''){
		  global $db;
		  $sql = "SELECT  mtr.jenis_materi,bhn.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_bahan_sosialisasi as bhn
					left outer join ms_user as us_crt
						on us_crt.kode_user=bhn.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=bhn.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=bhn.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=bhn.kode_upt
					left outer join ms_materi as mtr
						on mtr.kode_materi=bhn.materi
					".$where." and bhn.kode_bhn_sosialisasi  in (".base64_decode($data_notif).")   and bhn.status not in ('2')  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountBahanSosialisasiNotif($where,$data_notif=''){
		  global $db;
		  $sql = "SELECT bhn.kode_bhn_sosialisasi
					FROM tr_bahan_sosialisasi as bhn
					left outer join ms_user as us_crt
						on us_crt.kode_user=bhn.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=bhn.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=bhn.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=bhn.kode_upt
					left outer join ms_materi as mtr
						on mtr.kode_materi=bhn.materi
					".$where." and bhn.kode_bhn_sosialisasi  in (".base64_decode($data_notif).")   and bhn.status not in ('2')  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getRencanaSosialisasiNotif($where,$order,$limit,$data_notif=''){
		  global $db;
		  $sql = "SELECT  kgt.kegiatan,rcn.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_rencana_sosialisasi as rcn
					left outer join ms_user as us_crt
						on us_crt.kode_user=rcn.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=rcn.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=rcn.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=rcn.kode_upt
					left outer join ms_kegiatan as kgt
						on kgt.kode_kegiatan=rcn.jenis_kegiatan
					".$where." and rcn.kode_rencana_sosialisasi  in (".base64_decode($data_notif).")   and rcn.status not in ('2')  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountRencanaSosialisasiNotif($where,$data_notif=''){
		  global $db;
		  $sql = "SELECT rcn.kode_rencana_sosialisasi
					FROM tr_rencana_sosialisasi as rcn
					left outer join ms_user as us_crt
						on us_crt.kode_user=rcn.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=rcn.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=rcn.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=rcn.kode_upt
					left outer join ms_kegiatan as kgt
						on kgt.kode_kegiatan=rcn.jenis_kegiatan
					".$where." and rcn.kode_rencana_sosialisasi  in (".base64_decode($data_notif).")   and rcn.status not in ('2')  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getMonevSosialisasiNotif($where,$order,$limit,$data_notif=''){
		  global $db;
		  $sql = "SELECT  kgt.kegiatan,mnv.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_monev_sosialisasi as mnv
					left outer join ms_user as us_crt
						on us_crt.kode_user=mnv.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=mnv.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=mnv.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=mnv.kode_upt
					left outer join ms_kegiatan as kgt
						on kgt.kode_kegiatan=mnv.jenis_kegiatan
					".$where." and mnv.kode_monev_sosialisasi  in (".base64_decode($data_notif).")   and mnv.status not in ('2')  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountMonevSosialisasiNotif($where,$data_notif=''){
		  global $db;
		  $sql = "SELECT mnv.kode_monev_sosialisasi
					FROM tr_monev_sosialisasi as mnv
					left outer join ms_user as us_crt
						on us_crt.kode_user=mnv.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=mnv.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=mnv.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=mnv.kode_upt
					left outer join ms_kegiatan as kgt
						on kgt.kode_kegiatan=mnv.jenis_kegiatan
					".$where." and mnv.kode_monev_sosialisasi  in (".base64_decode($data_notif).")   and mnv.status not in ('2')  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getGaleriNotif($where,$order,$limit,$data_notif=''){
		  global $db;
		  $sql = "SELECT  upt.nama_upt,pv.nama_prov,gl.*,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_galeri as gl
					left outer join ms_user as us_crt
						on us_crt.kode_user=gl.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=gl.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=gl.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=gl.kode_upt
					".$where." and gl.kode_galeri  in (".base64_decode($data_notif).")   and gl.status not in ('2')  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountGaleriNotif($where,$data_notif=''){
		  global $db;
		  $sql = "SELECT  gl.kode_galeri
					FROM tr_galeri as gl
					left outer join ms_user as us_crt
						on us_crt.kode_user=gl.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=gl.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=gl.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=gl.kode_upt
					".$where." and gl.kode_galeri  in (".base64_decode($data_notif).")   and gl.status not in ('2')  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getValidasiDataNotif($where,$order,$limit,$data_notif=''){
		  global $db;
		  $sql = "SELECT  vld.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_validasi_data as vld
					left outer join ms_user as us_crt
						on us_crt.kode_user=vld.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=vld.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=vld.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=vld.kode_upt
					".$where." and vld.kode_validasi_data  in (".base64_decode($data_notif).")   and vld.status not in ('2')  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountValidasiDataNotif($where,$data_notif=''){
		  global $db;
		  $sql = "SELECT vld.kode_validasi_data
					FROM tr_validasi_data as vld
					left outer join ms_user as us_crt
						on us_crt.kode_user=vld.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=vld.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=vld.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=vld.kode_upt
					".$where." and vld.kode_validasi_data  in (".base64_decode($data_notif).")   and vld.status not in ('2')  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getMonevKinerjaNotif($where,$order,$limit,$data_notif=''){
		  global $db;
		  $sql = "SELECT  mnv.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_monev_kinerja as mnv
					left outer join ms_user as us_crt
						on us_crt.kode_user=mnv.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=mnv.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=mnv.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=mnv.kode_upt
					".$where." and mnv.kode_monev_kinerja  in (".base64_decode($data_notif).")   and mnv.status not in ('2')  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountMonevKinerjaNotif($where,$data_notif=''){
		  global $db;
		  $sql = "SELECT mnv.kode_monev_kinerja
					FROM tr_monev_kinerja as mnv
					left outer join ms_user as us_crt
						on us_crt.kode_user=mnv.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=mnv.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=mnv.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=mnv.kode_upt
					".$where." and mnv.kode_monev_kinerja  in (".base64_decode($data_notif).")   and mnv.status not in ('2')  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}

}
?>