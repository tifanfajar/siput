<?php 
class sosialisasi_bimtek { 
	//Start Bahan Sosialisasi
	function getBahanSosialisasi($where,$order,$limit,$prov='',$upt=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		// $query_add=" and (bhn.id_prov='".$prov."' or bhn.id_prov is null)";
		  }	 
		  if(!empty($upt)){
		  		// $query_add=" and (bhn.kode_upt='".$upt."' or bhn.kode_upt is null)";
		  }	
		  $sql = "SELECT  pv.nama_prov,upt.nama_upt,mtr.jenis_materi,bhn.*,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_bahan_sosialisasi as bhn
					left outer join ms_user as us_crt
						on us_crt.kode_user=bhn.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=bhn.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=bhn.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=bhn.kode_upt
					left outer join ms_materi as mtr
						on mtr.kode_materi=bhn.materi
					".$where." ".$query_add." and bhn.status='3'  ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountBahanSosialisasi($where,$prov='',$upt=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		// $query_add=" and (bhn.id_prov='".$prov."' or bhn.id_prov is null)";
		  }	 
		  if(!empty($upt)){
		  		 //$query_add=" and (bhn.kode_upt='".$upt."' or bhn.kode_upt is null)";
		  }	
		  $sql = "SELECT bhn.judul
					FROM tr_bahan_sosialisasi as bhn
					left outer join ms_user as us_crt
						on us_crt.kode_user=bhn.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=bhn.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=bhn.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=bhn.kode_upt
					left outer join ms_materi as mtr
						on mtr.kode_materi=bhn.materi
					".$where."  ".$query_add." and bhn.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getBahanSosialisasiWaiting($where,$order,$limit,$prov='',$upt=''){
		  global $db;
		  $sql = "SELECT  mtr.jenis_materi,bhn.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_bahan_sosialisasi as bhn
					left outer join ms_user as us_crt
						on us_crt.kode_user=bhn.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=bhn.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=bhn.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=bhn.kode_upt
					left outer join ms_materi as mtr
						on mtr.kode_materi=bhn.materi
					".$where." and bhn.id_prov='".$prov."' and bhn.kode_upt='".$upt."' and bhn.status in ('0','1','4')  ".$order.$limit;
					//echo $sql;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountBahanSosialisasiWaiting($where,$prov='',$upt=''){
		  global $db;
		  $sql = "SELECT  bhn.kode_bhn_sosialisasi
					FROM tr_bahan_sosialisasi as bhn
					left outer join ms_user as us_crt
						on us_crt.kode_user=bhn.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=bhn.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=bhn.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=bhn.kode_upt
					left outer join ms_materi as mtr
						on mtr.kode_materi=bhn.materi
					".$where." and bhn.id_prov='".$prov."'  and bhn.kode_upt='".$upt."' and bhn.status in ('0','1','4') ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getBahanSosialisasiFilter($where,$order,$limit,$param){
		  global $db;
		  $sql = "SELECT  mtr.jenis_materi,bhn.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_bahan_sosialisasi as bhn
					left outer join ms_user as us_crt
						on us_crt.kode_user=bhn.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=bhn.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=bhn.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=bhn.kode_upt
					left outer join ms_materi as mtr
						on mtr.kode_materi=bhn.materi	
					".$where." ".base64_decode($param)."  and bhn.status='3'  ".$order.$limit;
				
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountBahanSosialisasiFilter($where,$param){
		  global $db;
		  $sql = "SELECT  bhn.kode_bhn_sosialisasi
					FROM tr_bahan_sosialisasi as bhn
					left outer join ms_user as us_crt
						on us_crt.kode_user=bhn.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=bhn.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=bhn.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=bhn.kode_upt
					left outer join ms_materi as mtr
						on mtr.kode_materi=bhn.materi	
					".$where." ".base64_decode($param)."   and bhn.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	//End Bahan Sosialisasi

	//Start Rencana Sosialisasi
	function getRencanaSosialisasi($where,$order,$limit,$prov='',$upt=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		// $query_add=" and rcn.id_prov='".$prov."' ";
		  }	 
		  if(!empty($upt)){
		  		 //$query_add=" and rcn.kode_upt='".$upt."' ";
		  }	
		  $sql = "SELECT  kgt.kegiatan,rcn.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_rencana_sosialisasi as rcn
					left outer join ms_user as us_crt
						on us_crt.kode_user=rcn.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=rcn.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=rcn.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=rcn.kode_upt
					left outer join ms_kegiatan as kgt
						on kgt.kode_kegiatan=rcn.jenis_kegiatan
					".$where." ".$query_add." and rcn.status='3'  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountRencanaSosialisasi($where,$prov='',$upt=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		// $query_add=" and rcn.id_prov='".$prov."' ";
		  }	 
		  if(!empty($upt)){
		  		// $query_add=" and rcn.kode_upt='".$upt."' ";
		  }	
		  $sql = "SELECT rcn.kode_rencana_sosialisasi
					FROM tr_rencana_sosialisasi as rcn
					left outer join ms_user as us_crt
						on us_crt.kode_user=rcn.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=rcn.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=rcn.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=rcn.kode_upt
					left outer join ms_kegiatan as kgt
						on kgt.kode_kegiatan=rcn.jenis_kegiatan
					".$where."  ".$query_add." and rcn.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getRencanaSosialisasiWaiting($where,$order,$limit,$prov='',$upt=''){
		  global $db;
		  $sql = "SELECT  kgt.kegiatan,rcn.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_rencana_sosialisasi as rcn
					left outer join ms_user as us_crt
						on us_crt.kode_user=rcn.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=rcn.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=rcn.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=rcn.kode_upt
					left outer join ms_kegiatan as kgt
						on kgt.kode_kegiatan=rcn.jenis_kegiatan
					".$where." and rcn.id_prov='".$prov."' and rcn.kode_upt='".$upt."' and rcn.status in ('0','1','4')  ".$order.$limit;
					//echo $sql;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountRencanaSosialisasiWaiting($where,$prov='',$upt=''){
		  global $db;
		  $sql = "SELECT rcn.kode_rencana_sosialisasi
					FROM tr_rencana_sosialisasi as rcn
					left outer join ms_user as us_crt
						on us_crt.kode_user=rcn.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=rcn.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=rcn.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=rcn.kode_upt
					left outer join ms_kegiatan as kgt
						on kgt.kode_kegiatan=rcn.jenis_kegiatan
					".$where." and rcn.id_prov='".$prov."'  and rcn.kode_upt='".$upt."' and rcn.status in ('0','1','4') ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getRencanaSosialisasiFilter($where,$order,$limit,$param){
		  global $db;
		  $sql = "SELECT  kgt.kegiatan,rcn.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_rencana_sosialisasi as rcn
					left outer join ms_user as us_crt
						on us_crt.kode_user=rcn.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=rcn.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=rcn.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=rcn.kode_upt
					left outer join ms_kegiatan as kgt
						on kgt.kode_kegiatan=rcn.jenis_kegiatan
					".$where." ".base64_decode($param)."  and rcn.status='3'  ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountRencanaSosialisasiFilter($where,$param){
		  global $db;
		  $sql = "SELECT rcn.kode_rencana_sosialisasi
					FROM tr_rencana_sosialisasi as rcn
					left outer join ms_user as us_crt
						on us_crt.kode_user=rcn.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=rcn.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=rcn.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=rcn.kode_upt
					left outer join ms_kegiatan as kgt
						on kgt.kode_kegiatan=rcn.jenis_kegiatan
					".$where." ".base64_decode($param)."   and rcn.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	//End Rencana Sosialisasi

	//Start Monev Sosialisasi
	function getMonevSosialisasi($where,$order,$limit,$prov='',$upt=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		// $query_add=" and mnv.id_prov='".$prov."' ";
		  }	 
		  if(!empty($upt)){
		  		// $query_add=" and mnv.kode_upt='".$upt."' ";
		  }	
		  $sql = "SELECT  kgt.kegiatan,mnv.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_monev_sosialisasi as mnv
					left outer join ms_user as us_crt
						on us_crt.kode_user=mnv.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=mnv.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=mnv.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=mnv.kode_upt
					left outer join ms_kegiatan as kgt
						on kgt.kode_kegiatan=mnv.jenis_kegiatan
					".$where." ".$query_add." and mnv.status='3'  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountMonevSosialisasi($where,$prov='',$upt=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		// $query_add=" and mnv.id_prov='".$prov."' ";
		  }	 
		  if(!empty($upt)){
		  		// $query_add=" and mnv.kode_upt='".$upt."' ";
		  }	
		  $sql = "SELECT mnv.kode_monev_sosialisasi
					FROM tr_monev_sosialisasi as mnv
					left outer join ms_user as us_crt
						on us_crt.kode_user=mnv.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=mnv.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=mnv.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=mnv.kode_upt
					left outer join ms_kegiatan as kgt
						on kgt.kode_kegiatan=mnv.jenis_kegiatan
					".$where."  ".$query_add." and mnv.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getMonevSosialisasiWaiting($where,$order,$limit,$prov='',$upt=''){
		  global $db;
		  $sql = "SELECT  kgt.kegiatan,mnv.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_monev_sosialisasi as mnv
					left outer join ms_user as us_crt
						on us_crt.kode_user=mnv.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=mnv.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=mnv.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=mnv.kode_upt
					left outer join ms_kegiatan as kgt
						on kgt.kode_kegiatan=mnv.jenis_kegiatan
					".$where." and mnv.id_prov='".$prov."' and mnv.kode_upt='".$upt."' and mnv.status in ('0','1','4')  ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountMonevSosialisasiWaiting($where,$prov='',$upt=''){
		  global $db;
		  $sql = "SELECT mnv.kode_monev_sosialisasi
					FROM tr_monev_sosialisasi as mnv
					left outer join ms_user as us_crt
						on us_crt.kode_user=mnv.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=mnv.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=mnv.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=mnv.kode_upt
					left outer join ms_kegiatan as kgt
						on kgt.kode_kegiatan=mnv.jenis_kegiatan
					".$where." and mnv.id_prov='".$prov."'  and mnv.kode_upt='".$upt."' and mnv.status in ('0','1','4') ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getMonevSosialisasiFilter($where,$order,$limit,$param){
		  global $db;
		  $sql = "SELECT  kgt.kegiatan,mnv.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_monev_sosialisasi as mnv
					left outer join ms_user as us_crt
						on us_crt.kode_user=mnv.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=mnv.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=mnv.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=mnv.kode_upt
					left outer join ms_kegiatan as kgt
						on kgt.kode_kegiatan=mnv.jenis_kegiatan
					".$where." ".base64_decode($param)."  and mnv.status='3'  ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountMonevSosialisasiFilter($where,$param){
		  global $db;
		  $sql = "SELECT  mnv.kode_monev_sosialisasi
					FROM tr_monev_sosialisasi as mnv
					left outer join ms_user as us_crt
						on us_crt.kode_user=mnv.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=mnv.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=mnv.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=mnv.kode_upt
					left outer join ms_kegiatan as kgt
						on kgt.kode_kegiatan=mnv.jenis_kegiatan
					".$where." ".base64_decode($param)."   and mnv.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	//End Monev Sosialisasi

	//Start Galeri
	function getGaleri($where,$order,$limit,$prov='',$upt=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		 //$query_add=" and gl.id_prov='".$prov."' ";
		  }	 
		  if(!empty($upt)){
		  		 //$query_add=" and gl.kode_upt='".$upt."' ";
		  }	
		  $sql = "SELECT  upt.nama_upt,pv.nama_prov,gl.*,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_galeri as gl
					left outer join ms_user as us_crt
						on us_crt.kode_user=gl.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=gl.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=gl.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=gl.kode_upt
					".$where." ".$query_add."  and gl.status='3' ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountGaleri($where,$prov='',$upt=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		 $query_add=" and gl.id_prov='".$prov."' ";
		  }	 
		  if(!empty($upt)){
		  		 $query_add=" and gl.kode_upt='".$upt."' ";
		  }	
		  $sql = "SELECT  gl.kode_galeri
					FROM tr_galeri as gl
					left outer join ms_user as us_crt
						on us_crt.kode_user=gl.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=gl.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=gl.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=gl.kode_upt
					".$where." ".$query_add." and gl.status='3'  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getGaleriFilter($where,$order,$limit,$param){
		  global $db;
		  $sql = "SELECT  upt.nama_upt,pv.nama_prov,gl.*,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_galeri as gl
					left outer join ms_user as us_crt
						on us_crt.kode_user=gl.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=gl.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=gl.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=gl.kode_upt
					".$where." ".base64_decode($param)."  and gl.status='3'  ".$order.$limit;
				
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountGaleriFilter($where,$param){
		  global $db;
		  $sql = "SELECT  gl.kode_galeri
					FROM tr_galeri as gl
					left outer join ms_user as us_crt
						on us_crt.kode_user=gl.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=gl.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=gl.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=gl.kode_upt	
					".$where." ".base64_decode($param)."   and gl.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getGaleriWaiting($where,$order,$limit,$prov='',$upt=''){
		  global $db;
		  $sql = "SELECT  upt.nama_upt,pv.nama_prov,gl.*,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_galeri as gl
					left outer join ms_user as us_crt
						on us_crt.kode_user=gl.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=gl.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=gl.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=gl.kode_upt
					".$where." and gl.id_prov='".$prov."' and gl.kode_upt='".$upt."' and gl.status in ('0','1','4') ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountGaleriWaiting($where,$prov='',$upt=''){
		  global $db;
		  $sql = "SELECT  gl.kode_galeri
					FROM tr_galeri as gl
					left outer join ms_user as us_crt
						on us_crt.kode_user=gl.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=gl.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=gl.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=gl.kode_upt
					".$where." and gl.id_prov='".$prov."' and gl.kode_upt='".$upt."' and gl.status in ('0','1','4')";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	//End Galeri
}
?>