<?php 
class pelayanan { 

	//Start ISR
	function getISRFilter($where,$order,$limit,$param){
		  global $db;
		  $sql = "SELECT  DISTINCT(isr.kode_isr),isr.*,spp.no_klien_licence,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah,cp.company_name,mtd.metode as nama_metode
					FROM tr_isr as isr
					inner join tr_spp AS spp 
						on spp.no_spp = isr.no_spp
					left outer join ms_company AS cp 
						on cp.company_id = spp.no_klien_licence
					left outer join ms_user as us_crt
						on us_crt.kode_user=isr.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=isr.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=isr.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=isr.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=isr.metode	
					".$where." ".base64_decode($param)."  and isr.status='3'  ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountISRFilter($where,$param){
		  global $db;
		  $sql = "SELECT  DISTINCT(isr.kode_isr)
					FROM tr_isr as isr
					inner join tr_spp AS spp 
						on spp.no_spp = isr.no_spp
					left outer join ms_company AS cp 
						on cp.company_id = spp.no_klien_licence
					left outer join ms_user as us_crt
						on us_crt.kode_user=isr.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=isr.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=isr.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=isr.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=isr.metode	
					".$where." ".base64_decode($param)."   and isr.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getISR($where,$order,$limit,$prov=''){
		  global $db;
		  $query_prov="";
		  if(!empty($prov)){
		  		 $query_prov=" and isr.id_prov='".$prov."' ";
		  }	
		  $sql = "SELECT  DISTINCT(isr.kode_isr),isr.*,spp.no_klien_licence,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah,cp.company_name,mtd.metode as nama_metode
					FROM tr_isr as isr
					inner join tr_spp AS spp 
						on spp.no_spp = isr.no_spp
					left outer join ms_company AS cp 
						on cp.company_id = spp.no_klien_licence
					left outer join ms_user as us_crt
						on us_crt.kode_user=isr.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=isr.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=isr.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=isr.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=isr.metode	
					".$where." ".$query_prov."  and isr.status='3'  ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountISR($where,$prov=''){
		  global $db;
		  $query_prov="";
		  if(!empty($prov)){
		  		 $query_prov=" and isr.id_prov='".$prov."' ";
		  }	
		  $sql = "SELECT  DISTINCT(isr.kode_isr)
					FROM tr_isr as isr
					inner join tr_spp AS spp 
						on spp.no_spp = isr.no_spp
					left outer join ms_company AS cp 
						on cp.company_id = spp.no_klien_licence
					left outer join ms_user as us_crt
						on us_crt.kode_user=isr.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=isr.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=isr.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=isr.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=isr.metode	
					".$where." ".$query_prov."  and isr.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getISRWaiting($where,$order,$limit,$prov=''){
		  global $db;
		  $sql = "SELECT  DISTINCT(isr.kode_isr),isr.*,spp.no_klien_licence,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah,cp.company_name,mtd.metode as nama_metode
					FROM tr_isr as isr
					inner join tr_spp AS spp 
						on spp.no_spp = isr.no_spp
					left outer join ms_company AS cp 
						on cp.company_id = spp.no_klien_licence
					left outer join ms_user as us_crt
						on us_crt.kode_user=isr.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=isr.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=isr.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=isr.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=isr.metode	
					".$where." and isr.id_prov='".$prov."' and isr.status in ('0','1','4')  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountISRWaiting($where,$prov=''){
		  global $db;
		  $sql = "SELECT  DISTINCT(isr.kode_isr)
					FROM tr_isr as isr
					inner join tr_spp AS spp 
						on spp.no_spp = isr.no_spp
					left outer join ms_company AS cp 
						on cp.company_id = spp.no_klien_licence
					left outer join ms_user as us_crt
						on us_crt.kode_user=isr.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=isr.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=isr.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=isr.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=isr.metode	
					".$where." and isr.id_prov='".$prov."' and isr.status in ('0','1','4') ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	//End ISR

	//Start Revoke
	function getRevokeFilter($where,$order,$limit,$param){
		  global $db;
		  $sql = "SELECT  rvk.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah,cp.company_name,mtd.metode as nama_metode
					FROM tr_revoke as rvk
					left outer join ms_company AS cp 
						on cp.company_id = rvk.no_client
					left outer join ms_user as us_crt
						on us_crt.kode_user=rvk.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=rvk.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=rvk.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=rvk.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=rvk.metode		
					".$where." ".base64_decode($param)."  and rvk.status='3'  ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountRevokeFilter($where,$param){
		  global $db;
		  $sql = "SELECT  rvk.kode_revoke
					FROM tr_revoke as rvk
					left outer join ms_company AS cp 
						on cp.company_id = rvk.no_client
					left outer join ms_user as us_crt
						on us_crt.kode_user=rvk.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=rvk.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=rvk.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=rvk.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=rvk.metode	
					".$where." ".base64_decode($param)."   and rvk.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getRevoke($where,$order,$limit,$prov=''){
		  global $db;
		  $query_prov="";
		  if(!empty($prov)){
		  		 $query_prov=" and rvk.id_prov='".$prov."' ";
		  }	
		  $sql = "SELECT  rvk.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah,cp.company_name,mtd.metode as nama_metode
					FROM tr_revoke as rvk
					left outer join ms_company AS cp 
						on cp.company_id = rvk.no_client
					left outer join ms_user as us_crt
						on us_crt.kode_user=rvk.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=rvk.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=rvk.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=rvk.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=rvk.metode	
					".$where." ".$query_prov."  and rvk.status='3'  ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountRevoke($where,$prov=''){
		  global $db;
		  $query_prov="";
		  if(!empty($prov)){
		  		 $query_prov=" and rvk.id_prov='".$prov."' ";
		  }	
		  $sql = "SELECT  rvk.kode_revoke
					FROM tr_revoke as rvk
					left outer join ms_company AS cp 
						on cp.company_id = rvk.no_client
					left outer join ms_user as us_crt
						on us_crt.kode_user=rvk.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=rvk.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=rvk.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=rvk.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=rvk.metode	
					".$where." ".$query_prov."  and rvk.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getRevokeWaiting($where,$order,$limit,$prov=''){
		  global $db;
		  $sql = "SELECT  rvk.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah,cp.company_name,mtd.metode as nama_metode 
					FROM tr_revoke as rvk
					left outer join ms_company AS cp 
						on cp.company_id = rvk.no_client
					left outer join ms_user as us_crt
						on us_crt.kode_user=rvk.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=rvk.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=rvk.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=rvk.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=rvk.metode		
					".$where." and rvk.id_prov='".$prov."' and rvk.status in ('0','1','4')  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountRevokeWaiting($where,$prov=''){
		  global $db;
		  $sql = "SELECT  rvk.kode_revoke
					FROM tr_revoke as rvk
					left outer join ms_company AS cp 
						on cp.company_id = rvk.no_client
					left outer join ms_user as us_crt
						on us_crt.kode_user=rvk.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=rvk.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=rvk.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=rvk.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=rvk.metode	
					".$where." and rvk.id_prov='".$prov."' and rvk.status in ('0','1','4') ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	//End Revoke

	//Start SPP
	function getAkumulasiSPPAll($where,$order,$limit){
		  global $db;
		  $sql = "SELECT  pv.nama_prov,COUNT(kode_pelayanan) as total,pl.jenis_perizinan, 
						COUNT(IF(tujuan='Pengaduan Gangguan',1, NULL)) 'gangguan',	
						COUNT(IF(tujuan='Konsultasi',1, NULL)) 'konsultasi',
						COUNT(IF(tujuan='Aksestensi',1, NULL)) 'aksestensi',
						COUNT(IF(tujuan='Lain-Lain',1, NULL)) 'lain'
						from tr_pelayanan as pl
							inner join ms_provinsi as pv 
									on pv.id_prov=pl.id_prov
					".$where." and pl.status='3' GROUP BY pv.nama_prov  ".$order.$limit;
			
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountAkumulasiSPPAll($where){
		  global $db;
		  $sql = "SELECT  pv.nama_prov,COUNT(kode_pelayanan) as total,pl.jenis_perizinan, 
						COUNT(IF(tujuan='Pengaduan Gangguan',1, NULL)) 'gangguan',	
						COUNT(IF(tujuan='Konsultasi',1, NULL)) 'konsultasi',
						COUNT(IF(tujuan='Lain-Lain',1, NULL)) 'lain'
						from tr_pelayanan as pl
							inner join ms_provinsi as pv 
									on pv.id_prov=pl.id_prov
					".$where." and pl.status='3' GROUP BY pv.nama_prov  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getAkumulasiSPP($where,$order,$limit,$prov){
		  global $db;
		  $sql = "SELECT  pv.nama_prov,COUNT(kode_pelayanan) as total,pl.jenis_perizinan, 
						COUNT(IF(tujuan='Pengaduan Gangguan',1, NULL)) 'gangguan',	
						COUNT(IF(tujuan='Konsultasi',1, NULL)) 'konsultasi',
						COUNT(IF(tujuan='Lain-Lain',1, NULL)) 'lain'
						from tr_pelayanan as pl
							inner join ms_provinsi as pv 
									on pv.id_prov=pl.id_prov
					".$where." and pl.status='3' and pl.id_prov='".$prov."' GROUP BY jenis_perizinan ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountAkumulasiSPP($where,$prov){
		  global $db;
		  $sql = "SELECT  pv.nama_prov,COUNT(kode_pelayanan) as total,pl.jenis_perizinan, 
						COUNT(IF(tujuan='Pengaduan Gangguan',1, NULL)) 'gangguan',	
						COUNT(IF(tujuan='Konsultasi',1, NULL)) 'konsultasi',
						COUNT(IF(tujuan='Lain-Lain',1, NULL)) 'lain'
						from tr_pelayanan as pl
							inner join ms_provinsi as pv 
									on pv.id_prov=pl.id_prov
					".$where." and pl.status='3' and pl.id_prov='".$prov."' GROUP BY jenis_perizinan ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getSPPFilter($where,$order,$limit,$param){
		  global $db;
		  $sql = "SELECT  spp.*,kgs.kategori_spp as kgs_data,spp.created_date as tgl_buat,spp.status as status_verif,pv.nama_prov,us_crt.username as pembuat ,us_updt.username as perubah,cp.company_name,upt.nama_upt,mtd.metode as nama_metode
					FROM tr_spp as spp
					left outer join ms_kategori_spp AS kgs 
						on kgs.kode_kategori_spp = spp.kategori_spp
					left outer join ms_company AS cp 
						on cp.company_id = spp.no_klien_licence
					left outer join ms_user as us_crt
						on us_crt.kode_user=spp.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=spp.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=spp.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=spp.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=spp.metode	
					".$where." ".base64_decode($param)."  and spp.status='3'  ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountSPPFilter($where,$param){
		  global $db;
		  $sql = "SELECT  spp.kode_spp
					FROM tr_spp as spp
					left outer join ms_kategori_spp AS kgs 
						on kgs.kode_kategori_spp = spp.kategori_spp
					left outer join ms_company AS cp 
						on cp.company_id = spp.no_klien_licence
					left outer join ms_user as us_crt
						on us_crt.kode_user=spp.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=spp.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=spp.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=spp.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=spp.metode	
					".$where." ".base64_decode($param)."   and spp.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getSPP($where,$order,$limit,$prov='',$upt=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		 $query_add=" and spp.id_prov='".$prov."' ";
		  }	 
		  if(!empty($upt)){
		  		 $query_add=" and spp.kode_upt='".$upt."' ";
		  }	
		  $sql = "SELECT  spp.*,kgs.kategori_spp as kgs_data,spp.created_date as tgl_buat,spp.status as status_verif,pv.nama_prov,us_crt.username as pembuat ,us_updt.username as perubah,cp.company_name,upt.nama_upt,mtd.metode as nama_metode
					FROM tr_spp as spp
					left outer join ms_kategori_spp AS kgs 
						on kgs.kode_kategori_spp = spp.kategori_spp
					left outer join ms_company AS cp 
						on cp.company_id = spp.no_klien_licence
					left outer join ms_user as us_crt
						on us_crt.kode_user=spp.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=spp.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=spp.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=spp.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=spp.metode	
					".$where." ".$query_add."  and spp.status='3'  ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountSPP($where,$prov='',$upt=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		 $query_add=" and spp.id_prov='".$prov."' ";
		  }	 
		  if(!empty($upt)){
		  		 $query_add=" and spp.kode_upt='".$upt."' ";
		  }	
		  $sql = "SELECT  spp.kode_spp
					FROM tr_spp as spp
					left outer join ms_kategori_spp AS kgs 
						on kgs.kode_kategori_spp = spp.kategori_spp
					left outer join ms_company AS cp 
						on cp.company_id = spp.no_klien_licence
					left outer join ms_user as us_crt
						on us_crt.kode_user=spp.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=spp.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=spp.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=spp.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=spp.metode	
					".$where." ".$query_add."  and spp.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getSPPWaiting($where,$order,$limit,$prov='',$upt=''){
		  global $db;
		  $sql = "SELECT  spp.*,kgs.kategori_spp as kgs_data,spp.created_date as tgl_buat,spp.status as status_verif,pv.nama_prov,us_crt.username as pembuat ,us_updt.username as perubah,cp.company_name,upt.nama_upt,mtd.metode as nama_metode
					FROM tr_spp as spp
					left outer join ms_kategori_spp AS kgs 
						on kgs.kode_kategori_spp = spp.kategori_spp
					left outer join ms_company AS cp 
						on cp.company_id = spp.no_klien_licence
					left outer join ms_user as us_crt
						on us_crt.kode_user=spp.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=spp.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=spp.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=spp.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=spp.metode	
					".$where." and spp.id_prov='".$prov."' and spp.kode_upt='".$upt."'  and spp.status in ('0','1','4')  ".$order.$limit;


	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountSPPWaiting($where,$prov='',$upt=''){
		  global $db;
		  $sql = "SELECT  spp.kode_spp
					FROM tr_spp as spp
					left outer join ms_kategori_spp AS kgs 
						on kgs.kode_kategori_spp = spp.kategori_spp
					left outer join ms_company AS cp 
						on cp.company_id = spp.no_klien_licence
					left outer join ms_user as us_crt
						on us_crt.kode_user=spp.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=spp.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=spp.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=spp.kode_upt
					left outer join ms_metode as mtd
						on mtd.id_metode=spp.metode	
					".$where." and spp.id_prov='".$prov."' and spp.kode_upt='".$upt."' and spp.status in ('0','1','4') ";

				
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	//End SPP


	//Start Loket Pelayanan
	function getAkumulasiPelayananAll($where,$order,$limit){
		  global $db;

		  $tj ="";
		  $sql_tj = $db->Execute("select * from ms_tujuan");
		  while($aRowz = $sql_tj->FetchRow()){
		  		//$nama_tj = str_replace(' ','_',strtolower($aRowz['tujuan']));
		  		$tj .="COUNT(IF(tujuan='".$aRowz['kode_tujuan']."',1, NULL)) '".$aRowz['kode_tujuan']."',";
		  }
		  $tj = rtrim($tj,',');

		  $sql = "SELECT  distinct(upt.nama_upt),pv.id_prov,pv.nama_prov,grp.jenis,COUNT(kode_pelayanan) as total,pl.jenis_perizinan, 
						".$tj."
						from tr_pelayanan as pl
							inner join ms_provinsi as pv 
									on pv.id_prov=pl.id_prov
							inner join ms_upt as upt 
									on upt.id_prov=pl.id_prov
							inner join ms_group_layanan as grp 
									on grp.id_jenis=pl.jenis_perizinan
					".$where." and pl.status='3' GROUP BY upt.kode_upt  ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountAkumulasiPelayananAll($where){
		  global $db;
		  $tj ="";
		  $sql_tj = $db->Execute("select * from ms_tujuan");
		  while($aRowz = $sql_tj->FetchRow()){
		  		//$nama_tj = str_replace(' ','_',strtolower($aRowz['tujuan']));
		  		$tj .="COUNT(IF(tujuan='".$aRowz['kode_tujuan']."',1, NULL)) '".$aRowz['kode_tujuan']."',";
		  }
		  $tj = rtrim($tj,',');

		  $sql = "SELECT  DISTINCT(upt.nama_upt),pv.nama_prov,COUNT(kode_pelayanan) as total,pl.jenis_perizinan, 
						".$tj."
						from tr_pelayanan as pl
							inner join ms_provinsi as pv 
									on pv.id_prov=pl.id_prov
							inner join ms_upt as upt 
									on upt.id_prov=pl.id_prov
							inner join ms_group_layanan as grp 
									on grp.id_jenis=pl.jenis_perizinan
					".$where." and pl.status='3' GROUP BY upt.kode_upt";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getAkumulasiPelayanan($where,$order,$limit,$prov,$upt){
		  global $db;
		  $tj ="";
		  $sql_tj = $db->Execute("select * from ms_tujuan");
		  while($aRowz = $sql_tj->FetchRow()){
		  		//$nama_tj = str_replace(' ','_',strtolower($aRowz['tujuan']));
		  		$tj .="COUNT(IF(tujuan='".$aRowz['kode_tujuan']."',1, NULL)) '".$aRowz['kode_tujuan']."',";
		  }
		  $tj = rtrim($tj,',');
		  $sql = "SELECT  pv.nama_prov,COUNT(kode_pelayanan) as total,pl.jenis_perizinan,grp.jenis,
						".$tj."
						from tr_pelayanan as pl
							inner join ms_provinsi as pv 
									on pv.id_prov=pl.id_prov
							inner join ms_group_layanan as grp
									on grp.id_jenis=pl.jenis_perizinan
					".$where." and pl.status='3' and pl.id_prov='".$prov."' and pl.kode_upt='".$upt."'  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountAkumulasiPelayanan($where,$prov,$upt){
		  global $db;
		  $tj ="";
		  $sql_tj = $db->Execute("select * from ms_tujuan");
		  while($aRowz = $sql_tj->FetchRow()){
		  		//$nama_tj = str_replace(' ','_',strtolower($aRowz['tujuan']));
		  		$tj .="COUNT(IF(tujuan='".$aRowz['kode_tujuan']."',1, NULL)) '".$aRowz['kode_tujuan']."',";
		  }
		 $tj = rtrim($tj,',');
		  $sql = "SELECT  pv.nama_prov,COUNT(kode_pelayanan) as total,pl.jenis_perizinan, 
						".$tj."
						from tr_pelayanan as pl
							inner join ms_provinsi as pv 
									on pv.id_prov=pl.id_prov
							inner join ms_group_layanan as grp
									on grp.id_jenis=pl.jenis_perizinan
					".$where." and pl.status='3' and pl.id_prov='".$prov."' and pl.kode_upt='".$upt."' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getLoketPelayananFilter($where,$order,$limit,$param){
		  global $db;
		  $sql = "SELECT  pl.*,pv.nama_prov,cp.company_name,cp.alamat,us_crt.username as pembuat ,us_updt.username as perubah,upt.nama_upt,tj.tujuan as nama_tj,grp.jenis
					FROM tr_pelayanan as pl
					left outer join ms_company_loket as cp
						on cp.company_id=pl.kode_perusahaan
					left outer join ms_user as us_crt
						on us_crt.kode_user=pl.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=pl.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=pl.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=pl.kode_upt
					left outer join ms_group_layanan as  grp
						on grp.id_jenis=pl.jenis_perizinan
					left outer join ms_sub_group_layanan as  sb_grp
						on sb_grp.id_sub_group_layanan=pl.sub_group_perizinan
					left outer join ms_tujuan as  tj
						on tj.kode_tujuan=pl.tujuan
					".$where." ".base64_decode($param)."  and pl.status='3'  ".$order.$limit;
				
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountLoketPelayananFilter($where,$param){
		  global $db;
		  $sql = "SELECT  pl.kode_pelayanan
					FROM tr_pelayanan as pl
					left outer join ms_company_loket as cp
						on cp.company_id=pl.kode_perusahaan
					left outer join ms_user as us_crt
						on us_crt.kode_user=pl.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=pl.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=pl.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=pl.kode_upt
					left outer join ms_group_layanan as  grp
						on grp.id_jenis=pl.jenis_perizinan
					left outer join ms_sub_group_layanan as  sb_grp
						on sb_grp.id_sub_group_layanan=pl.sub_group_perizinan
					left outer join ms_tujuan as  tj
						on tj.kode_tujuan=pl.tujuan
					".$where." ".base64_decode($param)."   and pl.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getLoketPelayanan($where,$order,$limit,$prov='',$upt=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		 $query_add .=" and pl.id_prov='".$prov."' ";
		  }	
		  if(!empty($upt)){
		  		 $query_add .=" and pl.kode_upt='".$upt."' ";
		  }	
		  $sql = "SELECT  pl.*,pv.nama_prov,cp.company_name,cp.alamat,us_crt.username as pembuat ,us_updt.username as perubah,upt.nama_upt,grp.jenis,sb_grp.sub_group_layanan,tj.tujuan as nama_tujuan
					FROM tr_pelayanan as pl
					left outer join ms_company_loket as cp
						on cp.company_id=pl.kode_perusahaan
					left outer join ms_user as us_crt
						on us_crt.kode_user=pl.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=pl.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=pl.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=pl.kode_upt
					left outer join ms_group_layanan as  grp
						on grp.id_jenis=pl.jenis_perizinan
					left outer join ms_sub_group_layanan as  sb_grp
						on sb_grp.id_sub_group_layanan=pl.sub_group_perizinan
					left outer join ms_tujuan as  tj
						on tj.kode_tujuan=pl.tujuan
					".$where." ".$query_add."  and pl.status='3'  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountLoketPelayanan($where,$prov='',$upt=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		 $query_add .=" and pl.id_prov='".$prov."' ";
		  }	
		  if(!empty($upt)){
		  		 $query_add .=" and pl.kode_upt='".$upt."' ";
		  }	
		  $sql = "SELECT  pl.kode_pelayanan
					FROM tr_pelayanan as pl
					left outer join ms_company_loket as cp
						on cp.company_id=pl.kode_perusahaan
					left outer join ms_user as us_crt
						on us_crt.kode_user=pl.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=pl.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=pl.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=pl.kode_upt
					left outer join ms_group_layanan as  grp
						on grp.id_jenis=pl.jenis_perizinan
					left outer join ms_sub_group_layanan as  sb_grp
						on sb_grp.id_sub_group_layanan=pl.sub_group_perizinan
					left outer join ms_tujuan as  tj
						on tj.kode_tujuan=pl.tujuan
					".$where." ".$query_add."  and pl.status='3' ";

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getLoketPelayananWaiting($where,$order,$limit,$prov='',$upt=''){
		  global $db;
		  $sql = "SELECT  pl.*,cp.company_name,cp.alamat,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_pelayanan as pl
					left outer join ms_company_loket as cp
						on cp.company_id=pl.kode_perusahaan
					left outer join ms_user as us_crt
						on us_crt.kode_user=pl.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=pl.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=pl.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=pl.kode_upt
					left outer join ms_group_layanan as  grp
						on grp.id_jenis=pl.jenis_perizinan
					left outer join ms_sub_group_layanan as  sb_grp
						on sb_grp.id_sub_group_layanan=pl.sub_group_perizinan
					".$where." and pl.id_prov='".$prov."' and pl.kode_upt='".$upt."' and pl.status in ('0','1','4')  ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountLoketPelayananWaiting($where,$prov='',$upt=''){
		  global $db;
		  $sql = "SELECT  pl.kode_pelayanan
					FROM tr_pelayanan as pl
					left outer join ms_company_loket as cp
						on cp.company_id=pl.kode_perusahaan
					left outer join ms_user as us_crt
						on us_crt.kode_user=pl.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=pl.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=pl.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=pl.kode_upt
					left outer join ms_group_layanan as  grp
						on grp.id_jenis=pl.jenis_perizinan
					left outer join ms_sub_group_layanan as  sb_grp
						on sb_grp.id_sub_group_layanan=pl.sub_group_perizinan
					".$where." and pl.id_prov='".$prov."'  and pl.kode_upt='".$upt."' and pl.status in ('0','1','4') ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	//End Loket Pelayanan

	//Start Penanganan Piutang
	function getPiutangFilter($where,$order,$limit,$param){
		  global $db;
		  $sql = "SELECT  pt.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah,cp.company_name
					FROM tr_penanganan_piutang as pt
					left outer join ms_company AS cp 
						on cp.company_id = pt.no_client
					left outer join ms_user as us_crt
						on us_crt.kode_user=pt.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=pt.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=pt.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=pt.kode_upt	
					".$where." ".base64_decode($param)."  and pt.status='3'  ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountPiutangFilter($where,$param){
		  global $db;
		  $sql = "SELECT  pt.kode_piutang
					FROM tr_penanganan_piutang as pt
					left outer join ms_company AS cp 
						on cp.company_id = pt.no_client
					left outer join ms_user as us_crt
						on us_crt.kode_user=pt.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=pt.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=pt.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=pt.kode_upt
					".$where." ".base64_decode($param)."   and pt.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getPiutang($where,$order,$limit,$prov=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		 $query_add=" and (pt.id_prov='".$prov."' or pt.id_prov is null) ";
		  }	 
		  if(!empty($upt)){
		  		 $query_add=" and (pt.kode_upt='".$upt."' or pt.kode_upt is null) ";
		  }	
		  $sql = "SELECT  kb.nama_kab_kot,pt.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah,cp.company_name
					FROM tr_penanganan_piutang as pt
					left outer join ms_company AS cp 
						on cp.company_id = pt.no_client
					left outer join ms_user as us_crt
						on us_crt.kode_user=pt.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=pt.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=pt.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=pt.kode_upt	
					left outer join ms_kabupaten_kota as kb
						on kb.id_kabkot=pt.nama_kpknl	
					".$where." ".$query_add."  and pt.status='3'  ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountPiutang($where,$prov=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		 $query_add=" and (pt.id_prov='".$prov."' or pt.id_prov is null) ";
		  }	 
		  if(!empty($upt)){
		  		 $query_add=" and (pt.kode_upt='".$upt."' or pt.kode_upt is null) ";
		  }	
		  $sql = "SELECT  pt.kode_piutang
					FROM tr_penanganan_piutang as pt
					left outer join ms_company AS cp 
						on cp.company_id = pt.no_client
					left outer join ms_user as us_crt
						on us_crt.kode_user=pt.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=pt.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=pt.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=pt.kode_upt
					left outer join ms_kabupaten_kota as kb
						on kb.id_kabkot=pt.nama_kpknl		
					".$where." ".$query_add."  and pt.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getPiutangWaiting($where,$order,$limit,$prov=''){
		  global $db;
		  $sql = "SELECT  pt.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah,cp.company_name
					FROM tr_penanganan_piutang as pt
					left outer join ms_company AS cp 
						on cp.company_id = pt.no_client
					left outer join ms_user as us_crt
						on us_crt.kode_user=pt.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=pt.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=pt.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=pt.kode_upt
					".$where." and pt.id_prov='".$prov."' and pt.status in ('0','1','4')  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountPiutangWaiting($where,$prov=''){
		  global $db;
		  $sql = "SELECT  pt.kode_piutang
					FROM tr_penanganan_piutang as pt
					left outer join ms_company AS cp 
						on cp.company_id = pt.no_client
					left outer join ms_user as us_crt
						on us_crt.kode_user=pt.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=pt.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=pt.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=pt.kode_upt
					".$where." and pt.id_prov='".$prov."' and pt.status in ('0','1','4') ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	//End Penanganan Piutang

	//Start Unar
	function getUnarFilter($where,$order,$limit,$param){
		  global $db;
		  $sql = "SELECT  un.*,kb.nama_kab_kot,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_unar as un
					left outer join ms_user as us_crt
						on us_crt.kode_user=un.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=un.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=un.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=un.kode_upt	
					left outer join ms_kabupaten_kota as kb
						on kb.id_kabkot=un.id_kabkot
					".$where." ".base64_decode($param)."  and un.status='3'  ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountUnarFilter($where,$param){
		  global $db;
		  $sql = "SELECT  un.kode_unar
					FROM tr_unar as un
					left outer join ms_user as us_crt
						on us_crt.kode_user=un.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=un.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=un.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=un.kode_upt	
					left outer join ms_kabupaten_kota as kb
						on kb.id_kabkot=un.id_kabkot
					".$where." ".base64_decode($param)."   and un.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getUnar($where,$order,$limit,$prov='',$upt=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		 $query_add=" and (un.id_prov='".$prov."' or un.id_prov is null) ";
		  }	 
		  if(!empty($upt)){
		  		 $query_add=" and (un.kode_upt='".$upt."' or un.kode_upt is null) ";
		  }	
		  $sql = "SELECT  un.*,kb.nama_kab_kot,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_unar as un
					left outer join ms_user as us_crt
						on us_crt.kode_user=un.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=un.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=un.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=un.kode_upt	
					left outer join ms_kabupaten_kota as kb
						on kb.id_kabkot=un.id_kabkot
					".$where." ".$query_add."  and un.status='3'  ".$order.$limit;
			//echo $sql;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountUnar($where,$prov='',$upt=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		 $query_add=" and (un.id_prov='".$prov."' or un.id_prov is null) ";
		  }	 
		  if(!empty($upt)){
		  		 $query_add=" and (un.kode_upt='".$upt."' or un.kode_upt is null) ";
		  }	
		  $sql = "SELECT  un.kode_unar
					FROM tr_unar as un
					left outer join ms_user as us_crt
						on us_crt.kode_user=un.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=un.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=un.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=un.kode_upt	
					left outer join ms_kabupaten_kota as kb
						on kb.id_kabkot=un.id_kabkot
					".$where." ".$query_add."  and un.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getUnarWaiting($where,$order,$limit,$prov=''){
		  global $db;
		  $sql = "SELECT  un.*,kb.nama_kab_kot,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_unar as un
					left outer join ms_user as us_crt
						on us_crt.kode_user=un.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=un.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=un.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=un.kode_upt	
					left outer join ms_kabupaten_kota as kb
						on kb.id_kabkot=un.id_kabkot
					".$where." and un.id_prov='".$prov."' and un.status in ('0','1','4')  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountUnarWaiting($where,$prov=''){
		  global $db;
		  $sql = "SELECT  un.kode_unar
					FROM tr_unar as un
					left outer join ms_user as us_crt
						on us_crt.kode_user=un.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=un.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=un.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=un.kode_upt	
					left outer join ms_kabupaten_kota as kb
						on kb.id_kabkot=un.id_kabkot
					".$where." and un.id_prov='".$prov."' and un.status in ('0','1','4') ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	//End Unar
}
?>