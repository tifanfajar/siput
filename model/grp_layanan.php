<?php 
class grp_layanan { 

	function getGroupLayaan($where,$order,$limit){
		  global $db;
		  $sql = "SELECT  grp.*,sb.id_sub_group_layanan,sb.sub_group_layanan
					FROM ms_group_layanan as grp
						inner join ms_sub_group_layanan as sb
								on sb.id_jenis=grp.id_jenis
					".$where." and grp.status='1' and  sb.status='1'  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountGroupLayaan($where){
		  global $db;
		  $sql = "SELECT  grp.id_jenis
					FROM ms_group_layanan as grp
						inner join ms_sub_group_layanan as sb
								on sb.id_jenis=grp.id_jenis
					".$where." and grp.status='1' and  sb.status='1'  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getGroupLayaan2($where,$order,$limit){
		  global $db;
		  $sql = "SELECT  grp.*
					FROM ms_group_layanan as grp
					".$where." and grp.status='1'  ".$order.$limit;

	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountGroupLayaan2($where){
		  global $db;
		  $sql = "SELECT  grp.id_jenis
					FROM ms_group_layanan as grp
					".$where." and grp.status='1'  ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
}
?>