<?php 
class company { 

	function getCompany($where,$order,$limit){
		  global $db;
		  $sql = "SELECT  cp.*,pv.nama_prov,kb.nama_kab_kot
					FROM ms_company as cp
					left outer join ms_provinsi as pv  
						on pv.id_prov=cp.id_provinsi
					left outer join ms_kabupaten_kota as kb  
						on kb.id_kabkot=cp.id_kabkot
					".$where." and cp.status_delete='0' ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountCompany($where){
		  global $db;
		  $sql = "SELECT  cp.company_id
					FROM ms_company as cp
					left outer join ms_provinsi as pv  
						on pv.id_prov=cp.id_provinsi
					left outer join ms_kabupaten_kota as kb  
						on kb.id_kabkot=cp.id_kabkot
					".$where."  and cp.status_delete='0' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getCompanyFilter($where,$order,$limit,$param){
		  global $db;
		  $sql = "SELECT  cp.*,pv.nama_prov,kb.nama_kab_kot
					FROM ms_company as cp
					left outer join ms_provinsi as pv  
						on pv.id_prov=cp.id_provinsi
					left outer join ms_kabupaten_kota as kb  
						on kb.id_kabkot=cp.id_kabkot
					".$where." ".base64_decode($param)."  and cp.status_delete='0' ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountCompanyFilter($where,$param){
		  global $db;
		  $sql = "SELECT  cp.company_id
					FROM ms_company as cp
					left outer join ms_provinsi as pv  
						on pv.id_prov=cp.id_provinsi
					left outer join ms_kabupaten_kota as kb  
						on kb.id_kabkot=cp.id_kabkot
					".$where." ".base64_decode($param)."  and cp.status_delete='0' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getCompany_Loket($where,$order,$limit){
		  global $db;
		  $sql = "SELECT  cp.*,pv.nama_prov,kb.nama_kab_kot
					FROM ms_company_loket as cp
					left outer join ms_provinsi as pv  
						on pv.id_prov=cp.id_provinsi
					left outer join ms_kabupaten_kota as kb  
						on kb.id_kabkot=cp.id_kabkot
					".$where." and cp.status_delete='0' ".$order.$limit;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountCompany_Loket($where){
		  global $db;
		  $sql = "SELECT  cp.company_id
					FROM ms_company_loket as cp
					left outer join ms_provinsi as pv  
						on pv.id_prov=cp.id_provinsi
					left outer join ms_kabupaten_kota as kb  
						on kb.id_kabkot=cp.id_kabkot
					".$where."  and cp.status_delete='0' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
}
?>