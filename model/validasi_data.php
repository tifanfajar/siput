<?php 
class validasi_data { 
	//Start Validasi Data
	function getValidasiData($where,$order,$limit,$prov='',$upt=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		 $query_add=" and (vld.id_prov='".$prov."' or vld.id_prov is null) ";
		  }	 
		  if(!empty($upt)){
		  		 $query_add=" and (vld.kode_upt='".$upt."' or vld.kode_upt is null) ";
		  }	
		  $sql = "SELECT  vld.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_validasi_data as vld
					left outer join ms_user as us_crt
						on us_crt.kode_user=vld.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=vld.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=vld.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=vld.kode_upt
					".$where." ".$query_add." and vld.status='3'  ".$order.$limit;
					
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountValidasiData($where,$prov='',$upt=''){
		  global $db;
		  $query_add="";
		  if(!empty($prov)){
		  		 $query_add=" and vld.id_prov='".$prov."' ";
		  }	 
		  if(!empty($upt)){
		  		 $query_add=" and vld.kode_upt='".$upt."' ";
		  }	
		  $sql = "SELECT vld.kode_validasi_data
					FROM tr_validasi_data as vld
					left outer join ms_user as us_crt
						on us_crt.kode_user=vld.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=vld.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=vld.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=vld.kode_upt
					".$where."  ".$query_add." and vld.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getValidasiDataWaiting($where,$order,$limit,$prov='',$upt=''){
		  global $db;
		  $sql = "SELECT  vld.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_validasi_data as vld
					left outer join ms_user as us_crt
						on us_crt.kode_user=vld.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=vld.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=vld.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=vld.kode_upt
					".$where." and vld.id_prov='".$prov."' and vld.kode_upt='".$upt."' and vld.status in ('0','1','4')  ".$order.$limit;
					//echo $sql;
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountValidasiDataWaiting($where,$prov='',$upt=''){
		  global $db;
		  $sql = "SELECT vld.kode_validasi_data
					FROM tr_validasi_data as vld
					left outer join ms_user as us_crt
						on us_crt.kode_user=vld.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=vld.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=vld.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=vld.kode_upt
					".$where." and vld.id_prov='".$prov."'  and vld.kode_upt='".$upt."' and vld.status in ('0','1','4') ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	function getValidasiDataFilter($where,$order,$limit,$param){
		  global $db;
		  $sql = "SELECT  vld.*,pv.nama_prov,upt.nama_upt,us_crt.username as pembuat ,us_updt.username as perubah
					FROM tr_validasi_data as vld
					left outer join ms_user as us_crt
						on us_crt.kode_user=vld.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=vld.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=vld.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=vld.kode_upt
					".$where." ".base64_decode($param)."  and vld.status='3'  ".$order.$limit;
				
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs;
		  }
	}
	function getCountValidasiDataFilter($where,$param){
		  global $db;
		  $sql = "SELECT vld.kode_validasi_data
					FROM tr_validasi_data as vld
					left outer join ms_user as us_crt
						on us_crt.kode_user=vld.created_by
					left outer join ms_user as us_updt
						on us_updt.kode_user=vld.last_update_by
					left outer join ms_provinsi as pv
						on pv.id_prov=vld.id_prov
					left outer join ms_upt as upt
						on upt.kode_upt=vld.kode_upt	
					".$where." ".base64_decode($param)."   and vld.status='3' ";
	      $rs  = $db->Execute($sql);
		  if(!$rs) {
		  	return $db->ErrorMsg();	
		  }
		  else {
		  	return $rs->recordCount();
		  }
	}
	//End Validasi Data
}
?>