
// $(document).ready(function() {
//     $('#tableData').DataTable( {
//         dom: 'Bfrtip',
//         buttons: [
//          'csv', 'pdf', 'print', 'edit'
//         ]
//     } );
// } );

// $('#tableData').DataTable( {
//     dom: 'Bfrtip',
//     buttons: [
//         'pdf',
//         'print'
//     ]
// } );

$('#tableData').DataTable( {
    buttons: [
        {
            extend: 'remove',
            editor: myEditor
        }
    ]
} );
$(document).ready(function() {
    $('#tableData').DataTable( {
        columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]]
    } );
} );

//  $(document).ready(function() {
//     $('#tableData').DataTable( {
//         "dom": '<"filter">frtip'
//     } );
 
//     $("div.dataTables_filter").html('<button>Edit</button>');
// } );

