CKEDITOR.editorConfig = function( config ) {
	config.language = 'en';
	config.uiColor = '#F7B42C';
	config.extraPlugins = 'filebrowser,uploadimage,codesnippet,showprotected,fakeobjects,dialog,dialogui';
	config.height = '150px';
	//config.toolbarCanCollapse = true;
	//config.filebrowserUploadUrl = 'ckeditor/ckupload.php'; 
	config.toolbar = [
		{ name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
		{ name: 'editing', items: [ 'Find', 'Replace', '-', 'SelectAll','TextColor', 'BGColor','Maximize', 'ShowBlocks','-','Image', 'Table', 'HorizontalRule', 'SpecialChar', 'PageBreak', 'Youtube','-','Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
	
		{ name: 'paragraph', items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
		{ name: 'links', items: [ 'Link', 'Unlink','-','Styles', 'Format', 'Font', 'FontSize', 'CodeSnippet' ] },
		'/',
	];

};
CKEDITOR.on('dialogDefinition', function(ev) {
    var editor = ev.editor;
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;

    if (dialogName == 'image') {
        //Remove tab Link, Upload, Advanced
        dialogDefinition.removeContents( 'Link' );
        dialogDefinition.removeContents( 'Upload' );
        dialogDefinition.removeContents( 'advanced' );
    }
});

