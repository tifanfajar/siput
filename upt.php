<?php 
//General Controller
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();

//Model Pelayanan
include "model/upt.php";
$md_upt  = new upt();


//Model User
include "model/user.php";
$md_user      = new user();
session_start();

//Check Access Tab
$akses_tab = $gen_model->GetOneRow("tr_function_access_tab",array('id_tab'=>17,'kode_function_access'=>$_SESSION['group_user'])); 
if(empty($akses_tab['fua_edit'])){
  $akses_tab['fua_edit'] = 0;
}
if(empty($akses_tab['fua_delete'])){
  $akses_tab['fua_delete'] = 0;
}

$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}

$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //Check Session
  $gen_controller->check_session();

  //View
  include "view/header.php";
  include "view/upt.php";
  include "view/footer.php";
}
else if($act=="cetak"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/upt.php";
}
else if($act=="form"){
   include "view/default_style.php"; 
   $form_url  = $_REQUEST['form_url'];
   $activity  = $_REQUEST['activity'];
   if($form_url=="Form_UPT"){
      include "view/ajax_form/upt_form_data.php";
   }
}
else if($act=="table_filter"){
   $param  = $_REQUEST['param'];
   include "view/ajax_table/upt_filter.php";
}
else if($act=="edit" and $id_parameter!=""){
 $edit  = $gen_model->GetOneRow("ms_upt",array('kode_upt'=>$id_parameter)); 
    foreach($edit as $key=>$val){
                  $key=strtolower($key);
                  $$key=$val;
    }
    $data = array(
      'kode_upt'=>$kode_upt,
      'nama_upt'=>$nama_upt,
      'id_prov'=>$id_prov,
      'alamat'=>$alamat,
      'no_fax'=>$no_fax,
      'no_tlp'=>$no_tlp,
      'email'=>$email
    );
    echo json_encode($data); 
}
else if($act=="add"){

    if(!empty($_SESSION['kode_user'])){
      //Proses 
      $insert_data = array();
      $insert_data['kode_upt']            = "upt_".date("ymdhis")."_".rand(1000,9999);
      $insert_data['nama_upt']            = get($_POST['nama_upt']);
      $insert_data['id_prov']             = get($_POST['id_prov']);
      $insert_data['alamat']              = get($_POST['alamat']);
      $insert_data['no_fax']              = get($_POST['no_fax']);
      $insert_data['no_tlp']              = get($_POST['no_tlp']);
      $insert_data['email']               = get($_POST['email']);

       //Validation
      if($insert_data['nama_upt']!=""){
              echo $gen_model->Insert('ms_upt',$insert_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="update"){

    if(!empty($_SESSION['kode_user'])){

      //Proses 
      $update_data = array();
      $update_data['kode_upt']            = get($_POST['kode_upt']);
      $update_data['nama_upt']            = get($_POST['nama_upt']);
      $update_data['id_prov']             = get($_POST['id_prov']);
      $update_data['alamat']              = get($_POST['alamat']);
      $update_data['no_fax']              = get($_POST['no_fax']);
      $update_data['no_tlp']              = get($_POST['no_tlp']);
      $update_data['email']               = get($_POST['email']);

      $where_data = array();
      $where_data['kode_upt']           = get($_POST['kode_upt']);

       //Validation
      if($update_data['kode_upt']!=""){
          echo $gen_model->Update('ms_upt',$update_data,$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="delete"){
    if(!empty($_SESSION['kode_user'])){

       //Paramater
      $where_data = array();
      $where_data['kode_upt']       = $_POST['kode_upt'];

      //Validation
      if(!empty($_POST['kode_upt'])){
           echo $gen_model->Delete('ms_upt',$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest"){
  $aColumns = array('upt.id_prov','pv.nama_prov','upt.kode_upt','upt.nama_upt','upt.no_tlp','upt.no_fax','upt.email','upt.alamat'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_upt->getUPT($sWhere,$sOrder,$sLimit);
  $rResultFilterTotal   = $md_upt->getCountUPT($sWhere);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $param_id = $aRow['kode_upt'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_upt(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
 
    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
       $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_upt(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
    }
    if($akses_tab['fua_delete']=="1"){ 
       $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_upt(\''.base64_encode('kode_upt').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
     }

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($aRow['id_prov'],$aRow['nama_prov'],$aRow['kode_upt'],$aRow['nama_upt'],$aRow['no_tlp'],$aRow['no_fax'],$aRow['email'],$aRow['alamat'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_filter"){
   $param  = $_REQUEST['param'];
   $aColumns = array('upt.id_prov','pv.nama_prov','upt.kode_upt','upt.nama_upt','upt.no_tlp','upt.no_fax','upt.email','upt.alamat'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_upt->getUPTFilter($sWhere,$sOrder,$sLimit,$param);
  $rResultFilterTotal   = $md_upt->getCountUPTFilter($sWhere,$param);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $param_id = $aRow['kode_upt'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_upt(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
 
    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
       $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_upt(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
    }
    if($akses_tab['fua_delete']=="1"){ 
       $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_upt(\''.base64_encode('kode_upt').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
     }

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($aRow['id_prov'],$aRow['nama_prov'],$aRow['kode_upt'],$aRow['nama_upt'],$aRow['no_tlp'],$aRow['no_fax'],$aRow['email'],$aRow['alamat'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else {
  $gen_controller->response_code(http_response_code());
}
?>