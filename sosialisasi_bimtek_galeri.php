<?php 
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();

//Model Sosialisasi Bimtek
include "model/sosialisasi_bimtek.php";
$md_sosialisasi_bimtek  = new sosialisasi_bimtek();


//Model User
include "model/user.php";
$md_user      = new user();
session_start();

//Check Acces Menu and Tab For Notification
$id_tab  = 10;
$id_menu = 3; 
$grp_menu_ntf = get_grp_notif($id_menu,$id_tab);
$group_ntf    = array();
if(!empty($grp_menu_ntf)){
  $variableAry=explode(",",$grp_menu_ntf); 
  foreach($variableAry as $grp) {
    if(!empty($grp)){
     array_push($group_ntf,$grp);
    }
  }
}

//Check Access Tab
$akses_tab = $gen_model->GetOneRow("tr_function_access_tab",array('id_tab'=>$id_tab,'kode_function_access'=>$_SESSION['group_user'])); 
if(empty($akses_tab['fua_edit'])){
  $akses_tab['fua_edit'] = 0;
}
if(empty($akses_tab['fua_delete'])){
  $akses_tab['fua_delete'] = 0;
}




$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}

$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //Check Session
  $gen_controller->check_session();
  //View
  $gen_controller->response_code('404');
}
else if($act=="cetak_detail"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/galeri.php";
}
else if($act=="cetak_all"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/galeri_all.php";
}
else if($act=="table_detail_filter"){
   $param  = $_REQUEST['param'];
   include "view/ajax_table/sosialisasi_table_data_galeri_filter.php";
}
else if($act=="table_all_filter"){
   $param  = $_REQUEST['param'];
   include "view/ajax_table/sosialisasi_table_data_galeri_filter_all.php";
}
else if($act=="import"){
   //File
    $target = basename($_FILES['lampiran']['name']) ;
    move_uploaded_file($_FILES['lampiran']['tmp_name'], $target);
    $path_info = pathinfo($target);
    $ext =  strtolower($path_info['extension']); 
    
    if($ext!="xls"){
      echo "Format file salah";
      die();
    }

    require_once 'lib/excel_reader.php';
    $data = new Spreadsheet_Excel_Reader($_FILES['lampiran']['name'],false);
    $baris = $data->rowcount($sheet_index=0);
    $kode="";
    $hitung_files=0;
    for ($i=2; $i<=$baris; $i++) {
        $insert_data = array();
        $insert_data['kode_galeri']         = "gl_".date("ymdhis")."_".rand(1000,9999);
        $insert_data['id_prov']             = get($_POST['id_prov']);
        $insert_data['kode_upt']            = get($_POST['id_upt']);
        $insert_data['kegiatan']            = $data->val($i, 1);
        $insert_data['deskripsi']           = $data->val($i, 2);
        $insert_data['keterangan']          = $data->val($i, 3);
        $insert_data['created_date']        = $date_now_indo_full;
        $insert_data['last_update']         = $date_now_indo_full;
        $insert_data['created_by']          = $_SESSION['kode_user'];
        $insert_data['last_update_by']      = $_SESSION['kode_user'];

        
          $insert_data['status']              = "3";
        
         if($insert_data['kegiatan']!=""){
            $gen_model->Insert('tr_galeri',$insert_data);
            $kode .= $insert_data['kode_galeri'].",";
            $hitung_files++;
        }
    }
    $kode = rtrim($kode,",");
    //Notification
    if($hitung_files!=0){
         if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                notifikasi('4','10',$_POST['id_prov'],$_POST['id_upt'],'grp_171026194411_1142','Galeri','Ada '.$hitung_files.' data baru pada Galeri yang sudah di setujui',$kode);
              }
              else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                notifikasi('4','10',$_POST['id_prov'],$_POST['id_upt'],'grp_171026194411_1142','Galeri','Ada '.$hitung_files.' data baru pada Galeri yang sudah di setujui',$kode);
              }
              else {
                foreach ($group_ntf as $id_group) {
                   notifikasi('1','10',$_POST['id_prov'],$_POST['id_upt'],$id_group,'Galeri','Ada '.$hitung_files.' data baru pada Galeri',$kode);
                }
              }
    }
    echo "OK";
    
    unlink($_FILES['lampiran']['name']);
}
else if($act=="import_all"){
   //File
    $target = basename($_FILES['lampiran']['name']);
    move_uploaded_file($_FILES['lampiran']['tmp_name'], $target);
    $path_info = pathinfo($target);
    $ext =  strtolower($path_info['extension']); 
    
    if($ext!="xls"){
      echo "Format file salah";
      die();
    }

    require_once 'lib/excel_reader.php';
    $data = new Spreadsheet_Excel_Reader($_FILES['lampiran']['name'],false);
    $baris = $data->rowcount($sheet_index=0);
    for ($i=2; $i<=$baris; $i++) {
        $insert_data = array();
        $insert_data['kode_galeri']         = "gl_".date("ymdhis")."_".rand(1000,9999);
        $insert_data['id_prov']             = $data->val($i, 1);
        $insert_data['kode_upt']            = $data->val($i, 2);
        $insert_data['kegiatan']            = $data->val($i, 3);
        $insert_data['deskripsi']           = $data->val($i, 4);
        $insert_data['keterangan']          = $data->val($i, 5);
        $insert_data['created_date']        = $date_now_indo_full;
        $insert_data['last_update']         = $date_now_indo_full;
        $insert_data['created_by']          = $_SESSION['kode_user'];
        $insert_data['last_update_by']      = $_SESSION['kode_user'];

        $insert_data['status']              = "3";
       
        if( !empty($data->val($i, 1)) && !empty($data->val($i, 2)) ){ //Provinsi && UPT
            $gen_model->Insert('tr_galeri',$insert_data);

              //For Notification All
             $upt=$insert_data['kode_upt'];
             $upt_array[$upt][] =  array (
                                    'kode' => $insert_data['kode_galeri'],
                                    'prov' => $insert_data['id_prov'],
                                    'upt'  => $upt
                                  );
        }
    }

     $keys = array_keys($upt_array);
      for($i = 0; $i < count($upt_array); $i++) {
          $hitung_files = 0;
          $kode  = "";
          foreach($upt_array[$keys[$i]] as $key => $value) {
             $kode .= $value['kode'].",";
              $upt   = $value['upt'];
              $prov  = $value['prov'];
              $hitung_files++;
          }
          $kode = trim($kode,',');
          // echo "Id Prov : ".$prov."<br/>";
          // echo "UPT : ".$upt."<br/>";
          // echo "Kode : ".$kode."<br/>";
          // echo "Total : ".$hitung_files."<br/><br/>";

          //Notification
           if($hitung_files!=0){
               if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                      notifikasi('4','10',$prov,$upt,'grp_171026194411_1143','Galeri','Ada '.$hitung_files.' data baru pada Galeri yang sudah di setujui',$kode);
                    }
                    else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                      notifikasi('4','10',$prov,$upt,'grp_171026194411_1142','Galeri','Ada '.$hitung_files.' data baru pada Galeri yang sudah di setujui',$kode);
                    }
                    else {
                     foreach ($group_ntf as $id_group) {
                         notifikasi('1','10',$prov,$upt,$id_group,'Galeri','Ada '.$hitung_files.' data baru pada Galeri',$kode);
                      }
                    }
          }
      }
    echo "OK";
    
    unlink($_FILES['lampiran']['name']);
}
else if($act=="add"){
    //File
    $tmp              = $_FILES["lampiran"]["tmp_name"];
    $origin_file_name = $_FILES['lampiran']['name'];
    if(!empty($origin_file_name)){
      $ext              = strtolower(pathinfo($_FILES["lampiran"]["name"])['extension']);
      $file_name        = date("ymdhis")."_".rand(1000,9999).".".$ext;
      $path             = "assets/attachment/sosialisasi_bimtek/galeri/";
    }

    if(!empty($_SESSION['kode_user'])){
      //Proses
      $insert_data = array();
      $insert_data['kode_galeri']             = "gl_".date("ymdhis")."_".rand(1000,9999);
      $insert_data['kegiatan']                = get($_POST['kegiatan']);
      $insert_data['deskripsi']               = get($_POST['deskripsi']);
      $insert_data['keterangan']              = get($_POST['keterangan']);
      $insert_data['kode_upt']                = get($_POST['loket_upt']);
      $insert_data['id_prov']                 = get($_POST['id_prov']);
      $insert_data['created_date']            = $date_now_indo_full;
      $insert_data['last_update']             = $date_now_indo_full;
      $insert_data['created_by']              = $_SESSION['kode_user'];
      $insert_data['last_update_by']          = $_SESSION['kode_user'];
      if(!empty($origin_file_name)){
         $insert_data['lampiran']         = $file_name;
      }

       $insert_data['status']              = "3";

        if($_POST['id_prov']=="All"){
          $insert_data['id_prov'] ="";
        }
        if($_POST['loket_upt']=="All"){
          $insert_data['kode_upt']="";
       }

       //Validation
      if($insert_data['kegiatan']!=""){
           if($gen_model->Insert('tr_galeri',$insert_data)=="OK"){
               if(!empty($origin_file_name)){
                $gen_controller->upload_file($tmp,$path,$file_name);
              }
               //Notification
                if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                   notifikasi('4','10',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1142','Galeri','Ada 1 data baru pada Galeri yang sudah di setujui',$insert_data['kode_galeri']);
                }
                else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                  notifikasi('4','10',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1142','Galeri','Ada 1 data baru pada Galeri yang sudah di setujui',$insert_data['kode_galeri']);
                }
                else {
                  foreach ($group_ntf as $id_group) {
                     notifikasi('1','10',$_POST['id_prov'],$_POST['loket_upt'],$id_group,'Galeri','Ada 1 data baru pada Galeri',$insert_data['kode_galeri']);
                  }
                }
                echo "OK";
          }
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="edit" and $id_parameter!=""){
    $edit  = $gen_model->GetOneRow("tr_galeri",array('kode_galeri'=>$id_parameter)); 
    foreach($edit as $key=>$val){
                  $key=strtolower($key);
                  $$key=$val;
    }
    $data = array(
      'kode_galeri'=>$kode_galeri,
      'created_date'=>$gen_controller->get_date_indonesia($created_date),
      'kegiatan'=>$kegiatan,
      'deskripsi'=>$deskripsi,
      'lampiran'=>$basepath.'assets/attachment/sosialisasi_bimtek/galeri/'.$lampiran,
      'kode_upt'=>$kode_upt,
      'id_prov'=>$id_prov,
      'keterangan'=>$keterangan
    );
    echo json_encode($data); 
}
else if($act=="update"){
      //File
    $tmp              = $_FILES["lampiran"]["tmp_name"];
    $origin_file_name = $_FILES['lampiran']['name'];
    if(!empty($origin_file_name)){
      $ext              = strtolower(pathinfo($_FILES["lampiran"]["name"])['extension']);
      $file_name        = date("ymdhis")."_".rand(1000,9999).".".$ext;
      $path             = "assets/attachment/sosialisasi_bimtek/galeri/";
    }

    if(!empty($_SESSION['kode_user'])){
      //Proses
      $update_data = array();
      $update_data['kode_galeri']             = get($_POST['kode_galeri']);
      $update_data['kegiatan']                = get($_POST['kegiatan']);
      $update_data['deskripsi']               = get($_POST['deskripsi']);
      $update_data['keterangan']              = get($_POST['keterangan']);
      $update_data['kode_upt']                = get($_POST['loket_upt']);
      $update_data['id_prov']                 = get($_POST['id_prov']);
      $update_data['last_update']             = $date_now_indo_full;
      $update_data['last_update_by']          = $_SESSION['kode_user'];
      if(!empty($origin_file_name)){
         $update_data['lampiran']              = $file_name;
      }

      

       if($_POST['id_prov']=="All"){
          $update_data['id_prov'] ="";
        }
        if($_POST['loket_upt']=="All"){
          $update_data['kode_upt']="";
       }

       //Paramater
      $where_data = array();
      $where_data['kode_galeri']       = $_POST['kode_galeri'];

       //Validation
      if(!empty($_POST['kode_galeri'])){
             if($gen_model->Update('tr_galeri',$update_data,$where_data)=="OK"){
                if(!empty($origin_file_name)){
                    //Delete File
                    $old_file = $gen_model->GetOne('lampiran','tr_galeri',$where_data);
                    $gen_controller->delete_file($path,$old_file);

                    //Upload File
                    $gen_controller->upload_file($tmp,$path,$file_name);
                  } 
                    
              echo "OK";
          }
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="delete"){
    if(!empty($_SESSION['kode_user'])){
       $path             = "assets/attachment/sosialisasi_bimtek/galeri/";
       $update_data['status']              = "2"; //Delete

       //Paramater
      $where_data = array();
      $where_data['kode_galeri']       = $_POST['kode_galeri'];


       //Validation
      if(!empty($_POST['kode_galeri'])){
            $old_file = $gen_model->GetOne('lampiran','tr_galeri',$where_data);
            $gen_controller->delete_file($path,$old_file); 
            echo $gen_model->Update('tr_galeri',$update_data,$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="verifikasi"){
    if(!empty($_SESSION['kode_user'])){
       $db->Execute("update tr_galeri set status='3' where kode_galeri in(".$_REQUEST['kode'].") ");
       $total = $_REQUEST['total'];
        if(!empty($total)){
        //Notification
          if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
            $kode=str_replace("'","",$_REQUEST['kode']);
            notifikasi('4','10',$_SESSION['upt_provinsi'],$_SESSION['kode_upt'],'grp_171026194411_1142','Galeri','Ada '.$total.' data baru pada Galeri yang sudah di setujui',$kode);
          }
        }
       echo "OK";
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="reject"){
    if(!empty($_SESSION['kode_user'])){
       $db->Execute("update tr_galeri set status='1',ket_sts_apr_reject='".$_REQUEST['ket']."' where kode_galeri in(".$_REQUEST['kode'].") ");
        $total = $_REQUEST['total'];
        if(!empty($total)){
        //Notification
          if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
            $kode=str_replace("'","",$_REQUEST['kode']);
            notifikasi('3','10',$_SESSION['upt_provinsi'],$_SESSION['kode_upt'],'grp_171026194411_1143','Galeri','Ada '.$total.' data baru pada Galeri yang sudah di reject',$kode);
          }
        }
       echo "OK";
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_waiting"){
  $prov = $_REQUEST['id_prov'];
  $upt = $_REQUEST['upt'];
  $aColumns = array('gl.kode_galeri','gl.created_date','gl.status','gl.kegiatan','gl.lampiran','gl.kode_galeri'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_sosialisasi_bimtek->getGaleriWaiting($sWhere,$sOrder,$sLimit,$prov,$upt);
  $rResultFilterTotal   = $md_sosialisasi_bimtek->getCountGaleriWaiting($sWhere,$prov,$upt);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
     $wilayah ="";
    if(!empty($aRow['id_prov'])){
     $wilayah = $gen_model->GetOne("id_map","ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    }
    $param_id = $aRow['kode_galeri'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_galeri(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
    
  
    $checkbox="";
    if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
         if($aRow['status']!="4"){
             $checkbox = '<input type="checkbox" value="'.$param_id.'" name="kode_galeri">';
          }
    }
   
    $sts = "";
    if($aRow['status']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }
      //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                 $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_galeri(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                 $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_galeri(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
             }
        }
        else {
             $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_galeri(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_galeri(\''.base64_encode('kode_galeri').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>'; 
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_galeri(\''.base64_encode('kode_galeri').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
             }
        }
        else {
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_galeri(\''.base64_encode('kode_galeri').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }
     $lampiran ="";
    if(!empty($aRow['lampiran'])){
       $lampiran = '<center><a   title="Download File" style="cursor:pointer;color:blue" href="'.$basepath.'assets/attachment/sosialisasi_bimtek/galeri/'.$aRow['lampiran'].'" download><i class="fa fa-download fa-2x"></i></a></center>';
    }   
    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($checkbox,$gen_controller->get_date_indonesia($aRow['created_date']),$sts,$aRow['kegiatan'],$lampiran,"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest"){
   $aColumns = array('gl.created_date','pv.nama_prov','upt.nama_upt','gl.kegiatan','gl.lampiran','gl.deskripsi','gl.keterangan','gl.kode_galeri'); //Kolom Pada Tabel


    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_sosialisasi_bimtek->getGaleri($sWhere,$sOrder,$sLimit);
  $rResultFilterTotal   = $md_sosialisasi_bimtek->getCountGaleri($sWhere);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $wilayah ="";
    if(!empty($aRow['id_prov'])){
     $wilayah = $gen_model->GetOne("id_map","ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    }
    $param_id = $aRow['kode_galeri'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_galeri_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
              $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_galeri_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_galeri_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
             }
        }
        else {
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_galeri_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
              $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_galeri_all(\''.base64_encode('kode_galeri').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
              $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_galeri_all(\''.base64_encode('kode_galeri').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                
             }
        }
        else {
              $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_galeri_all(\''.base64_encode('kode_galeri').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            
        }
    }
    $lampiran ="";
    if(!empty($aRow['lampiran'])){
       $lampiran = '<center><a   title="Download File" style="cursor:pointer;color:blue" href="'.$basepath.'assets/attachment/sosialisasi_bimtek/galeri/'.$aRow['lampiran'].'" download><i class="fa fa-download fa-2x"></i></a></center>';
    }
    $nama_prov = $aRow['nama_prov'];
    $nama_upt  = $aRow['nama_upt'];
    if(empty($nama_prov)){
      $nama_prov = "<center>Semua Provinsi</center>";
    }  
    if(empty($nama_upt)){
      $nama_upt = "<center>Semua UPT</center>";
    }   
    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$nama_prov,$nama_upt,$aRow['kegiatan'],$lampiran,$aRow['deskripsi'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_filter"){
  $param = $_REQUEST['param'];
  $aColumns = array('gl.created_date','pv.nama_prov','upt.nama_upt','gl.kegiatan','gl.lampiran','gl.deskripsi','gl.keterangan','gl.kode_galeri'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_sosialisasi_bimtek->getGaleriFilter($sWhere,$sOrder,$sLimit,$param);
  $rResultFilterTotal   = $md_sosialisasi_bimtek->getCountGaleriFilter($sWhere,$param);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

   while($aRow = $rResult->FetchRow()){
    $wilayah ="";
    if(!empty($aRow['id_prov'])){
     $wilayah = $gen_model->GetOne("id_map","ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    }
    $param_id = $aRow['kode_galeri'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_galeri_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_galeri_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
               $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_galeri_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
             }
        }
        else {
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_galeri_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>'; 
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_galeri_all(\''.base64_encode('kode_galeri').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_galeri_all(\''.base64_encode('kode_galeri').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
             }
        }
        else {
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_galeri_all(\''.base64_encode('kode_galeri').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }
    $lampiran ="";
    if(!empty($aRow['lampiran'])){
       $lampiran = '<center><a   title="Download File" style="cursor:pointer;color:blue" href="'.$basepath.'assets/attachment/sosialisasi_bimtek/galeri/'.$aRow['lampiran'].'" download><i class="fa fa-download fa-2x"></i></a></center>';
    }
    $nama_prov = $aRow['nama_prov'];
    $nama_upt  = $aRow['nama_upt'];
    if(empty($nama_prov)){
      $nama_prov = "<center>Semua Provinsi</center>";
    }  
    if(empty($nama_upt)){
      $nama_upt = "<center>Semua UPT</center>";
    }   
    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$nama_prov,$nama_upt,$aRow['kegiatan'],$lampiran,"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_provinsi_filter"){
  $param = $_REQUEST['param'];
 $aColumns = array('gl.created_date','pv.nama_prov','upt.nama_upt','gl.kegiatan','gl.lampiran','gl.deskripsi','gl.keterangan','gl.kode_galeri'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_sosialisasi_bimtek->getGaleriFilter($sWhere,$sOrder,$sLimit,$param);
  $rResultFilterTotal   = $md_sosialisasi_bimtek->getCountGaleriFilter($sWhere,$param);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

 while($aRow = $rResult->FetchRow()){
    $wilayah ="";
    if(!empty($aRow['id_prov'])){
     $wilayah = $gen_model->GetOne("id_map","ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    }
    $param_id = $aRow['kode_galeri'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_galeri(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

      //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));

    $edit="";
    $delete="";
     if($akses_tab['fua_edit']=="1"){ 
        //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['kode_upt']==$aRow['kode_upt']){
                if($_SESSION['group_user']=="grp_171026194411_1143"){
                        $edit = '&nbsp; <button title="Edit"  type="button"  onclick="do_edit_galeri(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';    
                }
                else if($aRow['created_by']==$_SESSION['kode_user']){
                     $edit = '&nbsp; <button title="Edit"  type="button"  onclick="do_edit_galeri(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
                 }
             }
          }
          else {
             $edit = '&nbsp; <button title="Edit"  type="button"  onclick="do_edit_galeri(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
          }
      }
      if($akses_tab['fua_delete']=="1"){ 
          //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['kode_upt']==$aRow['kode_upt']){
                if($_SESSION['group_user']=="grp_171026194411_1143"){
                   $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_galeri(\''.base64_encode('kode_galeri').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';         
                }
                else if($aRow['created_by']==$_SESSION['kode_user']){
                  $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_galeri(\''.base64_encode('kode_galeri').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                 }
             }
          }
          else {
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_galeri(\''.base64_encode('kode_galeri').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
          }
      }
    $lampiran ="";
    if(!empty($aRow['lampiran'])){
       $lampiran = '<center><a   title="Download File" style="cursor:pointer;color:blue" href="'.$basepath.'assets/attachment/sosialisasi_bimtek/galeri/'.$aRow['lampiran'].'" download><i class="fa fa-download fa-2x"></i></a></center>';
    }
    $edit_delete = $detail.$edit.$delete;
    $nama_prov = $aRow['nama_prov'];
    $nama_upt  = $aRow['nama_upt'];
    if(empty($nama_prov)){
      $nama_prov = "<center>Semua Provinsi</center>";
    }  
    if(empty($nama_upt)){
      $nama_upt = "<center>Semua UPT</center>";
    }   
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$nama_prov,$nama_upt,$aRow['kegiatan'],$lampiran,$aRow['deskripsi'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_provinsi"){
  $prov = $_REQUEST['id_prov'];
  $upt  = $_REQUEST['upt'];
  $aColumns = array('gl.created_date','pv.nama_prov','upt.nama_upt','gl.kegiatan','gl.lampiran','gl.deskripsi','gl.keterangan','gl.kode_galeri'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_sosialisasi_bimtek->getGaleri($sWhere,$sOrder,$sLimit,$prov,$upt);
  $rResultFilterTotal   = $md_sosialisasi_bimtek->getCountGaleri($sWhere,$prov,$upt);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $wilayah ="";
    if(!empty($aRow['id_prov'])){
     $wilayah = $gen_model->GetOne("id_map","ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    }
    $param_id = $aRow['kode_galeri'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_galeri(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

      //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));


    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
        //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['kode_upt']==$aRow['kode_upt']){
                if($_SESSION['group_user']=="grp_171026194411_1143"){
                    $edit = '&nbsp; <button title="Edit"  type="button"  onclick="do_edit_galeri(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';         
                }
                else if($aRow['created_by']==$_SESSION['kode_user']){
                    $edit = '&nbsp; <button title="Edit"  type="button"  onclick="do_edit_galeri(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
                 }
             }
          }
          else {
            $edit = '&nbsp; <button title="Edit"  type="button"  onclick="do_edit_galeri(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
          }
    }
    if($akses_tab['fua_delete']=="1"){ 
        //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['kode_upt']==$aRow['kode_upt']){
                if($_SESSION['group_user']=="grp_171026194411_1143"){
                      $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_galeri(\''.base64_encode('kode_galeri').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';          
                }
                else if($aRow['created_by']==$_SESSION['kode_user']){
                     $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_galeri(\''.base64_encode('kode_galeri').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                 }
             }
          }
          else {
               $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_galeri(\''.base64_encode('kode_galeri').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
          }
    }
    $lampiran ="";
    if(!empty($aRow['lampiran'])){
       $lampiran = '<center><a   title="Download File" style="cursor:pointer;color:blue" href="'.$basepath.'assets/attachment/sosialisasi_bimtek/galeri/'.$aRow['lampiran'].'" download><i class="fa fa-download fa-2x"></i></a></center>';
    }
    $nama_prov = $aRow['nama_prov'];
    $nama_upt  = $aRow['nama_upt'];
    if(empty($nama_prov)){
      $nama_prov = "<center>Semua Provinsi</center>";
    }  
    if(empty($nama_upt)){
      $nama_upt = "<center>Semua UPT</center>";
    }   
    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$nama_prov,$nama_upt,$aRow['kegiatan'],$lampiran,$aRow['deskripsi'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else {
  $gen_controller->response_code(http_response_code());
}
?>