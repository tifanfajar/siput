<?php 
//General Controller
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();

//Model Pelayanan
include "model/pelayanan.php";
$md_pelayanan  = new pelayanan();


//Model User
include "model/user.php";
$md_user      = new user();
session_start();

$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}

$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //Check Session
  $gen_controller->check_session();

  //View
  include "view/header.php";
  include "view/pelayanan_filter_import.php";
  include "view/pelayanan.php";
  include "view/footer.php";
}
else if($act=="form_all"){
   include "view/default_style.php"; 
   $activity  = $_REQUEST['activity'];
   $form_url  = $_REQUEST['form_url'];
   if($form_url=="Form_Pelayanan_Loket_ALL"){
      include "view/ajax_form/pelayanan_form_data_pengunjung_all.php";
   }
   else if($form_url=="Form_SPP_ALL"){
      include "view/ajax_form/pelayanan_form_data_spp_all.php";
   }
   else if($form_url=="Form_ISR_ALL"){
      include "view/ajax_form/pelayanan_form_data_isr_all.php";
   }
   else if($form_url=="Form_Revoke_ALL"){
      include "view/ajax_form/pelayanan_form_data_revoke_all.php";
   }
   else if($form_url=="Form_Piutang_ALL"){
      include "view/ajax_form/pelayanan_form_data_piutang_all.php";
   }
   else if($form_url=="Form_Unar_ALL"){
      include "view/ajax_form/pelayanan_form_data_unar_all.php";
   }
}
else if($act=="form"){
   include "view/default_style.php"; 
   $form_url  = $_REQUEST['form_url'];
   $map_id    = $_REQUEST['map_id'];
   $upt       = $_REQUEST['upt'];
   $activity  = $_REQUEST['activity'];
   $wilayah   = $gen_model->GetOneRow("ms_provinsi",array('id_map'=>$map_id)); 
   if($form_url=="Form_Pelayanan_Loket"){
      include "view/ajax_form/pelayanan_form_data_pengunjung.php";
   }
   else if($form_url=="Form_SPP"){
      include "view/ajax_form/pelayanan_form_data_spp.php";
   }  
   else if($form_url=="Form_ISR"){
      include "view/ajax_form/pelayanan_form_data_isr.php";
   } 
   else if($form_url=="Form_Revoke"){
      include "view/ajax_form/pelayanan_form_data_revoke.php";
   } 
   else if($form_url=="Form_Piutang"){
      include "view/ajax_form/pelayanan_form_data_piutang.php";
   }
   else if($form_url=="Form_Unar"){
      include "view/ajax_form/pelayanan_form_data_unar.php";
   } 
   else if($form_url=="Form_Company_Loket"){
      include "view/ajax_form/company_loket_form_data.php";
   }
}
else if($act=="table"){
   $upt     = $_REQUEST['upt'];
   $div_id  = $_REQUEST['div_id'];
   $map_id  = $_REQUEST['map_id'];
   $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_map'=>$map_id)); 
   if($div_id=="pelayanan_table_data_utama"){
      include "view/ajax_table/pelayanan_table_data_pengunjung.php";
   }
   else if($div_id=="spp_table_data_utama"){
      include "view/ajax_table/pelayanan_table_data_spp.php";
   }  
   else if($div_id=="isr_table_data_utama"){
      include "view/ajax_table/pelayanan_table_data_isr.php";
   }  
   else if($div_id=="revoke_table_data_utama"){
      include "view/ajax_table/pelayanan_table_data_revoke.php";
   } 
   else if($div_id=="piutang_table_data_utama"){
      include "view/ajax_table/pelayanan_table_data_piutang.php";
   } 
   else if($div_id=="unar_table_data_utama"){
      include "view/ajax_table/pelayanan_table_data_unar.php";
   }
}
else if($act=="company_loket"){
  include "view/ajax_table/company_loket.php";
}
else if($act=="company_loket_form_data"){
   $activity  = $_REQUEST['activity'];
  include "view/ajax_form/company_loket_form_data.php";
}
else if($act=="pilih_upt"){
   $map_id  = $_REQUEST['map_id'];
   $div_id  = $_REQUEST['div_id'];
   $wilayah = $gen_model->GetOneRow("ms_provinsi",array('id_map'=>$map_id));  
   $total_upt = $gen_model->GetOne(' count(*) as total ','ms_upt',array('id_prov'=>$wilayah['id_prov']));
   if($total_upt==0){
    echo '<label class="loc-lbl">UPT '.$wilayah['nama_prov'].'</label>';
    echo '<center>Tidak ada UPT di provinsi ini</center>';
   }
   else if($total_upt==1){
     echo "<script>";
     $upt = $gen_model->GetOne('kode_upt','ms_upt',array('id_prov'=>$wilayah['id_prov']));
      if($div_id=="pelayanan_table_data_utama"){ 
          echo " detail_loket_pelayanan('".$map_id."','".$upt."') ";
       }
       else if($div_id=="spp_table_data_utama"){
         echo " detail_spp('".$map_id."','".$upt."') ";
       }  
       else if($div_id=="isr_table_data_utama"){
         echo " detail_isr('".$map_id."','".$upt."') ";
       }  
       else if($div_id=="revoke_table_data_utama"){
         echo " detail_revoke('".$map_id."','".$upt."') ";
       } 
       else if($div_id=="piutang_table_data_utama"){
         echo " detail_piutang('".$map_id."','".$upt."') ";
       } 
       else if($div_id=="unar_table_data_utama"){
         echo " detail_unar('".$map_id."','".$upt."') ";
       }
       echo "</script>";
   }  
   else { ?>
   <div style="background-color:white">
   <label class="loc-lbl">UPT <?php echo $wilayah['nama_prov'] ?></label>
     <div class="row">
        <div class="col-md-12">
           <select class="form_control" onchange="get_detail()" id="data_upt" style="width: 100%;">
              <?php 
                 $data_upt = $gen_model->GetWhere('ms_upt',array('id_prov'=>$wilayah['id_prov']));
                 echo '<option value="">Pilih UPT</option>';
                  while($list = $data_upt->FetchRow()){
                    foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
                              }  
                ?>
              <option value="<?php echo $kode_upt ?>"><?php echo $nama_upt ?></option>
              <?php }  ?>
           </select>
        </div>
     </div>
   </div>
   <script type="text/javascript">
     function get_detail(){
        var  map_id = "<?php echo $map_id ?>";
        var  upt    = $("#data_upt").val();
        if(upt){
            <?php 
                if($div_id=="pelayanan_table_data_utama"){
                    echo " detail_loket_pelayanan(map_id,upt) ";
                 }
                 else if($div_id=="spp_table_data_utama"){
                   echo " detail_spp(map_id,upt) ";
                 }  
                 else if($div_id=="isr_table_data_utama"){
                   echo " detail_isr(map_id,upt) ";
                 }  
                 else if($div_id=="revoke_table_data_utama"){
                   echo " detail_revoke(map_id,upt) ";
                 } 
                 else if($div_id=="piutang_table_data_utama"){
                   echo " detail_piutang(map_id,upt) ";
                 } 
                 else if($div_id=="unar_table_data_utama"){
                   echo " detail_unar(map_id,upt) ";
                 }
            ?>
        }
     }
   </script>
   <?php
   }
}
else if($act=="get_group_perizinan"){
    echo "<option value=''>Pilih Group Perizinan</option>";

    $get_grp  = $gen_model->GetWhere("ms_sub_group_layanan",array('id_jenis'=>$_POST['jenis'],'status'=>'1')); 
    while($list = $get_grp->FetchRow()){
      foreach($list as $key=>$val){
                        $key=strtolower($key);
                        $$key=$val;
      }  
      echo "<option value='".$id_sub_group_layanan."'>".$sub_group_layanan."</option>";
    }
}
else {
  $gen_controller->response_code(http_response_code());
}
?>