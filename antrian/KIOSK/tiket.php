<?php 
date_default_timezone_set("Asia/Jakarta");
function get_hari($tanggal){  //Only format yyyy-mm-dd
    $day = date('D', strtotime($tanggal));
    $dayList = array(
      'Sun' => 'Minggu',
      'Mon' => 'Senin',
      'Tue' => 'Selasa',
      'Wed' => 'Rabu',
      'Thu' => 'Kamis',
      'Fri' => 'Jumat',
      'Sat' => 'Sabtu'
    );
    return $dayList[$day]; //output data => Day Name
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Antrian</title>
  <style>
  @font-face {
      font-family: myFirstFont;
      src: url(Roboto-Medium.ttf);
    }
    body{
      padding: 10px;
      font-family:myFirstFont;
    }
    .logo{
      display: block;
      margin:0 auto;
      width: 15%;
    }
    .main-title{
      display: block;
      text-align: center;
      font-weight: bold;
      margin-top: 0px;
      margin-bottom:10px;
    }
    .cp{
      display: block;
      width: 100%;
      height: 2px;
      background-color: #000;
    }
    .sub-title{
      display: block;
      text-align: center;
      font-weight: bold;
      margin-top: 5px;
      margin-bottom: 0px;
      font-size: 10px;
    }
    .dt{
      text-align: center;
      display: block;
      margin-top: 0px;
      font-size: 10px;
    }
    .fl{
      text-align: center;
      display: block;
      font-size: 20px;
      margin-top: 10px;
    }
    .fl2{
      text-align: center;
      display: block;
      font-size: 20px;
      margin-top: 5px;
    }
    .num{
      display: block;
      text-align: center;
      font-weight: bold;
      font-size: 50px;
      margin-top: 0px;
      margin-bottom: 0px;
    }
    .sisa{
      text-align: center;
      display: block;
      font-size: 15px;
      margin-top: 0px;
    }
    small{
      display: block;
      text-align: center;
      margin-top: 10px;
      font-size: 10px;
    }
    @page 
    {
      size: auto;   /* auto is the initial value */
      margin: 0mm;  /* this affects the margin in the printer settings */
    }
  </style>
</head>
<body>
  <img class="logo" src="assets/images/icon_struk.png" alt="">
  <h2 class="main-title">Antrian PKC</h2>
  <div class="cp"></div>
  <h3 class="sub-title">SELAMAT DATANG DI Antrian PKC Pasar Rebo</h3>
  <span class="dt"><?php echo get_hari(date("Y-m-d")) ?>,<?php echo date("d-m-Y") ?> | <?php echo date("H:i") ?></span>
  <h1 class="num"><?php echo $_REQUEST['urut'] ?></h1>
  <span class="fl2">LOKET <?php echo $_REQUEST['layanan'] ?></span>
  <span class="sisa">Sisa Antrian <?php echo $_REQUEST['sisa'] ?></span>
  <small>SILAHKAN MENUNGGU DENGAN TERTIB, TERIMA KASIH</small>
</body>
</html>
<script>
window.print();
</script>