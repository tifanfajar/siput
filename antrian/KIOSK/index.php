<?php include '../config.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>KIOSK - KOMINFO</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="assets/images/icon.png">
	<link rel="stylesheet" href="assets/css/index.css">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="assets/js/sweetalert2/dist/sweetalert2.min.css">

</head>
<body class="dash-app" onload="startTime()">

	<video autoplay loop id="video-background" muted>
		<source src="assets/images/abstract_009.mp4" type="video/mp4">
	</video>

	<!-- Header Logo & Judul App -->
	<div class="container">
	  <div class="row">
	  	
	    <div class="col"></div>

	    <div class="col-6 main-cnt-judul">
	     <img src="assets/images/logo.png" class="logo-app" alt="">
	     <span class="judul-app">Sistem Aplikasi Antrian Loket</span>
	     <small class="sub-judul-app">Kementerian Komunikasi dan Informatika Republik Indonesia</small>
	    </div>

	    <div class="col"></div>
	  </div>
	</div>

	<!-- Start Modal Login -->
		<div id="myModalAkses" class="modal fade" role="dialog">
		  <div class="modal-dialog" style="width: 350px;">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header" style="display:inline;background-color: #337ab7;color: white;">
				<h4 class="modal-title">Akses KIOSK</h4>
			  </div>
			  <form method="POST" id="form_akses">
				 <div class="modal-body">
						<div class="row">
							<div class="col-sm-12 col-md-12">
								Masukan Password<br/>
								<input type="password" name="pwd" class="form-control" id="pwd">
							</div>
						</div>
				  </div>
				  <div class="modal-footer">
					<button type="submit"  id="keluar_btn" class="btn btn-primary" >Login</button>
				  </div>
			  </form>
			</div>

		  </div>
		</div>
	<!-- End Modal Akses Login -->

	<!-- Button App Dash -->
	<div class="container" style="margin-top: 3%;">
	  <div class="row justify-content-center align-items-center h-100">

	  	<?php 
	  		$data_ly=mysqli_query($con,"select * from ms_layanan where STATUS='1' order by NAMA_LAYANAN  ASC");
	  		while($ly=mysqli_fetch_object($data_ly))
	  		{ 	?>
	    <div data-tilt class="col-md-3">
	    	<a href="javascript:void(0);" onclick="get_antrian('<?php echo $ly->ID_LAYANAN ?>')" class="btn-dashboard" >
	    		<div class="o-ring"></div>
	    		<!-- <div class="icon-btn"><i class="fa fa-id-badge"></i></div> -->
	    		<span class="btn-name"><?php echo $ly->NAMA_LAYANAN ?></span>
	    		<!-- <small class="btn-des">Loket pemeriksaan dan pengecekan</small> -->
	    		<div class="img-circle2" style="background-image: url('<?php echo $basepath ?>assets/images/<?php echo $ly->IMAGE ?>');"></div>
	    	</a>
	    </div>
	    <?php } ?>
	  </div>
	</div>

	<div class="running-text-cnt">
		<div class="cnt-clock">
			<div id="txt"></div>
		</div>
		<?php 
			$running_text = mysqli_query($con,"select * from app_running_text where STATUS='AKTIF'");
			$rn = "";
			$separator = "&nbsp; <img style='width:25px;height:25px' src='assets/images/logo.png'> &nbsp;";
			while ($data_rn = mysqli_fetch_object($running_text)){
				$rn .= $data_rn->TEXT.$separator;
			}
		?>
		<marquee behavior="" direction="left"><?php echo $rn ?></marquee>
	</div>

	<script type="text/javascript">
		var kouta_full  	= "Telah melebihi kouta hari ini";
		var not_avail   	= "Layanan ini belum tersedia";
		var not_avail_time_awal  = "Layanan ini belum di mulai";
		var not_avail_time  = "Layanan ini telah melebihi batas waktu";
		var sorry  	    	= "Maaf";
		var thanks 	    	= "Terima Kasih";
		var sukses      	= "Silahkan ambil nomor antrian anda <br/> dan terima kasih telah menunggu.";
		function get_antrian(id){
					$.ajax({
				        url: 'ajax/antrian.php',
				        type: 'POST',
				        dataType: 'JSON',
				        data: 'act=ambil_tiket&id_layanan='+window.btoa(id),
				        success: function(mydata) {
							if(mydata.error=="NOT_PRINT"){
								swal(sorry,kouta_full,"error");
							}
							else if(mydata.error=="NOT_AVAIBLE"){
								swal(sorry,not_avail,"error");
							}
							else if(mydata.error=="NOT_JAM_AWAL"){
								swal(sorry,not_avail_time_awal,"error");
							}
							else if(mydata.error=="NOT_JAM_AKHIR"){
								swal(sorry,not_avail_time,"error");
							}
							else {
								$('#print_src').attr('src', "tiket.php?urut="+mydata.no_urut+"&sisa="+mydata.sisa+"&layanan="+mydata.layanan);
							}
				        	 
				        }
				    });
		}
	</script>
	<iframe id="print_src" style="width: 0; height: 0; border: 0; border: none; position: absolute;"></iframe>

	<script type="text/javascript" src="assets/js/jquery.js"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/time.js"></script>
	<script src="assets/js/sweetalert2/dist/sweetalert2.min.js"></script>
	<script type="text/javascript">
		$("#form_akses").on("submit", function (event) {
			event.preventDefault();
				do_akses();
		});
		function do_akses(){
			 $.ajax({
					url: 'ajax/login.php', 
					type: 'POST',
					data: new FormData($('#form_akses')[0]),  // Form ID
					processData: false,
					contentType: false,
					success: function(data) {
						if(data=="OK") {
							swal({
								title: 'Sukses',
								html: 'Berhasil Login',
								type: 'success',
								showCancelButton: false,
								showLoaderOnConfirm: false,
							  }).then(function() {
									  location.reload();
								});
						} 
						else
						{
							swal({
								title: 'Error',
								html: data,
								type: 'error',
								showCancelButton: false,
								showLoaderOnConfirm: false,
							  });
						}
					}
				});
	}	
		<?php  
		session_start();
		if(empty($_SESSION['pwd_kiosk'])){ ?>
			    $('#myModalAkses').modal({
			  	  backdrop: 'static',
			   	  keyboard: false
				})
		<?php } ?>
	</script>
</body>
</html>