<style>
.urut {
  background-color: lightgrey;
  width: 250px;
  padding: 25px;
  margin: 25px;
  color:black;
}
.loket {
  background-color: black;
  width: 200px;
  padding: 10px;
  margin: 25px;
  color:white;
}
</style>
<?php
echo "<title>Speaker Antrian</title>";
error_reporting("null");
date_default_timezone_set("Asia/Jakarta");
if(mysqli_connect_errno()){
echo "Database did not connect";
exit();
}
 
function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim(penyebut($nilai));
		} else {
			$hasil = trim(penyebut($nilai));
		}     		
		return $hasil;
}


function penyebut($nilai) {
		$nilai = ($nilai);	
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = penyebut($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
		}  
			
		return $temp;
}


$lkt = "4";
$numbers = "10";

$letters = wordwrap($_REQUEST['abjad'],1,'_',true );

$terbilang_loket =  str_replace(' ','_',terbilang($lkt));
$terbilang_urut  =  str_replace(' ','_',terbilang($numbers));


$panggil   =  "DING_Nomor-Antrian_".$letters."_".$terbilang_urut."_Loket_".$terbilang_loket;


   echo "<br/><center>
				<div class='urut'><span style='font-size:100px'>".$_REQUEST['abjad'].$numbers."</span>
					<div class='loket'><span style='font-size:30px'>LOKET : ".$lkt."</span></div>
				</div>
		</center>";
$var=explode('_',$panggil);
$order_sound = "";
foreach($var as $row) {
    $order_sound .= '"'.$basepath_speaker.'sound/'.$row.'.wav",';
}
$order_sound = rtrim($order_sound, ',');
?>

<br/>
<audio id="player"></audio>
<script type="text/javascript">
	var audioFiles = [
    	<?php echo $order_sound ?>
	];
	    
	function preloadAudio(url) {
	    var audio = new Audio();
	    // once this file loads, it will call loadedAudio()
	    // the file will be kept by the browser as cache
	    audio.addEventListener('canplaythrough', loadedAudio, false);
	    audio.src = url;
	}
	    
	var loaded = 0;
	function loadedAudio() {
	    // this will be called every time an audio file is loaded
	    // we keep track of the loaded files vs the requested files
	    loaded++;
	    if (loaded == audioFiles.length){
	    	// all have loaded
	    	init();
	    }
	}
	    
	var player = document.getElementById('player');
	function play(index) {
	    player.src = audioFiles[index];
	    player.play();
	}
	    
	function init() {
	    // do your stuff here, audio has been loaded
	    // for example, play all files one after the other
	    var i = 0;
	    // once the player ends, play the next one
	    player.onended = function() {
	    	i++;
	        if (i >= audioFiles.length) {
	            // end 
	            return;
	        }
	    	play(i);
	    };
	    // play the first file
	    play(i);
	}
	    
	// we start preloading all the audio files
	for (var i in audioFiles) {
	    preloadAudio(audioFiles[i]);
	}
</script>