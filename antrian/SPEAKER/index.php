<style>
.urut {
  background-color: lightgrey;
  width: 250px;
  padding: 25px;
  margin: 25px;
  color:black;
}
.loket {
  background-color: black;
  width: 200px;
  padding: 10px;
  margin: 25px;
  color:white;
}
</style>
<title>Speaker Antrian - KOMINFO</title>
<link rel="icon" href="images/icon.png">
<?php
error_reporting("null");
date_default_timezone_set("Asia/Jakarta");
include '../config.php'; 
$table = "temp_panggilan";

 
function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim(penyebut($nilai));
		} else {
			$hasil = trim(penyebut($nilai));
		}     		
		return $hasil;
}


function penyebut($nilai) {
		$nilai = ($nilai);	
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = penyebut($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
		}  
			
		return $temp;
}


$qry  = "select * from ".$table."  where  status='0' and ID_TEMP_PANGGILAN like '".date('ymd')."%' order by ID_TEMP_PANGGILAN ASC";  
$data = mysqli_fetch_object(mysqli_query($con,$qry));

if(empty($data)){
	echo   "<br><center>
				<div class='urut'>
					<span style='font-size:20px'><b>Tidak ada panggilan antrian</b></span>
	   			</div>	
	   			</center>";
	echo "<script>setInterval(function() { location.reload(); }, 2000);</script>";
	die();
}

$str_pg  = (explode(";",$data->PANGILAN));

$numbers = preg_replace('/[^0-9]/', '', $str_pg[1]);
$letters = preg_replace('/[^a-zA-Z]/', '', $str_pg[1]);
$letters_sound = wordwrap($letters,1,'_',true);

$lkt  = $str_pg[0];

$terbilang_loket =  str_replace(' ','_',terbilang($lkt));
$terbilang_urut  =  str_replace(' ','_',terbilang($numbers));

$spk = mysqli_fetch_object(mysqli_query($con,"select spk.SPEAKER 
		from data_petugas_loket as lkt 
			inner join tr_loket_speaker as spk 
					on spk.ID_DATA_PETUGAS_LOKET=lkt.ID_DATA_PETUGAS_LOKET
				where lkt.loket='".$lkt."'"));
$panggil_loket = "";



if(!empty($spk)){
	$url_data = $basepath_speaker."sound/".$spk->SPEAKER.".wav";
	if(file_get_contents($url_data)) {
	 	$panggil_loket = $spk->SPEAKER;
	 }
	 else{
	 	$panggil_loket = "Loket_".$terbilang_loket;
	 }
}
else {
	$panggil_loket = "Loket_".$terbilang_loket;
}

$panggil   =  "DING_Nomor-Antrian_".$letters_sound."_".$terbilang_urut."_".$panggil_loket;
if(!empty($data)){
	   echo "<br/><center>
	   				<div class='urut'><span style='font-size:100px'>".$letters.$numbers."</span>
	   					<div class='loket'><span style='font-size:30px'>LOKET : ".$lkt."</span></div>
	   				</div>
	   		</center>";
}
else {
	echo   "<br><center>
				<div class='urut'>
					<span style='font-size:20px'><b>Tidak ada panggilan antrian</b></span>
	   			</div>	
	   			</center>";
	echo "<script>setInterval(function() { location.reload(); }, 1000);</script>";
}


//mysqli_query($con,"delete from ".$table."  where ID_TEMP_PANGGILAN='".$data->ID_TEMP_PANGGILAN."' ");


$var=explode('_',$panggil);
$order_sound = "";
foreach($var as $row) {
    $order_sound .= '"'.$basepath_speaker.'sound/'.$row.'.wav",';
}
$order_sound = rtrim($order_sound, ',');
?>

<br/>
<audio id="player"></audio>
<script src="js/jquery.js"></script>
<script type="text/javascript">
	var audioFiles = [
    	<?php echo $order_sound ?>
	];
	    
	function preloadAudio(url) {
	    var audio = new Audio();
	    // once this file loads, it will call loadedAudio()
	    // the file will be kept by the browser as cache
	    audio.addEventListener('canplaythrough', loadedAudio, false);
	    audio.src = url;
	}
	    
	var loaded = 0;
	function loadedAudio() {
	    // this will be called every time an audio file is loaded
	    // we keep track of the loaded files vs the requested files
	    loaded++;
	    if (loaded == audioFiles.length){
	    	// all have loaded
	    	init();
	    }
	}
	    
	var player = document.getElementById('player');
	function play(index) {
	    player.src = audioFiles[index];
	    player.play();
	}
	    
	function init() {
	    // do your stuff here, audio has been loaded
	    // for example, play all files one after the other
	    var i = 0;
	    // once the player ends, play the next one
	    player.onended = function() {
	    	i++;
	        if (i >= audioFiles.length) {
	            // end 
	            //return;

	             $.ajax({
					url: 'ajax.php?id_pg=<?php echo $data->ID_PANGGILAN ?>', 
					type: 'POST',
					processData: false,
					contentType: false,
					success: function(data) {
						location.reload();
					}
				});

	            
	        }
	    	play(i);
	    };
	    // play the first file
	    play(i);
	}
	    
	// we start preloading all the audio files
	for (var i in audioFiles) {
	    preloadAudio(audioFiles[i]);
	}
</script>
<?php 
mysqli_close($con);
die();
?>