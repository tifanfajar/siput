<?php 
error_reporting("null");
date_default_timezone_set("Asia/Jakarta");
include '../config.php'; 
$qry = "select distinct(ly.ID_LAYANAN),ly.NAMA_LAYANAN
from ms_layanan as ly 
	inner join tr_layanan_loket lk 
			on ly.ID_LAYANAN=lk.ID_LAYANAN
	order by ly.ID_LAYANAN asc";
	$data_ly = mysqli_query($con,$qry);
while($ly=mysqli_fetch_object($data_ly)){ ?>
		<div class="col-md-6 floating grid-item ">
			<div class="bx-0 container-fluid">
				<span class="title1"><?php echo $ly->NAMA_LAYANAN ?></span>
				<?php 
					$qry_lk = "select lk.LOKET 
							from ms_layanan as ly 
								inner join tr_layanan_loket lk 
										on ly.ID_LAYANAN=lk.ID_LAYANAN
									where lk.ID_LAYANAN='".$ly->ID_LAYANAN."'
								order by lk.LOKET asc";
					$data_lk = mysqli_query($con,$qry_lk);
				echo '<div class="row">';
				$jml="0";
				$sisa="0";
				while($lk=mysqli_fetch_object($data_lk)){

				//Jumlah Antrian
				$qry_antrian = mysqli_query($con, "SELECT count(ID_DATA_ANTRIAN) as total from data_antrian where (DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (ID_LAYANAN = '".$ly->ID_LAYANAN."');");
				$data_antrian = mysqli_num_rows($qry_antrian); 
				if ($data_antrian === FALSE) { die(mysqli_error()); }
				while($hasil_antrian=mysqli_fetch_assoc($qry_antrian)){ 
					$jml = $hasil_antrian['total'];
				}

				//Antrian Saat Ini
				$tempAntrianSaatIniLkt="0";
				$qry_antrian_saat_ini = mysqli_query($con, "SELECT PANGILAN from temp_panggilan where (PANGILAN like '".$lk->LOKET.";%') and (ID_TEMP_PANGGILAN like '".date("ymd")."%') order by ID_TEMP_PANGGILAN DESC LIMIT 1;");
				$antrian_saat_ini = mysqli_num_rows($qry_antrian_saat_ini); 
				if ($antrian_saat_ini === FALSE) { die(mysqli_error()); }
				while($hasil_antrian_saat_ini=mysqli_fetch_assoc($qry_antrian_saat_ini)){ 
					$tempAntrianSaatIniLkt = $hasil_antrian_saat_ini['PANGILAN'];
				}
				$AntrianSaatIniLkt="0";
				if(!empty($tempAntrianSaatIniLkt)){
						$a1 = explode(";", $tempAntrianSaatIniLkt); $AntrianSaatIniLkt = $a1[1];
				}

				$qry_antrian_sisa = mysqli_query($con, "SELECT count(ID_DATA_ANTRIAN) as total from data_antrian where (DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (ID_LAYANAN = '".$ly->ID_LAYANAN."') AND (STAT_PANGILAN = '0');");
				$antrian_sisa = mysqli_num_rows($qry_antrian_sisa); 
				if ($antrian_sisa === FALSE) { die(mysqli_error()); }
				while($hasil_antrian_sisa=mysqli_fetch_assoc($qry_antrian_sisa)){ 
					$sisa = $hasil_antrian_sisa['total'];
				}


				 ?>
					<div class="loket-bx2">
							<span class="info-loket">Loket - <?php echo $lk->LOKET ?></span>
							<span class="num-loket"><?php echo (!empty($AntrianSaatIniLkt) ? $AntrianSaatIniLkt : '-') ?></span>
					</div>
				<?php } ?>
					<div class="row">
						<div class="col-md-6">
							<span class="total">Total : <?php echo $jml; ?></span>
						</div>
						<div class="col-md-6">
							<span class="sisa">Sisa : <?php echo $sisa; ?></span>
						</div>
					</div>
			</div>
		</div>
	</div>
<?php } ?>