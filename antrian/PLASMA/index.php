<?php
date_default_timezone_set("Asia/Jakarta");
$page = $_SERVER['PHP_SELF'];
include '../config.php'; 
$berita = '';

$pangilan = mysqli_query($con, "SELECT TEXT from data_running_text where STATUS = 'Aktif';");

$data = mysqli_num_rows($pangilan);
if ($data === FALSE) 
{ die(mysqli_error()); }
$separator = "&nbsp; <img src='assets/images/logo_icon.png' style='width:25px;height:20px'> &nbsp;";
while($hasil=mysqli_fetch_array($pangilan)) {
    $berita .= $hasil['TEXT'].$separator;
}

$setting = mysqli_fetch_object(mysqli_query($con,"select * from video_setting")); 
?>
<!DOCTYPE html>
<html>
<head>
	<title>PLASMA - KOMINFO</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/custom.css">
	<link rel="icon" href="assets/images/favico.png">
	<!-- <link rel="stylesheet" href="css/masonry-docs.css"> -->
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>
<style>
	/*Height Slider gambar*/
	.carousel-inner{
		height:auto !important;
	}
	.plhead {
        background-color: #0009;
        padding: 10px;
        float: left;
        width: 100%;
        margin-bottom: 5px;
     }
	.loket-bx{
		display: block;
		width: 100%;
		height: 40px;
		border-radius: 50px;
		background-color: #dedede;
	}
	.info-loket{
		text-align: left;
		width: 100px;
		line-height: 2;
		margin-left: 15px;
		font-weight: bold;
		font-size: 20px;
	}
	.num-loket{
		text-align: right;
		margin-right: 15px;
		font-weight: bold;
		font-size: 30px;
		width: 200px;
		float: right;
		line-height: 1.3;
	}
	.total,.sisa{
		text-align: center;
		font-weight: bold;
		font-size: 20px;
		width: 100%;
		display: block;
		margin-top: 10%;
		/*border: 1px solid #dedede;*/
		border-radius: 50px;
		color: #000;
		background: rgba(254,172,0,1);
		background: -moz-linear-gradient(top, rgba(254,172,0,1) 0%, rgba(232,131,1,1) 100%);
		background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(254,172,0,1)), color-stop(100%, rgba(232,131,1,1)));
		background: -webkit-linear-gradient(top, rgba(254,172,0,1) 0%, rgba(232,131,1,1) 100%);
		background: -o-linear-gradient(top, rgba(254,172,0,1) 0%, rgba(232,131,1,1) 100%);
		background: -ms-linear-gradient(top, rgba(254,172,0,1) 0%, rgba(232,131,1,1) 100%);
		background: linear-gradient(to bottom, rgba(254,172,0,1) 0%, rgba(232,131,1,1) 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#feac00', endColorstr='#e88301', GradientType=0 );
	}
	.floating{
		float: left;
		padding: 10px;
		background-image: url("assets/images/list-back.png");
		background-size: cover;
		margin: 3px;
		width: 49% !important;
	}
	.title1{
		display: block;
		text-align: center;
		font-family: arial;
		font-size: 13px;
		color: #fff;
		margin-bottom: 5px;
		font-weight: bold;
	}
	.loket-bx2{
		display: block;
		width: 100%;
		height: 40px;
		border-radius: 50px;
		background-color: #dedede;
		margin-top: 10px;
	}
	.grid {
	  /*background: #EEE;*/
	  max-width: 100%;
	}
	.grid:after {
	  content: '';
	  display: block;
	  clear: both;
	}
	.grid-item {
	  width: 160px;
	  /*height: 120px;*/
	  float: left;
	 /* background: #D26;
	  border: 2px solid #333;
	  border-color: hsla(0, 0%, 0%, 0.5);
	  border-radius: 5px;*/
	}
	.scr-bar{height: 90vh; overflow: hidden; margin-right:0px;}
</style>
<script type="text/javascript">
          function convert_vol(angka){
                   if(angka=="0"){
                      vol = 0; 
                    }else if(angka=="0.1"){
                      vol = 10; 
                    }else if(angka=="0.2"){
                      vol = 20; 
                    }else if(angka=="0.3"){
                      vol = 30; 
                    }else if(angka=="0.4"){
                      vol = 40; 
                    }else if(angka=="0.5"){
                      vol = 50; 
                    }else if(angka=="0.6"){
                      vol = 60; 
                    }else if(angka=="0.7"){
                      vol = 70; 
                    }else if(angka=="0.8"){
                      vol = 80; 
                    }else if(angka=="0.9"){
                      vol = 90; 
                    }else {
                      vol = 100; 
                    }
                    return vol; 
              }
</script>
<body>

<!-- Content -->
<div class="container-fluid">
	<div class="row">

		<!-- Header -->
		<div class="header plhead">
			<div class="logo-left">
				<img src="assets/images/logoHeaderLoket.png">
			</div>
			<div class="date">
				<h3>Jakarta  <span id="temp"></span>&#32;<span id="unit">°C</span> - <?php echo date("d F Y") ?></h3>
			</div>
		</div>

		<!-- Left Containter -->
		<div class="col-md-4 no-padding">
			<!-- Video -->
		      <div class="hvideo"> 
		        <div class="header-title vt"><h4>VIDEO</h4></div>

		          <div class="embed-responsive embed-responsive-16by9">

		            <?php
		            $jenis_player = $setting->jenis;
		            if($setting->jenis=="1"){
		              $berita1 = mysqli_query($con, "SELECT * from data_video_web where STATUS = '1' and STATUS_VIDEO='1' ;");
		              $data1 = mysqli_num_rows($berita1); if ($data1 === FALSE) { die(mysqli_error()); }
		              $list_video ="";
		              while($hasil1=mysqli_fetch_array($berita1)){ 
		                    $list_video .= "'".$hasil1['PATH_VIDEO']."',";      
		              }                            
		              ?>
		              <div class="video_container">
		               <video    id="myVideo" style="object-fit: fill;top:44px;height:85%" autoplay controls >
		                  <source src="" type="video/mp4">
		                  <source  type="video/mp3">
		                   <source type="video/ogg">
		                  Your browser does not support HTML5 video.
		                </video>
		               </div>
		              <script>
		                var myVideo=document.getElementById("myVideo"); 
		                var videoList=[<?php echo rtrim($list_video,',') ?>];
		                var index = videoList.indexOf(window.currentVideoName);
		                function nextButton(){
		                  myVideo.pause();
		                  myVideo.currentTime=0;
		                  index = index + 1;
		                  if(index==videoList.length)
		                  index = 0;
		                  myVideo.src = 'assets/images/video/'+videoList[index];
		                  window.currentVideoName=videoList[index];
		                  myVideo.play();
		                  document.getElementById('myVideo').addEventListener('ended',myHandler,false);
		                }
		                function myHandler(e){
		                  nextButton();
		                }
		                  function playPause(){ 
		                  if (myVideo.paused){
		                    myVideo.play(); 
		                  }
		                  else {
		                    myVideo.pause(); 
		                  }
		                  } 
		                nextButton();
		                myVideo.play();

		                //Set Volume Video
		                var vid = document.getElementById("myVideo");
		                vid.volume = <?php echo $setting->volume ?>;
		              </script>
		           <?php } else {   //Youtube Video
		             $berita1 = mysqli_query($con, "SELECT * from data_video_web where STATUS = '1' and STATUS_VIDEO='0' ;");
		              $data1 = mysqli_num_rows($berita1); if ($data1 === FALSE) { die(mysqli_error()); }
		              $list_video ="";
		              while($hasil1=mysqli_fetch_array($berita1)){ 
		                    $list_video .= "'".$hasil1['PATH_YOUTUBE']."',";      
		              }    
		            ?>
		            <div id="player"></div>
		            <script src="http://www.youtube.com/iframe_api"></script>
		            <script type="text/javascript">
		              
		              var videoIDs = [<?php echo rtrim($list_video,',') ?>];
		              var player, currentVideoId = 0;
		              function onYouTubeIframeAPIReady() {
		                  player = new YT.Player('player', {
		                      height: '350',
		                      width: '100%',
		                      events: {
		                          'onReady': onPlayerReady,
		                          'onStateChange': onPlayerStateChange
		                      }
		                  });
		              }
		              function onPlayerReady(event) {
		                  event.target.loadVideoById(videoIDs[currentVideoId]);
		              }
		              function onPlayerStateChange(event) {
		                  if (event.data == YT.PlayerState.ENDED) {
		                      currentVideoId++;
		                      if (currentVideoId < videoIDs.length) {
		                          player.loadVideoById(videoIDs[currentVideoId]);
		                      }
		                      else {
		                          currentVideoId = 0;
		                          player.loadVideoById(videoIDs[0]);
		                      }
		                  }
		              }
		               var vol_yt = convert_vol("<?php echo $setting->volume ?>");
		               setTimeout(function(){ 
		                  player.setVolume(vol_yt);
		               }, 3000);            
		             </script>
		           <?php } ?>
		           <script type="text/javascript">
		            var jenis_player = "<?php echo $jenis_player ?>";
		            setInterval(function() {
		              $.ajax({
		                url: 'ajax_setting_web.php',
		                type: 'POST',
		                dataType: 'JSON',
		                success: function(data) {
		                   var jenis = data.jenis;
		                    if(jenis_player!=data.jenis){
		                      location.reload();
		                    }
		                    else {
		                        if(jenis_player=="1"){
		                            var vid = document.getElementById("myVideo");
		                            vid.volume = data.volume;
		                        }
		                        else {
		                          var vol_yt = convert_vol(data.volume);
		                          player.setVolume(vol_yt);
		                        }
		                    }
		                }
		             });
		           }, 10000);
		           </script>
		        </div>
		      </div>
			<!-- Image Slide -->
			<div class="image-slide" >
					<div class="header-title"><h4>INFORMASI LAYANAN</h4></div>
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					  <!-- Indicators -->
					  <!-- <ol class="carousel-indicators">
					    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
					    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
					  </ol> -->

					  <!-- Wrapper for slides -->
                                          <?php
                                            $berita1 = mysqli_query($con, "SELECT ID_DATA_SLIDESHOW, IMAGE1, STATUS from data_slideshow where STATUS = '1';");
                                            echo '<div class="carousel-inner" role="listbox">';
                                            $aa = 0;
                                            $data1 = mysqli_num_rows($berita1); if ($data1 === FALSE) { die(mysqli_error()); }
                                            while($hasil1=mysqli_fetch_array($berita1))
                                            { 
                                                if($aa == 0)
                                                {
                                                    echo '<div class="item active">
                                                            <img src="assets/images/slideshow/'.$hasil1['IMAGE1'].'" alt="...">
                                                        </div>';
                                                    $aa = 1;
                                                }
                                                else
                                                {
                                                    echo '<div class="item">
                                                            <img src="assets/images/slideshow/'.$hasil1['IMAGE1'].'" alt="...">
                                                        </div>';
                                                }
                                            }
                                            echo '</div>';
                                          ?>
                                          
                                          
                                          
                                          
                                          
                                          
					<!--<div class="carousel-inner" role="listbox">
					    <div class="item active">
                                                <img src="assets/images/imgG.jpg" alt="...">
                                            </div>
                                            <div class="item">
                                                <img src="assets/images/imgG.jpg" alt="...">
                                            </div>
                                            <div class="item">
                                                <img src="assets/images/imgG.jpg" alt="...">
                                            </div>
					</div>-->
				</div>
			</div>
		</div>	
		<!-- Right Container -->
		<div id="scr-bar" class="col-md-8 scr-bar">
				<div class="row grid" id="ajax_data" style="padding-left:11px;"></div>
		</div>

		<!-- Time & Marque Line -->
		<div class="col-md-12 no-padding news-box">
			<div class="time">
				<h2><?php echo date("H:i") ?></h2>
			</div>
			<div class="news">
				<marquee style="color:white" behavior="scroll" direction="left"><?php echo $berita ?></marquee>
			</div>
		</div>

	</div>
	</div>
</div>

<!-- End Content -->

<!-- Javascript -->
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<!-- <script type="text/javascript" src="js/masonry.pkgd.min.js"></script> -->
<!-- <script type="" src="js/masonry-docs.min.js"></script> -->
<script src="https://masonry.desandro.com/masonry.pkgd.js"></script>
<!-- Start Sweet Alert  -->
<link rel="stylesheet" type="text/css" href="assets/js/sweetalert2/dist/sweetalert2.min.css">
<script src="assets/js/sweetalert2/dist/sweetalert2.min.js"></script>
<!-- End Sweet Alert  -->
<script>
	$('.grid').masonry({
	  itemSelector: '.grid-item',
	  columnWidth: 20
	});

	setInterval(function(){
	    $("#scr-bar").animate({ scrollTop: $(document).height() }, 18000);

	    setTimeout(function() {
	       $('#scr-bar').animate({scrollTop:0}, 8000);
	    },900);

	},8000);
	

	$(document).ready(function() {
	      getCityWeather();

			$.ajax({
				url: 'ajax_dashboard.php', 
				type: 'POST',
				success: function(data) {
					$("#ajax_data").html(data);
				}
			});
			
	});

	function call_pop_up(loket,no_antrian,id_antrian){
      swal({
        title: '<span style="font-size:50px">No Antrian</span>',
        html: '<span style="color:red;font-size:130px">'+no_antrian+' <br/> Loket '+loket+'</span>',
        showCancelButton: false,
        showConfirmButton: false,
        animation: false
        });
  	}


   var antrian_current;
    setInterval(function() {
        $.ajax({
          url: 'ajax_pop_up.php', 
          type: 'POST',
           success: function(mydata) {
            if(mydata!="no_data"){
                 var arr = mydata.split('%%$$%%');
                 if(antrian_current==""){
                    antrian_current = mydata;
                    call_pop_up(arr[0],arr[1],arr[2]);  
                 }
                 else {
                    if(mydata!=antrian_current){
                        call_pop_up(arr[0],arr[1],arr[2]); 
                    }
                 }
            }
            else {
               swal.close();
            }
          }
        });
    }, 3000);

	setInterval(function() {
		 $.ajax({
			url: 'ajax_dashboard.php', 
			type: 'POST',
			success: function(data) {
				$("#ajax_data").html(data);
			}
		});
	},3000);

	function getCityWeather() {
	  var openWeatherQuery = "http://api.openweathermap.org/data/2.5/weather?q=",
	    //cityName = $("#city").val(),
	    cityName = "Jakarta,id";
	    appid = "&units=metric&appid=940a484292c67ab8ad44b1bd3924a3f5",
	    corsAwURL = "https://cors-anywhere.herokuapp.com/";

	  $.getJSON(corsAwURL + openWeatherQuery + cityName + appid, openWeatherCall);

	  return 0;
	}


	function openWeatherCall(json) {
	  $("#city").val(json.name + ", " + json.sys.country);
	  $("#temp").html(json.main.temp.toFixed(0));
	  $("#hum").html(json.main.humidity);
	  $("#icon").html("<img src='http://openweathermap.org/img/w/" + json.weather[0]["icon"] + ".png'>");
	  $("#weather").html(json.weather[0]["description"]);

	  return 0;
	}



	function unitConversion() {
	  var T = $("#temp").html(),
	    unit = $("#unit").html();

	  if (unit == "°C") {
	    T = (T * 1.8) + 32;
	    $("#temp").html(T.toFixed(1));
	    $("#unit").html("°F");
	  } else {
	    T = (T - 32) / 1.8;
	    $("#temp").html(T.toFixed(1));
	    $("#unit").html("°C");
	  }

	  return 0;
	}


</script>


</body>
</html>