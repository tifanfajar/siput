

<!-- Bootstrap Switch-->
<script src="vendor/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>


<!-- Collapsible Menu Plugin -->
<script src="scripts/collapsibleMenu.min.js"></script>

<!-- Collapsible Sidebar Plugin -->
<script src="scripts/sidebar.js"></script>

<!-- Panel Actions -->
<script src="scripts/panel.min.js"></script>

<!-- Class Toggle Plugin -->
<script src="scripts/classtoggle.min.js"></script>

<!-- Initialize Emphasize -->
<script src="scripts/init.js"></script>


<!-- jQuery SlimScroll -->
<script src="vendor/jquery-slimscroll/jquery.slimscroll.min.js" charset="utf-8"></script>

<!-- Icheck Checkboxes -->
<script src="vendor/iCheck/icheck.min.js"></script>
<script src="vendor/d3/d3.min.js"></script>
<script src="vendor/c3/c3.min.js"></script>
<script>
  
function call_url(myurl){
	$.ajax({
	  type:"POST",
	  dataType:"html",
	  url:'pages/'+myurl,
	  success:function(hasil){
		$("#tombol").click();  
		$('#place_url').html(hasil);
	  }
	});
}
function call_url_with_close_modal(myurl){
	$('#myModal').modal('hide');
	$('#myModal2').modal('hide');
	$('body').removeClass('modal-open');
	$('.modal-backdrop').remove();
	$.ajax({
	  type:"POST",
	  dataType:"html",
	  url:'pages/'+myurl,
	  success:function(hasil){
		$("#tombol").click();
		$('#place_url').html(hasil);
	  }
	});
}
function do_act(form_id,act_controller,after_controller,header_text,content_text,type_icon){
                        swal({
                          title: header_text,
                          text: content_text,
                          type: type_icon,      // warning,info,success,error
                          showCancelButton: true,
                          showLoaderOnConfirm: true,
                          preConfirm: function(){
                            $.ajax({
                                url: act_controller, 
                                type: 'POST',
                                data: new FormData($('#'+form_id)[0]),  // Form ID
                                processData: false,
                                contentType: false,
                                success: function(data) {
                                    if(data=="OK")
                                    {
                                        swal({
                                            title: 'Success',
                                            type: 'success',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          }).then(function() {
											   if(after_controller!=''){
                                                     call_url(after_controller);
                                                }
                                                else {
                                                    location.reload();
                                                } 
											});
                                    } 
                                    else if(data=="NOT_LOGIN")
                                    {
                                          swal({
                                            title: 'Error',
                                            text: "You Must Login Again",
                                            type: 'error',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          },function(){
                                                window.location = '<?php echo $basepath ?>';
                                          });
                                    }
                                    else
                                    {
                                        swal({
                                            title: 'Error',
                                            html: data,
                                            type: 'error',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          });
                                    }
                                }
                            });
                         }
						});   
}
function clear_form(form_id){
                $('#'+form_id).find('input:text, input:password, select, textarea').val('');
                $('#'+form_id).find('input:radio, input:checkbox').prop('checked', false); 
}
</script>