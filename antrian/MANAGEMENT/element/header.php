<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<title>MANAGEMENT - KOMINFO</title>
<meta charset="UTF-8">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta content="Lefgrin Technologies" name="author" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="icon" type="image/png" href="images/logo.png">
<link rel="stylesheet" href="vendor/loaders.css/loaders.min.css">
<link href="vendor/font_googleapis/font_googleapis.css?family=Open+Sans:400,600%7CRubik:300,400,500%7CSource+Code+Pro" rel="stylesheet">

<link rel="stylesheet" href="vendor/c3/c3.min.css">
<!--  Bootstrap -->
<link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.min.css">

<!-- Font Awesome Icon Fonts -->
<link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">

<!-- Include MDI Icon Fonts -->
<link rel="stylesheet" href="vendor/mdi/css/materialdesignicons.min.css">

<!-- Open Iconic Icon Fonts -->
<link href="vendor/open-iconic/font/css/open-iconic-bootstrap.min.css" rel="stylesheet">

<!-- Bootstrap Switch -->
<link rel="stylesheet" href="vendor/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css" />


<!-- Toastr Popups -->
<link rel="stylesheet" type="text/css" href="vendor/toastr/toastr.min.css">

<!-- Sweet Alert Popups -->
<link rel="stylesheet" type="text/css" href="vendor/sweetalert2/dist/sweetalert2.min.css">

<!-- iCheck Checkboxes -->
<link rel="stylesheet" href="vendor/iCheck/skins/square/_all.css">



<link rel="stylesheet" href="css/style.css">
<style>
.subtasks h4 {
    margin-bottom: 1em;
}
.subtask h6 {
    margin-bottom: 1em;
}
</style>
<!-- jQuery -->
<script src="vendor/jquery/dist/jquery.min.js"></script>



<!-- Rupiah -->
<script src="vendor/rupiah.js"></script>


<!-- Datatables -->
<link rel="stylesheet" href="vendor/datatables/media/css/dataTables.bootstrap.min.css">
<script src="vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="vendor/datatables/media/js/dataTables.bootstrap.min.js"></script>

<!-- Sweet Alert -->
<script src="vendor/sweetalert2/dist/sweetalert2.min.js"></script>


<!-- Moment-->
<script src="vendor/moment/min/moment.min.js"></script>

<!-- Bootstrap Javascript -->
<script src="vendor/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Bootstrap DateTimePicker JS-->
<script src="vendor/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

<!-- Select Picker -->
<script src="vendor/select2/dist/js/select2.min.js"></script>
<link rel="stylesheet" href="vendor/select2/dist/css/select2.min.css">

<!-- Bootstrap DatePicker --> 
<script src="vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">


<!-- Bootstrap Date Range Picker -->
<script src="vendor/boostrap_daterangepicker/daterangepicker.js"></script>
<link rel="stylesheet" href="vendor/boostrap_daterangepicker/daterangepicker.css">


<script>
$('.datepicker').datepicker({
	orientation: "bottom",
	autoclose: true,
	todayHighlight: true,
	format: "yyyy-mm-dd"
});

$('.only_number').on('change keyup', function() {
  var sanitized = $(this).val().replace(/[^0-9]/g, '');
  $(this).val(sanitized);
});
</script>

<!-- Bootstrap DateTimePicker CSS-->
<link rel="stylesheet" href="vendor/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
<style>
 .required {
	 color : red;
 }
</style>

<!-- Bootstrap Slider-->
<link rel="stylesheet" type="text/css" href="vendor/bootstrap_slider/dist/css/bootstrap-slider.min.css">
<script src="vendor/bootstrap_slider/dist/bootstrap-slider.min.js"></script>

<!-- Loading CSS-->
<link rel="stylesheet" href="css/loading.css">
<style type="text/css">
	.modal-title{
		display: inline;
	}
</style>
<script type="text/javascript">
	function loadIframe(iframeName, url) {
		    var $iframe = $('#' + iframeName);
		    if ( $iframe.length ) {
		        $iframe.attr('src',url);   
		        return false;
		    }
		    return true;
		}
</script>
</head>