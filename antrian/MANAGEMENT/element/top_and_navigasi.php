<nav class="navbar navbar-blue navbar-fixed-top topbar" style="width:100%;" id="topbar">

    <div class="navbar-header navbar-always-float navbar-left" style="width: 100%;">
        <button class="btn btn-transparent-light navbar-btn navbar-sidebar-toggle  hidden-xs ">
            <i class="mdi mdi-menu"></i>
        </button>
        <button id="tombol"  class="btn btn-transparent-light navbar-btn navbar-sidebar-collapse hidden-sm hidden-md hidden-lg">
            <i class="mdi mdi-menu"></i>
        </button>
        <a class="navbar-brand" href="#">
            <!--<i class="mdi mdi-crown"></i>--><span class="navbar-brand-text">KOMINFO</span>
        </a>
    </div>

    <div class="navbar-body">
        <div class="">
                <div class="row row-table">
                    <div class="col-md-7 col-lg-8  hidden-sm hidden-xs">
                        <!--<form class="navbar-form navbar-form-full">
                            <div class="input-group navbar-input-group">
                                  <input type="text" class="form-control" placeholder="Search">
                                  <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"> <i class="mdi mdi-magnify"></i></button>
                                  </span>
                            </div>
                        </form>-->
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-11 col-xs-11">

                        <ul class="nav navbar-nav navbar-right navbar-always-float">

                            <!--<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <span class="notification-pulse pulse-right"></span>
                              <i class="fa fa-bell"></i>

                            </a>
                            <ul class="dropdown-menu notification-dropdown list-group list-group-linked list-group-wide ">
                              <li class="list-group-item list-group-header">
                                  <a href="#" class="link-wrapper">
                                      <div class="media">


                                        <div class="media-full">
                                            <h5 class="nowrap align-left" style="padding:0;margin:0; font-weight:400">
                                              <span class="pull-right">(12)</span>Notifikasi
                                            </h5>
                                        </div>
                                      </div>
                                  </a>
                              </li>
                              <li class="list-group-item">
                                  <a href="#" class="link-wrapper">
                                      <div class="media">
                                        <div class="media-left">
                                            <img class="media-object img-circle img-thumb-xxs" src="img/face3.jpg" alt="...">
                                        </div>
                                        <div class="media-body">
                                            <div class="media-post">
                                                <p class="media-content">
                                                  <span class="media-user-inline">Alexis Parker </span> is now following you
                                                </p>
                                                <div class="media-time text-small text-muted">1d</div>
                                            </div>

                                        </div>

                                      </div>
                                  </a>
                              </li>
                              <li class="list-group-item">
                                  <a href="#" class="link-wrapper">
                                      <div class="media">
                                        <div class="media-left">
                                            <span class="media-object rounded-icon rounded-icon-mini rounded-icon-success">
                                              <i class="mdi mdi-email"></i>
                                            </span>
                                        </div>
                                        <div class="media-body">
                                            <div class="media-post">
                                                <p class="media-content">
                                                  Company Official Policy on Use of Magic is now Out!
                                                </p>
                                                <div class="media-time text-small text-muted">1d</div>
                                            </div>

                                        </div>

                                      </div>
                                  </a>
                              </li>
                              <li class="list-group-item">
                                  <a href="#" class="link-wrapper">
                                      <div class="media">
                                        <div class="media-left">
                                            <span class="media-object rounded-icon rounded-icon-primary">
                                              <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                        <div class="media-body">
                                            <div class="media-post">
                                                <p class="media-content">
                                                  Meeting with Mr. Kirk at 3 PM
                                                </p>
                                                <div class="media-time text-small text-muted">1d</div>
                                            </div>

                                        </div>

                                      </div>
                                  </a>
                              </li>
                              <li class="list-group-item list-group-footer">
                                  <a href="#" class="link-wrapper">
                                      <div class="media">


                                        <div class="media-full">
                                            <h6 class="nowrap align-center bold text-muted" style="padding:0;margin:0; font-weight:400">
                                              SEE ALL
                                            </h6>

                                        </div>
                                      </div>
                                  </a>
                              </li>
                            </ul>
                          </li>-->
                            <!--<li>
                                <a href="#" role="button" title="Keluar" aria-title="Keluar"><i class="fa fa-sign-out"></i></a>
                            </li>-->
                        
						</ul>
                    </div>
                </div>
        </div><!-- /.container-fluid -->
    </div>

</nav>

    
<div class="sidebar sidebar-left sidebar-dark sidebar-fixed sidebar-navbar-theme" id="sidebar">


    <div class="sidebar-scrollable-content">

    <div class="sidebar-body">
    <div class="sidebar-cover">
        <a class="sidebar-user" data-toggle="collapse" href="#sidebar-highlight" aria-expanded="false" aria-controls="sidebar-highlight">

            <div class="sidebar-user-img">
                <img src="images/logo.png" alt="" class="img-circle img-online img-thumbnailimg-thumbnail-primary">
            </div>
            <div class="sidebar-user-name">
                 Administrator
                <span class="sidebar-user-expand"><i class="fa fa-caret-down"></i></span>
                <!--<span class="text-small sidebar-user-email">
                    Admin
                </span>-->
            </div>

        </a>
        <div class="sidebar-highlight collapse" id="sidebar-highlight">

                <ul class="main-nav">
                    <li>
                        <a style="cursor:pointer" onclick="call_url('pwd.php')" title="Ganti Password"><i class="mdi mdi-key"></i> Ganti Password</a>
                    </li>
                    <li>
                        <a href="logout" title="Keluar"><i class="mdi mdi-logout"></i> Keluar</a>
                    </li>
                </ul>

        </div>
    </div>

    <div class="main-menu-container">
        <ul class="main-nav" id="main-nav">
            <li class="main-nav-label">
                <span>
                    Main Navigation
                </span>
            </li>
			       <li>
                  <a href="" style="cursor:pointer" >
                    <i class="fa fa-th-large"></i><span class="title">Dashboard</span>
                  </a>
             </li>
             <li class=''>
                  <a style="cursor:pointer" class='has-arrow'>
                    <span class='menu-caret'></span>
                    <i class="fa fa-id-badge"></i><span class="title">KIOSK</span>
                  </a>
                  <ul class="">
                      <li class="">
                        <a style="cursor:pointer" onclick="call_url('kiosk_daftar_layanan.php')">
                          <span class="title">Daftar Layanan</span>
                        </a>
                      </li>
                      <!--<li class="">
                        <a style="cursor:pointer" onclick="call_url('kiosk_kouta_layanan.php')">
                          <span class="title">Kouta Layanan</span>
                        </a>
                      </li>-->
                      <li class="">
                        <a style="cursor:pointer" onclick="call_url('kiosk_running_text.php')">
                          <span class="title">Running Text</span>
                        </a>
                      </li>
                      <li class="">
                        <a style="cursor:pointer" onclick="call_url('lainnya_pwd_kiosk.php')">
                          <span class="title">Setting Password KIOSK</span>
                        </a>
                      </li>
                  </ul>
              </li>
              <li class=''>
                  <a style="cursor:pointer" class='has-arrow'>
                    <span class='menu-caret'></span>
                    <i class="fa fa-users"></i><span class="title">Loket</span>
                  </a>
                  <ul class="">
                      <li class="">
                        <a style="cursor:pointer" onclick="call_url('loket_daftar_loket.php')">
                          <span class="title">Daftar Loket</span>
                        </a>
                      </li>
                      <li class="">
                        <a style="cursor:pointer" onclick="call_url('loket_pelayanan_loket.php')">
                          <span class="title">Pelayanan Loket</span>
                        </a>
                      </li>
                      <li class="">
                        <a style="cursor:pointer" onclick="call_url('loket_group_layanan.php')">
                          <span class="title">Group Layanan</span>
                        </a>
                      </li>
                      <li class="">
                        <a style="cursor:pointer" onclick="call_url('loket_sub_group_layanan.php')">
                          <span class="title">Sub Group Layanan</span>
                        </a>
                      </li>
                      <li class="">
                        <a style="cursor:pointer" onclick="call_url('loket_running_text.php')">
                          <span class="title">Running Text</span>
                        </a>
                      </li>
                  </ul>
              </li>
              <li class=''>
                  <a style="cursor:pointer" class='has-arrow'>
                    <span class='menu-caret'></span>
                    <i class="fa fa-tv"></i><span class="title">Plasma</span>
                  </a>
                  <ul class="">
                      <li class="">
                        <a style="cursor:pointer" onclick="call_url('plasma_video.php')">
                          <span class="title">Video</span>
                        </a>
                      </li>
                      <li class="">
                        <a style="cursor:pointer" onclick="call_url('plasma_slideshow.php')">
                          <span class="title">Slideshow</span>
                        </a>
                      </li>
                      <li class="">
                        <a style="cursor:pointer" onclick="call_url('plasma_running_text.php')">
                          <span class="title">Running Text</span>
                        </a>
                      </li>
                  </ul>
             </li>
             <li class=''>
                  <a style="cursor:pointer" class='has-arrow'>
                    <span class='menu-caret'></span>
                    <i class="fa fa-comment"></i><span class="title">SKP</span>
                  </a>
                  <ul class="">
                       <li class=''>
                            <a style="cursor:pointer" class='has-arrow'>
                              <span class='menu-caret'></span>
                              <span class="title">Kepuasan Pelanggan</span>
                            </a>
                            <ul class="">
                                <li class="">
                                  <a style="cursor:pointer" onclick="call_url('skp_kepuasan_pelanggan_layanan.php')">
                                    <span class="title">Layanan</span>
                                  </a>
                                </li>
                                <li class="">
                                  <a style="cursor:pointer"  onclick="call_url('skp_kepuasan_pelanggan_loket.php')">
                                    <span class="title">Loket</span>
                                  </a>
                                </li>
                            </ul>
                      </li> 
                      <li class="">
                        <a style="cursor:pointer" onclick="call_url('skp_running_text.php')">
                          <span class="title">Running Text</span>
                        </a>
                      </li>
                  </ul>
             </li> 
             <li class=''>
                  <a style="cursor:pointer" class='has-arrow'>
                    <span class='menu-caret'></span>
                    <i class="fa fa-file"></i><span class="title">Report</span>
                  </a>
                  <ul class="">
                      <li class="">
                          <a style="cursor:pointer" onclick="call_url('report_sum_layanan.php')">
                            <span class="title">Layanan</span>
                          </a>
                        </li>
                        <li class="">
                          <a style="cursor:pointer"  onclick="call_url('report_sum_loket.php')">
                            <span class="title">Loket</span>
                          </a>
                        </li>
                  </ul>
             </li>
             <li class=''>
                  <a style="cursor:pointer" class='has-arrow'>
                    <span class='menu-caret'></span>
                    <i class="fa fa-link"></i><span class="title">Lainnya</span>
                  </a>
                  <ul class="">
                      <li class="">
                        <a style="cursor:pointer" onclick="call_url('lainnya_running_text.php')">
                          <span class="title">Running Text Antrian</span>
                        </a>
                      </li>
                  </ul>
             </li>
        </ul>
    </div>
    <!-- /.main-menu-container -->
    </div>
    <!-- /.sidebar-body -->
    </div>
    <!-- /.sidebar-scrollable-content -->

    <div class="sidebar-footer">
        <div class="horizontal-nav">
            <ul class="horizontal-nav horizontal-nav-3">
                <li>
                    <a style="cursor:pointer" onclick="call_url('pwd.php')" title="Ganti Password"><i class="mdi mdi-key"></i></a>
                </li>
               <li> <span style="color:#2c343f;">-</span>
                    <!--<a style="cursor:pointer" onclick="call_url('data_diri.php')" title="Data Diri"><i class="mdi mdi-account"></i></a>-->
                </li>
                <li>
                    <a href="logout" title="Keluar"><i class="mdi mdi-logout"></i></a>
                </li>
            </ul>
        </div>
    </div>


</div>