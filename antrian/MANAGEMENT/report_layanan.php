<?php 
	include "../config.php"; 
	include "function/otentifikasi.php"; 
	session_start();
	$user = mysqli_fetch_object(mysqli_query($con,"select 
			us.*,fa.function_access 
			from tb_user as us
			inner join tb_function_access as fa	
				on us.level=fa.kode_function_access 
		where kode_user='".$_SESSION['kode_user']."' "));
	$param  = base64_decode($_REQUEST['param']);
	$param2 = base64_decode($_REQUEST['param2']);
	$tipe   = $_REQUEST['tipe'];
	if($tipe=="pdf"){
		error_reporting(null);
		ini_set('max_execution_time', 9999999999999); 
		require_once 'function/mpdf/vendor/autoload.php';
		$mydata ='<center><h3>Report Summary Layanan</h3></center><br/>
					<table style="width:100%;border: 1px solid black;">
						<tr style="border: 1px solid black;">
							<th style="border: 1px solid black;">Layanan</th>
							<th style="border: 1px solid black;">Total</th>
						</tr>';
			$data_rpt = mysqli_query($con,"SELECT  Ly.ID_LAYANAN,ly.NAMA_LAYANAN
			FROM   ms_layanan as ly 
			where 1=1 ".$param." order by ly.ID_LAYANAN asc ");
		while ($aRow=mysqli_fetch_array($data_rpt)) {

			$ly = $aRow['ID_LAYANAN'];
			$dt_ly = $aRow['NAMA_LAYANAN'];
			$count = mysqli_fetch_object(mysqli_query($con,"select count(*) as total from data_antrian where ID_LAYANAN='".$ly."'  ".$param2." "));
			

			$mydata .=' <tr style="border: 1px solid black;">
							<td style="border: 1px solid black;">'.$dt_ly.'</td>
							<td style="border: 1px solid black;" align="center">'.$count->total.'</td>
						</tr>';
		} 

		$mydata .='</table>';

		$filename = "Report Summary Layanan ".date('Ymdhis');
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
		$mpdf->AddPage('A4-L');
		$mpdf->WriteHTML($mydata);
		$mpdf->Output($filename,'I');
	}
	else {
	if($tipe=="xls"){
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=Report-Summary-Layanan-".date('Ymdhis').".xls");
	}
	else if($tipe=="doc"){
		header("Content-Type: application/vnd.ms-word");
		header("Content-Disposition: attachment; filename=Report-Summary-Layanan-".date('Ymdhis').".doc");
	}
	?>
	<center><h3>Report Summary Layanan</h3></center><br/>
	<table style="width:100%">
		<tr>
			<th>Layanan</th>
			<th>Total</th>
		</tr>
		<?php
		$data_rpt = mysqli_query($con,"SELECT  Ly.ID_LAYANAN,ly.NAMA_LAYANAN
			FROM   ms_layanan as ly
			 where 1=1 ".$param." order by ly.ID_LAYANAN asc ");
		while ($aRow=mysqli_fetch_array($data_rpt)) { 

			$ly = $aRow['ID_LAYANAN'];
			$dt_ly = $aRow['NAMA_LAYANAN'];
			
			
			$count = mysqli_fetch_object(mysqli_query($con,"select count(*) as total from data_antrian where ID_LAYANAN='".$ly."' ".$param2." "));

			?>
		<tr>
			<td><?php echo $dt_ly ?></td>
			<td align="center"><?php echo $count->total ?></td>
		</tr>
		<?php } ?>
	</table>
	<?php } ?>