<?php 
include "../config.php"; 
session_start();
if($_SESSION['kode_user']!=""){
	echo "<script language='javascript'>document.location='home'</script>";
}
else {
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>MANAGEMENT - KOMINFO</title>
		<meta charset="UTF-8">
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
		<meta content="width=device-width, initial-scale=1.0" name="viewport" />
		<link rel="icon" type="image/png" href="img/<?php echo $web->favicon ?>">
		<link rel="icon" href="images/logo.png">
		<meta content="Lefgrin Technologies" name="author" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<?php 
			function random_background(){
				$dir = 'img/background';
				$files = glob($dir . '/*.*');
				$file = array_rand($files);
				return $files[$file];
			}
			?>
		<style>
		body{
		  padding:0;
		  margin:0;
		}
		.vid-container{
		  position:relative;
		  height:100vh;
		  overflow:hidden;
		}
		.bgvid{
		  left:0;
		  top:0;
		  z-index: -1;
		  width: 100%;
		  height: 100%;
		}
		.inner-container{
		  width:400px;
		  height:400px;
		  position:absolute;
		  top:calc(50vh - 200px);
		  left:calc(50vw - 200px);
		  overflow:hidden;
		}
		.bgvid.inner{
		  top:calc(-50vh + 200px);
		  left:calc(-50vw + 200px);
		  filter: url("data:image/svg+xml;utf9,<svg%20version='1.1'%20xmlns='http://www.w3.org/2000/svg'><filter%20id='blur'><feGaussianBlur%20stdDeviation='10'%20/></filter></svg>#blur");
		  -webkit-filter:blur(10px);
		  -ms-filter: blur(10px);
		  -o-filter: blur(10px);
		  filter:blur(10px);
		}
		.box{
		  position:absolute;
		  height:100%;
		  width:100%;
		  font-family:Helvetica;
		  color:#fff;
		  background:rgba(0, 0, 0, 0.74);
		  padding:30px 0px;
		}
		.box h1{
		  text-align:center;
		  margin:30px 0;
		  font-size:30px;
		}
		.box input{
		  display:block;
		  width:300px;
		  margin:20px auto;
		  padding:15px;
		  background:rgba(0,0,0,0.2);
		  color:#fff;
		  border:0;
		}
		.box input:focus,.box input:active,.box button:focus,.box button:active{
		  outline:none;
		}
		.box button{
		  background:#2ecc71;
		  border:0;
		  color:#fff;
		  padding:10px;
		  font-size:20px;
		  width:330px;
		  margin:20px auto;
		  display:block;
		  cursor:pointer;
		}
		.box button:active{
		  background:#27ae60;
		}
		.box p{
		  font-size:14px;
		  text-align:center;
		}
		.box p span{
		  cursor:pointer;
		  color:#666;
		}
		html, body {margin: 0; height: 100%; overflow: hidden}
		</style>
	</head>
	<body>
		<div class="vid-container">
		  <div class="bgvid" style="background: url('<?php echo random_background() ?>');background-size: cover;"></div>	
		  <div class="inner-container">
			<div class="box">
				<form id="f1" action="proses_login" method="POST">	
				    <center>
				  	 	<h2>Management</h2>
				 	 	<h3>Multi Antrian Kominfo</h3>
					</center>
				  <input type="text" name="username" placeholder="Username" autocomplete="off" >
				  <input type="password" name="password" placeholder="Password" >
				  <button type="submit">Login</button>
				  <!--<p><span style="color:cyan">Forgot Password ?</span></p>-->
				  <p>Copyright &copy; <?php echo date('Y') ?></p>
				</form>
			</div>
		  </div>
		</div>
	<script src="vendor/jquery/dist/jquery.min.js" ></script>
	<script>
	setInterval(function() {
		 $.ajax({
		  type:"POST",
		  dataType:"html",
		  url:"function/random_background_login.php",
		  success:function(hasil){
			var new_bg = hasil.replace("../","");  
			// $(".bgvid").css({
				// 'background-image' : 'url('+new_bg+')',
				// 'background-size'  : 'cover'
			// });
			$('.bgvid').animate({opacity: 0}, 'slow', function() {
				$(this)
					.css(
						{
							'background-image' : 'url('+new_bg+')',
							'background-size'  : 'cover'
						})
					.animate({opacity: 1});
			});
		  }
		});
	}, 50000); //5 minutes
	</script>
	</body>
</html>
<?php } ?>