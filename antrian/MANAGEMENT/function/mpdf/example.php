<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
ini_set('max_execution_time', 300); 
require_once __DIR__ . '/vendor/autoload.php';

$content = '<h3>Hellow World</h3>';

$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
$mpdf->AddPage('A4-L');
$mpdf->WriteHTML($content);
$mpdf->Output();