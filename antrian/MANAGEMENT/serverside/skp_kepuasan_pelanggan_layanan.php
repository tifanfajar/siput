<?php
session_start();
$ses_kode_user = $_SESSION['kode_user'];
include "../../config.php";
error_reporting(null);
		$aColumns = array('ly.NAMA_LAYANAN','total','total_sangat_puas','total_puas','total_tidak_puas');
		// Indexed column (used for fast and accurate table cardinality)
		$sIndexColumn = 'ID_LAYANAN';
		
		// DB table to use
		$sTable   = 'data_kepuasan_pengunjung'; // Nama Tabel
		$sTable2  = 'ms_layanan'; // Nama Tabel
		
		// Input method (use $_GET, $_POST or $_REQUEST)
		$input =& $_POST;

		
		$iColumnCount = count($aColumns);
		
		$db = mysqliConnection();
		$sLimit = Paging( $input );
		$sOrder = Ordering( $input, $aColumns );
		$sWhere = Filtering( $aColumns, $iColumnCount, $input, $db );
		
		$aQueryColumns = array();
		foreach ($aColumns as $col) {
			if ($col != ' ') {
				$aQueryColumns[] = $col;
			}
		}
		
		$sQuery = "select SQL_CALC_FOUND_ROWS ly.NAMA_LAYANAN,
						  (select count(*) from  ".$sTable." as rate where rate.ID_LAYANAN=ly.ID_LAYANAN) as total,
							(select count(*) from  ".$sTable." as rate where RATING = 'Sangat Puas' and rate.ID_LAYANAN=ly.ID_LAYANAN) as total_sangat_puas,
							(select count(*) from  ".$sTable." as rate where RATING = 'Puas' and rate.ID_LAYANAN=ly.ID_LAYANAN) as total_puas,
							(select count(*) from  ".$sTable." as rate where RATING = 'Tidak Puas' and rate.ID_LAYANAN=ly.ID_LAYANAN) as total_tidak_puas
							from ".$sTable." as kps 
								inner join ".$sTable2." as ly 
									on ly.ID_LAYANAN=kps.ID_LAYANAN
						 ".$sWhere." GROUP BY ly.ID_LAYANAN 	".$sOrder.$sLimit;
	
		$rResult = $db->query( $sQuery ) or die($db->error);
		// Data set length after filtering
		$sQuery = "SELECT FOUND_ROWS()";
		$rResultFilterTotal = $db->query( $sQuery ) or die($db->error);
		list($iFilteredTotal) = $rResultFilterTotal->fetch_row();
		
		// Total data set length
		$sQuery = "SELECT COUNT(DISTINCT(ly.".$sIndexColumn.")) from ".$sTable." as kps 
						inner join ".$sTable2." as ly 
							on ly.ID_LAYANAN=kps.ID_LAYANAN
				 		".$sWhere." GROUP BY ly.ID_LAYANAN ";	
		$rResultTotal = $db->query( $sQuery ) or die($db->error);
		list($iTotal) = $rResultTotal->fetch_row();
		
		/**
			* Output
		*/
		$output = array(
		"sEcho"                => intval($input['sEcho']),
		"iTotalRecords"        => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData"               => array(),
		);
		
		// Looping Data
		while ( $aRow = $rResult->fetch_assoc()) 
		{
			$row = array();
			$row = array("<center>".$aRow['NAMA_LAYANAN']."</center>","<center>".$aRow['total']."</center>","<center>".$aRow['total_sangat_puas']."</center>","<center>".$aRow['total_puas']."</center>","<center>".$aRow['total_tidak_puas']."</center>");
			$output['aaData'][] = $row;
		}
		
		echo json_encode( $output );
	
?>