<?php
session_start();
$ses_kode_user = $_SESSION['kode_user'];
include "../../config.php";
error_reporting(null);
		$aColumns = array('img.ID_DATA_SLIDESHOW','img.NAMA_IMAGE','img.STATUS');
		// Indexed column (used for fast and accurate table cardinality)
		$sIndexColumn = 'ID_DATA_SLIDESHOW';
		
		// DB table to use
		$sTable  = 'data_slideshow'; // Nama Tabel
		
		// Input method (use $_GET, $_POST or $_REQUEST)
		$input =& $_POST;

		
		$iColumnCount = count($aColumns);
		
		$db = mysqliConnection();
		$sLimit = Paging( $input );
		$sOrder = Ordering( $input, $aColumns );
		$sWhere = Filtering( $aColumns, $iColumnCount, $input, $db );
		
		$aQueryColumns = array();
		foreach ($aColumns as $col) {
			if ($col != ' ') {
				$aQueryColumns[] = $col;
			}
		}
		
		$sQuery = "SELECT SQL_CALC_FOUND_ROWS img.* FROM ".$sTable." as img
					".$sWhere."  ".$sOrder.$sLimit;
					
	
		$rResult = $db->query( $sQuery ) or die($db->error);
		// Data set length after filtering
		$sQuery = "SELECT FOUND_ROWS()";
		$rResultFilterTotal = $db->query( $sQuery ) or die($db->error);
		list($iFilteredTotal) = $rResultFilterTotal->fetch_row();
		
		// Total data set length
		$sQuery = "SELECT COUNT(img.".$sIndexColumn.") FROM ".$sTable." as img ".$sWhere." ";
		$rResultTotal = $db->query( $sQuery ) or die($db->error);
		list($iTotal) = $rResultTotal->fetch_row();
		
		/**
			* Output
		*/
		$output = array(
		"sEcho"                => intval($input['sEcho']),
		"iTotalRecords"        => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData"               => array(),
		);
		
		// Looping Data
		while ( $aRow = $rResult->fetch_assoc()) 
		{
			$row = array();
			
				$edit ='<a title="Edit" href="#" class="btn btn-success btn-sm" style="cursor:pointer" onclick="call_url(\'plasma_slideshow_update/'.base64_encode($aRow['ID_DATA_SLIDESHOW']).'\')" ><i class="fa fa-edit"></i></a>';

				$hapus ='<a title="Hapus" href="#" onClick="do_delete(\''.base64_encode($aRow['ID_DATA_SLIDESHOW']).'\')" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i></a>';
		
			
			$editresethapus = $edit." &nbsp; ".$hapus;
			$path = "../PLASMA/assets/images/slideshow/";
			$status = "<center>".($aRow['STATUS']=="1" ? 'Aktif' : 'Tidak Aktif')."</ccenter>";
			$row = array();
			$row = array("<center>".$editresethapus."</center>","<center><a onclick='show_slide(\"".$path.$aRow['IMAGE1']."\",\"".$aRow['NAMA_IMAGE']."\")' href='javascript:void(0)' style='text-decoration:none'>".$aRow['NAMA_IMAGE']."</a></center>",$status);
			$output['aaData'][] = $row;
		}
		
		echo json_encode( $output );
	
?>