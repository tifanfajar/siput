<?php 
session_start();
$ses_kode_user = $_SESSION['kode_user'];
include "../../config.php";
$param   = $_REQUEST['param']; 
$param2  = $_REQUEST['param2']; 
?>

<table class="table datatable2" style="width:100%">
    <thead>
        <tr>
            <th><center>Layanan</center></th>
            <th><center>No. Antrian</center></th>
            <th><center>Tanggal & Jam</center></th>
            <th><center>Nama Pemohon</center></th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<script type="text/javascript">
    var dTable2;
            $(document).ready(function() {
                dTable2 = $('.datatable2').DataTable( {
                    "bProcessing": true,
                    "bServerSide": true,
                    "bJQueryUI": false,
                    "responsive": false, 
                    "searching": false, 
                    "sAjaxSource": "serverside/report_detail_loket_data.php?param=<?php echo $param ?>&param2=<?php echo $param2 ?>", 
                    "sServerMethod": "POST",
                    "scrollX": true,
                    "columnDefs": [
                    { "orderable": false,   "targets": 0, "searchable": false},
                    { "orderable": false,   "targets": 1, "searchable": false},
                    { "orderable": false,   "targets": 2, "searchable": false},
                    { "orderable": false,   "targets": 3, "searchable": false}
                    ]
                } );
            } );
 setTimeout(function(){ 
    $('.datatable2').DataTable().ajax.reload();
 }, 2000);
</script>
