<?php
session_start();
$ses_kode_user = $_SESSION['kode_user'];
include "../../config.php";
error_reporting(null);
		$aColumns = array('sb.id_sub_group_layanan','ly.KODE_LAYANAN','ly.NAMA_LAYANAN','grp.group_layanan','sb.sub_group_layanan'); //Kolom Pada Tabel

		// Indexed column (used for fast and accurate table cardinality)
		$sIndexColumn = 'id_sub_group_layanan';
		
		// DB table to use
		$sTable   = 'ms_layanan'; // Nama Tabel
		$sTable2  = 'data_group_layanan'; // Nama Tabel
		$sTable3  = 'data_sub_group_layanan'; // Nama Tabel
		
		// Input method (use $_GET, $_POST or $_REQUEST)
		$input =& $_POST;

		
		$iColumnCount = count($aColumns);
		
		$db = mysqliConnection();
		$sLimit = Paging( $input );
		$sOrder = Ordering( $input, $aColumns );
		$sWhere = Filtering( $aColumns, $iColumnCount, $input, $db );
		
		$aQueryColumns = array();
		foreach ($aColumns as $col) {
			if ($col != ' ') {
				$aQueryColumns[] = $col;
			}
		}
		
		$sQuery = "SELECT SQL_CALC_FOUND_ROWS ly.KODE_LAYANAN,ly.NAMA_LAYANAN,grp.group_layanan,sb.id_sub_group_layanan,sb.sub_group_layanan
			FROM ".$sTable." as ly
			inner join ".$sTable2." as grp
				on ly.ID_LAYANAN=grp.jenis
			inner join ".$sTable3." as sb
				on sb.id_group_layanan=grp.id_group_layanan
					".$sWhere." ".$sOrder.$sLimit;
					
	
		$rResult = $db->query( $sQuery ) or die($db->error);
		// Data set length after filtering
		$sQuery = "SELECT FOUND_ROWS()";
		$rResultFilterTotal = $db->query( $sQuery ) or die($db->error);
		list($iFilteredTotal) = $rResultFilterTotal->fetch_row();
		
		// Total data set length
		$sQuery = "SELECT COUNT(sb.".$sIndexColumn.") 
		FROM ".$sTable." as ly
			inner join ".$sTable2." as grp
				on ly.ID_LAYANAN=grp.jenis
			inner join ".$sTable3." as sb
				on sb.id_group_layanan=grp.id_group_layanan ".$sWhere;
		$rResultTotal = $db->query( $sQuery ) or die($db->error);
		list($iTotal) = $rResultTotal->fetch_row();
		
		/**
			* Output
		*/
		$output = array(
		"sEcho"                => intval($input['sEcho']),
		"iTotalRecords"        => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData"               => array(),
		);
		
		// Looping Data
		while ( $aRow = $rResult->fetch_assoc()) 
		{
			$row = array();
			
				$edit ='<a title="Edit" href="#" class="btn btn-success btn-sm" style="cursor:pointer" onclick="call_url(\'loket_sub_group_layanan_update/'.base64_encode($aRow['id_sub_group_layanan']).'\')" ><i class="fa fa-edit"></i></a>';
			
				$hapus ='<a title="Hapus" href="#" onClick="do_delete(\''.base64_encode($aRow['id_sub_group_layanan']).'\')" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i></a>';
		
			
			$editresethapus = $edit." &nbsp; ".$hapus;
				
			$row = array("<center>".$editresethapus."</center>",$aRow['KODE_LAYANAN'],$aRow['NAMA_LAYANAN'],$aRow['group_layanan'],$aRow['sub_group_layanan']);
			$output['aaData'][] = $row;
		}
		
		echo json_encode( $output );
	
?>