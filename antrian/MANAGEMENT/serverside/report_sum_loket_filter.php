<?php 
session_start();
$ses_kode_user = $_SESSION['kode_user'];
include "../../config.php";
$param   = $_REQUEST['param']; 
$param2  = $_REQUEST['param2']; 
?>

<table class="table datatable" style="width:100%">
    <thead>
        <tr>
            <th><center>Loket</center></th>
            <th><center>Total</center></th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<script type="text/javascript">
   var dTable;
            $(document).ready(function() {
                dTable = $('.datatable').DataTable( {
                    "bProcessing": true,
                    "bServerSide": true,
                    "bJQueryUI": false,
                    "responsive": false, 
                    "sAjaxSource": "serverside/report_sum_loket_filter_data.php?param=<?php echo $param ?>&param2=<?php echo $param2 ?>", 
                    "sServerMethod": "POST",
                    "scrollX": true,
                    "aaSorting": [[ 0, "asc" ]],
                    "columnDefs": [
                    { "orderable": true,   "width": "15%", "targets": 0, "searchable": true},
                    { "orderable": false,  "width": "5%", "targets": 1, "searchable": false }
                    ]
                } );
            
                
            } );
</script>
