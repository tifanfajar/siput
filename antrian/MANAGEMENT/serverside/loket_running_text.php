<?php
session_start();
$ses_kode_user = $_SESSION['kode_user'];
include "../../config.php";
error_reporting(null);
		$aColumns = array('rn.ID_DATA_RUNNING','rn.STATUS','rn.TEXT'); //Kolom Pada Tabel
		// Indexed column (used for fast and accurate table cardinality)
		$sIndexColumn = 'ID_DATA_RUNNING';
		
		// DB table to use
		$sTable  = 'app_running_text_cso'; // Nama Tabel
		
		// Input method (use $_GET, $_POST or $_REQUEST)
		$input =& $_POST;

		
		$iColumnCount = count($aColumns);
		
		$db = mysqliConnection();
		$sLimit = Paging( $input );
		$sOrder = Ordering( $input, $aColumns );
		$sWhere = Filtering( $aColumns, $iColumnCount, $input, $db );
		
		$aQueryColumns = array();
		foreach ($aColumns as $col) {
			if ($col != ' ') {
				$aQueryColumns[] = $col;
			}
		}
		
		$sQuery = "SELECT SQL_CALC_FOUND_ROWS rn.* FROM ".$sTable." as rn
					".$sWhere."  ".$sOrder.$sLimit;
		
		$rResult = $db->query( $sQuery ) or die($db->error);
		// Data set length after filtering
		$sQuery = "SELECT FOUND_ROWS()";
		$rResultFilterTotal = $db->query( $sQuery ) or die($db->error);
		list($iFilteredTotal) = $rResultFilterTotal->fetch_row();
		
		// Total data set length
		$sQuery = "SELECT COUNT(rn.".$sIndexColumn.") FROM ".$sTable." as rn ".$sWhere." ";
		$rResultTotal = $db->query( $sQuery ) or die($db->error);
		list($iTotal) = $rResultTotal->fetch_row();
		
		/**
			* Output
		*/
		$output = array(
		"sEcho"                => intval($input['sEcho']),
		"iTotalRecords"        => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData"               => array(),
		);
		
		// Looping Data
		while ( $aRow = $rResult->fetch_assoc()) 
		{
			$row = array();
			
			$edit ='<a title="Edit" href="#" class="btn btn-success btn-sm" style="cursor:pointer" onclick="call_url(\'loket_running_text_update/'.base64_encode($aRow['ID_DATA_RUNNING']).'\')" ><i class="fa fa-edit"></i></a>';
			
			$hapus ='<a title="Hapus" href="#" onClick="do_delete(\''.base64_encode($aRow['ID_DATA_RUNNING']).'\')" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i></a>';
		
			
			$editresethapus = $edit." &nbsp; ".$hapus;
				
			$row = array();
			$row = array("<center>".$editresethapus."</center>","<center>".$aRow['STATUS']."</center>",$aRow['TEXT']);
			$output['aaData'][] = $row;
		}
		
		echo json_encode( $output );
	
?>