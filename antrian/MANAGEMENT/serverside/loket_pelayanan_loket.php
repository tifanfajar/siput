<?php
session_start();
$ses_kode_user = $_SESSION['kode_user'];
include "../../config.php";
error_reporting(null);
		$aColumns = array('lk.ID_LAYANAN_LOKET','ly.KODE_LAYANAN','ly.NAMA_LAYANAN','lk.LOKET'); //Kolom Pada Tabel

		// Indexed column (used for fast and accurate table cardinality)
		$sIndexColumn = 'ID_LAYANAN_LOKET';
		
		// DB table to use
		$sTable   = 'tr_layanan_loket'; // Nama Tabel
		$sTable2  = 'ms_layanan'; // Nama Tabel
		
		// Input method (use $_GET, $_POST or $_REQUEST)
		$input =& $_POST;

		
		$iColumnCount = count($aColumns);
		
		$db = mysqliConnection();
		$sLimit = Paging( $input );
		$sOrder = Ordering( $input, $aColumns );
		$sWhere = Filtering( $aColumns, $iColumnCount, $input, $db );
		
		$aQueryColumns = array();
		foreach ($aColumns as $col) {
			if ($col != ' ') {
				$aQueryColumns[] = $col;
			}
		}
		
		$sQuery = "SELECT SQL_CALC_FOUND_ROWS ly.KODE_LAYANAN,ly.NAMA_LAYANAN,lk.LOKET,lk.ID_LAYANAN_LOKET 
			FROM ".$sTable." as lk
			inner join ".$sTable2." as ly
				on ly.ID_LAYANAN=lk.ID_LAYANAN
					".$sWhere." ".$sOrder.$sLimit;
					
	
		$rResult = $db->query( $sQuery ) or die($db->error);
		// Data set length after filtering
		$sQuery = "SELECT FOUND_ROWS()";
		$rResultFilterTotal = $db->query( $sQuery ) or die($db->error);
		list($iFilteredTotal) = $rResultFilterTotal->fetch_row();
		
		// Total data set length
		$sQuery = "SELECT COUNT(lk.".$sIndexColumn.") FROM ".$sTable." as lk
			inner join ".$sTable2." as ly
				on ly.ID_LAYANAN=lk.ID_LAYANAN".$sWhere;
		$rResultTotal = $db->query( $sQuery ) or die($db->error);
		list($iTotal) = $rResultTotal->fetch_row();
		
		/**
			* Output
		*/
		$output = array(
		"sEcho"                => intval($input['sEcho']),
		"iTotalRecords"        => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData"               => array(),
		);
		
	
		// Looping Data
		while ( $aRow = $rResult->fetch_assoc()) 
		{
			$row = array();
			
				$edit ='<a title="Edit" href="#" class="btn btn-success btn-sm" style="cursor:pointer" onclick="call_url(\'loket_pelayanan_loket_update/'.base64_encode($aRow['ID_LAYANAN_LOKET']).'\')" ><i class="fa fa-edit"></i></a>';
			
				$hapus ='<a title="Hapus" href="#" onClick="do_delete(\''.base64_encode($aRow['ID_LAYANAN_LOKET']).'\')" class="btn btn-danger btn-sm" ><i class="fa fa-trash"></i></a>';
		
			
			$editresethapus = $edit." &nbsp; ".$hapus;
				
			$row = array("<center>".$editresethapus."</center>",$aRow['KODE_LAYANAN'],$aRow['NAMA_LAYANAN'],"<center>".$aRow['LOKET']."</center>");
			$output['aaData'][] = $row;
		}
		
		echo json_encode( $output );
	
?>