<?php 
include '../../config.php';
session_start();

$kode_user  	= $_SESSION['kode_user'];
$param	  	 	= base64_decode(mysqli_real_escape_string($con,$_POST['param']));
$kode  			= mysqli_real_escape_string($con,$_POST['kode_layanan']);
$nama   		= mysqli_real_escape_string($con,$_POST['nama_layanan']);
$jam_awal   	= mysqli_real_escape_string($con,$_POST['jam_awal']);
$jam_akhir   	= mysqli_real_escape_string($con,$_POST['jam_akhir']);
$durasi   		= mysqli_real_escape_string($con,$_POST['durasi']);
$status   		= mysqli_real_escape_string($con,$_POST['status']);
$suara   		= mysqli_real_escape_string($con,$_POST['suara']);
$kouta   		= mysqli_real_escape_string($con,$_POST['kouta']);

$check_kode = mysqli_fetch_object(mysqli_query($con,"select KODE_LAYANAN from ms_layanan where KODE_LAYANAN='".$kode."' and ID_LAYANAN!='".$param."'"));
$check_nama = mysqli_fetch_object(mysqli_query($con,"select NAMA_LAYANAN from ms_layanan where NAMA_LAYANAN='".$nama."' and ID_LAYANAN!='".$param."'"));


if($kode_user==""){
	echo "NOT_LOGIN";
}
else if($kode==""){
	echo "Kode Layanan harus di isi";
}
else if($nama==""){
	echo "Nama Layanan harus di isi";
}
else if($suara==""){
	echo "Prefix Suara harus di isi";
}
else {
		if($check_kode->KODE_LAYANAN==$kode){
			echo "Kode layanan tidak boleh sama";
		}
		else if($check_nama->NAMA_LAYANAN==$nama){
			echo "Nama layanan tidak boleh sama";
		}
		else {
			$data_kouta ="";
			if(empty($kouta)){
				$data_kouta = "KOUTA=null,";
			}
			else {
				$data_kouta = "KOUTA='".$kouta."',";
			}

			if(!empty($jam_awal) && !empty($jam_akhir)){
				if($jam_awal>$jam_akhir){
					echo "Limit Jam Awal lebih besar dari Limit Jam Akhir";
					die();
				}
			}

			$data_jam_awal ="";
			if(empty($jam_awal)){
				$data_jam_awal = "JAM_AWAL=null,";
			}
			else {
				$data_jam_awal = "JAM_AWAL='".$jam_awal."',";
			}

			$data_jam_akhir ="";
			if(empty($jam_akhir)){
				$data_jam_akhir = "JAM_AKHIR=null,";
			}
			else {
				$data_jam_akhir = "JAM_AKHIR='".$jam_akhir."',";
			}

			$data_durasi ="";
			if(empty($durasi)){
				$data_durasi = "DURASI=null,";
			}
			else {
				$data_durasi = "DURASI='".$durasi."',";
			}


			$query ="update ms_layanan set ".$data_durasi.$data_jam_awal.$data_jam_akhir.$data_kouta."STATUS='".$status."',KODE_LAYANAN='".$kode."',NAMA_LAYANAN='".$nama."',PREFIX_SUARA='".$suara."',LAST_UPDATE='".$tgl_indo_full."',LAST_UPDATE_BY='".$kode_user."' where  ID_LAYANAN='".$param."'";
			mysqli_query($con,$query);
			insert_log($con,'Ubah Daftar Layanan',$ip,$agent,$kode_user,$tgl_indo_full,base64_encode($query));
			echo "OK";	
		}
}

?>