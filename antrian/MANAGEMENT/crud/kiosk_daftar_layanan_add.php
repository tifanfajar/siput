<?php 
include '../../config.php'; 
session_start();

$kode_user  	= $_SESSION['kode_user'];
$kode  			= mysqli_real_escape_string($con,$_POST['kode_layanan']);
$nama   		= mysqli_real_escape_string($con,$_POST['nama_layanan']);
$kouta   		= mysqli_real_escape_string($con,$_POST['kouta']);
$jam_awal   	= mysqli_real_escape_string($con,$_POST['jam_awal']);
$jam_akhir   	= mysqli_real_escape_string($con,$_POST['jam_akhir']);
$durasi   		= mysqli_real_escape_string($con,$_POST['durasi']);
$status   		= mysqli_real_escape_string($con,$_POST['status']);
$suara   		= mysqli_real_escape_string($con,$_POST['suara']);
$ly_id	= "ly_".date("Ymdhis")."_".rand(1000,9999);


//Check Data 
$check_kode = mysqli_fetch_object(mysqli_query($con,"select KODE_LAYANAN from ms_layanan where KODE_LAYANAN='".$kode."'"));
$check_nama = mysqli_fetch_object(mysqli_query($con,"select NAMA_LAYANAN from ms_layanan where NAMA_LAYANAN='".$nama."'"));

if($kode_user==""){
	echo "NOT_LOGIN";
}
else if($kode==""){
	echo "Kode Layanan harus di isi";
}
else if($nama==""){
	echo "Nama Layanan harus di isi";
}
else if($suara==""){
	echo "Prefix Suara harus di isi";
}
else if($status==""){
	echo "Status Layanan tidak terpilih";
}
else {
	if($check_kode->KODE_LAYANAN==$kode){
		echo "Kode layanan tidak boleh sama";
	}
	else if($check_nama->NAMA_LAYANAN==$nama){
		echo "Nama layanan tidak boleh sama";
	}
	else {

		//Kouta
		if(empty($kouta)){
			$kouta = "null";
		}
		else {
			$kouta = "'".$kouta."'";
		}


		if(!empty($jam_awal) && !empty($jam_akhir)){
			if($jam_awal>$jam_akhir){
				echo "Limit Jam Awal lebih besar dari Limit Jam Akhir";
				die();
			}
		}

		//Jam Awal
		if(empty($jam_awal)){
			$jam_awal = "null";
		}
		else {
			$jam_awal = "'".$jam_awal."'";
		}

		//Jam Akhir
		if(empty($jam_akhir)){
			$jam_akhir = "null";
		}
		else {
			$jam_akhir = "'".$jam_akhir."'";
		}

			//Durasi
		if(empty($durasi)){
			$durasi = "null";
		}
		else {
			$durasi = "'".$durasi."'";
		}



		$array_gbr = array("peeps.jpg"=>"0", "1.png"=>"0","1.jpg"=>"0","2.jpg"=>"0"); 
		$gambar = array_rand($array_gbr);
		
		//Insert Data
		$query="insert into ms_layanan(JAM_AWAL,JAM_AKHIR,DURASI,KOUTA,STATUS,IMAGE,ID_LAYANAN,NAMA_LAYANAN,KODE_LAYANAN,CREATED_DATE,LAST_UPDATE,CREATED_BY,LAST_UPDATE_BY,PREFIX_SUARA) values (".$jam_awal.",".$jam_akhir.",".$durasi.",".$kouta.",'".$status."','".$gambar."','".$ly_id."','".$nama."','".$kode."','".$tgl_indo_full."','".$tgl_indo_full."','".$_SESSION['id_user']."','".$_SESSION['id_user']."','".$suara."')";
		mysqli_query($con,$query);
		insert_log($con,'Tambah Daftar Layanan',$ip,$agent,$kode_user,$tgl_indo_full,base64_encode($query));
		echo "OK";	
	}
}
?>