<?php 
include '../../config.php';
session_start();

$kode_user    = $_SESSION['kode_user'];
$old_pwd   	  = mysqli_real_escape_string($con,$_POST['old_pwd']);
$new_pwd      = mysqli_real_escape_string($con,$_POST['new_pwd']);
$c_new_pwd    = mysqli_real_escape_string($con,$_POST['c_new_pwd']);

$cek_pwd	= mysqli_fetch_object(mysqli_query($con,"select PASSWORD from data_penguna_aplikasi where ID_PENGUNA_APLIKASI='".$kode_user."'"));

if($kode_user==""){
	echo "NOT_LOGIN";
}
else if($old_pwd==""){
	echo "Password Lama harus diisi";
}
else if($old_pwd!=$cek_pwd->PASSWORD){
	echo "Password Lama salah";
}
else if($new_pwd==""){
	echo "Password Baru harus diisi";
}
else if($c_new_pwd==""){
	echo "Konfirmasi Password Baru harus diisi";
}
else if($new_pwd!=$c_new_pwd){
	echo "Password Baru Tidak Sama";
}
else {
	$query = "update data_penguna_aplikasi set PASSWORD='".$new_pwd."' where ID_PENGUNA_APLIKASI='".$kode_user."'";
	mysqli_query($con,$query) or die(mysqli_error($con));
	insert_log($con,'Ubah Password',$ip,$agent,$kode_user,$tgl_indo_full,base64_encode($query));
	echo "OK";
}


?>