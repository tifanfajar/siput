<?php include '../../config.php';   ?>
 <div class="page">
        <header class="ribbon">
            <h2>
                Tambah Running Text SKP
            </h2>
            <ol class="breadcrumb">
              <li><a href="#" onclick="location.reload();" style="text-decoration:none">Home</a></li>
              <li><a href="#" style="text-decoration:none">SKP</a></li>
              <li><a onclick="call_url('skp_running_text.php')" style="cursor:pointer;text-decoration:none">Daftar Running Text</a></li>
              <li class="active">Tambah Running Text SKP</li>
            </ol>
        </header>
        <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form  id="f1" enctype="multipart/form-data">
								<div class="row">	
									<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Running Text <span class="required">*</span></label>
											<input type="text" name="nama_running"  class="form-control" >
										</div>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Status <span class="required">*</span></label>
											<select class="form-control" name="status">
												    <option value="">Pilih Status</option>
												    <option value="Aktif">Aktif</option>
	      											<option value="Tidak Aktif">Tidak Aktif</option>
											</select>
										</div>
									</div>
                                </div>
								<div class="row"><br/>	
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
									   <center>
										   <button type="button" onclick="call_url('kiosk_running_text.php')" class="btn btn-warning">Kembali</button>&nbsp;
										   <button type="reset"  class="btn btn-danger">Reset</button>&nbsp;
										   <button type="submit" class="btn btn-success">Submit</button>
										</center>
									</div>
                                </div>
                            </form>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel-primary panel -->
                </div>
            </div>
	   </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.page-content -->
    </div>
    <!-- /.page -->
	<script>
		//Link
		$(function() {
			$( '.url' ).on( 'keydown', function( e ) {
				if( !$( this ).data( "value" ) )
					 $( this ).data( "value", this.value );
			});
			$( '.url' ).on( 'keyup', function( e ) {
				if (!/^[_0-9a-z]*$/i.test(this.value))
					this.value = $( this ).data( "value" );
				else
					$( this ).data( "value", null );
			});
		});
		$("#f1").on("submit", function (event) {
		event.preventDefault();
			do_act('f1','crud/skp_running_text_add.php','skp_running_text.php','Tambah Running Text','Apakah data yang di isi sudah benar ?','info');
		});
	</script>