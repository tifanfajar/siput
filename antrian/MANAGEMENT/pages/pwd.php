
<div class="page">
        <header class="ribbon">
            <h2>
                Ganti Password
            </h2>
        </header>
        <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form  id="f1">
								<div class="row">	
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
									    <div class="form-group ">
											<label>Password Lama <span class="required">*</span></label>
											<input type="password" name="old_pwd" class="form-control" >
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
									    <div class="form-group ">
											<label>Password Baru <span class="required">*</span></label>
											<input type="password" name="new_pwd" class="form-control" >
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
									    <div class="form-group ">
											<label>Konfirmasi Password Baru <span class="required">*</span></label>
											<input type="password" name="c_new_pwd" class="form-control" >
										</div>
									</div>
                                </div>
								<div class="row"><br/>	
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
									   <center>
										   <button type="button" onclick="clear_form('f1')"  class="btn btn-danger">Reset</button>&nbsp;
										   <button type="submit" class="btn btn-success">Submit</button>
										</center>
									</div>
                                </div>
                            </form>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel-primary panel -->
                </div>
            </div>
	   </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.page-content -->
    </div>
    <!-- /.page -->
	<script>
		$("#f1").on("submit", function (event) {
		event.preventDefault();
			do_act('f1','crud/pwd.php','','Ubah Password','Apakah anda ingin mengubah password anda ?','info');
		});
	</script>
