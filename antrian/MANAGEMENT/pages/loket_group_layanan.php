<?php 
include '../../config.php'; 
session_start();
?>
<style>
	.table th {
	   text-align: center;   
	   font-weight:bold;
	}
	.datatable>tbody>tr>td
	{
		white-space: nowrap;
	}
</style>
<div class="page">
        <header class="ribbon">
            <h2>
                Group Layanan
            </h2>
            <ol class="breadcrumb">
              <li><a href="#" onclick="location.reload();" style="text-decoration:none">Home</a></li>
              <li><a href="#" style="text-decoration:none">Loket</a></li>
              <li class="active">Group Layanan</li>
            </ol>
        </header>
        <div class="page-content">
        <div class="container-fluid">
           
            <!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <button onclick="call_url('loket_group_layanan_add.php')" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Group Layanan</button>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                            <table class="table datatable" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Fungsi</th>
                                        <th>Kode Layanan</th>
                                        <th>Nama Layanan</th>
                                        <th>Group Layanan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
								<tfoot>
                                    <tr>
                                        <th>Fungsi</th>
                                        <th>Kode Layanan</th>
                                        <th>Nama Layanan</th>
                                        <th>Group Layanan</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.page-content -->
    </div>
    <!-- /.page -->
	<script>
		var dTable;
			$(document).ready(function() {
				dTable = $('.datatable').DataTable( {
					"bProcessing": true,
					"bServerSide": true,
					"bJQueryUI": false,
					"responsive": false, 
					"sAjaxSource": "serverside/loket_group_layanan.php", 
					"sServerMethod": "POST",
					"scrollX": true,
					"columnDefs": [
					{ "orderable": false,  "targets": 0, "searchable": false},
					{ "orderable": true,   "targets": 1, "searchable": true },
					{ "orderable": true,   "targets": 2, "searchable": true },
					{ "orderable": true,   "targets": 3, "searchable": true }
					]
				} );
			
				$('.datatable').removeClass( 'display' ).addClass('table table-striped table-bordered');
				var i = 0;
				$('.table').find( 'tfoot tr th' ).each( function () {
					//Agar kolom Action Tidak Ada Tombol Pencarian
					if( $(this).text() == "Tanggal Buat" || $(this).text() == "Tanggal Update"  ){
						var width = $(".sorting_1").width();
						var title = $('.table thead th').eq( $(this).index() ).text();
						$(this).html( '<input type="text" placeholder="Cari '+title+'" class="form-control datepicker"  style="background-color:white;width:'+width+'" />' );
					}
					else if( $(this).text() != "Fungsi" ){
						var width = $(".sorting_1").width();
						var title = $('.table thead th').eq( $(this).index() ).text();
						$(this).html( '<input type="text" placeholder="Cari '+title+'" class="form-control" style="width:'+width+'" />' );
					}
					i++;
				} );
				
				dTable.columns().every( function () {
					var that = this;
					$( 'input', this.footer() ).on( 'keyup change', function () {
						that
						.search( this.value )
						.draw();
					} );
				});
				
				dTable.columns().every( function () {
					var that = this;
					$( 'select', this.footer() ).on( 'change', function () {
						that
						.search( this.value )
						.draw();
					} );
				});
				
				$('.datepicker').datepicker({
					orientation: "top",
					autoclose: true,
					todayHighlight: true,
					format: "yyyy-mm-dd"
				});
			} );
			
			
			
			
			function do_delete(send_param){
                        swal({
                          title: 'Hapus Group Layanan',
                          text: 'Apakah anda ingin menghapus Group layanan ini',
                          type: 'warning',      // warning,info,success,error
                          showCancelButton: true,
                          showLoaderOnConfirm: true,
                          preConfirm: function(){
                            $.ajax({
                                url: 'crud/loket_group_layanan_delete.php', 
                                type: 'POST',
                                data: 'param='+send_param, 
                                success: function(data) {
                                    if(data=="OK")
                                    {
                                        swal({
                                            title: 'Success',
                                            type: 'success',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          }).then(function() {
												$('.datatable').DataTable().ajax.reload();
											});
                                    } 
                                    else if(data=="NOT_LOGIN")
                                    {
                                          swal({
                                            title: 'Error',
                                            text: "You Must Login Again",
                                            type: 'error',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          },function(){
                                                location.reload();
                                          });
                                    }
                                    else
                                    {
                                        swal({
                                            title: 'Error',
                                            html: data,
                                            type: 'error',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          });
                                    }
                                }
                            });
                         }
						});   
			}
			
	</script>