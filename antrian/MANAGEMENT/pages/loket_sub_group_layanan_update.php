<?php
    include '../../config.php'; 
	$param = base64_decode($_GET['param']);
	$data    = mysqli_fetch_object(mysqli_query($con,"select * from data_sub_group_layanan where id_sub_group_layanan='".$param."'  "));
	$ply     = mysqli_fetch_object(mysqli_query($con,"select * from data_group_layanan where id_group_layanan='".$data->id_group_layanan."'  "));
	?>
 <div class="page">
        <header class="ribbon">
            <h2>
                Ubah Sub Group Layanan
            </h2>
            <ol class="breadcrumb">
              <li><a href="#" onclick="location.reload();" style="text-decoration:none">Home</a></li>
              <li><a href="#" style="text-decoration:none">Loket</a></li>
              <li><a onclick="call_url('loket_sub_group_layanan.php')" style="cursor:pointer;text-decoration:none">Sub Group Layanan</a></li>
              <li class="active">Ubah Sub Group Layanan</li>
            </ol>
        </header>
        <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form  id="f1" enctype="multipart/form-data">
								<div class="row">	
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">	
										<div class="form-group ">
											<label>Pelayanan <span class="required">*</span></label>
											<select onchange="call_grp_layanan();" name="pelayanan" id="pelayanan" class="form-control">
									          <option value="">Pilih Pelayanan</option>
									          <?php 
									              $dt_pl = mysqli_query($con,"select * from ms_layanan");
									              while ($pl=mysqli_fetch_object($dt_pl)){  ?>
									                   <option value="<?php echo $pl->ID_LAYANAN ?>"><?php echo $pl->NAMA_LAYANAN ?></option>
									        <?php } ?>
									      </select>
									      <input value="<?php echo $_GET['param'] ?>" type="hidden" name="param" >
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">	
										<div class="form-group ">
											<label>Group Layanan <span class="required">*</span></label>
											<select name="grp_layanan" id="grp_layanan" class="form-control">
									          <option value="">Pilih Group Layanan</option>
									      </select>
										</div>
									</div>
                                </div>
                                <div class="row">	
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Sub Group Layanan <span class="required">*</span></label>
											<input type="text" value="<?php echo $data->sub_group_layanan ?>" name="sb_grp_layanan" maxlength="50" class="form-control" >
										</div>
									</div>
                                </div>
								<div class="row"><br/>	
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
									   <center>
										   <button type="button" onclick="call_url('loket_sub_group_layanan.php')" class="btn btn-warning">Kembali</button>&nbsp;
										   <button type="reset"  class="btn btn-danger">Reset</button>&nbsp;
										   <button type="submit" class="btn btn-success">Submit</button>
										</center>
									</div>
                                </div>
                            </form>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel-primary panel -->
                </div>
            </div>
	   </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.page-content -->
    </div>
    <!-- /.page -->
	<script>
		$("#pelayanan").val("<?php echo $ply->jenis ?>");
         setTimeout(function(){ 
           call_grp_layanan();
          }, 1000);
         setTimeout(function(){ 
            $("#grp_layanan").val("<?php echo $data->id_group_layanan ?>");
          }, 2000);

		//Link
		$(function() {
			$( '.url' ).on( 'keydown', function( e ) {
				if( !$( this ).data( "value" ) )
					 $( this ).data( "value", this.value );
			});
			$( '.url' ).on( 'keyup', function( e ) {
				if (!/^[_0-9a-z]*$/i.test(this.value))
					this.value = $( this ).data( "value" );
				else
					$( this ).data( "value", null );
			});
		});
		$("#f1").on("submit", function (event) {
		event.preventDefault();
			do_act('f1','crud/loket_sub_group_layanan_update.php','loket_sub_group_layanan.php','Ubah Sub Group Layanan','Apakah data yang di isi sudah benar ?','info');
		});
		function call_grp_layanan(){
	    var ply = $("#pelayanan").val();
	    if(ply!=""){
	      $.ajax({
	            url: 'crud/loket_sub_group_layanan_get_grp.php',
	            type: 'POST',
	            dataType: 'html',
	            data: 'ply='+window.btoa(ply),
	            success: function(mydata) {
	              $("#grp_layanan").html(mydata);
	            }
	        });
	    }
	  }
	</script>