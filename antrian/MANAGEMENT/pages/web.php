  <?php
    include '../../config.php'; 
	$web    = mysqli_fetch_object(mysqli_query($con,"select * from tb_web"));
	
	session_start();
	$level = $_SESSION['type_user'];
	$fa=mysqli_fetch_object(mysqli_query($con,"select * from tb_function_access where kode_function_access='".$level."' and  id_menu='13'"));
	
	require_once '../mobile_detect/Mobile_Detect.php';
	$detect = new Mobile_Detect;
	
	if($detect->isMobile() || $detect->isTablet()) { ?>
	
	<script>$("#nama_menu").html("Website")</script>
<div id="page-content" class="page-content">



	<div id="page-content-scroll" class="header-clear-large">
		<div class="content">
			<div class="container-fluid">
				<div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                           <form  id="f1">
							    <div class="row">	
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Judul <span class="required">*</span></label>
											<input value="<?php echo $web->judul?>" type="text" name="judul" maxlength="100" class="form-control" >
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
									    <div class="form-group ">
											<label>Email <span class="required">*</span></label>
											<input value="<?php echo $web->email ?>" type="email" name="email" maxlength="150" class="form-control">
										</div>
									</div>
                                </div>
								<div class="row">	
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Kata Kunci <span class="required">*</span></label>
											<input value="<?php echo $web->kata_kunci ?>" type="text" placeholder="example : keyword 1, keyword 2" name="kata_kunci" class="form-control" >
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
									    <div class="form-group ">
											<label>Deskripsi <span class="required">*</span></label>
											<input value="<?php echo $web->deskripsi ?>" type="text" name="deskripsi" maxlength="150" class="form-control">
										</div>
									</div>
                                </div>
								<div class="row">	
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>No Tlp <span class="required">*</span></label>
											<input value="<?php echo $web->no_tlp ?>" type="text" name="no_tlp" maxlength="25"  class="form-control" >
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
									    <div class="form-group ">
											<label>No Hp <span class="required">*</span></label>
											<input value="<?php echo $web->no_hp ?>" type="text" name="no_hp" maxlength="25" class="form-control">
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
									    <div class="form-group ">
											<label>No Fax</label>
											<input value="<?php echo $web->no_fax ?>" type="text" name="no_fax" maxlength="25" class="form-control">
										</div>
									</div>
                                </div>
								<div class="row">	
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
									    <div class="form-group ">
											<label>Alamat <span class="required">*</span></label>
											<textarea class="form-control" rows="3" name="alamat"><?php echo $web->alamat ?></textarea>
										</div>
									</div>
                                </div>
								<div class="row">	
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
									    <div class="form-group ">
											<label>Logo </label>
											<input type="file" name="logo">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
									    <div class="form-group ">
											<label>Favicon </label>
											<input type="file" name="favicon">
										</div>
									</div>
                                </div>
								<div class="row"><br/>	
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
										<?php if($fa->fua_edit=="1"){ ?>
										   <center>
											   <button type="reset"  onclick="clear_form('f1')"  class="btn btn-danger">Reset</button>&nbsp;
											   <button type="submit" class="btn btn-success">Submit</button>
											</center>
										<?php } ?>
									</div>
                                </div>
                            </form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel-primary panel -->
                </div>
            </div>
			</div>	
		</div>					
	</div>
	
</div>

<script>
$("#f1").on("submit", function (event) {
		event.preventDefault();
			do_act('f1','crud/web.php','web.php','Ubah Data Website','Apakah data yang diisi sudah benar ?','info');
		});
</script>
	
	<?php } else { ?>
 <div class="page">
        <header class="ribbon">
            <h2>
                Website
            </h2>
            <ol class="breadcrumb">
              <li><a href="#" onclick="location.reload();" style="text-decoration:none">Home</a></li>
              <li><a href="#" style="text-decoration:none">Pengaturan</a></li>
              <li class="active">Manage Website</li>
            </ol>
        </header>
        <div class="page-content">
        <div class="container-fluid">
			 <?php if($fa->fua_read=="1"){ ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form  id="f1">
							    <div class="row">	
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Judul <span class="required">*</span></label>
											<input value="<?php echo $web->judul?>" type="text" name="judul" maxlength="100" class="form-control" >
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
									    <div class="form-group ">
											<label>Email <span class="required">*</span></label>
											<input value="<?php echo $web->email ?>" type="email" name="email" maxlength="150" class="form-control">
										</div>
									</div>
                                </div>
								<div class="row">	
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Kata Kunci <span class="required">*</span></label>
											<input value="<?php echo $web->kata_kunci ?>" type="text" placeholder="example : keyword 1, keyword 2" name="kata_kunci" class="form-control" >
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
									    <div class="form-group ">
											<label>Deskripsi <span class="required">*</span></label>
											<input value="<?php echo $web->deskripsi ?>" type="text" name="deskripsi" maxlength="150" class="form-control">
										</div>
									</div>
                                </div>
								<div class="row">	
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>No Tlp <span class="required">*</span></label>
											<input value="<?php echo $web->no_tlp ?>" type="text" name="no_tlp" maxlength="25"  class="form-control" >
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
									    <div class="form-group ">
											<label>No Hp <span class="required">*</span></label>
											<input value="<?php echo $web->no_hp ?>" type="text" name="no_hp" maxlength="25" class="form-control">
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
									    <div class="form-group ">
											<label>No Fax</label>
											<input value="<?php echo $web->no_fax ?>" type="text" name="no_fax" maxlength="25" class="form-control">
										</div>
									</div>
                                </div>
								<div class="row">	
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
									    <div class="form-group ">
											<label>Alamat <span class="required">*</span></label>
											<textarea class="form-control" rows="3" name="alamat"><?php echo $web->alamat ?></textarea>
										</div>
									</div>
                                </div>
								<div class="row">	
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
									    <div class="form-group ">
											<label>Logo </label>
											<input type="file" name="logo">
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
									    <div class="form-group ">
											<label>Favicon </label>
											<input type="file" name="favicon">
										</div>
									</div>
                                </div>
								<div class="row"><br/>	
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
										<?php if($fa->fua_edit=="1"){ ?>
										   <center>
											   <button type="reset"  onclick="clear_form('f1')"  class="btn btn-danger">Reset</button>&nbsp;
											   <button type="submit" class="btn btn-success">Submit</button>
											</center>
										<?php } ?>
									</div>
                                </div>
                            </form>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel-primary panel -->
                </div>
            </div>
			<?php } else { echo $no_akses; } ?>	
	   </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.page-content -->
    </div>
    <!-- /.page -->
	<script>
		$("#f1").on("submit", function (event) {
		event.preventDefault();
			do_act('f1','crud/web.php','web.php','Ubah Data Website','Apakah data yang diisi sudah benar ?','info');
		});
	</script>
	<?php } ?>