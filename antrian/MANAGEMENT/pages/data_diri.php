  <?php
    include '../../config.php'; 
	session_start();
	$us    = mysqli_fetch_object(mysqli_query($con,"select * from tb_user where kode_user='".$_SESSION['kode_user']."' and is_deleted='0' "));

	require_once '../mobile_detect/Mobile_Detect.php';
	$detect = new Mobile_Detect;

	if($detect->isMobile() || $detect->isTablet()) {  ?>
	
	<script>$("#nama_menu").html("Data Diri")</script>
<div id="page-content" class="page-content">



	<div id="page-content-scroll" class="header-clear-large">
		<div class="content">
			<div class="container-fluid">
				<div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form  id="f1">
								<div class="row">	
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Nama Lengkap <span class="required">*</span></label>
											<input value="<?php echo $us->nama_lengkap ?>" type="text" name="nama_lengkap" maxlength="50" class="form-control" >
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Email <span class="required">*</span></label>
											<input value="<?php echo $us->email ?>" type="email" name="email" maxlength="200" class="form-control" >
										</div>
									</div>
                                </div>
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Alamat <span class="required">*</span></label>
											<textarea class="form-control" name="alamat"><?php echo $us->alamat ?></textarea>
										</div>
									</div>
                                </div>
								<div class="row">
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>No Hp <span class="required">*</span></label>
											<input value="<?php echo $us->no_hp ?>" type="text" name="no_hp" maxlength="25" class="form-control" >
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>No Tlp</label>
											<input value="<?php echo $us->no_tlp ?>" type="text" name="no_tlp" maxlength="25" class="form-control" >
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>No Fax </label>
											<input value="<?php echo $us->no_fax ?>" type="text" name="no_fax" maxlength="25" class="form-control" >
										</div>
									</div>
                                </div>
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Vendor</label>
											<input value="<?php echo $us->vendor ?>" type="text" name="vendor" maxlength="50" class="form-control" >
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Pas Foto</label>
											<input type="file" name="pas_foto">
										</div>
									</div>
                                </div>
								<div class="row"><br/>	
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
									   <center>
										   <button type="button" onclick="clear_form('f1')"  class="btn btn-danger">Reset</button>&nbsp;
										   <button type="submit" class="btn btn-success">Submit</button>
										</center>
									</div>
                                </div>
                            </form>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel-primary panel -->
                </div>
            </div>
			</div>	
		</div>					
	</div>
	
</div>

<script>
//Link
		$(function() {
			$( '.url' ).on( 'keydown', function( e ) {
				if( !$( this ).data( "value" ) )
					 $( this ).data( "value", this.value );
			});
			$( '.url' ).on( 'keyup', function( e ) {
				if (!/^[_0-9a-z]*$/i.test(this.value))
					this.value = $( this ).data( "value" );
				else
					$( this ).data( "value", null );
			});
		});
		$("#f1").on("submit", function (event) {
		event.preventDefault();
			do_act('f1','crud/data_diri.php','','Ubah Data Diri','Apakah anda ingin mengubah data diri anda ?','info');
		});
</script>
	
	<?php } else  { ?>
 <div class="page">
        <header class="ribbon">
            <h2>
                Data Diri
            </h2>
        </header>
        <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form  id="f1">
								<div class="row">	
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Nama Lengkap <span class="required">*</span></label>
											<input value="<?php echo $us->nama_lengkap ?>" type="text" name="nama_lengkap" maxlength="50" class="form-control" >
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Email <span class="required">*</span></label>
											<input value="<?php echo $us->email ?>" type="email" name="email" maxlength="200" class="form-control" >
										</div>
									</div>
                                </div>
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Alamat <span class="required">*</span></label>
											<textarea class="form-control" name="alamat"><?php echo $us->alamat ?></textarea>
										</div>
									</div>
                                </div>
								<div class="row">
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>No Hp <span class="required">*</span></label>
											<input value="<?php echo $us->no_hp ?>" type="text" name="no_hp" maxlength="25" class="form-control" >
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>No Tlp</label>
											<input value="<?php echo $us->no_tlp ?>" type="text" name="no_tlp" maxlength="25" class="form-control" >
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>No Fax </label>
											<input value="<?php echo $us->no_fax ?>" type="text" name="no_fax" maxlength="25" class="form-control" >
										</div>
									</div>
                                </div>
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Vendor</label>
											<input value="<?php echo $us->vendor ?>" type="text" name="vendor" maxlength="50" class="form-control" >
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Pas Foto</label>
											<input type="file" name="pas_foto">
										</div>
									</div>
                                </div>
								<div class="row"><br/>	
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
									   <center>
										   <button type="button" onclick="clear_form('f1')"  class="btn btn-danger">Reset</button>&nbsp;
										   <button type="submit" class="btn btn-success">Submit</button>
										</center>
									</div>
                                </div>
                            </form>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel-primary panel -->
                </div>
            </div>
	   </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.page-content -->
    </div>
    <!-- /.page -->
	<script>
		//Link
		$(function() {
			$( '.url' ).on( 'keydown', function( e ) {
				if( !$( this ).data( "value" ) )
					 $( this ).data( "value", this.value );
			});
			$( '.url' ).on( 'keyup', function( e ) {
				if (!/^[_0-9a-z]*$/i.test(this.value))
					this.value = $( this ).data( "value" );
				else
					$( this ).data( "value", null );
			});
		});
		$("#f1").on("submit", function (event) {
		event.preventDefault();
			do_act('f1','crud/data_diri.php','','Ubah Data Diri','Apakah anda ingin mengubah data diri anda ?','info');
		});
	</script>
	<?php } ?>