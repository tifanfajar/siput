<?php
    include '../../config.php'; 
	$param = base64_decode($_GET['param']);
	$data    = mysqli_fetch_object(mysqli_query($con,"select * from app_running_text where ID_DATA_RUNNING='".$param."'  "));
	?>
 <div class="page">
        <header class="ribbon">
            <h2>
                Ubah Running Text
            </h2>
            <ol class="breadcrumb">
              <li><a href="#" onclick="location.reload();" style="text-decoration:none">Home</a></li>
              <li><a href="#" style="text-decoration:none">KIOSK</a></li>
              <li><a onclick="call_url('kiosk_running_text.php')" style="cursor:pointer;text-decoration:none">Daftar Running Text</a></li>
              <li class="active">Ubah Running Text</li>
            </ol>
        </header>
        <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form  id="f1" enctype="multipart/form-data">
								<div class="row">	
									<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Running Text <span class="required">*</span></label>
											<input type="text" value="<?php echo $data->TEXT ?>" name="nama_running"  class="form-control" >
											<input value="<?php echo $_GET['param'] ?>" type="hidden" name="param" >
										</div>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Status <span class="required">*</span></label>
											<select class="form-control" name="status" id="status">
											    <option value="">Pilih Status</option>
											    <option value="Aktif">Aktif</option>
      											<option value="Tidak Aktif">Tidak Aktif</option>
											</select>
										</div>
									</div>
                                </div>
								<div class="row"><br/>	
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
									   <center>
										   <button type="button" onclick="call_url('kiosk_running_text.php')" class="btn btn-warning">Kembali</button>&nbsp;
										   <button type="reset"  class="btn btn-danger">Reset</button>&nbsp;
										   <button type="submit" class="btn btn-success">Submit</button>
										</center>
									</div>
                                </div>
                            </form>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel-primary panel -->
                </div>
            </div>
	   </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.page-content -->
    </div>
    <!-- /.page -->
	<script>
		$("#status").val("<?php echo $data->STATUS ?>");
		//Link
		$(function() {
			$( '.url' ).on( 'keydown', function( e ) {
				if( !$( this ).data( "value" ) )
					 $( this ).data( "value", this.value );
			});
			$( '.url' ).on( 'keyup', function( e ) {
				if (!/^[_0-9a-z]*$/i.test(this.value))
					this.value = $( this ).data( "value" );
				else
					$( this ).data( "value", null );
			});
		});
		$("#f1").on("submit", function (event) {
		event.preventDefault();
			do_act('f1','crud/kiosk_running_text_update.php','kiosk_running_text.php','Ubah Running Text','Apakah data yang di isi sudah benar ?','info');
		});
	</script>