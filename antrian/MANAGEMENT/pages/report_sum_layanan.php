<?php 
include '../../config.php'; 
session_start();
?>


<div class="page">
        <header class="ribbon">
            <h2>
                Report Summary Layanan
            </h2>
            <ol class="breadcrumb">
              <li><a href="#" onclick="location.reload();" style="text-decoration:none">Home</a></li>
              <li><a href="#" style="text-decoration:none">Report</a></li>
              <li class="active">Layanan</li>
            </ol>
        </header>
        <div class="page-content">
        <div class="container-fluid">

        	<!-- Modal -->
			  <div class="modal fade" id="myModalFilter" role="dialog">
				<div class="modal-dialog" style="width: 80%;">
				  <!-- Modal content-->
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">Filter Report</span></h4>
					</div>
					<div class="modal-body" >  
						<form id="filter_report" method="POST">	
							<div class="row">	
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Layanan </label>
											<select name="pelayanan" id="pelayanan" class="form-control" >
									          <option value="">Pilih Layanan</option>
									          <?php 
									              $dt_pl = mysqli_query($con,"select * from ms_layanan");
									              while ($pl=mysqli_fetch_object($dt_pl)){  ?>
									                   <option value="<?php echo $pl->ID_LAYANAN ?>"><?php echo $pl->NAMA_LAYANAN ?></option>
									        <?php } ?>
									      </select>
										</div>
									</div>
								</div>
								<div class="row">	
									<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Tanggal Awal <span class="required">*</span></label>
											<input type="text" name="tgl_awal" id="tgl_awal" class="form-control datepicker_bottom" >
										</div>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Tanggal Akhir</label>
											<input type="text" name="tgl_aakhir" id="tgl_akhir" class="form-control datepicker_bottom" >
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
										<br/>
										<button type="button" onclick="cari()"  class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
											&nbsp;
										<button type="reset"  onclick="reset_data()" class="btn btn-danger"><i class="fa fa-eraser"></i> Reset</button>
									</div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
					  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				  </div>
				</div>
			  </div>


			  <!-- Modal -->
			  <div class="modal fade" id="myModal" role="dialog">
				<div class="modal-dialog" style="width: 80%;margin: auto;">
				  <!-- Modal content-->
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title" id="judul_layanan"></span></h4>
					</div>
					<div class="modal-body" >  
						<div class="row">
                    			<div class="col-md-10"> 
								</div>
								<div class="col-md-2"> 
									<button type="button"  onclick="print_laporan_dt('pdf')"  class="btn btn-success"><i class="fa fa-file-pdf-o"></i> Export PDF</button>
		                            &nbsp;&nbsp;
		                           <!-- <button type="button"  onclick="print_laporan_dt('xls')"  class="btn btn-success"><i class="fa fa-file-excel-o"></i> Export Excel</button>
		                            &nbsp;&nbsp;
		                            <button type="button" onclick="print_laporan_dt('doc')"  class="btn btn-success"><i class="fa fa-file-word-o"></i> Export Word</button>-->
                    			</div>
                    		</div>
						<div id="list_data_antrian" style="overflow-y: auto;max-height: 400px;overflow-x: hidden;"></div>
					</div>
					<div class="modal-footer">
					  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				  </div>
				</div>
			  </div>
           
            <!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                    	<div class="panel-heading">
                    		<div class="row">
                    			<div class="col-md-6"> 
                    				<button type="button" data-toggle="modal" data-target="#myModalFilter" class="btn btn-info"><i class="fa fa-search"></i> Filter Report</button>
								</div>
								<div class="col-md-6"> 
									<button type="button"  onclick="print_laporan('pdf')"  class="btn btn-success"><i class="fa fa-file-pdf-o"></i> Export PDF</button>
		                            &nbsp;&nbsp;
		                            <button type="button"  onclick="print_laporan('xls')"  class="btn btn-success"><i class="fa fa-file-excel-o"></i> Export Excel</button>
		                            &nbsp;&nbsp;
		                            <button type="button" onclick="print_laporan('doc')"  class="btn btn-success"><i class="fa fa-file-word-o"></i> Export Word</button>
                    			</div>
                    		</div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        	<input type="hidden" id="id_layanan_val">
                        	<input type="hidden" id="filter_sum_val">
                        	<input type="hidden" id="filter_sum_val_date">
		                        <div id="filter_sum" class="table-responsive">
		                            <table class="table datatable">
		                                <thead>
		                                    <tr>
		                                        <th><center>ID Layanan</center></th>
		                                        <th><center>Layanan</center></th>
		                                        <th><center>Total</center></th>
		                                    </tr>
		                                </thead>
		                                <tbody>
		                                </tbody>
		                            </table>
		                        </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.page-content -->
    </div>
    <!-- /.page -->
	<script>
		function date_to_default(tgl){
	      var newdate = tgl.split("/").reverse().join("-");
	      return newdate;
	    }
		function cari(){
			var layanan    = $("#pelayanan").val();
			var tgl_awal   = $("#tgl_awal").val();	
			var tgl_akhir  = $("#tgl_akhir").val();	
			var param="";
			var param2="";
			if(tgl_awal==""){
				swal("Terjadi Kesalahan","Tanggal Awal tidak boleh kosong","error");
			}
			else {
				 if(layanan!=""){
					param = " and ly.ID_LAYANAN ='"+layanan.trim()+"' ";
				}

			var err = "";	
			if(tgl_awal && tgl_akhir){
		        date1 = new Date(tgl_awal); 
		        date2 = new Date(tgl_akhir);

		        var date1   = ((new Date(tgl_awal.split('/').reverse().join('/')).getTime())/1000);
		        var date2   = ((new Date(tgl_akhir.split('/').reverse().join('/')).getTime())/1000);
		        if(date1 > date2){
		          swal("Terjadi Kesalahan","Tanggal Awal lebih besar daripada Tanggal Akhir","error");
		          err = "error";	
		        }
		        else {
		           param2 += " and DATE_FORMAT(TANGGAL , '%Y-%m-%d') between '"+date_to_default(tgl_awal.trim())+"' and  '"+date_to_default(tgl_akhir.trim())+"' ";
		        }
		      }
		      else if(tgl_awal){
		           param2 += " and DATE_FORMAT(TANGGAL , '%Y-%m-%d')='"+date_to_default(tgl_awal.trim())+"' ";
		      }
		      else if(tgl_akhir){
		          param2 += " and DATE_FORMAT(TANGGAL , '%Y-%m-%d')='"+date_to_default(tgl_akhir.trim())+"' ";
		      }

		      if(err!="error") {
			      	$("#filter_sum_val").val(param);
					$("#filter_sum_val_date").val(param2);
					$.ajax({
			              url: 'serverside/report_sum_layanan_filter.php',
			              type: 'POST',
			              data: 'param='+btoa(param)+'&param2='+btoa(param2),
			              success: function(mydata) {
			                  $('#filter_sum').html(mydata);
							  $("#myModalFilter").modal('hide');
			              }
			          });
		     }
		     
			}
		}
		function reset_data(){
			var param="";
			var param2="";
			$("#filter_sum_val").val("");
			$("#filter_sum_val_date").val("");
			$.ajax({
              url: 'serverside/report_sum_layanan_filter.php',
              type: 'POST',
              data: 'param='+btoa(param)+'&param2='+btoa(param2),
              success: function(mydata) {
                  $('#filter_sum').html(mydata);
              }
         	 });
			$("#myModalFilter").modal('hide');
		}
		function print_laporan(tipe){
          var param  = $("#filter_sum_val").val();
          var param2 = $("#filter_sum_val_date").val();

            var form = document.createElement("form");
            form.setAttribute("method", "POST");
            if(tipe=="print"){
             form.setAttribute("target", "_blank");
            }
            form.setAttribute("action", "report_layanan");
                   
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", "param");
                hiddenField.setAttribute("value", btoa(param));

                var hiddenField2 = document.createElement("input");
                hiddenField2.setAttribute("type", "hidden");
                hiddenField2.setAttribute("name", "tipe");
                hiddenField2.setAttribute("value", tipe);  

                var hiddenField3 = document.createElement("input");
                hiddenField3.setAttribute("type", "hidden");
                hiddenField3.setAttribute("name", "param2");
                hiddenField3.setAttribute("value", btoa(param2));

                form.appendChild(hiddenField);
                form.appendChild(hiddenField2);
                form.appendChild(hiddenField3);

            document.body.appendChild(form);
            form.submit();
        }
        function print_laporan_dt(tipe){
          var param  = $("#id_layanan_val").val();
          var param2 = $("#filter_sum_val_date").val();

            var form = document.createElement("form");
            form.setAttribute("method", "POST");
            if(tipe=="print"){
             form.setAttribute("target", "_blank");
            }
            form.setAttribute("action", "report_detail_layanan");
                   
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", "param");
                hiddenField.setAttribute("value", btoa(param));

                var hiddenField2 = document.createElement("input");
                hiddenField2.setAttribute("type", "hidden");
                hiddenField2.setAttribute("name", "tipe");
                hiddenField2.setAttribute("value", tipe);  

                var hiddenField3 = document.createElement("input");
                hiddenField3.setAttribute("type", "hidden");
                hiddenField3.setAttribute("name", "param2");
                hiddenField3.setAttribute("value", btoa(param2));

                form.appendChild(hiddenField);
                form.appendChild(hiddenField2);
                form.appendChild(hiddenField3);

            document.body.appendChild(form);
            form.submit();
        }
		function detail_data(link_layanan,header,id_layanan){
			$("#judul_layanan").html(header);
			$("#id_layanan_val").val(id_layanan);
			$("#myModal").modal('show');
			var param2 = $("#filter_sum_val_date").val();

			$.ajax({
	        url: link_layanan,
	        type: 'POST',
	        dataType: 'html',
	        data: 'param2='+btoa(param2),
		        success: function(mydata) {
		            $("#list_data_antrian").html(mydata);
		        }
		    });
		}
		$('.datepicker_bottom').datepicker({
					orientation: "bottom",
					autoclose: true,
					todayHighlight: true,
					format: "yyyy-mm-dd"
				});
		var dTable;
			$(document).ready(function() {
				dTable = $('.datatable').DataTable( {
					"bProcessing": true,
					"bServerSide": true,
					"bJQueryUI": false,
					"responsive": false, 
					"sAjaxSource": "serverside/report_sum_layanan.php", 
					"sServerMethod": "POST",
					"scrollX": true,
					"aaSorting": [[ 0, "asc" ]],
					"columnDefs": [
					{ "orderable": true,  "visible": false, "width": "15%", "targets": 0, "searchable": true},
					{ "orderable": true,   "width": "15%", "targets": 1, "searchable": true},
                    { "orderable": false,  "width": "5%", "targets": 2, "searchable": false }
					]
				} );
			
				
			} );

	</script>