<?php 
include '../../config.php'; 
session_start();
?>
<style>
	.table th {
	   text-align: center;   
	   font-weight:bold;
	}
	.datatable>tbody>tr>td
	{
		white-space: nowrap;
	}
</style>
<div class="page">
        <header class="ribbon">
            <h2>
                Daftar Video
            </h2>
            <ol class="breadcrumb">
              <li><a href="#" onclick="location.reload();" style="text-decoration:none">Home</a></li>
              <li><a href="#" style="text-decoration:none">Plasma</a></li>
              <li class="active">Daftar Video</li>
            </ol>
        </header>

        <div class="modal fade" style="z-index:9999" id="modal_video" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="title_video">Title</h5>
                <button onclick="pause_video()" type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body" id="video_show">
                <video style="width:100%" loop controls>
                   <source id="video_local_src" src="" type="video/mp4">
                </video>
              </div>
            </div>
          </div>
        </div>
        <?php $setting = mysqli_fetch_object(mysqli_query($con,"select * from video_setting")); ?>
        <div class="modal fade" id="modal_volume" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" >Pengaturan Volume</h5>
            <button  type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" >
               <style type="text/css">
                    #ex1Slider .slider-selection {
                        background: #BABABA;
                    }
               </style>
               <center>
               <input id="ex1" data-slider-id='ex1Slider' type="text" data-slider-min="0.0" data-slider-max="1" data-slider-step="0.1" data-slider-value="<?php echo $setting->volume ?>"/>
                <input type="hidden" value="<?php echo $setting->volume ?>" id="ex1val">
                 &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
                <button onclick="save_volume()" class="btn btn-info"><i class="fa fa-check"></i> Save</button>
                <br/>
                Note : 0.5 (50%), 1 (100%)

               </center>

          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" style="z-index:9999" id="modal_video_putar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" >Video Yang Diputar</h5>
            <button  type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body" >
              <center>
                  <input <?php echo ($setting->jenis=="1" ? 'checked' : '') ?> onclick="set_video_putar('1')" type="radio" value="1" name="video_putar"> Video Local &nbsp;&nbsp;
                  <input <?php echo ($setting->jenis=="2" ? 'checked' : '') ?> onclick="set_video_putar('2')" type="radio" value="2" name="video_putar"> Video Youtube
              </center>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" style="z-index:9999" id="modal_video_yt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document"  style="max-width: 1000px; width: 600px;">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="title_video_yt">Title</h5>
          <button onclick="stopVideoYoutube()" type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="video_show_yt">
            <iframe width="100%" id="youtube_video" name="youtube_video" src="" height="315"  frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
    </div>
  </div>


        <div class="page-content">
        <div class="container-fluid">
           


            <!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <center><br/>
                            <button onclick="pengaturan_volume()" class="btn btn-info">Pengaturan Volume</button> &nbsp;&nbsp;

                            <button onclick="video_putar()" class="btn btn-success">Video Yang Diputar</button>
                        </center><br/><br/>

                        <center>
                            <h3>Video Local</h3>
                        </center>
                         <div class="panel-heading">
                            <button onclick="call_url('plasma_video_add.php')" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Video</button>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                            <table class="table datatable" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Fungsi</th>
                                        <th>Video</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
								                <tfoot>
                                    <tr>
                                        <th>Fungsi</th>
                                        <th>Video</th>
                                        <th>Status</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        </div>
                        <!-- /.panel-body -->


                        <center>
                            <h3>Video Youtbe</h3>
                        </center>
                         <div class="panel-heading">
                            <button onclick="call_url('plasma_video_youtube_add.php')" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Video Youtbe</button>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                            <table id="my_table" class="table datatable2" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Fungsi</th>
                                        <th>Youtube Video</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Fungsi</th>
                                        <th>Youtube Video</th>
                                        <th>Status</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.page-content -->
    </div>
    <!-- /.page -->
	<script>
		var dTable;
			$(document).ready(function() {
				dTable = $('.datatable').DataTable( {
					"bProcessing": true,
					"bServerSide": true,
					"bJQueryUI": false,
					"responsive": false, 
					"sAjaxSource": "serverside/plasma_video.php", 
					"sServerMethod": "POST",
					"scrollX": true,
					"columnDefs": [
					{ "orderable": false,  "targets": 0, "searchable": false},
          { "orderable": true,   "targets": 1, "searchable": true },
					{ "orderable": true,   "targets": 2, "searchable": true }
					]
				} );
			
				$('.datatable').removeClass( 'display' ).addClass('table table-striped table-bordered');
				var i = 0;
				$('.table').find( 'tfoot tr th' ).each( function () {
					//Agar kolom Action Tidak Ada Tombol Pencarian
					if( $(this).text() == "Tanggal Buat" || $(this).text() == "Tanggal Update"  ){
						var width = $(".sorting_1").width();
						var title = $('.table thead th').eq( $(this).index() ).text();
						$(this).html( '<input type="text" placeholder="Cari '+title+'" class="form-control datepicker"  style="background-color:white;width:'+width+'" />' );
					}
          else if( $(this).text() == "Status" ){
                        var width = $(".sorting_1").width();
                        var title = $('.table thead th').eq( $(this).index() ).text();
                        $(this).html( '<select class="form-control" style="width:'+width+'"><option value="">Pilih Status</option><option value="1">Aktif</option><option value="0">Tidak Aktif</option></select>' );
          }
					else if( $(this).text() != "Fungsi" ){
						var width = $(".sorting_1").width();
						var title = $('.table thead th').eq( $(this).index() ).text();
						$(this).html( '<input type="text" placeholder="Cari '+title+'" class="form-control" style="width:'+width+'" />' );
					}
					i++;
				} );
				
				dTable.columns().every( function () {
					var that = this;
					$( 'input', this.footer() ).on( 'keyup change', function () {
						that
						.search( this.value )
						.draw();
					} );
				});
				
				dTable.columns().every( function () {
					var that = this;
					$( 'select', this.footer() ).on( 'change', function () {
						that
						.search( this.value )
						.draw();
					} );
				});
				
				$('.datepicker').datepicker({
					orientation: "top",
					autoclose: true,
					todayHighlight: true,
					format: "yyyy-mm-dd"
				});
			} );


      var dTable2;
      $(document).ready(function() {
        dTable2 = $('.datatable2').DataTable( {
          "bProcessing": true,
          "bServerSide": true,
          "bJQueryUI": false,
          "responsive": false, 
          "sAjaxSource": "serverside/plasma_video_youtube.php", 
          "sServerMethod": "POST",
          "scrollX": true,
          "columnDefs": [
          { "orderable": false,  "targets": 0, "searchable": false},
          { "orderable": true,   "targets": 1, "searchable": true },
          { "orderable": true,   "targets": 2, "searchable": true }
          ]
        } );
      
        $('.datatable2').removeClass( 'display' ).addClass('table table-striped table-bordered');
        var i = 0;
        $('#my_table').find( 'tfoot tr th' ).each( function () {
          //Agar kolom Action Tidak Ada Tombol Pencarian
          if( $(this).text() == "Tanggal Buat" || $(this).text() == "Tanggal Update"  ){
            var width = $(".sorting_1").width();
            var title = $('#my_table thead th').eq( $(this).index() ).text();
            $(this).html( '<input type="text" placeholder="Cari '+title+'" class="form-control datepicker"  style="background-color:white;width:'+width+'" />' );
          }
          else if( $(this).text() == "Status" ){
                        var width = $(".sorting_1").width();
                        var title = $('.table thead th').eq( $(this).index() ).text();
                        $(this).html( '<select class="form-control" style="width:'+width+'"><option value="">Pilih Status</option><option value="1">Aktif</option><option value="0">Tidak Aktif</option></select>' );
           }
          else if( $(this).text() != "Fungsi" ){
            var width = $(".sorting_1").width();
            var title = $('#my_table thead th').eq( $(this).index() ).text();
            $(this).html( '<input type="text" placeholder="Cari '+title+'" class="form-control" style="width:'+width+'" />' );
          }
          i++;
        } );
        
        dTable2.columns().every( function () {
          var that = this;
          $( 'input', this.footer() ).on( 'keyup change', function () {
            that
            .search( this.value )
            .draw();
          } );
        });
        
        dTable2.columns().every( function () {
          var that = this;
          $( 'select', this.footer() ).on( 'change', function () {
            that
            .search( this.value )
            .draw();
          } );
        });
        
        $('.datepicker').datepicker({
          orientation: "top",
          autoclose: true,
          todayHighlight: true,
          format: "yyyy-mm-dd"
        });
      } );



			$('#ex1').slider({
            formatter: function(value) {
                 $("#ex1val").val(value);
                 return 'Volume : ' + value;
            }
        });
        function save_volume(){
           var vol = $("#ex1val").val();
            $.ajax({
              url: 'crud/plasma_video_volume.php?vol='+vol,
              type: 'POST',
              dataType: 'html',
              success: function(result_data) {
                swal("Success", "", "success");
              }
            });  
        }
        function set_video_putar(value){
             $.ajax({
                url: 'crud/plasma_video_putar.php?putar='+value,
                type: 'POST',
                dataType: 'html',
                success: function(result_data) {
                }
             });  
        }
        function pengaturan_volume(){
            $('#modal_volume').modal({
                    backdrop: 'static',
                    keyboard: false
            })
        }  
        function video_putar(){
            $('#modal_video_putar').modal({
                    backdrop: 'static',
                    keyboard: false
            })
        }
			function pause_video(){
            $('#video_show video').trigger('pause');
       }
      function show_video(video,title_video){
          $('#modal_video').modal({
                  backdrop: 'static',
                  keyboard: false
          })
          $("#title_video").html(title_video);
          $("#video_local_src").attr("src",video);
          $("#video_show video")[0].load();
      }
			
      function do_delete(send_param){
                        swal({
                          title: 'Hapus Video',
                          text: 'Apakah anda ingin menghapus video ini',
                          type: 'warning',      // warning,info,success,error
                          showCancelButton: true,
                          showLoaderOnConfirm: true,
                          preConfirm: function(){
                            $.ajax({
                                url: 'crud/plasma_video_delete.php', 
                                type: 'POST',
                                data: 'param='+send_param, 
                                success: function(data) {
                                    if(data=="OK")
                                    {
                                        swal({
                                            title: 'Success',
                                            type: 'success',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          }).then(function() {
                        $('.datatable').DataTable().ajax.reload();
                      });
                                    } 
                                    else if(data=="NOT_LOGIN")
                                    {
                                          swal({
                                            title: 'Error',
                                            text: "You Must Login Again",
                                            type: 'error',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          },function(){
                                                location.reload();
                                          });
                                    }
                                    else
                                    {
                                        swal({
                                            title: 'Error',
                                            html: data,
                                            type: 'error',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          });
                                    }
                                }
                            });
                         }
            });   
      }
          function show_youtube(video,title_video){
            var src = 'https://www.youtube.com/embed/'+video;
            loadIframe('youtube_video',src);
            $('#modal_video_yt').modal({
                  backdrop: 'static',
                  keyboard: false
            })
            $("#title_video_yt").html(title_video);
          }
	</script>