<?php include '../../config.php';   ?>
 <div class="page">
        <header class="ribbon">
            <h2>
                Tambah Daftar Layanan
            </h2>
            <ol class="breadcrumb">
              <li><a href="#" onclick="location.reload();" style="text-decoration:none">Home</a></li>
              <li><a href="#" style="text-decoration:none">KIOSK</a></li>
              <li><a onclick="call_url('kiosk_daftar_layanan.php')" style="cursor:pointer;text-decoration:none">Daftar Layanan</a></li>
              <li class="active">Tambah Layanan</li>
            </ol>
        </header>
        <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form  id="f1" enctype="multipart/form-data">
								<div class="row">	
									<div class="col-lg-2 col-md-2 col-sm-10 col-xs-12">	
										<div class="form-group ">
											<label>Kode Layanan <span class="required">*</span></label>
											<input type="text" name="kode_layanan" maxlength="25" class="form-control" >
										</div>
									</div>
									<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">	
										<div class="form-group ">
											<label>Nama Layanan <span class="required">*</span></label>
											<input type="text" name="nama_layanan" maxlength="100" class="form-control" >
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">	
										<div class="form-group ">
											<label>Prefix Suara & Antrian</label>
											<input type="text"  name="suara" id="suara" class="form-control" maxlength="5">
											<span style="color:blue;cursor:pointer;text-decoration:none" onclick="test_suara()" >Test Suara</span>
										</div>
									</div>
                                </div>
                                <div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">	
										<div class="form-group ">
											<label>Kouta / Hari </label>
											<input type="text" name="kouta"  class="form-control only_number" >
										</div>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">	
										<div class="form-group ">
											<label>Durasi (Menit) </label>
											<input type="text" name="durasi"  class="form-control only_number" maxlength="5">
										</div>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">	
										<div class="form-group ">
											<label>Limit Jam Awal</label>
											<input type="text" name="jam_awal"  class="form-control datetimepicker" >
										</div>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">	
										<div class="form-group ">
											<label>Limit Jam Akhir</label>
											<input type="text" name="jam_akhir"  class="form-control datetimepicker" >
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">	
										<div class="form-group ">
											<label>Status <span class="required">*</span></label>
											<select name="status" id="status" class="form-control">
												<option value="">Pilih Status</option>
												<option value="1">Aktif</option>
												<option value="0">Tidak Aktif</option>
											</select>
										</div>
									</div>
                                </div>
								<div class="row"><br/>	
									<iframe id="suara_src" style="width: 0; height: 0; border: 0; border: none; position: absolute;"></iframe>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
									   <center>
										   <button type="button" onclick="call_url('kiosk_daftar_layanan.php')" class="btn btn-warning">Kembali</button>&nbsp;
										   <button type="reset"  class="btn btn-danger">Reset</button>&nbsp;
										   <button type="submit" class="btn btn-success">Submit</button>
										</center>
									</div>
                                </div>
                            </form>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel-primary panel -->
                </div>
            </div>
	   </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.page-content -->
    </div>
    <!-- /.page -->
	<script>
		//Link
		$(function() {
			$( '.url' ).on( 'keydown', function( e ) {
				if( !$( this ).data( "value" ) )
					 $( this ).data( "value", this.value );
			});
			$( '.url' ).on( 'keyup', function( e ) {
				if (!/^[_0-9a-z]*$/i.test(this.value))
					this.value = $( this ).data( "value" );
				else
					$( this ).data( "value", null );
			});
		});
		function test_suara(){
		  var val_suara = $("#suara").val();
		  if(val_suara!=""){
		    $('#suara_src').attr('src','<?php echo $basepath_speaker ?>test.php?abjad='+val_suara);
		  }
		  else {
		    alert("Prefix Suara harus di isi");
		  }
		}  
		$("#f1").on("submit", function (event) {
		event.preventDefault();
			do_act('f1','crud/kiosk_daftar_layanan_add.php','kiosk_daftar_layanan.php','Tambah Layanan','Apakah data yang di isi sudah benar ?','info');
		});
		$('.datetimepicker').datetimepicker({
		format: 'HH:mm',
		  widgetPositioning:{
                                horizontal: 'auto',
                                vertical: 'bottom'
                            }
		});
		$('.only_number').on('change keyup', function() {
		  var sanitized = $(this).val().replace(/[^0-9]/g, '');
		  $(this).val(sanitized);
		});
	</script>