<?php
    include '../../config.php'; 
	$param = base64_decode($_GET['param']);
	$data    = mysqli_fetch_object(mysqli_query($con,"select * from data_petugas_loket where ID_DATA_PETUGAS_LOKET='".$param."'  "));
	?>
 <div class="page">
        <header class="ribbon">
            <h2>
                Ubah Loket
            </h2>
            <ol class="breadcrumb">
              <li><a href="#" onclick="location.reload();" style="text-decoration:none">Home</a></li>
              <li><a href="#" style="text-decoration:none">Loket</a></li>
              <li><a onclick="call_url('loket_daftar_loket.php')" style="cursor:pointer;text-decoration:none">Daftar Loket</a></li>
              <li class="active">Ubah Loket</li>
            </ol>
        </header>
        <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form  id="f1" enctype="multipart/form-data">
								<div class="row">	
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">	
										<div class="form-group ">
											<label>Username <span class="required">*</span></label>
											<input value="<?php echo  $data->USER_NAME ?>" type="text" name="username" class="form-control" maxlength="50" >
											<input value="<?php echo $_GET['param'] ?>" type="hidden" name="param" >
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">	
										<div class="form-group ">
											<label>Password </label>
											<input type="password" name="pwd"  class="form-control" >
										</div>
									</div>
                                </div>
                                <div class="row">	
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">	
										<div class="form-group ">
											<label>Petugas <span class="required">*</span></label>
											<input type="text" value="<?php echo  $data->Petugas ?>" name="petugas"  class="form-control" maxlength="50" >
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">	
										<div class="form-group ">
											<label>Loket <span class="required">*</span></label>
											<input type="text" value="<?php echo  $data->LOKET ?>" name="loket"  class="form-control" maxlength="11" >
										</div>
									</div>
                                </div>
								<div class="row"><br/>	
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
									   <center>
										   <button type="button" onclick="call_url('loket_daftar_loket.php')" class="btn btn-warning">Kembali</button>&nbsp;
										   <button type="reset"  class="btn btn-danger">Reset</button>&nbsp;
										   <button type="submit" class="btn btn-success">Submit</button>
										</center>
									</div>
                                </div>
                            </form>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel-primary panel -->
                </div>
            </div>
	   </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.page-content -->
    </div>
    <!-- /.page -->
	<script>
		//Link
		$(function() {
			$( '.url' ).on( 'keydown', function( e ) {
				if( !$( this ).data( "value" ) )
					 $( this ).data( "value", this.value );
			});
			$( '.url' ).on( 'keyup', function( e ) {
				if (!/^[_0-9a-z]*$/i.test(this.value))
					this.value = $( this ).data( "value" );
				else
					$( this ).data( "value", null );
			});
		});
		$("#f1").on("submit", function (event) {
		event.preventDefault();
			do_act('f1','crud/loket_daftar_loket_update.php','loket_daftar_loket.php','Ubah Loket','Apakah data yang di isi sudah benar ?','info');
		});
	</script>