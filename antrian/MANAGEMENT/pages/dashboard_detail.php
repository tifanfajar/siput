  <?php
    include '../../config.php'; 
	require_once '../mobile_detect/Mobile_Detect.php';
	$detect = new Mobile_Detect;
	$param  = base64_decode($_GET['param']);
	$pr    = mysqli_fetch_object(mysqli_query($con,"select * from tb_perangkat where kode_perangkat='".$param."' and is_deleted='0' "));
	session_start();
	$level = $_SESSION['type_user'];
	$fa=mysqli_fetch_object(mysqli_query($con,"select * from tb_function_access where kode_function_access='".$level."' and  id_menu='1'"));
	
	
	
	if($detect->isMobile() || $detect->isTablet()) { 
	?>
	
	<script>$("#nama_menu").html("<?php echo $pr->nama_perangkat ?>")</script>
<div id="page-content" class="page-content">
<script>
	function check_checked(id,my_status){
		$("#"+id).bootstrapSwitch({onColor: 'success',offColor: 'danger'});
		$("#"+id).bootstrapSwitch(my_status);
	}
</script>

	<div id="page-content-scroll" class="header-clear-larger">
		 <?php if($fa->fua_read=="1"){ ?>
		 
			
			
			<div class="modal fade" id="myModal_chart" role="dialog">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close"   data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">Chart</h4>
					</div>
					<form  id="f2">
						<div class="modal-body" id="scroll">
							<div id="parameter"></div>
						</div>
						<div class="modal-footer">
						  <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
						</div>
					</form>
				  </div>
				</div>
			</div>
			
			<div class="modal fade" id="myModal_detail" role="dialog">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close"   data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">Alat Detail</h4>
					</div>
					<form  id="f2">
						<div class="modal-body scroll">
							<div id="div_data"></div>
						</div>
						<div class="modal-footer">
						  <button type="button"  class="btn btn-danger" data-dismiss="modal">Tutup</button>
						</div>
					</form>
				  </div>
				</div>
			</div>
			
			
			<script>
				function chart(){
						$.ajax({
						  type:"POST",
						  dataType:"html",
						  url:'pages/chart_modal.php',
						  success:function(hasil){
								$("#parameter").html(hasil);
								chart_all();
								$("#chart_option").val('1');
								$(".kostum_data").hide();
								
								
									$('.datepicker').datepicker({
										orientation: "bottom",
										autoclose: true,
										todayHighlight: true,
										format: "yyyy-mm-dd"
									});
									
									$('.datetimepicker').datetimepicker({
										format: 'HH',
										  widgetPositioning:{
																horizontal: 'auto',
																vertical: 'bottom'
															}
									});
									
								
								var mywindow = $(window).height()-178;
								 $('#scroll').slimScroll({
									height: mywindow+'px'
								});
									
						  }
						});
				}
			function check_checked(id,my_status){
				$("#"+id).bootstrapSwitch({onColor: 'success',offColor: 'danger'});
				$("#"+id).bootstrapSwitch(my_status);
			}
			</script>
			
				<div class="decoration decoration-margins"></div>
				<div class="col-md-12 col-sm-12 col-xs-12">
						
						<table border="0">
							<tr>
								<td style="width:90%"><?php echo $pr->nama_perangkat ?></td>
								<td ><i onclick="chart()" data-toggle="modal" data-target="#myModal_chart" class="fa fa-bar-chart fa-3x" aria-hidden="true"></i></td>
							</tr>
						</table>
						<div class="myscroll">
								<?php 
								$data_alat = mysqli_query($con,"select md.id_module,SUBSTR(alt.keterangan , 1, 10) as ket,alt.pin_alat,alt.kode_my_alat,alt.status,mrk.merek,pr.nama_perangkat,pr.jenis_icon,pr.gambar,pr.icon_code,md.nama_module 
																	from tb_my_alat as alt
																	inner join tb_module as md on md.kode_module=alt.kode_module
																	inner join tb_perangkat as pr on pr.kode_perangkat=alt.kode_perangkat
																	inner join tb_merek as mrk on mrk.kode_merek=alt.kode_merek
																	where alt.is_deleted='0' and pr.kode_perangkat='".$param."' order by alt.pin_alat asc");
								while($alat=mysqli_fetch_object($data_alat)){ ?>
								<div class="panel panel-default">
									<div class="panel-body ">
											<div class="chart-container"> 
													<div style="cursor:pointer;" title="Klik untuk melihat detail alat">
														<table style="width:100%">
															<tr data-toggle="modal" data-target="#myModal_detail"  onclick="detail_alat('<?php echo base64_encode($alat->kode_my_alat) ?>')" >
																<td rowspan="4" style="margin-right: 20px;">
																	<center>
																	<?php if($alat->jenis_icon=="1"){ ?>
																		<i  class="fa <?php echo $alat->icon_code ?> fa-5x"></i>
																	<?php } else { ?>
																		<img src="img/icon_perangkat/<?php echo $alat->gambar ?>" style="width:80px;height:70px">
																	<?php } ?>
																	<br/><?php echo $alat->merek ?>
																	</center>
																</td>
																<td style="width: 50%;"><b><?php echo $alat->nama_perangkat ?></b></td>
															</tr>
															<tr>
																<td>KET : <?php echo $alat->id_module ?>-<?php echo $alat->pin_alat ?></td>
															</tr>
															<tr>
																<td><?php if($alat->ket=="" or $alat->ket==null) { echo "-"; } else { echo $alat->ket; }  ?></td>
															</tr>
															<tr>
																<?php 
																	$status="";
																	$checked="";
																	if($alat->status=="1"){
																		$status="true";
																		$checked="checked";
																	}
																	else {
																		$status="false";
																		$checked="";
																	}
																?>
																<td >
																	<br/>
																	<b>
																	<input type="checkbox" name="my-checkbox_<?php echo $alat->kode_my_alat ?>" <?php echo $checked ?> onchange="send_value('<?php echo $alat->kode_my_alat?>')"  id="my-checkbox_<?php echo $alat->kode_my_alat ?>" class="checkbox_switch" ></b>
																
																	
																	<br/><br/>
																	<div style="display:block">
																		<i  title="Info" onclick="info()" class="fa fa-info"></i> 
																	</div>
																</td>
															</tr>
														</table>
													</div>
											</div>
										</div>
								</div>
								<script>
									check_checked('my-checkbox_<?php echo $alat->kode_my_alat ?>','<?php echo $status ?>');
								</script>
								<?php } ?>
								
								<script>
									function info(){
										swal(
											  '',
											  'Jika anda On/Off manual alat ini <br/>  maka pararental control alat ini menjadi tidak aktif <br/> Kecuali menggunakan timer'
											)
									}
									
									
									function send_value(id){
										var send_param = $('#my-checkbox_'+id).bootstrapSwitch('state');
										$.ajax({
											url: 'crud/alat_update_status.php?mode='+window.btoa('1'), 
											type: 'POST',
											data: 'param='+window.btoa(id)+'&param2='+window.btoa(send_param)
										});
										
										var status="";
										if(send_param==true){
											status="1";
										}
										else {
											status="0";
										}
									}
									
									
									function show_costum(){
										var val_chart = $("#chart_option").val();
										if(val_chart=="5"){
											$(".kostum_data").show();
										}
										else {
											$(".kostum_data").hide();
										}
									}
									function chart_all_detail(){
										var kode_alat = $("#kode_alat").val();
										var val_chart = $("#chart_option_dt").val();
										var tgl_chart = $("#tgl_chart_dt").val();
										
										if(val_chart==""){
											$.ajax({
													  type:"POST",
													  dataType:"html",
													  url:'pages/chart_alat_detail.php',
													  data: 'kode_my_alat='+kode_alat+'&val_chart='+window.btoa('2'), 
													  success:function(hasil){
														$('#chart_all_dt').html(hasil);
													  }
											});
										}
										else {
											if(val_chart=="5"){
												if(tgl_chart==""){
													alert("Tanggal tidak boleh kosong");
												}
												else {
													$.ajax({
													  type:"POST",
													  dataType:"html",
													  url:'pages/chart_alat_detail.php',
													  data: 'kode_my_alat='+kode_alat+'&val_chart='+window.btoa(val_chart)+'&param2='+window.btoa(tgl_chart), 
													  success:function(hasil){
														$('#chart_all_dt').html(hasil);
													  }
													});
												}
											}
											else {
												$.ajax({
													  type:"POST",
													  dataType:"html",
													  url:'pages/chart_alat_detail.php',
													  data: 'kode_my_alat='+kode_alat+'&val_chart='+window.btoa(val_chart), 
													  success:function(hasil){
														$('#chart_all_dt').html(hasil);
													  }
												});
											}
										}
									}
									
							
									
								</script>
						</div>
					</div>
			 <?php } else { echo $no_akses; } ?>
			<!-- jQuery SlimScroll -->
<script src="vendor/jquery-slimscroll/jquery.slimscroll.min.js" charset="utf-8"></script>
		<div class="decoration decoration-margins"></div>
	</div>
</div>
		
	<?php } else  { ?>
 <!-- Highchart -->
<script src="vendor/highchart/highcharts.js"></script>
<script src="vendor/highchart/exporting.js"></script>
<div class="page">
        <header class="ribbon">
            <h2 onclick="call_url('dashboard_detail/<?php echo base64_encode($param) ?>')" style="cursor:pointer">
                <?php echo $pr->nama_perangkat ?>
            </h2>
            <ol class="breadcrumb">
                 <li class=""><a  style="cursor:pointer;text-decoration:none" onclick="location.reload();">Home</a></li>
                 <li class="active" onclick="call_url('dashboard_detail/<?php echo base64_encode($param) ?>')" style="cursor:pointer"><?php echo $pr->nama_perangkat ?></li>
            </ol>
        </header>
		
		
        <div class="page-content">
        <div class="container-fluid">
			<?php if($fa->fua_read=="1") { ?>
			<div class="modal fade" id="myModal_Add" role="dialog">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close"  onclick="clear_form('f1')" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">Tambah Alat <b><?php echo $pr->nama_perangkat ?></b></h4>
					</div>
					<form  id="f1">
						<div class="modal-body">
								<div class="row">
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
											<div class="form-group ">
												<label>Merek <span class="required">*</span></label>
												<select name="merek" class="form-control">
													<option value="">Pilih Merek</option>
													<?php 
													$data_merek = mysqli_query($con,"select * from tb_merek where is_deleted='0'");
													while($mrk=mysqli_fetch_object($data_merek)){ ?>
														<option value="<?php echo $mrk->kode_merek ?>"><?php echo $mrk->merek ?></option>
													<?php } ?>
												</select>
											</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
											<div class="form-group ">
												<label>Module <span class="required">*</span></label>
												<input type="hidden" name="perangkat" value="<?php echo $pr->kode_perangkat ?>">
												<select onchange="get_pin()" name="module" id="module" class="form-control">
													<option value="">Pilih Module</option>
													<?php 
													$data_md = mysqli_query($con,"select * from tb_module where is_deleted='0'");
													while($md=mysqli_fetch_object($data_md)){ ?>
														<option value="<?php echo base64_encode($md->kode_module) ?>"><?php echo $md->nama_module ?></option>
													<?php } ?>
												</select>
											</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
											<div class="form-group ">
												<label>PIN <span class="required">*</span></label>
												<select name="pin_alat" id="pin_alat"  class="form-control">
													<option value="">Pilih PIN</option>
												</select>
											</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
											<div class="form-group ">
												<label>Keterangan</label>
												<textarea class="form-control" name="ket" rows="3"></textarea>
											</div>
									</div>
								</div>
						</div>
						<div class="modal-footer">
						  <button type="submit" class="btn btn-success" >Simpan</button>
						  <button type="button" onclick="clear_form('f1')" class="btn btn-danger" data-dismiss="modal">Tutup</button>
						</div>
					</form>
				  </div>
				</div>
			</div>
			
			<div class="modal fade" id="myModal_Edit" role="dialog">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close"  onclick="clear_form('f2')" data-dismiss="modal">&times;</button>
					  <h4 class="modal-title">Ubah Alat</h4>
					</div>
					<form  id="f2">
						<div class="modal-body" id="modal_edit"></div>
						<div class="modal-footer">
						  <button type="submit" class="btn btn-success" >Ubah</button>
						  <button type="button" onclick="clear_form('f2')" class="btn btn-danger" data-dismiss="modal">Tutup</button>
						</div>
					</form>
				  </div>
				</div>
			</div>
			
			<script>
			function check_checked(id,my_status){
				$("#"+id).bootstrapSwitch({onColor: 'success',offColor: 'danger'});
				$("#"+id).bootstrapSwitch(my_status);
			}
			</script>
           
		    <div class="row" id="my_div">
				<div class="col-md-4">
						<div class="myscroll" style="height: 500px;">
								<?php
								$data_alat = mysqli_query($con,"select md.id_module,SUBSTR(alt.keterangan , 1, 10) as ket,alt.pin_alat,alt.kode_my_alat,alt.status,mrk.merek,pr.nama_perangkat,pr.jenis_icon,pr.gambar,pr.icon_code,md.nama_module 
																	from tb_my_alat as alt
																	inner join tb_module as md on md.kode_module=alt.kode_module
																	inner join tb_perangkat as pr on pr.kode_perangkat=alt.kode_perangkat
																	inner join tb_merek as mrk on mrk.kode_merek=alt.kode_merek
																	where alt.is_deleted='0' and pr.kode_perangkat='".$param."' order by alt.pin_alat asc");
								while($alat=mysqli_fetch_object($data_alat)){ ?>
								<div class="panel panel-default">
									<div class="panel-body ">
											<div class="chart-container">
													<div style="cursor:pointer;" onclick="detail_alat('<?php echo base64_encode($alat->kode_my_alat) ?>')" title="Klik untuk melihat detail alat">
														<table style="width:100%">
															<tr>
																<td rowspan="3" style="margin-right: 20px;">
																	<center>
																	<?php if($alat->jenis_icon=="1"){ ?>
																		<i  class="fa <?php echo $alat->icon_code ?> fa-5x"></i>
																	<?php } else { ?>
																		<img src="img/icon_perangkat/<?php echo $alat->gambar ?>" style="width:80px;height:70px">
																	<?php } ?>
																	<br/><?php echo $alat->merek ?>
																	</center>
																</td>
																<td style="width: 50%;"><b><?php echo $alat->nama_module ?></b></td>
															</tr>
															<tr>
																<td>KET : <?php echo $alat->id_module ?>-<?php echo $alat->pin_alat ?></td>
															</tr>
															<tr>
																<td><?php echo $alat->ket ?></td>
															</tr>
															<tr>
																<?php 
																	$status="";
																	$checked="";
																	if($alat->status=="1"){
																		$status="true";
																		$checked="checked";
																	}
																	else {
																		$status="false";
																		$checked="";
																	}
																?>
																<td ><b><input type="checkbox" name="my-checkbox_<?php echo $alat->kode_my_alat ?>" <?php echo $checked ?> onchange="send_value('<?php echo $alat->kode_my_alat?>')"  id="my-checkbox_<?php echo $alat->kode_my_alat ?>" class="checkbox_switch" ></b>&nbsp;&nbsp;&nbsp;&nbsp; <i title="Info" onclick="info()" class="fa fa-info"></i>
																</td>
															</tr>
														</table>
													</div>
											</div>
										</div>
								</div>
								<script>
									check_checked('my-checkbox_<?php echo $alat->kode_my_alat ?>','<?php echo $status ?>');
								</script>
								<?php } ?>
								
								<script>
									function info(){
										swal(
											  '',
											  'Jika anda On/Off manual alat ini <br/>  maka pararental control alat ini menjadi tidak aktif <br/> Kecuali menggunakan timer'
											)
									}
									
									function send_value(id){
										var send_param = $('#my-checkbox_'+id).bootstrapSwitch('state');
										$.ajax({
											url: 'crud/alat_update_status.php?mode='+window.btoa('1'), 
											type: 'POST',
											data: 'param='+window.btoa(id)+'&param2='+window.btoa(send_param)
										});
										
										var status="";
										if(send_param==true){
											status="1";
										}
										else {
											status="0";
										}
									}
								</script>
						</div>
					</div>
				<div class="col-md-8" >
					<div class="myscroll"  style="height: 500px;">
					    <!--<div class="panel panel-default">-->
						<div style="background-color: white;">
							<div class="panel-body" id="div_data">
								<div class="row">
									<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
											<div class="form-group ">
												<select name="chart_option" id="chart_option" onchange="show_costum()" class="form-control select2">
													<option value="2">Hari</option>
													<option value="6">Minggu</option>
													<option value="3">Bulan</option>
													<option value="4">Tahun</option>
													<option value="5">Kostum</option>
												</select>
											</div>
									</div>
									<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
											<div class="form-group ">
												<select name="satuan" id="satuan"  class="form-control select2">
													<?php 
														$data_st = mysqli_query($con,"SELECT DISTINCT(satuan) FROM tb_pemakaian_daya");
														while($st=mysqli_fetch_object($data_st)){ ?>
															<option value="<?php echo $st->satuan ?>"><?php echo $st->satuan ?></option>
														<?php } ?>
												</select>
											</div>
									</div>
									<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12" id="param_cst_week">
											<div class="form-group kostum_data">
												<input type="text" placeholder="Tanggal *" name="tgl_chart" readonly style="background-color:white" id="tgl_chart" class="form-control datepicker">
											</div>
											<div class="form-group minggu_data">
												<input type='text' name='minggu_chart' id='minggu_chart' class='form-control daterangepicker7days' style='background-color:white' readonly>
											</div>
									</div>
									<div class="col-md-1 col-lg-1 col-sm-12 col-xs-12">
											<button type="button" onclick="chart_all()" class="btn btn-info">Submit</button>
									</div>
									<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
												<br/>
												<div class="windows8">
													<div class="wBall" id="wBall_1">
														<div class="wInnerBall"></div>
													</div>
													<div class="wBall" id="wBall_2">
														<div class="wInnerBall"></div>
													</div>
													<div class="wBall" id="wBall_3">
														<div class="wInnerBall"></div>
													</div>
													<div class="wBall" id="wBall_4">
														<div class="wInnerBall"></div>
													</div>
													<div class="wBall" id="wBall_5">
														<div class="wInnerBall"></div>
													</div>
												</div>
									</div>
								</div>
								<br/>
								
								<div id="chart_all">
									
								</div>
							</div>
						</div>
					</div>
				</div>
            </div>
            
			<script>
					$("#param_cst_week").hide();
					$("#satuan").val('kWh');
					chart_all();
					$('.select2').select2({ width: '100%' });
					$(".kostum_data").hide();
					$(".minggu_data").hide();
					function show_costum(){
						var val_chart = $("#chart_option").val();
						if(val_chart=="5"){
							$("#param_cst_week").show();
							$(".minggu_data").hide();
							$(".kostum_data").show();
						}
						else if(val_chart=="6"){
							$("#param_cst_week").show();
							$(".kostum_data").hide();
							$(".minggu_data").show();
						}
						else {
							$("#param_cst_week").hide();
							$(".kostum_data").hide();
							$(".minggu_data").hide();
						}
					}
					function chart_all(){
						$(".windows8").show();	
						var val_chart   = $("#chart_option").val();
						var satuan 	  	= $("#satuan").val();
						var week_chart  = $("#minggu_chart").val();
						var tgl_chart   = $("#tgl_chart").val();
						
						if(val_chart==""){
								$('#chart_all').html("");	
								$.ajax({
									  type:"POST",
									  dataType:"html",
									  url:'pages/chart_dashboard_detail.php',
									  data: 'kode_perangkat=<?php echo base64_encode($param) ?>&param4='+window.btoa('kWh')+'&val_chart='+window.btoa('2'), 
									  success:function(hasil){
										$(".windows8").hide();  
										$('#chart_all').html(hasil);
									  }
								});
						}
						else {
							if(satuan==""){
								$(".windows8").hide();
								alert("Satuan tidak terpilih");								
							}
							else {
								  if(val_chart=="5"){
										if(tgl_chart==""){
											$(".windows8").hide();	
											alert("Tanggal tidak boleh kosong");
										}
										else {
											$('#chart_all').html("");
											$.ajax({
											  type:"POST",
											  dataType:"html",
											  url:'pages/chart_dashboard_detail.php',
											  data: 'param4='+window.btoa(satuan)+'&kode_perangkat=<?php echo base64_encode($param) ?>&val_chart='+window.btoa(val_chart)+'&param2='+window.btoa(tgl_chart), 
											  success:function(hasil){
												$(".windows8").hide();  
												$('#chart_all').html(hasil);
											  }
											});
										}
								}
								else if(val_chart=="6"){
										if(week_chart==""){
											$(".windows8").hide();
											alert("Tanggal tidak boleh kosong");		
										}
										else {
											$('#chart_all').html("");
											$.ajax({
											  type:"POST",
											  dataType:"html",
											  url:'pages/chart_dashboard_detail.php',
											  data: 'param4='+window.btoa(satuan)+'&kode_perangkat=<?php echo base64_encode($param) ?>&val_chart='+window.btoa(val_chart)+'&param3='+window.btoa(week_chart), 
											  success:function(hasil){
												$(".windows8").hide();	  
												$('#chart_all').html(hasil);
											  }
											});
										}
								}
								else {
									$('#chart_all').html("");
									$.ajax({
										  type:"POST",
										  dataType:"html",
										  url:'pages/chart_dashboard_detail.php',
										  data: 'param4='+window.btoa(satuan)+'&kode_perangkat=<?php echo base64_encode($param) ?>&val_chart='+window.btoa(val_chart), 
										  success:function(hasil){
											$(".windows8").hide();	   
											$('#chart_all').html(hasil);
										  }
									});
								}
							}	
						}
					}
					
					function show_costum_dt(){
						var val_chart = $("#chart_option_dt").val();
						if(val_chart=="5"){
							$("#param_cst_week_dt").show();
							$(".minggu_data_dt").hide();
							$(".kostum_data_dt").show();
						}
						else if(val_chart=="6"){
							$("#param_cst_week_dt").show();
							$(".kostum_data_dt").hide();
							$(".minggu_data_dt").show();
						}
						else {
							$("#param_cst_week_dt").hide();
							$(".kostum_data_dt").hide();
							$(".minggu_data_dt").hide();
						}
					}
					function chart_all_detail(){
						$("#windows8_dt").show();
						var kode_alat = $("#kode_alat").val();
						var val_chart   = $("#chart_option_dt").val();
						var satuan 	  	= $("#satuan_dt").val();
						var week_chart  = $("#minggu_chart_dt").val();
						var tgl_chart   = $("#tgl_chart_dt").val();
						
						if(val_chart==""){
								$('#chart_all_dt').html("");	
								$.ajax({
									  type:"POST",
									  dataType:"html",
									  url:'pages/chart_alat_detail.php',
									  data: 'kode_my_alat='+kode_alat+'&param4='+window.btoa('kWh')+'&val_chart='+window.btoa('2'), 
									  success:function(hasil){
										$("#windows8_dt").hide();  
										$('#chart_all_dt').html(hasil);
									  }
								});
						}
						else {
							if(satuan==""){
								$("#windows8_dt").hide();
								alert("Satuan tidak terpilih");								
							}
							else {
								  if(val_chart=="5"){
										if(tgl_chart==""){
											$("#windows8_dt").hide();
											alert("Tanggal tidak boleh kosong");
										}
										else {
											$('#chart_all_dt').html("");
											$.ajax({
											  type:"POST",
											  dataType:"html",
											  url:'pages/chart_alat_detail.php',
											  data: 'param4='+window.btoa(satuan)+'&kode_my_alat='+kode_alat+'&val_chart='+window.btoa(val_chart)+'&param2='+window.btoa(tgl_chart), 
											  success:function(hasil){
												$("#windows8_dt").hide();
												$('#chart_all_dt').html(hasil);
											  }
											});
										}
								}
								else if(val_chart=="6"){
										if(week_chart==""){
											$("#windows8_dt").hide();
											alert("Tanggal tidak boleh kosong");		
										}
										else {
											$('#chart_all_dt').html("");
											$.ajax({
											  type:"POST",
											  dataType:"html",
											  url:'pages/chart_alat_detail.php',
											  data: 'param4='+window.btoa(satuan)+'&kode_my_alat='+kode_alat+'&val_chart='+window.btoa(val_chart)+'&param3='+window.btoa(week_chart), 
											  success:function(hasil){
												$("#windows8_dt").hide();	  
												$('#chart_all_dt').html(hasil);
											  }
											});
										}
								}
								else {
									$('#chart_all_dt').html("");
									$.ajax({
										  type:"POST",
										  dataType:"html",
										  url:'pages/chart_alat_detail.php',
										  data: 'param4='+window.btoa(satuan)+'&kode_my_alat='+kode_alat+'&val_chart='+window.btoa(val_chart), 
										  success:function(hasil){
											$("#windows8_dt").hide();	   
											$('#chart_all_dt').html(hasil);
										  }
									});
								}
							}	
						}
						
						
					}
					
					var mywindow = $(window).height()-178;
					 $('.myscroll').slimScroll({
						height: mywindow+'px'
					});
					
					function show_edit(alat_id){
						$('#modal_edit').html("");
						$.ajax({
						  type:"POST",
						  dataType:"html",
						  url:'crud/alat_data.php',
						  data: 'tipe=dashboard_alat&param='+alat_id, 
						  success:function(hasil){
							$('#modal_edit').html(hasil);
						  }
						});
					}
					
					function laporan(div_laporan){
							var myalat 		= $("#myalat_"+div_laporan).val();
							var myjenis 	= $("#jenis_"+div_laporan).val();
							var mysatuan 	= $("#satuan_"+div_laporan).val();
							var mydate 		= $("#my_date_"+div_laporan).val();
							var my_week 	= $("#my_week_"+div_laporan).val();
							
							
							var form = document.createElement("form");
								form.setAttribute("method", "POST");
								form.setAttribute("action", 'cetak_laporan.php');
								
										//alat
										var alat = document.createElement("input");
										alat.setAttribute("type", "hidden");
										alat.setAttribute("name", "alat");
										alat.setAttribute("value", myalat);
										
										//jenis
										var jenis = document.createElement("input");
										jenis.setAttribute("type", "hidden");
										jenis.setAttribute("name", "jenis");
										jenis.setAttribute("value", myjenis);
										
										//satuan
										var satuan = document.createElement("input");
										satuan.setAttribute("type", "hidden");
										satuan.setAttribute("name", "satuan");
										satuan.setAttribute("value", mysatuan);
										
									    //date
										var tgl = document.createElement("input");
										tgl.setAttribute("type", "hidden");
										tgl.setAttribute("name", "my_date");
										tgl.setAttribute("value", mydate);
										
										//week
										var week = document.createElement("input");
										week.setAttribute("type", "hidden");
										week.setAttribute("name", "my_week");
										week.setAttribute("value", my_week);
					
										form.appendChild(alat);
										form.appendChild(jenis);
										form.appendChild(satuan);
										form.appendChild(tgl);
										form.appendChild(week);
										
								document.body.appendChild(form);
								form.submit();
					}
					
					function get_pin(){
						var kode_module = $("#module").val();
						$.ajax({
						  type:"POST",
						  dataType:"html",
						  url:'crud/get_pin.php',
						  data: 'param='+kode_module, 
						  success:function(hasil){
							$('#pin_alat').html(hasil);
						  }
						});
					}
					
					function get_pin2(){
						var kode_module = $("#module2").val();
						$.ajax({
						  type:"POST",
						  dataType:"html",
						  url:'crud/get_pin.php',
						  data: 'param='+kode_module, 
						  success:function(hasil){
							$('#pin_alat2').html(hasil);
						  }
						});
					}
					
					
					function detail_alat(alat_id){
						$('#div_data').html("");
						$.ajax({
						  type:"POST",
						  dataType:"html",
						  url:'crud/alat_detail.php',
						  data: 'param='+alat_id, 
						  success:function(hasil){
							$('#div_data').html(hasil);
							$("#chart_option_dt").val('2');
							$(".kostum_data_dt").hide();
						  }
						});
						setTimeout( function() {  window.location.hash = '#div_data'; }, 500 );
					}
					
					//Add Alat
					$("#f1").on("submit", function (event) {
					event.preventDefault();
						do_act_alat('f1','crud/alat_add_dashboard.php','Tambah Alat','Apakah data yang diisi sudah benar ?','info');
					});
					
					//Update Alat
					$("#f2").on("submit", function (event) {
					event.preventDefault();
						do_act_alat('f2','crud/alat_update.php','Ubah Alat','Apakah data yang diisi sudah benar ?','warning');
					});
					
					function do_act_alat(form_id,act_controller,header_text,content_text,jenis_icon){
                        swal({
                          title: header_text,
                          text:  content_text,
                          type: jenis_icon,      // warning,info,success,error
                          showCancelButton: true,
                          showLoaderOnConfirm: true,
                          preConfirm: function(){
                            $.ajax({
                                url: act_controller, 
                                type: 'POST',
                                data: new FormData($('#'+form_id)[0]),  // Form ID
                                processData: false,
                                contentType: false,
                                success: function(data) {
                                    if(data=="OK")
                                    {
                                        swal({
                                            title: 'Success',
                                            type: 'success',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          }).then(function() {
												$('#myModal_Add').modal('hide');
												$('#myModal_Edit').modal('hide');
												$('body').removeClass('modal-open');
												$('.modal-backdrop').remove();
												call_url('dashboard_detail/<?php echo $_GET['param']; ?>');  
										  });
                                    } 
                                    else if(data=="NOT_LOGIN")
                                    {
                                          swal({
                                            title: 'Error',
                                            text: "You Must Login Again",
                                            type: 'error',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          },function(){
                                                window.location = '<?php echo $basepath ?>';
                                          });
                                    }
                                    else
                                    {
                                        swal({
                                            title: 'Error',
                                            html: data,
                                            type: 'error',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          });
                                    }
                                }
                            });
                         }
						});   
					}
					
					function do_delete(send_param){
                        swal({
                          title: 'Hapus Alat',
                          text: 'Apakah anda ingin menghapus alat ini',
                          type: 'warning',      // warning,info,success,error
                          showCancelButton: true,
                          showLoaderOnConfirm: true,
                          preConfirm: function(){
                            $.ajax({
                                url: 'crud/alat_delete.php', 
                                type: 'POST',
                                data: 'param='+send_param, 
                                success: function(data) {
                                    if(data=="OK")
                                    {
                                        swal({
                                            title: 'Success',
                                            type: 'success',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          }).then(function() {
											call_url('dashboard_detail/<?php echo $_GET['param']; ?>');  
										  });
                                    } 
                                    else if(data=="NOT_LOGIN")
                                    {
                                          swal({
                                            title: 'Error',
                                            text: "You Must Login Again",
                                            type: 'error',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          },function(){
                                                location.reload();
                                          });
                                    }
                                    else
                                    {
                                        swal({
                                            title: 'Error',
                                            html: data,
                                            type: 'error',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          });
                                    }
                                }
                            });
                         }
						});   
					}
					$('.datepicker').datepicker({
						orientation: "bottom",
						autoclose: true,
						todayHighlight: true,
						format: "yyyy-mm-dd"
					});
					
					$('.daterangepicker7days').daterangepicker({
						"dateLimit": {
							"days": 7
						},
						"locale": {
							"format": "DD/MM/YYYY",
						},
						"autoApply": true,
						"opens": "left"
					});	
			</script>
			<?php } else { echo $no_akses; } ?>	
        </div>
        </div>
    </div>
    <!-- /.container-fluid -->
	<?php } ?>