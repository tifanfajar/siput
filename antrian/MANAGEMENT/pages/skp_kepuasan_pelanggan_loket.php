<?php 
include '../../config.php'; 
session_start();
?>


<div class="page">
        <header class="ribbon">
            <h2>
                Kepuasan Pelanggan Loket
            </h2>
            <ol class="breadcrumb">
              <li><a href="#" onclick="location.reload();" style="text-decoration:none">Home</a></li>
              <li><a href="#" style="text-decoration:none">SKP</a></li>
              <li><a href="#" style="text-decoration:none">Kepuasan Pelanggan</a></li>
              <li class="active">Loket</li>
            </ol>
        </header>
        <div class="page-content">
        <div class="container-fluid">
           
            <!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                            <table class="table datatable">
                                <thead>
                                    <tr>
                                        <th><center>Loket</center></th>
                                        <th><center>Total</center></th>
                                        <th><center>Sangat Puas</center></th>
                                        <th><center>Puas</center></th>
                                        <th><center>Tidak Puas</center></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.page-content -->
    </div>
    <!-- /.page -->
	<script>
		var dTable;
			$(document).ready(function() {
				dTable = $('.datatable').DataTable( {
					"bProcessing": true,
					"bServerSide": true,
					"bJQueryUI": false,
					"responsive": false, 
					"sAjaxSource": "serverside/skp_kepuasan_pelanggan_loket.php", 
					"sServerMethod": "POST",
					"scrollX": true,
					"columnDefs": [
					{ "orderable": true,  "width": "5%", "targets": 0, "searchable": true},
                    { "orderable": false,  "width": "5%", "targets": 1, "searchable": false },
                    { "orderable": false,  "width": "5%", "targets": 2, "searchable": false },
                    { "orderable": false,  "width": "5%", "targets": 3, "searchable": false },
					{ "orderable": false,  "width": "5%", "targets": 4, "searchable": false }
					]
				} );
			
				$('.datatable').removeClass( 'display' ).addClass('table table-striped table-bordered');
				var i = 0;
				$('.table').find( 'tfoot tr th' ).each( function () {
					//Agar kolom Action Tidak Ada Tombol Pencarian
					
					i++;
				} );
				
				dTable.columns().every( function () {
					var that = this;
					$( 'input', this.footer() ).on( 'keyup change', function () {
						that
						.search( this.value )
						.draw();
					} );
				});
				
				dTable.columns().every( function () {
					var that = this;
					$( 'select', this.footer() ).on( 'change', function () {
						that
						.search( this.value )
						.draw();
					} );
				});
				
				$('.datepicker').datepicker({
					orientation: "top",
					autoclose: true,
					todayHighlight: true,
					format: "yyyy-mm-dd"
				});
			} );
			
			
			
	</script>