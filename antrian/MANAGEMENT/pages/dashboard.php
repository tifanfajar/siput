<?php 
session_start();
 ?>
<div class="page">

        <header class="ribbon">
            <h2>
                Dashboard
            </h2>
            <ol class="breadcrumb">
                 <li class=""><a href="#">Home</a></li>
                 <li class="active">Dashboard</li>
            </ol>
        </header>
        <div class="page-content">
        <div class="container-fluid">
		    <div class="row">
				
		    	<div class="col-lg-6">
                    <a style="cursor:pointer;text-decoration:none" href="../KIOSK" target="_blank" >
						<div class="panel panel-raised" >
							<div class="panel-body ">
								<div class="chart-container">
										<center>
										 <h6>KIOSK</h6>
										 <i class="fa fa-id-badge fa-5x"></i>
										</center>
								</div>
							</div>
						</div>
					</a>
                </div>

                <div class="col-lg-6">
                    <a style="cursor:pointer;text-decoration:none" href="../CSO" target="_blank" >
						<div class="panel panel-raised" >
							<div class="panel-body ">
								<div class="chart-container">
										<center>
										 <h6>LOKET</h6>
										 <i class="fa fa-users fa-5x"></i>
										</center>
								</div>
							</div>
						</div>
					</a>
                </div>

                <div class="col-lg-6">
                    <a style="cursor:pointer;text-decoration:none" href="../PLASMA" target="_blank" >
						<div class="panel panel-raised" >
							<div class="panel-body ">
								<div class="chart-container">
										<center>
										 <h6>PLASMA</h6>
										 <i class="fa fa-television fa-5x"></i>
										</center>
								</div>
							</div>
						</div>
					</a>
                </div>

                <div class="col-lg-6">
                    <a style="cursor:pointer;text-decoration:none" href="../SKP" target="_blank" >
						<div class="panel panel-raised" >
							<div class="panel-body ">
								<div class="chart-container">
										<center>
										 <h6>Sistem Kepuasan Pelanggan (SKP)</h6>
										 <i class="fa fa-comment fa-5x"></i>
										</center>
								</div>
							</div>
						</div>
					</a>
                </div>



            </div>
			
        </div>
        </div>
    </div>
    <!-- /.container-fluid -->