  <?php
    include '../../config.php'; 
	$param = base64_decode($_GET['param']);
	$data    = mysqli_fetch_object(mysqli_query($con,"select * from ms_layanan where ID_LAYANAN='".$param."'  "));
	?>
 <div class="page">
        <header class="ribbon">
            <h2>
                Ubah Kouta Layanan
            </h2>
            <ol class="breadcrumb">
              <li><a href="#" onclick="location.reload();" style="text-decoration:none">Home</a></li>
              <li><a href="#" style="text-decoration:none">KIOSK</a></li>
              <li><a onclick="call_url('kiosk_kouta_layanan.php')" style="cursor:pointer;text-decoration:none">Daftar Kouta Layanan</a></li>
              <li class="active">Ubah Kouta Layanan</li>
            </ol>
        </header>
        <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form  id="f1">
                                <div class="row">	
									<div class="col-lg-2 col-md-2 col-sm-10 col-xs-12">	
										<div class="form-group ">
											<label>Kode Layanan <span class="required">*</span></label>
											<input value="<?php echo $data->KODE_LAYANAN ?>" type="text" name="kode_layanan" maxlength="25" class="form-control" readonly>
											<input value="<?php echo $_GET['param'] ?>" type="hidden" name="param" >
										</div>
									</div>
									<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">	
										<div class="form-group ">
											<label>Nama Layanan <span class="required">*</span></label>
											<input value="<?php echo $data->NAMA_LAYANAN ?>" type="text" name="nama_layanan" maxlength="100" class="form-control" readonly >
										</div>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">	
										<div class="form-group ">
											<label>Kouta Layanan <span class="required">*</span></label>
											<input value="<?php echo $data->KOUTA ?>" type="text" name="kouta"  class="form-control" >
										</div>
									</div>
                                </div>
								<div class="row"><br/>	
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
									   <center>
										   <button type="button" onclick="call_url('kiosk_kouta_layanan.php')" class="btn btn-warning">Kembali</button>&nbsp;
										   <button type="button" onclick="clear_form('f1')"  class="btn btn-danger">Reset</button>&nbsp;
										   <button type="submit" class="btn btn-success">Submit</button>
										</center>
									</div>
                                </div>
                            </form>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel-primary panel -->
                </div>
            </div>
	   </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.page-content -->
    </div>
    <!-- /.page -->
	<script>
		//Link
		$(function() {
			$( '.url' ).on( 'keydown', function( e ) {
				if( !$( this ).data( "value" ) )
					 $( this ).data( "value", this.value );
			});
			$( '.url' ).on( 'keyup', function( e ) {
				if (!/^[_0-9a-z]*$/i.test(this.value))
					this.value = $( this ).data( "value" );
				else
					$( this ).data( "value", null );
			});
		});
		$("#f1").on("submit", function (event) {
		event.preventDefault();
			do_act('f1','crud/kiosk_kouta_layanan_update.php','kiosk_kouta_layanan.php','Ubah Kouta Layanan','Apakah data yang di isi sudah benar ?','info');
		});
	</script>