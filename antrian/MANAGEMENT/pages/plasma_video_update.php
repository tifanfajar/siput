  <?php
    include '../../config.php'; 
	$param = base64_decode($_GET['param']);
	$data    = mysqli_fetch_object(mysqli_query($con,"select * from data_video_web where ID_DATA_VIDEO_WEB='".$param."'  "));
	?>
 <div class="page">
        <header class="ribbon">
            <h2>
                Ubah Video
            </h2>
            <ol class="breadcrumb">
              <li><a href="#" onclick="location.reload();" style="text-decoration:none">Home</a></li>
              <li><a href="#" style="text-decoration:none">Plasma</a></li>
              <li><a onclick="call_url('plasma_video.php')" style="cursor:pointer;text-decoration:none">Daftar Video</a></li>
              <li class="active">Ubah Video</li>
            </ol>
        </header>
        <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form  id="f1" enctype="multipart/form-data">
                                <div class="row">	
									
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">	
										<div class="form-group ">
											<label>Video (mp4)</label>
											<input  type="file" id="video" name="video"  >
											 <small>Kosongkan jika tidak ingin mengganti video</small>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">	
										<div class="form-group ">
											<label>Nama Video <span class="required">*</span></label>
											 <input  value="<?php echo $data->NAMA_VIDEO ?>" maxlength="255" type="text" class="form-control" id="nama_video" name="nama_video" required>
											<input value="<?php echo $_GET['param'] ?>" type="hidden" name="param" >
										</div>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">	
										<div class="form-group ">
											<label>Status <span class="required">*</span></label>
											<select class="form-control" name="status" id="status">
												    <option value="">Pilih Status</option>
												    <option value="1">Aktif</option>
	      											<option value="0">Tidak Aktif</option>
											</select>
										</div>
									</div>
                                </div>
								<div class="row"><br/>	
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
									   <center>
										   <button type="button" onclick="call_url('plasma_video.php')" class="btn btn-warning">Kembali</button>&nbsp;
										   <button type="button" onclick="clear_form('f1')"  class="btn btn-danger">Reset</button>&nbsp;
										   <button type="submit" class="btn btn-success">Submit</button>
										</center>
									</div>
                                </div>
                            </form>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel-primary panel -->
                </div>
            </div>
	   </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.page-content -->
    </div>
    <!-- /.page -->
	<script>
		$("#status").val("<?php echo $data->STATUS ?>");
		//Link
		$(function() {
			$( '.url' ).on( 'keydown', function( e ) {
				if( !$( this ).data( "value" ) )
					 $( this ).data( "value", this.value );
			});
			$( '.url' ).on( 'keyup', function( e ) {
				if (!/^[_0-9a-z]*$/i.test(this.value))
					this.value = $( this ).data( "value" );
				else
					$( this ).data( "value", null );
			});
		});
		$("#f1").on("submit", function (event) {
		event.preventDefault();
			do_act('f1','crud/plasma_video_update.php','plasma_video.php','Ubah Video','Apakah data yang di isi sudah benar ?','info');
		});
	</script>