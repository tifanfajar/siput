<?php 
include '../../config.php'; 
session_start();
?>
<style>
	.table th {
	   text-align: center;   
	   font-weight:bold;
	}
	.datatable>tbody>tr>td
	{
		white-space: nowrap;
	}
</style>
<div class="page">
        <header class="ribbon">
            <h2>
                Kouta Layanan
            </h2>
            <ol class="breadcrumb">
              <li><a href="#" onclick="location.reload();" style="text-decoration:none">Home</a></li>
              <li><a href="#" style="text-decoration:none">KIOSK</a></li>
              <li class="active">Kouta Layanan</li>
            </ol>
        </header>
        <div class="page-content">
        <div class="container-fluid">
           
            <!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                            <table class="table datatable">
                                <thead>
                                    <tr>
                                        <th>Fungsi</th>
                                        <th>Kode Layanan</th>
                                        <th>Nama Layanan</th>
                                        <th>Kouta Layanan</th>
                                        <th>Tanggal Buat</th>
										<th>Tanggal Update</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
								<tfoot>
                                    <tr>
                                        <th>Fungsi</th>
                                        <th>Kode Layanan</th>
                                        <th>Nama Layanan</th>
                                        <th>Kouta Layanan</th>
                                        <th>Tanggal Buat</th>
										<th>Tanggal Update</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.page-content -->
    </div>
    <!-- /.page -->
	<script>
		var dTable;
			$(document).ready(function() {
				dTable = $('.datatable').DataTable( {
					"bProcessing": true,
					"bServerSide": true,
					"bJQueryUI": false,
					"responsive": false, 
					"sAjaxSource": "serverside/kiosk_kouta_layanan.php", 
					"sServerMethod": "POST",
					"scrollX": true,
					"columnDefs": [
					{ "orderable": false,  "targets": 0, "searchable": false},
					{ "orderable": true,   "targets": 1, "searchable": true },
					{ "orderable": true,   "targets": 2, "searchable": true },
					{ "orderable": true,   "targets": 3, "searchable": true },
                    { "orderable": true,   "targets": 4, "searchable": true },
					{ "orderable": true,   "targets": 5, "searchable": true }
					]
				} );
			
				$('.datatable').removeClass( 'display' ).addClass('table table-striped table-bordered');
				var i = 0;
				$('.table').find( 'tfoot tr th' ).each( function () {
					//Agar kolom Action Tidak Ada Tombol Pencarian
					if( $(this).text() == "Tanggal Buat" || $(this).text() == "Tanggal Update"  ){
						var width = $(".sorting_1").width();
						var title = $('.table thead th').eq( $(this).index() ).text();
						$(this).html( '<input type="text" placeholder="Cari '+title+'" class="form-control datepicker"  style="background-color:white;width:'+width+'" />' );
					}
					else if( $(this).text() != "Fungsi" ){
						var width = $(".sorting_1").width();
						var title = $('.table thead th').eq( $(this).index() ).text();
						$(this).html( '<input type="text" placeholder="Cari '+title+'" class="form-control" style="width:'+width+'" />' );
					}
					i++;
				} );
				
				dTable.columns().every( function () {
					var that = this;
					$( 'input', this.footer() ).on( 'keyup change', function () {
						that
						.search( this.value )
						.draw();
					} );
				});
				
				dTable.columns().every( function () {
					var that = this;
					$( 'select', this.footer() ).on( 'change', function () {
						that
						.search( this.value )
						.draw();
					} );
				});
				
				$('.datepicker').datepicker({
					orientation: "top",
					autoclose: true,
					todayHighlight: true,
					format: "yyyy-mm-dd"
				});
			} );
	</script>