<?php include '../../config.php';   ?>
 <div class="page">
        <header class="ribbon">
            <h2>
                Tambah Group Layanan
            </h2>
            <ol class="breadcrumb">
              <li><a href="#" onclick="location.reload();" style="text-decoration:none">Home</a></li>
              <li><a href="#" style="text-decoration:none">Loket</a></li>
              <li><a onclick="call_url('loket_group_layanan.php')" style="cursor:pointer;text-decoration:none">Group Layanan</a></li>
              <li class="active">Tambah Group Layanan</li>
            </ol>
        </header>
        <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <form  id="f1" enctype="multipart/form-data">
								<div class="row">	
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">	
										<div class="form-group ">
											<label>Pelayanan <span class="required">*</span></label>
											<select name="pelayanan" id="pelayanan" class="form-control">
									          <option value="">Pilih Pelayanan</option>
									          <?php 
									              $dt_pl = mysqli_query($con,"select * from ms_layanan");
									              while ($pl=mysqli_fetch_object($dt_pl)){  ?>
									                   <option value="<?php echo $pl->ID_LAYANAN ?>"><?php echo $pl->NAMA_LAYANAN ?></option>
									        <?php } ?>
									      </select>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">	
										<div class="form-group ">
											<label>Group Layanan <span class="required">*</span></label>
											<input type="text" name="grp_layanan" maxlength="50" class="form-control" >
										</div>
									</div>
                                </div>
								<div class="row"><br/>	
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
									   <center>
										   <button type="button" onclick="call_url('loket_group_layanan.php')" class="btn btn-warning">Kembali</button>&nbsp;
										   <button type="reset"  class="btn btn-danger">Reset</button>&nbsp;
										   <button type="submit" class="btn btn-success">Submit</button>
										</center>
									</div>
                                </div>
                            </form>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel-primary panel -->
                </div>
            </div>
	   </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.page-content -->
    </div>
    <!-- /.page -->
	<script>
		//Link
		$(function() {
			$( '.url' ).on( 'keydown', function( e ) {
				if( !$( this ).data( "value" ) )
					 $( this ).data( "value", this.value );
			});
			$( '.url' ).on( 'keyup', function( e ) {
				if (!/^[_0-9a-z]*$/i.test(this.value))
					this.value = $( this ).data( "value" );
				else
					$( this ).data( "value", null );
			});
		});
		$("#f1").on("submit", function (event) {
		event.preventDefault();
			do_act('f1','crud/loket_group_layanan_add.php','loket_group_layanan.php','Tambah Group Layanan','Apakah data yang di isi sudah benar ?','info');
		});
	</script>