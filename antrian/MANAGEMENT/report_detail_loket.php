<?php 
	include "../config.php"; 
	include "function/otentifikasi.php"; 
	session_start();
	$user = mysqli_fetch_object(mysqli_query($con,"select 
			us.*,fa.function_access 
			from tb_user as us
			inner join tb_function_access as fa	
				on us.level=fa.kode_function_access 
		where kode_user='".$_SESSION['kode_user']."' "));
	$param  = base64_decode($_REQUEST['param']);
	$param_header  = base64_decode($_REQUEST['param']);
	$param2 = base64_decode($_REQUEST['param2']);


	if(!empty($param)){
		$param = "and LOKET='".$param."'";
	}
	$tipe   = $_REQUEST['tipe'];
	if($tipe=="pdf"){
		error_reporting(null);
		ini_set('max_execution_time', 9999999999999); 
		require_once 'function/mpdf/vendor/autoload.php';
		$mydata ='<h3 style="text-align:center">Report Detail Loket '.$param_header.'</h3><br/>
					<table style="margin-left:auto;margin-right:auto;width:70%;border: 1px solid black;">
						<tr style="border: 1px solid black;">
							<th style="border: 1px solid black;">Layanan</th>
							<th style="border: 1px solid black;">No. Antrian</th>
							<th style="border: 1px solid black;">Tanggal & Jam</th>
							<th style="border: 1px solid black;">Nama Pemohon</th>
						</tr>';
			$data_rpt = mysqli_query($con,"SELECT  *
						FROM data_antrian							
					  where 1=1 ".$param.$param2." order by -ID_LAYANAN DESC,TANGGAL ASC");
		while ($aRow=mysqli_fetch_array($data_rpt)) {


			$mydata .=' <tr style="border: 1px solid black;">
							<td style="border: 1px solid black;"><center>'.$aRow['LAYANAN'].'</center></td>
							<td style="border: 1px solid black;"><center>'.$aRow['NO_URUT'].'</center></td>
							<td style="border: 1px solid black;"><center>'.$aRow['TANGGAL'].'</center></td>
							<td style="border: 1px solid black;"><center>'.$aRow['NAMA'].'</center></td>
						</tr>';
		} 

		$mydata .='</table>';

		$filename = "Report Detail Loket ".$param_header." ".date('Ymdhis');
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
		$mpdf->AddPage('A4-L');
		$mpdf->WriteHTML($mydata);
		$mpdf->Output($filename,'I');
	}
	else {
	if($tipe=="xls"){
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=Report-Detail-Loket-".$param_header."-".date('Ymdhis').".xls");
	}
	else if($tipe=="doc"){
		header("Content-Type: application/vnd.ms-word");
		header("Content-Disposition: attachment; filename=Report-Detail-Loket-".$param_header."-".date('Ymdhis').".doc");
	}
	?>
	<style type="text/css">
		table {
	    	border-collapse: collapse;
		}
		table, th, td {
		    border: 1px solid black;
		}
	</style>
	<center><h3>Report Detail Loket <?php echo $param_header ?></h3></center><br/>
	<table style="width:100%">
		<tr>
			<th>Layanan</th>
			<th>No. Antrian</th>
			<th>Tanggal & Jam</th>
			<th>Nama Pemohon</th>
		</tr>
		<?php
		$data_rpt = mysqli_query($con,"SELECT  *
						FROM data_antrian							
					  where 1=1 ".$param.$param2." order by -ID_LAYANAN DESC,TANGGAL ASC");
		while ($aRow=mysqli_fetch_array($data_rpt)) { 


			?>
		<tr style="height:50px;">
			<td><?php echo "<center>".$aRow['LAYANAN']."<center>"; ?></td>
			<td><?php echo "<center>".$aRow['NO_URUT']."<center>";  ?></td>
			<td><?php echo "<center>".$aRow['TANGGAL']."</center>"  ?></td>
			<td><?php echo "<center>".$aRow['NAMA']."</center>"  ?></td>
		</tr>
		<?php } ?>
	</table>
	<?php } ?>