<?php
error_reporting(null);
session_start();
include "../config.php";
include "function/otentifikasi.php";
if(ISSET($_SESSION['username']))
{
	date_default_timezone_set("Asia/Jakarta"); 
	insert_log($con,'Logout/Keluar',$ip,$agent,$_SESSION['kode_user'],$tgl_indo_full,'');
	
	session_destroy();

	//--- unset cookie -----
	setcookie("sessid", "", time()-3600);
	
	echo "<script language='JavaScript'>document.location='.'</script>";
}

	echo "<script language='JavaScript'>document.location='.'</script>";
?>