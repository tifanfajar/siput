<?php
include '../../../config.php'; 
error_reporting(0);
mb_internal_encoding('UTF-8');
		$aColumns = array('grp.group_layanan','sub.sub_group_layanan','jns.jenis_permohonan','pmh.NAMA','dt.NO_URUT','dt.ID_DATA_ANTRIAN'); //Kolom Pada Tabel

		// Indexed column (used for fast and accurate table cardinality)
		$sIndexColumn = 'ID_DATA_ANTRIAN';

		// DB table to use
		$sTable  = 'data_antrian'; // Nama Tabel
		$sTable2  = 'data_group_layanan'; // Nama Tabel
		$sTable3  = 'data_sub_group_layanan'; // Nama Tabel
		$sTable4  = 'data_jenis_permohonan'; // Nama Tabel
		$sTable5  = 'data_biodata'; // Nama Tabel

		// Input method (use $_GET, $_POST or $_REQUEST)
		$input =& $_POST;


		$iColumnCount = count($aColumns);

		$db = mysqliConnection();
		$sLimit = Paging( $input );
		$sOrder = Ordering( $input, $aColumns );
		$sWhere = Filtering( $aColumns, $iColumnCount, $input, $db );

		$aQueryColumns = array();
		foreach ($aColumns as $col) {
			if ($col != ' ') {
				$aQueryColumns[] = $col;
			}
		}
		$lkt = base64_decode($_REQUEST['loket']);
		$ly = mysqli_fetch_object(mysqli_query($con,"select ID_LAYANAN from tr_layanan_loket where LOKET='".$lkt."' "));
		$sQuery = "SELECT SQL_CALC_FOUND_ROWS pmh.NAMA,dt.ID_DATA_ANTRIAN,grp.group_layanan,sub.sub_group_layanan,jns.jenis_permohonan,pmh.NAMA,dt.NO_URUT
						FROM ".$sTable." as dt
							left outer join  ".$sTable2." as grp
								on grp.id_group_layanan=dt.ID_GROUP_LAYANAN
							left outer join  ".$sTable3." as sub
								on sub.id_sub_group_layanan=dt.ID_SUB_GROUP_LAYANAN
							left outer join  ".$sTable4." as jns
								on jns.id_jenis_permohonan=dt.ID_JNS_PERMOHONAN
							left outer join  ".$sTable5." as pmh
								on pmh.ID_BIODATA=dt.ID_BIODATA									
					".$sWhere." and dt.ID_LAYANAN='".$ly->ID_LAYANAN."'   and  dt.STAT_PANGILAN='1' and dt.TANGGAL like '".date("Y-m-d")."%' ".$sOrder.$sLimit;

		$rResult = $db->query( $sQuery ) or die($db->error);
		// Data set length after filtering
		$sQuery = "SELECT FOUND_ROWS()";
		$rResultFilterTotal = $db->query( $sQuery ) or die($db->error);
		list($iFilteredTotal) = $rResultFilterTotal->fetch_row();

		// Total data set length
		$sQuery = "SELECT COUNT(dt.".$sIndexColumn.") 
					FROM ".$sTable." as dt
							left outer join  ".$sTable2." as grp
								on grp.id_group_layanan=dt.ID_GROUP_LAYANAN
							left outer join  ".$sTable3." as sub
								on sub.id_sub_group_layanan=dt.ID_SUB_GROUP_LAYANAN
							left outer join  ".$sTable4." as jns
								on jns.id_jenis_permohonan=dt.ID_JNS_PERMOHONAN
							left outer join  ".$sTable5." as pmh
								on pmh.ID_BIODATA=dt.ID_BIODATA		
					".$sWhere."  and dt.ID_LAYANAN='".$ly->ID_LAYANAN."'   and  dt.STAT_PANGILAN='1' and dt.TANGGAL like '".date("Y-m-d")."%' ";
					
					
		$rResultTotal = $db->query( $sQuery ) or die($db->error);
		list($iTotal) = $rResultTotal->fetch_row();

		/**
			* Output
		*/
		$output = array(
		"sEcho"                => intval($input['sEcho']),
		"iTotalRecords"        => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData"               => array(),
		);

		// Looping Data
		while ( $aRow = $rResult->fetch_assoc())
		{

				$edit ='<a href="javascript:void(0)" data-toggle="modal" data-target="#edit_history"  title="Edit" class="btn btn-primary btn-aksi" style="cursor:pointer" onclick="edit_history(\''.base64_encode($aRow['ID_DATA_ANTRIAN']).'\')" >Edit</a>';

				$hapus='';
				// $hapus ='<a title="Hapus" href="#" onClick="do_delete(\''.base64_encode($aRow['IdAgama']).'\')" class="btn btn-danger btn-aksi" ><i class="fa fa-trash"></i></a>';
			

			$editresethapus = $edit." &nbsp; ".$hapus;

			$row = array();
			$row = array($aRow['group_layanan'],$aRow['sub_group_layanan'],$aRow['jenis_permohonan'],"<center>".$aRow['NO_URUT']."</center>",$aRow['NAMA'],$editresethapus);
			$output['aaData'][] = $row;
		}

		echo json_encode( $output );

?>
