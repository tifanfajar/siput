<?php
include '../../../config.php'; 
error_reporting(0);
mb_internal_encoding('UTF-8');
		$aColumns = array('pmh.NAMA','pmh.NIK','pmh.JENIS_KELAMIN','pmh.NO_TLP','pmh.NO_HP','pmh.ALAMAT','pmh.ID_BIODATA'); //Kolom Pada Tabel

		// Indexed column (used for fast and accurate table cardinality)
		$sIndexColumn = 'ID_BIODATA';

		// DB table to use
		$sTable  = 'data_biodata'; // Nama Tabel

		// Input method (use $_GET, $_POST or $_REQUEST)
		$input =& $_POST;


		$iColumnCount = count($aColumns);

		$db = mysqliConnection();
		$sLimit = Paging( $input );
		$sOrder = Ordering( $input, $aColumns );
		$sWhere = Filtering( $aColumns, $iColumnCount, $input, $db );

		$aQueryColumns = array();
		foreach ($aColumns as $col) {
			if ($col != ' ') {
				$aQueryColumns[] = $col;
			}
		}

		$sQuery = "SELECT SQL_CALC_FOUND_ROWS pmh.* FROM ".$sTable." as pmh
					".$sWhere." ".$sOrder.$sLimit;

		$rResult = $db->query( $sQuery ) or die($db->error);
		// Data set length after filtering
		$sQuery = "SELECT FOUND_ROWS()";
		$rResultFilterTotal = $db->query( $sQuery ) or die($db->error);
		list($iFilteredTotal) = $rResultFilterTotal->fetch_row();

		// Total data set length
		$sQuery = "SELECT COUNT(pmh.".$sIndexColumn.") FROM ".$sTable." as pmh ".$sWhere;
		$rResultTotal = $db->query( $sQuery ) or die($db->error);
		list($iTotal) = $rResultTotal->fetch_row();

		/**
			* Output
		*/
		$output = array(
		"sEcho"                => intval($input['sEcho']),
		"iTotalRecords"        => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData"               => array(),
		);

		// Looping Data
		while ( $aRow = $rResult->fetch_assoc())
		{

				$edit ='<a href="javascript:void(0)" data-toggle="modal" data-target="#edit_pemohon"  title="Edit" class="btn btn-primary btn-aksi" style="cursor:pointer" onclick="edit_biodata(\''.base64_encode($aRow['ID_BIODATA']).'\')" >Edit</a>';

				$nama ='<span style="cursor:pointer" onclick="get_name_pmh2(\''.base64_encode($aRow['ID_BIODATA']).'\')" >'.$aRow['NAMA'].'</span>';
			
				$hapus='';
				// $hapus ='<a title="Hapus" href="#" onClick="do_delete(\''.base64_encode($aRow['IdAgama']).'\')" class="btn btn-danger btn-aksi" ><i class="fa fa-trash"></i></a>';
			

			$editresethapus = $edit." &nbsp; ".$hapus;

			$row = array();
			$row = array($nama,$aRow['NIK'],$aRow['JENIS_KELAMIN'],$aRow['NO_TLP'],$aRow['NO_HP'],$aRow['ALAMAT'],$editresethapus);
			$output['aaData'][] = $row;
		}

		echo json_encode( $output );

?>
