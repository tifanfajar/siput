<?php
include '../../../config.php'; 
error_reporting(0);
mb_internal_encoding('UTF-8');
		$aColumns = array('cmp.company_name','cmp.no_tlp_company','cmp.alamat','cmp.company_id'); //Kolom Pada Tabel

		// Indexed column (used for fast and accurate table cardinality)
		$sIndexColumn = 'company_id';

		// DB table to use
		$sTable  = 'data_company'; // Nama Tabel

		// Input method (use $_GET, $_POST or $_REQUEST)
		$input =& $_POST;


		$iColumnCount = count($aColumns);

		$db = mysqliConnection();
		$sLimit = Paging( $input );
		$sOrder = Ordering( $input, $aColumns );
		$sWhere = Filtering( $aColumns, $iColumnCount, $input, $db );

		$aQueryColumns = array();
		foreach ($aColumns as $col) {
			if ($col != ' ') {
				$aQueryColumns[] = $col;
			}
		}

		$sQuery = "SELECT SQL_CALC_FOUND_ROWS cmp.* FROM ".$sTable." as cmp
					".$sWhere." ".$sOrder.$sLimit;

		$rResult = $db->query( $sQuery ) or die($db->error);
		// Data set length after filtering
		$sQuery = "SELECT FOUND_ROWS()";
		$rResultFilterTotal = $db->query( $sQuery ) or die($db->error);
		list($iFilteredTotal) = $rResultFilterTotal->fetch_row();

		// Total data set length
		$sQuery = "SELECT COUNT(cmp.".$sIndexColumn.") FROM ".$sTable." as cmp ".$sWhere;
		$rResultTotal = $db->query( $sQuery ) or die($db->error);
		list($iTotal) = $rResultTotal->fetch_row();

		/**
			* Output
		*/
		$output = array(
		"sEcho"                => intval($input['sEcho']),
		"iTotalRecords"        => $iTotal,
		"iTotalDisplayRecords" => $iFilteredTotal,
		"aaData"               => array(),
		);

		// Looping Data
		while ( $aRow = $rResult->fetch_assoc())
		{

				$edit ='<a data-toggle="modal" data-target="#edit_company" href="javascript:void(0)" title="Edit" class="btn btn-primary btn-aksi" style="cursor:pointer" onclick="edit_company(\''.base64_encode($aRow['company_id']).'\')" >Edit</a>';

				$nama ='<span style="cursor:pointer" onclick="get_name_cmp(\''.base64_encode($aRow['company_id']).'\')" >'.$aRow['company_name'].'</span>';
			
				$hapus='';
				// $hapus ='<a title="Hapus" href="#" onClick="do_delete(\''.base64_encode($aRow['IdAgama']).'\')" class="btn btn-danger btn-aksi" ><i class="fa fa-trash"></i></a>';
			

			$editresethapus = $edit." &nbsp; ".$hapus;

			$row = array();
			$row = array($nama,$aRow['no_tlp_company'],$aRow['alamat'],$editresethapus);
			$output['aaData'][] = $row;
		}

		echo json_encode( $output );

?>
