<?php 
	include '../config.php'; 
	include 'function/otentifikasi.php'; 
	
	$user = mysqli_fetch_object(mysqli_query($con,"select * from data_petugas_loket where USER_NAME='".$_SESSION['username']."'"));
	$get_layanan = mysqli_fetch_object(mysqli_query($con,"select 
			ly.NAMA_LAYANAN,ly.ID_LAYANAN,ly.DURASI
			from tr_layanan_loket as lk 
			inner join  ms_layanan as ly 
				on ly.ID_LAYANAN=lk.ID_LAYANAN 
			where lk.LOKET='".$_SESSION['loket']."'"));
	$layanan = (!empty($get_layanan->NAMA_LAYANAN) ? $get_layanan->NAMA_LAYANAN : '');
	$durasi_total = (!empty($get_layanan->DURASI) ? $get_layanan->DURASI : '0');
?>
<!DOCTYPE html>
<html>
<head>
	<title>CSO - KOMINFO</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
	<meta name="viewport" content="width=device-width">
	<link rel="icon" href="assets/logo.png">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<link rel="stylesheet" type="text/css" href="js/sweetalert2/dist/sweetalert2.min.css">
	<link rel="stylesheet" type="text/css" href="css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="js/datepicker/dist/css/bootstrap-datepicker.min.css">
	<style type="text/css">
	@media (min-width: 1200px)
	.container {
		width: 840px !important;
	}
	.blink_me {
	  animation: blinker 1s linear infinite;
	}
	@keyframes blinker {  
	  50% { opacity: 0; }
	}
	.must_fill{
		color:red;
	}
	/*
	.btn-antrian-layanan{
		width: 293px;
		height: 100px;
		display: block;
		position: absolute;
		bottom: -190px;
		right: 0px;
	}*/
	.btn-antrian-layanan {
		width: 182px;
		height: 100px;
		display: block;
		position: absolute;
		bottom: -22px;
		right: -189px;
	}
	/*
	.btn-antrian-history{
		width: 257px;
		height: 100px;
		display: block;
		position: absolute;
		bottom: -190px;
		right: 299px;
	}*/
	.btn-antrian-history {
		width: 182px;
		height: 100px;
		display: block;
		position: absolute;
		bottom: -78px;
		right: -189px;
	}
	/*
	.btn-antrian-refresh{
		width: 240px;
		height: 100px;
		display: block;
		position: absolute;
		bottom: -190px;
		right: 560px;
	}*/
	.btn-antrian-refresh {
		width: 182px;
		height: 100px;
		display: block;
		position: absolute;
		bottom: -134px;
		right: -189px;
	}
	.antrian-layanan{
		width: 100%;
		height: 50px;
		display: block;
		/*background-color: #ffae00;*/

		background: rgba(255,174,0,1);
		background: -moz-linear-gradient(top, rgba(255,174,0,1) 0%, rgba(255,144,0,1) 100%);
		background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(255,174,0,1)), color-stop(100%, rgba(255,144,0,1)));
		background: -webkit-linear-gradient(top, rgba(255,174,0,1) 0%, rgba(255,144,0,1) 100%);
		background: -o-linear-gradient(top, rgba(255,174,0,1) 0%, rgba(255,144,0,1) 100%);
		background: -ms-linear-gradient(top, rgba(255,174,0,1) 0%, rgba(255,144,0,1) 100%);
		background: linear-gradient(to bottom, rgba(255,174,0,1) 0%, rgba(255,144,0,1) 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffae00', endColorstr='#ff9000', GradientType=0 );

		text-align: center;
		line-height: 45px;
		color: #201f1f;
		font-size: 15px;
		text-transform: uppercase;
		font-weight: bold;
	}
	.antrian-layanan:hover{
		text-decoration: none;
	}
	.antrian-history{
		width: 100%;
		right: 293px;
		height: 50px;
		display: block;
		/*background-color: #ffae00;*/
		
		background: rgba(255,174,0,1);
		background: -moz-linear-gradient(top, rgba(255,174,0,1) 0%, rgba(255,144,0,1) 100%);
		background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(255,174,0,1)), color-stop(100%, rgba(255,144,0,1)));
		background: -webkit-linear-gradient(top, rgba(255,174,0,1) 0%, rgba(255,144,0,1) 100%);
		background: -o-linear-gradient(top, rgba(255,174,0,1) 0%, rgba(255,144,0,1) 100%);
		background: -ms-linear-gradient(top, rgba(255,174,0,1) 0%, rgba(255,144,0,1) 100%);
		background: linear-gradient(to bottom, rgba(255,174,0,1) 0%, rgba(255,144,0,1) 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffae00', endColorstr='#ff9000', GradientType=0 );


		text-align: center;
		line-height: 45px;
		color: #201f1f;
		font-size: 15px;
		text-transform: uppercase;
		font-weight: bold;
	}
	.antrian-history:hover{
		text-decoration: none;
	}
	.antrian-refresh{
		width: 100%;
		right: 293px;
		height: 50px;
		display: block;
		/*background-color: #ffae00;*/
		
		background: rgba(255,174,0,1);
		background: -moz-linear-gradient(top, rgba(255,174,0,1) 0%, rgba(255,144,0,1) 100%);
		background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(255,174,0,1)), color-stop(100%, rgba(255,144,0,1)));
		background: -webkit-linear-gradient(top, rgba(255,174,0,1) 0%, rgba(255,144,0,1) 100%);
		background: -o-linear-gradient(top, rgba(255,174,0,1) 0%, rgba(255,144,0,1) 100%);
		background: -ms-linear-gradient(top, rgba(255,174,0,1) 0%, rgba(255,144,0,1) 100%);
		background: linear-gradient(to bottom, rgba(255,174,0,1) 0%, rgba(255,144,0,1) 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffae00', endColorstr='#ff9000', GradientType=0 );


		text-align: center;
		line-height: 45px;
		color: #201f1f;
		font-size: 15px;
		text-transform: uppercase;
		font-weight: bold;
	}
	.antrian-refresh:hover{
		text-decoration: none;
	}
	.timer-div{
		background-color: #0008;
		display: block;
		width: 244px;
		height: 125px;
		position: absolute;
		right: -14px;
		z-index: 100;
	}
	.number-text{
		color: #fff;
		font-size: 60px;
		font-weight: bold;
		/*text-align: center;*/
		margin-top: -11px;
		margin-left: 63px;
		position: relative;
	}
	.mulai{
		cursor:pointer;
		text-decoration:none;
		display: block;
		position: absolute;
		right: 0px;
		/*top: 0px;*/
		width: 100%;
		background-color: #ff9000;
		padding: 10px;
		height: 30%;
		text-align: center;
		/*line-height: 8;*/
		color: #393838;
		font-weight: bold;
		bottom: 0px;
	}
	.mulai_disabled {
		cursor: not-allowed;
		text-decoration:none;
		display: block;
		position: absolute;
		right: 0px;
		/*top: 0px;*/
		width: 100%;
		background-color: #ff9000;
		padding: 10px;
		height: 30%;
		text-align: center;
		/*line-height: 8;*/
		color: #393838;
		font-weight: bold;
		bottom: 0px;
	}
	.marq-bar{position:fixed;}
	</style>
</head>
<body>
	<div class="line-back"></div>
	<div class="logo-center">
		<img class="logo" src="assets/headTitle.png">
	<!-- User Picture -->
	<div class="info-text">
		<h5><?php echo ucwords($user->Petugas) ?></h5>
		<label><?php echo tgl_indo(date("Y-m-d")) ?></label> | <a href="javascript:void(0)" data-toggle="modal" data-target="#myModalLogout">Logout</a>
	</div>
	<div class="frame">
		<!--<img class="fto-box" src="assets/fto.jpg" width="100%">-->
		<?php echo '<img class="fto-box" src="data:image/jpeg;base64,'.base64_encode($user->IMAGE).'" style="width:100%;height:70px" >'; ?>
	</div>
	<!-- End Pict -->
	</div>
	<div class="container" style="width: 840px; margin-left:13%;">
		<div class="row">
			<div class="col-md-7">
				<div class="text-row">
					<h2 class="title-1">Loket <?php echo $_SESSION['loket'] ?></h2>
					<h1 class="title-2"><?php echo $layanan ?></h1>
				</div>
				<?php  if(!empty($durasi_total)){ ?>
					<div class="timer-div">
						<small style="color: #fff; margin-left: 10px; margin-top: 5px; display: block;">Timer</small>
						<h1 class="number-text" id="timer_loket">00:00</h1>
						<a id="btn_mulai" style="text-decoration:none;" onclick="mulai()" class="mulai"  >Mulai</a>
					</div>
				<?php } ?>
				<div class="row">
					<div class="col-md-6">
						<div class="box-sisa">
							<p>Total Antrian</p>
							<h1 id="antrian_total"></h1>
						</div>
					</div>
					<div class="col-md-6">
						<div class="box-antrian">
							<p>Antrian saat ini</p>
							<h1 id="antrian_now">-</h1>
							<input type="hidden" id="id_panggilan" >
							<input type="hidden" id="id_panggilan_speaker" >
							<input type="hidden" value="" name="id_data_antrian" id="id_data_antrian">
							<input type="hidden" value="" name="durasi_total" id="durasi_total">
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-5 pull-right cc">
				<div class="next-line">
					<h2 id="antrian_next"></h2>
					<a href="javascript:void(0)" onclick="konfirmasi_next_panggilan()" class="btn-next-call"></a>
					<a href="javascript:void(0)" onclick="get_repeat_antrian()" class="btn-repeat"></a>
					<a href="javascript:void(0)" onclick="refresh_table();" data-toggle="modal" data-target="#history" class="btn-bio" ></a>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 5px;">
			<div class="col-md-12">
	
			<div class="box-sisa2" style="margin-top: 0px;">
			<p>Jumlah antrian tersisa</p>
			<h1 id="antrian_sisa"></h1>
			</div>	

			<div class="fast-call pull-right">
				<div class="row">
					<div class="col-md-6" style="position: relative;">
					<input type="text" ID="no_antrian_cepat" class="text-fast" name="" placeholder="Nomor Antrian">
					</div>
					<div class="col-md-6" style="position: relative;">
						<a href="javascript:void(0)" onclick="get_fast_antrian();" class="btn-fastCall"></a>
					</div>
				</div>
				<div class="btn-antrian-layanan">
					<a style="cursor:pointer;text-decoration:none;color:white" onclick="show_antrian_layanan()" class="antrian-layanan">Antrian Layanan</a>
				</div>
				<div class="btn-antrian-history">
					<a style="cursor:pointer;text-decoration:none;color:white" onclick="show_antrian_histori()" class="antrian-history">History Antrian</a>
				</div>
				<div class="btn-antrian-refresh">
					<a style="cursor:pointer;text-decoration:none;color:white" onclick="refresh()" class="antrian-refresh">Refresh</a>
				</div>
			</div>
				
			</div>
			<div class="col-md-12 send-to pull-right">
				<a href="javascript:void(0);" class="btn-visitor" data-toggle="modal" data-target="#visitor"></a>
				<input type="text" id="running_text" maxlength="255" class="text-send" autocomplete="off"  placeholder="Masukan Pesan">
				<a href="javascript:void(0);" onclick="send_running_text()" class="btn-sendTo"></a>
			</div>
		</div>
	</div>

	<div class="marq-bar">
		<div class="time">
			<h2><span id="jam"></span><span class="blink_me" id="titik">:</span><span id="menit"></span></h2>
		</div>
		<?php 
			$running_text = mysqli_query($con,"select * from app_running_text_cso where STATUS='AKTIF'");
			$rn = "";
			$separator = "&nbsp; <img style='width:25px;height:25px' src='assets/logo.png'> &nbsp;";
			while ($data_rn = mysqli_fetch_object($running_text)){
				$rn .= $data_rn->TEXT.$separator;
			}
		?>
		<marquee><?php echo $rn ?></marquee>
	</div>


	
	<!-- Start Modal Logout -->
		<div id="myModalLogout" class="modal fade" role="dialog">
		  <div class="modal-dialog" style="width: 350px;">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header" style="background-color: #337ab7;color: white;">
				<button style="color: white;" type="button"  class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-power-off"></i> Keluar</h4>
			  </div>
			  <form method="POST" id="form_keluar">
				 <div class="modal-body">
						<div class="row">
							<div class="col-sm-12 col-md-12" style="background: url(images/lk-back.png) no-repeat center center fixed; ">
								Masukan Password<br/>
								<input type="password" name="pwd" class="form-control" id="pwd">
							</div>
						</div>
				  </div>
				  <div class="modal-footer">
					<button type="submit"  id="keluar_btn" class="btn btn-danger" ><i class="fa fa-power-off"></i> Keluar</button>
				  </div>
			  </form>
			</div>

		  </div>
		</div>
	<!-- End Modal Logout -->

	<!-- Modal -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Biodata</h4>
				</div>
				<div class="modal-body" id="detail_biodata">
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>

	<!-- Modal Visitor-->
	<div class="modal fade" id="visitor" role="dialog">
		<div class="modal-dialog" style="border: 2px solid #424242; border-radius: 7px;">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header2">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" style="text-align: center; font-weight: unset; font-size: 13px;">Keterangan Data Pemohon</h4>
				</div>
				<!--<div class="bar-title">
					Data Pemohon
				</div>-->
				<div class="modal-body">
					<form autocomplete="off" class="form-horizontal" method="POST" id="f1">
						  <div style="height: 430px;overflow-y: scroll;overflow-x: hidden;">
								  <div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Nama <span class="must_fill">*</span></label>
									<div class="col-sm-7">
									  <input readonly maxlength="50"  id="nama_pemohon" name="nama_pemohon" type="text" class="form-control" required>
									  <a href="#" onclick="refresh_table()" data-toggle="modal" data-target="#picker_pemohon" style="text-decoration:none">Cari Data Pemohon</a>
									  <input type="hidden" id="id_pemohon"  name="id_pemohon" required>
									  <input type="hidden" id="loket" value="<?php echo $_SESSION['loket'] ?>" name="loket" required>
									  <input type="hidden" id="no_urut" name="no_urut" required>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Instansi / Company <span class="must_fill">*</span></label>
									<div class="col-sm-7">
									  <input readonly maxlength="150" name="instansi" type="text" class="form-control" id="instansi" required>
									  <a href="#" onclick="refresh_table()" data-toggle="modal" data-target="#picker_company" style="text-decoration:none">Cari Data Instansi / Company</a>
									  <input type="hidden" id="instansi_id"  name="instansi_id" required>
									</div>
								  </div>
								  <div id="div_grup_layanan" class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Jenis Layanan <span class="must_fill">*</span></label>
									<div class="col-sm-7">
										<select disabled class="form-control" id="jenis_layanan" required>
										   <option value="">Pilih Jenis Layanan</option>
										  <?php 
								              $dt_pl = mysqli_query($con,"select * from ms_layanan");
								              while ($pl=mysqli_fetch_object($dt_pl)){  ?>
								                   <option <?php echo ($pl->ID_LAYANAN==$get_layanan->ID_LAYANAN ? 'selected' : '') ?> value="<?php echo $pl->ID_LAYANAN ?>"><?php echo $pl->NAMA_LAYANAN ?></option>
								        <?php } ?>
										</select>
									<input type="hidden" value="<?php echo $get_layanan->ID_LAYANAN ?>"  name="jenis_layanan">
									</div>
								  </div>
								  <div id="div_grup_layanan" class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Group Layanan <span class="must_fill">*</span></label>
									<div class="col-sm-7">
										<select class="form-control" id="group_layanan" name="group_layanan" onchange="show_sub()" required>
											<option value="">Pilih Group Layanan</option>
										</select>
									</div>
								  </div>
								  <div id="div_sub_grup_layanan" class="form-group" >
									<label for="inputPassword3" class="col-sm-3 control-label">Sub Group Layanan</label>
									<div class="col-sm-7">
										<select class="form-control" id="sub_group_layanan" onchange="get_sertifikasi('sub_group_layanan','sertifikasi','sertifikasi_id')" name="sub_group_layanan"  >
											<option value="">Pilih Sub Group Layanan</option>
										</select>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Jenis Permohonan <span class="must_fill">*</span></label>
									<div class="col-sm-7">
										<select class="form-control" id="jns_permohonan" name="jns_permohonan"  required>
											<option value="">Pilih Jenis Permohonan</option>
											<?php $data_jns = mysqli_query($con,"select * from data_jenis_permohonan order by jenis_permohonan asc");
												  while($jns = mysqli_fetch_object($data_jns)){ ?>
												 <option value="<?php echo $jns->id_jenis_permohonan ?>"><?php echo $jns->jenis_permohonan ?></option>
											<?php } ?>
										</select>
									</div>
								   </div>
									<div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Perihal <span class="must_fill">*</span></label>
									<div class="col-sm-7">
									  <input maxlength="100" name="perihal" type="text" class="form-control" id="perihal" required>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Keterangan</label>
									<div class="col-sm-7">
									  <textarea name="ket"  class="form-control" id="ket" placeholder=""></textarea>
									</div>
								  </div>
								  
								   <input type="hidden" name="sertifikasi" id="sertifikasi_id">
								  
								  <div id="sertifikasi" style="display:none"><br/>
								  	<center><h4>Sertifikasi</h4></center>

								  	  <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">Tanggal Masuk</label>
										<div class="col-sm-7">
										  <input  name="tgl_masuk" type="text" class="form-control tgl" id="tgl_masuk" >
										</div>
									  </div>
									  <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">Tanggal Keluar</label>
										<div class="col-sm-7">
										  <input  name="tgl_keluar" type="text" class="form-control tgl" id="tgl_keluar" >
										</div>
									  </div>
									   <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">Tanggal Pembayaran PNBP</label>
										<div class="col-sm-7">
										  <input  name="tgl_pembayaran_pnbp" type="text" class="form-control tgl" id="tgl_pembayaran_pnbp" >
										</div>
									  </div> 

									  <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">Bank</label>
										<div class="col-sm-7">
										  <select class="form-control" id="bank" name="bank"  >
											<option value="">Pilih Bank</option>
											<?php $data_bank = mysqli_query($con,"select * from data_bank order by id_bank asc");
												  while($bank = mysqli_fetch_object($data_bank)){ ?>
												 <option value="<?php echo $bank->id_bank ?>"><?php echo $bank->nama_bank ?></option>
											<?php } ?>
										</select>
										</div>
									  </div>

									  <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">No. Sertifikat Lama</label>
										<div class="col-sm-7">
										  <input  name="no_sertifikat_lama"  maxlength="100" type="text" class="form-control" id="no_sertifikat_lama" >
										</div>
									  </div>
									  <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">No. Sertifikat Baru</label>
										<div class="col-sm-7">
										  <input  name="no_sertifikat_baru" maxlength="100" type="text" class="form-control" id="no_sertifikat_baru" >
										</div>
									  </div> 

									  <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">Pengurus</label>
										<div class="col-sm-7">
										  <input  name="pengurus"  maxlength="50" type="text" class="form-control" id="pengurus" >
										</div>
									  </div>
									  <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">Contact Pengurus</label>
										<div class="col-sm-7">
										  <input  name="contact_pengurus" maxlength="25" type="text" class="form-control" id="contact_pengurus" >
										</div>
									  </div>

									  <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">Lembaga</label>
										<div class="col-sm-7">
										  <input readonly  name="lembaga" type="text" class="form-control" id="lembaga" required>
										  <a href="#" onclick="refresh_table()" data-toggle="modal" data-target="#picker_lembaga" style="text-decoration:none">Cari Data Lembaga</a>
										  <input type="hidden" id="lembaga_id"  name="lembaga_id" >
										</div>
									  </div>

								  </div>
						 
						  </div>
						</div>
						<div class="modal-footer">
							<button type="reset" class="btn btn-default btn-box">Reset</button>
							<button type="button" class="btn btn-default btn-box" data-dismiss="modal">Batal</button>
							<button type="submit" class="btn btn-default btn-box">Simpan</button>
						</div>
					</form>
			</div>

		</div>
	</div>
	
	<!-- Modal History Visitor-->
	<div class="modal fade" id="edit_history" style="z-index: 10000000 !important;" role="dialog">
		<div class="modal-dialog" style="border: 2px solid #424242; border-radius: 7px;">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header2">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title" style="text-align: center; font-weight: unset; font-size: 13px;">Keterangan Data Pemohon</h4>
				</div>
				<!--<div class="bar-title">
					Data Pemohon
				</div>-->
				<div class="modal-body">
					<form autocomplete="off" class="form-horizontal" method="POST" id="f2">
						  <div style="height: 430px;overflow-y: scroll;overflow-x: hidden;">
								  <div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Nama <span class="must_fill">*</span></label>
									<div class="col-sm-7">
									  <input readonly maxlength="50"  id="nama_pemohon_his" name="nama_pemohon" type="text" class="form-control" required>
									  <a href="#" onclick="refresh_table()" data-toggle="modal" data-target="#picker_pemohon2" style="text-decoration:none">Cari Data Pemohon</a>
									  <input type="hidden" id="id_pemohon_his"  name="id_pemohon" required>
									  <input type="hidden" id="loket_his" value="<?php echo $_SESSION['loket'] ?>" name="loket" required>
									  <input type="hidden" id="no_urut_his" name="no_urut" required>
									  <input type="hidden" id="id_his" name="id_his" required>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Instansi / Company <span class="must_fill">*</span></label>
									<div class="col-sm-7">
									  <input readonly maxlength="150" name="instansi" type="text" class="form-control" id="instansi_his" required>
									  <a href="#" onclick="refresh_table()" data-toggle="modal" data-target="#picker_company2" style="text-decoration:none">Cari Data Instansi / Company</a>
									  <input type="hidden" id="instansi_id_his"  name="instansi_id" required>
									</div>
								  </div>
								  <div id="div_grup_layanan" class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Jenis Layanan <span class="must_fill">*</span></label>
									<div class="col-sm-7">
										<select disabled class="form-control" id="jenis_layanan_his" required>
										   <option value="">Pilih Jenis Layanan</option>
										  <?php 
								              $dt_pl = mysqli_query($con,"select * from ms_layanan");
								              while ($pl=mysqli_fetch_object($dt_pl)){  ?>
								                   <option <?php echo ($pl->ID_LAYANAN==$get_layanan->ID_LAYANAN ? 'selected' : '') ?> value="<?php echo $pl->ID_LAYANAN ?>"><?php echo $pl->NAMA_LAYANAN ?></option>
								        <?php } ?>
										</select>
										<input type="hidden" value="<?php echo $get_layanan->ID_LAYANAN ?>"  name="jenis_layanan">
									</div>
								  </div>
								  <div id="div_grup_layanan" class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Group Layanan <span class="must_fill">*</span></label>
									<div class="col-sm-7">
										<select class="form-control" id="group_layanan_his" name="group_layanan" onchange="show_sub2()" required>
											<option value="">Pilih Group Layanan</option>
										</select>
									</div>
								  </div>
								  <div id="div_sub_grup_layanan" class="form-group" >
									<label for="inputPassword3" class="col-sm-3 control-label">Sub Group Layanan</label>
									<div class="col-sm-7">
										<select class="form-control" id="sub_group_layanan_his"  onchange="get_sertifikasi('sub_group_layanan_his','sertifikasi_his','sertifikasi_id_his')" name="sub_group_layanan"  name="sub_group_layanan"  >
											<option value="">Pilih Sub Group Layanan</option>
										</select>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Jenis Permohonan <span class="must_fill">*</span></label>
									<div class="col-sm-7">
										<select class="form-control" name="jns_permohonan" id="jns_permohonan_his"  required>
											<option value="">Pilih Jenis Permohonan</option>
											<?php $data_jns = mysqli_query($con,"select * from data_jenis_permohonan order by jenis_permohonan asc");
												  while($jns = mysqli_fetch_object($data_jns)){ ?>
												 <option value="<?php echo $jns->id_jenis_permohonan ?>"><?php echo $jns->jenis_permohonan ?></option>
											<?php } ?>
										</select>
									</div>
								   </div>
									<div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Perihal <span class="must_fill">*</span></label>
									<div class="col-sm-7">
									  <input maxlength="100" name="perihal" type="text" class="form-control" id="perihal_his" required>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Keterangan</label>
									<div class="col-sm-7">
									  <textarea name="ket"  class="form-control" id="ket_his" placeholder=""></textarea>
									</div>
								  </div>
								  
								   <input type="hidden" name="sertifikasi" id="sertifikasi_id_his">
								  
								  <div id="sertifikasi_his" style="display:none"><br/>
								  	<center><h4>Sertifikasi</h4></center>

								  	  <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">Tanggal Masuk</label>
										<div class="col-sm-7">
										  <input  name="tgl_masuk" type="text" class="form-control tgl" id="tgl_masuk_his" >
										</div>
									  </div>
									  <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">Tanggal Keluar</label>
										<div class="col-sm-7">
										  <input  name="tgl_keluar" type="text" class="form-control tgl" id="tgl_keluar_his" >
										</div>
									  </div>
									   <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">Tanggal Pembayaran PNBP</label>
										<div class="col-sm-7">
										  <input  name="tgl_pembayaran_pnbp" type="text" class="form-control tgl" id="tgl_pembayaran_pnbp_his" >
										</div>
									  </div> 

									  <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">Bank</label>
										<div class="col-sm-7">
										  <select class="form-control" id="bank_his" name="bank"  >
											<option value="">Pilih Bank</option>
											<?php $data_bank = mysqli_query($con,"select * from data_bank order by id_bank asc");
												  while($bank = mysqli_fetch_object($data_bank)){ ?>
												 <option value="<?php echo $bank->id_bank ?>"><?php echo $bank->nama_bank ?></option>
											<?php } ?>
										</select>
										</div>
									  </div>

									  <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">No. Sertifikat Lama</label>
										<div class="col-sm-7">
										  <input  name="no_sertifikat_lama"  maxlength="100" type="text" class="form-control" id="no_sertifikat_lama_his" >
										</div>
									  </div>
									  <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">No. Sertifikat Baru</label>
										<div class="col-sm-7">
										  <input  name="no_sertifikat_baru" maxlength="100" type="text" class="form-control" id="no_sertifikat_baru_his" >
										</div>
									  </div> 

									  <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">Pengurus</label>
										<div class="col-sm-7">
										  <input  name="pengurus"  maxlength="50" type="text" class="form-control" id="pengurus_his" >
										</div>
									  </div>
									  <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">Contact Pengurus</label>
										<div class="col-sm-7">
										  <input  name="contact_pengurus" maxlength="25" type="text" class="form-control" id="contact_pengurus_his" >
										</div>
									  </div>

									  <div class="form-group">
										<label for="inputPassword3" class="col-sm-3 control-label">Lembaga</label>
										<div class="col-sm-7">
										  <input readonly  name="lembaga" type="text" class="form-control" id="lembaga_his" required>
										  <a href="#" onclick="refresh_table()" data-toggle="modal" data-target="#picker_lembaga2" style="text-decoration:none">Cari Data Lembaga</a>
										  <input type="hidden" id="lembaga_id_his"  name="lembaga_id" >
										</div>
									  </div>

								  </div>
						  </div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default btn-box" data-dismiss="modal">Batal</button>
							<button type="submit" class="btn btn-default btn-box">Simpan</button>
						</div>
					</form>
			</div>

		</div>
	</div>

	<!-- Modal Tambah Company-->
	<div style="z-index: 10000000000 !important;" class="modal fade" id="tambah_company" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header2">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Tambah Data Instansi / Company</h4>
				</div>
				<div class="modal-body">
					<form autocomplete="off" class="form-horizontal" method="POST" id="form_add_company">
						  <div style="height: 430px;overflow-y: scroll;overflow-x: hidden;">
								  <div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Instansi / Company <span class='must_fill'>*</span></label>
									<div class="col-sm-7">
									  <input  maxlength="200"  id="company_name" name="company_name" type="text" class="form-control" required>
									  <input   id="id_user" name="id_user" type="hidden" value="<?php echo $_SESSION['id_petugas'] ?>" required>
									</div>
								  </div>
								   <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">No HP <span class='must_fill'>*</span></label>
									<div class="col-sm-4">
									  <input maxlength="25" name="no_hp" type="text" class="form-control" id="no_hp" required>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">No Telepon </label>
									<div class="col-sm-4">
									  <input maxlength="25" name="no_telepon" type="text" class="form-control" id="no_telepon" >
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Provinsi <span class='must_fill'>*</span></label>
									<div class="col-sm-7">
									  <select onchange="get_kabkot('id_propinsi_add','id_kabkot_add')"  class="form-control" name="provinsi" id="id_propinsi_add" required>
										<option value="">Pilih Provinsi</option>
											<?php $data_prov = mysqli_query($con,"select * from ms_provinsi");
												  while($prov = mysqli_fetch_object($data_prov)){ ?>
												 <option value="<?php echo $prov->id_prov ?>"><?php echo $prov->nama_prov ?></option>
											<?php } ?>
									  </select>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Kota/Kabupaten <span class='must_fill'>*</span></label>
									<div class="col-sm-7">
									  <select class="form-control" name="kabkot" id="id_kabkot_add" required>
										<option value="">Pilih Kota / Kabupaten</option>
									  </select>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Alamat</label>
									<div class="col-sm-7">
									  <textarea name="alamat"  class="form-control" id="alamat" placeholder=""></textarea>
									</div>
								  </div>
						  </div>
						</div>
						<div class="modal-footer">
							<button type="reset" class="btn btn-default btn-box" data-dismiss="modal">Batal</button>
							<button type="submit" class="btn btn-default btn-box">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<!-- Modal Edit Company-->
	<div style="z-index: 10000000000 !important;" class="modal fade" id="edit_company" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header2">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit Data Instansi / Company</h4>
				</div>
				<div class="modal-body">
					<form autocomplete="off" class="form-horizontal" method="POST" id="form_edit_company">
						  <div style="height: 430px;overflow-y: scroll;overflow-x: hidden;">
								  <div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Instansi / Company <span class='must_fill'>*</span></label>
									<div class="col-sm-7">
									  <input  maxlength="200"  id="company_name_edit" name="company_name" type="text" class="form-control" required>
									  <input   id="id_user" name="id_user" type="hidden" value="<?php echo $_SESSION['id_petugas'] ?>" required>
									  <input   id="id_company_edit" name="id_company" type="hidden"  required>
									</div>
								  </div>
								   <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">No HP <span class='must_fill'>*</span></label>
									<div class="col-sm-4">
									  <input maxlength="25" name="no_hp" type="text" class="form-control" id="no_hp_cmp_edit" required>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">No Telepon </label>
									<div class="col-sm-4">
									  <input maxlength="25" name="no_telepon" type="text" class="form-control" id="no_telepon_cmp_edit" >
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Provinsi <span class='must_fill'>*</span></label>
									<div class="col-sm-7">
									  <select onchange="get_kabkot('id_propinsi_edit','id_kabkot_edit')"  class="form-control" name="provinsi" id="id_propinsi_edit" required>
										<option value="">Pilih Provinsi</option>
											<?php $data_prov = mysqli_query($con,"select * from ms_provinsi");
												  while($prov = mysqli_fetch_object($data_prov)){ ?>
												 <option value="<?php echo $prov->id_prov ?>"><?php echo $prov->nama_prov ?></option>
											<?php } ?>
									  </select>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Kota/Kabupaten <span class='must_fill'>*</span></label>
									<div class="col-sm-7">
									  <select class="form-control" name="kabkot" id="id_kabkot_edit" required>
										<option value="">Pilih Kota / Kabupaten</option>
									  </select>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Alamat</label>
									<div class="col-sm-7">
									  <textarea name="alamat"  class="form-control" id="alamat_cmp_edit" placeholder=""></textarea>
									</div>
								  </div>
						  </div>
						</div>
						<div class="modal-footer">
							<button type="reset" class="btn btn-default btn-box" data-dismiss="modal">Batal</button>
							<button type="submit" class="btn btn-default btn-box">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
		
		<!-- Modal Tambah Lembaga-->
	<div style="z-index: 100000000000 !important;" class="modal fade" id="tambah_lembaga" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header2">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Tambah Data Lembaga</h4>
				</div>
				<div class="modal-body">
					<form autocomplete="off" class="form-horizontal" method="POST" id="form_add_lembaga">
						  <div style="height: 430px;overflow-y: scroll;overflow-x: hidden;">
								  <div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Lembaga <span class='must_fill'>*</span></label>
									<div class="col-sm-7">
									  <input  maxlength="200"  id="lembaga_name" name="lembaga_name" type="text" class="form-control" required>
									  <input   id="id_user" name="id_user" type="hidden" value="<?php echo $_SESSION['id_petugas'] ?>" required>
									</div>
								  </div>
								   <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">No HP <span class='must_fill'>*</span></label>
									<div class="col-sm-4">
									  <input maxlength="25" name="no_hp" type="text" class="form-control" id="no_hp_lembaga" required>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">No Telepon </label>
									<div class="col-sm-4">
									  <input maxlength="25" name="no_telepon" type="text" class="form-control" id="no_telepon_lembaga" >
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Provinsi <span class='must_fill'>*</span></label>
									<div class="col-sm-7">
									  <select onchange="get_kabkot('id_propinsi_lembaga_add','id_kabkot_lembaga_add')"  class="form-control" name="provinsi" id="id_propinsi_lembaga_add" required>
										<option value="">Pilih Provinsi</option>
											<?php $data_prov = mysqli_query($con,"select * from ms_provinsi");
												  while($prov = mysqli_fetch_object($data_prov)){ ?>
												 <option value="<?php echo $prov->id_prov ?>"><?php echo $prov->nama_prov ?></option>
											<?php } ?>
									  </select>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Kota/Kabupaten <span class='must_fill'>*</span></label>
									<div class="col-sm-7">
									  <select class="form-control" name="kabkot" id="id_kabkot_lembaga_add" required>
										<option value="">Pilih Kota / Kabupaten</option>
									  </select>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Alamat</label>
									<div class="col-sm-7">
									  <textarea name="alamat"  class="form-control" id="alamat" placeholder=""></textarea>
									</div>
								  </div>
						  </div>
						</div>
						<div class="modal-footer">
							<button type="reset" class="btn btn-default btn-box" data-dismiss="modal">Batal</button>
							<button type="submit" class="btn btn-default btn-box">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>	
		
		
		<!-- Modal Edit Lembaga-->
	<div style="z-index: 10000000000 !important;" class="modal fade" id="edit_lembaga" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header2">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit Data Lembaga</h4>
				</div>
				<div class="modal-body">
					<form autocomplete="off" class="form-horizontal" method="POST" id="form_edit_lembaga">
						  <div style="height: 430px;overflow-y: scroll;overflow-x: hidden;">
								  <div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Lembaga <span class='must_fill'>*</span></label>
									<div class="col-sm-7">
									  <input  maxlength="200"  id="lembaga_name_edit" name="lembaga_name" type="text" class="form-control" required>
									   <input   id="id_user" name="id_user" type="hidden" value="<?php echo $_SESSION['id_petugas'] ?>" required>
									  <input   id="id_lembaga_edit" name="id_lembaga" type="hidden"  required>
									</div>
								  </div>
								   <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">No HP <span class='must_fill'>*</span></label>
									<div class="col-sm-4">
									  <input maxlength="25" name="no_hp" type="text" class="form-control" id="no_hp_lembaga_edit" required>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">No Telepon </label>
									<div class="col-sm-4">
									  <input maxlength="25" name="no_telepon" type="text" class="form-control" id="no_telepon_lembaga_edit" >
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Provinsi <span class='must_fill'>*</span></label>
									<div class="col-sm-7">
									  <select onchange="get_kabkot('id_propinsi_lembaga_edit','id_kabkot_lembaga_edit')"  class="form-control" name="provinsi" id="id_propinsi_lembaga_edit" required>
										<option value="">Pilih Provinsi</option>
											<?php $data_prov = mysqli_query($con,"select * from ms_provinsi");
												  while($prov = mysqli_fetch_object($data_prov)){ ?>
												 <option value="<?php echo $prov->id_prov ?>"><?php echo $prov->nama_prov ?></option>
											<?php } ?>
									  </select>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Kota/Kabupaten <span class='must_fill'>*</span></label>
									<div class="col-sm-7">
									  <select class="form-control" name="kabkot" id="id_kabkot_lembaga_edit" required>
										<option value="">Pilih Kota / Kabupaten</option>
									  </select>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Alamat</label>
									<div class="col-sm-7">
									  <textarea name="alamat"  class="form-control" id="alamat_lembaga_edit" placeholder=""></textarea>
									</div>
								  </div>
						  </div>
						</div>
						<div class="modal-footer">
							<button type="reset" class="btn btn-default btn-box" data-dismiss="modal">Batal</button>
							<button type="submit" class="btn btn-default btn-box">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>

	

	<!-- Modal Tambah Pemohon-->
	<div style="z-index: 10000000000 !important;" class="modal fade" id="tambah_pemohon" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header2">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Tambah Data Pemohon</h4>
				</div>
				<div class="modal-body">
					<form autocomplete="off" class="form-horizontal" method="POST" id="form_add_pemohon">
						  <div style="height: 430px;overflow-y: scroll;overflow-x: hidden;">
								  <div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Nama <span class='must_fill'>*</span></label>
									<div class="col-sm-7">
									  <input  maxlength="50"  id="nama" name="nama" type="text" class="form-control" required>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">No Identitas <span class='must_fill'>*</span></label>
									<div class="col-sm-7">
									  <input maxlength="20" name="nik"  type="text" class="form-control" id="nik" required>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Jenis Kelamin <span class='must_fill'>*</span></label>
									<div class="col-sm-6">
									  <input type="radio"   name="kelamin" value="Pria" required> Laki - Laki &nbsp;&nbsp;
									  <input type="radio"   name="kelamin" value="Wanita" required> Perempuan
									</div>
								  </div>
								   <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">No HP <span class='must_fill'>*</span></label>
									<div class="col-sm-4">
									  <input maxlength="25" name="no_hp" type="text" class="form-control" id="no_hp" required>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">No Telepon </label>
									<div class="col-sm-4">
									  <input maxlength="25" name="no_telepon" type="text" class="form-control" id="no_telepon" >
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Alamat</label>
									<div class="col-sm-7">
									  <textarea name="alamat"  class="form-control" id="alamat" placeholder=""></textarea>
									</div>
								  </div>
						  </div>
						</div>
						<div class="modal-footer">
							<button type="reset" class="btn btn-default btn-box" data-dismiss="modal">Batal</button>
							<button type="submit" class="btn btn-default btn-box">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Edit Pemohon-->
	<div style="z-index: 10000000000 !important;" class="modal fade" id="edit_pemohon" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header2">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit Data Pemohon</h4>
				</div>
				<div class="modal-body">
					<form autocomplete="off" class="form-horizontal" method="POST" id="form_edit_pemohon">
						  <div style="height: 430px;overflow-y: scroll;overflow-x: hidden;">
								  <div class="form-group">
									<label for="inputEmail3" class="col-sm-3 control-label">Nama <span class='must_fill'>*</span></label>
									<div class="col-sm-7">
									  <input  maxlength="50"  id="nama_edit" name="nama" type="text" class="form-control" required>
									  <input  id="id_pemohon_edit" name="id_pemohon" type="hidden" required>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">No Identitas <span class='must_fill'>*</span></label>
									<div class="col-sm-7">
									  <input maxlength="20" name="nik"  type="text" class="form-control" id="nik_edit" required>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Jenis Kelamin <span class='must_fill'>*</span></label>
									<div class="col-sm-6">
									  <input type="radio"  id="kelamin_edit" name="kelamin" value="Pria" required> Laki - Laki &nbsp;&nbsp;
									  <input type="radio"  id="kelamin_edit"  name="kelamin" value="Wanita" required> Perempuan
									</div>
								  </div>
								   <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">No HP <span class='must_fill'>*</span></label>
									<div class="col-sm-4">
									  <input maxlength="25" name="no_hp" type="text" class="form-control" id="no_hp_edit" required>
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">No Telepon </label>
									<div class="col-sm-4">
									  <input maxlength="25" name="no_telepon" type="text" class="form-control" id="no_telepon_edit" >
									</div>
								  </div>
								  <div class="form-group">
									<label for="inputPassword3" class="col-sm-3 control-label">Alamat</label>
									<div class="col-sm-7">
									  <textarea name="alamat"  class="form-control" id="alamat_edit" placeholder=""></textarea>
									</div>
								  </div>
						  </div>
						</div>
						<div class="modal-footer">
							<button type="reset" class="btn btn-default btn-box" data-dismiss="modal">Batal</button>
							<button type="submit" class="btn btn-default btn-box">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal History -->
	<div class="modal fade" id="history" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header2">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">List Data History</h4>
				</div>
				<div class="modal-body">
					<div class="page-wrapper">
		                <div class="content container-fluid">
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table style="width:100%" id="table_data_history" class="display table table-striped custom-table datatable">
											<thead>
												<tr>
													<th class="text-center">Group Layanan</th>
													<th class="text-center">Sub Layanan</th>
													<th class="text-center">Permohonan</th>
													<th class="text-center">No Urut</th>
													<th class="text-center">Nama</th>
													<th class="text-center">Aksi</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
		                </div>
		            </div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal Pemohon-->
	<div class="modal fade" id="picker_pemohon" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header2">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">List Data Pemohon</h4>
				</div>
				<div class="modal-body">
					<div class="page-wrapper">
		                <div class="content container-fluid">
							<div class="row">
								<div class="col-sm-12 col-xs-12 text-right m-b-20">
									<a href="#" class="btn btn-success rounded pull-right" data-toggle="modal" data-target="#tambah_pemohon"><i class="fa fa-plus"></i> Tambah Data</a>
								</div>
							</div><BR/>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table style="width:100%" id="table_data_pemohon" class="display table table-striped custom-table datatable">
											<thead>
												<tr>
													<th class="text-center">Nama</th>
													<th class="text-center">NIK</th>
													<th class="text-center">Jenis Kelamin</th>
													<th class="text-center">No. Tlp</th>
													<th class="text-center">No. Hp </th>
													<th class="text-center">Alamat</th>
													<th class="text-center">Aksi</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
		                </div>
		            </div>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modal Pemohon-->
	<div class="modal fade" id="picker_pemohon2"  style="z-index: 10000000 !important;" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header2">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">List Data Pemohon</h4>
				</div>
				<div class="modal-body">
					<div class="page-wrapper">
		                <div class="content container-fluid">
							<div class="row">
								<div class="col-sm-12 col-xs-12 text-right m-b-20">
									<a href="#" class="btn btn-success rounded pull-right" data-toggle="modal" data-target="#tambah_pemohon"><i class="fa fa-plus"></i> Tambah Data</a>
								</div>
							</div><BR/>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table style="width:100%" id="table_data_pemohon2" class="display table table-striped custom-table datatable">
											<thead>
												<tr>
													<th class="text-center">Nama</th>
													<th class="text-center">NIK</th>
													<th class="text-center">Jenis Kelamin</th>
													<th class="text-center">No. Tlp</th>
													<th class="text-center">No. Hp </th>
													<th class="text-center">Alamat</th>
													<th class="text-center">Aksi</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
		                </div>
		            </div>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Instansi-->
	<div class="modal fade" id="picker_company" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header2">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">List Data Instansi / Company</h4>
				</div>
				<div class="modal-body">
					<div class="page-wrapper">
		                <div class="content container-fluid">
							<div class="row">
								<div class="col-sm-12 col-xs-12 text-right m-b-20">
									<a href="#" class="btn btn-success rounded pull-right" data-toggle="modal" data-target="#tambah_company"><i class="fa fa-plus"></i> Tambah Instansi / Company</a>
								</div>
							</div><BR/>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table style="width:100%" id="table_data_company" class="display table table-striped custom-table datatable">
											<thead>
												<tr>
													<th class="text-center">Instansi / Company</th>
													<th class="text-center">No.Tlp</th>
													<th class="text-center">Alamat</th>
													<th class="text-center">Aksi</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
		                </div>
		            </div>
				</div>
			</div>

		</div>
	</div>
	
	<!-- Modal Instansi-->
	<div class="modal fade" id="picker_company2"   style="z-index: 10000000 !important;" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header2">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">List Data Instansi / Company</h4>
				</div>
				<div class="modal-body">
					<div class="page-wrapper">
		                <div class="content container-fluid">
							<div class="row">
								<div class="col-sm-12 col-xs-12 text-right m-b-20">
									<a href="#" class="btn btn-success rounded pull-right" data-toggle="modal" data-target="#tambah_company"><i class="fa fa-plus"></i> Tambah Instansi / Company</a>
								</div>
							</div><BR/>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table style="width:100%" id="table_data_company2" class="display table table-striped custom-table datatable">
											<thead>
												<tr>
													<th class="text-center">Instansi / Company</th>
													<th class="text-center">No.Tlp</th>
													<th class="text-center">Alamat</th>
													<th class="text-center">Aksi</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
		                </div>
		            </div>
				</div>
			</div>

		</div>
	</div>
	
	<!-- Modal Lembaga-->
	<div class="modal fade" id="picker_lembaga" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header2">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">List Data Lembaga</h4>
				</div>
				<div class="modal-body">
					<div class="page-wrapper">
		                <div class="content container-fluid">
							<div class="row">
								<div class="col-sm-12 col-xs-12 text-right m-b-20">
									<a href="#" class="btn btn-success rounded pull-right" data-toggle="modal" data-target="#tambah_lembaga"><i class="fa fa-plus"></i> Tambah Lembaga</a>
								</div>
							</div><BR/>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table style="width:100%" id="table_data_lembaga" class="display table table-striped custom-table datatable">
											<thead>
												<tr>
													<th class="text-center">Lembaga</th>
													<th class="text-center">No.Tlp / Hp</th>
													<th class="text-center">Alamat</th>
													<th class="text-center">Aksi</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
		                </div>
		            </div>
				</div>
			</div>

		</div>
	</div>
	
	<!-- Modal Lembaga 2-->
	<div class="modal fade" id="picker_lembaga2" style="z-index: 10000000 !important;" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header2">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">List Data Lembaga</h4>
				</div>
				<div class="modal-body">
					<div class="page-wrapper">
		                <div class="content container-fluid">
							<div class="row">
								<div class="col-sm-12 col-xs-12 text-right m-b-20">
									<a href="#" class="btn btn-success rounded pull-right" data-toggle="modal" data-target="#tambah_lembaga"><i class="fa fa-plus"></i> Tambah Lembaga</a>
								</div>
							</div><BR/>
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table style="width:100%" id="table_data_lembaga2" class="display table table-striped custom-table datatable">
											<thead>
												<tr>
													<th class="text-center">Lembaga</th>
													<th class="text-center">No.Tlp / Hp</th>
													<th class="text-center">Alamat</th>
													<th class="text-center">Aksi</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
		                </div>
		            </div>
				</div>
			</div>

		</div>
	</div>

	<div class="modal fade" id="modal_histori" tabindex="-1" role="dialog" aria-labelledby="modalOssLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document" style="width:750px;">
	    <div class="modal-content">
	      <div class="modal-header2">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Histori Antrian Loket <?php echo $_SESSION['loket'] ?></h4>
		   </div>
	      <div class="modal-body" >
	      	<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
					<div class="form-group ">
						<label>Tanggal Awal <span style="color:red">*</span></label>
						<input type="text" name="tgl_awal" id="tgl_awal" class="form-control datepicker_bottom" >
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
					<div class="form-group ">
						<label>Tanggal Akhir</label>
						<input type="text" name="tgl_akhir" id="tgl_akhir" class="form-control datepicker_bottom" >
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">	
					<br/>
					<button type="button" onclick="cari()"  class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
						&nbsp;
					<button type="reset"  onclick="reset_data()" class="btn btn-danger"><i class="fa fa-eraser"></i> Reset</button>
				</div>
	      	</div>
	      	<div id="data_histori" class="table-responsive" style="overflow-x:hidden;overflow-y:auto;height:300px;"></div>
	     </div>
	  </div>
	</div>		
	</div>

	<!-- Modal -->
	<div class="modal fade" id="modal_layanan" tabindex="-1" role="dialog" aria-labelledby="modalOssLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	       <div class="modal-header2">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Antrian Loket <?php echo $layanan ?></h4>
		   </div>
	      <div class="modal-body" >
	      	<div id="data_layanan" class="row"></div>
	    </div>
	  </div>
	</div>		
	</div>

	
	
	<!-- Script -->
<script type="text/javascript" src="js/jquery.js"></script>
<script src="js/bootstrap3-typeahead/bootstrap3-typeahead.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script src="js/sweetalert2/dist/sweetalert2.min.js"></script>
<script src="js/moment.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/dataTables.bootstrap.min.js"></script>
<script src="js/datepicker/dist/js/bootstrap-datepicker.min.js"></script>
</body>



<script>
var active_mulai="NOK";
var status_durasi="NOK";
var id_now;
var refreshIntervalId;
function cari(){
	var loket      = "<?php echo $_SESSION['loket']?>";
	var tgl_awal   = $("#tgl_awal").val();	
	var tgl_akhir  = $("#tgl_akhir").val();	
	var param="";
	var param2="";
	if(tgl_awal==""){
		swal("Terjadi Kesalahan","Tanggal Awal tidak boleh kosong","error");
	}
	else {
		
		param = " and LOKET ='"+loket+"' ";
		
		
	var err = "";	
	if(tgl_awal && tgl_akhir){
	    date1 = new Date(tgl_awal); 
	    date2 = new Date(tgl_akhir);

	    var date1   = ((new Date(tgl_awal.split('/').reverse().join('/')).getTime())/1000);
	    var date2   = ((new Date(tgl_akhir.split('/').reverse().join('/')).getTime())/1000);
	    if(date1 > date2){
	      swal("Terjadi Kesalahan","Tanggal Awal lebih besar daripada Tanggal Akhir","error");
	      err = "error";	
	    }
	    else {
	       param2 += " and DATE_FORMAT(TANGGAL , '%Y-%m-%d') between '"+date_to_default(tgl_awal.trim())+"' and  '"+date_to_default(tgl_akhir.trim())+"' ";
	    }
	  }
	  else if(tgl_awal){
	       param2 += " and DATE_FORMAT(TANGGAL , '%Y-%m-%d')='"+date_to_default(tgl_awal.trim())+"' ";
	  }
	  else if(tgl_akhir){
	      param2 += " and DATE_FORMAT(TANGGAL , '%Y-%m-%d')='"+date_to_default(tgl_akhir.trim())+"' ";
	  }

	  if(err!="error") {
	      	//$("#filter_sum_val").val(param);
			//$("#filter_sum_val_date").val(param2);
			$.ajax({
	              url: 'ajax/serverSide/histori_antrian.php',
	              type: 'POST',
	              data: 'param='+btoa(param)+'&param2='+btoa(param2),
	              success: function(mydata) {
	                  $('#data_histori').html(mydata);
	              }
	          });
	 }
	 
	}
}	
function show_antrian_histori(){
	$("#modal_histori").modal('show');
}
$(".tgl").datepicker({
	format: 'dd/mm/yyyy'
});
$('.datepicker_bottom').datepicker({
	orientation: "bottom",
	autoclose: true,
	todayHighlight: true,
	format: "yyyy-mm-dd"
});

$('#nama_pemohon').typeahead({
	hint: true,
	highlight: true,
	source: function (query, process) {
	    $.ajax({
	        url: 'ajax/pemohon.php',
	        type: 'POST',
	        dataType: 'JSON',
	        data: 'act=reqPemohon&query='+$('#nama_pemohon').val(),
	        success: function(data) {
	            var results = data.map(function(item) {
	                var someItem = {name:item.label, myvalue:item.value ,nama_pmh:item.pmh ,ints:item.ints ,alamat:item.alamat ,no_hp:item.no_hp ,nik:item.nik,jk:item.jk};
	                return someItem;
	            });
	            return process(results);
	        }
	    });
	},
	afterSelect: function(item) {
    	this.$element[0].value = item.nama_pmh
	},
	updater: function(item) {
	    $('#id_pemohon').val(item.myvalue);
	    $('#nik').val(item.nik);
	    $('#instansi').val(item.ints);
	    $('#alamat').val(item.alamat);
	    $('#no_hp').val(item.no_hp);
	    // $('#group').val(item.group);
	    // $('#ket').val(item.ket);

	    //Jenis Kelamin
	    $('input:radio[name="kelamin"][value='+item.jk+']').attr('checked', true);
		
		console.log(item);
	    return item;
	}
});
	
get_total_antrian();
get_next_antrian_show();
get_sisa_antrian();
data_layanan();

<?php 
$ly = mysqli_fetch_object(mysqli_query($con,"select ID_LAYANAN from tr_layanan_loket where LOKET='".$_SESSION['loket']."' "));
$count_current = mysqli_fetch_object(mysqli_query($con,"select count(*) as total from data_antrian where LOKET='".$_SESSION['loket']."'  and ID_LAYANAN='".$ly->ID_LAYANAN."'  and STATUS_CURRENT='Y' and tanggal like '".date('Y-m-d')."%' "));
if($count_current->total>0){ 
	echo "get_current_antrian();";
} 
 ?>


	function refresh() {
		get_total_antrian();
		get_next_antrian_show();
		get_sisa_antrian();
		check_durasi();
		data_layanan();
	}
	function check_durasi(){
		<?php  if($id_layanan!=""){ ?>
			  $.ajax({
		        url: 'ajax/antrian.php?act=check_durasi&id_layanan=<?php echo $id_layanan ?>',
		        type: 'POST',
		        dataType: 'html',
		        success: function(mydata) {
		        	var status_durasi = localStorage.getItem("status");
		        	if(status_durasi==mydata){

		        	}
		        	else {
		        		status_durasi=localStorage.setItem("status",mydata);
		        		location.reload();
		        	}
		        }
		    });
		<?php  } ?>
	}
	function konfirmasi_next_panggilan() {
			get_next_antrian_show();
			swal({
			  title: "Panggilan Berikutnya",
			  text: "Apakah anda ingin melakukan panggilan berikutnya ?",
			  type:  'info', 
			  showCancelButton: true,
	          showLoaderOnConfirm: true,
	          preConfirm: function(){
	          		get_next_antrian();
	          }
			});   
	}
	function show_antrian_layanan(){
			$("#modal_layanan").modal('show');
	}
	function data_layanan(){
		$.ajax({
			url: 'ajax/antrian.php',
			type: 'POST',
			dataType: 'html',
			data: 'act=layanan_current&loket=<?php echo base64_encode($_SESSION['loket'])?>',
			success: function(mydataku) {
				if(mydataku!=""){
					$("#data_layanan").html(mydataku);
				}
				else {
					$("#data_layanan").html("-");
				}

			}
		});	
	}
    function startTimer(duration, display) {
	    var timer = duration, minutes, seconds;
	    refreshIntervalId  = setInterval(function () {
	        minutes = parseInt(timer / 60, 10)
	        seconds = parseInt(timer % 60, 10);

	        minutes = minutes < 10 ? "0" + minutes : minutes;
	        seconds = seconds < 10 ? "0" + seconds : seconds;

	        var waktu = minutes + ":" + seconds
	        display.textContent = waktu;
	        save_times(waktu);

	        if (--timer < 0) {
	        	save_times(waktu);
	            timer = duration;
	            get_next_antrian();

	     	}
	    }, 1000);
	}
	function startTimer_display(duration, display) {
	    var timer = duration, minutes, seconds;
	        minutes = parseInt(timer / 60, 10)
	        seconds = parseInt(timer % 60, 10);

	        minutes = minutes < 10 ? "0" + minutes : minutes;
	        seconds = seconds < 10 ? "0" + seconds : seconds;

	        display.textContent = minutes + ":" + seconds;

	   		var durasi_total = minutes + ":" + seconds;
	   		$("#durasi_total").val(durasi_total);

	   		setTimeout(function(){
		   		var id_data_antrian = $("#id_data_antrian").val();
			    if(id_data_antrian!=""){
			    	 $.ajax({
						url: 'ajax/antrian.php?act=save_durasi&id_data_antrian='+id_data_antrian+'&durasi_total='+durasi_total, 
						type: 'POST',
						processData: false,
						contentType: false,
						success: function(mydata) {
							
						}
					});  
			    }
	   		 }, 2000);
	}
    function refresh_timer(){
		<?php  if(!empty($durasi_total)){ ?>
			clearInterval(refreshIntervalId);
			active_mulai  ="NOK";
			$("#btn_mulai").attr('class', 'mulai');

			 var Minutes2 = 60 * <?php echo $durasi_total ?>,
			 display2 = document.querySelector('#timer_loket');
			 startTimer_display(Minutes2, display2);	
		<?php } ?>
	}
	function mulai(){
		 var id= $("#id_panggilan_speaker").val();
		 if(id!=""){
		 	if(active_mulai=="NOK"){
		 		if(id_now==id){
		 			show_alert('warning','Opps','Waktu habis');
		 		}
		 		else {
		 			var Minutes = 60 * <?php echo $durasi_total ?>,
			        display = document.querySelector('#timer_loket');
			        active_mulai ="OK";
			        id_now=id;
					startTimer(Minutes, display);
					$("#btn_mulai").attr('class', 'mulai_disabled');
		 		}
		 	}
		 	else {
		 		show_alert('warning','Opps','Waktu sudah berjalan, untuk memberhentikan waktu layanan, silahkan melanjutkan antrian berikutnya');
		 	}
		 }
		 else {
		 	show_alert('warning','Opps','Nomor Antrian Kosong');
		 }	
	}
	function save_times(waktu){
			var id_data_antrian = $("#id_data_antrian").val();
		    $.ajax({
				url: 'ajax/antrian.php?act=save_times&id_data_antrian='+id_data_antrian+'&waktu='+waktu, 
				type: 'POST',
				processData: false,
				contentType: false,
				success: function(mydata) {
					
				}
			});
	}

	<?php  if(!empty($durasi_total)){ ?>
	   var Minutes = 60 * <?php echo $durasi_total ?>,
	        display = document.querySelector('#timer_loket');
	    startTimer_display(Minutes, display);

		 setTimeout(function(){ 
		 	var id_data_antrian = $("#id_data_antrian").val();
		    $.ajax({
				url: 'ajax/antrian.php?act=check_waktu&id_data_antrian='+id_data_antrian, 
				type: 'POST',
				processData: false,
				contentType: false,
				success: function(mydata) {
					if(mydata!="no_data"){
						 var arr = mydata.split(':');
						 var menit = arr[0];
						 var detik =  parseInt(arr[1]);

						 active_mulai ="OK";
						 $("#btn_mulai").attr('class', 'mulai_disabled');

						 if(menit=="00"){
		   					startTimer(detik, display);
						 } else {
						 	var Minutes2 = 60 * parseInt(menit) + detik;
						 	startTimer(Minutes2, display);
						 }
					}
				}
			});

		}, 1000);

	<?php } ?>

get_kabkot('id_propinsi_add','id_kabkot_add');
get_kabkot('id_propinsi_lembaga_add','id_kabkot_lembaga_add')
get_kabkot('id_propinsi_edit','id_kabkot_edit');


$("#f1").on("submit", function (event) {
event.preventDefault();
	var panggilan = $("#id_panggilan").val();
	$("#no_urut").val(panggilan);
	do_act('f1','crud/do_save_antrian.php','Tambah Visitor','Apakah data yang diisi sudah benar ?','info');

});


$("#f2").on("submit", function (event) {
event.preventDefault();
	swal({
		  title: 'Update Visitor',
		  text: 'Apakah data yang di isi sudah benar ?',
		  type: 'warning',      // warning,info,success,error
		  showCancelButton: true,
		  showLoaderOnConfirm: true,
		  preConfirm: function(){
			$.ajax({
				url: 'crud/do_update_antrian.php', 
				type: 'POST',
				data: new FormData($('#f2')[0]),  // Form ID
				processData: false,
				contentType: false,
				success: function(data) {
					var data_trim = $.trim(data);
					if(data_trim=="OK")
					{
						swal({
							title: 'Success',
							type: 'success',
							showCancelButton: false,
							showLoaderOnConfirm: false,
						  }).then(function() {
								refresh_table();
								$('#f2')[0].reset();
								$('#edit_history').modal('toggle');
							});
					} 
					else
					{
						swal({
							title: 'Error',
							html: data_trim,
							type: 'error',
							showCancelButton: false,
							showLoaderOnConfirm: false,
						  });
					}
				}
			});
		 }
		});   
});


function get_kabkot(from_id,result_id,act=null){
	if(act=="edit"){
		// create variable to get data from element id
			  var mydata = $("#"+from_id).val();
			  // send data with ajax
			  $.ajax({
				 type: "POST",
				 dataType: "html",
				 url: "ajax/get_data_kabkot.php",
				 data: "prov="+mydata,
				 success: function(msg){
					// when no result
					if(msg==''){
					   //alert('Tidak ada data Kabupaten');
					}
					// when have a resul then result data show in element result_id
					else{
					   $("#"+result_id).html(msg);                                                      
					}

				 }
			  }); 
	}
	else {
		 $("#"+from_id).change(function()
			{
			  // create variable to get data from element id
			  var mydata = $("#"+from_id).val();
			  // send data with ajax
			  $.ajax({
				 type: "POST",
				 dataType: "html",
				 url: "ajax/get_data_kabkot.php",
				 data: "prov="+mydata,
				 success: function(msg){
					// when no result
					if(msg==''){
					   //alert('Tidak ada data Kabupaten');
					}
					// when have a resul then result data show in element result_id
					else{
					   $("#"+result_id).html(msg);                                                      
					}

				 }
			  });     
		   });	
	}
}

$("#form_add_company").on("submit", function (event) {
event.preventDefault();
	swal({
		  title: 'Tambah Instansi / Company',
		  text: 'Apakah data yang di isi sudah benar ?',
		  type: 'info',      // warning,info,success,error
		  showCancelButton: true,
		  showLoaderOnConfirm: true,
		  preConfirm: function(){
			$.ajax({
				url: 'crud/do_save_company.php?act=add', 
				type: 'POST',
				data: new FormData($('#form_add_company')[0]),  // Form ID
				processData: false,
				contentType: false,
				success: function(data) {
					var data_trim = $.trim(data);
					if(data_trim=="OK")
					{
						swal({
							title: 'Success',
							type: 'success',
							showCancelButton: false,
							showLoaderOnConfirm: false,
						  }).then(function() {
								refresh_table();
								$('#form_add_company')[0].reset();
								$('#tambah_company').modal('toggle');
							});
					} 
					else
					{
						swal({
							title: 'Error',
							html: data_trim,
							type: 'error',
							showCancelButton: false,
							showLoaderOnConfirm: false,
						  });
					}
				}
			});
		 }
		});   
});

$("#form_add_lembaga").on("submit", function (event) {
event.preventDefault();
	swal({
		  title: 'Tambah Lembaga',
		  text: 'Apakah data yang di isi sudah benar ?',
		  type: 'info',      // warning,info,success,error
		  showCancelButton: true,
		  showLoaderOnConfirm: true,
		  preConfirm: function(){
			$.ajax({
				url: 'crud/do_save_lembaga.php?act=add', 
				type: 'POST',
				data: new FormData($('#form_add_lembaga')[0]),  // Form ID
				processData: false,
				contentType: false,
				success: function(data) {
					var data_trim = $.trim(data);
					if(data_trim=="OK")
					{
						swal({
							title: 'Success',
							type: 'success',
							showCancelButton: false,
							showLoaderOnConfirm: false,
						  }).then(function() {
								refresh_table();
								$('#form_add_lembaga')[0].reset();
								$('#tambah_lembaga').modal('toggle');
							});
					} 
					else
					{
						swal({
							title: 'Error',
							html: data_trim,
							type: 'error',
							showCancelButton: false,
							showLoaderOnConfirm: false,
						  });
					}
				}
			});
		 }
		});   
});

$("#form_edit_company").on("submit", function (event) {
event.preventDefault();
	swal({
		  title: 'Edit Instansi / Company',
		  text: 'Apakah data yang di isi sudah benar ?',
		  type: 'warning',      // warning,info,success,error
		  showCancelButton: true,
		  showLoaderOnConfirm: true,
		  preConfirm: function(){
			$.ajax({
				url: 'crud/do_save_company.php?act=edit', 
				type: 'POST',
				data: new FormData($('#form_edit_company')[0]),  // Form ID
				processData: false,
				contentType: false,
				success: function(data) {
					var data_trim = $.trim(data);
					if(data_trim=="OK")
					{
						swal({
							title: 'Success',
							type: 'success',
							showCancelButton: false,
							showLoaderOnConfirm: false,
						  }).then(function() {
								refresh_table();
								$('#form_edit_company')[0].reset();
								$('#edit_company').modal('toggle');
							});
					} 
					else
					{
						swal({
							title: 'Error',
							html: data_trim,
							type: 'error',
							showCancelButton: false,
							showLoaderOnConfirm: false,
						  });
					}
				}
			});
		 }
		});   
});

$("#form_edit_lembaga").on("submit", function (event) {
event.preventDefault();
	swal({
		  title: 'Edit Lembaga',
		  text: 'Apakah data yang di isi sudah benar ?',
		  type: 'warning',      // warning,info,success,error
		  showCancelButton: true,
		  showLoaderOnConfirm: true,
		  preConfirm: function(){
			$.ajax({
				url: 'crud/do_save_lembaga.php?act=edit', 
				type: 'POST',
				data: new FormData($('#form_edit_lembaga')[0]),  // Form ID
				processData: false,
				contentType: false,
				success: function(data) {
					var data_trim = $.trim(data);
					if(data_trim=="OK")
					{
						swal({
							title: 'Success',
							type: 'success',
							showCancelButton: false,
							showLoaderOnConfirm: false,
						  }).then(function() {
								refresh_table();
								$('#form_edit_lembaga')[0].reset();
								$('#edit_lembaga').modal('toggle');
							});
					} 
					else
					{
						swal({
							title: 'Error',
							html: data_trim,
							type: 'error',
							showCancelButton: false,
							showLoaderOnConfirm: false,
						  });
					}
				}
			});
		 }
		});   
});

$("#form_add_pemohon").on("submit", function (event) {
event.preventDefault();
	swal({
		  title: 'Tambah Pemohon',
		  text: 'Apakah data yang di isi sudah benar ?',
		  type: 'info',      // warning,info,success,error
		  showCancelButton: true,
		  showLoaderOnConfirm: true,
		  preConfirm: function(){
			$.ajax({
				url: 'crud/do_save_pemohon.php?act=add', 
				type: 'POST',
				data: new FormData($('#form_add_pemohon')[0]),  // Form ID
				processData: false,
				contentType: false,
				success: function(data) {
					var data_trim = $.trim(data);
					if(data_trim=="OK")
					{
						swal({
							title: 'Success',
							type: 'success',
							showCancelButton: false,
							showLoaderOnConfirm: false,
						  }).then(function() {
								refresh_table();
								$('#form_add_pemohon')[0].reset();
								$('#tambah_pemohon').modal('toggle');
							});
					} 
					else
					{
						swal({
							title: 'Error',
							html: data_trim,
							type: 'error',
							showCancelButton: false,
							showLoaderOnConfirm: false,
						  });
					}
				}
			});
		 }
		});   
});

$("#form_edit_pemohon").on("submit", function (event) {
event.preventDefault();
	swal({
		  title: 'Edit Pemohon',
		  text: 'Apakah data yang di isi sudah benar ?',
		  type: 'warning',      // warning,info,success,error
		  showCancelButton: true,
		  showLoaderOnConfirm: true,
		  preConfirm: function(){
			$.ajax({
				url: 'crud/do_save_pemohon.php?act=edit', 
				type: 'POST',
				data: new FormData($('#form_edit_pemohon')[0]),  // Form ID
				processData: false,
				contentType: false,
				success: function(data) {
					var data_trim = $.trim(data);
					if(data_trim=="OK")
					{
						swal({
							title: 'Success',
							type: 'success',
							showCancelButton: false,
							showLoaderOnConfirm: false,
						  }).then(function() {
								refresh_table();
								$('#form_edit_pemohon')[0].reset();
								$('#edit_pemohon').modal('toggle');
							});
					} 
					else
					{
						swal({
							title: 'Error',
							html: data_trim,
							type: 'error',
							showCancelButton: false,
							showLoaderOnConfirm: false,
						  });
					}
				}
			});
		 }
		});   
});


	
//Jam
 var interval = setInterval(function() {
        var momentNow = moment();
        $('#jam').html(momentNow.format('HH'));
        $('#menit').html(momentNow.format('mm'));
    }, 100);

$("#form_keluar").on("submit", function (event) {
		event.preventDefault();
			do_logout();
});
function show_alert(err,judul,text_data){
	swal({
		title: judul,	
		type: err,	//error,warning,success
		text : text_data,
		showCancelButton: false,
		showLoaderOnConfirm: false,
	  });
}
function send_running_text(){
	var rn = $("#running_text").val();
	if(rn!=""){
		$.ajax({
	        url: 'ajax/antrian.php',
	        type: 'POST',
	        dataType: 'html',
	        data: 'act=antrian_running&loket=<?php echo base64_encode($_SESSION['loket'])?>&rn='+window.btoa(rn),
	        success: function(mydata) {
	        	show_alert('success','Berhasil','Running berhasil disimpan');
	        }
	    });
	}
	else {
		show_alert('error','Terjadi Kesalahan','Running harus diisi');
	}
}
function get_sertifikasi(div,div2,div3){
	var rn = $("#"+div).val();
	if(rn!=""){
		$.ajax({
	        url: 'ajax/pemohon.php',
	        type: 'POST',
	        dataType: 'html',
	        data: 'act=sertifikasi&rn='+rn,
	        success: function(mydata) {
	        	if(mydata=="Ya"){
	        		$("#"+div2).show();
	        		$("#"+div3).val(mydata);
	        	}
	        	else {
	        		$("#"+div2).hide();
	        		$("#"+div3).val(mydata);
	        	}
	        }
	    });
	}
	else {
		$("#"+div2).hide();
	}
}
show_grup();
function show_grup(){
	var rn = $("#jenis_layanan").val();
	if(rn!=""){
		$.ajax({
	        url: 'ajax/group_layanan.php',
	        type: 'POST',
	        dataType: 'html',
	        data: 'rn='+window.btoa(rn),
	        success: function(mydata) {
	        	$("#group_layanan").html(mydata);
	        }
	    });
	}
}
function show_sub(){
	var rn = $("#group_layanan").val();
	if(rn!=""){
		$.ajax({
	        url: 'ajax/sub_group_layanan.php',
	        type: 'POST',
	        dataType: 'html',
	        data: 'rn='+window.btoa(rn),
	        success: function(mydata) {
	        	$("#sub_group_layanan").html(mydata);
	        }
	    });
	}
}
show_grup2();
function show_grup2(){
	var rn = $("#jenis_layanan_his").val();
	if(rn!=""){
		$.ajax({
	        url: 'ajax/group_layanan.php',
	        type: 'POST',
	        dataType: 'html',
	        data: 'rn='+window.btoa(rn),
	        success: function(mydata) {
	        	$("#group_layanan_his").html(mydata);
	        }
	    });
	}
}
function show_sub2(){
	var rn = $("#group_layanan_his").val();
	if(rn!=""){
		$.ajax({
	        url: 'ajax/sub_group_layanan.php',
	        type: 'POST',
	        dataType: 'html',
	        data: 'rn='+window.btoa(rn),
	        success: function(mydata) {
	        	$("#sub_group_layanan_his").html(mydata);
	        }
	    });
	}
}
function get_repeat_antrian(){
	var id_panggilan = $("#id_panggilan_speaker").val();
	if(id_panggilan!=""){
		$.ajax({
	        url: 'ajax/antrian.php',
	        type: 'POST',
	        dataType: 'html',
	        data: 'act=repeat_panggilan&no_antrian='+window.btoa(id_panggilan)+'&loket=<?php echo base64_encode($_SESSION['loket'])?>',
	        success: function(mydata) {
	        	if(mydata!="OK"){
	        		show_alert('error','Opps',mydata);
	        	}
	        	else {
	        		var id_dt = $("#id_data_antrian").val();
	        	}
	        }
	    });
	}
	else {
		show_alert('warning','Opps','Nomor Antrian Kosong');
	}
	refresh();
}
function get_fast_antrian(){
	var no_antrian = $("#no_antrian_cepat").val();
	if(no_antrian!=""){
		$.ajax({
			url: 'ajax/antrian.php',
			type: 'POST',
			dataType: 'html',
			data: 'act=antrian_fast&no_antrian='+window.btoa(no_antrian)+'&loket=<?php echo base64_encode($_SESSION['loket'])?>',
			success: function(mydata) {
				if(mydata=="NOK"){
					show_alert('error','Terjadi Kesalahan','Nomor Antrian Tidak Terdaftar');
					get_current_antrian();	
				}
				else if(mydata=="NOK2"){
					show_alert('error','Terjadi Kesalahan','Melebihi batas pemanggilan');
					get_current_antrian();	
				}
				else {
					 $("#id_panggilan").val(no_antrian);
					 var arr = mydata.split('%%$$%%');
						$("#id_panggilan_speaker").val(arr[0]);
						$("#antrian_now").html(arr[0]);
						$("#id_data_antrian").val(arr[2]);
					 refresh_timer();   

				}
			}
		});
	}
	else {
		show_alert('error','Terjadi Kesalahan','Nomor Antrian Kosong');
	}
	refresh();
}
function get_next_antrian_show(){
	$.ajax({
		url: 'ajax/antrian.php',
		type: 'POST',
		dataType: 'html',
		data: 'act=antrian_next_show&loket=<?php echo base64_encode($_SESSION['loket'])?>',
		success: function(mydataku) {
			if(mydataku!=""){
				$("#antrian_next").html(mydataku);
			}
			else {
				$("#antrian_next").html("-");
			}

		}
	});	
}
function check_antrian(){
	$.ajax({
	        url: 'ajax/antrian.php',
	        type: 'POST',
	        dataType: 'html',
	        data: 'act=check_antrian&loket=<?php echo base64_encode($_SESSION['loket'])?>',
	        success: function(mydata) {
	        	var arr = mydata.split('%%$$%%');
	            var atr_now =  $("#antrian_now").html();
	            if(atr_now!="-"){
	            	if(atr_now!=arr[1]){
		            	location.reload();
		            }
	            }
	          
	        }
	    });
}
function get_current_antrian(aksi=''){
	$.ajax({
        url: 'ajax/antrian.php',
        type: 'POST',
        dataType: 'html',
        data: 'act=antrian_current&loket=<?php echo base64_encode($_SESSION['loket'])?>',
        success: function(mydata) {
        	var arr = mydata.split('%%$$%%');
            $("#antrian_now").html(arr[1]);
            $("#id_panggilan").val(arr[0]);
            $("#id_panggilan_speaker").val(arr[1]);
            $("#id_data_antrian").val(arr[3]);
            get_sisa_antrian();
            //next antrian show

            if(aksi=='next'){
            	get_panggilan("<?php echo $_SESSION['loket'] ?>",arr[1]);
            	check_antrian();
            }

        }
    });
}
function get_panggilan(loket,id){
	$.ajax({
        url: 'ajax/antrian.php',
        type: 'POST',
        dataType: 'html',
        data: 'act=antrian_panggil&loket='+loket+'&id='+id,
        success: function(mydata) {
           //console.log(mydata);
        }
    });
}
function reset_data(){
		var loket      = "<?php echo $_SESSION['loket']?>";
		var param      = " and LOKET ='"+loket+"' ";
		var param2="";
		$.ajax({
          url: 'ajax/data_table/histori_antrian.php',
          type: 'POST',
          data: 'param='+btoa(param)+'&param2='+btoa(param2),
          success: function(mydata) {
              $('#data_histori').html(mydata);
          }
     	 });
}
function get_next_antrian(){
	var id_panggilan = $("#id_panggilan").val();
	$.ajax({
        url: 'ajax/antrian.php',
        type: 'POST',
        dataType: 'html',
        data: 'act=antrian_next&id_panggilan='+window.btoa(id_panggilan)+'&loket=<?php echo base64_encode($_SESSION['loket'])?>',
        success: function(mydata) {
        	console.log(mydata);
			if(mydata=="-"){
				refresh_timer();
				get_current_antrian('next');
			}
			else {
				if(mydata!=""){
					var arr = mydata.split('%%$$%%');
					$("#id_panggilan").val(arr[0]);
					$("#id_panggilan_speaker").val(arr[1]);
					$("#id_data_antrian").val(arr[3]);
		            refresh_timer();
				}
				else {
					$("#antrian_next").html("0");
					refresh_timer();
				}
				get_sisa_antrian();
				get_current_antrian('next');

			}
          
        }
    });
	
	setTimeout(function(){ 
		refresh(); 
	}, 3000);
	
}
function get_total_antrian(){
	$.ajax({
        url: 'ajax/antrian.php',
        type: 'POST',
        dataType: 'html',
        data: 'act=antrian_total&loket=<?php echo base64_encode($_SESSION['loket'])?>',
        success: function(mydata) {
            $("#antrian_total").html(mydata);
        }
    });
}
function get_sisa_antrian(){
	$.ajax({
			url: 'ajax/antrian.php',
			type: 'POST',
			dataType: 'html',
			data: 'act=antrian_sisa&loket=<?php echo base64_encode($_SESSION['loket'])?>',
			success: function(mydata) {
				$("#antrian_sisa").html(mydata);
			}
		});
}
function call_biodata(){
	$.ajax({
	        url: 'ajax/pemohon.php',
	        type: 'POST',
	        dataType: 'html',
	        data: 'act=detailPemohon&id_biodata='+id_biodata,
	        success: function(mydata) {
	            $("#detail_biodata").html(mydata);
	        }
	    });
}
function do_logout(){
	swal({
	  title: 'Keluar Aplikasi',
	  type:  'warning',      // warning,info,success,error
	  showCancelButton: true,
	  showLoaderOnConfirm: true,
	  preConfirm: function(){
		$.ajax({
			url: 'crud/do_check_pwd_logout.php', 
			type: 'POST',
			data: new FormData($('#form_keluar')[0]),  // Form ID
			processData: false,
			contentType: false,
			beforeSend: function() {
				$('#keluar_btn').prop('disabled', true);
			},
			success: function(data) {
				$('#keluar_btn').prop('disabled', false);
				if(data=="OK") {
					swal({
						title: 'Sukses',
						html: 'Aplikasi di tutup',
						type: 'success',
						showCancelButton: false,
						showLoaderOnConfirm: false,
					  }).then(function() {
							  window.location = "."; 
						});
				} 
				else
				{
					swal({
						title: 'Error',
						html: data,
						type: 'error',
						showCancelButton: false,
						showLoaderOnConfirm: false,
					  });
				}
			}
		});
	 }
	});   
}	
function do_act(form_id,act_controller,header_text,content_text,type_icon){
		swal({
		  title: header_text,
		  text: content_text,
		  type: type_icon,      // warning,info,success,error
		  showCancelButton: true,
		  showLoaderOnConfirm: true,
		  preConfirm: function(){
			$.ajax({
				url: act_controller, 
				type: 'POST',
				data: new FormData($('#'+form_id)[0]),  // Form ID
				processData: false,
				contentType: false,
				success: function(data) {
					var data_trim = $.trim(data);
					if(data_trim=="OK")
					{
						swal({
							title: 'Success',
							type: 'success',
							showCancelButton: false,
							showLoaderOnConfirm: false,
						  }).then(function() {
						  		$('#visitor').modal('toggle');
								// get_total_antrian();
								// get_sisa_antrian();
								// get_current_antrian();
								// get_repeat_antrian();
								// $('#'+form_id).trigger("reset");
							});
					} 
					else
					{
						swal({
							title: 'Error',
							html: data_trim,
							type: 'error',
							showCancelButton: false,
							showLoaderOnConfirm: false,
						  });
					}
				}
			});
		 }
		});   
}
	function get_name_lbg(id){
		$.ajax({
		        url: 'ajax/pemohon.php',
		        type: 'POST',
		        dataType: 'JSON',
		        data: 'act=reqPemohonLBG&id_lbg='+id,
		        success: function(mydata) {
		            $("#lembaga").val(mydata.lembaga_name);
		            $("#lembaga_id").val(mydata.lembaga_id);
		            $('#picker_lembaga').modal('toggle');
		        }
		    });
	}
	function get_name_lbg2(id){
		$.ajax({
		        url: 'ajax/pemohon.php',
		        type: 'POST',
		        dataType: 'JSON',
		        data: 'act=reqPemohonLBG&id_lbg='+id,
		        success: function(mydata) {
		            $("#lembaga_his").val(mydata.lembaga_name);
		            $("#lembaga_id_his").val(id);
		            $('#picker_lembaga2').modal('toggle');
		        }
		    });
	}
	function get_name_pmh(id){
		$.ajax({
		        url: 'ajax/pemohon.php',
		        type: 'POST',
		        dataType: 'JSON',
		        data: 'act=reqPemohonID&id_biodata='+id,
		        success: function(mydata) {
		            $("#nama_pemohon").val(mydata.NAMA);
		            $("#id_pemohon").val(mydata.ID_BIODATA);
		            $('#picker_pemohon').modal('toggle');
		        }
		    });
	}
	function get_name_pmh2(id){
		$.ajax({
		        url: 'ajax/pemohon.php',
		        type: 'POST',
		        dataType: 'JSON',
		        data: 'act=reqPemohonID&id_biodata='+id,
		        success: function(mydata) {
		            $("#nama_pemohon_his").val(mydata.NAMA);
		            $("#id_pemohon_his").val(window.atob(mydata.ID_BIODATA));
		            $('#picker_pemohon2').modal('toggle');
		        }
		    });
	}
	function edit_history(id){
		 	$.ajax({
		        url: 'ajax/history.php?id_antrian='+id,
		        type: 'POST',
		        dataType: 'JSON',
		        data: '',
		        success: function(mydata) {
					$("#id_his").val(id);
					$.ajax({
						url: 'ajax/pemohon.php',
						type: 'POST',
						dataType: 'JSON',
						data: 'act=reqPemohonID&id_biodata='+window.btoa(mydata.bio_id),
						success: function(mydataku) {
							$("#nama_pemohon_his").val(mydataku.NAMA);
						}
					});
					
					$.ajax({
						url: 'ajax/pemohon.php',
						type: 'POST',
						dataType: 'JSON',
						data: 'act=reqPemohonCMPID&id_cmp='+window.btoa(mydata.cp_id),
						success: function(mydataku) {
							$("#instansi_his").val(mydataku.company_name);
						}
					});
					
		            $("#id_pemohon_his").val(mydata.bio_id);
		            $("#instansi_id_his").val(mydata.cp_id);
		            $("#no_urut_his").val(mydata.no_urut);
		            $("#jns_permohonan_his").val(mydata.jns);
		            $("#perihal_his").val(mydata.perihal);
		            $("#ket_his").val(mydata.ket);
					$("#jenis_layanan_his").val(mydata.jns_layanan);
					
					setTimeout(function(){  
						var rn = $("#jenis_layanan_his").val();
						if(rn!=""){
							$.ajax({
								url: 'ajax/group_layanan.php',
								type: 'POST',
								dataType: 'html',
								data: 'rn='+window.btoa(rn),
								success: function(mydata2) {
									$("#group_layanan_his").html(mydata2);
									$("#group_layanan_his").val(mydata.grp);
								}
							});
						}
					}, 2000);
					
					setTimeout(function(){  
						var rn2 = $("#group_layanan_his").val();
						if(rn2!=""){
							$.ajax({
								url: 'ajax/sub_group_layanan.php',
								type: 'POST',
								dataType: 'html',
								data: 'rn='+window.btoa(rn2),
								success: function(mydata2) {
									$("#sub_group_layanan_his").html(mydata2);
									$("#sub_group_layanan_his").val(mydata.sub);
								}
							});
						}
					}, 3000);
					
					
					if(mydata.sertifikasi=="Ya"){
						$("#sertifikasi_his").show();
						$("#tgl_masuk_his").val(mydata.tgl_masuk);
						$("#tgl_keluar_his").val(mydata.tgl_keluar);
						$("#tgl_pembayaran_pnbp_his").val(mydata.tgl_bayar);
						$("#bank_his").val(mydata.bank);
						$("#no_sertifikat_lama_his").val(mydata.old_sertifikat);
						$("#no_sertifikat_baru_his").val(mydata.new_sertifikat);
						$("#pengurus_his").val(mydata.pengurus);
						$("#contact_pengurus_his").val(mydata.contact_pengurus);
						$.ajax({
							url: 'ajax/pemohon.php',
							type: 'POST',
							dataType: 'JSON',
							data: 'act=reqPemohonLBG&id_lbg='+window.btoa(mydata.lembaga),
							success: function(mydataku) {
								$("#lembaga_his").val(mydataku.lembaga_name);
							}
						});
						
					}
					else {
						$("#sertifikasi_his").hide();
					}
					
		        }
		    });
	}
	function edit_biodata(id){
		$.ajax({
		        url: 'ajax/pemohon.php',
		        type: 'POST',
		        dataType: 'JSON',
		        data: 'act=reqPemohonID&id_biodata='+id,
		        success: function(mydata) {
		            $("#nama_edit").val(mydata.NAMA);
		            $("#id_pemohon_edit").val(mydata.ID_BIODATA);
		            $("#no_telepon_edit").val(mydata.NO_TLP);
		            $("#no_hp_edit").val(mydata.NO_HP);
		            $("#nik_edit").val(mydata.NIK);
		            $("#alamat_edit").val(mydata.ALAMAT);
		            $('input:radio[name="kelamin"][value='+mydata.JENIS_KELAMIN+']').attr('checked', true);
		        }
		    });
	}
	function edit_company(id){
		$.ajax({
		        url: 'ajax/pemohon.php',
		        type: 'POST',
		        dataType: 'JSON',
		        data: 'act=reqPemohonCMPID&id_cmp='+id,
		        success: function(mydata) {
					$("#id_propinsi_edit").val(mydata.id_prov);
					get_kabkot('id_propinsi_edit','id_kabkot_edit','edit');
		            $("#company_name_edit").val(mydata.company_name);
		            $("#id_company_edit").val(mydata.company_id);
		            $("#no_telepon_cmp_edit").val(mydata.no_tlp);
		            $("#no_hp_cmp_edit").val(mydata.no_hp);
		            $("#alamat_cmp_edit").val(mydata.alamat);
					setTimeout(function(){  
						$("#id_kabkot_edit").val(mydata.id_kabkot); 
					}, 1000);
		        }
		    });
	}
	function edit_lembaga(id){
		$.ajax({
		        url: 'ajax/pemohon.php',
		        type: 'POST',
		        dataType: 'JSON',
		        data: 'act=reqPemohonLBG&id_lbg='+id,
		        success: function(mydata) {
					$("#id_propinsi_lembaga_edit").val(mydata.id_prov);
					get_kabkot('id_propinsi_lembaga_edit','id_kabkot_lembaga_edit','edit');
		            $("#lembaga_name_edit").val(mydata.lembaga_name);
		            $("#id_lembaga_edit").val(mydata.lembaga_id);
		            $("#no_telepon_lembaga_edit").val(mydata.no_tlp);
		            $("#no_hp_lembaga_edit").val(mydata.no_hp);
		            $("#alamat_lembaga_edit").val(mydata.alamat);
					setTimeout(function(){  
						$("#id_kabkot_lembaga_edit").val(mydata.id_kabkot); 
					}, 1000);
		        }
		    });
	}
	function get_name_cmp(id){
		$.ajax({
		        url: 'ajax/pemohon.php',
		        type: 'POST',
		        dataType: 'JSON',
		        data: 'act=reqPemohonCMPID&id_cmp='+id,
		        success: function(mydata) {
		            $("#instansi").val(mydata.company_name);
		            $("#instansi_id").val(mydata.company_id);
		            $('#picker_company').modal('toggle');
		        }
		    });
	}
	function get_name_cmp2(id){
		$.ajax({
		        url: 'ajax/pemohon.php',
		        type: 'POST',
		        dataType: 'JSON',
		        data: 'act=reqPemohonCMPID&id_cmp='+id,
		        success: function(mydata) {
		            $("#instansi_his").val(mydata.company_name);
		            $("#instansi_id_his").val(window.atob(mydata.company_id));
		            $('#picker_company2').modal('toggle');
		        }
		    });
	}
function date_to_default(tgl){
  var newdate = tgl.split("/").reverse().join("-");
  return newdate;
}
var dTable1_1;
var dTable1;
var dTable2;
var dTable2_2;
var dTable4;
$(document).ready(function() {
	
	dTable1_1 = $('#table_data_pemohon2').DataTable( {
		"bProcessing": true,
		"bServerSide": true,
		"bJQueryUI": false,
		"responsive": false,
		"autoWidth": false,
		"sAjaxSource": "ajax/serverSide/show_table_pemohon2.php", 
		"sServerMethod": "POST",
		"scrollX": true,
		"scrollY": "300px",
			"scrollCollapse": true,
		"columnDefs": [
		{ "orderable": true, "targets": 0, "searchable": true},
		{ "orderable": true, "targets": 1, "searchable": true},
		{ "orderable": true, "targets": 2, "searchable": true},
		{ "orderable": true, "targets": 3, "searchable": true},
		{ "orderable": true, "targets": 4, "searchable": true},
		{ "orderable": true, "targets": 5, "searchable": true},
		{ "orderable": false, "targets": 6, "searchable": false}
		]
	} );
	
	dTable1 = $('#table_data_pemohon').DataTable( {
		"bProcessing": true,
		"bServerSide": true,
		"bJQueryUI": false,
		"responsive": false,
		"autoWidth": false,
		"sAjaxSource": "ajax/serverSide/show_table_pemohon.php", 
		"sServerMethod": "POST",
		"scrollX": true,
		"scrollY": "300px",
			"scrollCollapse": true,
		"columnDefs": [
		{ "orderable": true, "targets": 0, "searchable": true},
		{ "orderable": true, "targets": 1, "searchable": true},
		{ "orderable": true, "targets": 2, "searchable": true},
		{ "orderable": true, "targets": 3, "searchable": true},
		{ "orderable": true, "targets": 4, "searchable": true},
		{ "orderable": true, "targets": 5, "searchable": true},
		{ "orderable": false, "targets": 6, "searchable": false}
		]
	} );

	dTable2_2 = $('#table_data_company2').DataTable( {
		"bProcessing": true,
		"bServerSide": true,
		"bJQueryUI": false,
		"responsive": false,
		"autoWidth": false,
		"sAjaxSource": "ajax/serverSide/show_table_company2.php", 
		"sServerMethod": "POST",
		"scrollX": true,
		"scrollY": "300px",
			"scrollCollapse": true,
		"columnDefs": [
		{ "orderable": true, "targets": 0, "searchable": true},
		{ "orderable": true, "targets": 1, "searchable": true},
		{ "orderable": true, "targets": 2, "searchable": true},
		{ "orderable": true, "targets": 3, "searchable": true}
		]
	} );
	
	dTable2 = $('#table_data_company').DataTable( {
		"bProcessing": true,
		"bServerSide": true,
		"bJQueryUI": false,
		"responsive": false,
		"autoWidth": false,
		"sAjaxSource": "ajax/serverSide/show_table_company.php", 
		"sServerMethod": "POST",
		"scrollX": true,
		"scrollY": "300px",
			"scrollCollapse": true,
		"columnDefs": [
		{ "orderable": true, "targets": 0, "searchable": true},
		{ "orderable": true, "targets": 1, "searchable": true},
		{ "orderable": true, "targets": 2, "searchable": true},
		{ "orderable": true, "targets": 3, "searchable": true}
		]
	} );
	
	
	dTable3 = $('#table_data_history').DataTable( {
		"bProcessing": true,
		"bServerSide": true,
		"bJQueryUI": false,
		"responsive": false,
		"autoWidth": false,
		"sAjaxSource": "ajax/serverSide/show_table_history.php?loket=<?php echo base64_encode($_SESSION['loket'])?>", 
		"sServerMethod": "POST",
		"scrollX": true,
		"scrollY": "300px",
		"scrollCollapse": true,
		"order": [[ 3, "asc" ]],
		"columnDefs": [
		{ "orderable": true, "targets": 0, "searchable": true},
		{ "orderable": true, "targets": 1, "searchable": true},
		{ "orderable": true, "targets": 2, "searchable": true},
		{ "orderable": true, "targets": 3, "searchable": true},
		{ "orderable": true, "targets": 4, "searchable": true},
		{ "orderable": false, "targets": 5, "searchable": false}
		]
	} );
	
	dTable4 = $('#table_data_lembaga').DataTable( {
		"bProcessing": true,
		"bServerSide": true,
		"bJQueryUI": false,
		"responsive": false,
		"autoWidth": false,
		"sAjaxSource": "ajax/serverSide/show_table_lembaga.php", 
		"sServerMethod": "POST",
		"scrollX": true,
		"scrollY": "300px",
			"scrollCollapse": true,
		"columnDefs": [
		{ "orderable": true, "targets": 0, "searchable": true},
		{ "orderable": true, "targets": 1, "searchable": true},
		{ "orderable": true, "targets": 2, "searchable": true},
		{ "orderable": true, "targets": 3, "searchable": true}
		]
	} );
	
	dTable5 = $('#table_data_lembaga2').DataTable( {
		"bProcessing": true,
		"bServerSide": true,
		"bJQueryUI": false,
		"responsive": false,
		"autoWidth": false,
		"sAjaxSource": "ajax/serverSide/show_table_lembaga2.php", 
		"sServerMethod": "POST",
		"scrollX": true,
		"scrollY": "300px",
			"scrollCollapse": true,
		"columnDefs": [
		{ "orderable": true, "targets": 0, "searchable": true},
		{ "orderable": true, "targets": 1, "searchable": true},
		{ "orderable": true, "targets": 2, "searchable": true},
		{ "orderable": true, "targets": 3, "searchable": true}
		]
	} );
	
});

function refresh_table(){
	setTimeout(function(){ 
	 	$('#table_data_pemohon2').DataTable().ajax.reload();
	 	$('#table_data_pemohon').DataTable().ajax.reload();
	 	$('#table_data_company').DataTable().ajax.reload();
	 	$('#table_data_company2').DataTable().ajax.reload();
	 	$('#table_data_history').DataTable().ajax.reload();
	 	$('#table_data_lembaga').DataTable().ajax.reload();
	 	$('#table_data_lembaga2').DataTable().ajax.reload();
	}, 2000);
}
</script>
</html>