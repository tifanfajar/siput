<?php include '../config.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>CSO - KOMINFO</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="icon" href="assets/logo.png">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/login.css">
	<link rel="stylesheet" type="text/css" href="css/animate.css">
	<link rel="stylesheet" type="text/css" href="js/sweetalert2/dist/sweetalert2.min.css">
</head>
<body>

<div class="container">
	<div class="row">
		<div class="col-md-12 animated fadeInUp center-logo">
			<img src="assets/logo.png" width="50" style="text-align: center; display: block; margin: 0 auto;">
			<h4 style="margin-bottom: 10px;">KEMENTERIAN KOMUNIKASI DAN INFORMATIKA <br> REPUBLIK INDONESIA</h4>
			<p>Sistem Aplikasi Manajemen Antrian</p>
		</div>
		<div class="col-md-2"></div>
		<div class="col-md-4 left animated fadeInLeft">
			<label>Untuk login aplikasi, silahkan isi informasi pengguna pada form username dan password.</label>
			<img src="assets/1.png" width="100%;">
			<p>&copy; <?php echo date("Y") ?> KEMKOMINFO REPUBLIK INDONESIA</p>
		</div>
		<div class="col-md-4 right animated fadeInRight">
			<form class="form-horizontal" method="POST" id="f1">
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-3 control-label">Username</label>
					<div class="col-sm-12">
						<input type="text" class="form-control" id="inputEmail3" name="username" required>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Password</label>
					<div class="col-sm-12">
						<input type="password" class="form-control" id="inputPassword3" name="password" required>
					</div>
				</div>
				<!--<div class="form-group">
					<input type="checkbox" name="" class="col-sm-2" style="float: left; width: 20px; margin-left: 15px;">
					<label for="inputPassword3" class="col-sm-6">Remember Me</label>
				</div>-->
				<div class="form-group">
					<button class="btn btn-info" type="submit" style="margin-left: 15px;">Login</button>
				</div>
			</form>
		</div>
		<div class="col-md-2"></div>
	</div>
</div>

<!-- Script -->

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script src="js/sweetalert2/dist/sweetalert2.min.js"></script>
<script>
$("#f1").on("submit", function (event) {
	event.preventDefault();
	do_proses('f1','crud/do_login.php','home');
});

function do_proses(form_id,act_controller,after_controller){
	$.ajax({
		url: act_controller, 
		type: 'POST',
		data: new FormData($('#'+form_id)[0]),  // Form ID
		processData: false,
		contentType: false,
		success: function(data) {
			var data_trim = $.trim(data);
			if(data_trim=="OK"){
				swal({
					title: 'Success',
					type: 'success',
					showCancelButton: false,
					showLoaderOnConfirm: false,
				  }).then(function() {
						 if(after_controller!=''){
							window.location = '<?php echo $basepath_cso ?>'+after_controller;
						 }
						else {
							location.reload();
						} 
				 });
			} 
			else if(data_trim=="NOK"){
				  swal({
					title: 'Error',
					text: "Username / Password Salah",
					type: 'error',
					showCancelButton: false,
					showLoaderOnConfirm: false,
				  });
			}
		}
	});
}
</script>
</body>
</html>