<?php include 'config.php';  ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>DASHBOARD - KOMINFO</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="assets/images/icon.png">
	<link rel="stylesheet" href="assets/css/index.css">
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/fonts/all.css" >
	<link rel="stylesheet" type="text/css" href="assets/js/sweetalert2/dist/sweetalert2.min.css">
	<link rel="stylesheet" type="text/css" href="assets/js/bootstrap_slider/dist/css/bootstrap-slider.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/dataTables.bootstrap.min.css">
	<style>
		video{width: 100%; display: block;}
		.required{
			color:red;
		}
		.table th {
		   text-align: center;
		   font-weight:bold;
		}
		.datatable>tbody>tr>td
		{
			white-space: nowrap;
		}
	</style>

</head>
<body class="dash-app" onload="startTime()">

	<video autoplay loop id="video-background" muted>
		<source src="assets/images/blueLine.mp4" type="video/mp4">
	</video>

	<!-- Header Logo & Judul App -->
	<div class="container">
	  <div class="row">
	    <div class="col"></div>

	    <div class="col-6 main-cnt-judul">
	     <img src="assets/images/logo.png" class="logo-app" alt="">
	     <span class="judul-app">Sistem Aplikasi Manajemen Antrian</span>
	     <small class="sub-judul-app">Kementerian Komunikasi dan Informatika Republik Indonesia</small>
	    </div>

	    <div class="col"></div>
	  </div>
	</div>

	<!-- Button App Dash -->
	<div class="container" style="margin-top: 2%;">
	  <div class="row justify-content-center align-items-center h-100">

	    <div data-tilt class="col-md-3">
	    	<!--<a href="" class="btn-dashboard" data-toggle="modal" data-target="#modal_kiosk">-->
	    	<a target="_blank" href="KIOSK" class="btn-dashboard">
	    		
	    		<div class="icon-btn"><i class="fa fa-id-badge"></i></div>
	    		<span class="btn-name">Kiosk</span>
	    		<small  class="btn-des">Loket pemeriksaan dan pengecekan</small>
	    		
	    	</a>
	    </div>

	    
	    <div data-tilt class="col-md-3">
	    	<!--<a href="" class="btn-dashboard" data-toggle="modal" data-target="#modal_plasma">-->
	    	<a target="_blank" href="PLASMA" class="btn-dashboard">	
	    		<div class="icon-btn"><i class="fa fa-tv"></i></div>
	    		<span class="btn-name">Plasma</span>
	    		<small class="btn-des">Loket pelayanan pembayaran</small>
	    		
	    	</a>
	    </div>

	    <div data-tilt class="col-md-3">
	    	<!--<a href="" class="btn-dashboard" data-toggle="modal" data-target="#modal_loket">-->
	    	<a target="_blank" href="CSO" class="btn-dashboard">	
	    		<div class="icon-btn"><i class="fa fa-users"></i></div>
	    		<span class="btn-name">Loket</span>
	    		<small class="btn-des">Loket Pendaftaran</small>
	    		
	    	</a>
	    </div>

	  <!--   <div class="w-100"></div> -->


		 <div class="w-100"></div>

	  	
		
	    <div data-tilt class="col-md-3">
	    	<a target="_blank" href="SKP" class="btn-dashboard">
	    		
	    		<div class="icon-btn"><i class="fa fa-comment"></i></div>
	    		<span class="btn-name">Pendapat Layanan</span>
	    		<small class="btn-des">Loket pelayanan pembayaran</small>
	    		
	    	</a>
	    </div>


	    <div data-tilt class="col-md-3">
	    	<!--<a href="" class="btn-dashboard" data-toggle="modal" data-target="#modal_management">-->
	    	<a target="_blank" href="MANAGEMENT" class="btn-dashboard">	
	    		
	    		<div class="icon-btn"><i class="fa fa-list-alt"></i></div>
	    		<span class="btn-name">Manajemen</span>
	    		<small class="btn-des">Loket pelayanan pembayaran</small>
	    		
	    	</a>
	    </div>

	  </div>
	</div>

	<div class="running-text-cnt" >
		<div class="cnt-clock">
			<div id="txt"></div>
		</div>
		<?php 
			$running_text = mysqli_query($con,"select * from app_running_text_multi_antrian where STATUS='AKTIF'");
			$rn = "";
			$separator = "&nbsp; <img style='width:25px;height:25px' src='assets/images/logo.png'> &nbsp;";
			while ($data_rn = mysqli_fetch_object($running_text)){
				$rn .= $data_rn->TEXT.$separator;
			}
		?>
		<marquee style="cursor:pointer"  behavior="" direction="left"><?php echo $rn ?></marquee>
	</div>
	
  

	<script type="text/javascript" src="assets/js/jquery.js"></script>
	<script src="assets/js/jquery.dataTables.min.js"></script>
	<script src="assets/js/dataTables.bootstrap.min.js"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/time.js"></script>
	<script src="assets/js/sweetalert2/dist/sweetalert2.min.js"></script>
	<script src="assets/js/bootstrap_slider/dist/bootstrap-slider.min.js"></script>
</body>
</html>