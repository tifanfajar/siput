<?php
error_reporting(null);
$basepath 	  	  = "http://".$_SERVER['HTTP_HOST']."/antrian/"; //folder project
$basepath_cso 	  = "http://".$_SERVER['HTTP_HOST']."/antrian/CSO/"; //folder project CSO
$basepath_plasma  = "http://".$_SERVER['HTTP_HOST']."/antrian/PLASMA/"; //folder project PLASMA
$basepath_kiosk   = "http://".$_SERVER['HTTP_HOST']."/antrian/KIOSK/"; //folder project KIOSK
$basepath_skp  	  = "http://".$_SERVER['HTTP_HOST']."/antrian/SKP/"; //folder project SKP
$basepath_speaker = "http://".$_SERVER['HTTP_HOST']."/antrian/SPEAKER/"; //folder project Speaker
$con = mysqli_connect("localhost","root","","dbantrianv10");	//database config
if(mysqli_connect_errno()){
echo "Database did not connect";
exit();
}

date_default_timezone_set("Asia/Jakarta");
$date_now_indo = date("Y-m-d");
$date_now_indo_full = date("Y-m-d H:i:s");

//For Connection DataTables ServerSide
function mysqliConnection(){
	// Database connection information
	$gaSql['user']     = 'root';	//Username
	$gaSql['password'] = '';   //Password
	$gaSql['db']       = 'dbantrianv10';  //Database
	$gaSql['server']   = 'localhost';
	$gaSql['charset']  = 'utf8';
	$db = new mysqli($gaSql['server'], $gaSql['user'], $gaSql['password'], $gaSql['db']);
	if (mysqli_connect_error()) {
		die( 'Error connecting to MySQL server (' . mysqli_connect_errno() .') '. mysqli_connect_error() );
	}

	if (!$db->set_charset($gaSql['charset'])) {
		die( 'Error loading character set "'.$gaSql['charset'].'": '.$db->error );
	}
	return $db;
}


//Property DataTables ServerSide
function Paging( $input ){
		$sLimit = "";
		if ( isset( $input['iDisplayStart'] ) && $input['iDisplayLength'] != '-1' ) {
			$sLimit = " LIMIT ".intval( $input['iDisplayStart'] ).", ".intval( $input['iDisplayLength'] );
		}

		return $sLimit;
}

function Ordering( $input, $aColumns ){
				$aOrderingRules = array();
				if ( isset( $input['iSortCol_0'] ) ) {
					$iSortingCols = intval( $input['iSortingCols'] );
					for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
						if ( $input[ 'bSortable_'.intval($input['iSortCol_'.$i]) ] == 'true' ) {
							$aOrderingRules[] =
							$aColumns[ intval( $input['iSortCol_'.$i] ) ]." "
							.($input['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
						}
					}
				}

				if (!empty($aOrderingRules)) {
					$sOrder = " ORDER BY ".implode(", ", $aOrderingRules);
					} else {
					$sOrder = "";
				}
				return $sOrder;
}

function Filtering( $aColumns, $iColumnCount, $input, $db ){
				if ( isset($input['sSearch']) && $input['sSearch'] != "" ) {
					$aFilteringRules = array();
					for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
						if ( isset($input['bSearchable_'.$i]) && $input['bSearchable_'.$i] == 'true' ) {
							$aFilteringRules[] = $aColumns[$i]." LIKE '%".$db->real_escape_string( $input['sSearch'] )."%'";
						}
					}
					if (!empty($aFilteringRules)) {
						$aFilteringRules = array('('.implode(" OR ", $aFilteringRules).')');
					}
}

				// Individual column filtering
				for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
					if ( isset($input['bSearchable_'.$i]) && $input['bSearchable_'.$i] == 'true' && $input['sSearch_'.$i] != '' ) {
						$aFilteringRules[] = $aColumns[$i]."  LIKE '%".$db->real_escape_string($input['sSearch_'.$i])."%'";
					}
				}

				if (!empty($aFilteringRules)) {
					$sWhere = " WHERE ".implode(" OR ", $aFilteringRules);
					} else {
					$sWhere = " WHERE 1=1 ";
				}
				return $sWhere;
}


// error_reporting(null);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

//Get IP
function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function date_indo($date){  //Only  yyyy-mm-dd
	$newDate = date("d/m/Y", strtotime($date));
	if($date==null or $date==""){
		return "";
	}
	else {
		return $newDate;
	}
}

function tgl_indo($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun
 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

function num_with_zero($num){
	$num_padded = sprintf("%03d", $num);
	return $num_padded;
}

//Insert Log
function insert_log($con,$act,$ip,$agent,$kode_user,$tgl,$query){
	$random_number = substr(str_shuffle("0123456789"), 0, 5); 
	$generate 	   = "log_".date("ymdhis")."_".$random_number;
	
	mysqli_query($con,"insert into tb_log_user(kode_log,kode_user,aksi,ip_address,agent,tanggal,query) values ('".$generate."','".$kode_user."','".$act."','".$ip."','".$agent."','".$tgl."','".$query."')") or die(mysqli_error($con));
	
}

date_default_timezone_set("Asia/Jakarta");
$tgl_indo = date("Y-m-d");
$tgl_indo_full = date("Y-m-d H:i:s");
$ip = get_client_ip();
$agent = $_SERVER['HTTP_USER_AGENT'];
?>
