<html>
<head>
  <title>SKP - KOMINFO</title>
<link rel="icon" href="images/logo-kominfo.png">
<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Arimo' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Hind:300' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
<style>
html { 
  background: url(images/imgG.jpg) no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  overflow: hidden;
}

img{
  display: block;
  margin: auto;
  width: 100%;
  height: auto;
}

#login-button{
  cursor: pointer;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  padding: 30px;
  margin: auto;
  width: 100px;
  height: 100px;
  border-radius: 50%;
  background: rgba(3,3,3,.8);
  overflow: hidden;
  opacity: 0.7;
  box-shadow: 10px 10px 30px #000;}

/* Login container */
#container{
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  width: 260px;
  height: 260px;
  border-radius: 5px;
  background: rgba(3, 3, 3, 0.87);
  box-shadow: 1px 1px 50px #000;
  display: none;
}

.close-btn{
  position: absolute;
  cursor: pointer;
  font-family: 'Open Sans Condensed', sans-serif;
  line-height: 18px;
  top: 3px;
  right: 3px;
  width: 20px;
  height: 20px;
  text-align: center;
  border-radius: 10px;
  opacity: .2;
  -webkit-transition: all 2s ease-in-out;
  -moz-transition: all 2s ease-in-out;
  -o-transition: all 2s ease-in-out;
  transition: all 0.2s ease-in-out;
}

.close-btn:hover{
  opacity: .5;
}

/* Heading */
h4{
  font-family: 'Open Sans Condensed', sans-serif;
  position: relative;
  margin-top: 10px;
  text-align: center;
  /*font-size: 40px;*/
  color: #ddd;
  text-shadow: 3px 3px 10px #000;
}

/* Inputs */
.tombol,
input{
  font-family: 'Open Sans Condensed', sans-serif;
  text-decoration: none;
  position: relative;
  width: 80%;
  display: block;
  margin: 9px auto;
  font-size: 17px;
  color: #fff;
  padding: 8px;
  border-radius: 6px;
  border: none;
  background: rgba(255, 255, 255, 0.1);
  -webkit-transition: all 2s ease-in-out;
  -moz-transition: all 2s ease-in-out;
  -o-transition: all 2s ease-in-out;
  transition: all 0.2s ease-in-out;
}

input:focus{
  outline: none;
  box-shadow: 3px 3px 10px #333;
  background: rgba(3,3,3,.18);
}

/* Placeholders */
::-webkit-input-placeholder {
   color: #ddd;  }
:-moz-placeholder { /* Firefox 18- */
   color: red;  }
::-moz-placeholder {  /* Firefox 19+ */
   color: red;  }
:-ms-input-placeholder {  
   color: #333;  }

/* Link */
.tombol{
  font-family: 'Open Sans Condensed', sans-serif;
  text-align: center;
  padding: 4px 8px;
  background: rgba(107,255,3,0.3);
}

.tombol:hover{
  opacity: 0.7;
}

#remember-container{
  position: relative;
  margin: -5px 20px;
}

.checkbox {
  position: relative;
  cursor: pointer;
	-webkit-appearance: none;
	padding: 5px;
	border-radius: 4px;
  background: rgba(3,3,3,.2);
	display: inline-block;
  width: 16px;
  height: 15px;
}

.checkbox:checked:active {
	box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px 1px 3px rgba(0,0,0,0.1);
}

.checkbox:checked {
  background: rgba(3,3,3,.4);
	box-shadow: 0 1px 2px rgba(0,0,0,0.05), inset 0px -15px 10px -12px rgba(0,0,0,0.05), inset 15px 10px -12px rgba(255,255,255,0.5);
	color: #fff;
}

.checkbox:checked:after {
	content: '\2714';
	font-size: 10px;
	position: absolute;
	top: 0px;
	left: 4px;
	color: #fff;
}

#remember{
  position: absolute;
  font-size: 13px;
  font-family: 'Hind', sans-serif;
  color: rgba(255,255,255,.5);
  top: 7px;
  left: 20px;
}

#forgotten{
  position: absolute;
  font-size: 12px;
  font-family: 'Hind', sans-serif;
  color: rgba(255,255,255,.2);
  right: 0px;
  top: 8px;
  cursor: pointer;
  -webkit-transition: all 2s ease-in-out;
  -moz-transition: all 2s ease-in-out;
  -o-transition: all 2s ease-in-out;
  transition: all 0.2s ease-in-out;
}

#forgotten:hover{
  color: rgba(255,255,255,.6);
}

#forgotten-container{
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  width: 260px;
  height: 180px;
  border-radius: 10px;
  background: rgba(3,3,3,0.25);
  box-shadow: 1px 1px 50px #000;
  display: none;
}

.orange-btn{
  background: rgba(87,198,255,.5);
}
.footer-text{
  display: block;
  text-align: center;
  font-family: arial;
  position: absolute;
  bottom: 0px;
  font-size: 12px;
  color: #fff;
  margin-bottom: 10px;
  width: 100%;
}
</style>
</head>
<body onclick="fullScreenApi.requestFullScreen(document.documentElement)">
<!--Content-->
<div id="login-button">
  <img src="images/logo-kominfo.png">
  </img>
</div>
<div id="container">
  <h4>KEMENTERIAN KOMUNIKASI <br> DAN INFORMATIKA</h4>
  <span class="close-btn">
    <img src="https://cdn4.iconfinder.com/data/icons/miu/22/circle_close_delete_-128.png"></img>
  </span>

<form action="login.php" method="post" onSubmit="return validasi()">
    <input type="text" name="username" id="username" placeholder="Username" autocomplete="off">
    <input type="password" name="password" id="password"  placeholder="Password">
    <!--<a href="#" type="submit">Log in</a>-->
    <input type="submit" value="Log in" class="tombol">
    <div id="remember-container">
      <!--<input type="checkbox" id="checkbox-2-1" class="checkbox" checked="checked"/>
      <span id="remember">Remember me</span>
      <span id="forgotten">Forgotten password</span>-->
    </div>
</form>
</div>

<!-- Forgotten Password Container -->
<div id="forgotten-container">
   <h1>Forgotten</h1>
  <span class="close-btn">
    <img src="https://cdn4.iconfinder.com/data/icons/miu/22/circle_close_delete_-128.png"></img>
  </span>

  <form>
    <input type="email" name="email" placeholder="E-mail">
    <a href="#" class="orange-btn">Get new password</a>
</form>
</div>

<span class="footer-text">KEMENTERIAN KOMUNIKASI DAN INFORMATIKA REPUBLIK INDONESA</span>
<!--End Content-->
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/TweenMax.min.js"></script>

<script type="text/javascript">
$("#login-button").click(function() {
  $("#login-button").fadeOut("slow", function() {
    $("#container").fadeIn();
    TweenMax.from("#container", 0.4, { scale: 0, ease: Sine.easeInOut });
    TweenMax.to("#container", 0.4, { scale: 1, ease: Sine.easeInOut });
  });
});

$(".close-btn").click(function() {
  TweenMax.from("#container", 0.4, { scale: 1, ease: Sine.easeInOut });
  TweenMax.to("#container", 0.4, {
    left: "0px",
    scale: 0,
    ease: Sine.easeInOut
  });
  $("#container, #forgotten-container").fadeOut(800, function() {
    $("#login-button").fadeIn(800);
  });
});

/* Forgotten Password */
$("#forgotten").click(function() {
  $("#container").fadeOut(function() {
    $("#forgotten-container").fadeIn();
  });
});

 (function() {
				var
					fullScreenApi = {
						supportsFullScreen: false,
						nonNativeSupportsFullScreen: false,
						isFullScreen: function() { return false; },
						requestFullScreen: function() {},
						cancelFullScreen: function() {},
						fullScreenEventName: '',
						prefix: ''
					},
					browserPrefixes = 'webkit moz o ms khtml'.split(' ');
			 
				// check for native support
				if (typeof document.cancelFullScreen != 'undefined') {
					fullScreenApi.supportsFullScreen = true;
				} else {
					// check for fullscreen support by vendor prefix
					for (var i = 0, il = browserPrefixes.length; i < il; i++ ) {
						fullScreenApi.prefix = browserPrefixes[i];
			 
						if (typeof document[fullScreenApi.prefix + 'CancelFullScreen' ] != 'undefined' ) {
							fullScreenApi.supportsFullScreen = true;
			 
							break;
						}
					}
				}
			 
				// update methods to do something useful
				if (fullScreenApi.supportsFullScreen) {
					fullScreenApi.fullScreenEventName = fullScreenApi.prefix + 'fullscreenchange';
			 
					fullScreenApi.isFullScreen = function() {
						switch (this.prefix) {
							case '':
								return document.fullScreen;
							case 'webkit':
								return document.webkitIsFullScreen;
							default:
								return document[this.prefix + 'FullScreen'];
						}
					}
					fullScreenApi.requestFullScreen = function(el) {
						return (this.prefix === '') ? el.requestFullScreen() : el[this.prefix + 'RequestFullScreen']();
					}
					fullScreenApi.cancelFullScreen = function(el) {
						return (this.prefix === '') ? document.cancelFullScreen() : document[this.prefix + 'CancelFullScreen']();
					}
				}
				else if (typeof window.ActiveXObject !== "undefined") { // IE.
					fullScreenApi.nonNativeSupportsFullScreen = true;
					fullScreenApi.requestFullScreen = fullScreenApi.requestFullScreen = function (el) {
							var wscript = new ActiveXObject("WScript.Shell");
							if (wscript !== null) {
								wscript.SendKeys("{F11}");
							}
					}
					fullScreenApi.isFullScreen = function() {
							return document.body.clientHeight == screen.height && document.body.clientWidth == screen.width;
					}
				}
			 
				// jQuery plugin
				if (typeof jQuery != 'undefined') {
					jQuery.fn.requestFullScreen = function() {
			 
						return this.each(function() {
							if (fullScreenApi.supportsFullScreen) {
								fullScreenApi.requestFullScreen(this);
							}
						});
					};
				}
			 
				// export api
				window.fullScreenApi = fullScreenApi;
			})();
</script>
</body>

<script type="text/javascript">
	function validasi() {
		var username = document.getElementById("username").value;
		var password = document.getElementById("password").value;		
		if (username != "" && password!="") {
                    return true;
		}else{
                    alert('Username dan Password harus di isi !');
                    return false;
		}
	}
</script>

</html>
