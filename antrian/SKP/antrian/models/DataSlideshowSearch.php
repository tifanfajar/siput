<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DataSlideshow;

/**
 * DataSlideshowSearch represents the model behind the search form of `app\models\DataSlideshow`.
 */
class DataSlideshowSearch extends DataSlideshow
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_DATA_SLIDESHOW', 'IMAGE1', 'IMAGE2', 'IMAGE3', 'STATUS'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DataSlideshow::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'ID_DATA_SLIDESHOW', $this->ID_DATA_SLIDESHOW])
            ->andFilterWhere(['like', 'IMAGE1', $this->IMAGE1])
            ->andFilterWhere(['like', 'IMAGE2', $this->IMAGE2])
            ->andFilterWhere(['like', 'IMAGE3', $this->IMAGE3])
            ->andFilterWhere(['like', 'STATUS', $this->STATUS]);

        return $dataProvider;
    }
}
