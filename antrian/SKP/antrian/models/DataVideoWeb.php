<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_video_web".
 *
 * @property string $ID_DATA_VIDEO_WEB
 * @property string $STATUS_VIDEO
 * @property string $PATH_YOUTUBE
 * @property string $PATH_VIDEO
 * @property string $STATUS
 */
class DataVideoWeb extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_video_web';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['STATUS_VIDEO', 'STATUS'], 'required'],
            [['PATH_YOUTUBE'], 'string'],
            [['ID_DATA_VIDEO_WEB'], 'string', 'max' => 8],
            [['STATUS_VIDEO', 'STATUS'], 'string', 'max' => 1],
            [['PATH_VIDEO'], 'file', 'skipOnEmpty' => true, 'extensions' => 'mp4'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_DATA_VIDEO_WEB' => 'Id  Data  Video  Web',
            'STATUS_VIDEO' => 'Status  Video',
            'PATH_YOUTUBE' => 'Path  Youtube',
            'PATH_VIDEO' => 'Path  Video',
            'STATUS' => 'Status',
        ];
    }
}
