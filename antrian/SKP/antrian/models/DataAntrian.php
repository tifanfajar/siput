<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_antrian".
 *
 * @property string $ID_DATA_ANTRIAN
 * @property integer $LOKET
 * @property string $TANGGAL
 * @property integer $NO_URUT
 * @property string $STAT_PANGILAN
 * @property string $STAT_PROSES
 * @property string $TGL_AWAL_PROSES
 * @property string $TGL_AKHIR_PROSES
 * @property integer $WAKTU_PROSES_MENIT
 * @property integer $WAKTU_PROSES_DETIK
 * @property string $JENIS_KELAMIN
 * @property string $KET_KETERLAMBATAN
 * @property string $NAMA
 * @property string $NO_IDENTITAS
 * @property string $INTANSI
 * @property string $ALAMAT
 * @property string $NO_TELP
 */
class DataAntrian extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_antrian';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_DATA_ANTRIAN'], 'required'],
            [['LOKET', 'NO_URUT', 'WAKTU_PROSES_MENIT', 'WAKTU_PROSES_DETIK'], 'integer'],
            [['TANGGAL', 'TGL_AWAL_PROSES', 'TGL_AKHIR_PROSES'], 'safe'],
            [['KET_KETERLAMBATAN', 'ALAMAT'], 'string'],
            [['ID_DATA_ANTRIAN'], 'string', 'max' => 10],
            [['STAT_PANGILAN', 'STAT_PROSES'], 'string', 'max' => 1],
            [['JENIS_KELAMIN'], 'string', 'max' => 255],
            [['NAMA', 'NO_IDENTITAS', 'INTANSI', 'NO_TELP'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_DATA_ANTRIAN' => 'Id  Data  Antrian',
            'LOKET' => 'Loket',
            'TANGGAL' => 'Tanggal',
            'NO_URUT' => 'No  Urut',
            'STAT_PANGILAN' => 'Stat  Pangilan',
            'STAT_PROSES' => 'Stat  Proses',
            'TGL_AWAL_PROSES' => 'Tgl  Awal  Proses',
            'TGL_AKHIR_PROSES' => 'Tgl  Akhir  Proses',
            'WAKTU_PROSES_MENIT' => 'Waktu  Proses  Menit',
            'WAKTU_PROSES_DETIK' => 'Waktu  Proses  Detik',
            'JENIS_KELAMIN' => 'Jenis  Kelamin',
            'KET_KETERLAMBATAN' => 'Ket  Keterlambatan',
            'NAMA' => 'Nama',
            'NO_IDENTITAS' => 'No  Identitas',
            'INTANSI' => 'Intansi',
            'ALAMAT' => 'Alamat',
            'NO_TELP' => 'No  Telp',
        ];
    }
}
