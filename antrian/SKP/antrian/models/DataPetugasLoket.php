<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_petugas_loket".
 *
 * @property string $ID_DATA_PETUGAS_LOKET
 * @property integer $LOKET
 * @property string $USER_NAME
 * @property string $PASSWORD
 * @property string $STATUS
 * @property string $NAMA_LOKET
 * @property resource $IMAGE
 */
class DataPetugasLoket extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_petugas_loket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_DATA_PETUGAS_LOKET'], 'required'],
            [['LOKET'], 'integer'],
            [['IMAGE'], 'string'],
            [['ID_DATA_PETUGAS_LOKET'], 'string', 'max' => 8],
            [['USER_NAME', 'PASSWORD', 'NAMA_LOKET'], 'string', 'max' => 50],
            [['STATUS'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_DATA_PETUGAS_LOKET' => 'Id  Data  Petugas  Loket',
            'LOKET' => 'Loket',
            'USER_NAME' => 'User  Name',
            'PASSWORD' => 'Password',
            'STATUS' => 'Status',
            'NAMA_LOKET' => 'Nama  Loket',
            'IMAGE' => 'Image',
        ];
    }
}
