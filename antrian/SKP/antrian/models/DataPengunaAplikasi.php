<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_penguna_aplikasi".
 *
 * @property string $ID_PENGUNA_APLIKASI
 * @property string $USER_NAME
 * @property string $PASSWORD
 * @property string $LOKET
 * @property string $KIOSK
 * @property string $PLASMA
 * @property string $KOTAK_SARAN
 * @property string $MANAJEMEN
 * @property string $LAPORAN
 */
class DataPengunaAplikasi extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;
    public $PASSWORD_LAMA;
    public $PASSWORD_BARU;
    public $PASSWORD_REPEAT_BARU;
    
    public static function tableName()
    {
        return 'data_penguna_aplikasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_PENGUNA_APLIKASI'], 'required'],
            [['ID_PENGUNA_APLIKASI'], 'string', 'max' => 8],
            [['USER_NAME', 'PASSWORD'], 'string', 'max' => 50],
            [['LOKET', 'KIOSK', 'PLASMA', 'KOTAK_SARAN', 'MANAJEMEN', 'LAPORAN'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_PENGUNA_APLIKASI' => Yii::t('app', 'Id  Penguna  Aplikasi'),
            'USER_NAME' => Yii::t('app', 'User  Name'),
            'PASSWORD' => Yii::t('app', 'Password'),
            'LOKET' => Yii::t('app', 'Loket'),
            'KIOSK' => Yii::t('app', 'Kiosk'),
            'PLASMA' => Yii::t('app', 'Plasma'),
            'KOTAK_SARAN' => Yii::t('app', 'Kotak  Saran'),
            'MANAJEMEN' => Yii::t('app', 'Manajemen'),
            'LAPORAN' => Yii::t('app', 'Laporan'),
        ];
    }
    

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $user =self::findOne($id);
        if (!count($user)) {
           return null;
        }
        return new static($user);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        //throw new \yii\base\NotSupportedException();
        return self::findOne($token);
        
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {   
        foreach (self::find()->all() as $user) 
        {
            if (strcasecmp($user['USER_NAME'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->ID_PENGUNA_APLIKASI;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->PASSWORD;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->PASSWORD === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {   
        /*$pass_hass = crypt($password,$this->PASSWORD);
        print_r($pass_hass);die;
        if($pass_hass == $this->PASSWORD){
            //return true;
            return $this->PASSWORD === $pass_hass;
        }
            //return false;

        //return $this->PASSWORD === $password;
        */
        
        //$pass_hass = crypt($password,$this->PASSWORD);
        //print_r($pass_hass);die;
        if($password == $this->PASSWORD){
            return $this->PASSWORD === $password;
        }
        
        
    }
}
