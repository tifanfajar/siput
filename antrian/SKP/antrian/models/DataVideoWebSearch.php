<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DataVideoWeb;

/**
 * DataVideoWebSearch represents the model behind the search form of `app\models\DataVideoWeb`.
 */
class DataVideoWebSearch extends DataVideoWeb
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_DATA_VIDEO_WEB', 'STATUS_VIDEO', 'PATH_YOUTUBE', 'PATH_VIDEO', 'STATUS'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DataVideoWeb::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'ID_DATA_VIDEO_WEB', $this->ID_DATA_VIDEO_WEB])
            ->andFilterWhere(['like', 'STATUS_VIDEO', $this->STATUS_VIDEO])
            ->andFilterWhere(['like', 'PATH_YOUTUBE', $this->PATH_YOUTUBE])
            ->andFilterWhere(['like', 'PATH_VIDEO', $this->PATH_VIDEO])
            ->andFilterWhere(['like', 'STATUS', $this->STATUS]);

        return $dataProvider;
    }
}
