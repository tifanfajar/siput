<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_running_text".
 *
 * @property string $ID_DATA_RUNNING
 * @property string $TEXT
 * @property string $STATUS
 */
class DataRunningText extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_running_text';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_DATA_RUNNING'], 'required'],
            [['TEXT'], 'string'],
            [['ID_DATA_RUNNING'], 'string', 'max' => 8],
            [['STATUS'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_DATA_RUNNING' => 'Id  Data  Running',
            'TEXT' => 'Text',
            'STATUS' => 'Status',
        ];
    }
}
