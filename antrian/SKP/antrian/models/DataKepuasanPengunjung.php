<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_kepuasan_pengunjung".
 *
 * @property string $ID_DATA_KEPUASAN_PENGUNJUNG
 * @property integer $LOKET
 * @property string $TANGGAL
 * @property integer $NO_URUT
 * @property string $RATING
 * @property string $PATH_GAMBAR
 */
class DataKepuasanPengunjung extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
    public $Tahun = "";
    public $Bulan = "";
    public $Minggu = "";
    
    public static function tableName()
    {
        return 'data_kepuasan_pengunjung';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_DATA_KEPUASAN_PENGUNJUNG'], 'required'],
            [['LOKET', 'NO_URUT'], 'integer'],
            [['TANGGAL'], 'safe'],
            [['PATH_GAMBAR'], 'string'],
            [['ID_DATA_KEPUASAN_PENGUNJUNG'], 'string', 'max' => 10],
            [['RATING'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_DATA_KEPUASAN_PENGUNJUNG' => 'Id  Data  Kepuasan  Pengunjung',
            'LOKET' => 'Loket',
            'TANGGAL' => 'Tanggal',
            'NO_URUT' => 'No  Urut',
            'RATING' => 'Rating',
            'PATH_GAMBAR' => 'Path  Gambar',
            
            'Tahun' => 'Tahun',
            'Bulan' => 'Bulan',
            'Minggu' => 'Minggu',
        ];
    }
    public static function getnama($tanggal, $loket, $noantrian)
    {
        $tgl1 = $tanggal;
        $tgl2 = date('Y-m-d', strtotime('+1 days', strtotime($tgl1)));
        
        $query = "TANGGAL BETWEEN '".$tgl1."' AND '".$tgl2."' AND LOKET = '".$loket."' AND NO_URUT = '".$noantrian."'";
        
        $detail = \app\models\DataAntrian::find()
                ->select('NAMA')
                ->where($query)
                ->one();
        
        if(isset($detail))
        {return $detail->NAMA;}
        else
        {return "-";}
    }
    public static function getJenisKelamin($tanggal, $loket, $noantrian)
    {
        $tgl1 = $tanggal;
        $tgl2 = date('Y-m-d', strtotime('+1 days', strtotime($tgl1)));
        
        $query = "TANGGAL BETWEEN '".$tgl1."' AND '".$tgl2."' AND LOKET = '".$loket."' AND NO_URUT = '".$noantrian."'";
        
        $detail = \app\models\DataAntrian::find()
                ->select('JENIS_KELAMIN')
                ->where($query)
                ->one();
        
        if(isset($detail))
        {return $detail->JENIS_KELAMIN;}
        else
        {return "-";}
    }
    public static function getInstansi($tanggal, $loket, $noantrian)
    {
        $tgl1 = $tanggal;
        $tgl2 = date('Y-m-d', strtotime('+1 days', strtotime($tgl1)));
        
        $query = "TANGGAL BETWEEN '".$tgl1."' AND '".$tgl2."' AND LOKET = '".$loket."' AND NO_URUT = '".$noantrian."'";
        
        $detail = \app\models\DataAntrian::find()
                ->select('INTANSI')
                ->where($query)
                ->one();
        
        if(isset($detail))
        {return $detail->INTANSI;}
        else
        {return "-";}
    }
    public static function getWaktuProses($tanggal, $loket, $noantrian)
    {
        $tgl1 = $tanggal;
        $tgl2 = date('Y-m-d', strtotime('+1 days', strtotime($tgl1)));
        
        $query = "TANGGAL BETWEEN '".$tgl1."' AND '".$tgl2."' AND LOKET = '".$loket."' AND NO_URUT = '".$noantrian."'";
        
        $detail = \app\models\DataAntrian::find()
                ->select('WAKTU_PROSES_MENIT, WAKTU_PROSES_DETIK')
                ->where($query)
                ->one();
        
        if(isset($detail))
        {return $detail->WAKTU_PROSES_MENIT.":".$detail->WAKTU_PROSES_DETIK;}
        else
        {return "-";}
    }
}
