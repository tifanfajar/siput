<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_slideshow".
 *
 * @property string $ID_DATA_SLIDESHOW
 * @property string $IMAGE1
 * @property string $IMAGE2
 * @property string $IMAGE3
 * @property string $STATUS
 */
class DataSlideshow extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_slideshow';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_DATA_SLIDESHOW'], 'required'],
            [['IMAGE1', 'IMAGE2', 'IMAGE3'], 'string'],
            [['ID_DATA_SLIDESHOW'], 'string', 'max' => 8],
            [['STATUS'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_DATA_SLIDESHOW' => 'Id  Data  Slideshow',
            'IMAGE1' => 'Image 1',
            'IMAGE2' => 'Image 2',
            'IMAGE3' => 'Image 3',
            'STATUS' => 'Status',
        ];
    }
}
