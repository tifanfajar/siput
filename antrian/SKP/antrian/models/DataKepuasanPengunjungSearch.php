<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DataKepuasanPengunjung;

/**
 * DataKepuasanPengunjungSearch represents the model behind the search form of `app\models\DataKepuasanPengunjung`.
 */
class DataKepuasanPengunjungSearch extends DataKepuasanPengunjung
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_DATA_KEPUASAN_PENGUNJUNG', 'TANGGAL', 'RATING', 'PATH_GAMBAR'], 'safe'],
            [['LOKET', 'NO_URUT'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DataKepuasanPengunjung::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'LOKET' => $this->LOKET,
            'TANGGAL' => $this->TANGGAL,
            'NO_URUT' => $this->NO_URUT,
        ]);

        $query->andFilterWhere(['like', 'ID_DATA_KEPUASAN_PENGUNJUNG', $this->ID_DATA_KEPUASAN_PENGUNJUNG])
            ->andFilterWhere(['like', 'RATING', $this->RATING])
            ->andFilterWhere(['like', 'PATH_GAMBAR', $this->PATH_GAMBAR]);

        return $dataProvider;
    }
    public function searchTahun($params, $qwry)
    {
        $query = DataKepuasanPengunjung::find()
                ->where($qwry);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize'=>100000000]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'LOKET' => $this->LOKET,
            'TANGGAL' => $this->TANGGAL,
            'NO_URUT' => $this->NO_URUT,
        ]);

        $query->andFilterWhere(['like', 'ID_DATA_KEPUASAN_PENGUNJUNG', $this->ID_DATA_KEPUASAN_PENGUNJUNG])
            ->andFilterWhere(['like', 'RATING', $this->RATING])
            ->andFilterWhere(['like', 'PATH_GAMBAR', $this->PATH_GAMBAR]);

        return $dataProvider;
    }
}
