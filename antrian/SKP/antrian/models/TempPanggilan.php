<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "temp_panggilan".
 *
 * @property string $ID_TEMP_PANGGILAN
 * @property string $PANGILAN
 * @property string $STATUS
 */
class TempPanggilan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'temp_panggilan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_TEMP_PANGGILAN'], 'required'],
            [['ID_TEMP_PANGGILAN'], 'string', 'max' => 12],
            [['PANGILAN'], 'string', 'max' => 50],
            [['STATUS'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID_TEMP_PANGGILAN' => 'Id  Temp  Panggilan',
            'PANGILAN' => 'Pangilan',
            'STATUS' => 'Status',
        ];
    }
}
