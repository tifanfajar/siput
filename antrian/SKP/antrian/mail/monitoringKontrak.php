<?php
use app\models\TblKontrak;
?>

<table width="100%" border="0">
  <tr>
    <td><div align="center">Monitoring Kontrak </div></td>
  </tr>
  <tr>
    <td>Berikut kami tampilkan data kontrak yang akan berakhir dan yang telah berakhir sebelum tanggal <?= $tgl ?> : </td>
  </tr>
  <tr>
    <td><table width="100%" border="1">
      <tr>
        <td><div align="center"><strong>No. Kontrak PLN </strong></div></td>
        <td><div align="center"><strong>Nama Customer </strong></div></td>
        <td><div align="center"><strong>Tanggal Kontrak </strong></div></td>
        <td><div align="center"><strong>Uraian Pekerjaan </strong></div></td>
        <td><div align="center"><strong>Tanggal Mulai </strong></div></td>
        <td><div align="center"><strong>Tanggal Akhir </strong></div></td>
        <td><div align="center"><strong>Selisih (Hari) </strong></div></td>
        <td><div align="center"><strong>Kont./Bln(PPN)</strong></div></td>
      </tr>
      <?php         
        $las_po = TblKontrak::find()
            ->select('nokon_pln, nama_cust_up, tgl_kon, uraian_pek, tgl_start, nikon_perblnppn')
            ->where($whereequal)
            ->all();
        
        foreach ($las_po as $value):                
      ?>      
             <tr>
                <td><?= $value->nokon_pln; ?></td>
                <td><?= $value->nama_cust_up; ?></td>
                <td><?= $value->tgl_kon; ?></td>
                <td><?= $value->uraian_pek; ?></td>
                <td><?= $value->tgl_start; ?></td>
                <td><?= $model->getTanggalAkhir($value->nokon_pln, $value->tgl_start); ?></td>
                <td><?= $model->HitungHari($value->nokon_pln, $value->tgl_start); ?></td>
                <td><?= number_format($value->nikon_perblnppn,0,",","."); ?></td>
              </tr>            
      <?php  
        endforeach;
      ?>
    </table></td>
  </tr>
</table>