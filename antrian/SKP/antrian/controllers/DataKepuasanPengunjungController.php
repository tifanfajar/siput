<?php

namespace app\controllers;

use Yii;
use app\models\DataKepuasanPengunjung;
use app\models\DataKepuasanPengunjungSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DataKepuasanPengunjungController implements the CRUD actions for DataKepuasanPengunjung model.
 */
class DataKepuasanPengunjungController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DataKepuasanPengunjung models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DataKepuasanPengunjungSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DataKepuasanPengunjung model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DataKepuasanPengunjung model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DataKepuasanPengunjung();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID_DATA_KEPUASAN_PENGUNJUNG]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DataKepuasanPengunjung model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID_DATA_KEPUASAN_PENGUNJUNG]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DataKepuasanPengunjung model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DataKepuasanPengunjung model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return DataKepuasanPengunjung the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DataKepuasanPengunjung::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionKepuasanpengunjung($loket)
    {
        return $this->render('formKepuasan', [
            'loket' => $loket,
            //'model' => $this->findModel($id),
        ]);
    }
    public function actionStatistikloket($loket)
    {
        $model = new DataKepuasanPengunjung();
        
        return $this->render('statistikLoket', [
            'loket' => $loket,
            'model' => $model,
        ]);
    }
    public function actionIndextabtahun($query)
    {
        $searchModel = new DataKepuasanPengunjungSearch();
        $dataProvider = $searchModel->searchTahun(Yii::$app->request->queryParams, $query);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionBtnklik($loket)
    {
        //print_r($loket);die;
        $model = new DataKepuasanPengunjung();

        if(isset($_POST['btnGrafTahun']))
        {
            if(isset($_POST['DataKepuasanPengunjung']))
            {   
                $query = "";
                $a = $_POST['DataKepuasanPengunjung'];
                $tahun = $a['Tahun'];
                if(strlen($tahun)>=2)
                {$query = "YEAR(TANGGAL)='".$tahun."' AND LOKET = '".$loket."'";}
                else
                {$query = "YEAR(TANGGAL)='2017' AND LOKET = '".$loket."'";}
                return $this->redirect(['indextabtahun', 'query' => $query]);
            }
        }
        elseif(isset($_POST['btnGrafBulan']))
        {
            if(isset($_POST['DataKepuasanPengunjung']))
            {   
                $query = "";
                $a = $_POST['DataKepuasanPengunjung'];
                $tahun = $a['Tahun'];
                $bulan = $a['Bulan'];
                if(strlen($tahun)>=2 and strlen($bulan)>=1)
                {$query = "month(TANGGAL)='".$bulan."' and YEAR(TANGGAL)='".$tahun."' AND LOKET = '".$loket."'";}
                else
                {$query = "month(TANGGAL)='1' and YEAR(TANGGAL)='2017' AND LOKET = '".$loket."'";}
                return $this->redirect(['indextabtahun', 'query' => $query]);
            }
        }
        elseif(isset($_POST['btnGrafMinggu']))
        {
            if(isset($_POST['DataKepuasanPengunjung']))
            {   
                $query = "";
                $a = $_POST['DataKepuasanPengunjung'];
                $tahun = $a['Tahun'];
                $minggu = $a['Minggu'];
                if(strlen($tahun)>=2 and strlen($minggu)>=1)
                {$query = "WEEKOFYEAR(TANGGAL)='".$minggu."' and YEAR(TANGGAL)='".$tahun."' AND LOKET = '".$loket."'";}
                else
                {$query = "month(TANGGAL)='1' and YEAR(TANGGAL)='2017' AND LOKET = '".$loket."'";}
                return $this->redirect(['indextabtahun', 'query' => $query]);
            }
        }
        /*elseif(isset($_POST['btnMonTagSPK2']))
        {
            if(isset($_POST['DataPenagihan']))
            {   
                $query = "";
                $a = $_POST['DataPenagihan'];
                $unitbtnMonTagSPKblm = $a['custbtnMonTagSPK'];
                //print_r($unitbtnMonTagSPKblm);die;
                if(strlen($unitbtnMonTagSPKblm)>=2)
                {
                    $query = "KODE_CUSTOMER = '".$unitbtnMonTagSPKblm."' and STATUS = 'Aktif'";                    
                }
                return $this->render('cetakMonTagSPK2', [
                    'query' => $query,
                    'model' => $model,
                ]);
            }
        }
        //proyeksi lunas
        elseif(isset($_POST['btnProyeksiLunas']))
        {
            if(isset($_POST['DataPenagihan']))
            {   
                $query = "";
                $a = $_POST['DataPenagihan'];
                $unitbtnProyeksiLunas = $a['unitbtnProyeksiLunas'];
                //print_r($unitbtnMonTagSPKblm);die;
                if(strlen($unitbtnProyeksiLunas)>=2)
                {
                    $query = "ID_UNIT = '".$unitbtnProyeksiLunas."' and STATUS = 'Aktif'";                    
                }
                return $this->render('cetakMonRekapPelunasan', [
                    'query' => $query,
                    'model' => $model,
                ]);
            }
        }
        elseif(isset($_POST['btnProyeksiLunas2']))
        {
            if(isset($_POST['DataPenagihan']))
            {   
                $query = "";
                $a = $_POST['DataPenagihan'];
                $unitbtnProyeksiLunas = $a['custbtnProyeksiLunas'];
                //print_r($unitbtnMonTagSPKblm);die;
                if(strlen($unitbtnProyeksiLunas)>=2)
                {
                    $query = "KODE_CUSTOMER = '".$unitbtnProyeksiLunas."' and STATUS = 'Aktif'";                    
                }
                return $this->render('cetakMonRekapPelunasan', [
                    'query' => $query,
                    'model' => $model,
                ]);
            }
        }
        //Kode masalah
        elseif(isset($_POST['btnKodeMasalah']))
        {
            if(isset($_POST['DataPenagihan']))
            {   
                $query = "";
                $a = $_POST['DataPenagihan'];
                $unitbtnProyeksiLunas = $a['unitbtnKodeMasalah'];
                //print_r($unitbtnMonTagSPKblm);die;
                if(strlen($unitbtnProyeksiLunas)>=2)
                {
                    $query = "ID_UNIT = '".$unitbtnProyeksiLunas."' and STATUS = 'Aktif'";                    
                }
                return $this->render('cetakKodeMalasahSPK', [
                    'query' => $query,
                    'model' => $model,
                ]);
            }
        }
        elseif(isset($_POST['btnKodeMasalah2']))
        {
            if(isset($_POST['DataPenagihan']))
            {   
                $query = "";
                $a = $_POST['DataPenagihan'];
                $unitbtnProyeksiLunas = $a['custbtnKodeMasalah'];
                //print_r($unitbtnMonTagSPKblm);die;
                if(strlen($unitbtnProyeksiLunas)>=2)
                {
                    $query = "KODE_CUSTOMER = '".$unitbtnProyeksiLunas."' and STATUS = 'Aktif'";                    
                }
                return $this->render('cetakKodeMalasahSPK', [
                    'query' => $query,
                    'model' => $model,
                ]);
            }
        }
        //tagihan dummy
        elseif(isset($_POST['btnMonTagDummySPK']))
        {
            if(isset($_POST['DataPenagihan']))
            {   
                $query = "";
                $a = $_POST['DataPenagihan'];
                $unitbtnProyeksiLunas = $a['unitbtnMonTagDummySPK'];
                //print_r($unitbtnMonTagSPKblm);die;
                if(strlen($unitbtnProyeksiLunas)>=2)
                {
                    $query = "ID_UNIT = '".$unitbtnProyeksiLunas."' and STATUS = 'Aktif'";                    
                }
                return $this->render('cetakMonTagDummySPK', [
                    'query' => $query,
                    'model' => $model,
                ]);
            }
        }
        elseif(isset($_POST['btnMonTagDummySPK2']))
        {
            if(isset($_POST['DataPenagihan']))
            {   
                $query = "";
                $a = $_POST['DataPenagihan'];
                $unitbtnProyeksiLunas = $a['custbtnMonTagDummySPK'];
                //print_r($unitbtnMonTagSPKblm);die;
                if(strlen($unitbtnProyeksiLunas)>=2)
                {
                    $query = "KODE_CUSTOMER = '".$unitbtnProyeksiLunas."' and STATUS = 'Aktif'";                    
                }
                return $this->render('cetakMonTagDummySPK', [
                    'query' => $query,
                    'model' => $model,
                ]);
            }
        }*/
    }
}
