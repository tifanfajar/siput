<?php

namespace app\controllers;

use Yii;
use app\models\DataSlideshow;
use app\models\DataSlideshowSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * DataSlideshowController implements the CRUD actions for DataSlideshow model.
 */
class DataSlideshowController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DataSlideshow models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DataSlideshowSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DataSlideshow model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DataSlideshow model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DataSlideshow();

        if ($model->load(Yii::$app->request->post())) 
        {
            yii::$app->db->createCommand()->update('data_slideshow',[
                'STATUS'=>'0',
                ],'ID_DATA_SLIDESHOW like "%0%"')
            ->execute();
            
            $id = $this->getnoid();
            
            $model->IMAGE1 = UploadedFile::getInstance($model, 'IMAGE1');
            $filename1 = 'images/slideshow/none.jpg';
            if(!empty($model->IMAGE1))
            {
                $filename1 = 'images/slideshow/'.$id.$model->IMAGE1->name;
                $model->IMAGE1->saveAs('../images/slideshow/' .$id. $model->IMAGE1->baseName . '.' . $model->IMAGE1->extension);
            }
            
            $model->IMAGE2 = UploadedFile::getInstance($model, 'IMAGE2');
            $filename2 = 'images/slideshow/none.jpg';
            if(!empty($model->IMAGE2))
            {
                $filename2 = 'images/slideshow/'.$id.$model->IMAGE2->name;
                $model->IMAGE2->saveAs('../images/slideshow/' .$id. $model->IMAGE2->baseName . '.' . $model->IMAGE2->extension);
            }
            
            $model->IMAGE3 = UploadedFile::getInstance($model, 'IMAGE3');
            $filename3 = 'images/slideshow/none.jpg';
            if(!empty($model->IMAGE3))
            {
                $filename3 = 'images/slideshow/'.$id.$model->IMAGE3->name;
                $model->IMAGE3->saveAs('../images/slideshow/' .$id. $model->IMAGE3->baseName . '.' . $model->IMAGE3->extension);
            }
                
            yii::$app->db->createCommand()->insert('data_slideshow',[
                'ID_DATA_SLIDESHOW'=>$id,
                'IMAGE1'=>$filename1,
                'IMAGE2'=>$filename2,
                'IMAGE3'=>$filename3,
                'STATUS'=>$model->STATUS,
            ])->execute();
            
            //$model->save();
            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DataSlideshow model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID_DATA_SLIDESHOW]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DataSlideshow model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DataSlideshow model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return DataSlideshow the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DataSlideshow::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function getnoid()
    {
        $dp = date('ym');
        $no = '0001';

        $las_po = DataSlideshow::find()->select('ID_DATA_SLIDESHOW')->andWhere(['like','ID_DATA_SLIDESHOW',$dp])->orderBy(['ID_DATA_SLIDESHOW'=> SORT_DESC])->all();
        
        if($las_po != NULL)
        {
            $a = substr($las_po[0]['ID_DATA_SLIDESHOW'], 4);
            $a++;
            $a = substr('0000',strlen($a)).$a;
            
            return $dp.$a;
        }
        else
        {
            return $dp.$no;
        }
    }
}
