<?php

namespace app\controllers;

use Yii;
use app\models\DataVideoWeb;
use app\models\DataVideoWebSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * DataVideoWebController implements the CRUD actions for DataVideoWeb model.
 */
class DataVideoWebController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DataVideoWeb models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DataVideoWebSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DataVideoWeb model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DataVideoWeb model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DataVideoWeb();

        if ($model->load(Yii::$app->request->post())) 
        {
            yii::$app->db->createCommand()->update('data_video_web',[
                'STATUS'=>'0',
                ],'ID_DATA_VIDEO_WEB like "%0%"')
            ->execute();
            
            $id = $this->getnoid();
            if($model->STATUS_VIDEO == '0')
            {
                yii::$app->db->createCommand()->insert('data_video_web',[
                    'ID_DATA_VIDEO_WEB'=>$id,
                    'STATUS_VIDEO'=>$model->STATUS_VIDEO,
                    'PATH_YOUTUBE'=>$model->PATH_YOUTUBE,
                    'PATH_VIDEO'=>'-',
                    'STATUS'=>$model->STATUS,
                ])->execute();
            }
            else
            {
                //print_r($model->PATH_VIDEO);die;
                $model->PATH_VIDEO = UploadedFile::getInstance($model, 'PATH_VIDEO');
                $filename = 'images/video/none.mp4';
                if(!empty($model->PATH_VIDEO))
                {
                    $filename = 'images/video/'.$id.$model->PATH_VIDEO->name;
                    $model->PATH_VIDEO->saveAs('../images/video/' .$id. $model->PATH_VIDEO->baseName . '.' . $model->PATH_VIDEO->extension);
                }
                
                yii::$app->db->createCommand()->insert('data_video_web',[
                    'ID_DATA_VIDEO_WEB'=>$id,
                    'STATUS_VIDEO'=>$model->STATUS_VIDEO,
                    'PATH_YOUTUBE'=>'-',
                    'PATH_VIDEO'=>$filename,
                    'STATUS'=>$model->STATUS,
                ])->execute();
                
            }

            return $this->redirect(['view', 'id' => $id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing DataVideoWeb model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID_DATA_VIDEO_WEB]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing DataVideoWeb model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DataVideoWeb model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return DataVideoWeb the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DataVideoWeb::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function getnoid()
    {
        $dp = date('ym');
        $no = '0001';

        $las_po = DataVideoWeb::find()->select('ID_DATA_VIDEO_WEB')->andWhere(['like','ID_DATA_VIDEO_WEB',$dp])->orderBy(['ID_DATA_VIDEO_WEB'=> SORT_DESC])->all();
        
        if($las_po != NULL)
        {
            $a = substr($las_po[0]['ID_DATA_VIDEO_WEB'], 4);
            $a++;
            $a = substr('0000',strlen($a)).$a;
            
            return $dp.$a;
        }
        else
        {
            return $dp.$no;
        }
    }
}
