<?php

/* @var $this yii\web\View */


use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="jumbotron">
        <h3>Dashboard Monitoring Kontrak dan Tagihan</h3>        
    </div>
    
    
    <div class="body-content">
        <!-- row pertama -->
        <div class="row dashboard">
            <div class="col-lg-6">
                <?=
                Highcharts::widget([
                    'setupOptions' => [
                        'global' => ['useUTC' => FALSE],
                    ],

                    'options' => [
                        'chart' =>[
                            'type' => 'pie',
                            'options3d' => [
                                'enabled'=>true,
                                'alpha'=>45,
                                //'beta'=>0,
                                //'depth'=>70,
                                //'viewDistance'=>25,
                            ],
                        ],
                        //'colors'=>["#146626", "#ffa022", "#009b88", "#f04242", "#0073dd", "#e74c6c", "#561e6d", "#f04242", "#7f4b05", "#325eff", "#dd002e"],
                        'title' => ['text' => 'Data Tagihan Per Kode Masalah'],
                        'subtitle'=>[
                            'text'=>'Total Tagihan : Rp. '.number_format($totTagihan,0,",","."),
                        ],
                        'tooltip'=>[
                            'pointFormat'=>'{series.name}: <b>{point.percentage:.1f}%</b>'
                        ],
                        'plotOptions'=> [
                            'pie'=>[
                                //'allowPointSelect' =>true,
                                //'cursor'=>'pointer',
                                'innerSize'=>100,
                                'depth'=>45,
                                //'dataLabels'=>[
                                    //'enabled'=>false,
                                    //'format'=>'{point.name}',
                                //],
                                //'showInLegend'=>true,
                            ]
                        ],
                        'series' => [
                            [
                                'name' => 'Tagihan',
                                'colorByPoint'=>true, 
                                'data' => [
                                    [
                                        'name'=>'A.01 : '.number_format($modelA01,0,",",".").' ('.$percentA01.')',
                                        'y'=>$descA01,
                                    ],
                                    [
                                        'name'=>'A.02 : '.number_format($modelA02,0,",",".").' ('.$percentA02.')',
                                        'y'=>$descA02,
                                    ],
                                    [
                                        'name'=>'A.03 : '.number_format($modelA03,0,",",".").' ('.$percentA03.')',
                                        'y'=>$descA03,
                                    ],
                                    [
                                        'name'=>'A.04 : '.number_format($modelA04,0,",",".").' ('.$percentA04.')',
                                        'y'=>$descA04,
                                    ],
                                    [
                                        'name'=>'A.05 : '.number_format($modelA05,0,",",".").' ('.$percentA05.')',
                                        'y'=>$descA05,
                                    ],
                                    [
                                        'name'=>'A.06 : '.number_format($modelA06,0,",",".").' ('.$percentA06.')',
                                        'y'=>$descA06,
                                    ],
                                    [
                                        'name'=>'B.01 : '.number_format($modelB01,0,",",".").' ('.$percentB01.')',
                                        'y'=>$descB01,
                                    ],
                                    [
                                        'name'=>'B.02 : '.number_format($modelB02,0,",",".").' ('.$percentB02.')',
                                        'y'=>$descB02,
                                    ],
                                    [
                                        'name'=>'B.03 : '.number_format($modelB03,0,",",".").' ('.$percentB03.')',
                                        'y'=>$descB03,
                                        //'sliced'=>true,
                                        //'selected'=>true
                                    ],
                                    [
                                        'name'=>'B.04 : '.number_format($modelB04,0,",",".").' ('.$percentB04.')',
                                        'y'=>$descB04,
                                    ],
                                    [
                                        'name'=>'Belum : '.number_format($modelBelum,0,",",".").' ('.$percentBelum.')',
                                        'y'=>$descBelum,
                                    ],
                                ]
                            ],
                        ]
                    ]
                ]);
                ?>
            </div>
            <div class="col-lg-6">
                <?=
                    Highcharts::widget([
                        'scripts' => [
                            'modules/exporting',
                            'themes/grid-light',
                        ],
                        'options' => [
                            'title' => [
                                'text' => 'Data Tagihan Per Kode Masalah',
                            ],
                            'xAxis' => [
                                'categories' => ["Tagihan"],
                            ],

                            'series' => [
                                [
                                    'type' => 'column',
                                    'name' => 'A.01',
                                    'data' => [floatval($modelA01)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'A.02',
                                    'data' => [floatval($modelA02)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'A.03',
                                    'data' => [floatval($modelA03)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'A.04',
                                    'data' => [floatval($modelA04)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'A.05',
                                    'data' => [floatval($modelA05)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'A.06',
                                    'data' => [floatval($modelA06)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'B.01',
                                    'data' => [floatval($modelB01)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'B.02',
                                    'data' => [floatval($modelB02)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'B.03',
                                    'data' => [floatval($modelB03)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'B.04',
                                    'data' => [floatval($modelB04)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'Belum',
                                    'data' => [floatval($modelBelum)],
                                ],
                            ],
                        ]
                    ]); 
                ?>
            </div>
        </div>
        <div class="row dashboard">
            &nbsp;
            &nbsp;
        </div>
        <div class="row dashboard">
            <div class="col-lg-6">
                <?php $jmlbastp = $jmlSum0130BASTP + $jmlSum3160BASTP + $jmlSum6190BASTP + $jmlSum90BASTP + $jmlSumBelumBASTP; ?>
                <?= 
                Highcharts::widget([
                    'scripts' => [
                        'modules/exporting',
                        'themes/grid-light',
                    ],
                    'options' => [
                        'title' => [
                            'text' => 'Data Umur Tagihan BASTP',
                        ],
                        'subtitle'=>[
                            'text'=>'Total Tagihan : Rp. '.number_format($jmlbastp,0,",","."),
                        ],
                        'xAxis' => [
                            'categories' => ['Tagihan'],
                        ],
                        'labels' => [
                            'items' => [
                                [
                                    //'html' => 'Total Umur Tagihan BASTP',
                                    'style' => [
                                        'left' => '50px',
                                        'top' => '18px',
                                        'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                                    ],
                                ],
                            ],
                        ],
                        'series' => [
                            [
                                'type' => 'column',
                                'name' => 'Belum ditagih',
                                'data' => [floatval($jmlSumBelumBASTP)],
                            ],
                            [
                                'type' => 'column',
                                'name' => '01-30 hari',
                                'data' => [floatval($jmlSum0130BASTP)],
                            ],
                            [
                                'type' => 'column',
                                'name' => '31-60 hari',
                                'data' => [floatval($jmlSum3160BASTP)],
                            ],
                            [
                                'type' => 'column',
                                'name' => '61-90 hari',
                                'data' => [floatval($jmlSum6190BASTP)],
                            ],
                            [
                                'type' => 'column',
                                'name' => 'Diatas 90 hari',
                                'data' => [floatval($jmlSum90BASTP)],
                            ],
                            [
                                'type' => 'pie',
                                'name' => 'Total',
                                'data' => [
                                    [
                                        'name' => 'Belum ditagih'." (".Yii::$app->formatter->asPercent(($jmlSumBelumBASTP/$jmlbastp),2).")",
                                        'y' => floatval($jmlSumBelumBASTP),
                                        'color' => new JsExpression('Highcharts.getOptions().colors[0]'), // Jane's color
                                    ],
                                    [
                                        'name' => '01-30 hari'." (".Yii::$app->formatter->asPercent(($jmlSumBelumBASTP/$jmlbastp),2).")",
                                        'y' => floatval($jmlSum0130BASTP),
                                        'color' => new JsExpression('Highcharts.getOptions().colors[1]'), // John's color
                                    ],
                                    [
                                        'name' => '31-60 hari'." (".Yii::$app->formatter->asPercent(($jmlSumBelumBASTP/$jmlbastp),2).")",
                                        'y' => floatval($jmlSum3160BASTP),
                                        'color' => new JsExpression('Highcharts.getOptions().colors[2]'), // Joe's color
                                    ],
                                    [
                                        'name' => '61-90 hari'." (".Yii::$app->formatter->asPercent(($jmlSumBelumBASTP/$jmlbastp),2).")",
                                        'y' => floatval($jmlSum6190BASTP),
                                        'color' => new JsExpression('Highcharts.getOptions().colors[3]'), // Joe's color
                                    ],
                                    [
                                        'name' => 'Diatas 90 hari'." (".Yii::$app->formatter->asPercent(($jmlSumBelumBASTP/$jmlbastp),2).")",
                                        'y' => floatval($jmlSum90BASTP),
                                        'color' => new JsExpression('Highcharts.getOptions().colors[4]'), // Joe's color
                                    ],
                                ],
                                'center' => [100, 80],
                                'size' => 100,
                                'showInLegend' => false,
                                'dataLabels' => [
                                    'enabled' => false,
                                ],
                            ],
                        ],
                    ]
                ]);
                ?>
            </div>
            <div class="col-lg-6">
                <?php $jmlkeu = $jmlSum0130Keu + $jmlSum3160Keu + $jmlSum6190Keu + $jmlSum90Keu + $jmlSumBelumKeu; ?>
                <?= 
                Highcharts::widget([
                    'scripts' => [
                        'modules/exporting',
                        'themes/grid-light',
                    ],
                    'options' => [
                        'title' => [
                            'text' => 'Data Umur Tagihan Keuangan',
                        ],
                        'subtitle'=>[
                            'text'=>'Total Tagihan : Rp. '.number_format($jmlkeu,0,",","."),
                        ],
                        'xAxis' => [
                            'categories' => ['Tagihan'],
                        ],
                        'labels' => [
                            'items' => [
                                [
                                    //'html' => 'Total Umur Tagihan Keuangan',
                                    'style' => [
                                        'left' => '50px',
                                        'top' => '18px',
                                        'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                                    ],
                                ],
                            ],
                        ],
                        'series' => [
                            [
                                'type' => 'column',
                                'name' => 'Belum di keuangan',
                                'data' => [floatval($jmlSumBelumKeu)],
                            ],
                            [
                                'type' => 'column',
                                'name' => '01-30 hari',
                                'data' => [floatval($jmlSum0130Keu)],
                            ],
                            [
                                'type' => 'column',
                                'name' => '31-60 hari',
                                'data' => [floatval($jmlSum3160Keu)],
                            ],
                            [
                                'type' => 'column',
                                'name' => '61-90 hari',
                                'data' => [floatval($jmlSum6190Keu)],
                            ],
                            [
                                'type' => 'column',
                                'name' => 'Diatas 90 hari',
                                'data' => [floatval($jmlSum90Keu)],
                            ],
                            [
                                'type' => 'pie',
                                'name' => 'Total',
                                'data' => [
                                    [
                                        'name' => 'Belum di keuangan'." (".Yii::$app->formatter->asPercent(($jmlSumBelumKeu/$jmlkeu),2).")",
                                        'y' => floatval($jmlSumBelumKeu),
                                        'color' => new JsExpression('Highcharts.getOptions().colors[0]'), // Jane's color
                                    ],
                                    [
                                        'name' => '01-30 hari'." (".Yii::$app->formatter->asPercent(($jmlSum0130Keu/$jmlkeu),2).")",
                                        'y' => floatval($jmlSum0130Keu),
                                        'color' => new JsExpression('Highcharts.getOptions().colors[1]'), // John's color
                                    ],
                                    [
                                        'name' => '31-60 hari'." (".Yii::$app->formatter->asPercent(($jmlSum3160Keu/$jmlkeu),2).")",
                                        'y' => floatval($jmlSum3160Keu),
                                        'color' => new JsExpression('Highcharts.getOptions().colors[2]'), // Joe's color
                                    ],
                                    [
                                        'name' => '61-90 hari'." (".Yii::$app->formatter->asPercent(($jmlSum6190Keu/$jmlkeu),2).")",
                                        'y' => floatval($jmlSum6190Keu),
                                        'color' => new JsExpression('Highcharts.getOptions().colors[3]'), // Joe's color
                                    ],
                                    [
                                        'name' => 'Diatas 90 hari'." (".Yii::$app->formatter->asPercent(($jmlSum90Keu/$jmlkeu),2).")",
                                        'y' => floatval($jmlSum90Keu),
                                        'color' => new JsExpression('Highcharts.getOptions().colors[4]'), // Joe's color
                                    ],
                                ],
                                'center' => [100, 80],
                                'size' => 100,
                                'showInLegend' => false,
                                'dataLabels' => [
                                    'enabled' => false,
                                ],
                            ],
                        ],
                    ]
                ]);
                ?>
            </div>
        </div>
        <div class="row dashboard">
            &nbsp;
            &nbsp;
        </div>
        <div class="row dashboard">
            <div class="col-lg-6">
                <?php $jmlPelunasan = $jmlSumBulan01 + $jmlSumBulan02 + $jmlSumBulan03 + $jmlSumBulan04 + $jmlSumBulan05 + $jmlSumBulan06 + $jmlSumBulan07 + $jmlSumBulan08 + $jmlSumBulan09 + $jmlSumBulan10 + $jmlSumBulan11 + $jmlSumBulan12; ?>
                <?=
                    Highcharts::widget([
                        'scripts' => [
                            'modules/exporting',
                            'themes/grid-light',
                        ],
                        'options' => [
                            'title' => [
                                'text' => 'Data Pelunasan Per Bulan Tahun '.date("Y"),
                            ],
                            'subtitle'=>[
                                'text'=>'Total Pelunasan : Rp. '.number_format($jmlPelunasan,0,",","."),
                            ],
                            'xAxis' => [
                                'categories' => ["Bulan"],
                            ],

                            'series' => [
                                [
                                    'type' => 'column',
                                    'name' => 'Januari',
                                    'data' => [floatval($jmlSumBulan01)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'Februari',
                                    'data' => [floatval($jmlSumBulan02)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'Maret',
                                    'data' => [floatval($jmlSumBulan03)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'April',
                                    'data' => [floatval($jmlSumBulan04)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'Mei',
                                    'data' => [floatval($jmlSumBulan05)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'Juni',
                                    'data' => [floatval($jmlSumBulan06)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'Juli',
                                    'data' => [floatval($jmlSumBulan07)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'Agustus',
                                    'data' => [floatval($jmlSumBulan08)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'September',
                                    'data' => [floatval($jmlSumBulan09)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'Oktober',
                                    'data' => [floatval($jmlSumBulan10)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'November',
                                    'data' => [floatval($jmlSumBulan11)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => 'Desember',
                                    'data' => [floatval($jmlSumBulan12)],
                                ],
                            ],
                        ]
                    ]); 
                ?>
            </div>
            <div class="col-lg-6">
                <?php $jmlTarget = $NBlm + $NNMin2 + $NNMin1 + $NN + $NNPlus1 + $NNPlus2 + $NNPlus3 + $NNPlus4 + $NNPlus5 + $NNPlus6 + $NNPlus7 + $NNPlus8 + $NNPlus9 + $NNPlus10; ?>
                <?=
                    Highcharts::widget([
                        'scripts' => [
                            'modules/exporting',
                            'themes/grid-light',
                        ],
                        'options' => [
                            'title' => [
                                'text' => 'Data Target Pelunasan Tagihan Tahun '.date("Y"),
                            ],
                            'subtitle'=>[
                                'text'=>'Total DPP : Rp. '.number_format($jmlTarget,0,",","."),
                            ],
                            'xAxis' => [
                                'categories' => ["Minggu"],
                            ],

                            'series' => [
                                [
                                    'type' => 'column',
                                    'name' => 'Belum',
                                    'data' => [floatval($NBlm)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => strlen($MguNMin2) == 1 ? '0'.$MguNMin2 : $MguNMin2,
                                    'data' => [floatval($NNMin2)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => strlen($MguNMin1) == 1 ? '0'.$MguNMin1 : $MguNMin1,
                                    'data' => [floatval($NNMin1)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => strlen($MguN) == 1 ? '0'.$MguN : $MguN,
                                    'data' => [floatval($NN)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => strlen($MguNPlus1) == 1 ? '0'.$MguNPlus1 : $MguNPlus1,
                                    'data' => [floatval($NNPlus1)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => strlen($MguNPlus2) == 1 ? '0'.$MguNPlus2 : $MguNPlus2,
                                    'data' => [floatval($NNPlus2)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => strlen($MguNPlus3) == 1 ? '0'.$MguNPlus3 : $MguNPlus3,
                                    'data' => [floatval($NNPlus3)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => strlen($MguNPlus4) == 1 ? '0'.$MguNPlus4 : $MguNPlus4,
                                    'data' => [floatval($NNPlus4)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => strlen($MguNPlus5) == 1 ? '0'.$MguNPlus5 : $MguNPlus5,
                                    'data' => [floatval($NNPlus5)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => strlen($MguNPlus6) == 1 ? '0'.$MguNPlus6 : $MguNPlus6,
                                    'data' => [floatval($NNPlus6)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => strlen($MguNPlus7) == 1 ? '0'.$MguNPlus7 : $MguNPlus7,
                                    'data' => [floatval($NNPlus7)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => strlen($MguNPlus8) == 1 ? '0'.$MguNPlus8 : $MguNPlus8,
                                    'data' => [floatval($NNPlus8)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => strlen($MguNPlus9) == 1 ? '0'.$MguNPlus9 : $MguNPlus9,
                                    'data' => [floatval($NNPlus9)],
                                ],
                                [
                                    'type' => 'column',
                                    'name' => strlen($MguNPlus10) == 1 ? '0'.$MguNPlus10 : $MguNPlus10,
                                    'data' => [floatval($NNPlus10)],
                                ],
                            ],
                        ]
                    ]); 
                ?>
            </div>
        </div>
        <?php 
        
        
        
        
        
        
        
        /*<!-- row pertama -->
        <div class="row dashboard">
            <div class="col-lg-6">
                <?=

                Highcharts::widget([
                    'setupOptions' => [
                        'global' => ['useUTC' => FALSE],
                    ],

                    'options' => [
                        'chart' =>[
                            'type' => 'pie',
                            'options3d' => [
                                'enabled'=>true,
                                'alpha'=>45,
                                //'beta'=>0,
                                //'depth'=>70,
                                //'viewDistance'=>25,
                            ],
                        ],
                        'colors'=>["#3097FE", "#CF1A1F", "#FFB844", "#1ACF1F", "#8547C0"],
                        'title' => ['text' => 'Data Status Kerja'],
                        'subtitle'=>[
                            'text'=>'Total Pegawai : '.$totpeg.' Pegawai',
                        ],
                        'tooltip'=>[
                            'pointFormat'=>'{series.name}: <b>{point.percentage:.1f}%</b>'
                        ],
                        'plotOptions'=> [
                            'pie'=>[
                                //'allowPointSelect' =>true,
                                //'cursor'=>'pointer',
                                'innerSize'=>100,
                                'depth'=>45,
                                //'dataLabels'=>[
                                    //'enabled'=>false,
                                    //'format'=>'{point.name}',
                                //],
                                //'showInLegend'=>true,
                            ]
                        ],
                        'series' => [
                            [
                                'name' => 'Pegawai',
                                'colorByPoint'=>true, 
                                'data' => [
                                    [
                                        'name'=>'PKWT : '.$modelpegpkwt.' ('.$percentpkwt.')',
                                        'y'=>$descpkwt,
                                    ],
                                    [
                                        'name'=>'PKWTT : '.$modelpegpkwtt.' ('.$percentpkwtt.')',
                                        'y'=> $descpkwtt,
                                        'sliced'=>true,
                                        'selected'=>true
                                    ],
                                    [
                                        'name'=>'THL : '.$modelpegthl.' ('.$percentthl.')',
                                        'y'=> $descthl,
                                    ],
                                    [
                                        'name'=>'DLL : '.$modelpegdll.' ('.$percentdll.')',
                                        'y'=> $descdll,
                                    ],
                                    [
                                        'name'=>'PLN : '.$modelpegpln.' ('.$percentpln.')',
                                        'y'=> $descpln,
                                    ],
                                ]
                            ],
                        ]
                    ]
                ]);

                ?>
            </div>
            <div class="col-lg-6">
                <?=

                Highcharts::widget([
                    'options' => [
                        'chart' =>[
                            'type' => 'pie',
                            'options3d' => [
                                'enabled'=>true,
                                'alpha'=>45,
                                //'beta'=>0,
                                //'depth'=>70,
                                //'viewDistance'=>25,
                            ],
                        ],
                        'colors'=>["#3097FE", "#CF1A1F", "#FFB844", "#1ACF1F", "#8547C0"],
                        'title' => ['text' => 'Data Usia Pegawai'],
                        'subtitle'=>[
                            'text'=>'Total Pegawai : '.$totpeg4.' Pegawai',
                        ],
                        'tooltip'=>[
                            'pointFormat'=>'{series.name}: <b>{point.percentage:.1f}%</b>'
                        ],
                        'plotOptions'=> [
                            'pie'=>[
                                //'allowPointSelect' =>true,
                                //'cursor'=>'pointer',
                                'innerSize'=>100,
                                'depth'=>45,
                                //'dataLabels'=>[
                                    //'enabled'=>false,
                                    //'format'=>'{point.name}',
                                //],
                                //'showInLegend'=>true,
                            ]
                        ],
                        'series' => [
                            [
                                'name' => 'Pegawai',
                                'colorByPoint'=>true, 
                                'data' => [
                                    [
                                        'name'=>'Usia < 45 : '.$modelusia1.' ('.$percentusia1.')',
                                        'y'=>$descusia1,
                                        'sliced'=>true,
                                        'selected'=>true
                                    ],
                                    [
                                        'name'=>'< 45 Usia <= 46 : '.$modelusia2.' ('.$percentusia2.')',
                                        'y'=> $descusia2,
                                    ],
                                    [
                                        'name'=>'> 46 Usia <= 56 : '.$modelusia3.' ('.$percentusia3.')',
                                        'y'=> $descusia3,
                                    ],
                                    [
                                        'name'=>'Usia > 56 : '.$modelusia4.' ('.$percentusia4.')',
                                        'y'=> $descusia4,
                                    ],
                                    [
                                        'name'=>'Dll : '.$modelusiadll.' ('.$percentusiadll.')',
                                        'y'=> $descusiadll,
                                    ],
                                ]
                            ],
                        ]
                    ]
                ]);

                ?>
            </div>
        </div>
        <!-- row kedua -->
        <div class="row dashboard">
            <div class="col-lg-6">
                <?=
                    Highcharts::widget([
                        'options' => [
                            'chart' =>[
                                'type' => 'column',
                                'options3d' => [
                                    'enabled'=>true,
                                    'alpha'=>10,
                                    'beta'=>25,
                                    'depth'=>70,
                                    //'viewDistance'=>25,
                                ],
                            ],
                            'colors'=>["#CF1A1F"],
                            'title' => ['text' => 'Data Pendidikan Tenaga Kerja'],
                            'xAxis'=>[
                                 "categories"=>["SD","SMP","SMA","D1","D2","D3","D4","S1","S2","S3","Dll"]
                              ],
                            'yAxis'=>[
                                 "title"=>["text"=>"Jumlah Tenaga Kerja"]
                              ],
                            'plotOptions'=> [
                                'column'=>[
                                    'depth' =>45,
                                ]
                            ],
                            'series' => [
                                [
                                    'name' => 'Tenaga Kerja',
                                    'data' => [
                                        [
                                            'y'=>intval($countSD),
                                        ],
                                        [
                                            'y'=>intval($countSMP),
                                        ],
                                        [
                                            'y'=>intval($countSMA),
                                        ],
                                        [
                                            'y'=>intval($countD1),
                                        ],
                                        [
                                            'y'=>intval($countD2),
                                        ],
                                        [
                                            'y'=>intval($countD3),
                                        ],
                                        [
                                            'y'=>intval($countD4),
                                        ],
                                        [
                                            'y'=>intval($countS1),
                                        ],
                                        [
                                            'y'=>intval($countS2),
                                        ],
                                        [
                                            'y'=>intval($countS3),
                                        ],
                                        [
                                            'y'=>intval($countdll),
                                        ],
                                    ]
                                ],
                            ]
                        ]
                    ]);
                ?>
            </div>
            <div class="col-lg-6">
                
            </div>
        </div>*/
        ?>
    </div>

</div>
