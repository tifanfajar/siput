<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\DataVideoWeb */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-video-web-form">

     <?php $form = ActiveForm::begin([
        'id' => 'form-video-web',
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 2,'showErrors' => true, 'deviceSize' => ActiveForm::SIZE_LARGE],
        'options'=>['enctype'=>'multipart/form-data']
    ]); ?>

    <div class="panel panel-default">
        <div class="panel-body">
            <?= $form->field($model, 'STATUS_VIDEO')->dropDownList(['0'=>'Youtube','1'=>'File'], ['prompt' => ' -- Select --']) ?>

            <?= $form->field($model, 'PATH_YOUTUBE')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'PATH_VIDEO')->widget(FileInput::classname(), [
                'options' => ['multiple' => false, 'accept' => 'mp4/*'],
                'pluginOptions' => [
                    'initialPreview'=>[
                        Html::img('../'.$model->PATH_VIDEO, ['class'=>'file-preview-image','width'=>'200px']),
                    ],
                    'initialCaption'=> substr($model->PATH_VIDEO,17,-17).'*.mp4',
                    'allowedFileExtensions'=>['mp4'],
                    'showPreview' => true,
                    'showCaption' => true,
                    'showRemove' => true,
                    'showUpload' => false
                ]
                ]) 
            ?> 

            <?= $form->field($model, 'STATUS')->dropDownList(['0'=>'Tidak Aktif','1'=>'Aktif'], ['prompt' => ' -- Select --']) ?>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-9"><?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?></div>
                
            </div>
        </div>
    </div>
    

    <?php ActiveForm::end(); ?>

</div>
