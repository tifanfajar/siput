<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DataVideoWeb */

$this->title = 'Ubah Data Video : ' . $model->ID_DATA_VIDEO_WEB;
$this->params['breadcrumbs'][] = ['label' => 'Data Video', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_DATA_VIDEO_WEB, 'url' => ['view', 'id' => $model->ID_DATA_VIDEO_WEB]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="data-video-web-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
