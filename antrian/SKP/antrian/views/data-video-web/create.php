<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DataVideoWeb */

$this->title = 'Tambah Data Video';
$this->params['breadcrumbs'][] = ['label' => 'Data Video', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-video-web-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
