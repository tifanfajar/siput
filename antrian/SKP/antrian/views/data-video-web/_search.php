<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DataVideoWebSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-video-web-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID_DATA_VIDEO_WEB') ?>

    <?= $form->field($model, 'STATUS_VIDEO') ?>

    <?= $form->field($model, 'PATH_YOUTUBE') ?>

    <?= $form->field($model, 'PATH_VIDEO') ?>

    <?= $form->field($model, 'STATUS') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
