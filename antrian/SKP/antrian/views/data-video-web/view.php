<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DataVideoWeb */

$this->title = 'View Data Video : '.$model->ID_DATA_VIDEO_WEB;
$this->params['breadcrumbs'][] = ['label' => 'Data Video Web', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-video-web-view">
    <div class="panel panel-default">
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'ID_DATA_VIDEO_WEB',
                    [                     
                        'label' => 'Status Video',
                        'value' => $model->STATUS_VIDEO === '0' ? 'Youtube' : ($model->STATUS_VIDEO === '1' ? 'File':'-'),
                    ],
                    'PATH_YOUTUBE:ntext',
                    'PATH_VIDEO:ntext',
                    [                     
                        'label' => 'Status',
                        'value' => $model->STATUS === '0' ? 'Tidak Aktif' : ($model->STATUS === '1' ? 'Aktif':'-'),
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>
