<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DataVideoWebSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Video';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-video-web-index">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>
                        <?= Html::a('Tambah Data Video', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                </div>
            </div>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'ID_DATA_VIDEO_WEB',
                    [
                        'attribute'=>'STATUS_VIDEO',
                        //'header'=>'Status PPN',
                        'value'=>function($data) {
                            $group = $data->STATUS_VIDEO;
                            if($group === '1')
                            {return "File";}
                            else 
                            { return "Youtube"; }
                        },
                        'headerOptions'=>['style'=>'width: 60px;'],
                    ],          
                    'PATH_YOUTUBE:ntext',
                    'PATH_VIDEO:ntext',
                    [
                        'attribute'=>'STATUS',
                        //'header'=>'Status PPN',
                        'value'=>function($data) {
                            $group = $data->STATUS;
                            if($group === '1')
                            {return "Aktif";}
                            else 
                            { return "Tidak Aktif"; }
                        },
                        'headerOptions'=>['style'=>'width: 60px;'],
                    ],    
                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
