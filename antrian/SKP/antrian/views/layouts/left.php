<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="/antrian/images/logo.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><small>Kementerian</br>Komunikasi dan</br>Informatika</small></p>                
            </div>
        </div>
        
        <?php
        if(!yii::$app->user->isGuest)
        {
            
            
            echo dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => 
                [
                    ['label' => 'Home', 'icon' => 'home', 'url' => ['site/index']],
                    [
                        'label' => 'Statistik Loket',
                        'icon' => 'area-chart',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Sertifikat REOR','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/statistikloket', 'loket'=>'1']],
                            ['label' => 'Pengambilan ISR Darat','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/statistikloket', 'loket'=>'2']],
                            ['label' => 'Permohonan ISR Darat','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/statistikloket', 'loket'=>'3']],
                            ['label' => 'Payment & BHP','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/statistikloket', 'loket'=>'4']],
                            ['label' => 'Pencetakan SSP','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/statistikloket', 'loket'=>'5']],
                            ['label' => 'Permohonan ISR Maritim & Penerbangan','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/statistikloket', 'loket'=>'6']],
                            ['label' => 'ISR Broadcast & Satelit','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/statistikloket', 'loket'=>'7']],
                            ['label' => 'Sertifikasi Perangkat & Perpanjangan','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/statistikloket', 'loket'=>'8']],
                            ['label' => 'Pengambilan Sertifikat','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/statistikloket', 'loket'=>'9']],
                            ['label' => 'Pengambilan SP2 & SP3','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/statistikloket', 'loket'=>'10']],
                        ],
                    ],
                    [
                        'label' => 'Setting',
                        'icon' => 'fa fa-gears',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Data Video','icon' => 'circle-o','url' => ['/data-video-web/index']],
                            ['label' => 'Data Slide Show','icon' => 'circle-o','url' => ['/data-slideshow/index']],
                        ],
                    ],
                    /*[
                        'label' => 'Kepuasan Pengunjung',
                        'icon' => 'smile-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Sertifikat REOR','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/kepuasanpengunjung', 'loket'=>'1']],
                            ['label' => 'Pengambilan ISR Darat','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/kepuasanpengunjung', 'loket'=>'2']],
                            ['label' => 'Permohonan ISR Darat','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/kepuasanpengunjung', 'loket'=>'3']],
                            ['label' => 'Payment & BHP','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/kepuasanpengunjung', 'loket'=>'4']],
                            ['label' => 'Pencetakan SSP','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/kepuasanpengunjung', 'loket'=>'5']],
                            ['label' => 'Permohonan ISR Maritim & Penerbangan','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/kepuasanpengunjung', 'loket'=>'6']],
                            ['label' => 'ISR Broadcast & Satelit','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/kepuasanpengunjung', 'loket'=>'7']],
                            ['label' => 'Sertifikasi Perangkat & Perpanjangan','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/kepuasanpengunjung', 'loket'=>'8']],
                            ['label' => 'Pengambilan Sertifikat','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/kepuasanpengunjung', 'loket'=>'9']],
                            ['label' => 'Pengambilan SP2 & SP3','icon' => 'circle-o','url' => ['/data-kepuasan-pengunjung/kepuasanpengunjung', 'loket'=>'10']],
                        ],
                    ],*/
                    //['label' => 'Logout', 'icon' => 'sign-out', 'url' => ['site/logout']],
                ]
            ]);
        }
        else
        {
            echo dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => 
                [
                    ['label' => 'Home', 'icon' => 'home ', 'url' => ['site/index']],
                    ['label' => 'Login', 'icon' => 'sign-in', 'url' => ['site/login']],                    
                ]
            ]);
        }
        ?>
    </section>

</aside>
