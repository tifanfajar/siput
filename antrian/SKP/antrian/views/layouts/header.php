<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">APP</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
               
                <!-- User Account: style can be found in dropdown.less -->
                
                <?php
                if(!yii::$app->user->isGuest)
                {
                ?>                
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/antrian/images/logo.png" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"><?= Yii::$app->user->identity->USER_NAME ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/antrian/images/logo.png" class="img-circle"
                                 alt="User Image"/>

                            <p>
                                <?= Yii::$app->user->identity->USER_NAME ?>
                                <small><?= Yii::$app->user->identity->USER_NAME ?></small>
                            </p>
                        </li>                  
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>
                <?php                
                }else{                
                ?>
                <!--<li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/asset/images/logo.png"  class="user-image" alt="User Image"/>
                        <span class="hidden-xs">Badan Intelijen Negara</span>
                    </a>
                    <ul class="dropdown-menu">
                        
                        <li class="user-header">
                            <img src="/asset/images/logo.png" class="img-circle" alt="User Image"/>
                            <p>
                                Badan Intelijen Negara
                                <small>Jl. Seno Raya, Pejaten Timur, Pasar Minggu, RT.2/RW.4, Ps. Minggu, Jakarta Selatan 12510</small>
                            </p>
                        </li>
                    </ul>
                </li>-->
                <?php                
                }                
                ?>

                <!-- User Account: style can be found in dropdown.less -->
                <li>
                    <!--<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>-->
                </li>
            </ul>
        </div>
    </nav>
</header>
