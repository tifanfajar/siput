<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DataSlideshowSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Slide Show';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-slideshow-index">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="panel panel-default">
                <div class="panel-body">
                    <p>
                        <?= Html::a('Tambah Data Slide Show', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                </div>
            </div>  
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    'ID_DATA_SLIDESHOW',
                    'IMAGE1:ntext',
                    'IMAGE2:ntext',
                    'IMAGE3:ntext',
                    [
                        'attribute'=>'STATUS',
                        //'header'=>'Status PPN',
                        'value'=>function($data) {
                            $group = $data->STATUS;
                            if($group === '1')
                            {return "Aktif";}
                            else 
                            { return "Tidak Aktif"; }
                        },
                        'headerOptions'=>['style'=>'width: 60px;'],
                    ],    

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            
            
            
            
            
            
            
        </div>
    </div>

    
</div>
