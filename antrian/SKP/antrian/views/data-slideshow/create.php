<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DataSlideshow */

$this->title = 'Tambah Data Slide Show';
$this->params['breadcrumbs'][] = ['label' => 'Data Slide Show', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-slideshow-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
