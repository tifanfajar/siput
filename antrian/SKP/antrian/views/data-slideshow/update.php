<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DataSlideshow */

$this->title = 'Ubah Data Slide Show : ' . $model->ID_DATA_SLIDESHOW;
$this->params['breadcrumbs'][] = ['label' => 'Data Slideshow', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_DATA_SLIDESHOW, 'url' => ['view', 'id' => $model->ID_DATA_SLIDESHOW]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="data-slideshow-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
