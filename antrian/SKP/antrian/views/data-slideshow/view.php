<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DataSlideshow */

$this->title = 'View Data Slide Show : '.$model->ID_DATA_SLIDESHOW;
$this->params['breadcrumbs'][] = ['label' => 'Data Slideshows', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-slideshow-view">
    <div class="panel panel-default">
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'ID_DATA_SLIDESHOW',
                    'IMAGE1:ntext',
                    'IMAGE2:ntext',
                    'IMAGE3:ntext',
                    [                     
                        'label' => 'Status',
                        'value' => $model->STATUS === '0' ? 'Tidak Aktif' : ($model->STATUS === '1' ? 'Aktif':'-'),
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>
