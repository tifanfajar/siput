<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DataSlideshowSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-slideshow-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID_DATA_SLIDESHOW') ?>

    <?= $form->field($model, 'IMAGE1') ?>

    <?= $form->field($model, 'IMAGE2') ?>

    <?= $form->field($model, 'IMAGE3') ?>

    <?= $form->field($model, 'STATUS') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
