<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\DataSlideshow */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-slideshow-form">
    
     <?php $form = ActiveForm::begin([
        'id' => 'form-slide-show',
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 2,'showErrors' => true, 'deviceSize' => ActiveForm::SIZE_LARGE],
        'options'=>['enctype'=>'multipart/form-data']
    ]); ?>

    <div class="panel panel-default">
        <div class="panel-body">
            
            <?= $form->field($model, 'IMAGE1')->widget(FileInput::classname(), [
                'options' => ['multiple' => false, 'accept' => 'jpg/*'],
                'pluginOptions' => [
                    'initialPreview'=>[
                        Html::img('../'.$model->IMAGE1, ['class'=>'file-preview-image','width'=>'200px']),
                    ],
                    'initialCaption'=> substr($model->IMAGE1,17,-17).'*.jpg',
                    'allowedFileExtensions'=>['jpg','jpeg','png'],
                    'showPreview' => true,
                    'showCaption' => true,
                    'showRemove' => true,
                    'showUpload' => false
                ]
                ]) 
            ?> 
            
            <?= $form->field($model, 'IMAGE2')->widget(FileInput::classname(), [
                'options' => ['multiple' => false, 'accept' => 'jpg/*'],
                'pluginOptions' => [
                    'initialPreview'=>[
                        Html::img('../'.$model->IMAGE2, ['class'=>'file-preview-image','width'=>'200px']),
                    ],
                    'initialCaption'=> substr($model->IMAGE2,17,-17).'*.jpg',
                    'allowedFileExtensions'=>['jpg','jpeg','png'],
                    'showPreview' => true,
                    'showCaption' => true,
                    'showRemove' => true,
                    'showUpload' => false
                ]
                ]) 
            ?> 
            
            <?= $form->field($model, 'IMAGE3')->widget(FileInput::classname(), [
                'options' => ['multiple' => false, 'accept' => 'jpg/*'],
                'pluginOptions' => [
                    'initialPreview'=>[
                        Html::img('../'.$model->IMAGE3, ['class'=>'file-preview-image','width'=>'200px']),
                    ],
                    'initialCaption'=> substr($model->IMAGE3,17,-17).'*.jpg',
                    'allowedFileExtensions'=>['jpg','jpeg','png'],
                    'showPreview' => true,
                    'showCaption' => true,
                    'showRemove' => true,
                    'showUpload' => false
                ]
                ]) 
            ?> 
            
            
            <?= $form->field($model, 'STATUS')->dropDownList(['0'=>'Tidak Aktif','1'=>'Aktif'], ['prompt' => ' -- Select --']) ?>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-9"><?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?></div>
                
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
