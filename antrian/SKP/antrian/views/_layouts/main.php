<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\data\DataProviderInterface;
use app\models\TblKontrakSearch;
use app\models\DataPenagihanSearch;
use app\models\DataPerubahanPenagihanSearch;
use app\models\DataPenggunaDetail;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= /*Html::encode($this->title)*/ yii::$app->name ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="row" style="margin:0;">
        <?= Html::img('/hpikomon/images/header_01.png',$options = ['class' => 'img-responsive pos-img image']);?>
    </div>
    <?php
    NavBar::begin([
        //'brandLabel' => yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
        'class' => 'navbar-inverse',
        ],
    ]);

    if(!yii::$app->user->isGuest)
    {
        //buat bikin query where unit
        $whereqwery = '';
        $idxReg=0;
        $idxUnit=0;
        
        if (empty(yii::$app->session['qweryhakakses']))
        {
            $whereqwery = "";
            
            if(Yii::$app->user->identity->JENIS_USER=="2" or Yii::$app->user->identity->JENIS_USER=="4")
            {
                $idPeng = Yii::$app->user->identity->ID_PENGGUNA;
                $whereqwery = ' ( ';

                $model = DataPenggunaDetail::find()
                    ->select('ID_UNIT')  
                    ->where(['ID_PENGGUNA' => $idPeng]) 
                    ->all();   

                foreach ($model as $value) :
                    if($value->ID_UNIT != "")
                    {$whereqwery = $whereqwery . "ID_UNIT = '" . $value->ID_UNIT . "' OR " ;$idxUnit++;}
                endforeach;
                //print_r($idxUnit);die;
                
                if($idxUnit==0)
                {$whereqwery = substr($whereqwery, 0, -7);}
                else 
                {$whereqwery = ($idxUnit != 0) ? substr($whereqwery, 0, -3). ")" : $whereqwery;}
                
                if($idxUnit!=0)
                {
                    yii::$app->session['qweryhakakses'] = $whereqwery;
                    yii::$app->session['qweryhakaksesand'] = ' AND'.$whereqwery;
                }
                //print_r($whereqwery);die;
            }   
            
            
        }
        
        //print_r(yii::$app->session['qweryhakakses']);
        //print_r(yii::$app->session['qweryhakaksesand']);die;
        
        $jml = 0;
        
        $PegApp = new TblKontrakSearch();
        $ProvPegApp = $PegApp->searchApproval(Yii::$app->request->queryParams);
        $jml = $ProvPegApp->getTotalCount();
        
        $jmlPenagihan = 0;
        $jmlPerubahanTagihan = 0;
        
        $PenagihanApp = new DataPenagihanSearch();
        $ProvPenagihanApp = $PenagihanApp->searchInputFaktur(Yii::$app->request->queryParams);
        $jmlInputFaktur = $ProvPenagihanApp->getTotalCount();
        
        $PerubahanTagihanApp = new DataPerubahanPenagihanSearch();
        $ProvPerubahanTagihanApp = $PerubahanTagihanApp->searchBelumApproval(Yii::$app->request->queryParams);
        $jmlPerubahanTagihan = $ProvPerubahanTagihanApp->getTotalCount();
        
        $jmlPenagihan = $jmlInputFaktur + $jmlPerubahanTagihan;
        
        $jmlMonitoring = 0;
        $jmlMonitoringKdMsalah = 0;
        $jmlMonitoringProyeksi = 0;
        
        $MonitoringApp = new DataPenagihanSearch();
        $ProvMonitoringApp = $MonitoringApp->searchMonitoring(Yii::$app->request->queryParams, "");
        $jmlMonitoringKdMsalah = $ProvMonitoringApp->getTotalCount();
        
        $jmlMonitoring = $jmlMonitoringKdMsalah + $jmlMonitoringProyeksi;
        
        if(Yii::$app->user->identity->JENIS_USER=="2")
        {
            $menu_items[] = ['label' => 'Home', 'url' => ['/site/index']];  
            $menu_items[] = ['label' => 'Data Tagihan', 'url' => ['#'],
                                        'items' => [
                                        ['label' => 'Create Tagihan', 'url' => ['/data-penagihan/create']],
                                        ['label' => 'Cetak SPP dan Kwitansi', 'url' => ['/data-penagihan/indexcetak']],
                                        ['label' => 'Daftar Tagihan', 'url' => ['/data-penagihan/index']],                                    
                                        //'<li class="divider"></li>',
                                        //['label' => 'Create Tagihan Dummy', 'url' => ['/data-dummy-tagihan/create']],
                                        //['label' => 'Daftar Tagihan Dummy', 'url' => ['/data-dummy-tagihan/index']], 
                                        '<li class="divider"></li>',
                                        ['label' => 'Upload BAPP dan BASTP', 'url' => ['/data-penagihan/uploadbappbastp']],
                                        ['label' => 'Upload Bukti Potong PPh Pasal 23', 'url' => ['/data-penagihan/uploadpph23']],
                                        '<li class="divider"></li>',
                                        ['label' => 'Pengantian & Pembatalan Faktur', 'url' => ['/data-perubahan-penagihan/create']],
                                        ['label' => 'Daftar Pengantian & Pembatalan Faktur', 'url' => ['/data-perubahan-penagihan/index']],
                                        ]
                            ];
            //$menu_items[] = ['label' => 'Monitoring Tagihan ('.$jmlMonitoring.')', 'url' => ['/data-penagihan/indexmonitoring']];        
            $menu_items[] = ['label' => 'Monitoring ('.$jmlMonitoring.')', 'url' => ['#'],
                                        'items' => [
                                        ['label' => 'Kode Masalah', 'url' => ['/data-penagihan/indexmonitoring']],
                                        ['label' => 'Proyeksi Lunas', 'url' => ['/data-penagihan/indexmonitoringproyeksi']],
                                        ]
                            ];
            $menu_items[] = ['label' => 'Laporan', 'url' => ['#'],
                                        'items' => [
                                        ['label' => 'Laporan Umur Tagihan BASTP', 'url' => ['/data-penagihan/indexlapumurtagbapp']],
                                        ['label' => 'Laporan Umur Tagihan Keuangan', 'url' => ['/data-penagihan/indexlapumurtagkeu']],
                                        ['label' => 'Laporan Bukti Potong', 'url' => ['/data-penagihan/indexbuktipotong']],
                                        ['label' => 'Laporan Pelunasan', 'url' => ['/data-penagihan/indexlappelunasan']],
                                        ['label' => 'Laporan Rekonsiliasi', 'url' => ['/data-penagihan/indexlaprekapitulasi']],
                                        ['label' => 'Laporan Rekap Monitoring', 'url' => ['/data-penagihan/rekapmonitoring']],
                                        ]
                            ];
            $menu_items[] = ['label' => 'Data master', 'url' => ['#'],
                                        'items' => [
                                        //['label' => 'Data Pengguna Aplikasi', 'url' => ['/data-pengguna/index']],
                                        ['label' => 'Data Kode Customer', 'url' => ['/data-kcust-pln/index']],
                                        //['label' => 'Data Internal Order', 'url' => ['/data-internal-order/index']],
                                        //['label' => 'Data Kode Bank', 'url' => ['/data-kode-bank/index']],
                                        //['label' => 'Data Kode Permasalahan', 'url' => ['/data-kode-permasalahan/index']],
                                        //['label' => 'Data Jenis Pekerjaan', 'url' => ['/data-jenis-pekerjaan/index']],
                                        //['label' => 'Data Region', 'url' => ['/data-region/index']],
                                        ['label' => 'Data Unit HPI', 'url' => ['/data-unit/index']],
                                        //['label' => 'Data Penerima Email', 'url' => ['/tbl-mail/index']],
                                        ]
                            ];
            $menu_items[] = ['label' => Yii::$app->user->identity->NAMA_PENGGUNA,//yii::$app->session['qweryhakakses'],
                                        'items' => [
                                        ['label' => 'Rubah Password','url' => ['/data-pengguna/updatepass']],
                                        '<li class="divider"></li>',
                                        ['label' => 'Logout','url' => ['/site/logout'],
                                        'linkOptions' => ['data-method' => 'post']]
                                        ]
                            ];
        }
        else if(Yii::$app->user->identity->JENIS_USER=="4")
        {
            $menu_items[] = ['label' => 'Home', 'url' => ['/site/index']];
            $menu_items[] = ['label' => 'Data Kontrak ('.$jml.')', 'url' => ['#'],
                                        'items' => [
                                        ['label' => 'Data Kontrak', 'url' => ['/data-header-kontrak/indexview']],
                                        ]
                            ];
            $menu_items[] = ['label' => 'Data Tagihan', 'url' => ['#'],
                                        'items' => [
                                        ['label' => 'Daftar Tagihan', 'url' => ['/data-penagihan/indexview']],                                    
                                        ]
                            ];
            $menu_items[] = ['label' => 'Monitoring ('.$jmlMonitoring.')', 'url' => ['#'],
                                        'items' => [
                                        ['label' => 'Kode Masalah', 'url' => ['/data-penagihan/indexmonitoringview']],
                                        ['label' => 'Proyeksi Lunas', 'url' => ['/data-penagihan/indexmonitoringproyeksiview']],
                                        ]
                            ];
            $menu_items[] = ['label' => Yii::$app->user->identity->NAMA_PENGGUNA,//yii::$app->session['qweryhakakses'],
                                        'items' => [
                                        ['label' => 'Logout','url' => ['/site/logout'],
                                        'linkOptions' => ['data-method' => 'post']]
                                        ]
                            ];
        }
        else
        {
            $menu_items[] = ['label' => 'Home', 'url' => ['/site/index']];        
            $menu_items[] = ['label' => 'Data Kontrak ('.$jml.')', 'url' => ['#'],
                                        'items' => [
                                        ['label' => 'Tambah Kontrak', 'url' => ['/data-header-kontrak/create']],
                                        ['label' => 'Data Kontrak', 'url' => ['/data-header-kontrak/index']],
                                        /*['label' => 'Tambah Kontrak', 'url' => ['/tbl-kontrak/create']],
                                        ['label' => 'Data Kontrak', 'url' => ['/tbl-kontrak/index']],
                                        ['label' => 'Data On Progress', 'url' => ['/tbl-kontrak/indexonprogress']],
                                        ['label' => "Data Multi Year's", 'url' => ['/tbl-kontrak/indexmultiyears']],
                                        ['label' => 'Data History Kontrak', 'url' => ['/tbl-kontrak/indexhistory']],
                                        ['label' => 'Approval Kontrak ('.$jml.')', 'url' => ['/tbl-kontrak/indexapproval']],
                                        ['label' => 'Update Status Kontrak', 'url' => ['#']],
                                        ['label' => 'Update Progress Kontrak Pending', 'url' => ['#']],*/
                                        ]
                            ];
            /*$menu_items[] = ['label' => 'Monitoring', 'url' => ['#'],
                                        'items' => [
                                        ['label' => 'Monitoring Kontrak', 'url' => ['/tbl-kontrak/indexmonkontrak', 'Mgu'=>'1']],
                                        ['label' => 'Monitoring Pengguna Jasa', 'url' => ['/tbl-kontrak/indexmonpengjasa', 'KodeCust'=>'-']],
                                        ['label' => 'Summary Kontrak', 'url' => ['/tbl-kontrak/indexsummary']],
                                        ]
                            ];*/
            $menu_items[] = ['label' => 'Data Tagihan ('.$jmlPenagihan.')', 'url' => ['#'],
                                        'items' => [
                                        ['label' => 'Create Tagihan', 'url' => ['/data-penagihan/create']],
                                        ['label' => 'Cetak SPP dan Kwitansi', 'url' => ['/data-penagihan/indexcetak']],                                        
                                        ['label' => 'Daftar Tagihan', 'url' => ['/data-penagihan/index']],                                    
                                        ['label' => 'Input Data Faktur ('.$jmlInputFaktur.')', 'url' => ['/data-penagihan/indexinputfaktur']],
                                        '<li class="divider"></li>',
                                        ['label' => 'Create Tagihan Dummy', 'url' => ['/data-dummy-tagihan/create']],
                                        ['label' => 'Daftar Tagihan Dummy', 'url' => ['/data-dummy-tagihan/index']], 
                                        '<li class="divider"></li>',
                                        ['label' => 'Pengantian & Pembatalan Faktur', 'url' => ['/data-perubahan-penagihan/create']],
                                        ['label' => 'Daftar Pengantian & Pembatalan Faktur', 'url' => ['/data-perubahan-penagihan/index']],
                                        ['label' => 'Approval Pengantian & Pembatalan Faktur ('.$jmlPerubahanTagihan.')', 'url' => ['/data-perubahan-penagihan/indexapproval']],
                                        '<li class="divider"></li>',
                                        ['label' => 'Upload BAPP dan BASTP', 'url' => ['/data-penagihan/uploadbappbastp']],
                                        ['label' => 'Upload Bukti Potong PPh Pasal 23', 'url' => ['/data-penagihan/uploadpph23']],
                                        '<li class="divider"></li>',
                                        ['label' => 'Pelunasan Tagihan', 'url' => ['/data-penagihan/indexpelunasan']],                                            
                                        '<li class="divider"></li>',
                                        ['label' => 'Update & Delete Data Tagihan', 'url' => ['/data-penagihan/indexupdatetagihan']],                                            
                                        ]
                            ];
            //$menu_items[] = ['label' => 'Monitoring Tagihan ('.$jmlMonitoring.')', 'url' => ['/data-penagihan/indexmonitoring']];        
            $menu_items[] = ['label' => 'Monitoring ('.$jmlMonitoring.')', 'url' => ['#'],
                                        'items' => [
                                        ['label' => 'Kode Masalah', 'url' => ['/data-penagihan/indexmonitoring']],
                                        ['label' => 'Proyeksi Lunas', 'url' => ['/data-penagihan/indexmonitoringproyeksi']],
                                        ]
                            ];
            $menu_items[] = ['label' => 'Laporan', 'url' => ['#'],
                                        'items' => [
                                        ['label' => 'Laporan Umur Tagihan BASTP', 'url' => ['/data-penagihan/indexlapumurtagbapp']],
                                        ['label' => 'Laporan Umur Tagihan Keuangan', 'url' => ['/data-penagihan/indexlapumurtagkeu']],
                                        ['label' => 'Laporan Bukti Potong', 'url' => ['/data-penagihan/indexbuktipotong']],
                                        ['label' => 'Laporan Pelunasan', 'url' => ['/data-penagihan/indexlappelunasan']],
                                        ['label' => 'Laporan Rekonsiliasi', 'url' => ['/data-penagihan/indexlaprekapitulasi']],
                                        ['label' => 'Laporan Rekap Monitoring', 'url' => ['/data-penagihan/rekapmonitoring']],
                                        ]
                            ];
            $menu_items[] = ['label' => 'Data master', 'url' => ['#'],
                                        'items' => [
                                        ['label' => 'Aktifitas Pengguna', 'url' => ['/aktivitas-pengguna/index']],
                                        ['label' => 'Data Pengguna Aplikasi', 'url' => ['/data-pengguna/index']],
                                        ['label' => 'Data Kode Customer', 'url' => ['/data-kcust-pln/index']],
                                        ['label' => 'Data Internal Order', 'url' => ['/data-internal-order/index']],
                                        ['label' => 'Data Kode Bank', 'url' => ['/data-kode-bank/index']],
                                        ['label' => 'Data Kode Permasalahan', 'url' => ['/data-kode-permasalahan/index']],
                                        ['label' => 'Data Jenis Pekerjaan', 'url' => ['/data-jenis-pekerjaan/index']],
                                        ['label' => 'Data Region', 'url' => ['/data-region/index']],
                                        ['label' => 'Data Unit HPI', 'url' => ['/data-unit/index']],
                                        ['label' => 'Data Penerima Email', 'url' => ['/tbl-mail/index']],
                                        ]
                            ];
            $menu_items[] = ['label' => Yii::$app->user->identity->NAMA_PENGGUNA,//yii::$app->session['qweryhakakses'],
                                        'items' => [
                                        ['label' => 'Rubah Password','url' => ['/data-pengguna/updatepass']],
                                        '<li class="divider"></li>',
                                        ['label' => 'Logout','url' => ['/site/logout'],
                                        'linkOptions' => ['data-method' => 'post']]
                                        ]
                            ];
        }
        
        
        
    }    
    else
    {
        $menu_items[] = ['label' => 'Home', 'url' => ['/site/index']];        
        $menu_items[] = ['label' => 'Login', 'url' => ['/site/login']];
        //$menu_items[] = ['label' => 'Pengajuan SPPD', 'url' => ['/data-pengajuan-sppd/create']];
        //['label' => 'Pengajuan SPPD', 'url' => ['/data-pengajuan-sppd/create']],
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => $menu_items,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container margin-bottom">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 center">
                Copyright &copy; <?= date('Y')?> <a href="www.hapindo.co.id">PT. Haleyora Powerindo</a> 
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3 center">
                All Rights Reserved.
            </div>
        </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
