<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DataKepuasanPengunjung */

$this->title = $model->ID_DATA_KEPUASAN_PENGUNJUNG;
$this->params['breadcrumbs'][] = ['label' => 'Data Kepuasan Pengunjungs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-kepuasan-pengunjung-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID_DATA_KEPUASAN_PENGUNJUNG], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID_DATA_KEPUASAN_PENGUNJUNG], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID_DATA_KEPUASAN_PENGUNJUNG',
            'LOKET',
            'TANGGAL',
            'NO_URUT',
            'RATING',
            'PATH_GAMBAR:ntext',
        ],
    ]) ?>

</div>
