<!DOCTYPE html>
<html>
<head>
	<title>Kementerian Komunikasi & Informatika</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="/antrian/images/kepuasan/custom.css">
<style type="text/css">
	.lk-back{
	 background: url(/antrian/images/kepuasan/lk-back.png);
	 background-repeat: no-repeat; 
	 background-position: center;
	 padding-top: 40px;
	 height: 120px;
	}
</style>
</head>
<body>

<!-- Main Content Mi -->
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4" style=" padding-left: 50px; padding-right: 50px;">
			<div class="col-md-12 lk-back">
                            <?php 
                             if($loket == "1"){
                            ?>
                            <label class="lbl-lak">Loket 1</label>
                            <label class="lbl-inf">Sertifikat REOR</label>
                            <?php 
                             }elseif($loket == "2"){
                            ?>
                            <label class="lbl-lak">Loket 2</label>
                            <label class="lbl-inf">Pengambilan ISR Darat</label>
                            <?php 
                             }elseif($loket == "3"){
                            ?>
                            <label class="lbl-lak">Loket 3</label>
                            <label class="lbl-inf">Permohonan ISR Darat</label>
                            <?php 
                             }elseif($loket == "4"){
                            ?>
                            <label class="lbl-lak">Loket 4</label>
                            <label class="lbl-inf">Payment & BHP</label>
                            <?php 
                             }elseif($loket == "5"){
                            ?>
                            <label class="lbl-lak">Loket 5</label>
                            <label class="lbl-inf">Pencetakan SSP</label>
                            <?php 
                             }elseif($loket == "6"){
                            ?>
                            <label class="lbl-lak">Loket 6</label>
                            <label class="lbl-inf">Permohonan ISR Maritim & Penerbangan</label>
                            <?php 
                             }elseif($loket == "7"){
                            ?>
                            <label class="lbl-lak">Loket 7</label>
                            <label class="lbl-inf">ISR Broadcast & Satelit</label>
                            <?php 
                             }elseif($loket == "8"){
                            ?>
                            <label class="lbl-lak">Loket 8</label>
                            <label class="lbl-inf">Sertifikasi Perangkat & Perpanjangan</label>
                            <?php 
                             }elseif($loket == "9"){
                            ?>
                            <label class="lbl-lak">Loket 9</label>
                            <label class="lbl-inf">Pengambilan Sertifikat</label>
                            <?php 
                             }elseif($loket == "10"){
                            ?>
                            <label class="lbl-lak">Loket 10</label>
                            <label class="lbl-inf">Pengambilan SP2 & SP3</label>
                            <?php 
                             }
                            ?>
			</div>
			<div class="img-div" style="margin-bottom: 10px;">
				<img src="/antrian/images/kepuasan/img-c.gif" width="100%">
			</div>
			<!-- <div class="cnt-no-antrian">
				<h5>Masukan Nomor Antrian Anda</h5>
				<input class="" type="text" name="input-nomor" placeholder="Nomor antrian anda">
			</div>
			<div class="form-group">
			    <label for="email">Nomor Antrian:</label>
			    <input type="email" class="form-control" id="email">
			  </div> -->
		</div>
		<div class="col-md-8">
			<h1>Apakah anda puas dengan pelayanan kami ?</h1>
			<a href="" class="col-md-12 btn-a">
				<img src="/antrian/images/kepuasan/img-a.gif" style="float: left; margin-right: 15px;">
				<h1>Sangat Puas</h1>
				<h4>Sangat puas dengan pelayanan kami</h4>
				<img class="img-right" src="/antrian/images/kepuasan/img-c0.png">
			</a>
			<a href="" class="col-md-12 btn-b">
			<img src="/antrian/images/kepuasan/img-b.gif" style="float: left; margin-right: 15px;">
				<h1>Puas</h1>
				<h4>Puas dengan pelayanan kami</h4>
				<img class="img-right" src="/antrian/images/kepuasan/img-c1.png">
			</a>
			<a href="" class="col-md-12 btn-c">
				<img src="/antrian/images/kepuasan/img-c.gif" style="float: left; margin-right: 15px;">
				<h1>Tidak Puas</h1>
				<h4>Tidak puas dengan pelayanan kami</h4>
				<img class="img-right" src="/antrian/images/kepuasan/img-c2.png">
			</a>
		</div>
	</div>
</div>
<!-- End Content Mi -->

<!--<marquee>Di cari buronan ber inisial T, ciri-ciri pundungan, rusuh, tukang revisi bilamana anda menemukanya jangan mau di ajak ke pom bensin atau ber akhir tragis.</marquee>-->


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>