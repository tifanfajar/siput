<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DataKepuasanPengunjung */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-kepuasan-pengunjung-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID_DATA_KEPUASAN_PENGUNJUNG')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'LOKET')->textInput() ?>

    <?= $form->field($model, 'TANGGAL')->textInput() ?>

    <?= $form->field($model, 'NO_URUT')->textInput() ?>

    <?= $form->field($model, 'RATING')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PATH_GAMBAR')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
