<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DataKepuasanPengunjungSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Kepuasan Pengunjung';
$this->params['breadcrumbs'][] = $this->title;
?>
<script>
    function printDiv(divName) {
		 var printContents = document.getElementById(divName).innerHTML;
		 var originalContents = document.body.innerHTML;

		 document.body.innerHTML = printContents;

		 window.print();

		 document.body.innerHTML = originalContents;
	}
</script>
<div class="data-kepuasan-pengunjung-index">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= ExportMenu::widget ([
                        'dataProvider' => $dataProvider,   
                        'columns' => [
                            [
                                'header' => 'Nama',
                                'value' => function($data) {
                                    return $data->getNama($data->TANGGAL, $data->LOKET, $data->NO_URUT);
                                },
                                'headerOptions'=>['style'=>'width: 100px; text-align:center;'],
                            ],
                            [
                                'header' => 'Jenis Kelamin',
                                'value' => function($data) {
                                    return $data->getJenisKelamin($data->TANGGAL, $data->LOKET, $data->NO_URUT);
                                },
                                'headerOptions'=>['style'=>'width: 100px; text-align:center;'],
                            ],
                            [
                                'header' => 'Instansi',
                                'value' => function($data) {
                                    return $data->getInstansi($data->TANGGAL, $data->LOKET, $data->NO_URUT);
                                },
                                'headerOptions'=>['style'=>'width: 100px; text-align:center;'],
                            ],                    
                            [
                                'header' => 'Tanggal',
                                'value' => function($data) {
                                    return $data->TANGGAL;
                                },
                                'headerOptions'=>['style'=>'width: 100px; text-align:center;'],
                            ],
                            [
                                'header' => 'Loket',
                                'value' => function($data) {
                                    return $data->LOKET;
                                },
                                'headerOptions'=>['style'=>'width: 50px; text-align:center;'],
                            ],
                            [
                                'header' => 'No Antrian',
                                'value' => function($data) {
                                    return $data->NO_URUT;
                                },
                                'headerOptions'=>['style'=>'width: 50px; text-align:center;'],
                            ],
                            [
                                'header' => 'Waktu Proses',
                                'value' => function($data) {
                                    return $data->getWaktuProses($data->TANGGAL, $data->LOKET, $data->NO_URUT);
                                },
                                'headerOptions'=>['style'=>'width: 50px; text-align:center;'],
                            ],
                            [
                                'header' => 'Rating',
                                'value' => function($data) {
                                    return $data->RATING;
                                },
                                'headerOptions'=>['style'=>'width: 100px; text-align:center;'],
                            ],
                        ],
                        'exportConfig'=> [
                            ExportMenu::FORMAT_TEXT => false,
                            ExportMenu::FORMAT_HTML => false,
                            //ExportMenu::FORMAT_CSV => false,
                            //ExportMenu::FORMAT_PDF => false
                        ],  
                        'filename'=>'Data Kepuasan Pelanggan ', 
                        //'showConfirmAlert'=>false,
                        ]);
                    ?>
                    <input type="button" onclick="printDiv('kwitansi')" value='Print' class="btn btn-success" />
                </div>        
            </div>
            <div id="kwitansi">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'responsive'=>true,
                    'columns' => [
                        [
                            'header' => 'No',
                            'class' => 'yii\grid\SerialColumn',
                            'headerOptions'=>['style'=>'width: 10px; text-align:center;'],
                        ],
                        [
                            'header' => 'Nama',
                            'value' => function($data) {
                                return $data->getNama($data->TANGGAL, $data->LOKET, $data->NO_URUT);
                            },
                            'headerOptions'=>['style'=>'width: 100px; text-align:center;'],
                        ],
                        [
                            'header' => 'Jenis Kelamin',
                            'value' => function($data) {
                                return $data->getJenisKelamin($data->TANGGAL, $data->LOKET, $data->NO_URUT);
                            },
                            'headerOptions'=>['style'=>'width: 100px; text-align:center;'],
                        ],
                        [
                            'header' => 'Instansi',
                            'value' => function($data) {
                                return $data->getInstansi($data->TANGGAL, $data->LOKET, $data->NO_URUT);
                            },
                            'headerOptions'=>['style'=>'width: 100px; text-align:center;'],
                        ],                    
                        [
                            'header' => 'Tanggal',
                            'value' => function($data) {
                                return $data->TANGGAL;
                            },
                            'headerOptions'=>['style'=>'width: 100px; text-align:center;'],
                        ],
                        [
                            'header' => 'Loket',
                            'value' => function($data) {
                                return $data->LOKET;
                            },
                            'headerOptions'=>['style'=>'width: 50px; text-align:center;'],
                        ],
                        [
                            'header' => 'No Antrian',
                            'value' => function($data) {
                                return $data->NO_URUT;
                            },
                            'headerOptions'=>['style'=>'width: 50px; text-align:center;'],
                        ],
                        [
                            'header' => 'Waktu Proses',
                            'value' => function($data) {
                                return $data->getWaktuProses($data->TANGGAL, $data->LOKET, $data->NO_URUT);
                            },
                            'headerOptions'=>['style'=>'width: 50px; text-align:center;'],
                        ],
                        [
                            'header' => 'Rating',
                            'value' => function($data) {
                                return $data->RATING;
                            },
                            'headerOptions'=>['style'=>'width: 100px; text-align:center;'],
                        ],
                    ],
                ]); ?>
            </div>            
        </div>
    </div>
</div>
