<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DataKepuasanPengunjung */

$this->title = 'Create Data Kepuasan Pengunjung';
$this->params['breadcrumbs'][] = ['label' => 'Data Kepuasan Pengunjungs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-kepuasan-pengunjung-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
