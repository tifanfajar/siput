<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DataKepuasanPengunjung */


if($loket == "1"){$this->title = "Sertifikat REOR";}
elseif($loket == "2"){$this->title = "Pengambilan ISR Darat";}
elseif($loket == "3"){$this->title = "Permohonan ISR Darat";}
elseif($loket == "4"){$this->title = "Payment & BHP";}
elseif($loket == "5"){$this->title = "Pencetakan SSP";}
elseif($loket == "6"){$this->title = "Permohonan ISR Maritim & Penerbangan";}
elseif($loket == "7"){$this->title = "ISR Broadcast & Satelit";}
elseif($loket == "8"){$this->title = "Sertifikasi Perangkat & Perpanjangan";}
elseif($loket == "9"){$this->title = "Pengambilan Sertifikat";}
elseif($loket == "10"){$this->title = "Pengambilan SP2 & SP3";}
else{$this->title = "-";}

//Tahunan
$varTidakPuasThn1 = \app\models\DataKepuasanPengunjung::find()->where("YEAR(TANGGAL)='2017' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$varTidakPuasThn2  = \app\models\DataKepuasanPengunjung::find()->where("YEAR(TANGGAL)='2018' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$varTidakPuasThn3  = \app\models\DataKepuasanPengunjung::find()->where("YEAR(TANGGAL)='2019' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$varTidakPuasThn4  = \app\models\DataKepuasanPengunjung::find()->where("YEAR(TANGGAL)='2020' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$varTidakPuasThn5  = \app\models\DataKepuasanPengunjung::find()->where("YEAR(TANGGAL)='2021' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
            
$varPuasThn1 = \app\models\DataKepuasanPengunjung::find()->where("YEAR(TANGGAL)='2017' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$varPuasThn2 = \app\models\DataKepuasanPengunjung::find()->where("YEAR(TANGGAL)='2018' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$varPuasThn3 = \app\models\DataKepuasanPengunjung::find()->where("YEAR(TANGGAL)='2019' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$varPuasThn4 = \app\models\DataKepuasanPengunjung::find()->where("YEAR(TANGGAL)='2020' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$varPuasThn5 = \app\models\DataKepuasanPengunjung::find()->where("YEAR(TANGGAL)='2021' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
            
$varSangatPuasThn1 = \app\models\DataKepuasanPengunjung::find()->where("year(TANGGAL)='2017' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$varSangatPuasThn2 = \app\models\DataKepuasanPengunjung::find()->where("year(TANGGAL)='2018' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$varSangatPuasThn3 = \app\models\DataKepuasanPengunjung::find()->where("year(TANGGAL)='2019' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$varSangatPuasThn4 = \app\models\DataKepuasanPengunjung::find()->where("year(TANGGAL)='2020' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$varSangatPuasThn5 = \app\models\DataKepuasanPengunjung::find()->where("year(TANGGAL)='2021' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
      

//bulanan
$varTidakPuasBul1 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='1' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$varTidakPuasBul2  = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='2' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$varTidakPuasBul3  = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='3' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$varTidakPuasBul4  = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='4' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$varTidakPuasBul5  = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='5' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$varTidakPuasBul6  = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='6' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$varTidakPuasBul7  = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='7' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$varTidakPuasBul8  = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='8' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$varTidakPuasBul9  = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='9' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$varTidakPuasBul10  = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='10' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$varTidakPuasBul11  = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='11' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$varTidakPuasBul12  = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='12' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
            
$varPuasBul1 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='1' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$varPuasBul2 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='2' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$varPuasBul3 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='3' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$varPuasBul4 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='4' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$varPuasBul5 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='5' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$varPuasBul6 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='6' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$varPuasBul7 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='7' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$varPuasBul8 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='8' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$varPuasBul9 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='9' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$varPuasBul10 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='10' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$varPuasBul11 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='11' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$varPuasBul12 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='12' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
            
$varSangatPuasBul1 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='1' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$varSangatPuasBul2 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='2' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$varSangatPuasBul3 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='3' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$varSangatPuasBul4 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='4' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$varSangatPuasBul5 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='5' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$varSangatPuasBul6 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='6' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$varSangatPuasBul7 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='7' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$varSangatPuasBul8 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='8' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$varSangatPuasBul9 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='9' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$varSangatPuasBul10 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='10' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$varSangatPuasBul11 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='11' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$varSangatPuasBul12 = \app\models\DataKepuasanPengunjung::find()->where("month(TANGGAL)='12' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
            
//minggu
$valTidakPuasMg1 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='1' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg2 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='2' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg3 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='3' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg4 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='4' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg5 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='5' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg6 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='6' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg7 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='7' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg8 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='8' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg9 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='9' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg10 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='10' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg11 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='11' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg12 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='12' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg13 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='13' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg14 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='14' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg15 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='15' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg16 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='16' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg17 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='17' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg18 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='18' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg19 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='19' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg20 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='20' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg21 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='21' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg22 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='22' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg23 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='23' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg24 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='24' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg25 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='25' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg26 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='26' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg27 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='27' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg28 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='28' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg29 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='29' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg30 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='30' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg31 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='31' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg32 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='32' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg33 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='33' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg34 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='34' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg35 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='35' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg36 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='36' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg37 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='37' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg38 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='38' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg39 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='39' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg40 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='40' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg41 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='41' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg42 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='42' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg43 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='43' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg44 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='44' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg45 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='45' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg46 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='46' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg47 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='47' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg48 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='48' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg49 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='49' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg50 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='50' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg51 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='51' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');
$valTidakPuasMg52 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='52' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Tidak Puas'")->count('RATING');

$valPuasMg1 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='1' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg2 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='2' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg3 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='3' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg4 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='4' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg5 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='5' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg6 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='6' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg7 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='7' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg8 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='8' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg9 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='9' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg10 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='10' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg11 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='11' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg12 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='12' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg13 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='13' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg14 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='14' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg15 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='15' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg16 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='16' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg17 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='17' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg18 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='18' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg19 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='19' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg20 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='20' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg21 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='21' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg22 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='22' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg23 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='23' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg24 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='24' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg25 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='25' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg26 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='26' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg27 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='27' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg28 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='28' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg29 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='29' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg30 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='30' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg31 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='31' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg32 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='32' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg33 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='33' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg34 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='34' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg35 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='35' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg36 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='36' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg37 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='37' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg38 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='38' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg39 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='39' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg40 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='40' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg41 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='41' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg42 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='42' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg43 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='43' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg44 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='44' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg45 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='45' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg46 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='46' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg47 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='47' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg48 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='48' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg49 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='49' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg50 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='50' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg51 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='51' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');
$valPuasMg52 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='52' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Puas'")->count('RATING');

$valSangatPuasMg1 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='1' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg2 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='2' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg3 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='3' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg4 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='4' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg5 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='5' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg6 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='6' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg7 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='7' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg8 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='8' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg9 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='9' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg10 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='10' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg11 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='11' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg12 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='12' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg13 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='13' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg14 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='14' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg15 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='15' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg16 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='16' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg17 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='17' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg18 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='18' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg19 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='19' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg20 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='20' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg21 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='21' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg22 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='22' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg23 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='23' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg24 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='24' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg25 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='25' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg26 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='26' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg27 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='27' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg28 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='28' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg29 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='29' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg30 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='30' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg31 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='31' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg32 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='32' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg33 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='33' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg34 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='34' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg35 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='35' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg36 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='36' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg37 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='37' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg38 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='38' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg39 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='39' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg40 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='40' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg41 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='41' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg42 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='42' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg43 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='43' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg44 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='44' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg45 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='45' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg46 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='46' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg47 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='47' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg48 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='48' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg49 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='49' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg50 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='50' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg51 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='51' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');
$valSangatPuasMg52 = \app\models\DataKepuasanPengunjung::find()->where("WEEKOFYEAR(TANGGAL)='52' and YEAR(TANGGAL) = '".date("Y")."' and LOKET = '".$loket."' and RATING = 'Sangat Puas'")->count('RATING');

   
?>
<?php $form = kartik\widgets\ActiveForm::begin([
    'id' => 'form-statistik-loket', 
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'formConfig' => ['labelSpan' => 2,'showErrors' => true, 'deviceSize' => ActiveForm::SIZE_LARGE],
    'options'=>['enctype'=>'multipart/form-data'],
    'action' => 'index.php?r=data-kepuasan-pengunjung/btnklik&loket='.$loket,
]); ?>
<div class="data-kepuasan-pengunjung-view">

    <div class="row dashboard">
        <div class="col-lg-12">
            <p>
                <div class="panel panel-default">
                    <div class="panel-body">   
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <?= $form->field($model, 'Tahun', ['options' => ['class'=>'col-md-4']])->dropDownList(['2017'=>'2017','2018'=>'2018','2019'=>'2019','2020'=>'2020','2021'=>'2021'], ['prompt' => ' -- Select --']) ?>
                                <?= Html::submitButton('<i class="glyphicon glyphicon-list"></i> Data Tabular', ['class' => 'btn btn-success', 'name'=>'btnGrafTahun']) ?>
                            </div>
                        </div>
                        <?=  Highcharts::widget([
                            'options' => [
                                'chart' =>[
                                    'type' => 'area',
                                ],
                               'title' => ['text' => 'Grafik Tahunan'],
                               'xAxis' => [
                                  'categories' => ['2017', '2018', '2019', '2020', '2021']
                               ],
                               'yAxis' => [
                                  'title' => ['text' => 'Jumlah']
                               ],
                               'series' => [
                                  ['name' => 'Tidak Puas', 'data' => [floatval($varTidakPuasThn1), floatval($varTidakPuasThn2), floatval($varTidakPuasThn3), floatval($varTidakPuasThn4), floatval($varTidakPuasThn5)]],
                                  ['name' => 'Puas', 'data' => [floatval($varPuasThn1), floatval($varPuasThn2), floatval($varPuasThn3), floatval($varPuasThn4), floatval($varPuasThn5)]],
                                  ['name' => 'Sangat Puas', 'data' => [floatval($varSangatPuasThn1), floatval($varSangatPuasThn2), floatval($varSangatPuasThn3), floatval($varSangatPuasThn4), floatval($varSangatPuasThn5)]],
                               ]
                            ]
                         ]);
                        ?>
                    </div>
                </div>
            </p>            
        </div>
        <div class="col-lg-12">
            <p>
                <div class="panel panel-default">
                    <div class="panel-body">   
                        <div class="panel panel-default">
                            <div class="panel-body">                        
                                <?= $form->field($model, 'Tahun', ['options' => ['class'=>'col-md-4']])->dropDownList(['2017'=>'2017','2018'=>'2018','2019'=>'2019','2020'=>'2020','2021'=>'2021'], ['prompt' => ' -- Select --']) ?>
                                <?= $form->field($model, 'Bulan', ['options' => ['class'=>'col-md-4']])->dropDownList([
                                        '1'=>'Januari',
                                        '2'=>'Februari',
                                        '3'=>'Maret',
                                        '4'=>'April',
                                        '5'=>'Mei',
                                        '6'=>'Juni',
                                        '7'=>'Juli',
                                        '8'=>'Agustus',
                                        '9'=>'September',
                                        '10'=>'Oktober',
                                        '11'=>'November',
                                        '12'=>'Desember',
                                    ], ['prompt' => ' -- Select --']) ?>
                                <?= Html::submitButton('<i class="glyphicon glyphicon-list"></i> Data Tabular', ['class' => 'btn btn-success', 'name'=>'btnGrafBulan']) ?>
                            </div>
                        </div>
                        <?=  Highcharts::widget([
                            'options' => [
                                'chart' =>[
                                    'type' => 'area',
                                ],
                               'title' => ['text' => 'Grafik Bulanan'],
                               'xAxis' => [
                                  'categories' => ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
                               ],
                               'yAxis' => [
                                  'title' => ['text' => 'Jumlah']
                               ],
                               'series' => [
                                  ['name' => 'Tidak Puas', 'data' => [floatval($varTidakPuasBul1), floatval($varTidakPuasBul2), floatval($varTidakPuasBul3), floatval($varTidakPuasBul4), floatval($varTidakPuasBul5), floatval($varTidakPuasBul6), floatval($varTidakPuasBul7), floatval($varTidakPuasBul8), floatval($varTidakPuasBul9), floatval($varTidakPuasBul10), floatval($varTidakPuasBul11), floatval($varTidakPuasBul12)]],
                                  ['name' => 'Puas', 'data' => [floatval($varPuasBul1), floatval($varPuasBul2), floatval($varPuasBul3), floatval($varPuasBul4), floatval($varPuasBul5), floatval($varPuasBul6), floatval($varPuasBul7), floatval($varPuasBul8), floatval($varPuasBul9), floatval($varPuasBul10), floatval($varPuasBul11), floatval($varPuasBul12)]],
                                  ['name' => 'Sangat Puas', 'data' => [floatval($varSangatPuasBul1), floatval($varSangatPuasBul2), floatval($varSangatPuasBul3), floatval($varSangatPuasBul4), floatval($varSangatPuasBul5), floatval($varSangatPuasBul6), floatval($varSangatPuasBul7), floatval($varSangatPuasBul8), floatval($varSangatPuasBul9), floatval($varSangatPuasBul10), floatval($varSangatPuasBul11), floatval($varSangatPuasBul12)]],
                               ]
                            ]
                         ]);
                        ?>
                    </div>
                </div>
            </p>            
        </div>
        <div class="col-lg-12">
            <p>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <?= $form->field($model, 'Tahun', ['options' => ['class'=>'col-md-4']])->dropDownList(['2017'=>'2017','2018'=>'2018','2019'=>'2019','2020'=>'2020','2021'=>'2021'], ['prompt' => ' -- Select --']) ?>
                                <?= $form->field($model, 'Minggu', ['options' => ['class'=>'col-md-4']])->dropDownList([
                                        '1'=>'1',
                                        '2'=>'2',
                                        '3'=>'3',
                                        '4'=>'4',
                                        '5'=>'5',
                                        '6'=>'6',
                                        '7'=>'7',
                                        '8'=>'8',
                                        '9'=>'9',
                                        '10'=>'10',
                                        '11'=>'11',
                                        '12'=>'12',
                                        '13'=>'13',
                                        '14'=>'14',
                                        '15'=>'15',
                                        '16'=>'16',
                                        '17'=>'17',
                                        '18'=>'18',
                                        '19'=>'19',
                                        '20'=>'20',
                                        '21'=>'21',
                                        '22'=>'22',
                                        '23'=>'23',
                                        '24'=>'24',
                                        '25'=>'25',
                                        '26'=>'26',
                                        '27'=>'27',
                                        '28'=>'28',
                                        '29'=>'29',
                                        '30'=>'30',
                                        '31'=>'31',
                                        '32'=>'32',
                                        '33'=>'33',
                                        '34'=>'34',
                                        '35'=>'35',
                                        '36'=>'36',
                                        '37'=>'37',
                                        '38'=>'38',
                                        '39'=>'39',
                                        '40'=>'40',
                                        '41'=>'41',
                                        '42'=>'42',
                                        '43'=>'43',
                                        '44'=>'44',
                                        '45'=>'45',
                                        '46'=>'46',
                                        '47'=>'47',
                                        '48'=>'48',
                                        '49'=>'49',
                                        '50'=>'50',
                                        '51'=>'51',
                                        '52'=>'52',

                                    ], ['prompt' => ' -- Select --']) ?>
                                <?= Html::submitButton('<i class="glyphicon glyphicon-list"></i> Data Tabular', ['class' => 'btn btn-success', 'name'=>'btnGrafMinggu']) ?>
                            </div>
                        </div>
                        <?=  Highcharts::widget([
                            'options' => [
                                'chart' =>[
                                    'type' => 'area',
                                ],
                               'title' => ['text' => 'Grafik Mingguan'],
                               'xAxis' => [
                                  'categories' => ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','41','42','43','44','45','46','47','48','49','50','51','52']
                               ],
                               'yAxis' => [
                                  'title' => ['text' => 'Jumlah']
                               ],
                               'series' => [
                                  ['name' => 'Tidak Puas', 'data' => [floatval($valTidakPuasMg1), floatval($valTidakPuasMg2), floatval($valTidakPuasMg3), floatval($valTidakPuasMg4), floatval($valTidakPuasMg5), floatval($valTidakPuasMg6), floatval($valTidakPuasMg7), floatval($valTidakPuasMg8), floatval($valTidakPuasMg9), floatval($valTidakPuasMg10), floatval($valTidakPuasMg11), floatval($valTidakPuasMg12), floatval($valTidakPuasMg13), floatval($valTidakPuasMg14), floatval($valTidakPuasMg15), floatval($valTidakPuasMg16), floatval($valTidakPuasMg17), floatval($valTidakPuasMg18), floatval($valTidakPuasMg19), floatval($valTidakPuasMg20), floatval($valTidakPuasMg21), floatval($valTidakPuasMg22), floatval($valTidakPuasMg23), floatval($valTidakPuasMg24), floatval($valTidakPuasMg25), floatval($valTidakPuasMg26), floatval($valTidakPuasMg27), floatval($valTidakPuasMg28), floatval($valTidakPuasMg29), floatval($valTidakPuasMg30), floatval($valTidakPuasMg31), floatval($valTidakPuasMg32), floatval($valTidakPuasMg33), floatval($valTidakPuasMg34), floatval($valTidakPuasMg35), floatval($valTidakPuasMg36), floatval($valTidakPuasMg37), floatval($valTidakPuasMg38), floatval($valTidakPuasMg39), floatval($valTidakPuasMg40), floatval($valTidakPuasMg41), floatval($valTidakPuasMg42), floatval($valTidakPuasMg43), floatval($valTidakPuasMg44), floatval($valTidakPuasMg45), floatval($valTidakPuasMg46), floatval($valTidakPuasMg47), floatval($valTidakPuasMg48), floatval($valTidakPuasMg49), floatval($valTidakPuasMg50), floatval($valTidakPuasMg51), floatval($valTidakPuasMg52)]],
                                  ['name' => 'Puas', 'data' => [floatval($valPuasMg1), floatval($valPuasMg2), floatval($valPuasMg3), floatval($valPuasMg4), floatval($valPuasMg5), floatval($valPuasMg6), floatval($valPuasMg7), floatval($valPuasMg8), floatval($valPuasMg9), floatval($valPuasMg10), floatval($valPuasMg11), floatval($valPuasMg12), floatval($valPuasMg13), floatval($valPuasMg14), floatval($valPuasMg15), floatval($valPuasMg16), floatval($valPuasMg17), floatval($valPuasMg18), floatval($valPuasMg19), floatval($valPuasMg20), floatval($valPuasMg21), floatval($valPuasMg22), floatval($valPuasMg23), floatval($valPuasMg24), floatval($valPuasMg25), floatval($valPuasMg26), floatval($valPuasMg27), floatval($valPuasMg28), floatval($valPuasMg29), floatval($valPuasMg30), floatval($valPuasMg31), floatval($valPuasMg32), floatval($valPuasMg33), floatval($valPuasMg34), floatval($valPuasMg35), floatval($valPuasMg36), floatval($valPuasMg37), floatval($valPuasMg38), floatval($valPuasMg39), floatval($valPuasMg40), floatval($valPuasMg41), floatval($valPuasMg42), floatval($valPuasMg43), floatval($valPuasMg44), floatval($valPuasMg45), floatval($valPuasMg46), floatval($valPuasMg47), floatval($valPuasMg48), floatval($valPuasMg49), floatval($valPuasMg50), floatval($valPuasMg51), floatval($valPuasMg52)]],
                                  ['name' => 'Sangat Puas', 'data' => [floatval($valSangatPuasMg1), floatval($valSangatPuasMg2), floatval($valSangatPuasMg3), floatval($valSangatPuasMg4), floatval($valSangatPuasMg5), floatval($valSangatPuasMg6), floatval($valSangatPuasMg7), floatval($valSangatPuasMg8), floatval($valSangatPuasMg9), floatval($valSangatPuasMg10), floatval($valSangatPuasMg11), floatval($valSangatPuasMg12), floatval($valSangatPuasMg13), floatval($valSangatPuasMg14), floatval($valSangatPuasMg15), floatval($valSangatPuasMg16), floatval($valSangatPuasMg17), floatval($valSangatPuasMg18), floatval($valSangatPuasMg19), floatval($valSangatPuasMg20), floatval($valSangatPuasMg21), floatval($valSangatPuasMg22), floatval($valSangatPuasMg23), floatval($valSangatPuasMg24), floatval($valSangatPuasMg25), floatval($valSangatPuasMg26), floatval($valSangatPuasMg27), floatval($valSangatPuasMg28), floatval($valSangatPuasMg29), floatval($valSangatPuasMg30), floatval($valSangatPuasMg31), floatval($valSangatPuasMg32), floatval($valSangatPuasMg33), floatval($valSangatPuasMg34), floatval($valSangatPuasMg35), floatval($valSangatPuasMg36), floatval($valSangatPuasMg37), floatval($valSangatPuasMg38), floatval($valSangatPuasMg39), floatval($valSangatPuasMg40), floatval($valSangatPuasMg41), floatval($valSangatPuasMg42), floatval($valSangatPuasMg43), floatval($valSangatPuasMg44), floatval($valSangatPuasMg45), floatval($valSangatPuasMg46), floatval($valSangatPuasMg47), floatval($valSangatPuasMg48), floatval($valSangatPuasMg49), floatval($valSangatPuasMg50), floatval($valSangatPuasMg51), floatval($valSangatPuasMg52)]],
                               ]
                            ]
                         ]);
                        ?>
                    </div>
                </div>
            </p>
        </div>
        <div class="col-lg-12">
            
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>