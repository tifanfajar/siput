<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DataKepuasanPengunjungSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-kepuasan-pengunjung-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID_DATA_KEPUASAN_PENGUNJUNG') ?>

    <?= $form->field($model, 'LOKET') ?>

    <?= $form->field($model, 'TANGGAL') ?>

    <?= $form->field($model, 'NO_URUT') ?>

    <?= $form->field($model, 'RATING') ?>

    <?php // echo $form->field($model, 'PATH_GAMBAR') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
