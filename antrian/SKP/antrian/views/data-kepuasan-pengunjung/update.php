<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DataKepuasanPengunjung */

$this->title = 'Update Data Kepuasan Pengunjung: ' . $model->ID_DATA_KEPUASAN_PENGUNJUNG;
$this->params['breadcrumbs'][] = ['label' => 'Data Kepuasan Pengunjungs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_DATA_KEPUASAN_PENGUNJUNG, 'url' => ['view', 'id' => $model->ID_DATA_KEPUASAN_PENGUNJUNG]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="data-kepuasan-pengunjung-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
