<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DataKepuasanPengunjung */

?>
<div class="data-kepuasan-pengunjung-create">

    <?= $this->render('kepuasanPengunjung', [
        'loket' => $loket,
    ]) ?>

</div>
