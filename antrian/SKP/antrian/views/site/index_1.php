<?php

/* @var $this yii\web\View */


//use miloschuman\highcharts\Highcharts;
//use yii\web\JsExpression;

$this->title = ' ';
//berita
$berita = '';
$header = \app\models\DataRunningText::find()
          ->select('TEXT')
          ->where("STATUS = 'Aktif';")                
          ->all();

foreach ($header as $value):
    $berita = $berita.$value->TEXT." - ";
endforeach;

//nama loket
$nmLoket1 = '';
$nmLoket2 = '';
$nmLoket3 = '';
$nmLoket4 = '';
$nmLoket5 = '';
$nmLoket6 = '';
$nmLoket7 = '';
$nmLoket8 = '';
$nmLoket9 = '';
$nmLoket10 = '';

$berita1 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 1") ->all();
foreach ($berita1 as $value): $nmLoket1 = $value->NAMA_LOKET; endforeach;

$berita2 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 2") ->all();
foreach ($berita2 as $value): $nmLoket2 = $value->NAMA_LOKET; endforeach;

$berita3 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 3") ->all();
foreach ($berita3 as $value): $nmLoket3 = $value->NAMA_LOKET; endforeach;

$berita4 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 4") ->all();
foreach ($berita4 as $value): $nmLoket4 = $value->NAMA_LOKET; endforeach;

$berita5 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 5") ->all();
foreach ($berita5 as $value): $nmLoket5 = $value->NAMA_LOKET; endforeach;

$berita6 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 6") ->all();
foreach ($berita6 as $value): $nmLoket6 = $value->NAMA_LOKET; endforeach;

$berita7 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 7") ->all();
foreach ($berita7 as $value): $nmLoket7 = $value->NAMA_LOKET; endforeach;

$berita8 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 8") ->all();
foreach ($berita8 as $value): $nmLoket8 = $value->NAMA_LOKET; endforeach;

$berita9 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 9") ->all();
foreach ($berita9 as $value): $nmLoket9 = $value->NAMA_LOKET; endforeach;

$berita10 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 10") ->all();
foreach ($berita10 as $value): $nmLoket10 = $value->NAMA_LOKET; endforeach;

//jml antrian
$jmlAntrianLkt1 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 1) ")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianLkt2 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 2) ")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianLkt3 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 3) ")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianLkt4 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 4) ")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianLkt5 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 5) ")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianLkt6 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 6) ")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianLkt7 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 7) ")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianLkt8 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 8) ")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianLkt9 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 9) ")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianLkt10 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 10) ")            
->count('ID_DATA_ANTRIAN');

//sisa jml antrian
$jmlAntrianSisaLkt1 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 1) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianSisaLkt2 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 2) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianSisaLkt3 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 3) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianSisaLkt4 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 4) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianSisaLkt5 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 5) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianSisaLkt6 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 6) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianSisaLkt7 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 7) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianSisaLkt8 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 8) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianSisaLkt9 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 9) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianSisaLkt10 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 10) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');


//antrian saat ini
$tempAntrianSaatIniLkt1 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '1;%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
$tempAntrianSaatIniLkt2 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '2;%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
$tempAntrianSaatIniLkt3 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '3;%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
$tempAntrianSaatIniLkt4 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '4;%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
$tempAntrianSaatIniLkt5 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '5;%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
$tempAntrianSaatIniLkt6 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '6;%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
$tempAntrianSaatIniLkt7 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '7;%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
$tempAntrianSaatIniLkt8 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '8;%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
$tempAntrianSaatIniLkt9 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '9;%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
$tempAntrianSaatIniLkt10 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '10;%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
//print_r($tempAntrianSaatIniLkt1['PANGILAN']);die;
$a1 = explode(";", $tempAntrianSaatIniLkt1['PANGILAN']); $AntrianSaatIniLkt1 = $a1[1];
$a2 = explode(";", $tempAntrianSaatIniLkt2['PANGILAN']); $AntrianSaatIniLkt2 = $a2[1];
$a3 = explode(";", $tempAntrianSaatIniLkt3['PANGILAN']); $AntrianSaatIniLkt3 = $a3[1];
$a4 = explode(";", $tempAntrianSaatIniLkt4['PANGILAN']); $AntrianSaatIniLkt4 = $a4[1];
$a5 = explode(";", $tempAntrianSaatIniLkt5['PANGILAN']); $AntrianSaatIniLkt5 = $a5[1];
$a6 = explode(";", $tempAntrianSaatIniLkt6['PANGILAN']); $AntrianSaatIniLkt6 = $a6[1];
$a7 = explode(";", $tempAntrianSaatIniLkt7['PANGILAN']); $AntrianSaatIniLkt7 = $a7[1];
$a8 = explode(";", $tempAntrianSaatIniLkt8['PANGILAN']); $AntrianSaatIniLkt8 = $a8[1];
$a9 = explode(";", $tempAntrianSaatIniLkt9['PANGILAN']); $AntrianSaatIniLkt9 = $a9[1];
$a10 = explode(";", $tempAntrianSaatIniLkt10['PANGILAN']); $AntrianSaatIniLkt10 = $a10[1];

?>
<!--<div class="site-index">
    <div class="jumbotron">
        <h3></h3>        
    </div>
</div>-->
<!DOCTYPE html>
<html>
<head>
	<title>KEMKOMINFO</title>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/antrian/images/plasma/css/custom.css">
</head>
<body>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-4 left">
      <img src="/antrian/images/plasma/logo-left.png">
      <!--<i class="fa fa-moon-o wtr"></i>-->
      <h1><?php echo date("G:i") ?></h1>
      <h3 style="margin-top:20px;">JAKARTA</h3>
      <h4><?php echo date("d F Y") ?></h4>

        <table class="table table-bordered">
          <thead>
            <tr>
              <th>Country</th>
              <th>Sell</th>
              <th>Buy</th>
            </tr>
            <tr>
              <td>USD <i class="fa fa-sort-down crs"</td>
              <td>13.510</td> 
              <td>13.510</td>
            </tr>
            <tr>
              <td>USD <i class="fa fa-sort-up crs"</td>
              <td>13.510</td>
              <td>13.510</td>
            </tr>
            <tr>
              <td>USD <i class="fa fa-sort-down crs"</td>
              <td>13.510</td>
              <td>13.510</td>
            </tr>
          </thead>
        </table>

        <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/IxtMRPMFEYc?&autoplay=1&mute=1&showinfo=0&controls=0&loop=1&playlist=IxtMRPMFEYc"></iframe>
        </div>

    </div>
    <div class="col-md-8 right">
      <!--   News     -->
      <!-- <div class="news-box">
        <div class="row">
          <div class="col-md-6">asdadadadada</div>
          <div class="col-md-6">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/IxtMRPMFEYc?&autoplay=1&mute=1&showinfo=0&controls=0&loop=1&playlist=IxtMRPMFEYc"></iframe>
            </div>
          </div>
        </div>
      </div> -->
      <!--    List    -->
      <div class="list-box">

          <div class="col-md-6">
            
            <div class="cnt-antrian">
              <h4>LOKET 1</h4>
              <h5><?php echo $nmLoket1 ?></h5>
              <div class="table cc">
                <table class="table table-condensed" style="margin-top:0px; margin-bottom:0px;">
                  <thead>
                    <tr>
                      <th>Antrian</th>
                      <th>Total</th>
                      <th>Sisa</th>
                    </tr>
                  </thead>
                  <tr>
                    <td><?php if(strlen($AntrianSaatIniLkt1)==1){echo '00'.$AntrianSaatIniLkt1;}elseif (strlen($AntrianSaatIniLkt1)==2){echo '0'.$AntrianSaatIniLkt1;}else{echo $AntrianSaatIniLkt1;} ?></td>
                    <td><?php if(strlen($jmlAntrianLkt1)==1){echo '00'.$jmlAntrianLkt1;}elseif (strlen($jmlAntrianLkt1)==2){echo '0'.$jmlAntrianLkt1;}else{echo $jmlAntrianLkt1;} ?></td>
                    <td><?php if(strlen($jmlAntrianSisaLkt1)==1){echo '00'.$jmlAntrianSisaLkt1;}elseif (strlen($jmlAntrianSisaLkt1)==2){echo '0'.$jmlAntrianSisaLkt1;}else{echo $jmlAntrianSisaLkt1;} ?></td>
                  </tr>
                </table>
              </div>
              
              
              
            </div>
            <div class="cnt-antrian">
              <h4>LOKET 2</h4>
              <h5><?php echo $nmLoket2 ?></h5>
              <div class="table cc">
                <table class="table table-condensed" style="margin-top:0px; margin-bottom:0px;">
                  <thead>
                    <tr>
                      <th>Antrian</th>
                      <th>Total</th>
                      <th>Sisa</th>
                    </tr>
                  </thead>
                  <tr>
                    <td><?php if(strlen($AntrianSaatIniLkt2)==1){echo '00'.$AntrianSaatIniLkt2;}elseif (strlen($AntrianSaatIniLkt2)==2){echo '0'.$AntrianSaatIniLkt2;}else{echo $AntrianSaatIniLkt2;} ?></td>
                    <td><?php if(strlen($jmlAntrianLkt2)==1){echo '00'.$jmlAntrianLkt2;}elseif (strlen($jmlAntrianLkt2)==2){echo '0'.$jmlAntrianLkt2;}else{echo $jmlAntrianLkt2;} ?></td>
                    <td><?php if(strlen($jmlAntrianSisaLkt2)==1){echo '00'.$jmlAntrianSisaLkt2;}elseif (strlen($jmlAntrianSisaLkt2)==2){echo '0'.$jmlAntrianSisaLkt2;}else{echo $jmlAntrianSisaLkt2;} ?></td>
                  </tr>
                </table>
              </div>
              
              
              
            </div>
            <div class="cnt-antrian">
              <h4>LOKET 3</h4>
              <h5><?php echo $nmLoket3 ?></h5>
              <div class="table cc">
                <table class="table table-condensed" style="margin-top:0px; margin-bottom:0px;">
                  <thead>
                    <tr>
                      <th>Antrian</th>
                      <th>Total</th>
                      <th>Sisa</th>
                    </tr>
                  </thead>
                  <tr>
                    <td><?php if(strlen($AntrianSaatIniLkt3)==1){echo '00'.$AntrianSaatIniLkt3;}elseif (strlen($AntrianSaatIniLkt3)==2){echo '0'.$AntrianSaatIniLkt3;}else{echo $AntrianSaatIniLkt3;} ?></td>
                    <td><?php if(strlen($jmlAntrianLkt3)==1){echo '00'.$jmlAntrianLkt3;}elseif (strlen($jmlAntrianLkt3)==2){echo '0'.$jmlAntrianLkt3;}else{echo $jmlAntrianLkt3;} ?></td>
                    <td><?php if(strlen($jmlAntrianSisaLkt3)==1){echo '00'.$jmlAntrianSisaLkt3;}elseif (strlen($jmlAntrianSisaLkt3)==2){echo '0'.$jmlAntrianSisaLkt3;}else{echo $jmlAntrianSisaLkt3;} ?></td>
                  </tr>
                </table>
              </div>
              
              
              
            </div>
            <div class="cnt-antrian">
              <h4>LOKET 4</h4>
              <h5><?php echo $nmLoket4 ?></h5>
              <div class="table cc">
                <table class="table table-condensed" style="margin-top:0px; margin-bottom:0px;">
                  <thead>
                    <tr>
                      <th>Antrian</th>
                      <th>Total</th>
                      <th>Sisa</th>
                    </tr>
                  </thead>
                  <tr>
                    <td><?php if(strlen($AntrianSaatIniLkt4)==1){echo '00'.$AntrianSaatIniLkt4;}elseif (strlen($AntrianSaatIniLkt4)==2){echo '0'.$AntrianSaatIniLkt4;}else{echo $AntrianSaatIniLkt4;} ?></td>
                    <td><?php if(strlen($jmlAntrianLkt4)==1){echo '00'.$jmlAntrianLkt4;}elseif (strlen($jmlAntrianLkt4)==2){echo '0'.$jmlAntrianLkt4;}else{echo $jmlAntrianLkt4;} ?></td>
                    <td><?php if(strlen($jmlAntrianSisaLkt4)==1){echo '00'.$jmlAntrianSisaLkt4;}elseif (strlen($jmlAntrianSisaLkt4)==2){echo '0'.$jmlAntrianSisaLkt4;}else{echo $jmlAntrianSisaLkt4;} ?></td>
                  </tr>
                </table>
              </div>
              
              
              
            </div>
            <div class="cnt-antrian">
              <h4>LOKET 5</h4>
              <h5><?php echo $nmLoket5 ?></h5>
              <div class="table cc">
                <table class="table table-condensed" style="margin-top:0px; margin-bottom:0px;">
                  <thead>
                    <tr>
                      <th>Antrian</th>
                      <th>Total</th>
                      <th>Sisa</th>
                    </tr>
                  </thead>
                  <tr>
                    <td><?php if(strlen($AntrianSaatIniLkt5)==1){echo '00'.$AntrianSaatIniLkt5;}elseif (strlen($AntrianSaatIniLkt5)==2){echo '0'.$AntrianSaatIniLkt5;}else{echo $AntrianSaatIniLkt5;} ?></td>
                    <td><?php if(strlen($jmlAntrianLkt5)==1){echo '00'.$jmlAntrianLkt5;}elseif (strlen($jmlAntrianLkt5)==2){echo '0'.$jmlAntrianLkt5;}else{echo $jmlAntrianLkt5;} ?></td>
                    <td><?php if(strlen($jmlAntrianSisaLkt5)==1){echo '00'.$jmlAntrianSisaLkt5;}elseif (strlen($jmlAntrianSisaLkt5)==2){echo '0'.$jmlAntrianSisaLkt5;}else{echo $jmlAntrianSisaLkt5;} ?></td>
                  </tr>
                </table>
              </div>
              
              
              
            </div>
          </div>
          <div class="col-md-6">
          	
          	<div class="cnt-antrian">
              <h4>LOKET 6</h4>
              <h5><?php echo $nmLoket6 ?></h5>
              <div class="table cc">
                <table class="table table-condensed" style="margin-top:0px; margin-bottom:0px;">
                  <thead>
                    <tr>
                      <th>Antrian</th>
                      <th>Total</th>
                      <th>Sisa</th>
                    </tr>
                  </thead>
                  <tr>
                    <td><?php if(strlen($AntrianSaatIniLkt6)==1){echo '00'.$AntrianSaatIniLkt6;}elseif (strlen($AntrianSaatIniLkt6)==2){echo '0'.$AntrianSaatIniLkt6;}else{echo $AntrianSaatIniLkt6;} ?></td>
                    <td><?php if(strlen($jmlAntrianLkt6)==1){echo '00'.$jmlAntrianLkt6;}elseif (strlen($jmlAntrianLkt6)==2){echo '0'.$jmlAntrianLkt6;}else{echo $jmlAntrianLkt6;} ?></td>
                    <td><?php if(strlen($jmlAntrianSisaLkt6)==1){echo '00'.$jmlAntrianSisaLkt6;}elseif (strlen($jmlAntrianSisaLkt6)==2){echo '0'.$jmlAntrianSisaLkt6;}else{echo $jmlAntrianSisaLkt6;} ?></td>
                  </tr>
                </table>
              </div>
          	</div>

          	<div class="cnt-antrian">
              <h4>LOKET 7</h4>
              <h5><?php echo $nmLoket7 ?></h5>
              <div class="table cc">
                <table class="table table-condensed" style="margin-top:0px; margin-bottom:0px;">
                  <thead>
                    <tr>
                      <th>Antrian</th>
                      <th>Total</th>
                      <th>Sisa</th>
                    </tr>
                  </thead>
                  <tr>
                    <td><?php if(strlen($AntrianSaatIniLkt7)==1){echo '00'.$AntrianSaatIniLkt7;}elseif (strlen($AntrianSaatIniLkt7)==2){echo '0'.$AntrianSaatIniLkt7;}else{echo $AntrianSaatIniLkt7;} ?></td>
                    <td><?php if(strlen($jmlAntrianLkt7)==1){echo '00'.$jmlAntrianLkt7;}elseif (strlen($jmlAntrianLkt7)==2){echo '0'.$jmlAntrianLkt7;}else{echo $jmlAntrianLkt7;} ?></td>
                    <td><?php if(strlen($jmlAntrianSisaLkt7)==1){echo '00'.$jmlAntrianSisaLkt7;}elseif (strlen($jmlAntrianSisaLkt7)==2){echo '0'.$jmlAntrianSisaLkt7;}else{echo $jmlAntrianSisaLkt7;} ?></td>
                  </tr>
                </table>
              </div>
          	</div>

          	<div class="cnt-antrian">
              <h4>LOKET 8</h4>
              <h5><?php echo $nmLoket8 ?></h5>
              <div class="table cc">
                <table class="table table-condensed" style="margin-top:0px; margin-bottom:0px;">
                  <thead>
                    <tr>
                      <th>Antrian</th>
                      <th>Total</th>
                      <th>Sisa</th>
                    </tr>
                  </thead>
                  <tr>
                    <td><?php if(strlen($AntrianSaatIniLkt8)==1){echo '00'.$AntrianSaatIniLkt8;}elseif (strlen($AntrianSaatIniLkt8)==2){echo '0'.$AntrianSaatIniLkt8;}else{echo $AntrianSaatIniLkt8;} ?></td>
                    <td><?php if(strlen($jmlAntrianLkt8)==1){echo '00'.$jmlAntrianLkt8;}elseif (strlen($jmlAntrianLkt8)==2){echo '0'.$jmlAntrianLkt8;}else{echo $jmlAntrianLkt8;} ?></td>
                    <td><?php if(strlen($jmlAntrianSisaLkt8)==1){echo '00'.$jmlAntrianSisaLkt8;}elseif (strlen($jmlAntrianSisaLkt8)==2){echo '0'.$jmlAntrianSisaLkt8;}else{echo $jmlAntrianSisaLkt8;} ?></td>
                  </tr>
                </table>
              </div>
          	</div>

          	<div class="cnt-antrian">
              <h4>LOKET 9</h4>
              <h5><?php echo $nmLoket9 ?></h5>
              <div class="table cc">
                <table class="table table-condensed" style="margin-top:0px; margin-bottom:0px;">
                  <thead>
                    <tr>
                      <th>Antrian</th>
                      <th>Total</th>
                      <th>Sisa</th>
                    </tr>
                  </thead>
                  <tr>
                    <td><?php if(strlen($AntrianSaatIniLkt9)==1){echo '00'.$AntrianSaatIniLkt9;}elseif (strlen($AntrianSaatIniLkt9)==2){echo '0'.$AntrianSaatIniLkt9;}else{echo $AntrianSaatIniLkt9;} ?></td>
                    <td><?php if(strlen($jmlAntrianLkt9)==1){echo '00'.$jmlAntrianLkt9;}elseif (strlen($jmlAntrianLkt9)==2){echo '0'.$jmlAntrianLkt9;}else{echo $jmlAntrianLkt9;} ?></td>
                    <td><?php if(strlen($jmlAntrianSisaLkt9)==1){echo '00'.$jmlAntrianSisaLkt9;}elseif (strlen($jmlAntrianSisaLkt9)==2){echo '0'.$jmlAntrianSisaLkt9;}else{echo $jmlAntrianSisaLkt9;} ?></td>                    
                  </tr>
                </table>
              </div>
          	</div>

          	<div class="cnt-antrian">
              <h4>LOKET 10</h4>
              <h5><?php echo $nmLoket10 ?></h5>
              <div class="table cc">
                <table class="table table-condensed" style="margin-top:0px; margin-bottom:0px;">
                  <thead>
                    <tr>
                      <th>Antrian</th>
                      <th>Total</th>
                      <th>Sisa</th>
                    </tr>
                  </thead>
                  <tr>
                    <td><?php if(strlen($AntrianSaatIniLkt10)==1){echo '00'.$AntrianSaatIniLkt10;}elseif (strlen($AntrianSaatIniLkt10)==2){echo '0'.$AntrianSaatIniLkt10;}else{echo $AntrianSaatIniLkt10;} ?></td>
                    <td><?php if(strlen($jmlAntrianLkt10)==1){echo '00'.$jmlAntrianLkt10;}elseif (strlen($jmlAntrianLkt10)==2){echo '0'.$jmlAntrianLkt10;}else{echo $jmlAntrianLkt10;} ?></td>
                    <td><?php if(strlen($jmlAntrianSisaLkt10)==1){echo '00'.$jmlAntrianSisaLkt10;}elseif (strlen($jmlAntrianSisaLkt10)==2){echo '0'.$jmlAntrianSisaLkt10;}else{echo $jmlAntrianSisaLkt10;} ?></td>                    
                  </tr>
                </table>
              </div>
          	</div>

      </div>
      <div class="col-md-12" style="padding: 0px;">
      	<marquee behavior="scroll" direction="left"><?php echo $berita ?></marquee>
      </div>
    </div>
  </div>
</div>


<script url="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script url="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
