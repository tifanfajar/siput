<?php

/* @var $this yii\web\View */


//use miloschuman\highcharts\Highcharts;
//use yii\web\JsExpression;

$this->title = ' ';
//berita
$berita = '';
$header = \app\models\DataRunningText::find()
          ->select('TEXT')
          ->where("STATUS = 'Aktif';")                
          ->all();

foreach ($header as $value):
    $berita = $berita.$value->TEXT." - ";
endforeach;

//nama loket
$nmLoket1 = '';
$nmLoket2 = '';
$nmLoket3 = '';
$nmLoket4 = '';
$nmLoket5 = '';
$nmLoket6 = '';
$nmLoket7 = '';
$nmLoket8 = '';
$nmLoket9 = '';
$nmLoket10 = '';

$berita1 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 1") ->all();
foreach ($berita1 as $value): $nmLoket1 = $value->NAMA_LOKET; endforeach;

$berita2 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 2") ->all();
foreach ($berita2 as $value): $nmLoket2 = $value->NAMA_LOKET; endforeach;

$berita3 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 3") ->all();
foreach ($berita3 as $value): $nmLoket3 = $value->NAMA_LOKET; endforeach;

$berita4 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 4") ->all();
foreach ($berita4 as $value): $nmLoket4 = $value->NAMA_LOKET; endforeach;

$berita5 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 5") ->all();
foreach ($berita5 as $value): $nmLoket5 = $value->NAMA_LOKET; endforeach;

$berita6 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 6") ->all();
foreach ($berita6 as $value): $nmLoket6 = $value->NAMA_LOKET; endforeach;

$berita7 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 7") ->all();
foreach ($berita7 as $value): $nmLoket7 = $value->NAMA_LOKET; endforeach;

$berita8 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 8") ->all();
foreach ($berita8 as $value): $nmLoket8 = $value->NAMA_LOKET; endforeach;

$berita9 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 9") ->all();
foreach ($berita9 as $value): $nmLoket9 = $value->NAMA_LOKET; endforeach;

$berita10 = \app\models\DataPetugasLoket::find() ->select('NAMA_LOKET') ->where("LOKET = 10") ->all();
foreach ($berita10 as $value): $nmLoket10 = $value->NAMA_LOKET; endforeach;

//jml antrian
$jmlAntrianLkt1 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 1) ")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianLkt2 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 2) ")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianLkt3 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 3) ")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianLkt4 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 4) ")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianLkt5 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 5) ")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianLkt6 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 6) ")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianLkt7 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 7) ")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianLkt8 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 8) ")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianLkt9 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 9) ")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianLkt10 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 10) ")            
->count('ID_DATA_ANTRIAN');

//sisa jml antrian
$jmlAntrianSisaLkt1 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 1) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianSisaLkt2 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 2) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianSisaLkt3 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 3) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianSisaLkt4 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 4) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianSisaLkt5 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 5) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianSisaLkt6 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 6) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianSisaLkt7 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 7) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianSisaLkt8 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 8) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianSisaLkt9 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 9) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

$jmlAntrianSisaLkt10 = \app\models\DataAntrian::find()
->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 10) AND (STAT_PANGILAN = '0')")            
->count('ID_DATA_ANTRIAN');

//print_r(date("ymd"));die;
//antrian saat ini
$tempAntrianSaatIniLkt1 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '1;%') and (ID_TEMP_PANGGILAN like '".date("ymd")."%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
$tempAntrianSaatIniLkt2 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '2;%') and (ID_TEMP_PANGGILAN like '".date("ymd")."%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
$tempAntrianSaatIniLkt3 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '3;%') and (ID_TEMP_PANGGILAN like '".date("ymd")."%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
$tempAntrianSaatIniLkt4 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '4;%') and (ID_TEMP_PANGGILAN like '".date("ymd")."%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
$tempAntrianSaatIniLkt5 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '5;%') and (ID_TEMP_PANGGILAN like '".date("ymd")."%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
$tempAntrianSaatIniLkt6 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '6;%') and (ID_TEMP_PANGGILAN like '".date("ymd")."%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
$tempAntrianSaatIniLkt7 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '7;%') and (ID_TEMP_PANGGILAN like '".date("ymd")."%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
$tempAntrianSaatIniLkt8 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '8;%') and (ID_TEMP_PANGGILAN like '".date("ymd")."%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
$tempAntrianSaatIniLkt9 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '9;%') and (ID_TEMP_PANGGILAN like '".date("ymd")."%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();
$tempAntrianSaatIniLkt10 = \app\models\TempPanggilan::find()->select("PANGILAN")->where("(PANGILAN like '10;%') and (ID_TEMP_PANGGILAN like '".date("ymd")."%')")->orderBy(["ID_TEMP_PANGGILAN"=>SORT_DESC])->one();

$AntrianSaatIniLkt1 = "0";
$AntrianSaatIniLkt2 = "0";
$AntrianSaatIniLkt3 = "0";
$AntrianSaatIniLkt4 = "0";
$AntrianSaatIniLkt5 = "0";
$AntrianSaatIniLkt6 = "0";
$AntrianSaatIniLkt7 = "0";
$AntrianSaatIniLkt8 = "0";
$AntrianSaatIniLkt9 = "0";
$AntrianSaatIniLkt10 = "0";

if(!empty($tempAntrianSaatIniLkt1)){$a1 = explode(";", $tempAntrianSaatIniLkt1['PANGILAN']); $AntrianSaatIniLkt1 = $a1[1];}
if(!empty($tempAntrianSaatIniLkt2)){$a2 = explode(";", $tempAntrianSaatIniLkt2['PANGILAN']); $AntrianSaatIniLkt2 = $a2[1];}
if(!empty($tempAntrianSaatIniLkt3)){$a3 = explode(";", $tempAntrianSaatIniLkt3['PANGILAN']); $AntrianSaatIniLkt3 = $a3[1];}
if(!empty($tempAntrianSaatIniLkt4)){$a4 = explode(";", $tempAntrianSaatIniLkt4['PANGILAN']); $AntrianSaatIniLkt4 = $a4[1];}
if(!empty($tempAntrianSaatIniLkt5)){$a5 = explode(";", $tempAntrianSaatIniLkt5['PANGILAN']); $AntrianSaatIniLkt5 = $a5[1];}
if(!empty($tempAntrianSaatIniLkt6)){$a6 = explode(";", $tempAntrianSaatIniLkt6['PANGILAN']); $AntrianSaatIniLkt6 = $a6[1];}
if(!empty($tempAntrianSaatIniLkt7)){$a7 = explode(";", $tempAntrianSaatIniLkt7['PANGILAN']); $AntrianSaatIniLkt7 = $a7[1];}
if(!empty($tempAntrianSaatIniLkt8)){$a8 = explode(";", $tempAntrianSaatIniLkt8['PANGILAN']); $AntrianSaatIniLkt8 = $a8[1];}
if(!empty($tempAntrianSaatIniLkt9)){$a9 = explode(";", $tempAntrianSaatIniLkt9['PANGILAN']); $AntrianSaatIniLkt9 = $a9[1];}
if(!empty($tempAntrianSaatIniLkt10)){$a10 = explode(";", $tempAntrianSaatIniLkt10['PANGILAN']); $AntrianSaatIniLkt10 = $a10[1];}

//rating sangat puas
$jmlSangatPuasLkt1 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 1) AND (RATING = 'Sangat Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlSangatPuasLkt2 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 2) AND (RATING = 'Sangat Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlSangatPuasLkt3 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 3) AND (RATING = 'Sangat Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlSangatPuasLkt4 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 4) AND (RATING = 'Sangat Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlSangatPuasLkt5 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 5) AND (RATING = 'Sangat Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlSangatPuasLkt6 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 6) AND (RATING = 'Sangat Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlSangatPuasLkt7 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 7) AND (RATING = 'Sangat Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlSangatPuasLkt8 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 8) AND (RATING = 'Sangat Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlSangatPuasLkt9 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 9) AND (RATING = 'Sangat Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlSangatPuasLkt10 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 10) AND (RATING = 'Sangat Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');

//rating sangat puas
$jmlPuasLkt1 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 1) AND (RATING = 'Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlPuasLkt2 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 2) AND (RATING = 'Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlPuasLkt3 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 3) AND (RATING = 'Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlPuasLkt4 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 4) AND (RATING = 'Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlPuasLkt5 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 5) AND (RATING = 'Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlPuasLkt6 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 6) AND (RATING = 'Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlPuasLkt7 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 7) AND (RATING = 'Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlPuasLkt8 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 8) AND (RATING = 'Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlPuasLkt9 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 9) AND (RATING = 'Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlPuasLkt10 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 10) AND (RATING = 'Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');

//rating tidak puas
$jmlTidakPuasLkt1 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 1) AND (RATING = 'Tidak Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlTidakPuasLkt2 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 2) AND (RATING = 'Tidak Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlTidakPuasLkt3 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 3) AND (RATING = 'Tidak Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlTidakPuasLkt4 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 4) AND (RATING = 'Tidak Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlTidakPuasLkt5 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 5) AND (RATING = 'Tidak Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlTidakPuasLkt6 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 6) AND (RATING = 'Tidak Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlTidakPuasLkt7 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 7) AND (RATING = 'Tidak Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlTidakPuasLkt8 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 8) AND (RATING = 'Tidak Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlTidakPuasLkt9 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 9) AND (RATING = 'Tidak Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');
$jmlTidakPuasLkt10 = \app\models\DataKepuasanPengunjung::find()->where("(DATE_FORMAT(TANGGAL,'%Y-%m-%d')='".date("Y-m-d")."') AND (LOKET = 10) AND (RATING = 'Tidak Puas')")->count('ID_DATA_KEPUASAN_PENGUNJUNG');


$suhuMin = 0;
$suhuMax = 0;
$suhu = 0;
try
{
    $url = "http://data.bmkg.go.id/propinsi_13_1.xml"; // from http://data.bmkg.go.id/ sesuaikan dengan lokasi yang diinginkan
    $sUrl = file_get_contents($url, False);
    $xml = simplexml_load_string($sUrl);
    for ($i=0; $i<sizeof($xml->Isi->Row); $i++) {
        $row = $xml->Isi->Row[$i];
        if(strtolower($row->Kota) == "bekasi") {// blitar merupakan contoh kota yang diambil data cuacanya dari bmkg
            //echo "<b>" . strtoupper($row->Kota) . "</b><br/>";
            //echo "<img src='http://www.bmkg.go.id/ImagesStatus/" . $row->Cuaca . ".gif' alt='" . $row->Cuaca . "'><br/>";
            //echo "Cuaca : " . $row->Cuaca . "<br/>";
            //echo "Suhu : " . $row->SuhuMin . " - ".$row->SuhuMax . " &deg;C<br/>";
            //echo "Kelembapan : " . $row->KelembapanMin . " - " . $row->KelembapanMax . " %<br/>";
            //echo "Kecepatan Angin : " . $row->KecepatanAngin . " (km/jam)<br/>";
            //echo "Arah Angin : " . $row->ArahAngin . "<br/>";
            $suhuMin = $row->SuhuMin;
            $suhuMax = $row->SuhuMax;
            break;
        }
    }
    $suhu = ($suhuMin + $suhuMax)/2;
}
 catch (yii\console\Exception $zz)
 {}



?>
<!DOCTYPE html>
<html>
<head>
	<title>Kementrian Komunikasi & Informatika Republik Indonesia</title>
	<link rel="stylesheet" type="text/css" href="/antrian/images/plasmanew/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/antrian/images/plasmanew/css/custom.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
        <style>
            .content, .content-header{padding: 0px !important;}
        </style>
</head>
<body style="font-family: 'Roboto', sans-serif;">

<!-- Content -->
<div class="container-fluid" style="background-image: url('/antrian/images/plasmanew/images/background.jpg');">
	<div class="row">

		<!-- Header -->
		<div class="header">
			<div class="logo-left">
				<img src="/antrian/images/plasmanew/images/logo.png">
			</div>
			<div class="date">
				<h3><?php echo date("d F Y") ?></h3>
			</div>
		</div>

		<!-- Left Containter -->
		<div class="col-md-4 no-padding">
			<!-- Cuaca -->
			<div class="cnt-cuaca">
				<div class="header-title"><h4>PERKIRAAN CUACA</h4></div>
				<div class="cuaca-display-left">
					<h1><?php echo $suhu; ?> C</h1>
					<h2>JAKARTA</h2>
				</div>
				<div class="cuaca-display-right">
					<img src="/antrian/images/plasmanew/images/cloudy.png">
				</div>
			</div>
			<!-- Image Slide -->
			<div class="image-slide">
					<div class="header-title"><h4>INFORMASI LAYANAN</h4></div>
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					  <!-- Indicators -->
					  <!-- <ol class="carousel-indicators">
					    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
					    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
					  </ol> -->

					  <!-- Wrapper for slides -->
                                          <?php
                                            $header = \app\models\DataSlideshow::find()
                                                ->select('ID_DATA_SLIDESHOW, IMAGE1, IMAGE2, IMAGE3, STATUS')
                                                ->where("STATUS = '1'")                
                                                ->all();

                                            foreach ($header as $value):                
                                                echo '<div class="carousel-inner" role="listbox">
                                                        <div class="item active">
                                                          <img src="/antrian/'.$value->IMAGE1.'" alt="...">
                                                            </div>
                                                            <div class="item">
                                                          <img src="/antrian/'.$value->IMAGE2.'" alt="...">
                                                            </div>
                                                            <div class="item">
                                                          <img src="/antrian/'.$value->IMAGE3.'" alt="...">
                                                            </div>
                                                    </div>';
                                            endforeach;

                                          ?>
                                          
                                          
                                          
                                          
                                          
                                          
					<!--<div class="carousel-inner" role="listbox">
					    <div class="item active">
					      <img src="/antrian/images/plasmanew/images/imgG.jpg" alt="...">
					  	</div>
					  	<div class="item">
					      <img src="/antrian/images/plasmanew/images/imgG.jpg" alt="...">
					  	</div>
					  	<div class="item">
					      <img src="/antrian/images/plasmanew/images/imgG.jpg" alt="...">
					  	</div>
					</div>-->
				</div>
			</div>
			<!-- Video -->
			<div class="hvideo"> 
				<div class="header-title vt"><h4>VIDEO</h4></div>
				<!--<iframe src="/antrian/images/plasmanew/images/vid.mp4" width="100%" height="250" frameborder="0" loop="true" controls="false"></iframe>-->
                                <div class="embed-responsive embed-responsive-16by9">
                                <?php
                                $header = \app\models\DataVideoWeb::find()
                                    ->select('ID_DATA_VIDEO_WEB, STATUS_VIDEO, PATH_YOUTUBE, PATH_VIDEO, STATUS')
                                    ->where("STATUS = '1'")                
                                    ->all();

                                foreach ($header as $value):                
                                    if($value->STATUS_VIDEO == '0')
                                    {echo '<iframe class="embed-responsive-item" src="'.$value->PATH_YOUTUBE.'"></iframe>';}
                                    else
                                    {echo '<iframe class="embed-responsive-item" src="/antrian/'.$value->PATH_VIDEO.'"></iframe>';}
                                endforeach;
                                 
                                ?>
                                    
                                    
                                
        </div>
			</div>
		</div>	
		<!-- Right Container -->
		<div class="col-md-8 no-padding">
			<div class="row">
				<div class="col-md-6 ">
					<!-- list -->
					<div class="list-box">
						<h3>1 <?php echo $nmLoket1 ?></h3>
						<div class="row">
							<div class="col-md-6">
							<span class="label label-primary" style="font-size: 13px; padding-top: 5px; ">ANTRIAN SEKARANG <label class="numeric" style="font-size: 15px;"> &nbsp;<?php if(strlen($AntrianSaatIniLkt1)==1){echo '00'.$AntrianSaatIniLkt1;}elseif (strlen($AntrianSaatIniLkt1)==2){echo '0'.$AntrianSaatIniLkt1;}else{echo $AntrianSaatIniLkt1;} ?></label></span>
							<span class="label label-primary">TOTAL <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianLkt1)==1){echo '00'.$jmlAntrianLkt1;}elseif (strlen($jmlAntrianLkt1)==2){echo '0'.$jmlAntrianLkt1;}else{echo $jmlAntrianLkt1;} ?></label></span>
							<span class="label label-primary">SISA <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianSisaLkt1)==1){echo '00'.$jmlAntrianSisaLkt1;}elseif (strlen($jmlAntrianSisaLkt1)==2){echo '0'.$jmlAntrianSisaLkt1;}else{echo $jmlAntrianSisaLkt1;} ?></label></span>	
							</div>
							<div class="col-md-6">
                                                        <span class="label label-primary">SANGAT PUAS <label class="numericB"><?php if($jmlAntrianLkt1 == 0){echo "0";}else{$sp1 = ($jmlSangatPuasLkt1/$jmlAntrianLkt1)*100; echo $sp1;}?>%</label></span>
                                                        <span class="label label-primary">PUAS <label class="numericB"><?php if($jmlAntrianLkt1 == 0){echo "0";}else{$p1 = ($jmlPuasLkt1/$jmlAntrianLkt1)*100; echo $p1;}?>%</label></span>
                                                        <span class="label label-primary">TIDAK PUAS <label class="numericB"><?php if($jmlAntrianLkt1 == 0){echo "0";}else{$tp1 = ($jmlTidakPuasLkt1/$jmlAntrianLkt1)*100; echo $tp1;}?>%</label></span>	
							</div>
						</div>
					</div>

					<div class="list-box">
						<h3>2 <?php echo $nmLoket2 ?></h3>
						<div class="row">
							<div class="col-md-6">
							<span class="label label-primary" style="font-size: 13px; padding-top: 5px; ">ANTRIAN SEKARANG <label class="numeric" style="font-size: 15px;"> &nbsp;<?php if(strlen($AntrianSaatIniLkt2)==1){echo '00'.$AntrianSaatIniLkt2;}elseif (strlen($AntrianSaatIniLkt2)==2){echo '0'.$AntrianSaatIniLkt2;}else{echo $AntrianSaatIniLkt2;} ?></label></span>
							<span class="label label-primary">TOTAL <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianLkt2)==1){echo '00'.$jmlAntrianLkt2;}elseif (strlen($jmlAntrianLkt2)==2){echo '0'.$jmlAntrianLkt2;}else{echo $jmlAntrianLkt2;} ?></label></span>
							<span class="label label-primary">SISA <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianSisaLkt2)==1){echo '00'.$jmlAntrianSisaLkt2;}elseif (strlen($jmlAntrianSisaLkt2)==2){echo '0'.$jmlAntrianSisaLkt2;}else{echo $jmlAntrianSisaLkt2;} ?></label></span>		
							</div>
							<div class="col-md-6">
                                                        <span class="label label-primary">SANGAT PUAS <label class="numericB"><?php if($jmlAntrianLkt2 == 0){echo "0";}else{$sp2 = ($jmlSangatPuasLkt2/$jmlAntrianLkt2)*100; echo $sp2;}?>%</label></span>
                                                        <span class="label label-primary">PUAS <label class="numericB"><?php if($jmlAntrianLkt2 == 0){echo "0";}else{$p2 = ($jmlPuasLkt2/$jmlAntrianLkt2)*100; echo $p2;}?>%</label></span>
                                                        <span class="label label-primary">TIDAK PUAS <label class="numericB"><?php if($jmlAntrianLkt2 == 0){echo "0";}else{$tp2 = ($jmlTidakPuasLkt2/$jmlAntrianLkt2)*100; echo $tp2;}?>%</label></span>	
							</div>
						</div>
					</div>

					<div class="list-box">
						<h3>3 <?php echo $nmLoket3 ?></h3>
						<div class="row">
							<div class="col-md-6">
							<span class="label label-primary" style="font-size: 13px; padding-top: 5px; ">ANTRIAN SEKARANG <label class="numeric" style="font-size: 15px;"> &nbsp;<?php if(strlen($AntrianSaatIniLkt3)==1){echo '00'.$AntrianSaatIniLkt3;}elseif (strlen($AntrianSaatIniLkt3)==2){echo '0'.$AntrianSaatIniLkt3;}else{echo $AntrianSaatIniLkt3;} ?></label></span>
							<span class="label label-primary">TOTAL <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianLkt3)==1){echo '00'.$jmlAntrianLkt3;}elseif (strlen($jmlAntrianLkt3)==2){echo '0'.$jmlAntrianLkt3;}else{echo $jmlAntrianLkt3;} ?></label></span>
							<span class="label label-primary">SISA <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianSisaLkt3)==1){echo '00'.$jmlAntrianSisaLkt3;}elseif (strlen($jmlAntrianSisaLkt3)==2){echo '0'.$jmlAntrianSisaLkt3;}else{echo $jmlAntrianSisaLkt3;} ?></label></span>	
							</div>
							<div class="col-md-6">
                                                        <span class="label label-primary">SANGAT PUAS <label class="numericB"><?php if($jmlAntrianLkt3 == 0){echo "0";}else{$sp3 = ($jmlSangatPuasLkt3/$jmlAntrianLkt3)*100; echo $sp3;}?>%</label></span>
                                                        <span class="label label-primary">PUAS <label class="numericB"><?php if($jmlAntrianLkt3 == 0){echo "0";}else{$p3 = ($jmlPuasLkt3/$jmlAntrianLkt3)*100; echo $p3;}?>%</label></span>
                                                        <span class="label label-primary">TIDAK PUAS <label class="numericB"><?php if($jmlAntrianLkt3 == 0){echo "0";}else{$tp3 = ($jmlTidakPuasLkt3/$jmlAntrianLkt3)*100; echo $tp3;}?>%</label></span>		
							</div>
						</div>
					</div>

					<div class="list-box">
						<h3>4 <?php echo $nmLoket4 ?></h3>
						<div class="row">
							<div class="col-md-6">
							<span class="label label-primary" style="font-size: 13px; padding-top: 5px; ">ANTRIAN SEKARANG <label class="numeric" style="font-size: 15px;"> &nbsp;<?php if(strlen($AntrianSaatIniLkt4)==1){echo '00'.$AntrianSaatIniLkt4;}elseif (strlen($AntrianSaatIniLkt4)==2){echo '0'.$AntrianSaatIniLkt4;}else{echo $AntrianSaatIniLkt4;} ?></label></span>
							<span class="label label-primary">TOTAL <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianLkt4)==1){echo '00'.$jmlAntrianLkt4;}elseif (strlen($jmlAntrianLkt4)==2){echo '0'.$jmlAntrianLkt4;}else{echo $jmlAntrianLkt4;} ?></label></span>
							<span class="label label-primary">SISA <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianSisaLkt4)==1){echo '00'.$jmlAntrianSisaLkt4;}elseif (strlen($jmlAntrianSisaLkt4)==2){echo '0'.$jmlAntrianSisaLkt4;}else{echo $jmlAntrianSisaLkt4;} ?></label></span>	
							</div>
							<div class="col-md-6">
                                                        <span class="label label-primary">SANGAT PUAS <label class="numericB"><?php if($jmlAntrianLkt4 == 0){echo "0";}else{$sp4 = ($jmlSangatPuasLkt4/$jmlAntrianLkt4)*100; echo $sp4;}?>%</label></span>
                                                        <span class="label label-primary">PUAS <label class="numericB"><?php if($jmlAntrianLkt4 == 0){echo "0";}else{$p4 = ($jmlPuasLkt4/$jmlAntrianLkt4)*100; echo $p4;}?>%</label></span>
                                                        <span class="label label-primary">TIDAK PUAS <label class="numericB"><?php if($jmlAntrianLkt4 == 0){echo "0";}else{$tp4 = ($jmlTidakPuasLkt4/$jmlAntrianLkt4)*100; echo $tp4;}?>%</label></span>		
							</div>
						</div>
					</div>

					<div class="list-box">
						<h3>5 <?php echo $nmLoket5 ?></h3>
						<div class="row">
							<div class="col-md-6">
							<span class="label label-primary" style="font-size: 13px; padding-top: 5px; ">ANTRIAN SEKARANG <label class="numeric" style="font-size: 15px;"> &nbsp;<?php if(strlen($AntrianSaatIniLkt5)==1){echo '00'.$AntrianSaatIniLkt5;}elseif (strlen($AntrianSaatIniLkt5)==2){echo '0'.$AntrianSaatIniLkt5;}else{echo $AntrianSaatIniLkt5;} ?></label></span>
							<span class="label label-primary">TOTAL <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianLkt5)==1){echo '00'.$jmlAntrianLkt5;}elseif (strlen($jmlAntrianLkt5)==2){echo '0'.$jmlAntrianLkt5;}else{echo $jmlAntrianLkt5;} ?></label></span>
							<span class="label label-primary">SISA <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianSisaLkt5)==1){echo '00'.$jmlAntrianSisaLkt5;}elseif (strlen($jmlAntrianSisaLkt5)==2){echo '0'.$jmlAntrianSisaLkt5;}else{echo $jmlAntrianSisaLkt5;} ?></label></span>	
							</div>
							<div class="col-md-6">
                                                        <span class="label label-primary">SANGAT PUAS <label class="numericB"><?php if($jmlAntrianLkt5 == 0){echo "0";}else{$sp5 = ($jmlSangatPuasLkt5/$jmlAntrianLkt5)*100; echo $sp5;}?>%</label></span>
                                                        <span class="label label-primary">PUAS <label class="numericB"><?php if($jmlAntrianLkt5 == 0){echo "0";}else{$p5 = ($jmlPuasLkt5/$jmlAntrianLkt5)*100; echo $p5;}?>%</label></span>
                                                        <span class="label label-primary">TIDAK PUAS <label class="numericB"><?php if($jmlAntrianLkt5 == 0){echo "0";}else{$tp5 = ($jmlTidakPuasLkt5/$jmlAntrianLkt5)*100; echo $tp5;}?>%</label></span>		
							</div>
						</div>
					</div>

				</div>
				<div class="col-md-6 no-padding">
					<!-- list -->
					<div class="list-box">
						<h3>6 <?php echo $nmLoket6 ?></h3>
						<div class="row">
							<div class="col-md-6">
							<span class="label label-primary" style="font-size: 13px; padding-top: 5px; ">ANTRIAN SEKARANG <label class="numeric" style="font-size: 15px;"> &nbsp;<?php if(strlen($AntrianSaatIniLkt6)==1){echo '00'.$AntrianSaatIniLkt6;}elseif (strlen($AntrianSaatIniLkt6)==2){echo '0'.$AntrianSaatIniLkt6;}else{echo $AntrianSaatIniLkt6;} ?></label></span>
							<span class="label label-primary">TOTAL <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianLkt6)==1){echo '00'.$jmlAntrianLkt6;}elseif (strlen($jmlAntrianLkt6)==2){echo '0'.$jmlAntrianLkt6;}else{echo $jmlAntrianLkt6;} ?></label></span>
							<span class="label label-primary">SISA <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianSisaLkt6)==1){echo '00'.$jmlAntrianSisaLkt6;}elseif (strlen($jmlAntrianSisaLkt6)==2){echo '0'.$jmlAntrianSisaLkt6;}else{echo $jmlAntrianSisaLkt6;} ?></label></span>	
							</div>
							<div class="col-md-6">
                                                        <span class="label label-primary">SANGAT PUAS <label class="numericB"><?php if($jmlAntrianLkt6 == 0){echo "0";}else{$sp6 = ($jmlSangatPuasLkt6/$jmlAntrianLkt6)*100; echo $sp6;}?>%</label></span>
                                                        <span class="label label-primary">PUAS <label class="numericB"><?php if($jmlAntrianLkt6 == 0){echo "0";}else{$p6 = ($jmlPuasLkt6/$jmlAntrianLkt6)*100; echo $p6;}?>%</label></span>
                                                        <span class="label label-primary">TIDAK PUAS <label class="numericB"><?php if($jmlAntrianLkt6 == 0){echo "0";}else{$tp6 = ($jmlTidakPuasLkt6/$jmlAntrianLkt6)*100; echo $tp6;}?>%</label></span>		
							</div>
						</div>
					</div>

					<div class="list-box">
						<h3>7 <?php echo $nmLoket7 ?></h3>
						<div class="row">
							<div class="col-md-6">
							<span class="label label-primary" style="font-size: 13px; padding-top: 5px; ">ANTRIAN SEKARANG <label class="numeric" style="font-size: 15px;"> &nbsp;<?php if(strlen($AntrianSaatIniLkt7)==1){echo '00'.$AntrianSaatIniLkt7;}elseif (strlen($AntrianSaatIniLkt7)==2){echo '0'.$AntrianSaatIniLkt7;}else{echo $AntrianSaatIniLkt7;} ?></label></span>
							<span class="label label-primary">TOTAL <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianLkt7)==1){echo '00'.$jmlAntrianLkt7;}elseif (strlen($jmlAntrianLkt7)==2){echo '0'.$jmlAntrianLkt7;}else{echo $jmlAntrianLkt7;} ?></label></span>
							<span class="label label-primary">SISA <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianSisaLkt7)==1){echo '00'.$jmlAntrianSisaLkt7;}elseif (strlen($jmlAntrianSisaLkt7)==2){echo '0'.$jmlAntrianSisaLkt7;}else{echo $jmlAntrianSisaLkt7;} ?></label></span>	
							</div>
							<div class="col-md-6">
                                                        <span class="label label-primary">SANGAT PUAS <label class="numericB"><?php if($jmlAntrianLkt7 == 0){echo "0";}else{$sp7 = ($jmlSangatPuasLkt7/$jmlAntrianLkt7)*100; echo $sp7;}?>%</label></span>
                                                        <span class="label label-primary">PUAS <label class="numericB"><?php if($jmlAntrianLkt7 == 0){echo "0";}else{$p7 = ($jmlPuasLkt7/$jmlAntrianLkt7)*100; echo $p7;}?>%</label></span>
                                                        <span class="label label-primary">TIDAK PUAS <label class="numericB"><?php if($jmlAntrianLkt7 == 0){echo "0";}else{$tp7 = ($jmlTidakPuasLkt7/$jmlAntrianLkt7)*100; echo $tp7;}?>%</label></span>		
							</div>
						</div>
					</div>

					<div class="list-box">
						<h3>8 <?php echo $nmLoket8 ?></h3>
						<div class="row">
							<div class="col-md-6">
							<span class="label label-primary" style="font-size: 13px; padding-top: 5px; ">ANTRIAN SEKARANG <label class="numeric" style="font-size: 15px;"> &nbsp;<?php if(strlen($AntrianSaatIniLkt8)==1){echo '00'.$AntrianSaatIniLkt8;}elseif (strlen($AntrianSaatIniLkt8)==2){echo '0'.$AntrianSaatIniLkt8;}else{echo $AntrianSaatIniLkt8;} ?></label></span>
							<span class="label label-primary">TOTAL <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianLkt8)==1){echo '00'.$jmlAntrianLkt8;}elseif (strlen($jmlAntrianLkt8)==2){echo '0'.$jmlAntrianLkt8;}else{echo $jmlAntrianLkt8;} ?></label></span>
							<span class="label label-primary">SISA <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianSisaLkt8)==1){echo '00'.$jmlAntrianSisaLkt8;}elseif (strlen($jmlAntrianSisaLkt8)==2){echo '0'.$jmlAntrianSisaLkt8;}else{echo $jmlAntrianSisaLkt8;} ?></label></span>	
							</div>
							<div class="col-md-6">
                                                        <span class="label label-primary">SANGAT PUAS <label class="numericB"><?php if($jmlAntrianLkt1 == 0){echo "0";}else{$sp8 = ($jmlSangatPuasLkt8/$jmlAntrianLkt8)*100; echo $sp8;}?>%</label></span>
                                                        <span class="label label-primary">PUAS <label class="numericB"><?php if($jmlAntrianLkt1 == 0){echo "0";}else{$p8 = ($jmlPuasLkt8/$jmlAntrianLkt8)*100; echo $p8;}?>%</label></span>
                                                        <span class="label label-primary">TIDAK PUAS <label class="numericB"><?php if($jmlAntrianLkt1 == 0){echo "0";}else{$tp8 = ($jmlTidakPuasLkt8/$jmlAntrianLkt8)*100; echo $tp8;}?>%</label></span>		
							</div>
						</div>
					</div>

					<div class="list-box">
						<h3>9 <?php echo $nmLoket9 ?></h3>
						<div class="row">
							<div class="col-md-6">
							<span class="label label-primary" style="font-size: 13px; padding-top: 5px; ">ANTRIAN SEKARANG <label class="numeric" style="font-size: 15px;"> &nbsp;<?php if(strlen($AntrianSaatIniLkt9)==1){echo '00'.$AntrianSaatIniLkt9;}elseif (strlen($AntrianSaatIniLkt9)==2){echo '0'.$AntrianSaatIniLkt9;}else{echo $AntrianSaatIniLkt9;} ?></label></span>
							<span class="label label-primary">TOTAL <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianLkt9)==1){echo '00'.$jmlAntrianLkt9;}elseif (strlen($jmlAntrianLkt9)==2){echo '0'.$jmlAntrianLkt9;}else{echo $jmlAntrianLkt9;} ?></label></span>
							<span class="label label-primary">SISA <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianSisaLkt9)==1){echo '00'.$jmlAntrianSisaLkt9;}elseif (strlen($jmlAntrianSisaLkt9)==2){echo '0'.$jmlAntrianSisaLkt9;}else{echo $jmlAntrianSisaLkt9;} ?></label></span>	
							</div>
							<div class="col-md-6">
                                                        <span class="label label-primary">SANGAT PUAS <label class="numericB"><?php if($jmlAntrianLkt9 == 0){echo "0";}else{$sp9 = ($jmlSangatPuasLkt9/$jmlAntrianLkt9)*100; echo $sp9;}?>%</label></span>
                                                        <span class="label label-primary">PUAS <label class="numericB"><?php if($jmlAntrianLkt9 == 0){echo "0";}else{$p9 = ($jmlPuasLkt9/$jmlAntrianLkt9)*100; echo $p9;}?>%</label></span>
                                                        <span class="label label-primary">TIDAK PUAS <label class="numericB"><?php if($jmlAntrianLkt9 == 0){echo "0";}else{$tp9 = ($jmlTidakPuasLkt9/$jmlAntrianLkt9)*100; echo $tp9;}?>%</label></span>		
							</div>
						</div>
					</div>

					<div class="list-box">
						<h3>10 <?php echo $nmLoket10 ?></h3>
						<div class="row">
							<div class="col-md-6">
							<span class="label label-primary" style="font-size: 13px; padding-top: 5px; ">ANTRIAN SEKARANG <label class="numeric" style="font-size: 15px;"> &nbsp;<?php if(strlen($AntrianSaatIniLkt10)==1){echo '00'.$AntrianSaatIniLkt10;}elseif (strlen($AntrianSaatIniLkt10)==2){echo '0'.$AntrianSaatIniLkt10;}else{echo $AntrianSaatIniLkt10;} ?></label></span>
							<span class="label label-primary">TOTAL <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianLkt10)==1){echo '00'.$jmlAntrianLkt10;}elseif (strlen($jmlAntrianLkt10)==2){echo '0'.$jmlAntrianLkt10;}else{echo $jmlAntrianLkt10;} ?></label></span>
							<span class="label label-primary">SISA <label class="numeric"> &nbsp;<?php if(strlen($jmlAntrianSisaLkt10)==1){echo '00'.$jmlAntrianSisaLkt10;}elseif (strlen($jmlAntrianSisaLkt10)==2){echo '0'.$jmlAntrianSisaLkt10;}else{echo $jmlAntrianSisaLkt10;} ?></label></span>	
							</div>
							<div class="col-md-6">
                                                        <span class="label label-primary">SANGAT PUAS <label class="numericB"><?php if($jmlAntrianLkt10 == 0){echo "0";}else{$sp10 = ($jmlSangatPuasLkt10/$jmlAntrianLkt10)*100; echo $sp10;}?>%</label></span>
                                                        <span class="label label-primary">PUAS <label class="numericB"><?php if($jmlAntrianLkt10 == 0){echo "0";}else{$p10 = ($jmlPuasLkt10/$jmlAntrianLkt10)*100; echo $p10;}?>%</label></span>
                                                        <span class="label label-primary">TIDAK PUAS <label class="numericB"><?php if($jmlAntrianLkt10 == 0){echo "0";}else{$tp10 = ($jmlTidakPuasLkt10/$jmlAntrianLkt10)*100; echo $tp10;}?>%</label></span>		
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		
	<!-- Time & Marque Line -->
		<div class="col-md-12 no-padding news-box">
			<div class="time">
				<h2><?php echo date("G:i") ?></h2>
			</div>
			<div class="news">
				<marquee behavior="scroll" direction="left"><?php echo $berita ?></marquee>
			</div>
		</div>
	</div>
</div>
<!-- End Content -->

<!-- Javascript -->
<script type="text/javascript" src="/antrian/images/plasmanew/js/jquery.min.js"></script>
<script type="text/javascript" src="/antrian/images/plasmanew/js/bootstrap.min.js"></script>
</body>
</html>

