<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'name' => 'Antrian',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'id',
    'modules' => [
        'gridview' => [
            'class' => 'kartik\grid\Module',
        ],
        'datecontrol' =>  [
            'class' => '\kartik\datecontrol\Module'
        ]
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'tFV-nHe6UeStmx0Hek3qK6IgLEuntMqG',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => ['app\models\DataPengunaAplikasi','app\models\DataPengunaAplikasi'],
            'enableAutoLogin' => true,
            'enableSession'=>true,
        ],
        'formatter' => [
            'locale' => 'id-ID',
            'dateFormat' => 'yyyy-MM-dd',
            'datetimeFormat' => 'yyyy-MM-dd H:m:s',
            'timeFormat' => 'H:m:s',            
            'timeZone' => 'UTC',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'authManager'=>[
            'class'=>'yii\rbac\DbManager',
            'defaultRoles'=>['guest'],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
			//'viewPath' => '@app\mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'kontrakhpi@gmail.com',
                'password' => 'Pasadena2014?',
                'port' => '465',
                'encryption' => 'ssl',                  
				//'port' => '587',
                //'encryption' => 'tls',                  
            ],
            /*'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.hapindo.co.id',
				//'host' => '103.30.84.157',
                'username' => 'simponik@hapindo.co.id',
                'password' => 'majalengka1233',
                'port' => '465',
                'encryption' => 'ssl',
				//'port' => '25',
                //'encryption' => 'tls',
            ],*/
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
		/*'view' => [
			 'theme' => [
				 'pathMap' => [
					'@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/yiisoft/yii2-app'
				 ],
			 ],
		],*/
        'db' => require(__DIR__ . '/db.php'),
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
		
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '169.254.239.208', '::1'],
    ];
}

return $config;
