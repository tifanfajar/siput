<!DOCTYPE html>
<?php 
include 'config.php';
error_reporting(null);
session_start();
$_SESSION['foto'] 		= $_REQUEST['foto']; 	
$_SESSION['loket'] 		= $_REQUEST['loket']; 	
$_SESSION['petugas'] 	= $_REQUEST['petugas'];

?>
<html>
<head>
	<title>Kementerian Komunikasi & Informatika</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/custom.css">
	<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="js/sweetalert2/dist/sweetalert2.min.css">
<style type="text/css">
	.lk-back{
	 background: url(images/lk-back.png);
	 background-repeat: no-repeat; 
	 background-position: center;
	 padding-top: 40px;
	 height: 120px;
	}
	.blink_me {
	  animation: blinker 1s linear infinite;
	}

	@keyframes blinker {  
	  50% { opacity: 0; }
	}
</style>
</head>
<body onclick="fullScreenApi.requestFullScreen(document.documentElement)"  >
<nav class="navbar navbar-default" role="navigation" >
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<!-- <div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">Title</a>
		</div> -->

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<div class="nav navbar-nav navbar-left">
				<img src="images/logo-left.png">
				<B>KEPUASAN PELANGGAN KOMINFO</B>
			</div>
			<ul class="nav navbar-nav navbar-right">
					<label class="time">
						<span id="jam"></span>
						<span class="blink_me" id="titik">:</span>
						<span id="menit"></span>&nbsp;
						<i data-toggle="modal" data-target="#myModalLogout" class="fa fa-power-off" style="color:red"></i>
					</label> 
			</ul>
		</div><!-- /.navbar-collapse -->
	</div>
</nav>


<!-- Main Content Mi -->
<div class="container-fluid">  
			<div class="row">
				<div class="col-md-4 col-sm-4 lk-back">
									<div style="background-color:#9595f64d"><center>
									   <?php 
										session_start();
										if($_SESSION['loket'] == "1"){
									   ?>
									   <label class="lbl-lak">Loket 1</label>
									   <label class="lbl-inf">Sertifikat REOR</label>
									   <?php 
										}elseif($_SESSION['loket'] == "2"){
									   ?>
									   <label class="lbl-lak">Loket 2</label><br/>
									   <label class="lbl-inf">Pengambilan ISR Darat</label>
									   <?php 
										}elseif($_SESSION['loket'] == "3"){
									   ?>
									   <label class="lbl-lak">Loket 3</label>
									   <label class="lbl-inf">Permohonan ISR Darat</label>
									   <?php 
										}elseif($_SESSION['loket'] == "4"){
									   ?>
									   <label class="lbl-lak">Loket 4</label>
									   <label class="lbl-inf">Payment & BHP</label>
									   <?php 
										}elseif($_SESSION['loket'] == "5"){
									   ?>
									   <label class="lbl-lak">Loket 5</label>
									   <label class="lbl-inf">Pencetakan SSP</label>
									   <?php 
										}elseif($_SESSION['loket'] == "6"){
									   ?>
									   <label class="lbl-lak">Loket 6</label>
									   <label class="lbl-inf">Permohonan ISR Maritim & Penerbangan</label>
									   <?php 
										}elseif($_SESSION['loket'] == "7"){
									   ?>
									   <label class="lbl-lak">Loket 7</label>
									   <label class="lbl-inf">ISR Broadcast & Satelit</label>
									   <?php 
										}elseif($_SESSION['loket'] == "8"){
									   ?>
									   <label class="lbl-lak">Loket 8</label>
									   <label class="lbl-inf">Sertifikasi Perangkat & Perpanjangan</label>
									   <?php 
										}elseif($_SESSION['loket'] == "9"){
									   ?>
									   <label class="lbl-lak">Loket 9</label>
									   <label class="lbl-inf">Pengambilan Sertifikat</label>
									   <?php 
										}elseif($_SESSION['loket'] == "10"){
									   ?>
									   <label class="lbl-lak">Loket 10</label>
									   <label class="lbl-inf">Pengambilan SP2 & SP3</label>
									   <?php 
										}
									   ?>
								       <br/><b><?php echo $_SESSION['petugas'] ?></b>
								   </center></div>
								   <div class="img-div" style="margin-bottom: 3px;">
										<img src="images/cs/<?php echo $_SESSION['foto'] ?>" style="width:100%;height:265px">
								   </div>
								   <?php 
								   $antrian = mysqli_fetch_object(mysqli_query($con,"select * from temp_panggilan where pangilan like '".$_SESSION['loket']."%'  ORDER BY ID_TEMP_PANGGILAN DESC LIMIT 1"));
								   $antr = explode(';',$antrian->PANGILAN);
								   $no_antr="";
								   if($antr==null or $antr==""){
									   $no_antr="";
								   }
								   else {
									   $no_antr=$antr[1];
								   }
								   ?>
								   <div style="margin-bottom: 40px;">
									  <center>
									  No. Antrian Anda<br/>
									  <span id="no_antrian"><?php echo $no_antr; ?></span>
									  </center>
								   </div>
					</div>
					
				<!-- <div class="cnt-no-antrian">
					<h5>Masukan Nomor Antrian Anda</h5>
					<input class="" type="text" name="input-nomor" placeholder="Nomor antrian anda">
				</div>
				<div class="form-group">
					<label for="email">Nomor Antrian:</label>
					<input type="email" class="form-control" id="email">
				</div>-->
			<div class="col-md-8 col-sm-8">
					<h3 id="title_pelayanan" >Apakah anda puas dengan pelayanan kami ?</h3>
					<a onclick="get_val('Sangat Puas');" style="display:block;cursor:pointer"  data-toggle="modal" data-target="#myModal" class="col-md-12 btn-a">
						<img  src="images/img-a.gif" style="float: left; margin-right: 15px;height:80px">
						<h4>Sangat Puas</h4>
						<p>Sangat puas dengan pelayanan kami</p>
						<img class="img-right" src="images/img-c0.png" style="height:108px">
					</a>
					<a onclick="get_val('Puas');" style="display:block;cursor:pointer" data-toggle="modal" data-target="#myModal" class="col-md-12 btn-b">
						<img src="images/img-b.gif" style="float: left; margin-right: 15px;height:80px">
						<h4>Puas</h4>
						<p>Puas dengan pelayanan kami</p>
						<img class="img-right" src="images/img-c1.png" style="height:108px">
					</a>
					<a onclick="get_val('Tidak Puas');" style="display:block;cursor:pointer" data-toggle="modal" data-target="#myModal" class="col-md-12 btn-c">
						<img src="images/img-c.gif" style="float: left; margin-right: 15px;height:80px">
						<h4>Tidak Puas</h4>
						<p>Tidak puas dengan pelayanan kami</p>
						<img class="img-right" src="images/img-c2.png" style="height:108px" >
					</a>
			</div>
		</div>
</div>
<!-- End Content Mi -->
<?php 
$berita = array();
        
$data=mysqli_query($con,"select TEXT from data_running_text where STATUS = 'Aktif'");
if ($data === FALSE) 
{ die(mysqli_error()); }
while($hasil=mysqli_fetch_array($data))
{
    $berita[] = $hasil['TEXT'];
}
?>

<marquee><?php echo implode(' - ', $berita); ?></marquee>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 850px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background-color: #337ab7;color: white;">
        <button style="color: white;" type="button"  class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-camera"></i> Ambil Gambar</h4>
      </div>
	  
	  <form method="POST" id="form_survey">
		 <div class="modal-body">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<center>
								<input type="file" accept="image/*;capture=camera" name="pas_foto" id="my-file" style="display:none" >
								<img id="my-camera" src="images/photo.png" style="width:295px;height:320px"  />
						</center>
					</div>
					<div class="col-sm-6 col-md-6" style="background: url(images/lk-back.png) no-repeat center center fixed; ">
						<h1>Terima Kasih</h1>
						Anda telah memberikan pendapat anda terhadap pelayanan kami<br/><br/><br/>
						<textarea rows="5" class="form-control" name="deskripsi" id="deskripsi"></textarea>
					</div>
				</div>
				<input type="hidden" name="penilaian" id="penilaian" required>
				<input type="hidden" name="id_loket"     value="<?php echo $_SESSION['loket'] ?>"  id="id_loket" required>
				<input type="hidden" name="id_antrian"   id="id_antrian" >
		  </div>
		  <div class="modal-footer">
			<button type="submit"  id="kirim_btn" class="btn btn-success" >Kirim</button>
			<button type="button"  id="cancel_btn" class="btn btn-danger" data-dismiss="modal">Batalkan</button>
		  </div>
	  </form>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="myModalLogout" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 350px;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background-color: #337ab7;color: white;">
        <button style="color: white;" type="button"  class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-power-off"></i> Keluar</h4>
      </div>
	  <form method="POST" id="form_keluar">
		 <div class="modal-body">
				<div class="row">
					<div class="col-sm-12 col-md-12" style="background: url(images/lk-back.png) no-repeat center center fixed; ">
						Masukan Password<br/>
						<input type="password" name="pwd" class="form-control" id="pwd">
					</div>
				</div>
		  </div>
		  <div class="modal-footer">
			<button type="submit"  id="keluar_btn" class="btn btn-danger" ><i class="fa fa-power-off"></i> Keluar</button>
		  </div>
	  </form>
    </div>

  </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/sweetalert2/dist/sweetalert2.min.js"></script>
<script src="js/moment.min.js"></script>
<script type="text/javascript">
function fungsiReload() {
    location.reload();
}
setInterval(get_antrian, 10000);
function get_antrian(){
	$.ajax({
			url: 'get_antrian.php?loket=<?php echo $_SESSION['loket'] ?>', 
			type: 'POST',
			processData: false,
			contentType: false,
			success: function(data) {
				$("#no_antrian").html(data);
			}
		});
}

 var interval = setInterval(function() {
        var momentNow = moment();
        $('#jam').html(momentNow.format('HH'));
        $('#menit').html(momentNow.format('mm'));
    }, 100);
$("#form_survey").on("submit", function (event) {
		event.preventDefault();
			do_act();
});
$("#form_keluar").on("submit", function (event) {
		event.preventDefault();
			do_logout();
});
function get_val(id_val){
	$("#penilaian").val(id_val);
	var no_antrian = $("#no_antrian").html();
	$("#id_antrian").val(no_antrian);
	
	if(no_antrian==""){
		 swal({
			title: 'Terjadi Kesalahan',
			html: 'Maaf, anda belum mendapat nomor antrian',
			type: 'error',
			showCancelButton: false,
			showLoaderOnConfirm: false
		  }).then(function() {
				  $('#myModal').modal('toggle');
		 });
	}
}
$('#my-camera').click(function(){
    $('#my-file').click();
});
$("#my-file").change(function(){
    readURL(this);
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#my-camera').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
			  (function() {
				var
					fullScreenApi = {
						supportsFullScreen: false,
						nonNativeSupportsFullScreen: false,
						isFullScreen: function() { return false; },
						requestFullScreen: function() {},
						cancelFullScreen: function() {},
						fullScreenEventName: '',
						prefix: ''
					},
					browserPrefixes = 'webkit moz o ms khtml'.split(' ');
			 
				// check for native support
				if (typeof document.cancelFullScreen != 'undefined') {
					fullScreenApi.supportsFullScreen = true;
				} else {
					// check for fullscreen support by vendor prefix
					for (var i = 0, il = browserPrefixes.length; i < il; i++ ) {
						fullScreenApi.prefix = browserPrefixes[i];
			 
						if (typeof document[fullScreenApi.prefix + 'CancelFullScreen' ] != 'undefined' ) {
							fullScreenApi.supportsFullScreen = true;
			 
							break;
						}
					}
				}
			 
				// update methods to do something useful
				if (fullScreenApi.supportsFullScreen) {
					fullScreenApi.fullScreenEventName = fullScreenApi.prefix + 'fullscreenchange';
			 
					fullScreenApi.isFullScreen = function() {
						switch (this.prefix) {
							case '':
								return document.fullScreen;
							case 'webkit':
								return document.webkitIsFullScreen;
							default:
								return document[this.prefix + 'FullScreen'];
						}
					}
					fullScreenApi.requestFullScreen = function(el) {
						return (this.prefix === '') ? el.requestFullScreen() : el[this.prefix + 'RequestFullScreen']();
					}
					fullScreenApi.cancelFullScreen = function(el) {
						return (this.prefix === '') ? document.cancelFullScreen() : document[this.prefix + 'CancelFullScreen']();
					}
				}
				else if (typeof window.ActiveXObject !== "undefined") { // IE.
					fullScreenApi.nonNativeSupportsFullScreen = true;
					fullScreenApi.requestFullScreen = fullScreenApi.requestFullScreen = function (el) {
							var wscript = new ActiveXObject("WScript.Shell");
							if (wscript !== null) {
								wscript.SendKeys("{F11}");
							}
					}
					fullScreenApi.isFullScreen = function() {
							return document.body.clientHeight == screen.height && document.body.clientWidth == screen.width;
					}
				}
			 
				// jQuery plugin
				if (typeof jQuery != 'undefined') {
					jQuery.fn.requestFullScreen = function() {
			 
						return this.each(function() {
							if (fullScreenApi.supportsFullScreen) {
								fullScreenApi.requestFullScreen(this);
							}
						});
					};
				}
			 
				// export api
				window.fullScreenApi = fullScreenApi;
			})();
function do_act(){
                        swal({
                          title: 'Kirim Penilaian',
                          text:  'Apakah data ini sudah benar ? ',
                          type:  'info',      // warning,info,success,error
                          showCancelButton: true,
                          showLoaderOnConfirm: true,
                          preConfirm: function(){
                            $.ajax({
                                url: 'kirim_survey.php', 
                                type: 'POST',
                                data: new FormData($('#form_survey')[0]),  // Form ID
                                processData: false,
                                contentType: false,
								beforeSend: function() {
									$('#kirim_btn').prop('disabled', true);
									$('#cancel_btn').prop('disabled', true);
								},
                                success: function(data) {
									$('#kirim_btn').prop('disabled', false);
									$('#cancel_btn').prop('disabled', false);
                                    if(data=="OK")
                                    {
                                        swal({
                                            title: 'Sukses',
                                            type: 'success',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          }).then(function() {
                                                  $("#no_antrian").html("");
												  $('#myModal').modal('toggle');
												  $("#my-camera").attr("src","images/photo.png");
												  $("#deskripsi").val(""); 
											});
                                    } 
                                    else
                                    {
                                        swal({
                                            title: 'Error',
                                            html: data,
                                            type: 'error',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          });
                                    }
                                }
                            });
                         }
						});   
}

function do_logout(){
                        swal({
                          title: 'Keluar Aplikasi',
                          type:  'warning',      // warning,info,success,error
                          showCancelButton: true,
                          showLoaderOnConfirm: true,
                          preConfirm: function(){
                            $.ajax({
                                url: 'check_pwd.php', 
                                type: 'POST',
                                data: new FormData($('#form_keluar')[0]),  // Form ID
                                processData: false,
                                contentType: false,
								beforeSend: function() {
									$('#keluar_btn').prop('disabled', true);
								},
                                success: function(data) {
									$('#keluar_btn').prop('disabled', false);
                                    if(data=="OK") {
										swal({
                                            title: 'Sukses',
                                            html: 'Aplikasi di tutup',
                                            type: 'success',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          }).then(function() {
                                                  window.location = "."; 
											});
                                    } 
                                    else
                                    {
                                        swal({
                                            title: 'Error',
                                            html: data,
                                            type: 'error',
                                            showCancelButton: false,
                                            showLoaderOnConfirm: false,
                                          });
                                    }
                                }
                            });
                         }
						});   
}
</script> 
</body>
</html>