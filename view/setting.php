<?php  
 $check_access = $gen_model->GetOneRow("tr_function_access",array('id_menu'=>3,'kode_function_access'=>$_SESSION['group_user'])); 
?>
<title>Setting - <?php echo str_replace('<br/>','',$web['judul']) ?></title>


<style type="text/css">
  #example_paginate{position: relative; left: 40%; top: 5px;}
  .pagination li a{margin:5px 15px; }
  .dataTables_info{margin-bottom: 15px;}
  .datatable>tbody>tr>td
  {
    white-space: nowrap;
  } 
  .table th {
     text-align: center;
     font-weight:bold;
  }
</style>
<script type="text/javascript" src="<?php echo $basepath ?>assets/js/jscolor.js"></script>
<script src="assets/js/ckeditor/ckeditor.js"></script>

<body class="pelayanan-cnt">


  <?php  include "view/top_menu.php";  
  $akses_add_tab_dft_pg = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>13,'kode_function_access'=>$_SESSION['group_user'])); 
  $akses_add_tab_hak_akses = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>14,'kode_function_access'=>$_SESSION['group_user'])); 
  $akses_add_tab_sims = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>15,'kode_function_access'=>$_SESSION['group_user'])); 
  $akses_add_tab_company = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>16,'kode_function_access'=>$_SESSION['group_user'])); 
  $akses_add_tab_upt = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>17,'kode_function_access'=>$_SESSION['group_user'])); 
  $akses_edit_tab_website = $gen_model->GetOne("fua_edit","tr_function_access_tab",array('id_tab'=>18,'kode_function_access'=>$_SESSION['group_user'])); 
  $akses_add_tab_ref = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>19,'kode_function_access'=>$_SESSION['group_user'])); 
  $akses_add_tab_backup = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>20,'kode_function_access'=>$_SESSION['group_user'])); 

  $akses_add_tab_dft_pg       = (!empty($akses_add_tab_dft_pg) ? $akses_add_tab_dft_pg : '0');
  $akses_add_tab_hak_akses    = (!empty($akses_add_tab_hak_akses) ? $akses_add_tab_hak_akses : '0');
  $akses_add_tab_sims         = (!empty($akses_add_tab_sims) ? $akses_add_tab_sims : '0');
  $akses_add_tab_company      = (!empty($akses_add_tab_company) ? $akses_add_tab_company : '0');
  $akses_add_tab_upt          = (!empty($akses_add_tab_upt) ? $akses_add_tab_upt : '0');
  $akses_edit_tab_website     = (!empty($akses_edit_tab_website) ? $akses_edit_tab_website : '0');
  $akses_add_tab_ref          = (!empty($akses_add_tab_ref) ? $akses_add_tab_ref : '0');
  $akses_add_tab_backup       = (!empty($akses_add_tab_backup) ? $akses_add_tab_backup : '0');

  $readonly_web="";
  if($akses_edit_tab_website==0){ 
      $readonly_web=" readonly ";
   } 
  ?>
  
  <!-- Main Container -->
  <div class="container-fluid animated fadeInUp" style="z-index: 3;">
    <div class="row">
  <?php  
    include "view/menu.php";  
   ?>
  <!-- Content -->
    <div class="container-fluid main-content" id="div_content" >
      <div class="row">

        <ul class="nav nav-tabs main-nav-tab" id="myTab" role="tablist">
           <?php 
              $data_tb = $db->SelectLimit("select * from ms_menu_tab where men_id='6' order by urutan asc"); 
              $no_tab = 1;
              $men_active =""; 
              $total_tab = 0;
              while($list = $data_tb->FetchRow()){
                  foreach($list as $key=>$val){
                        $key=strtolower($key);
                        $$key=$val;
                      } 
                      $whr_tab = array();
                      $whr_tab['kode_function_access'] = $_SESSION['group_user'];
                      $whr_tab['id_tab'] = $tab_id;
                      $check_tab = $gen_model->GetOne("fua_read","tr_function_access_tab",$whr_tab);
                     if(!empty($check_tab)){ 
                      $active_tab ="";   
                      if($no_tab==1) {
                           $active_tab ="active";
                           $men_active = $tab_url;   
                      }
                      ?>  
                      <li class="nav-item">
                        <a class="nav-link <?php echo $active_tab ?>" onclick="refresh_table()" id="<?php echo $tab_url ?>-tab" data-toggle="tab" href="#<?php echo $tab_url ?>" role="tab" aria-controls="<?php echo $tab_url ?>" aria-selected="true"><?php echo $tab ?></a>
                      </li>
                   <?php $no_tab++;
                   $total_tab++;      
                 } 
              } 

              function active_tab($list_tab){
                    global $men_active;
                    if($men_active==$list_tab){
                       return "show active";
                    }
              }
              ?>
        </ul>

         <?php  if($total_tab!=0){  ?>
        <div class="tab-content" id="myTabContent" style="padding-top:0px">
          <!-- Tab Daftar Pengguna -->
          <div class="tab-pane fade <?php echo active_tab("daftar_pengguna") ?>" id="daftar_pengguna" role="tabpanel" aria-labelledby="home-tab">
          
            <input type="hidden" name="DaftarPenggunaPencarian" id="DaftarPenggunaPencarian" >
            <script type="text/javascript">
              function print_laporan_pengguna(tipe){
                  var param = $("#DaftarPenggunaPencarian").val();


                    var form = document.createElement("form");
                    form.setAttribute("method", "POST");
                    if(tipe=="print"){
                     form.setAttribute("target", "_blank");
                    }
                    form.setAttribute("action", "<?php echo $basepath ?>daftar_pengguna/cetak");
                           
                        var hiddenField = document.createElement("input");
                        hiddenField.setAttribute("type", "hidden");
                        hiddenField.setAttribute("name", "param");
                        hiddenField.setAttribute("value", param);

                        var hiddenField2 = document.createElement("input");
                        hiddenField2.setAttribute("type", "hidden");
                        hiddenField2.setAttribute("name", "tipe");
                        hiddenField2.setAttribute("value", tipe);

                        form.appendChild(hiddenField);
                        form.appendChild(hiddenField2);

                    document.body.appendChild(form);
                    form.submit();
                }
            </script>
            <!-- Tabel -->
            <div class="cnt-heaeder" style="margin-top: 30px; margin-bottom: 20px;">
              <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-users"></i> Daftar Pengguna</h6>
              
              <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">

                    <?php  if($akses_add_tab_dft_pg=="1"){  ?>
                     <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Tambah" onclick="call_modal_default('Tambah Pengguna','Form_Pengguna','do_add','daftar_pengguna/form')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                    <?php } ?>

                   <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Cetak" onclick="print_laporan_pengguna('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>
                  <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Filterisasi" data-target="#Filter_User"  ><i class="fa fa-filter"></i></a>
                <div class="btn-group" role="group" data-tooltip="tooltip" data-placement="bottom" title="Export">
                  <button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-file-pdf"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="red">
                    <a class="dropdown-item"  onclick="print_laporan_pengguna('pdf')" href="javascript:void(0)">Export to PDF</a>
                    <a class="dropdown-item"   onclick="print_laporan_pengguna('xls')" href="javascript:void(0)">Export to XLS</a>
                    <a class="dropdown-item" onclick="print_laporan_pengguna('doc')"   href="javascript:void(0)">Export to DOCS</a>
                  </div>
                </div>
              </div>
              <hr style="width: 100%; float: left; display: block;">
            </div>

            <div id="filter_pengguna" class="table table-responsive pelayanan-top">
               <table id="tb_user" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
                    <th>Username</th>
                    <th>Nama</th>
                    <th>Level</th>
                    <th>Status</th>
                    <th>Provinsi</th>
                    <th>UPT</th>
                    <th>No.Tlp</th>
                    <th>No.Hp</th>
                    <th>Email</th>
                    <th>Tanggal Buat</th>
                    <th>Tanggal Ubah</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            <script type="text/javascript">
              var dTable;
              $(document).ready(function() {
                dTable = $('#tb_user').DataTable( {
                  "bProcessing": true,
                  "bServerSide": true,
                  "bJQueryUI": false,
                  "responsive": false,
                  "autoWidth": false,
                  "sAjaxSource": "<?php echo $basepath ?>daftar_pengguna/rest", 
                  "sServerMethod": "POST",
                  "scrollX": true,
                  "searching": false,
                  "scrollY": "350px",
                    "scrollCollapse": true,
                  "columnDefs": [
                  { "orderable": true, "targets": 0, "searchable": true},
                  { "orderable": true, "targets": 1, "searchable": true},
                  { "orderable": true, "targets": 2, "searchable": true},
                  { "orderable": true, "targets": 3, "searchable": true},
                  { "orderable": true, "targets": 4, "searchable": true},
                  { "orderable": true, "targets": 5, "searchable": true},
                  { "orderable": true, "targets": 6, "searchable": true},
                  { "orderable": true, "targets": 7, "searchable": true},
                  { "orderable": true, "targets": 8, "searchable": true},
                  { "orderable": true, "targets": 9, "searchable": true},
                  { "orderable": true, "targets": 10, "searchable": true},
                  { "orderable": false, "targets": 11, "searchable": false, "width":170}
                  ]
                } );
              });
            </script>

            </div>  
            <script type="text/javascript">
              function do_edit_user(param_id,map_id){
                call_modal_default('Ubah Pengguna','Form_Pengguna','do_edit','daftar_pengguna/form');
                 setTimeout(function(){ 
                     $.ajax({
                          url: '<?php echo $basepath ?>daftar_pengguna/edit/'+param_id,
                          type: 'POST',
                          dataType: 'JSON',
                          success: function(data) {
                              $("#kode_user").val(data.kode_user);
                              $("#username").val(data.username);
                              $("#nama_lengkap").val(data.nama_lengkap);
                              $("#status").val(data.status);
                              $("#group_user").val(data.group_user);
                              $("#upt_provinsi").val(data.upt_provinsi);
                              $("#no_tlp").val(data.no_tlp);
                              $("#no_hp").val(data.no_hp);
                              $("#no_fax").val(data.no_fax);
                              $("#email").val(data.email);
                              $("#alamat").val(data.alamat);
                                if(data.group_user=="grp_171026194411_1143"  || data.group_user=="grp_171026194411_1144"){
                                  $("#div_upt_provinsi").show();
                                  $("#div_upt").show();
                                  $.ajax({
                                       type: "POST",
                                       dataType: "html",
                                       url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
                                       data: "provinsi="+data.upt_provinsi,
                                       success: function(msg){
                                          if(msg){
                                            $("#kode_upt").html(msg);

                                            setTimeout(function(){ 
                                                 $("#kode_upt").val(data.kode_upt);        
                                            }, 2000);
                                           
                                          }
                                       }
                                    }); 

                                }
                          }
                      });
                 }, 1000);
              }
              function do_delete_user(parameter_data,param_id){
                  act_delete('Hapus Pengguna','Anda ingin menghapus Pengguna ? ','warning','daftar_pengguna/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
              }
            </script>  
          </div>

          <!-- Tab Hak Akses -->
          <div class="tab-pane fade <?php echo active_tab("hak_akses") ?>" id="hak_akses" role="tabpanel" aria-labelledby="hak_akses-tab">

             <div class="cnt-heaeder" style="margin-top: 30px; margin-bottom: 20px;">
              <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-lock"></i> Hak Akses</h6>
              
              <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">
                    <?php  if($akses_add_tab_hak_akses=="1"){  ?>
                     <a data-tooltip="tooltip" data-placement="bottom" title="Tambah" href="javascript:void(0)"  onclick="call_modal_default_no_full('Tambah Hak Akses','Form_Hak_Akses','do_add','hak_akses/form')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                  <?php } ?>
              </div>
              <hr style="width: 100%; float: left; display: block;">
            </div>
         

            <div id="filter_hak_akses" class="table table-responsive pelayanan-top">
                 <table id="tb_hak_akses" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
                    <th>Hak Akses</th>
                    <th>Tanggal Buat</th>
                    <th>Tanggal Ubah</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
           

            <script type="text/javascript">
              var dTable;
              $(document).ready(function() {
                dTable = $('#tb_hak_akses').DataTable( {
                  "bProcessing": true,
                  "bServerSide": true,
                  "bJQueryUI": false,
                  "searching":false,
                  "responsive": false,
                  "autoWidth": false,
                  "sAjaxSource": "<?php echo $basepath ?>hak_akses/rest", 
                  "sServerMethod": "POST",
                  "scrollX": true,
                  "bLengthChange": false,
                  "bPaginate": false,
                  "info": false,
                  "scrollY": "350px",
                    "scrollCollapse": true,
                  "columnDefs": [
                  { "orderable": true, "targets": 0, "searchable": true},
                  { "orderable": true, "targets": 1, "searchable": true},
                  { "orderable": true, "targets": 2, "searchable": true},
                  { "orderable": false, "targets": 3, "searchable": false, "width":170}
                  ]
                } );
              });
            </script>
            </div>    
          </div>

          <!-- Tab SIMS -->
          <div class="tab-pane fade <?php echo active_tab("sims") ?>" id="sims" role="tabpanel" aria-labelledby="sims-tab">
            <!-- Tabel -->
            <div class="cnt-heaeder" style="margin-top: 30px; margin-bottom: 20px;">
              <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-file"></i> Sims</h6>
              
              <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">

                  <?php  if($akses_add_tab_sims=="1"){  ?>
                    <a data-tooltip="tooltip" data-placement="bottom" title="Export" href="javascript:void(0)"  onclick="call_modal_default('Tambah SIMS','Form_SIMS','do_add','sims/form')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                  <?php } ?>

                  <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Cetak" onclick="print_laporan_sims('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

                  <?php  if($akses_add_tab_sims=="1"){  ?>
                     <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Import" data-target="#Import_Sims"><i class="fa fa-upload"></i></a>
                  <?php } ?>

                  <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Filterisasi" data-target="#Filter_Sims"  ><i class="fa fa-filter"></i></a>

                  <div class="btn-group" role="group" data-tooltip="tooltip" data-placement="bottom" title="Export">
                  <button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-file-pdf"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="red">
                    <a class="dropdown-item"  onclick="print_laporan_sims('pdf')" href="javascript:void(0)">Export to PDF</a>
                    <a class="dropdown-item"   onclick="print_laporan_sims('xls')" href="javascript:void(0)">Export to XLS</a>
                    <a class="dropdown-item" onclick="print_laporan_sims('doc')"   href="javascript:void(0)">Export to DOCS</a>
                  </div>
                </div>
              </div>
              <hr style="width: 100%; float: left; display: block;">
            </div>

            <div id="filter_sims" class="table table-responsive pelayanan-top">
               <table id="tb_sims" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
                    <th>No SPP</th>
                    <th>No Klien Licence</th>
                    <th>Perusahaan</th>
                    <th>Request Reff</th>
                    <th>Licence State</th>
                    <th>Tagihan</th>
                    <th>Status</th>
                    <th>Tanggal Begin</th>
                    <th>Status Izin</th>
                    <th>Kategori SPP</th>
                    <th>Service</th>
                    <th>Sub Service</th>
                    <th>Created Date</th>
                    <th>Last Update</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            <script type="text/javascript">
              var dTable;
              $(document).ready(function() {
                dTable = $('#tb_sims').DataTable( {
                  "bProcessing": true,
                  "bServerSide": true,
                  "bJQueryUI": false,
                  "responsive": false,
                  "autoWidth": false,
                  "sAjaxSource": "<?php echo $basepath ?>sims/rest", 
                  "sServerMethod": "POST",
                  "scrollX": true,
                  "searching": false,
                  "scrollY": "350px",
                    "scrollCollapse": true,
                  "columnDefs": [
                  { "orderable": true, "targets": 0, "searchable": true},
                  { "orderable": true, "targets": 1, "searchable": true},
                  { "orderable": true, "targets": 2, "searchable": true},
                  { "orderable": true, "targets": 3, "searchable": true},
                  { "orderable": true, "targets": 4, "searchable": true},
                  { "orderable": true, "targets": 5, "searchable": true},
                  { "orderable": true, "targets": 6, "searchable": true},
                  { "orderable": true, "targets": 7, "searchable": true},
                  { "orderable": true, "targets": 8, "searchable": true},
                  { "orderable": true, "targets": 9, "searchable": true},
                  { "orderable": true, "targets": 10, "searchable": true},
                  { "orderable": true, "targets": 11, "searchable": true},
                  { "orderable": true, "targets": 12, "searchable": true},
                  { "orderable": true, "targets": 13, "searchable": true},
                  { "orderable": false, "targets": 14, "searchable": false, "width":170}
                  ]
                } );
              });
            </script>
            </div>  
            <script type="text/javascript">
              function do_detail_sims(param_id){
                call_modal_default('Detail SIMS','Form_SIMS','do_detail','sims/form');
                 setTimeout(function(){ 
                     $.ajax({
                          url: '<?php echo $basepath ?>sims/edit/'+param_id,
                          type: 'POST',
                          dataType: 'JSON',
                          success: function(data) {
                              $("#kode_sims").val(data.kode_sims);
                              $("#no_spp").val(data.no_spp);
                              $("#no_client").val(data.no_klien_licence);
                              $("#no_aplikasi").val(data.no_aplikasi);
                              $("#kode_perusahaan").val(data.kode_perusahaan);
                              $("#group_user").val(data.group_user);
                              $("#nama_perusahaan").val(data.cmp);
                              $("#request_reff").val(data.request_reff);
                              $("#licence_state").val(data.licence_state);
                              $("#tagihan").val(data.tagihan);
                              $("#status").val(data.status);
                              $("#tgl_begin").val(data.tgl_begin);
                              $("#status").val(data.status);
                              $("#status_izin").val(data.status_izin);
                              $("#kategori_spp").val(data.kategori_spp);
                              $("#service").val(data.service);
                              $("#subservice").val(data.subservice);
                          }
                      });
                 }, 1000);
              }
              function do_edit_sims(param_id,map_id){
                call_modal_default('Ubah SIMS','Form_SIMS','do_edit','sims/form');
                 setTimeout(function(){ 
                     $.ajax({
                          url: '<?php echo $basepath ?>sims/edit/'+param_id,
                          type: 'POST',
                          dataType: 'JSON',
                          success: function(data) {
                              $("#kode_sims").val(data.kode_sims);
                              $("#no_spp").val(data.no_spp);
                              $("#no_client").val(data.no_klien_licence);
                              $("#no_aplikasi").val(data.no_aplikasi);
                              $("#kode_perusahaan").val(data.kode_perusahaan);
                              $("#group_user").val(data.group_user);
                              $("#nama_perusahaan").val(data.cmp);
                              $("#request_reff").val(data.request_reff);
                              $("#licence_state").val(data.licence_state);
                              $("#tagihan").val(data.tagihan);
                              $("#status").val(data.status);
                              $("#tgl_begin").val(data.tgl_begin);
                              $("#status").val(data.status);
                              $("#status_izin").val(data.status_izin);
                              $("#kategori_spp").val(data.kategori_spp);
                              $("#service").val(data.service);
                              $("#subservice").val(data.subservice);
                          }
                      });
                 }, 1000);
              }
              function do_delete_sims(parameter_data,param_id){
                 act_delete('Hapus SIMS','Anda ingin menghapus DATA SIMS ? ','warning','sims/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
              }
            </script>  
          </div>

          <!-- Tab Perusahaan -->
          <div class="tab-pane fade <?php echo active_tab("company") ?>" id="company" role="tabpanel" aria-labelledby="company-tab">
            <div class="cnt-heaeder" style="margin-top: 30px; margin-bottom: 20px;">
              <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-briefcase"></i> Perusahaan</h6>
               <input type="hidden" name="CompanyPencarian" id="CompanyPencarian" >
                <script type="text/javascript">
                  function print_laporan_company(tipe){
                      var param = $("#CompanyPencarian").val();

                        var form = document.createElement("form");
                        form.setAttribute("method", "POST");
                        if(tipe=="print"){
                         form.setAttribute("target", "_blank");
                        }
                        form.setAttribute("action", "<?php echo $basepath ?>company/cetak");
                               
                            var hiddenField = document.createElement("input");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("name", "param");
                            hiddenField.setAttribute("value", param);

                            var hiddenField2 = document.createElement("input");
                            hiddenField2.setAttribute("type", "hidden");
                            hiddenField2.setAttribute("name", "tipe");
                            hiddenField2.setAttribute("value", tipe);

                            form.appendChild(hiddenField);
                            form.appendChild(hiddenField2);

                        document.body.appendChild(form);
                        form.submit();
                    }
                </script>
              <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">

                  <?php  if($akses_add_tab_company=="1"){  ?>
                     <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Tambah"  onclick="call_modal_default('Tambah Perusahaan','Form_Perusahaan','do_add','company/form')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                  <?php } ?>

                  <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Cetak" onclick="print_laporan_company('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

                  <!--
                     <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" title="Import" data-target="#Import_Company"><i class="fa fa-upload"></i></a>
                  -->

                  <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Filterisasi" data-target="#Filter_Perusahaan"  ><i class="fa fa-filter"></i></a>

                  <div class="btn-group" role="group" data-tooltip="tooltip" data-placement="bottom" title="Export">
                  <button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-file-pdf"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="red">
                    <a class="dropdown-item"  onclick="print_laporan_company('pdf')" href="javascript:void(0)">Export to PDF</a>
                    <a class="dropdown-item"   onclick="print_laporan_company('xls')" href="javascript:void(0)">Export to XLS</a>
                    <a class="dropdown-item" onclick="print_laporan_company('doc')"   href="javascript:void(0)">Export to DOCS</a>
                  </div>
                </div>
              </div>
              <hr style="width: 100%; float: left; display: block;">
            </div>

            <div id="filter_company" class="table table-responsive pelayanan-top">
               <table id="tb_company" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
                    <th>No Klien Licence</th>
                    <th>Nama Perusahaan</th>
                    <th>No Telp</th>
                    <th>No Hp</th>
                    <th>Provinsi</th>
                    <th>Kota/Kabupaten</th>
                    <th>Alamat</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            <script type="text/javascript">
              var dTable;
              $(document).ready(function() {
                dTable = $('#tb_company').DataTable( {
                  "bProcessing": true,
                  "bServerSide": true,
                  "bJQueryUI": false,
                  "responsive": false,
                  "autoWidth": false,
                  "sAjaxSource": "<?php echo $basepath ?>company/rest", 
                  "sServerMethod": "POST",
                  "scrollX": true,
                  "searching": false,
                  "scrollY": "350px",
                    "scrollCollapse": true,
                  "columnDefs": [
                    { "orderable": true, "targets": 0, "searchable": true},
                    { "orderable": true, "targets": 1, "searchable": true},
                    { "orderable": true, "targets": 2, "searchable": true},
                    { "orderable": true, "targets": 3, "searchable": true},
                    { "orderable": true, "targets": 4, "searchable": true },
                    { "orderable": true, "targets": 5, "searchable": true },
                    { "orderable": true, "targets": 6, "searchable": true },
                    { "orderable": false, "targets": 7, "searchable": false, "width":170}
                  ]
                } );
              });
            </script>
            </div>  
            <script type="text/javascript">
              function do_detail_company(param_id){
                call_modal_default('Detail Perusahaan','Form_Perusahaan','do_detail','company/form');
                 setTimeout(function(){ 
                     $.ajax({
                          url: '<?php echo $basepath ?>company/edit/'+param_id,
                          type: 'POST',
                          dataType: 'JSON',
                          success: function(data) {
                              $("#company_id").val(data.company_id);
                              $("#nama_perusahaan").val(data.company_name);
                              $("#co_no_tlp").val(data.no_tlp);
                              $("#co_no_hp").val(data.no_hp);
                              $("#co_provinsi").val(data.id_provinsi);
                              $("#co_alamat").val(data.alamat);
                                $.ajax({
                                 type: "POST",
                                 dataType: "html",
                                 url: "<?php echo $basepath ?>daftar_pengguna/get_kabkot",
                                 data: "provinsi="+data.id_provinsi,
                                 success: function(msg){
                                    if(msg){
                                      $("#co_kabkot").html(msg);
                                      setTimeout(function(){ 
                                           $("#co_kabkot").val(data.id_kabkot);        
                                      }, 500);
                                     
                                    }
                                 }
                              }); 
                          }
                      });
                 }, 1000);
              }
              function do_edit_company(param_id,map_id){
                call_modal_default('Ubah Perusahaan','Form_Perusahaan','do_edit','company/form');
                 setTimeout(function(){ 
                     $.ajax({
                          url: '<?php echo $basepath ?>company/edit/'+param_id,
                          type: 'POST',
                          dataType: 'JSON',
                          success: function(data) {
                              $("#company_id").val(data.company_id);
                              $("#company_id_temp").val(data.company_id);
                              $("#nama_perusahaan").val(data.company_name);
                              $("#co_no_tlp").val(data.no_tlp);
                              $("#co_no_hp").val(data.no_hp);
                              $("#co_provinsi").val(data.id_provinsi);
                              $("#co_alamat").val(data.alamat);
                                $.ajax({
                                 type: "POST",
                                 dataType: "html",
                                 url: "<?php echo $basepath ?>daftar_pengguna/get_kabkot",
                                 data: "provinsi="+data.id_provinsi,
                                 success: function(msg){
                                    if(msg){
                                      $("#co_kabkot").html(msg);
                                      setTimeout(function(){ 
                                           $("#co_kabkot").val(data.id_kabkot);        
                                      }, 500);
                                     
                                    }
                                 }
                              }); 
                          }
                      });
                 }, 1000);
              }
              function do_delete_company(parameter_data,param_id){
                 act_delete('Hapus Perusahaan','Anda ingin menghapus Data Perusahaan ? ','warning','company/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
              }
            </script>  
          </div>


          <!-- Tab UPT -->
          <div class="tab-pane fade <?php echo active_tab("upt") ?>" id="upt" role="tabpanel" aria-labelledby="upt-tab">
             <div class="cnt-heaeder" style="margin-top: 30px; margin-bottom: 20px;">
              <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-briefcase"></i> UPT</h6>
               <input type="hidden" name="UPTPencarian" id="UPTPencarian" >
                <script type="text/javascript">
                  function print_laporan_upt(tipe){
                      var param = $("#UPTPencarian").val();

                        var form = document.createElement("form");
                        form.setAttribute("method", "POST");
                        if(tipe=="print"){
                         form.setAttribute("target", "_blank");
                        }
                        form.setAttribute("action", "<?php echo $basepath ?>upt/cetak");
                               
                            var hiddenField = document.createElement("input");
                            hiddenField.setAttribute("type", "hidden");
                            hiddenField.setAttribute("name", "param");
                            hiddenField.setAttribute("value", param);

                            var hiddenField2 = document.createElement("input");
                            hiddenField2.setAttribute("type", "hidden");
                            hiddenField2.setAttribute("name", "tipe");
                            hiddenField2.setAttribute("value", tipe);

                            form.appendChild(hiddenField);
                            form.appendChild(hiddenField2);

                        document.body.appendChild(form);
                        form.submit();
                    }
                </script>
              <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">

                  <?php  if($akses_add_tab_upt=="1"){  ?>
                     <a data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Tambah" href="javascript:void(0)"  onclick="call_modal_default('Tambah UPT','Form_UPT','do_add','upt/form')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                  <?php } ?>

                  <a href="javascript:void(0)" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Cetak" onclick="print_laporan_upt('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

                  <!--
                     <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" title="Import" data-target="#Import_Company"><i class="fa fa-upload"></i></a>
                  -->

                  <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Filterisasi" data-target="#Filter_UPT"  ><i class="fa fa-filter"></i></a>

                  <div class="btn-group" role="group" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Export">
                  <button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-file-pdf"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="red">
                    <a class="dropdown-item"  onclick="print_laporan_upt('pdf')" href="javascript:void(0)">Export to PDF</a>
                    <a class="dropdown-item"   onclick="print_laporan_upt('xls')" href="javascript:void(0)">Export to XLS</a>
                    <a class="dropdown-item" onclick="print_laporan_upt('doc')"   href="javascript:void(0)">Export to DOCS</a>
                  </div>
                </div>
              </div>
              <hr style="width: 100%; float: left; display: block;">
            </div>


            <div id="filter_upt" class="table table-responsive pelayanan-top">
               <table id="tb_upt" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
                    <th>Kode Provinsi</th>
                    <th>Provinsi</th>
                    <th>Kode UPT</th>
                    <th>Nama UPT</th>
                    <th>No Telp</th>
                    <th>No Fax</th>
                    <th>Email</th>
                    <th>Alamat</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            <script type="text/javascript">
              var dTable;
              $(document).ready(function() {
                dTable = $('#tb_upt').DataTable( {
                  "bProcessing": true,
                  "bServerSide": true,
                  "bJQueryUI": false,
                  "responsive": false,
                  "autoWidth": false,
                  "bLengthChange": false,
                  "bPaginate": false,
                  "sAjaxSource": "<?php echo $basepath ?>upt/rest", 
                  "sServerMethod": "POST",
                  "scrollX": true,
                  "searching": false,
                  "scrollY": "350px",
                    "scrollCollapse": true,
                  "columnDefs": [
                    { "orderable": true, "targets": 0, "searchable": true},
                    { "orderable": true, "targets": 1, "searchable": true},
                    { "orderable": true, "targets": 2, "searchable": true},
                    { "orderable": true, "targets": 3, "searchable": true},
                    { "orderable": true, "targets": 4, "searchable": true },
                    { "orderable": true, "targets": 5, "searchable": true },
                    { "orderable": true, "targets": 6, "searchable": true },
                    { "orderable": true, "targets": 7, "searchable": true },
                    { "orderable": true, "targets": 8, "searchable": true }
                  ]
                } );
              });
            </script>
            </div> 
            <script type="text/javascript">
              function do_detail_upt(param_id){
                call_modal_default('Detail UPT','Form_UPT','do_detail','upt/form');
                 setTimeout(function(){ 
                     $.ajax({
                          url: '<?php echo $basepath ?>upt/edit/'+param_id,
                          type: 'POST',
                          dataType: 'JSON',
                          success: function(data) {
                              $("#kode_upt").val(data.kode_upt);
                              $("#nama_upt").val(data.nama_upt);
                              $("#upt_no_fax").val(data.no_fax);
                              $("#upt_no_tlp").val(data.no_tlp);
                              $("#upt_email").val(data.email);
                              $("#upt_provinsi").val(data.id_prov);
                              $("#upt_alamat").val(data.alamat);
                          }
                      });
                 }, 1000);
              }
              function do_edit_upt(param_id,map_id){
                call_modal_default('Ubah UPT','Form_UPT','do_edit','upt/form');
                 setTimeout(function(){ 
                     $.ajax({
                          url: '<?php echo $basepath ?>upt/edit/'+param_id,
                          type: 'POST',
                          dataType: 'JSON',
                          success: function(data) {
                              $("#kode_upt").val(data.kode_upt);
                              $("#nama_upt").val(data.nama_upt);
                              $("#upt_no_fax").val(data.no_fax);
                              $("#upt_no_tlp").val(data.no_tlp);
                              $("#upt_email").val(data.email);
                              $("#upt_provinsi").val(data.id_prov);
                              $("#upt_alamat").val(data.alamat);
                          }
                      });
                 }, 1000);
              }
              function do_delete_upt(parameter_data,param_id){
                 act_delete('Hapus UPT','Anda ingin menghapus Data UPT ? ','warning','upt/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
              }
            </script>   
          </div>


          <!-- Tab Website -->
          <div class="tab-pane fade <?php echo active_tab("website") ?>" id="website" role="tabpanel" aria-labelledby="website-tab">
            <!-- Tabel -->
            <div class="cnt-heaeder" style="margin-top: 30px; margin-bottom: 20px;">
              <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-portrait"></i> Website</h6>
            </div><br/>

            <form method="POST" id="ubah_data_web"  autocomplete="off" enctype="multipart/form-data">

                 <div  class="table table-responsive pelayanan-top">
                  <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-12">
                        <span class="label">Judul</span> <span class="required">*</span>
                        <input  class="form-control" maxlength="255" id="judul" name="judul"   placeholder="" type="text" value="<?php echo $web['judul'] ?>" required <?php echo $readonly_web ?>>
                    </div>
                  </div>
                  <br/>
                  <center><h4>Contact</h4></center>
                  <div class="row">
                    <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">No. Tlp</span> <span class="required">*</span>
                        <input  class="form-control" maxlength="25" id="no_tlp" name="no_tlp"   placeholder="" type="text" value="<?php echo $web['no_tlp'] ?>" required <?php echo $readonly_web ?>>
                    </div>
                    <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">No. Hp</span> 
                        <input  class="form-control" maxlength="25" id="no_hp" name="no_hp"   placeholder="" type="text" value="<?php echo $web['no_hp'] ?>"  <?php echo $readonly_web ?>>
                    </div>
                    <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">No. Fax</span>
                        <input  class="form-control" maxlength="25" id="no_fax" name="no_fax"   placeholder="" type="text" value="<?php echo $web['no_fax'] ?>"  <?php echo $readonly_web ?>>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 form-group form-box col-xs-12">
                        <span class="label">Email</span> <span class="required">*</span>
                        <input  class="form-control" maxlength="255" id="email" name="email"   placeholder="" type="email" value="<?php echo $web['email'] ?>" required <?php echo $readonly_web ?>>
                    </div>
                    <div class="col-md-6 form-group form-box col-xs-12">
                        <span class="label">Alamat</span> <span class="required">*</span>
                        <input  class="form-control"  id="alamat" name="alamat"   placeholder="" type="text" value="<?php echo $web['alamat'] ?>" required <?php echo $readonly_web ?>>
                    </div>
                  </div>
                  <br/>
                  <center><h4>Running Text</h4></center>
                  <div class="row">
                    <div class="col-md-6 form-group form-box col-xs-12">
                        <span class="label">Running Text</span> 
                        <input  class="form-control" maxlength="255" id="running_text" name="running_text"   placeholder="" type="text" value="<?php echo $web['running_text'] ?>" <?php echo $readonly_web ?>>
                    </div>
                    <div class="col-md-2 form-group form-box col-xs-12">
                        <span class="label">Background Color</span> 
                        <input  class="form-control jscolor" id="running_text_color_bg" name="running_text_color_bg"   placeholder="" type="text" maxlength="6" value="<?php echo $web['running_text_color_bg'] ?>" <?php echo $readonly_web ?>>
                    </div> 
                    <div class="col-md-2 form-group form-box col-xs-12">
                        <span class="label">Text Color</span> 
                        <input  class="form-control jscolor" id="running_text_color" name="running_text_color"   placeholder="" type="text" maxlength="6" value="<?php echo $web['running_text_color'] ?>" <?php echo $readonly_web ?>>
                    </div>
                    <div class="col-md-2 form-group form-box col-xs-12">
                        <span class="label">Running Text Size</span>
                        <input  class="form-control" min="1" id="running_text_size" name="running_text_size"   placeholder="" type="number" value="<?php echo $web['running_text_size'] ?>" <?php echo $readonly_web ?>>
                    </div>
                  </div>
                  <br/>
                  <center><h4>Information</h4></center>
                  <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-12">
                        <span class="label">Note Pengisian (SPP/ISR/Revoke)</span> <span class="required">*</span>
                        <textarea class="form-control ckeditor" id="note_pengisian" name="note_pengisian" <?php echo $readonly_web ?>><?php echo $web['note_pengisian'] ?></textarea>   
                    </div>
                  </div> 

                  <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-12">
                        <p align="right"><button type="submit" class="btn btn-success">Simpan</button></p>
                    </div>
                  </div>

                </div>

            </form>

          </div>

           <!-- Tab Referensi -->
          <div class="tab-pane fade <?php echo active_tab("referensi") ?>" id="referensi" role="tabpanel" aria-labelledby="referensi-tab">

                <!-- Tabel Kategori SPP-->
                <div class="cnt-heaeder" style="margin-top: 30px; margin-bottom: 20px;">
                  <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-file"></i> Kategori SPP</h6>
                  
                  <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">

                    <?php  if($akses_add_tab_ref=="1"){  ?>
                         <a data-tooltip="tooltip" data-placement="bottom" title="Tambah" href="javascript:void(0)"  onclick="call_modal_default_no_full('Tambah Kategori SPP','Form_Kategori_SPP','do_add','kategori_spp/form')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                      <?php } ?>
                  </div>
                  <hr style="width: 100%; float: left; display: block;">
                </div>

                <div id="" class="table table-responsive pelayanan-top">
                   <table id="tb_kategori_spp" class="display table table-striped pelayanan-table">
                      <thead>
                        <tr>
                          <th>Kode Kategori SPP</th>
                          <th>Kategori SPP</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                    </table>
                  <script type="text/javascript">
                    var dTable;
                    $(document).ready(function() {
                      dTable = $('#tb_kategori_spp').DataTable( {
                        "bProcessing": true,
                        "bServerSide": true,
                        "bJQueryUI": false,
                        "responsive": false,
                        "autoWidth": false,
                        "sAjaxSource": "<?php echo $basepath ?>kategori_spp/rest", 
                        "sServerMethod": "POST",
                        "scrollX": true,
                        "searching": true,
                        "scrollY": "350px",
                        "scrollCollapse": true,
                        "columnDefs": [
                        { "orderable": true, "targets": 0, "searchable": true},
                        { "orderable": true, "targets": 1, "searchable": true},
                        { "orderable": false, "targets": 2, "searchable": false, "width":170}
                        ]
                      } );
                    });
                  </script>
                </div>

                 <!-- Tabel Materi-->
                <div class="cnt-heaeder" style="margin-top: 30px; margin-bottom: 20px;">
                  <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-file"></i> Materi</h6>
                  
                  <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">

                    <?php  if($akses_add_tab_ref=="1"){  ?>
                         <a data-tooltip="tooltip" data-placement="bottom" title="Tambah" href="javascript:void(0)"  onclick="call_modal_default_no_full('Tambah Materi','Form_Materi','do_add','materi/form')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                      <?php } ?>
                  </div>
                  <hr style="width: 100%; float: left; display: block;">
                </div>

                <div id="" class="table table-responsive pelayanan-top">
                   <table id="tb_materi" class="display table table-striped pelayanan-table">
                      <thead>
                        <tr>
                          <th>Kode Materi</th>
                          <th>Jenis Materi</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                    </table>
                  <script type="text/javascript">
                    var dTable;
                    $(document).ready(function() {
                      dTable = $('#tb_materi').DataTable( {
                        "bProcessing": true,
                        "bServerSide": true,
                        "bJQueryUI": false,
                        "responsive": false,
                        "autoWidth": false,
                        "sAjaxSource": "<?php echo $basepath ?>materi/rest", 
                        "sServerMethod": "POST",
                        "scrollX": true,
                        "searching": true,
                        "scrollY": "350px",
                        "scrollCollapse": true,
                        "columnDefs": [
                        { "orderable": true, "targets": 0, "searchable": true},
                        { "orderable": true, "targets": 1, "searchable": true},
                        { "orderable": false, "targets": 2, "searchable": false, "width":170}
                        ]
                      } );
                    });
                  </script>
                </div>

              <!-- Tabel Kegiatan -->
                <div class="cnt-heaeder" style="margin-top: 30px; margin-bottom: 20px;">
                  <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-file"></i> Kegiatan</h6>
                  
                  <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">

                    <?php  if($akses_add_tab_ref=="1"){  ?>
                         <a href="javascript:void(0)"  onclick="call_modal_default_no_full('Tambah Kegiatan','Form_Kegiatan','do_add','kegiatan/form')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                      <?php } ?>
                  </div>
                  <hr style="width: 100%; float: left; display: block;">
                </div>

                <div id="" class="table table-responsive pelayanan-top">
                   <table id="tb_kegiatan" class="display table table-striped pelayanan-table">
                    <thead>
                      <tr>
                      <th>Kode Kegiatan</th>
                      <th>Jenis Kegiatan</th>
                      <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                <script type="text/javascript">
                  var dTable;
                  $(document).ready(function() {
                    dTable = $('#tb_kegiatan').DataTable( {
                      "bProcessing": true,
                      "bServerSide": true,
                      "bJQueryUI": false,
                      "responsive": false,
                      "autoWidth": false,
                      "sAjaxSource": "<?php echo $basepath ?>kegiatan/rest", 
                      "sServerMethod": "POST",
                      "scrollX": true,
                      "searching": true,
                      "scrollY": "350px",
                        "scrollCollapse": true,
                      "columnDefs": [
                      { "orderable": true, "targets": 0, "searchable": true},
                      { "orderable": true, "targets": 1, "searchable": true},
                      { "orderable": false, "targets": 2, "searchable": false, "width":170}
                      ]
                    } );
                  });
                </script>
              </div>

              <!-- Tabel Metode -->
                <div class="cnt-heaeder" style="margin-top: 30px; margin-bottom: 20px;">
                  <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-file"></i> Metode</h6>
                  
                  <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">

                       <?php  if($akses_add_tab_ref=="1"){  ?>
                         <a href="javascript:void(0)"  data-tooltip="tooltip" data-placement="bottom" title="Tambah" onclick="call_modal_default_no_full('Tambah Metode','Form_Metode','do_add','metode/form')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                      <?php } ?>
                  </div>
                  <hr style="width: 100%; float: left; display: block;">
                </div>

                <div id="" class="table table-responsive pelayanan-top">
                   <table id="tb_metode" class="display table table-striped pelayanan-table">
                    <thead>
                      <tr>
                        <th>Kode Metode</th>
                        <th>Metode</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                  <script type="text/javascript">
                    var dTable;
                    $(document).ready(function() {
                      dTable = $('#tb_metode').DataTable( {
                        "bProcessing": true,
                        "bServerSide": true,
                        "bJQueryUI": false,
                        "responsive": false,
                        "autoWidth": false,
                        "sAjaxSource": "<?php echo $basepath ?>metode/rest", 
                        "sServerMethod": "POST",
                        "scrollX": true,
                        "searching": true,
                        "scrollY": "350px",
                          "scrollCollapse": true,
                        "columnDefs": [
                        { "orderable": true, "targets": 0, "searchable": true},
                        { "orderable": true, "targets": 1, "searchable": true},
                        { "orderable": false, "targets": 2, "searchable": false, "width":170}
                        ]
                      } );
                    });
                  </script>
                </div>

              <!-- Tabel Tujuan -->
                <div class="cnt-heaeder" style="margin-top: 30px; margin-bottom: 20px;">
                  <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-file"></i> Tujuan</h6>
                  
                  <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">

                       <?php  if($akses_add_tab_ref=="1"){  ?>
                         <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Tambah" onclick="call_modal_default_no_full('Tambah Tujuan','Form_Tujuan','do_add','tujuan/form')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                      <?php } ?>
                  </div>
                  <hr style="width: 100%; float: left; display: block;">
                </div>

                <div id="" class="table table-responsive pelayanan-top">
                   <table id="tb_tujuan" class="display table table-striped pelayanan-table">
                    <thead>
                      <tr>
                        <th>Kode Tujuan</th>
                        <th>Jenis Tujuan</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                  <script type="text/javascript">
                    var dTable;
                    $(document).ready(function() {
                      dTable = $('#tb_tujuan').DataTable( {
                        "bProcessing": true,
                        "bServerSide": true,
                        "bJQueryUI": false,
                        "responsive": false,
                        "autoWidth": false,
                        "sAjaxSource": "<?php echo $basepath ?>tujuan/rest", 
                        "sServerMethod": "POST",
                        "scrollX": true,
                        "searching": true,
                        "scrollY": "350px",
                          "scrollCollapse": true,
                        "columnDefs": [
                        { "orderable": true, "targets": 0, "searchable": true},
                        { "orderable": true, "targets": 1, "searchable": true},
                        { "orderable": false, "targets": 2, "searchable": false, "width":170}
                        ]
                      } );
                    });
                  </script>
                </div>

                <!-- Tabel Group Perizinan -->
                <div class="cnt-heaeder" style="margin-top: 30px; margin-bottom: 20px;">
                  <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-file"></i> Group Perizinan</h6>
                  
                  <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">
                      <?php  if($akses_add_tab_ref=="1"){  ?>
                         <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Tambah" onclick="call_modal_default_no_full('Tambah Group Perizinan','Form_Perizinan','do_add','grp_layanan/form')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                      <?php } ?>
                  </div>
                  <hr style="width: 100%; float: left; display: block;">
                </div>

                <div id="" class="table table-responsive pelayanan-top">
                   <table id="tb_grp_perizinan" class="display table table-striped pelayanan-table">
                    <thead>
                      <tr>
                        <th>Kode Group Perizinan</th>
                        <th>Group Perizinan</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                  <script type="text/javascript">
                    var dTable;
                    $(document).ready(function() {
                      dTable = $('#tb_grp_perizinan').DataTable( {
                        "bProcessing": true,
                        "bServerSide": true,
                        "bJQueryUI": false,
                        "responsive": false,
                        "autoWidth": false,
                        "sAjaxSource": "<?php echo $basepath ?>grp_layanan/rest_grp", 
                        "sServerMethod": "POST",
                        "scrollX": true,
                        "searching": true,
                        "scrollY": "350px",
                          "scrollCollapse": true,
                        "columnDefs": [
                        { "orderable": true, "targets": 0, "searchable": true},
                        { "orderable": true, "targets": 1, "searchable": true},
                        { "orderable": false, "targets": 2, "searchable": false, "width":170}
                        ]
                      } );
                    });
                  </script>
                </div>

                <!-- Tabel Sub Group Perizinan -->
                <!--<div class="cnt-heaeder" style="margin-top: 30px; margin-bottom: 20px;">
                  <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-file"></i> Sub Group Perizinan</h6>
                  
                  <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">

                     <?php  if($akses_add_tab_ref=="1"){  ?>
                         <a href="javascript:void(0)"  onclick="call_modal_default_no_full('Tambah Sub Group Perizinan','Form_Sub_Perizinan','do_add','grp_layanan/form')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                      <?php } ?>
                  </div>
                  <hr style="width: 100%; float: left; display: block;">
                </div>-->

               <!-- <div id="" class="table table-responsive pelayanan-top">
                   <table id="tb_sb_grp_perizinan" class="display table table-striped pelayanan-table">
                    <thead>
                      <tr>
                        <th>Kode Group Perizinan</th>
                        <th>Group Perizinan</th>
                        <th>Kode Sub Group Perizinan</th>
                        <th>Sub Group Perizinan</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                  <script type="text/javascript">
                    var dTable;
                    $(document).ready(function() {
                      dTable = $('#tb_sb_grp_perizinan').DataTable( {
                        "bProcessing": true,
                        "bServerSide": true,
                        "bJQueryUI": false,
                        "responsive": false,
                        "autoWidth": false,
                        "sAjaxSource": "<?php echo $basepath ?>grp_layanan/rest", 
                        "sServerMethod": "POST",
                        "scrollX": true,
                        "searching": true,
                        "scrollY": "350px",
                          "scrollCollapse": true,
                        "columnDefs": [
                        { "orderable": true, "targets": 0, "searchable": true},
                        { "orderable": true, "targets": 1, "searchable": true},
                        { "orderable": true, "targets": 2, "searchable": true},
                        { "orderable": false, "targets": 3, "searchable": false, "width":170}
                        ]
                      } );
                    });
                  </script>
                </div>
          </div>-->

         
        </div>

         <!-- Tab Backup -->
          <div class="tab-pane fade <?php echo active_tab("backup") ?>" id="backup" role="tabpanel" aria-labelledby="backup-tab">
            <!-- Tabel -->
            <div class="cnt-heaeder" style="margin-top: 30px; margin-bottom: 20px;">
              <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-portrait"></i> Backup</h6>
            </div>
        <br/><br/>
        <center>
          Klik to backup database<br/>
          <form method="POST" action="<?php echo $basepath ?>ajax/backup_database">
               <button type="submit" class="btn btn-success">Backup Database</button>
          </form>
        </center>
          </div>
          
        <?php } else { 
        echo "<div class='row'>
                <div class='col-md-12'>
                    <h3>Anda tidak mempunyai akses apapun pada halaman ini</h3>
                </div>
              </div>"; 
        echo "<script>
                var  height_main =  $('.pelayanan-cnt').height();
                     $('#div_content').height(height_main);
                     $('#myTab').remove();
              </script>";
       }?>
      </div>
    </div>
    </div>
  </div>

<script type="text/javascript">
   function do_delete_akses(parameter_data,param_id){
                 act_delete('Hapus Hak Akses','Anda ingin menghapus Hak Akses ? ','warning','hak_akses/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
     }
      function do_delete_kgp(parameter_data,param_id){
        act_delete('Hapus Kategori SPP','Anda ingin menghapus Kategori SPP ? ','warning','kategori_spp/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
    }
    function do_edit_kgp(param_id){
      call_modal_default_no_full('Ubah Kategori SPP','Form_Kategori_SPP','do_edit','kategori_spp/form');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>kategori_spp/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_kategori_spp").val(data.kode_kategori_spp);
                    $("#kgp").val(data.kategori_spp);
                }
            });
       }, 1000);
    } 
    function do_delete_mtr(parameter_data,param_id){
        act_delete('Hapus Materi','Anda ingin menghapus Materi ? ','warning','materi/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
    }
    function do_edit_mtr(param_id){
      call_modal_default_no_full('Ubah Materi','Form_Materi','do_edit','materi/form');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>materi/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_materi").val(data.kode_materi);
                    $("#materi").val(data.jenis_materi);
                }
            });
       }, 1000);
    } 
    function do_delete_mtd(parameter_data,param_id){
        act_delete('Hapus Metode','Anda ingin menghapus Metode ? ','warning','metode/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
    }
    function do_edit_mtd(param_id){
      call_modal_default_no_full('Ubah Metode','Form_Metode','do_edit','metode/form');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>metode/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#id_metode").val(data.id_metode);
                    $("#metode").val(data.metode);
                }
            });
       }, 1000);
    } 
    function do_delete_kgt(parameter_data,param_id){
        act_delete('Hapus Kegiatan','Anda ingin menghapus Kegiatan ? ','warning','kegiatan/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
    }
    function do_edit_kgt(param_id){
      call_modal_default_no_full('Ubah Kegiatan','Form_Kegiatan','do_edit','kegiatan/form');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>kegiatan/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_kegiatan").val(data.kode_kegiatan);
                    $("#kegiatan").val(data.kegiatan);
                }
            });
       }, 1000);
    } 
    function do_delete_tj(parameter_data,param_id){
        act_delete('Hapus Tujuan','Anda ingin menghapus Tujuan ? ','warning','tujuan/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
    }
    function do_edit_tj(param_id){
      call_modal_default_no_full('Ubah Tujuan','Form_Tujuan','do_edit','tujuan/form');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>tujuan/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_tujuan").val(data.kode_tujuan);
                    $("#tujuan").val(data.tujuan);
                }
            });
       }, 1000);
    }
    function do_delete_grp(parameter_data,param_id){
        act_delete('Hapus Group Perizinan','Anda ingin menghapus Group Perizinan ? ','warning','grp_layanan/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
    }
    function do_edit_grp(param_id){
      call_modal_default_no_full('Ubah Group Perizinan','Form_Perizinan','do_edit','grp_layanan/form');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>grp_layanan/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#id_jenis").val(data.id_jenis);
                    $("#jenis").val(data.jenis);
                }
            });
       }, 1000);
    } 
    function do_delete_sb_grp(parameter_data,param_id){
        act_delete('Hapus Sub Group Perizinan','Anda ingin menghapus Sub Group Perizinan ? ','warning','grp_layanan/delete_sb','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
    }
    function do_edit_sb_grp(param_id){
      call_modal_default_no_full('Ubah Sub Group Perizinan','Form_Sub_Perizinan','do_edit','grp_layanan/form');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>grp_layanan/edit_sb/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#id_jenis_sb").val(data.id_sub_group_layanan);
                    $("#jenis").val(data.id_jenis);
                    $("#jenis_sb").val(data.sub_group_layanan);
                }
            });
       }, 1000);
    }
function refresh_table(){
  setTimeout(function(){ 
    $('#tb_user').DataTable().ajax.reload();
    $('#tb_hak_akses').DataTable().ajax.reload();
    $('#tb_sims').DataTable().ajax.reload();
    $('#tb_company').DataTable().ajax.reload();
    $('#tb_upt').DataTable().ajax.reload();
    $('#tb_materi').DataTable().ajax.reload();
    $('#tb_kategori_spp').DataTable().ajax.reload();
    $('#tb_tujuan').DataTable().ajax.reload();
    $('#tb_kegiatan').DataTable().ajax.reload();
    $('#tb_grp_perizinan').DataTable().ajax.reload();
    $('#tb_sb_grp_perizinan').DataTable().ajax.reload();
    $('#tb_metode').DataTable().ajax.reload();
  }, 1000);
  $("#call_modal").modal("hide");
  $("#call_modal_no_full").modal("hide");
  $(".call_modal").modal("hide");
}
$("[type='number']").keypress(function (evt) {
    evt.preventDefault();
});
$("#ubah_data_web").on("submit", function (event) {
    event.preventDefault();
        do_act('ubah_data_web','setting/update_web','no_refresh','Ubah Data Website','Apakah anda ingin mengubah data website ?','info');
    });
$('[data-tooltip="tooltip"]').tooltip();
</script>