<?php  
 $check_akses = $gen_model->GetOne("fua_read","tr_function_access",array('id_menu'=>3,'kode_function_access'=>$_SESSION['group_user']));
 if(empty($check_akses)){
    echo '<meta http-equiv="refresh" content="0;url='.$basepath.'home">';
 }
 else { ?>
<title>Sosialisasi & Bimtek - <?php echo str_replace('<br/>','',$web['judul']) ?></title>


<style type="text/css">
  #example_paginate{position: relative; left: 40%; top: 5px;}
  .pagination li a{margin:5px 15px; }
  .dataTables_info{margin-bottom: 15px;}
  .datatable>tbody>tr>td
  {
    white-space: nowrap;
  } 
  .table th {
     text-align: center;
     font-weight:bold;
  }
</style>
<body class="pelayanan-cnt">
  <?php  
  
  include "view/top_menu.php";  
  $akses_add_tab_bhn_so = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>7,'kode_function_access'=>$_SESSION['group_user'])); 
  $akses_add_tab_rcn_so = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>8,'kode_function_access'=>$_SESSION['group_user'])); 
  $akses_add_tab_monev_so = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>9,'kode_function_access'=>$_SESSION['group_user'])); 
  $akses_add_tab_galeri = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>10,'kode_function_access'=>$_SESSION['group_user'])); 

  $akses_add_tab_bhn_so      = (!empty($akses_add_tab_bhn_so) ? $akses_add_tab_bhn_so : '0');
  $akses_add_tab_rcn_so      = (!empty($akses_add_tab_rcn_so) ? $akses_add_tab_rcn_so : '0');
  $akses_add_tab_monev_so    = (!empty($akses_add_tab_monev_so) ? $akses_add_tab_monev_so : '0');
  $akses_add_tab_galeri      = (!empty($akses_add_tab_galeri) ? $akses_add_tab_galeri : '0');
  ?>
  
  <!-- Main Container -->
  <div class="container-fluid animated fadeInUp" style="z-index: 3;" >
    <div class="row">
  <?php  
    include "view/menu.php";  
   ?>
  <!-- Content -->
    <div class="container-fluid main-content" id="div_content" >
      <div class="row">


        <ul class="nav nav-tabs main-nav-tab" id="myTab" role="tablist">
          <?php 
              $data_tb = $db->SelectLimit("select * from ms_menu_tab where men_id='3' order by urutan asc"); 
              $no_tab = 1;
              $men_active =""; 
              $total_tab = 0;
              while($list = $data_tb->FetchRow()){
                  foreach($list as $key=>$val){
                        $key=strtolower($key);
                        $$key=$val;
                      } 
                      $whr_tab = array();
                      $whr_tab['kode_function_access'] = $_SESSION['group_user'];
                      $whr_tab['id_tab'] = $tab_id;
                      $check_tab = $gen_model->GetOne("fua_read","tr_function_access_tab",$whr_tab);
                     if(!empty($check_tab)){ 
                      $active_tab ="";   
                      if($no_tab==1) {
                           $active_tab ="active";
                           $men_active = $tab_url;   
                      }
                      ?>  
                      <li class="nav-item">
                        <a class="nav-link <?php echo $active_tab ?>" onclick="refresh_table()" id="<?php echo $tab_url ?>-tab" data-toggle="tab" href="#<?php echo $tab_url ?>" role="tab" aria-controls="<?php echo $tab_url ?>" aria-selected="true"><?php echo $tab ?></a>
                      </li>
                   <?php $no_tab++;
                   $total_tab++;      
                 } 
              } 

              function active_tab($list_tab){
                    global $men_active;
                    if($men_active==$list_tab){
                       return "show active";
                    }
              }
              ?>
        </ul>


         <?php  if($total_tab!=0){  ?>
          <div class="tab-content" id="myTabContent">
            <!-- Tab Bahan Sosialisasi -->
            <div class="tab-pane fade <?php echo active_tab("bhn_sosialisasi") ?>" id="bhn_sosialisasi" role="tabpanel" aria-labelledby="home-tab">
             
              <!-- Peta -->
              <label class="judul-peta if_upt bhn_sosialisasi_utama"><i class="fa fa-map-marked-alt"></i> Peta Lokasi UPT</label>
              <hr class="if_upt bhn_sosialisasi_utama">
              <div class="peta-cnt" id="peta_bhn_sosialisasi"></div>

              <!-- Grafik Chart -->
              <!-- <div class="row if_upt bhn_sosialisasi_utama"  >
                  <div class="col-sm-6" id="div_bhn_so_grafik_1">
                    <div class="card-set">
                      <div class="headset"><i class="fa fa-chart-pie"></i> Grafik Jumlah Per Materi</div>
                      <div class="img-chart">
                        <div id="bhn_so_grafik_1" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6" id="div_bhn_so_grafik_2">
                    <div class="card-set">
                      <div class="headset"><i class="fa fa-chart-pie"></i> Grafik Jumlah Bahan Sosialisasi Per Provinsi</div>
                      <div class="img-chart">
                        <div id="bhn_so_grafik_2" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                      </div>
                    </div>
                  </div> <br/>
              </div>-->
              <div class="row">
                  <div class="col-sm-12" id="bahan_sosialisasi_table_data_utama"></div>
              </div>
              <input type="hidden" name="BhnSosialisasiPencarian_all" id="BhnSosialisasiPencarian_all" >
              <script type="text/javascript">
                function print_laporan_bhn_so_all(tipe){
                    var param = $("#BhnSosialisasiPencarian_all").val();

                      var form = document.createElement("form");
                      form.setAttribute("method", "POST");
                      if(tipe=="print"){
                       form.setAttribute("target", "_blank");
                      }
                      form.setAttribute("action", "<?php echo $basepath ?>sosialisasi_bimtek_bahan_sosialisasi/cetak_all");
                             
                          var hiddenField = document.createElement("input");
                          hiddenField.setAttribute("type", "hidden");
                          hiddenField.setAttribute("name", "param");
                          hiddenField.setAttribute("value", param);

                          var hiddenField2 = document.createElement("input");
                          hiddenField2.setAttribute("type", "hidden");
                          hiddenField2.setAttribute("name", "tipe");
                          hiddenField2.setAttribute("value", tipe);

                          form.appendChild(hiddenField);
                          form.appendChild(hiddenField2);

                      document.body.appendChild(form);
                      form.submit();
                  }
              </script>
              <!-- Tabel -->
              <div class="cnt-heaeder bhn_sosialisasi_utama" style="margin-top: 30px; margin-bottom: 20px;">
                <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-portrait"></i> Bahan Sosialisasi</h6>
                
                <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">
                  
                  <?php  if($akses_add_tab_bhn_so="1"){  ?>
                    <a href="javascript:void(0)"  data-tooltip="tooltip" data-placement="bottom" title="Tambah" onclick="call_modal_default('Tambah Bahan Sosialisasi','Form_Bahan_Sosialisasi_ALL','do_add','sosialisasi_bimtek/form_all')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                  <?php }  ?>

                  <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Cetak" onclick="print_laporan_bhn_so_all('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

                  <?php  if($akses_add_tab_bhn_so="1"){  ?>
                     <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Import" data-target="#Import_BhnSosialisasiAll"><i class="fa fa-upload"></i></a>
                  <?php }  ?>
                  <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Filterisasi" data-target="#Filter_BhnSosialisasiAll"  ><i class="fa fa-filter"></i></a>

                  <div class="btn-group" role="group" data-tooltip="tooltip" data-placement="bottom" title="Export">
                    <button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-file-pdf"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="red">
                      <a class="dropdown-item"  onclick="print_laporan_bhn_so_all('pdf')" href="javascript:void(0)">Export to PDF</a>
                      <a class="dropdown-item"   onclick="print_laporan_bhn_so_all('xls')" href="javascript:void(0)">Export to XLS</a>
                      <a class="dropdown-item" onclick="print_laporan_bhn_so_all('doc')"   href="javascript:void(0)">Export to DOCS</a>
                    </div>
                  </div>
                </div>
                <hr style="width: 100%; float: left; display: block;">
              </div>

              <div id="filter_bhn_so_all" class="table table-responsive pelayanan-top bhn_sosialisasi_utama">
                <table id="tb_bahan_sosialisasi" class="display table table-striped pelayanan-table datatable">
                  <thead>
                    <tr>
                      <th class="text-center">Tanggal</th>
                      <th class="text-center">Provinsi</th>
                      <th class="text-center">UPT</th>
                      <th class="text-center">Materi</th>
                      <th class="text-center">Judul</th>
                      <th class="text-center">Author</th>
                      <th class="text-center">Lampiran</th>
                      <th class="text-center">Deskripsi</th>
                      <th class="text-center">Keterangan</th>
                      <th class="text-center">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
             

              <script type="text/javascript">
                var dTable1;
                $(document).ready(function() {
                  dTable1 = $('#tb_bahan_sosialisasi').DataTable( {
                    "bProcessing": true,
                    "searching": false,
                    "bServerSide": true,
                    "bJQueryUI": false,
                    "retrieve": true,
                    "responsive": false,
                    "autoWidth": false,
                    "sAjaxSource": "<?php echo $basepath ?>sosialisasi_bimtek_bahan_sosialisasi/rest", 
                    "sServerMethod": "POST",
                    "scrollX": true,
                    "scrollY": "350px",
                      "scrollCollapse": true,
                      "order": [[ 0, "desc" ]],
                    "columnDefs": [
                    { "orderable": true, "targets": 0, "searchable": true},
                    { "orderable": true, "targets": 1, "searchable": true},
                    { "orderable": true, "targets": 2, "searchable": true },
                    { "orderable": true, "targets": 3, "searchable": true },
                    { "orderable": true, "targets": 4, "searchable": true },
                    { "orderable": true, "targets": 5, "searchable": true },
                    { "orderable": true, "targets": 6, "searchable": true },
                    { "orderable": true, "targets": 7, "searchable": true },
                    { "orderable": true, "targets": 8, "searchable": true },
                    { "orderable": false, "targets": 9, "searchable": false, "width":200}
                    ]
                  } );
                });
              </script>
              </div>    
            </div>

            <!-- Tab Rencana Sosialisasi -->
            <div class="tab-pane fade <?php echo active_tab("rencana_sosialisasi") ?>" id="rencana_sosialisasi" role="tabpanel" aria-labelledby="rencana_sosialisasi-tab">
             
              <!-- Peta -->
              <label class="judul-peta if_upt rencana_sosialisasi_utama"><i class="fa fa-map-marked-alt"></i> Peta Lokasi UPT</label>
              <hr class="if_upt rencana_sosialisasi_utama">
              <div class="peta-cnt" id="peta_rencana_sosialisasi"></div>

              <!-- Grafik Chart -->
              <div class="row if_upt rencana_sosialisasi_utama" >
                  <div class="col-sm-6" id="div_rcn_so_grafik_1">
                    <div class="card-set">
                      <div class="headset"><i class="fa fa-chart-pie"></i> Rekapitulasi Kegiatan Sosialisasi & Bimtek</div>
                      <div class="img-chart">
                        <!-- <img src="images/chart.jpg" alt=""> -->
                        <div id="rcn_so_grafik_1" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6" id="div_rcn_so_grafik_2">
                    <div class="card-set">
                      <div class="headset"><i class="fa fa-chart-pie"></i> Rekapitulasi Kegiatan Sosialisasi & Bimtek Per UPT</div>
                      <div class="img-chart">
                        <!-- <img src="images/chart.jpg" alt=""> -->
                        <div id="rcn_so_grafik_2" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                      </div>
                    </div>
                  </div> <br/>
              </div>
              <div class="row">
                  <div class="col-sm-12" id="rencana_sosialisasi_table_data_utama"></div>
              </div>
              <input type="hidden" name="RencanaSosialisasiPencarian_all" id="RencanaSosialisasiPencarian_all" >
              <script type="text/javascript">
                function print_laporan_rencana_so_all(tipe){
                    var param = $("#RencanaSosialisasiPencarian_all").val();

                      var form = document.createElement("form");
                      form.setAttribute("method", "POST");
                      if(tipe=="print"){
                       form.setAttribute("target", "_blank");
                      }
                      form.setAttribute("action", "<?php echo $basepath ?>sosialisasi_bimtek_rencana_sosialisasi/cetak_all");
                             
                          var hiddenField = document.createElement("input");
                          hiddenField.setAttribute("type", "hidden");
                          hiddenField.setAttribute("name", "param");
                          hiddenField.setAttribute("value", param);

                          var hiddenField2 = document.createElement("input");
                          hiddenField2.setAttribute("type", "hidden");
                          hiddenField2.setAttribute("name", "tipe");
                          hiddenField2.setAttribute("value", tipe);

                          form.appendChild(hiddenField);
                          form.appendChild(hiddenField2);

                      document.body.appendChild(form);
                      form.submit();
                  }
              </script>
              <!-- Tabel -->
              <div class="cnt-heaeder rencana_sosialisasi_utama" style="margin-top: 30px; margin-bottom: 20px;">
                <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-portrait"></i> Rencana Sosialisasi</h6>
                
                <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">
                  
                  <?php  if($akses_add_tab_rcn_so="1"){  ?>
                      <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Tambah" onclick="call_modal_default('Tambah Rencana Sosialisasi','Form_Rencana_Sosialisasi_ALL','do_add','sosialisasi_bimtek/form_all')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                  <?php } ?>

                  <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Cetak" onclick="print_laporan_rencana_so_all('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

                  <?php  if($akses_add_tab_rcn_so="1"){  ?>
                     <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Import" data-target="#Import_RencanaSosialisasiAll"><i class="fa fa-upload"></i></a>
                  <?php } ?>
                  
                  <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Filterisasi" data-target="#Filter_RencanaSosialisasiAll"  ><i class="fa fa-filter"></i></a>

                  <div class="btn-group" role="group" data-tooltip="tooltip" data-placement="bottom" title="Export">
                    <button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-file-pdf"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="red">
                      <a class="dropdown-item"  onclick="print_laporan_rencana_so_all('pdf')" href="javascript:void(0)">Export to PDF</a>
                      <a class="dropdown-item"   onclick="print_laporan_rencana_so_all('xls')" href="javascript:void(0)">Export to XLS</a>
                      <a class="dropdown-item" onclick="print_laporan_rencana_so_all('doc')"   href="javascript:void(0)">Export to DOCS</a>
                    </div>
                  </div>
                </div>
                <hr style="width: 100%; float: left; display: block;">
              </div>

              <div id="filter_rencana_so_all" class="table table-responsive pelayanan-top rencana_sosialisasi_utama">
                <table id="tb_rencana_sosialisasi" class="display table table-striped pelayanan-table datatable">
                  <thead>
                    <tr>
                      <th class="text-center">Tanggal Buat</th>
                      <th class="text-center">Provinsi</th>
                      <th class="text-center">UPT</th>
                      <th class="text-center">Jenis Kegiatan</th>
                      <th class="text-center">Tanggal Pelaksanaan</th>
                      <th class="text-center">Tempat</th>
                      <th class="text-center">Tema</th>
                      <th class="text-center">Target Peserta</th>
                      <th class="text-center">Jumlah Peserta</th>
                      <th class="text-center">Anggaran</th>
                      <th class="text-center">Narasumber</th>
                      <th class="text-center">Lampiran</th>
                      <th class="text-center">Keterangan</th>
                      <th class="text-center">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
             

              <script type="text/javascript">
                var dTable1;
                $(document).ready(function() {
                  dTable1 = $('#tb_rencana_sosialisasi').DataTable( {
                    "bProcessing": true,
                    "searching": false,
                    "bServerSide": true,
                    "bJQueryUI": false,
                    "retrieve": true,
                    "responsive": false,
                    "autoWidth": false,
                    "sAjaxSource": "<?php echo $basepath ?>sosialisasi_bimtek_rencana_sosialisasi/rest", 
                    "sServerMethod": "POST",
                    "scrollX": true,
                    "scrollY": "350px",
                      "scrollCollapse": true,
                      "order": [[ 0, "desc" ]],
                    "columnDefs": [
                    { "orderable": true, "targets": 0, "searchable": true},
                    { "orderable": true, "targets": 1, "searchable": true},
                    { "orderable": true, "targets": 2, "searchable": true},
                    { "orderable": true, "targets": 3, "searchable": true},
                    { "orderable": true, "targets": 4, "searchable": true },
                    { "orderable": true, "targets": 5, "searchable": true },
                    { "orderable": true, "targets": 6, "searchable": true },
                    { "orderable": true, "targets": 7, "searchable": true },
                    { "orderable": true, "targets": 8, "searchable": true },
                    { "orderable": true, "targets": 9, "searchable": true },
                    { "orderable": true, "targets": 10, "searchable": true },
                    { "orderable": true, "targets": 11, "searchable": true },
                    { "orderable": false, "targets": 13, "searchable": false, "width":200}
                    ]
                  } );
                });
              </script>
              </div>    
            </div>

            <!-- Tab Monev Sosialisasi -->
            <div class="tab-pane fade <?php echo active_tab("monev_sosialisasi") ?>" id="monev_sosialisasi" role="tabpanel" aria-labelledby="monev_sosialisasi-tab">
             
              <!-- Peta -->
              <label class="judul-peta if_upt monev_sosialisasi_utama"><i class="fa fa-map-marked-alt"></i> Peta Lokasi UPT</label>
              <hr class="if_upt monev_sosialisasi_utama">
              <div class="peta-cnt" id="peta_monev_sosialisasi"></div>

              <!-- Grafik Chart -->
              <div class="row if_upt monev_sosialisasi_utama"  >
                  <div class="col-sm-6" id="div_mnv_so_grafik_1">
                    <div class="card-set">
                      <div class="headset"><i class="fa fa-chart-pie"></i> Rekapitulasi Kegiatan Sosialisasi & Bimtek</div>
                      <div class="img-chart">
                        <!-- <img src="images/chart.jpg" alt=""> -->
                        <div id="mnv_so_grafik_1" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6" id="div_mnv_so_grafik_2">
                    <div class="card-set">
                      <div class="headset"><i class="fa fa-chart-pie"></i> Rekapitulasi Kegiatan Sosialisasi & Bimtek Per UPT</div>
                      <div class="img-chart">
                        <!-- <img src="images/chart.jpg" alt=""> -->
                        <div id="mnv_so_grafik_2" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                      </div>
                    </div>
                  </div> <br/>
              </div>
              <div class="row">
                  <div class="col-sm-12" id="monev_sosialisasi_table_data_utama"></div>
              </div>
              <input type="hidden" name="MonevSosialisasiPencarian_all" id="MonevSosialisasiPencarian_all" >
              <script type="text/javascript">
                function print_laporan_monev_so_all(tipe){
                    var param = $("#MonevSosialisasiPencarian_all").val();

                      var form = document.createElement("form");
                      form.setAttribute("method", "POST");
                      if(tipe=="print"){
                       form.setAttribute("target", "_blank");
                      }
                      form.setAttribute("action", "<?php echo $basepath ?>sosialisasi_bimtek_monev_sosialisasi/cetak_all");
                             
                          var hiddenField = document.createElement("input");
                          hiddenField.setAttribute("type", "hidden");
                          hiddenField.setAttribute("name", "param");
                          hiddenField.setAttribute("value", param);

                          var hiddenField2 = document.createElement("input");
                          hiddenField2.setAttribute("type", "hidden");
                          hiddenField2.setAttribute("name", "tipe");
                          hiddenField2.setAttribute("value", tipe);

                          form.appendChild(hiddenField);
                          form.appendChild(hiddenField2);

                      document.body.appendChild(form);
                      form.submit();
                  }
              </script>
              <!-- Tabel -->
              <div class="cnt-heaeder monev_sosialisasi_utama" style="margin-top: 30px; margin-bottom: 20px;">
                <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-portrait"></i> Monev Sosialisasi</h6>
                
                <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">
                 
                  <?php  if($akses_add_tab_monev_so="1"){  ?>
                   <a href="javascript:void(0)"  data-tooltip="tooltip" data-placement="bottom" title="Tambah" onclick="call_modal_default('Tambah Monev Sosialisasi','Form_Monev_Sosialisasi_ALL','do_add','sosialisasi_bimtek/form_all')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                  <?php } ?>

                  <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Cetak" onclick="print_laporan_monev_so_all('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

                  <?php  if($akses_add_tab_monev_so="1"){  ?>
                    <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom"  title="Import" data-target="#Import_MonevSosialisasiAll"><i class="fa fa-upload"></i></a>
                  <?php } ?>

                  <a href="" data-tooltip="tooltip" data-placement="bottom" title="Filterisasi" class="btn btn-secondary btn-sm btn-table" data-toggle="modal"  data-target="#Filter_MonevSosialisasiAll"  ><i class="fa fa-filter"></i></a>

                  <div class="btn-group" role="group" data-tooltip="tooltip" data-placement="bottom" title="Export">
                    <button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-file-pdf"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="red">
                      <a class="dropdown-item"  onclick="print_laporan_monev_so_all('pdf')" href="javascript:void(0)">Export to PDF</a>
                      <a class="dropdown-item"   onclick="print_laporan_monev_so_all('xls')" href="javascript:void(0)">Export to XLS</a>
                      <a class="dropdown-item" onclick="print_laporan_monev_so_all('doc')"   href="javascript:void(0)">Export to DOCS</a>
                    </div>
                  </div>
                </div>
                <hr style="width: 100%; float: left; display: block;">
              </div>

              <div id="filter_monev_so_all" class="table table-responsive pelayanan-top monev_sosialisasi_utama">
                <table id="tb_monev_sosialisasi" class="display table table-striped pelayanan-table datatable">
                  <thead>
                    <tr>
                      <th class="text-center">Tanggal Buat</th>
                      <th class="text-center">Provinsi</th>
                      <th class="text-center">UPT</th>
                      <th class="text-center">Jenis Kegiatan</th>
                      <th class="text-center">Tanggal Pelaksanaan</th>
                      <th class="text-center">Tempat</th>
                      <th class="text-center">Tema</th>
                      <th class="text-center">Target Peserta</th>
                      <th class="text-center">Jumlah Peserta</th>
                      <th class="text-center">Anggaran</th>
                      <th class="text-center">Narasumber</th>
                      <th class="text-center">Lampiran</th>
                      <th class="text-center">Keterangan</th>
                      <th class="text-center">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
             

              <script type="text/javascript">
                var dTable1;
                $(document).ready(function() {
                  dTable1 = $('#tb_monev_sosialisasi').DataTable( {
                    "bProcessing": true,
                    "searching": false,
                    "bServerSide": true,
                    "bJQueryUI": false,
                    "retrieve": true,
                    "responsive": false,
                    "autoWidth": false,
                    "sAjaxSource": "<?php echo $basepath ?>sosialisasi_bimtek_monev_sosialisasi/rest", 
                    "sServerMethod": "POST",
                    "scrollX": true,
                    "scrollY": "350px",
                      "scrollCollapse": true,
                      "order": [[ 0, "desc" ]],
                    "columnDefs": [
                    { "orderable": true, "targets": 0, "searchable": true},
                    { "orderable": true, "targets": 1, "searchable": true},
                    { "orderable": true, "targets": 2, "searchable": true},
                    { "orderable": true, "targets": 3, "searchable": true},
                    { "orderable": true, "targets": 4, "searchable": true },
                    { "orderable": true, "targets": 5, "searchable": true },
                    { "orderable": true, "targets": 6, "searchable": true },
                    { "orderable": true, "targets": 7, "searchable": true },
                    { "orderable": true, "targets": 8, "searchable": true },
                    { "orderable": true, "targets": 9, "searchable": true },
                    { "orderable": true, "targets": 10, "searchable": true },
                    { "orderable": true, "targets": 11, "searchable": true },
                    { "orderable": true, "targets": 12, "searchable": true },
                    { "orderable": false, "targets": 13, "searchable": false, "width":200}
                    ]
                  } );
                });
              </script>
              </div>    
            </div>

            <!-- Tab Galeri -->
            <div class="tab-pane fade <?php echo active_tab("monev_sosialisasi") ?>" id="galeri" role="tabpanel" aria-labelledby="galeri-tab">
             
              <!-- Peta -->
              <label class="judul-peta if_upt galeri_utama"><i class="fa fa-map-marked-alt"></i> Peta Lokasi UPT</label>
              <hr class="if_upt galeri_utama">
              <div class="peta-cnt" id="peta_galeri"></div>

              <!-- Grafik Chart -->
              <!--<div class="row if_upt galeri_utama" >
                  <div class="col-sm-12" id="div_galeri_grafik_1">
                    <div class="card-set">
                      <div class="headset"><i class="fa fa-chart-pie"></i> Grafik Jumlah Galeri Per Provinsi</div>
                      <div class="img-chart">
                        <div id="galeri_grafik" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                      </div>
                    </div>
                  </div> <br/>
              </div>-->

              <div class="row">
                  <div class="col-sm-12" id="galeri_table_data_utama"></div>
              </div>
              <input type="hidden" name="GaleriPencarian_all" id="GaleriPencarian_all" >
              <script type="text/javascript">
                function print_laporan_galeri_all(tipe){
                    var param = $("#GaleriPencarian_all").val();

                      var form = document.createElement("form");
                      form.setAttribute("method", "POST");
                      if(tipe=="print"){
                       form.setAttribute("target", "_blank");
                      }
                      form.setAttribute("action", "<?php echo $basepath ?>sosialisasi_bimtek_galeri/cetak_all");
                             
                          var hiddenField = document.createElement("input");
                          hiddenField.setAttribute("type", "hidden");
                          hiddenField.setAttribute("name", "param");
                          hiddenField.setAttribute("value", param);

                          var hiddenField2 = document.createElement("input");
                          hiddenField2.setAttribute("type", "hidden");
                          hiddenField2.setAttribute("name", "tipe");
                          hiddenField2.setAttribute("value", tipe);

                          form.appendChild(hiddenField);
                          form.appendChild(hiddenField2);

                      document.body.appendChild(form);
                      form.submit();
                  }
              </script>
              <!-- Tabel -->
              <div class="cnt-heaeder galeri_utama" style="margin-top: 30px; margin-bottom: 20px;">
                <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-portrait"></i> Galeri</h6>
                
                <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">
                  
                  <?php  if($akses_add_tab_galeri="1"){  ?>
                    <a href="javascript:void(0)"  data-tooltip="tooltip" data-placement="bottom" title="Tambah" onclick="call_modal_default('Tambah Galeri','Form_Galeri_ALL','do_add','sosialisasi_bimtek/form_all')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                  <?php } ?>

                  <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Cetak" onclick="print_laporan_galeri_all('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

                  <?php  if($akses_add_tab_galeri="1"){  ?>
                    <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Import" data-target="#Import_GaleriAll"><i class="fa fa-upload"></i></a>
                  <?php } ?>

                  <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Filterisasi" data-target="#Filter_GaleriAll"  ><i class="fa fa-filter"></i></a>

                  <div class="btn-group" role="group" data-tooltip="tooltip" data-placement="bottom" title="Export">
                    <button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-file-pdf"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="red">
                      <a class="dropdown-item"  onclick="print_laporan_galeri_all('pdf')" href="javascript:void(0)">Export to PDF</a>
                      <a class="dropdown-item"   onclick="print_laporan_galeri_all('xls')" href="javascript:void(0)">Export to XLS</a>
                      <a class="dropdown-item" onclick="print_laporan_galeri_all('doc')"   href="javascript:void(0)">Export to DOCS</a>
                    </div>
                  </div>
                </div>
                <hr style="width: 100%; float: left; display: block;">
              </div>

              <div id="filter_galeri_all" class="table table-responsive pelayanan-top galeri_utama">
                <table id="tb_galeri" class="display table table-striped pelayanan-table datatable">
                  <thead>
                    <tr>
                      <th class="text-center">Tanggal</th>
                      <th class="text-center">Provinsi</th>
                      <th class="text-center">UPT</th>
                      <th class="text-center">Kegiatan</th>
                      <th class="text-center">Lampiran</th>
                      <th class="text-center">Deskripsi</th>
                      <th class="text-center">Keterangan</th>
                      <th class="text-center">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              <script type="text/javascript">
                var dTable1;
                $(document).ready(function() {
                  dTable1 = $('#tb_galeri').DataTable( {
                    "bProcessing": true,
                    "searching": false,
                    "bServerSide": true,
                    "bJQueryUI": false,
                    "retrieve": true,
                    "responsive": false,
                    "autoWidth": false,
                    "sAjaxSource": "<?php echo $basepath ?>sosialisasi_bimtek_galeri/rest", 
                    "sServerMethod": "POST",
                    "scrollX": true,
                    "scrollY": "350px",
                      "scrollCollapse": true,
                      "order": [[ 0, "desc" ]],
                    "columnDefs": [
                    { "orderable": true, "targets": 0, "searchable": true},
                    { "orderable": true, "targets": 1, "searchable": true},
                    { "orderable": true, "targets": 2, "searchable": true},
                    { "orderable": true, "targets": 3, "searchable": true},
                    { "orderable": true, "targets": 4, "searchable": true },
                    { "orderable": true, "targets": 5, "searchable": true },
                    { "orderable": true, "targets": 6, "searchable": true },
                    { "orderable": false, "targets": 7, "searchable": false, "width":200}
                    ]
                  } );
                });
              </script>
              </div>
            </div>
          </div>
         <?php } else { 
        echo "<div class='row'>
                <div class='col-md-12'>
                    <h3>Anda tidak mempunyai akses apapun pada halaman ini</h3>
                </div>
              </div>"; 
        echo "<script>
                var  height_main =  $('.pelayanan-cnt').height();
                     $('#div_content').height(height_main);
                     $('#myTab').remove();
              </script>";
       }?>
      </div>
    </div>
    </div>
  </div>

  

<script type="text/javascript">

function pilih_upt_bhn_sosialisasi(id_map) {
  call_lokasi_upt(id_map,'bahan_sosialisasi_table_data_utama');
  $('.bhn_sosialisasi_utama').hide();
}
function pilih_upt_rencana_sosialisasi(id_map) {
  call_lokasi_upt(id_map,'rencana_sosialisasi_table_data_utama');
  $('.rencana_sosialisasi_utama').hide();
}
function pilih_upt_monev_sosialisasi(id_map) {
  call_lokasi_upt(id_map,'monev_sosialisasi_table_data_utama');
  $('.monev_sosialisasi_utama').hide();
}
function pilih_upt_galeri(id_map) {
  call_lokasi_upt(id_map,'galeri_table_data_utama');
  $('.galeri_utama').hide();
}
function  call_lokasi_upt(id_map,div_id){
   $.ajax({
         type: "POST",
         dataType: "html",
         url: "<?php echo $basepath ?>sosialisasi_bimtek/pilih_upt",
         data: "map_id="+id_map+"&div_id="+div_id,
         success: function(msg){
            if(msg){
              $("#"+div_id).html(msg);
            }
         }
      }); 
}

  //Start Chart Bahan Sosialisasi
    var chart_bhn_so_materi = [
      <?php 
            $bhn_so_no_1 = 0;
            //Chart by materi
            $chart_materi="";
            $sql = "select count(*) as total,mtr.jenis_materi
                   from tr_bahan_sosialisasi as bhn 
                   inner join ms_materi as mtr 
                      on mtr.kode_materi=bhn.materi
                   where bhn.status='3' GROUP BY mtr.jenis_materi";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_materi .='{"name":"'.$jenis_materi.'","y":'.$total.'},';
              $bhn_so_no_1++;
            } 
            echo trim($chart_materi,',');
      ?>
    ];

    var chart_bhn_so_kunjungan = [
      <?php 
            $bhn_so_no_2 = 0;
            //Chart by kunjungan
            $chart_kunjungan="";
            $sql = "SELECT  pv.nama_prov,COUNT(kode_bhn_sosialisasi) as total
                from tr_bahan_sosialisasi as bhn
                  inner join ms_provinsi as pv 
                      on pv.id_prov=bhn.id_prov  where  bhn.status='3' GROUP BY pv.nama_prov";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_kunjungan .='{"name":"'.$nama_prov.'","y":'.$total.'},';
              $bhn_so_no_2++;
            } 
            echo trim($chart_kunjungan,',');
      ?>
    ];
      <?php 
        if($bhn_so_no_1==0){
          echo " $('#div_bhn_so_grafik_1').hide(); ";
        }
        if($bhn_so_no_2==0){
          echo " $('#div_bhn_so_grafik_2').hide(); ";   
        }
     ?>
     //End Chart Bahan Sosialisasi

     //Start Chart Rencana Sosialisasi
    var chart_rcn_so_kegiatan = [
      <?php 
            $rcn_so_no_1 = 0;
            //Chart by kegiatan
            $chart_kegiatan="";
            $sql = "select count(*) as total,kgt.kegiatan
                   from tr_rencana_sosialisasi as rcn 
                   inner join ms_kegiatan as kgt 
                      on kgt.kode_kegiatan=rcn.jenis_kegiatan
                   where rcn.status='3' GROUP BY kgt.kegiatan";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_kegiatan .='{"name":"'.$kegiatan.'","y":'.$total.'},';
              $rcn_so_no_1++;
            } 
            echo trim($chart_kegiatan,',');
      ?>
    ];

    var chart_rcn_so_kunjungan = [
      <?php 
            $rcn_so_no_2 = 0;
            //Chart by kunjungan
            $chart_kunjungan="";
            $sql = "SELECT  pv.nama_prov,COUNT(kode_rencana_sosialisasi) as total
                from tr_rencana_sosialisasi as rcn
                  inner join ms_provinsi as pv 
                      on pv.id_prov=rcn.id_prov  where  rcn.status='3' GROUP BY pv.nama_prov";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_kunjungan .='{"name":"'.$nama_prov.'","y":'.$total.'},';
              $rcn_so_no_2++;
            } 
            echo trim($chart_kunjungan,',');
      ?>
    ];
    <?php 
        if($rcn_so_no_1==0){
          echo " $('#div_rcn_so_grafik_1').hide(); ";
        }
        if($rcn_so_no_2==0){
          echo " $('#div_rcn_so_grafik_2').hide(); ";   
        }
     ?>
     //End Chart Rencana Sosialisasi 

     //Start Chart Monev Sosialisasi
    var chart_monev_so_kegiatan = [
      <?php 
            $mnv_no_1 = 0;
            //Chart by kegiatan
            $chart_kegiatan="";
            $sql = "select count(*) as total,kgt.kegiatan
                   from tr_monev_sosialisasi as mnv 
                   inner join ms_kegiatan as kgt 
                      on kgt.kode_kegiatan=mnv.jenis_kegiatan
                   where mnv.status='3' GROUP BY kgt.kegiatan";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_kegiatan .='{"name":"'.$kegiatan.'","y":'.$total.'},';
              $mnv_no_1++;
            } 
            echo trim($chart_kegiatan,',');
      ?>
    ];

    var chart_monev_so_kunjungan = [
      <?php 
            $mnv_no_2 = 0;
            //Chart by kunjungan
            $chart_kunjungan="";
            $sql = "SELECT  pv.nama_prov,COUNT(kode_monev_sosialisasi) as total
                from tr_monev_sosialisasi as mnv
                  inner join ms_provinsi as pv 
                      on pv.id_prov=mnv.id_prov  where  mnv.status='3' GROUP BY pv.nama_prov";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_kunjungan .='{"name":"'.$nama_prov.'","y":'.$total.'},';
              $mnv_no_2++;
            } 
            echo trim($chart_kunjungan,',');
      ?>
    ];
    <?php 
        if($mnv_no_1==0){
          echo " $('#div_mnv_so_grafik_1').hide(); ";
        }
        if($mnv_no_2==0){
          echo " $('#div_mnv_so_grafik_2').hide(); ";   
        }
     ?>
     //End Chart Monev Sosialisasi


     //Start Galeri
    var chart_galeri_kunjungan = [
      <?php 
            $galeri_no_1 = 0;
            //Chart by kunjungan
            $chart_kunjungan="";
            $sql = "SELECT  pv.nama_prov,COUNT(kode_galeri) as total
                from tr_galeri as gl
                  inner join ms_provinsi as pv 
                      on pv.id_prov=gl.id_prov  where  gl.status='3' GROUP BY pv.nama_prov";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_kunjungan .='{"name":"'.$nama_prov.'","y":'.$total.'},';
              $galeri_no_1++;
            } 
            echo trim($chart_kunjungan,',');
      ?>
    ];

    <?php 
        if($galeri_no_1==0){
          echo " $('#div_galeri_grafik_1').hide(); ";
        }
     ?>
     //End Chart Galeri

<?php  
//Kepala UPT atau Operator UPT
if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
    $wilayah   = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$_SESSION['upt_provinsi'])); ?>
     window.onload =  function(){
        $(".if_upt").hide();
        detail_bhn_sosialisasi('<?php echo $wilayah['id_map'] ?>','<?php echo $_SESSION['kode_upt'] ?>');
        detail_monev_sosialisasi('<?php echo $wilayah['id_map'] ?>','<?php echo $_SESSION['kode_upt'] ?>');
        detail_rencana_sosialisasi('<?php echo $wilayah['id_map'] ?>','<?php echo $_SESSION['kode_upt'] ?>');
        detail_galeri('<?php echo $wilayah['id_map'] ?>','<?php echo $_SESSION['kode_upt'] ?>');
     };

<?php } else { ?>
  
  //Map and chart
    window.onload =  function(){
      show_map('peta_bhn_sosialisasi','pilih_upt_bhn_sosialisasi');    
      show_map('peta_rencana_sosialisasi','pilih_upt_rencana_sosialisasi');    
      show_map('peta_monev_sosialisasi','pilih_upt_monev_sosialisasi');    
      show_map('peta_galeri','pilih_upt_galeri');  

      pie_chart('bhn_so_grafik_1',chart_bhn_so_materi);
      pie_chart('bhn_so_grafik_2',chart_bhn_so_kunjungan);

      pie_chart('rcn_so_grafik_1',chart_rcn_so_kegiatan);
      pie_chart('rcn_so_grafik_2',chart_rcn_so_kunjungan);

      pie_chart('mnv_so_grafik_1',chart_monev_so_kegiatan);
      pie_chart('mnv_so_grafik_2',chart_monev_so_kunjungan);

      //pie_chart('galeri_grafik',chart_galeri_kunjungan);
    };

<?php }  ?>

//Start Bahan Sosialisasi
function after_crud_bhn_sosialisasi(){
    var map_id = $(".data_map_id_bhn_so").val();
    var upt = $(".data_upt_bhn_so").val();
    detail_bhn_sosialisasi(map_id,upt);
    $('#call_modal').modal('hide');
}
function detail_bhn_sosialisasi(id_map,upt){
  $(".data_map_id_bhn_so").val(id_map);
  $(".data_upt_bhn_so").val(upt);
  $('.bhn_sosialisasi_utama').hide();
  ajax_table_map('bahan_sosialisasi_table_data_utama',id_map,upt,'sosialisasi_bimtek/table');
  //$('html, body').animate({
       // scrollTop: $("#pelayanan_table_data_utama").offset().top;
  //}, 1000);
  refresh_table();
}     
function do_edit_bhn_so_all(param_id){
  call_modal_default('Ubah Bahan Sosialisasi','Form_Bahan_Sosialisasi_ALL','do_edit','sosialisasi_bimtek/form_all');
   setTimeout(function(){ 
       $.ajax({
            url: '<?php echo $basepath ?>sosialisasi_bimtek_bahan_sosialisasi/edit/'+param_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $("#kode_bhn_sosialisasi").val(data.kode_bhn_sosialisasi);
                $("#materi_bhn_so").val(data.materi);
                $("#judul_bhn_so").val(data.judul);
                $("#author_bhn_so").val(data.author);
                $("#deskripsi_bhn_so").val(data.deskripsi);
                $("#keterangan_bhn_so").val(data.keterangan);
                $("#upt_provinsi").val(data.id_prov);
                $('#lampiran_bhn_so').attr('href',data.lampiran);
                     $.ajax({
                         type: "POST",
                         dataType: "html",
                         url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
                         data: "provinsi="+data.id_prov,
                         success: function(msg){
                            if(msg){
                              $("#kode_upt").html(msg);

                              setTimeout(function(){ 
                                   $("#kode_upt").val(data.kode_upt);        
                              }, 500);
                             
                            }
                         }
                      }); 

            }
        });
   }, 1000);
}
function do_edit_bhn_so(param_id,map_id,upt){
  call_modal_from_map('Ubah Bahan Sosialisasi','Form_Bahan_Sosialisasi',map_id,upt,'do_edit','sosialisasi_bimtek/form');
   setTimeout(function(){ 
       $.ajax({
            url: '<?php echo $basepath ?>sosialisasi_bimtek_bahan_sosialisasi/edit/'+param_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $("#kode_bhn_sosialisasi").val(data.kode_bhn_sosialisasi);
                $("#materi_bhn_so").val(data.materi);
                $("#judul_bhn_so").val(data.judul);
                $("#author_bhn_so").val(data.author);
                $("#deskripsi_bhn_so").val(data.deskripsi);
                $("#keterangan_bhn_so").val(data.keterangan);
                $('#lampiran_bhn_so').attr('href',data.lampiran);
            }
        });
   }, 1000);
}
function do_detail_bhn_so(param_id,map_id,upt){
   call_modal_from_map('Detail Bahan Sosialisasi','Form_Bahan_Sosialisasi',map_id,upt,'do_detail','sosialisasi_bimtek/form');
   setTimeout(function(){ 
       $.ajax({
            url: '<?php echo $basepath ?>sosialisasi_bimtek_bahan_sosialisasi/edit/'+param_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $("#kode_bhn_sosialisasi").val(data.kode_bhn_sosialisasi);
                $("#materi_bhn_so").val(data.materi);
                $("#judul_bhn_so").val(data.judul);
                $("#author_bhn_so").val(data.author);
                $("#deskripsi_bhn_so").val(data.deskripsi);
                $("#keterangan_bhn_so").val(data.keterangan);
                $("#id_prov_bhn_sosialisasi").val(data.id_prov);
                $("#kode_upt_bhn_sosialisasi").val(data.kode_upt); 
                $('#lampiran_bhn_so').attr('href',data.lampiran);   
            }
        });
   }, 1000);
}
function do_delete_bhn_so(parameter_data,param_id){
    act_delete('Hapus Bahan Sosialisasi','Anda ingin menghapus Bahan Sosialisasi ? ','warning','sosialisasi_bimtek_bahan_sosialisasi/delete','no_refresh','after_crud_bhn_sosialisasi',window.atob(parameter_data),window.atob(param_id));
}
function do_delete_bhn_so_all(parameter_data,param_id){
    act_delete('Hapus Bahan Sosialisasi','Anda ingin menghapus Bahan Sosialisasi ? ','warning','sosialisasi_bimtek_bahan_sosialisasi/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
}
//End Bahan Sosialisasi

//Start Rencana Sosialisasi
function after_crud_rencana_sosialisasi(){
    var map_id = $(".data_map_id_rencana_so").val();
    var upt = $(".data_upt_rencana_so").val();
    detail_rencana_sosialisasi(map_id,upt);
    $('#call_modal').modal('hide');
}
function detail_rencana_sosialisasi(id_map,upt){
  $(".data_map_id_rencana_so").val(id_map);
  $(".data_upt_rencana_so").val(upt);
  $('.rencana_sosialisasi_utama').hide();
  ajax_table_map('rencana_sosialisasi_table_data_utama',id_map,upt,'sosialisasi_bimtek/table');
  //$('html, body').animate({
       // scrollTop: $("#pelayanan_table_data_utama").offset().top;
  //}, 1000);
  refresh_table();
}     
function do_edit_rencana_so_all(param_id){
   call_modal_default('Ubah Rencana Sosialisasi','Form_Rencana_Sosialisasi_ALL','do_edit','sosialisasi_bimtek/form_all');
   setTimeout(function(){ 
       $.ajax({
            url: '<?php echo $basepath ?>sosialisasi_bimtek_rencana_sosialisasi/edit/'+param_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $("#kode_rencana_sosialisasi").val(data.kode_rencana_sosialisasi);
                $("#kegiatan_rcn_so").val(data.kegiatan);
                $("#tgl_pelaksanaan_rcn_so").val(data.tgl_pelaksanaan);
                $("#tema_rcn_so").val(data.tema);
                $("#tempat_rcn_so").val(data.tempat);
                $("#target_peserta_rcn_so").val(data.target_peserta);
                $("#jumlah_peserta_rcn_so").val(data.jumlah_peserta);
                $("#anggaran_rcn_so").val(data.anggaran);
                $("#keterangan_rcn_so").val(data.keterangan);
                $("#upt_provinsi").val(data.id_prov);
                $('#lampiran_rcn_so').attr('href',data.lampiran);
                     $.ajax({
                         type: "POST",
                         dataType: "html",
                         url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
                         data: "provinsi="+data.id_prov,
                         success: function(msg){
                            if(msg){
                              $("#kode_upt").html(msg);

                              setTimeout(function(){ 
                                   $("#kode_upt").val(data.kode_upt);        
                              }, 500);
                             
                            }
                         }
                      }); 
                var str = data.narasumber;
                var str_array = str.split('|');
                for(var i = 0; i < str_array.length; i++) {
                   str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
                    var  html ='<div style="margin-right:0px;margin-left:0px" class="row dinamis_narasumber" id="narasumber_'+i+'" >';
                         html+='<div class="col-md-10 form-group form-box col-xs-10">';
                           html+='<span class="label">Narasumber</span> <span class="required">*</span>';
                           html+='<input  class="form-control" value="'+str_array[i]+'"  name="narasumber[]"   placeholder="" type="text" required>';
                         html+='</div>';
                         html+='<div  class="col-md-2 form-group form-box col-xs-2">';
                          html+='<br/><button type="button"  onClick="hapus_narasumber(\''+i+'\');"  class="btn btn-danger btn-sm">Hapus</button>';
                         html+='</div></div>';
                         $("#data_narasumber").append(html);
                }

            }
        });
   }, 1000);
}
function do_edit_rencana_so(param_id,map_id,upt){
  call_modal_from_map('Ubah Rencana Sosialisasi','Form_Rencana_Sosialisasi',map_id,upt,'do_edit','sosialisasi_bimtek/form');
   setTimeout(function(){ 
       $.ajax({
            url: '<?php echo $basepath ?>sosialisasi_bimtek_rencana_sosialisasi/edit/'+param_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $("#kode_rencana_sosialisasi").val(data.kode_rencana_sosialisasi);
                $("#kegiatan_rcn_so").val(data.kegiatan);
                $("#tgl_pelaksanaan_rcn_so").val(data.tgl_pelaksanaan);
                $("#tema_rcn_so").val(data.tema);
                $("#tempat_rcn_so").val(data.tempat);
                $("#target_peserta_rcn_so").val(data.target_peserta);
                $("#jumlah_peserta_rcn_so").val(data.jumlah_peserta);
                $("#anggaran_rcn_so").val(data.anggaran);
                $("#keterangan_rcn_so").val(data.keterangan);
                $('#lampiran_rcn_so').attr('href',data.lampiran);
                var str = data.narasumber;
                var str_array = str.split('|');
                for(var i = 0; i < str_array.length; i++) {
                   str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
                    var  html ='<div style="margin-right:0px;margin-left:0px" class="row dinamis_narasumber" id="narasumber_'+i+'" >';
                         html+='<div class="col-md-10 form-group form-box col-xs-10">';
                           html+='<span class="label">Narasumber</span> <span class="required">*</span>';
                           html+='<input  class="form-control" value="'+str_array[i]+'"  name="narasumber[]"   placeholder="" type="text" required>';
                         html+='</div>';
                         html+='<div  class="col-md-2 form-group form-box col-xs-2">';
                          html+='<br/><button type="button"  onClick="hapus_narasumber(\''+i+'\');"  class="btn btn-danger btn-sm">Hapus</button>';
                         html+='</div></div>';
                         $("#data_narasumber").append(html);
                }
            }
        });
   }, 1000);
}
function do_detail_rencana_so(param_id,map_id,upt){
   call_modal_from_map('Detail Rencana Sosialisasi','Form_Rencana_Sosialisasi',map_id,upt,'do_detail','sosialisasi_bimtek/form');
   setTimeout(function(){ 
       $.ajax({
            url: '<?php echo $basepath ?>sosialisasi_bimtek_rencana_sosialisasi/edit/'+param_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $("#kode_rencana_sosialisasi").val(data.kode_rencana_sosialisasi);
                $("#kegiatan_rcn_so").val(data.kegiatan);
                $("#tgl_pelaksanaan_rcn_so").val(data.tgl_pelaksanaan);
                $("#tema_rcn_so").val(data.tema);
                $("#tempat_rcn_so").val(data.tempat);
                $("#target_peserta_rcn_so").val(data.target_peserta);
                $("#jumlah_peserta_rcn_so").val(data.jumlah_peserta);
                $("#anggaran_rcn_so").val(data.anggaran);
                $("#keterangan_rcn_so").val(data.keterangan);
                $('#lampiran_rcn_so').attr('href',data.lampiran);
                var str = data.narasumber;
                var str_array = str.split('|');
                for(var i = 0; i < str_array.length; i++) {
                   str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
                    var  html ='<div style="margin-right:0px;margin-left:0px" class="row" id="narasumber_'+i+'" >';
                         html+='<div class="col-md-12 form-group form-box col-xs-12">';
                           html+='<span class="label">Narasumber</span>';
                           html+='<input readonly class="form-control" value="'+str_array[i]+'"  name="narasumber[]"   placeholder="" type="text" required>';
                         html+='</div></div>';
                         $("#data_narasumber").append(html);
                }
            }
        });
   }, 1000);
}
function do_delete_rencana_so(parameter_data,param_id){
    act_delete('Hapus Rencana Sosialisasi','Anda ingin menghapus Rencana Sosialisasi ? ','warning','sosialisasi_bimtek_rencana_sosialisasi/delete','no_refresh','after_crud_rencana_sosialisasi',window.atob(parameter_data),window.atob(param_id));
}
function do_delete_rencana_so_all(parameter_data,param_id){
    act_delete('Hapus Rencana Sosialisasi','Anda ingin menghapus Rencana Sosialisasi ? ','warning','sosialisasi_bimtek_rencana_sosialisasi/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
}
//End Rencana Sosialisasi

//Start Monev Sosialisasi
function after_crud_monev_sosialisasi(){
    var map_id = $(".data_map_id_monev_so").val();
    var upt = $(".data_upt_monev_so").val();
    detail_monev_sosialisasi(map_id,upt);
    $('#call_modal').modal('hide');
}
function detail_monev_sosialisasi(id_map,upt){
  $(".data_map_id_monev_so").val(id_map);
  $(".data_upt_monev_so").val(upt);
  $('.monev_sosialisasi_utama').hide();
  ajax_table_map('monev_sosialisasi_table_data_utama',id_map,upt,'sosialisasi_bimtek/table');
  //$('html, body').animate({
       // scrollTop: $("#pelayanan_table_data_utama").offset().top;
  //}, 1000);
  refresh_table();
}     
function do_edit_monev_so_all(param_id){
   call_modal_default('Ubah Monev Sosialisasi','Form_Monev_Sosialisasi_ALL','do_edit','sosialisasi_bimtek/form_all');
   setTimeout(function(){ 
       $.ajax({
            url: '<?php echo $basepath ?>sosialisasi_bimtek_monev_sosialisasi/edit/'+param_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                var gabung = data.tgl_pelaksanaan+" - "+data.nama_kegiatan+" - "+data.tema;
                $("#suggest").val(gabung);
                $("#kode_monev_sosialisasi").val(data.kode_monev_sosialisasi);
                $("#kegiatan_mnv_so").val(data.kegiatan);
                $("#tgl_pelaksanaan_mnv_so").val(data.tgl_pelaksanaan);
                $("#tema_mnv_so").val(data.tema);
                $("#tempat_mnv_so").val(data.tempat);
                $("#target_peserta_mnv_so").val(data.target_peserta);
                $("#jumlah_peserta_mnv_so").val(data.jumlah_peserta);
                $("#anggaran_mnv_so").val(data.anggaran);
                $("#keterangan_mnv_so").val(data.keterangan);
                $('#lampiran_mnv_so').attr('href',data.lampiran);
                $("#upt_provinsi").val(data.id_prov);
                     $.ajax({
                         type: "POST",
                         dataType: "html",
                         url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
                         data: "provinsi="+data.id_prov,
                         success: function(msg){
                            if(msg){
                              $("#kode_upt").html(msg);

                              setTimeout(function(){ 
                                   $("#kode_upt").val(data.kode_upt);        
                              }, 500);
                             
                            }
                         }
                      }); 
                var str = data.narasumber;
                var str_array = str.split('|');
                for(var i = 0; i < str_array.length; i++) {
                   str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
                    var  html ='<div style="margin-right:0px;margin-left:0px" class="row dinamis_narasumber" id="narasumber_'+i+'" >';
                         html+='<div class="col-md-10 form-group form-box col-xs-10">';
                           html+='<span class="label">Narasumber</span> <span class="required">*</span>';
                           html+='<input  class="form-control" value="'+str_array[i]+'"  name="narasumber[]"   placeholder="" type="text" required>';
                         html+='</div>';
                         html+='<div  class="col-md-2 form-group form-box col-xs-2">';
                          html+='<br/><button type="button"  onClick="hapus_narasumber(\''+i+'\');"  class="btn btn-danger btn-sm">Hapus</button>';
                         html+='</div></div>';
                         $("#data_narasumber").append(html);
                }

            }
        });
   }, 1000);
}
function do_edit_monev_so(param_id,map_id,upt){
  call_modal_from_map('Ubah Monev Sosialisasi','Form_Monev_Sosialisasi',map_id,upt,'do_edit','sosialisasi_bimtek/form');
   setTimeout(function(){ 
       $.ajax({
            url: '<?php echo $basepath ?>sosialisasi_bimtek_monev_sosialisasi/edit/'+param_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $("#kode_rencana_sosialisasi").val(data.kode_rencana_sosialisasi);
                $("#kegiatan_rcn_so").val(data.kegiatan);
                $("#tgl_pelaksanaan_rcn_so").val(data.tgl_pelaksanaan);
                $("#tema_rcn_so").val(data.tema);
                $("#tempat_rcn_so").val(data.tempat);
                $("#target_peserta_rcn_so").val(data.target_peserta);
                $("#jumlah_peserta_rcn_so").val(data.jumlah_peserta);
                $("#anggaran_rcn_so").val(data.anggaran);
                $("#keterangan_rcn_so").val(data.keterangan);
                $('#lampiran_rcn_so').attr('href',data.lampiran);
                var str = data.narasumber;
                var str_array = str.split('|');
                for(var i = 0; i < str_array.length; i++) {
                   str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
                    var  html ='<div style="margin-right:0px;margin-left:0px" class="row dinamis_narasumber" id="narasumber_'+i+'" >';
                         html+='<div class="col-md-10 form-group form-box col-xs-10">';
                           html+='<span class="label">Narasumber</span> <span class="required">*</span>';
                           html+='<input  class="form-control" value="'+str_array[i]+'"  name="narasumber[]"   placeholder="" type="text" required>';
                         html+='</div>';
                         html+='<div  class="col-md-2 form-group form-box col-xs-2">';
                          html+='<br/><button type="button"  onClick="hapus_narasumber(\''+i+'\');"  class="btn btn-danger btn-sm">Hapus</button>';
                         html+='</div></div>';
                         $("#data_narasumber").append(html);
                }
            }
        });
   }, 1000);
}
function do_detail_monev_so(param_id,map_id,upt){
   call_modal_from_map('Detail Monev Sosialisasi','Form_Monev_Sosialisasi',map_id,upt,'do_detail','sosialisasi_bimtek/form');
   setTimeout(function(){ 
       $.ajax({
            url: '<?php echo $basepath ?>sosialisasi_bimtek_monev_sosialisasi/edit/'+param_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $("#kode_monev_sosialisasi").val(data.kode_monev_sosialisasi);
                $("#kegiatan_mnv_so").val(data.kegiatan);
                $("#tgl_pelaksanaan_mnv_so").val(data.tgl_pelaksanaan);
                $("#tema_mnv_so").val(data.tema);
                $("#tempat_mnv_so").val(data.tempat);
                $("#target_peserta_mnv_so").val(data.target_peserta);
                $("#jumlah_peserta_mnv_so").val(data.jumlah_peserta);
                $("#anggaran_mnv_so").val(data.anggaran);
                $("#keterangan_mnv_so").val(data.keterangan);
                $('#lampiran_mnv_so').attr('href',data.lampiran);
                var str = data.narasumber;
                var str_array = str.split('|');
                for(var i = 0; i < str_array.length; i++) {
                   str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
                    var  html ='<div style="margin-right:0px;margin-left:0px" class="row" id="narasumber_'+i+'" >';
                         html+='<div class="col-md-12 form-group form-box col-xs-12">';
                           html+='<span class="label">Narasumber</span>';
                           html+='<input readonly class="form-control" value="'+str_array[i]+'"  name="narasumber[]"   placeholder="" type="text" required>';
                         html+='</div></div>';
                         $("#data_narasumber").append(html);
                }
            }
        });
   }, 1000);
}
function do_delete_monev_so(parameter_data,param_id){
    act_delete('Hapus Monev Sosialisasi','Anda ingin menghapus Monev Sosialisasi ? ','warning','sosialisasi_bimtek_monev_sosialisasi/delete','no_refresh','after_crud_monev_sosialisasi',window.atob(parameter_data),window.atob(param_id));
}
function do_delete_monev_so_all(parameter_data,param_id){
    act_delete('Hapus Monev Sosialisasi','Anda ingin menghapus Monev Sosialisasi ? ','warning','sosialisasi_bimtek_monev_sosialisasi/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
}
//End Monev Sosialisasi

//Start Galeri
function after_crud_galeri(){
    var map_id = $(".data_map_id_galeri").val();
    var upt = $(".data_upt_galeri").val();
    detail_galeri(map_id,upt);
    $('#call_modal').modal('hide');
}
function detail_galeri(id_map,upt){
  $(".data_map_id_galeri").val(id_map);
  $(".data_upt_galeri").val(upt);
  $('.galeri_utama').hide();
  ajax_table_map('galeri_table_data_utama',id_map,upt,'sosialisasi_bimtek/table');
  //$('html, body').animate({
       // scrollTop: $("#pelayanan_table_data_utama").offset().top;
  //}, 1000);
  refresh_table();
}     
function do_edit_galeri_all(param_id){
  call_modal_default('Ubah Galeri','Form_Galeri_ALL','do_edit','sosialisasi_bimtek/form_all');
   setTimeout(function(){ 
       $.ajax({
            url: '<?php echo $basepath ?>sosialisasi_bimtek_galeri/edit/'+param_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $("#kode_galeri").val(data.kode_galeri);
                $("#kegiatan_galeri").val(data.kegiatan);
                $("#judul_bhn_so").val(data.judul);
                $("#deskripsi_galeri").val(data.deskripsi);
                $("#keterangan_galeri").val(data.keterangan);
                $("#upt_provinsi").val(data.id_prov);
                $('#lampiran_galeri').attr('href',data.lampiran)
                     $.ajax({
                         type: "POST",
                         dataType: "html",
                         url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
                         data: "provinsi="+data.id_prov,
                         success: function(msg){
                            if(msg){
                              $("#kode_upt").html(msg);

                              setTimeout(function(){ 
                                   $("#kode_upt").val(data.kode_upt);        
                              }, 500);
                             
                            }
                         }
                      }); 
            }
        });
   }, 1000);
}
function do_edit_galeri(param_id,map_id,upt){
  call_modal_from_map('Ubah Galeri','Form_Galeri',map_id,upt,'do_edit','sosialisasi_bimtek/form');
   setTimeout(function(){ 
       $.ajax({
            url: '<?php echo $basepath ?>sosialisasi_bimtek_galeri/edit/'+param_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $("#kode_galeri").val(data.kode_galeri);
                $("#kegiatan_galeri").val(data.kegiatan);
                $("#judul_bhn_so").val(data.judul);
                $("#deskripsi_galeri").val(data.deskripsi);
                $("#keterangan_galeri").val(data.keterangan);
                $("#upt_provinsi").val(data.id_prov);
                $("#kode_upt").val(data.kode_upt);    
                $('#lampiran_galeri').attr('href',data.lampiran);
            }
        });
   }, 1000);
}
function do_detail_galeri(param_id,map_id,upt){
   call_modal_from_map('Detail Galeri','Form_Galeri',map_id,upt,'do_detail','sosialisasi_bimtek/form');
   setTimeout(function(){ 
       $.ajax({
            url: '<?php echo $basepath ?>sosialisasi_bimtek_galeri/edit/'+param_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $("#kode_galeri").val(data.kode_galeri);
                $("#kegiatan_galeri").val(data.kegiatan);
                $("#judul_bhn_so").val(data.judul);
                $("#deskripsi_galeri").val(data.deskripsi);
                $("#keterangan_galeri").val(data.keterangan);
                $("#upt_provinsi").val(data.id_prov);
                $("#kode_upt").val(data.kode_upt);    
                $('#lampiran_galeri').attr('href',data.lampiran);
            }
        });
   }, 1000);
}
function do_delete_galeri(parameter_data,param_id){
    act_delete('Hapus Galeri','Anda ingin menghapus Galeri ? ','warning','sosialisasi_bimtek_galeri/delete','no_refresh','after_crud_galeri',window.atob(parameter_data),window.atob(param_id));
}
function do_delete_galeri_all(parameter_data,param_id){
    act_delete('Hapus Galeri','Anda ingin menghapus Galeri ? ','warning','sosialisasi_bimtek_galeri/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
}
//End Galeri

function refresh_table(){
  setTimeout(function(){ 
    //Bahan Sosialisasi
    $('#tb_bahan_sosialisasi').DataTable().ajax.reload();
    $('#tb_bahan_sosialisasi_waiting').DataTable().ajax.reload();

    //Rencana Sosialisasi
    $('#tb_rencana_sosialisasi').DataTable().ajax.reload();
    $('#tb_rencana_sosialisasi_waiting').DataTable().ajax.reload();  

    //Monev Sosialisasi
    $('#tb_monev_sosialisasi').DataTable().ajax.reload();
    $('#tb_monev_sosialisasi_waiting').DataTable().ajax.reload();

    //Galeri
    $('#tb_galeri').DataTable().ajax.reload();
    $('#tb_galeri_waiting').DataTable().ajax.reload();
  }, 1000);
  $("#call_modal").modal("hide");
  $(".call_modal").modal("hide");
}
</script>
<?php } ?>