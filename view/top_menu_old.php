   <!-- Header Bar -->
  <div class="container-fluid animated bounceInDown">
    <div class="row">
      <div class="main-bar">
        <div class="leftBar">
          <a href="<?php echo $basepath ?>home"><img class="logo-desktop" src="<?php echo $basepath ?>assets/images/left-logo.jpg" alt="<?php echo $web['judul']?>"></a>
          <a href="javascript:void(0)" style="text-decoration:none" onclick="myFunction()" class="mobile-menu-stack"><i class="fa fa-equals"></i></a>
        </div>
        <span class="title-mobile"><?php echo $web['judul']?></span>
        <div class="rightBar">
          <ul class="right-menu">
             <li class="admin-list">
              <div class="dropdown">
                <button class="btn btn-sm dropdown-toggle" style="background-color: transparent;" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-envelope" style="color: #007bff;"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                  <small style="padding: 5px 10px;">2 Pesan</small>
                  <hr>
                  <button class="dropdown-item" type="button"><i class="fa fa-envelope" style="font-size: 13px; color:#999;"></i> Admin1</button>
                  <button class="dropdown-item" type="button"><i class="fa fa-envelope" style="font-size: 13px; color:#999;"></i> Hermawan</button>
                </div>
                </div>
            </li> 
            <li class="admin-list"><a style="color:blue"><?php echo $_SESSION['username']  ?> / <?php echo $group_name  ?></a></li>
            <li><div class="line-sp"></div></li>
            <li>
              <a style="cursor:pointer"  class="desktop-setting"><img src="<?php echo $basepath ?>assets/images/user.png" width="100%" alt="<?php echo ucwords($_SESSION['username']) ?>"></a>
             <!--<a href="<?php echo $basepath ?>profile" class="desktop-setting"><img src="<?php echo $basepath ?>assets/images/poto.jpg" width="100%" alt="<?php echo ucwords($_SESSION['username']) ?>"></a>-->
            </li>
            <li><a href="<?php echo $basepath ?>auth/logout"  class="btn-logout" style="color:red" title="Keluar"><i class="fa fa-times"></i></a></li>
            <!--<li><a style="color:red;cursor:pointer;" onclick="logout()" title="Keluar" ><i class="fa fa-times"></i></a></li>-->
          </ul>
        </div>
      </div>
    </div>
  </div>