<?php  
$akses_add_tab_loket = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>1,'kode_function_access'=>$_SESSION['group_user'])); 
$akses_add_tab_loket    = (!empty($akses_add_tab_loket) ? $akses_add_tab_loket : '0');
$my_data_upt = $gen_model->GetOneRow("ms_upt",array('kode_upt'=>$upt)); 
?>
<style type="text/css">
	.table th {
	   text-align: center;
	   font-weight:bold;
	}
	.datatable>tbody>tr>td
	{
		white-space: nowrap;
	} 
</style>
<div style="background-color:white">
	<label class="loc-lbl">UPT <?php echo $wilayah['nama_prov'] ?></label>
	<div class="row">
		<div class="col-md-12">
			<?php 
				if($_SESSION['group_user']=="grp_171026194411_1142"){
			?>
		 <select class="form_control" onchange="get_detail_pengunjung()" id="mydata_upt_pengunjung" style="width: 100%;">
		    <?php 
		       $data_upt = $gen_model->GetWhere('ms_upt',array('id_prov'=>$wilayah['id_prov']));
		       echo '<option value="">Pilih UPT</option>';
		        while($list = $data_upt->FetchRow()){
		          foreach($list as $key=>$val){
		                      $key=strtolower($key);
		                      $$key=$val;
		                    }  
		      ?>
		    <option <?php echo ($kode_upt==$upt ? 'selected' : '') ?> value="<?php echo $kode_upt ?>"><?php echo $nama_upt ?></option>
		    <?php }  ?>
		 </select><br/>
		 	<?php } else { ?>
	        		<label style="margin-top: 0px;color:black;background-color:white" class="loc-lbl"><?php echo $my_data_upt['nama_upt'] ?></label>
		 	<?php } ?>
		</div>
	</div>
</div>
<script type="text/javascript">
 function get_detail_pengunjung(){
    var  map_id = "<?php echo $map_id ?>";
    var  upt    = $("#mydata_upt_pengunjung").val();
    if(upt){
         detail_loket_pelayanan(map_id,upt);
    }
 }
</script>


<input type="hidden" name="filter_provinsi" id="filter_provinsi" value="<?php echo $wilayah['id_prov'] ?>" >
<input type="hidden" name="filter_upt" id="filter_upt" value="<?php echo $upt  ?>" >
<input type="hidden" name="LoketPelayananPencarian" id="LoketPelayananPencarian" >
<input type="hidden" value="<?php echo $map_id ?>" name="data_map_id_pengunjung" class="data_map_id_pengunjung">
<input type="hidden" value="<?php echo $map_id ?>" name="data_map_id_spp" class="data_map_id_spp">
<input type="hidden" value="<?php echo $upt ?>" name="data_upt_pengunjung" class="data_upt_pengunjung">
<div class="row" id="chart_detail_loket_pelayanan_1">
	<div class="col-sm-12">
	  <div class="card-set">
	    <div class="headset"><i class="fa fa-chart-pie"></i> Grafik Jumlah Pengunjung</div>
	    <div id="pl_loket_pelayanan_grafik_jumlah_per_tujuan"></div>
	  </div>
	</div>
</div><br/>
<?php 
	// $check_waiting = "SELECT  count(*) as total
	// 				FROM tr_pelayanan as pl
	// 				left outer join ms_company as cp
	// 					on cp.company_id=pl.kode_perusahaan
	// 				left outer join ms_user as us_crt
	// 					on us_crt.kode_user=pl.created_by
	// 				left outer join ms_user as us_updt
	// 					on us_updt.kode_user=pl.last_update_by
	// 				where 1=1  and pl.id_prov='".$wilayah['id_prov']."'  and pl.kode_upt='".$upt."' and pl.status in ('0','1')";
	// $total_waiting = $db->GetOne($check_waiting);	
	// if($total_waiting!="0"){			
?>

<!-- Tabel Persetujuan-->
	<div class="cnt-header row" style="padding-right: 0px;display:none">
		<h6 class="table-label">Menunggu Persetujuan</h6>
			<?php  if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT ?>
				<a style="cursor:pointer;color:white" onclick="verif_data('kode_pelayanan','pelayanan_loket/verifikasi','no_refresh','refresh_table')" class="btn btn-success btn-sm pull-right"><i class="fa fa-check"></i> Verifikasi Data</a>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<a style="cursor:pointer;color:white" data-toggle="modal" title="Reject" data-target="#Reject_LoketPelayananDetail"  class="btn btn-danger btn-sm pull-right"><i class="fa fa-times"></i> Reject Data</a>
			<?php } ?>
	</div>

	<div class="table table-responsive pelayanan-top" style="display:none">
		<table id="tb_loket_waiting" class="display table table-striped pelayanan-table">
			<thead>
				<tr>
					<th class="text-center"></th>
					<th class="text-center">Tanggal</th>
					<th class="text-center">Status</th>
					<th class="text-center">Nama</th>
					<th class="text-center">Jabatan</th>
					<th class="text-center">Perusahaan</th>
					<th class="text-center">No.Tlp</th>
					<th class="text-center">No.Hp</th>
					<th class="text-center">Email</th>
					<th class="text-center">Tujuan</th>
					<th class="text-center">Perizinan</th>
					<th class="text-center">Keterangan</th>
					<th class="text-center">Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<?php //} ?>

	<script type="text/javascript">
		function print_laporan_detail(tipe){
			var param = $("#LoketPelayananPencarian").val();

			if(!param){
				var prov = " and pl.id_prov ='<?php echo $wilayah['id_prov'] ?>' ";
				param += prov;
				
				var upt = " and pl.kode_upt='<?php echo $upt ?>' ";
				param += upt;

				param = btoa(param);
			}

		    var form = document.createElement("form");
		    form.setAttribute("method", "POST");
		    if(tipe=="print"){
		  	 form.setAttribute("target", "_blank");
		    }
		    form.setAttribute("action", "<?php echo $basepath ?>pelayanan_loket/cetak_detail");
		           
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", "param");
            hiddenField.setAttribute("value", param);

            var hiddenField2 = document.createElement("input");
            hiddenField2.setAttribute("type", "hidden");
            hiddenField2.setAttribute("name", "tipe");
            hiddenField2.setAttribute("value", tipe);

            form.appendChild(hiddenField);
            form.appendChild(hiddenField2);

		    document.body.appendChild(form);
		    form.submit();
		}
	</script>

	<!-- Tabel Profil & Data Pengunjung -->
	<div class="cnt-header row" style="margin-top: 30px; margin-bottom: 20px;">
		<h6 class="table-label" style="display:block; width:100%; clear: both;"><i class="fa fa-portrait"></i> Profil & Data Pengunjung</h6>
		<!--<span style="float:left"><?php echo $my_data_upt['nama_upt'] ?>
        		<?php echo (!empty($my_data_upt['alamat']) ? '<br/>'.$my_data_upt['alamat'] : '')  ?>
        </span>-->

		<div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">

			<?php  if($akses_add_tab_loket=="1"){  ?>
				<a href="javascript:void(0)"  data-tooltip="tooltip" data-placement="bottom" title="Tambah" onclick="call_modal_from_map('Tambah Profil & Data Pengunjung UPT','Form_Pelayanan_Loket','<?php echo $map_id ?>','<?php echo $upt ?>','do_add','pelayanan/form')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
			<?php } ?>

			<a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Cetak" onclick="print_laporan_detail('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

			<?php  if($akses_add_tab_loket=="1"){  ?>
				<a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Import" data-target="#Import_LoketPelayananDetail"><i class="fa fa-upload"></i></a>
			<?php } ?>
			<a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Filterisasi" data-target="#Filter_LoketPelayananDetail"  ><i class="fa fa-filter"></i></a>

			<div class="btn-group" role="group"  data-tooltip="tooltip" data-placement="bottom" title="Export">
				<button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fa fa-file-pdf"></i>
				</button>
				<div class="dropdown-menu" aria-labelledby="red">
					<a class="dropdown-item"  onclick="print_laporan_detail('pdf')" href="javascript:void(0)">Download PDF</a>
					<a class="dropdown-item"   onclick="print_laporan_detail('xls')" href="javascript:void(0)">Download XLS</a>
					<a class="dropdown-item" onclick="print_laporan_detail('doc')"   href="javascript:void(0)">Download DOCS</a>
				</div>
			</div>
		</div>
		<hr style="width:100%; display: block; float: left;">
	</div>



	<div class="table table-responsive pelayanan-top" id="filter_pelayanan_detail">
		<table id="tb_loket" class="display table table-striped pelayanan-table" style="width: 100%;">
			<thead>
				<tr>
					<th class="text-center">Tanggal</th>
					<th class="text-center">Nama</th>
					<th class="text-center">Jabatan</th>
					<th class="text-center">Perusahaan</th>
					<th class="text-center">No.Tlp</th>
					<th class="text-center">No.Hp</th>
					<th class="text-center">Email</th>
					<th class="text-center">Tujuan</th>
					<th class="text-center">Perizinan</th>
					<th class="text-center">Keterangan</th>
					<th class="text-center">Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<script type="text/javascript">

	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});
	  //Loket Detail
        var dTable_loket_detail; 
         dTable_loket_detail = $('#tb_loket').DataTable( {
          "bProcessing": true,
          "bServerSide": true,
          "bJQueryUI": false,
          "searching": false,
          "responsive": false,
          "autoWidth": false,
          "sAjaxSource": "<?php echo $basepath ?>pelayanan_loket/rest_provinsi&id_prov=<?php echo $wilayah['id_prov'] ?>&upt=<?php echo $upt ?>", 
          "sServerMethod": "POST",
          "scrollX": true,
          "scrollY": "350px",
          "scrollCollapse": true,
          "order": [[ 0, "desc" ]],
          "columnDefs": [
          { "orderable": true, "targets": 0, "searchable": true},
          { "orderable": true, "targets": 1, "searchable": true},
          { "orderable": true, "targets": 2, "searchable": true},
          { "orderable": true, "targets": 3, "searchable": true},
          { "orderable": true, "targets": 4, "searchable": true },
          { "orderable": true, "targets": 5, "searchable": true },
          { "orderable": true, "targets": 6, "searchable": true },
          { "orderable": true, "targets": 7, "searchable": true },
          { "orderable": true, "targets": 8, "searchable": true },
          { "orderable": true, "targets": 9, "searchable": true },
          { "orderable": false, "targets": 10, "searchable": false, "width":170}
          ]
        } ); 	
	</script>

	
	<div class="cnt-header row" style="margin-top: 30px; margin-bottom: 20px;">
		<h6 class="table-label" style="display:block; width:100%; clear: both;"><i class="fa fa-portrait"></i> Resume UPT </h6>
		<!-- <span style="float:left"><?php echo $my_data_upt['nama_upt'] ?>
        		<?php echo (!empty($my_data_upt['alamat']) ? '<br/>'.$my_data_upt['alamat'] : '')  ?>
        </span>-->
        <hr style="width:100%; display: block; float: left;">
	</div>

    <?php   $count_tj = $gen_model->getOne("count(*)","ms_tujuan"); ?>
    <div class="table table-responsive pelayanan-top">
        <table id="tb_loket_akumulasi_detail" class="table table-striped pelayanan-table" style="width: 100%;">
          <thead>
            <tr>
              <th colspan="<?php echo $count_tj ?>" scope="col" style="text-align: center;">Tujuan</th>
              <th rowspan="2" scope="col">Total Pengunjung</th>
            </tr>
             <tr>
          <?php 
              $data_tj = $db->SelectLimit("select * from ms_tujuan"); 
              while($list = $data_tj->FetchRow()){
                foreach($list as $key=>$val){
                      $key=strtolower($key);
                      $$key=$val;
                    }  
            ?>  
          <th scope="col"><?php echo $tujuan ?></th>
        <?php } ?>
        </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
        <hr>
    </div>
    <script type="text/javascript">
    	$('[data-tooltip="tooltip"]').tooltip();
     //Start Loket Detail
        //Loket Detail Waiting
        var dTable_loket_detail_waiting;
            dTable_loket_detail_waiting = $('#tb_loket_waiting').DataTable( {
              "bProcessing": true,
              "bServerSide": true,
              "bJQueryUI": false,
              "responsive": false,
              "autoWidth": false,
              "retrieve": true,
              "bDestroy":true,
              "sAjaxSource": "<?php echo $basepath ?>pelayanan_loket/rest_waiting&id_prov=<?php echo $wilayah['id_prov'] ?>&upt=<?php echo $upt ?>", 
              "sServerMethod": "POST",
              "scrollX": true,
              "scrollY": "350px",
               "scrollCollapse": true,
               "order": [[ 1, "desc" ]],
              "columnDefs": [
              { "orderable": false, "targets": 0, "searchable": false},
              { "orderable": true, "targets": 1, "searchable": true},
              { "orderable": true, "targets": 2, "searchable": true},
              { "orderable": true, "targets": 3, "searchable": true},
              { "orderable": true, "targets": 4, "searchable": true },
              { "orderable": true, "targets": 5, "searchable": true },
              { "orderable": true, "targets": 6, "searchable": true },
              { "orderable": true, "targets": 7, "searchable": true },
              { "orderable": true, "targets": 8, "searchable": true },
              { "orderable": true, "targets": 9, "searchable": true },
              { "orderable": true, "targets": 10, "searchable": true },
              { "orderable": true, "targets": 11, "searchable": true },
              { "orderable": false, "targets": 12, "searchable": false, "width":170}
              ]
            } );

       

        //Loket Detail Akumulasi
        var dTable_loket_detail_akumulasi;
            dTable_loket_detail_akumulasi = $('#tb_loket_akumulasi_detail').DataTable( {
              "bProcessing": true,
              "bServerSide": true,
              "bJQueryUI": false,
              "retrieve": true,
              "bDestroy":true,
              "responsive": false,
              "autoWidth": false,
              "sAjaxSource": "<?php echo $basepath ?>pelayanan_loket/akumulasi_detail&id_prov=<?php echo $wilayah['id_prov'] ?>&upt=<?php echo $upt ?>", 
              "sServerMethod": "POST",
              "scrollX": true,
              "scrollY": "350px",
                "scrollCollapse": true,
              "columnDefs": [
              <?php
	            $no = 0;
	            $dt="";
	            for ($x=1;$x <= $count_tj; $x++) {
	               $dt .= '{ "orderable": false, "targets": '.$no.', "searchable": false},'.PHP_EOL;
	               $no++;

	            }
	            echo $dt;
	            ?>

              { "orderable": false, "targets": <?php echo $no; ?>, "searchable": false}
              ]
            } );
   
   //Start Loket Detail
	
    //Chart          
	var chart_detail_loket_pelayanan_1 = [
	  <?php 
	        $loket_detail_no_1 = 0;	
	        $chart_tujuan="";
	        $sql = "select count(*) as total,tj.tujuan
	                from tr_pelayanan as pl
                  		 inner join ms_tujuan as tj 
                      		on tj.kode_tujuan=pl.tujuan
                   where pl.status='3' and pl.id_prov='".$wilayah['id_prov']."' and pl.kode_upt='".$upt."'  GROUP BY pl.tujuan";
	        $get_chart_tujuan  = $db->Execute($sql);
	        while($list = $get_chart_tujuan->FetchRow()){
	          foreach($list as $key=>$val){
	                            $key=strtolower($key);
	                            $$key=$val;
	          }  
	          $chart_tujuan .='{"name":"'.$tujuan.'","y":'.$total.'},';
	          $loket_detail_no_1++;
	        } 
	        echo trim($chart_tujuan,',');
	  ?>
	];
	pie_chart('pl_loket_pelayanan_grafik_jumlah_per_tujuan',chart_detail_loket_pelayanan_1);

	<?php 
        if($loket_detail_no_1==0){
          echo " $('#chart_detail_loket_pelayanan_1').hide(); ";
        }
     ?>
</script>