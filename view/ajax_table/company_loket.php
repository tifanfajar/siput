<button title="Edit"  type="button"  onclick="company_loket_add()"  class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah Perusahaan</button><br/><br/>
<table id="tb_company_loket" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
                    <th>No Klien Licence</th>
                    <th>Nama Perusahaan</th>
                    <th>No Telp</th>
                    <th>No Hp</th>
                    <th>Provinsi</th>
                    <th>Kota/Kabupaten</th>
                    <th>Alamat</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
               <script type="text/javascript">
              var dTable1;
              $(document).ready(function() {
                dTable1 = $('#tb_company_loket').DataTable( {
                  "bProcessing": true,
                  "searching": true,
                  "bServerSide": true,
                  "bJQueryUI": false,
                  "retrieve": true,
                  "responsive": false,
                  "autoWidth": false,
                  "sAjaxSource": "<?php echo $basepath ?>company/rest_loket", 
                  "sServerMethod": "POST",
                  "scrollX": true,
                  "scrollY": "350px",
                    "scrollCollapse": true,
                  "columnDefs": [
                  { "orderable": true, "targets": 0, "searchable": true},
                    { "orderable": true, "targets": 1, "searchable": true},
                    { "orderable": true, "targets": 2, "searchable": true},
                    { "orderable": true, "targets": 3, "searchable": true},
                    { "orderable": true, "targets": 4, "searchable": true },
                    { "orderable": true, "targets": 5, "searchable": true },
                    { "orderable": true, "targets": 6, "searchable": true },
                    { "orderable": false, "targets": 7, "searchable": false, "width":170}
                  ]
                } );
              });
            </script>