<?php  
 $my_data_upt = $gen_model->GetOneRow("ms_upt",array('kode_upt'=>$upt)); 
 $akses_add_tab_bhn_so = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>7,'kode_function_access'=>$_SESSION['group_user'])); 
 $akses_add_tab_bhn_so      = (!empty($akses_add_tab_bhn_so) ? $akses_add_tab_bhn_so : '0');
?>
<style type="text/css">
	.table th {
	   text-align: center;
	   font-weight:bold;
	}
	.datatable>tbody>tr>td
	{
		white-space: nowrap;
	} 
</style>
<div style="background-color:white">
	<label class="loc-lbl">UPT <?php echo $wilayah['nama_prov'] ?></label>
	<div class="row">
		<div class="col-md-12">
			<?php 
				if($_SESSION['group_user']=="grp_171026194411_1142"){
			?>
		 <select class="form_control" onchange="get_detail_bhn_sosialisasi()" id="mydata_upt_bhn_so" style="width: 100%;">
		    <?php 
		       $data_upt = $gen_model->GetWhere('ms_upt',array('id_prov'=>$wilayah['id_prov']));
		       echo '<option value="">Pilih UPT</option>';
		        while($list = $data_upt->FetchRow()){
		          foreach($list as $key=>$val){
		                      $key=strtolower($key);
		                      $$key=$val;
		                    }  
		      ?>
		    <option <?php echo ($kode_upt==$upt ? 'selected' : '') ?> value="<?php echo $kode_upt ?>"><?php echo $nama_upt ?></option>
		    <?php }  ?>
		 </select><br/>
		 	<?php } else { ?>
	        		<label style="margin-top: 0px;color:black;background-color:white" class="loc-lbl"><?php echo $my_data_upt['nama_upt'] ?></label>
		 	<?php } ?>
		</div>
	</div>
</div>
<script type="text/javascript">
 function get_detail_bhn_sosialisasi(){
    var  map_id = "<?php echo $map_id ?>";
    var  upt    = $("#mydata_upt_bhn_so").val();
    if(upt){
         detail_bhn_sosialisasi(map_id,upt);
    }
 }
</script>


<input type="hidden" name="filter_provinsi" id="filter_provinsi_bhn_so" value="<?php echo $wilayah['id_prov'] ?>" >
<input type="hidden" name="filter_upt" id="filter_upt_bhn_so" value="<?php echo $upt  ?>" >
<input type="hidden" name="BhnSosialisasiPencarian" id="BhnSosialisasiPencarian" >
<input type="hidden" value="<?php echo $map_id ?>" name="data_map_id_bhn_so" class="data_map_id_bhn_so">
<input type="hidden" value="<?php echo $upt ?>" name="data_upt_bhn_so" class="data_upt_bhn_so">
<!--<div class="row" id="div_chart_detail_bhn_so_1">
	<div class="col-sm-12">
	  <div class="card-set">
	    <div class="headset"><i class="fa fa-chart-pie"></i>  Grafik Jumlah Per Materi</div>
	    <div id="chart_detail_bhn_so_per_materi"></div>
	  </div>
	</div>
</div><br/>-->
<?php 
	// $check_waiting = "SELECT  count(*) as total
	// 				FROM tr_bahan_sosialisasi as bhn
	// 				left outer join ms_user as us_crt
	// 					on us_crt.kode_user=bhn.created_by
	// 				left outer join ms_user as us_updt
	// 					on us_updt.kode_user=bhn.last_update_by
	// 				where 1=1  and bhn.id_prov='".$wilayah['id_prov']."'  and bhn.kode_upt='".$upt."' and bhn.status in ('0','1')";
	// $total_waiting = $db->GetOne($check_waiting);	
	// if($total_waiting!="0"){			
?>

<!-- Tabel Persetujuan-->
	<!--<div class="cnt-header row" style="padding-right: 0px;">
		<h6 class="table-label">Menunggu Persetujuan</h6>
			<?php  if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT ?>
				<a style="cursor:pointer;color:white" onclick="verif_data('kode_bhn_sosialisasi','sosialisasi_bimtek_bahan_sosialisasi/verifikasi','no_refresh','refresh_table')" class="btn btn-success btn-sm pull-right"><i class="fa fa-check"></i> Verifikasi Data</a>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<a style="cursor:pointer;color:white" data-toggle="modal" title="Reject" data-target="#Reject_Bahan_Sosialisasi"  class="btn btn-danger btn-sm pull-right"><i class="fa fa-times"></i> Reject Data</a>
			<?php } ?>
	</div>

	<div class="table table-responsive pelayanan-top">
		<table id="tb_bahan_sosialisasi_waiting" class="display table table-striped pelayanan-table">
			<thead>
				<tr>
					<th class="text-center"></th>
					<th class="text-center">Tanggal</th>
					<th class="text-center">Status</th>
                    <th class="text-center">Materi</th>
                    <th class="text-center">Judul</th>
                    <th class="text-center">Author</th>
                    <th class="text-center">Lampiran</th>
                    <th class="text-center">Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<script type="text/javascript">
		var dTable;
				$(document).ready(function() {
					dTable = $('#tb_bahan_sosialisasi_waiting').DataTable( {
						"bProcessing": true,
						"bServerSide": true,
						"bJQueryUI": false,
						"responsive": false,
						"autoWidth": false,
						"sAjaxSource": "<?php echo $basepath ?>sosialisasi_bimtek_bahan_sosialisasi/rest_waiting&id_prov=<?php echo $wilayah['id_prov'] ?>&upt=<?php echo $upt ?>", 
						"sServerMethod": "POST",
						"scrollX": true,
						"scrollY": "350px",
	  					"scrollCollapse": true,
	  					"order": [[ 1, "desc" ]],
						"columnDefs": [
						  { "orderable": false, "targets": 0, "searchable": false},
						  { "orderable": true, "targets": 1, "searchable": true},
		                  { "orderable": true, "targets": 2, "searchable": true},
		                  { "orderable": true, "targets": 3, "searchable": true},
		                  { "orderable": true, "targets": 4, "searchable": true},
		                  { "orderable": true, "targets": 5, "searchable": true },
		                  { "orderable": true, "targets": 6, "searchable": true },
		                  { "orderable": false, "targets": 7, "searchable": false, "width":200}
						]
					} );
				});
	</script>-->
	<?php // } ?>

	<script type="text/javascript">
		function print_laporan_bhn_detail(tipe){
			var param = $("#BhnSosialisasiPencarian").val();

			if(!param){
				// var prov = " and bhn.id_prov ='<?php echo $wilayah['id_prov'] ?>' ";
				// param += prov;
				
				// var upt = " and bhn.kode_upt='<?php echo $upt ?>' ";
				// param += upt;

				// param = btoa(param);
			}

		    var form = document.createElement("form");
		    form.setAttribute("method", "POST");
		    if(tipe=="print"){
		  	 form.setAttribute("target", "_blank");
		    }
		    form.setAttribute("action", "<?php echo $basepath ?>sosialisasi_bimtek_bahan_sosialisasi/cetak_detail");
		           
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", "param");
            hiddenField.setAttribute("value", param);

            var hiddenField2 = document.createElement("input");
            hiddenField2.setAttribute("type", "hidden");
            hiddenField2.setAttribute("name", "tipe");
            hiddenField2.setAttribute("value", tipe);

            form.appendChild(hiddenField);
            form.appendChild(hiddenField2);

		    document.body.appendChild(form);
		    form.submit();
		}
	</script>

	<!-- Tabel Bahan Sosialisasi -->
	<div class="cnt-header row" style="margin-top: 30px; margin-bottom: 20px;">
		<h6 class="table-label" style="display:block; width:100%; clear: both;"><i class="fa fa-portrait"></i> Bahan Sosialisasi</h6>
		<!--<span style="float:left"><?php echo $my_data_upt['nama_upt'] ?>
        		<?php echo (!empty($my_data_upt['alamat']) ? '<br/>'.$my_data_upt['alamat'] : '')  ?>
        </span>-->

		<div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">

		 	<?php  if($akses_add_tab_bhn_so="1"){  ?>
				<a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Tambah"  onclick="call_modal_from_map('Tambah Bahan Sosialisasi','Form_Bahan_Sosialisasi','<?php echo $map_id ?>','<?php echo $upt ?>','do_add','sosialisasi_bimtek/form')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
			<?php } ?>

			<a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Cetak" onclick="print_laporan_bhn_detail('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

			<?php  if($akses_add_tab_bhn_so="1"){  ?>
				<a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Import" data-target="#Import_BhnSosialisasi_Detail"><i class="fa fa-upload"></i></a>
			<?php } ?>

			<a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom"  title="Filterisasi" data-target="#Filter_BhnSosialisasiDetail"  ><i class="fa fa-filter"></i></a>

			<div class="btn-group" role="group" data-tooltip="tooltip" data-placement="bottom" title="Export">
				<button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fa fa-file-pdf"></i>
				</button>
				<div class="dropdown-menu" aria-labelledby="red">
					<a class="dropdown-item"  onclick="print_laporan_bhn_detail('pdf')" href="javascript:void(0)">Export to PDF</a>
					<a class="dropdown-item"   onclick="print_laporan_bhn_detail('xls')" href="javascript:void(0)">Export to XLS</a>
					<a class="dropdown-item" onclick="print_laporan_bhn_detail('doc')"   href="javascript:void(0)">Export to DOCS</a>
				</div>
			</div>
		</div>
		<hr style="width:100%; display: block; float: left;">
	</div>


	<div class="table table-responsive pelayanan-top" id="filter_bhn_so_detail">
		<table id="tb_bahan_sosialisasi" class="display table table-striped pelayanan-table" style="width: 100%;">
			<thead>
				<tr>
					<th class="text-center">Tanggal</th>
                    <th class="text-center">Provinsi</th>
                    <th class="text-center">UPT</th>
                    <th class="text-center">Materi</th>
                    <th class="text-center">Judul</th>
                    <th class="text-center">Author</th>
                    <th class="text-center">Lampiran</th>
                    <th class="text-center">Deskripsi</th>
                    <th class="text-center">Keterangan</th>
                    <th class="text-center">Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	<script type="text/javascript">

	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});
		var dTable1; 
		$(document).ready(function() {
			dTable1 = $('#tb_bahan_sosialisasi').DataTable( {
				"bProcessing": true,
				"bServerSide": true,
				"bJQueryUI": false,
				"searching": false,
				"searching": false,
				"responsive": false,
				"autoWidth": false,
				"sAjaxSource": "<?php echo $basepath ?>sosialisasi_bimtek_bahan_sosialisasi/rest_provinsi&id_prov=<?php echo $wilayah['id_prov'] ?>&upt=<?php echo $upt ?>", 
				"sServerMethod": "POST",
				"scrollX": true,
				"scrollY": "350px",
					"scrollCollapse": true,
					"order": [[ 0, "desc" ]],
				"columnDefs": [
			 		  { "orderable": true, "targets": 0, "searchable": true},
	                  { "orderable": true, "targets": 1, "searchable": true},
	                  { "orderable": true, "targets": 2, "searchable": true},
	                  { "orderable": true, "targets": 3, "searchable": true},
	                  { "orderable": true, "targets": 4, "searchable": true },
	                  { "orderable": true, "targets": 5, "searchable": true },
	                  { "orderable": true, "targets": 6, "searchable": true },
	                  { "orderable": true, "targets": 7, "searchable": true },
	                  { "orderable": true, "targets": 8, "searchable": true },
	                  { "orderable": false, "targets": 9, "searchable": false, "width":200}
				]
			} );
		});

		var chart_detail_bhn_so_1 = [
	  <?php 
	  		$bhn_so_detail_no_1 = 0;	
	        $chart_detail_1="";
	        $sql = "select count(*) as total,mtr.jenis_materi
                   from tr_bahan_sosialisasi as bhn 
                   inner join ms_materi as mtr 
                      on mtr.kode_materi=bhn.materi
                   where bhn.status='3' and bhn.id_prov='".$wilayah['id_prov']."' and bhn.kode_upt='".$upt."'  GROUP BY mtr.jenis_materi";
	        $get_chart_tujuan  = $db->Execute($sql);
	        while($list = $get_chart_tujuan->FetchRow()){
	          foreach($list as $key=>$val){
	                            $key=strtolower($key);
	                            $$key=$val;
	          }  
	          $chart_detail_1 .='{"name":"'.$jenis_materi.'","y":'.$total.'},';
	          $bhn_so_detail_no_1++;
	        } 
	        echo trim($chart_detail_1,',');
	  ?>
	];
	pie_chart('chart_detail_bhn_so_per_materi',chart_detail_bhn_so_1);

	<?php 
        if($bhn_so_detail_no_1==0){
          echo " $('#div_chart_detail_bhn_so_1').hide(); ";
        }
     ?>
     $('[data-tooltip="tooltip"]').tooltip();
	</script>
	</div>