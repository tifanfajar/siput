<table id="tb_loket" class="display table table-striped pelayanan-table" style="width: 100%;">
		<thead>
			<tr>
				<th class="text-center">Tanggal</th>
                <th class="text-center">UPT</th>
                <th class="text-center">Nama</th>
                <th class="text-center">Jabatan</th>
                <th class="text-center">Perusahaan</th>
                <th class="text-center">No.Tlp</th>
                <th class="text-center">No.Hp</th>
                <th class="text-center">Email</th>
                <th class="text-center">Tujuan</th>
                <th class="text-center">Perizinan</th>
                <th class="text-center">Keterangan</th>
                <th class="text-center">Aksi</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
</table>
<script type="text/javascript">

	var dTableFilter; 
	$(document).ready(function() {
		dTableFilter = $('#tb_loket').DataTable( {
			"bProcessing": true,
			"bServerSide": true,
			"bJQueryUI": false,
			"responsive": false,
			"searching": false,
			"autoWidth": false,
			"sAjaxSource": "<?php echo $basepath ?>pelayanan_loket/rest_filter&param=<?php echo $param ?>", 
			"sServerMethod": "POST",
			"scrollX": true,
			"scrollY": "350px",
				"scrollCollapse": true,
			"columnDefs": [
			{ "orderable": true, "targets": 0, "searchable": true},
			{ "orderable": true, "targets": 1, "searchable": true},
			{ "orderable": true, "targets": 2, "searchable": true},
			{ "orderable": true, "targets": 3, "searchable": true},
			{ "orderable": true, "targets": 4, "searchable": true },
			{ "orderable": true, "targets": 5, "searchable": true },
			{ "orderable": true, "targets": 6, "searchable": true },
			{ "orderable": true, "targets": 7, "searchable": true },
			{ "orderable": true, "targets": 8, "searchable": true },
			{ "orderable": true, "targets": 9, "searchable": true },
			{ "orderable": true, "targets": 10, "searchable": true },
			{ "orderable": false, "targets": 11, "searchable": false, "width":170}
			]
		} );
	});
</script>
