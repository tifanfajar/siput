<table id="tb_unar" class="display table table-striped pelayanan-table" style="width: 100%;">
		<thead>
				<tr>
                    <th class="text-center" rowspan="2">Created Date</th>
                    <th class="text-center" rowspan="2">Provinsi</th>
                    <th class="text-center" rowspan="2">UPT</th>
                    <th class="text-center" rowspan="2">Tanggal Pelaksanaan Ujian</th>
                    <th class="text-center" rowspan="2">Lokasi Ujian</th>
                    <th class="text-center" rowspan="2">Kab/Kota</th>
                    <th class="text-center" colspan="3">Jumlah Peserta</th>
                    <th class="text-center" colspan="3">Lulus</th>
                    <th class="text-center" colspan="3">Tidak Lulus</th>
                    <th class="text-center" rowspan="2">Ket</th>
                    <th class="text-center" rowspan="2">Aksi</th>
                  </tr>
                  <tr>
                    <th  class="text-center">YD</th>
                    <th  class="text-center">YC</th>
                    <th  class="text-center">YB</th>

                    <th  class="text-center">YD</th>
                    <th  class="text-center">YC</th>
                    <th  class="text-center">YB</th>

                    <th  class="text-center">YD</th>
                    <th  class="text-center">YC</th>
                    <th  class="text-center">YB</th>
                  </tr>
			</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	var dTable1; 
	$(document).ready(function() {
		dTable1 = $('#tb_unar').DataTable( {
			"bProcessing": true,
			"bServerSide": true,
			"bJQueryUI": false,
			"searching": false,
			"responsive": false,
			"autoWidth": false,
			"sAjaxSource": "<?php echo $basepath ?>pelayanan_unar/rest_filter&param=<?php echo $param ?>",
			"sServerMethod": "POST",
			"scrollX": true,
			"scrollY": "350px",
			"scrollCollapse": true,
			"columnDefs": [
			   	  { "orderable": true, "targets": 0, "searchable": true},
		          { "orderable": true, "targets": 1, "searchable": true},
		          { "orderable": true, "targets": 2, "searchable": true},
		          { "orderable": true, "targets": 3, "searchable": true},
		          { "orderable": true, "targets": 4, "searchable": true },
		          { "orderable": true, "targets": 5, "searchable": true },
		          { "orderable": true, "targets": 6, "searchable": true },
		          { "orderable": true, "targets": 7, "searchable": true },
		          { "orderable": true, "targets": 8, "searchable": true },
		          { "orderable": true, "targets": 9, "searchable": true },
		          { "orderable": true, "targets": 10, "searchable": true },
		          { "orderable": true, "targets": 11, "searchable": true },
		          { "orderable": true, "targets": 12, "searchable": true },
		          { "orderable": true, "targets": 13, "searchable": true },
		          { "orderable": true, "targets": 14, "searchable": true },
		          { "orderable": true, "targets": 15, "searchable": true },
		          { "orderable": false, "targets": 16, "searchable": false, "width":200}
			]
		} );
	});
</script>
