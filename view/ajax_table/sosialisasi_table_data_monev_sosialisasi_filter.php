<table id="tb_monev_sosialisasi" class="display table table-striped pelayanan-table datatable">
                <thead>
                  <tr>
                    <th class="text-center">Tanggal Buat</th>
                      <th class="text-center">Provinsi</th>
                      <th class="text-center">UPT</th>
                      <th class="text-center">Jenis Kegiatan</th>
                      <th class="text-center">Tanggal Pelaksanaan</th>
                      <th class="text-center">Tempat</th>
                      <th class="text-center">Tema</th>
                      <th class="text-center">Target Peserta</th>
                      <th class="text-center">Jumlah Peserta</th>
                      <th class="text-center">Anggaran</th>
                      <th class="text-center">Narasumber</th>
                      <th class="text-center">Lampiran</th>
                      <th class="text-center">Keterangan</th>
                      <th class="text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
           

            <script type="text/javascript">
              var dTable1;
              $(document).ready(function() {
                dTable1 = $('#tb_monev_sosialisasi').DataTable( {
                  "bProcessing": true,
                  "searching": false,
                  "bServerSide": true,
                  "bJQueryUI": false,
                  "retrieve": true,
                  "responsive": false,
                  "autoWidth": false,
                  "sAjaxSource": "<?php echo $basepath ?>sosialisasi_bimtek_monev_sosialisasi/rest_provinsi_filter&param=<?php echo $param ?>",
                  "sServerMethod": "POST",
                  "scrollX": true,
                  "scrollY": "350px",
                    "scrollCollapse": true,
                  "columnDefs": [
                   { "orderable": true, "targets": 0, "searchable": true},
                    { "orderable": true, "targets": 1, "searchable": true},
                    { "orderable": true, "targets": 2, "searchable": true},
                    { "orderable": true, "targets": 3, "searchable": true},
                    { "orderable": true, "targets": 4, "searchable": true },
                    { "orderable": true, "targets": 5, "searchable": true },
                    { "orderable": true, "targets": 6, "searchable": true },
                    { "orderable": true, "targets": 7, "searchable": true },
                    { "orderable": true, "targets": 8, "searchable": true },
                    { "orderable": true, "targets": 9, "searchable": true },
                    { "orderable": true, "targets": 10, "searchable": true },
                    { "orderable": true, "targets": 11, "searchable": true },
                    { "orderable": true, "targets": 12, "searchable": true },
                    { "orderable": false, "targets": 13, "searchable": false, "width":200}
                  ]
                } );
              });
            </script>