<table id="tb_user" class="display table table-striped pelayanan-table">
  <thead>
    <tr>
      <th>Username</th>
      <th>Nama</th>
      <th>Level</th>
      <th>Status</th>
      <th>Provinsi</th>
      <th>UPT</th>
      <th>No.Tlp</th>
      <th>No.Hp</th>
      <th>Email</th>
      <th>Tanggal Buat</th>
      <th>Tanggal Ubah</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody></tbody>
</table>
<script type="text/javascript">
  var dTable;
  $(document).ready(function() {
    dTable = $('#tb_user').DataTable( {
      "bProcessing": true,
      "bServerSide": true,
      "bJQueryUI": false,
      "responsive": false,
      "autoWidth": false,
      "sAjaxSource": "<?php echo $basepath ?>daftar_pengguna/rest_filter&param=<?php echo $param ?>", 
      "sServerMethod": "POST",
      "scrollX": true,
      "searching": false,
      "scrollY": "350px",
        "scrollCollapse": true,
      "columnDefs": [
      { "orderable": true, "targets": 0, "searchable": true},
      { "orderable": true, "targets": 1, "searchable": true},
      { "orderable": true, "targets": 2, "searchable": true},
      { "orderable": true, "targets": 3, "searchable": true},
      { "orderable": true, "targets": 4, "searchable": true},
      { "orderable": true, "targets": 5, "searchable": true},
      { "orderable": true, "targets": 6, "searchable": true},
      { "orderable": true, "targets": 7, "searchable": true},
      { "orderable": true, "targets": 8, "searchable": true},
      { "orderable": true, "targets": 9, "searchable": true},
      { "orderable": true, "targets": 10, "searchable": true},
      { "orderable": false, "targets": 11, "searchable": false, "width":170}
      ]
    } );
  });
</script>