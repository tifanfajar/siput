<table id="tb_company" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
                    <th>No Klien Licence</th>
                    <th>Nama Perusahaan</th>
                    <th>No Telp</th>
                    <th>No Hp</th>
                    <th>Provinsi</th>
                    <th>Kota/Kabupaten</th>
                    <th>Alamat</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
               <script type="text/javascript">
              var dTable1;
              $(document).ready(function() {
                dTable1 = $('#tb_company').DataTable( {
                  "bProcessing": true,
                  "searching": false,
                  "bServerSide": true,
                  "bJQueryUI": false,
                  "retrieve": true,
                  "responsive": false,
                  "autoWidth": false,
                  "sAjaxSource": "<?php echo $basepath ?>company/rest_filter&param=<?php echo $param ?>", 
                  "sServerMethod": "POST",
                  "scrollX": true,
                  "scrollY": "350px",
                    "scrollCollapse": true,
                  "columnDefs": [
                  { "orderable": true, "targets": 0, "searchable": true},
                    { "orderable": true, "targets": 1, "searchable": true},
                    { "orderable": true, "targets": 2, "searchable": true},
                    { "orderable": true, "targets": 3, "searchable": true},
                    { "orderable": true, "targets": 4, "searchable": true },
                    { "orderable": true, "targets": 5, "searchable": true },
                    { "orderable": true, "targets": 6, "searchable": true },
                    { "orderable": false, "targets": 7, "searchable": false, "width":170}
                  ]
                } );
              });
            </script>