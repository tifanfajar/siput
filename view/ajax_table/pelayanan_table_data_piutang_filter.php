<table id="tb_piutang" class="display table table-striped pelayanan-table datatable">
                <thead>
                  <tr>
                    <th class="text-center" rowspan="2">Tanggal Buat</th>
                    <th class="text-center" rowspan="2">Provinsi</th>
                    <th class="text-center" rowspan="2">UPT</th>
                    <th class="text-center" rowspan="2">Wajib Bayar</th>
                    <th class="text-center" rowspan="2">No. Client</th>
                    <th class="text-center" rowspan="2">Nilai Penyerahan</th>
                    <th class="text-center" rowspan="2">Tahun Pelimpahan</th>
                    <th class="text-center" rowspan="2">Nama KPKNL</th>
                    <th class="text-center" rowspan="2">Tahapan Pengurusan</th>
                    <th class="text-center" colspan="3">Pemindah Bukuan</th>
                    <th class="text-center" rowspan="2">PSBDT</th>
                    <th class="text-center" rowspan="2">Pembatalan</th>
                    <th class="text-center" rowspan="2">Sisa Piutang</th>
                    <th class="text-center" rowspan="2">Ket</th>
                    <th class="text-center" rowspan="2">Aksi</th>
                  </tr>
                  <tr>
                    <th class="text-center">Lunas</th>
                    <th class="text-center">Angsuran</th>
                    <th class="text-center">Tanggal</th>
                  </tr>
                  <tr>
                    <th class="text-center">1</th>
                    <th class="text-center">2</th>
                    <th class="text-center">3</th>
                    <th class="text-center">4</th>
                    <th class="text-center">5</th>
                    <th class="text-center">6</th>
                    <th class="text-center">7</th>
                    <th class="text-center">8</th>
                    <th class="text-center">9</th>
                    <th class="text-center">10</th>
                    <th class="text-center">11</th>
                    <th class="text-center">12</th>
                    <th class="text-center">13</th>
                    <th class="text-center">14</th>
                    <th class="text-center">15</th>
                    <th class="text-center">16</th>
                    <th class="text-center">17</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
           

            <script type="text/javascript">
              var dTable1;
              $(document).ready(function() {
                dTable1 = $('#tb_piutang').DataTable( {
                  "bProcessing": true,
                  "searching": false,
                  "bServerSide": true,
                  "bJQueryUI": false,
                  "retrieve": true,
                  "responsive": false,
                  "autoWidth": false,
                  "sAjaxSource": "<?php echo $basepath ?>pelayanan_piutang/rest_provinsi_filter&param=<?php echo $param ?>",
                  "sServerMethod": "POST",
                  "scrollX": true,
                  "scrollY": "350px",
                    "scrollCollapse": true,
                  "columnDefs": [
                    {"orderable": true, "targets": 0, "searchable": true},
                    { "orderable": true, "targets": 1, "searchable": true},
                    { "orderable": true, "targets": 2, "searchable": true},
                    { "orderable": true, "targets": 3, "searchable": true},
                    { "orderable": true, "targets": 4, "searchable": true },
                    { "orderable": true, "targets": 5, "searchable": true },
                    { "orderable": true, "targets": 6, "searchable": true },
                    { "orderable": true, "targets": 7, "searchable": true },
                    { "orderable": true, "targets": 8, "searchable": true },
                    { "orderable": true, "targets": 9, "searchable": true },
                    { "orderable": true, "targets": 10, "searchable": true },
                    { "orderable": true, "targets": 11, "searchable": true },
                    { "orderable": true, "targets": 12, "searchable": true },
                    { "orderable": true, "targets": 13, "searchable": true },
                    { "orderable": true, "targets": 14, "searchable": true },
                    { "orderable": true, "targets": 15, "searchable": true },
                    { "orderable": false, "targets": 16, "searchable": false, "width":200}
                  ]
                } );
              });
            </script>