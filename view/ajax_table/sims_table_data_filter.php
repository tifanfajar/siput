<table id="tb_sims" class="display table table-striped pelayanan-table" style="width: 100%;">
		<thead>
			<tr>
				<th>No SPP</th>
                <th>No Klien Licence</th>
                <th>Perusahaan</th>
                <th>Request Reff</th>
                <th>Licence State</th>
                <th>Tagihan</th>
                <th>Status</th>
                <th>Tanggal Begin</th>
                <th>Status Izin</th>
                <th>Kategori SPP</th>
                <th>Service</th>
                <th>Sub Service</th>
                <th>Created Date</th>
                <th>Last Update</th>
                <th>Aksi</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
</table>
<script type="text/javascript">

	var dTableFilter; 
	$(document).ready(function() {
		dTableFilter = $('#tb_sims').DataTable( {
			"bProcessing": true,
			"bServerSide": true,
			"bJQueryUI": false,
			"responsive": false,
			"autoWidth": false,
			"sAjaxSource": "<?php echo $basepath ?>sims/rest_filter<?php echo base64_decode($param) ?>", 
			"sServerMethod": "POST",
			"scrollX": true,
			"scrollY": "350px",
				"scrollCollapse": true,
			"columnDefs": [
			  { "orderable": true, "targets": 0, "searchable": true},
		      { "orderable": true, "targets": 1, "searchable": true},
		      { "orderable": true, "targets": 2, "searchable": true},
		      { "orderable": true, "targets": 3, "searchable": true},
		      { "orderable": true, "targets": 4, "searchable": true},
		      { "orderable": true, "targets": 5, "searchable": true},
		      { "orderable": true, "targets": 6, "searchable": true},
		      { "orderable": true, "targets": 7, "searchable": true},
		      { "orderable": true, "targets": 8, "searchable": true},
		      { "orderable": true, "targets": 9, "searchable": true},
		      { "orderable": true, "targets": 10, "searchable": true},
		      { "orderable": true, "targets": 11, "searchable": true},
		      { "orderable": true, "targets": 12, "searchable": true},
		      { "orderable": true, "targets": 13, "searchable": true},
		      { "orderable": false, "targets": 14, "searchable": false, "width":170}
			]
		} );
	});
</script>
