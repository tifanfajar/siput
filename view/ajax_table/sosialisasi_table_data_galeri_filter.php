<table id="tb_galeri" class="display table table-striped pelayanan-table" style="width: 100%;">
	<thead>
		<tr>
			<th class="text-center">Tanggal</th>
			<th class="text-center">Provinsi</th>
			<th class="text-center">UPT</th>
            <th class="text-center">Kegiatan</th>
            <th class="text-center">Lampiran</th>
            <th class="text-center">Deskripsi</th>
            <th class="text-center">Keterangan</th>
            <th class="text-center">Aksi</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
<script type="text/javascript">

$(".tgl").datepicker({
 format: 'dd/mm/yyyy',
});
var dTable1; 
$(document).ready(function() {
	dTable1 = $('#tb_galeri').DataTable( {
		"bProcessing": true,
		"bServerSide": true,
		"bJQueryUI": false,
		"searching": false,
		"searching": false,
		"responsive": false,
		"autoWidth": false,
		"sAjaxSource": "<?php echo $basepath ?>sosialisasi_bimtek_galeri/rest_provinsi_filter&param=<?php echo $param ?>", 
		"sServerMethod": "POST",
		"scrollX": true,
		"scrollY": "350px",
			"scrollCollapse": true,
		"columnDefs": [
	 		  { "orderable": true, "targets": 0, "searchable": true},
              { "orderable": true, "targets": 1, "searchable": true},
              { "orderable": true, "targets": 2, "searchable": true},
              { "orderable": true, "targets": 3, "searchable": true},
              { "orderable": true, "targets": 4, "searchable": true},
              { "orderable": true, "targets": 5, "searchable": true},
              { "orderable": true, "targets": 6, "searchable": true},
              { "orderable": false, "targets": 7, "searchable": false, "width":200}
		]
	} );
});
</script>