<?php  
 $akses_add_tab_spp = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>2,'kode_function_access'=>$_SESSION['group_user'])); 
 $akses_add_tab_spp    = (!empty($akses_add_tab_spp) ? $akses_add_tab_spp : '0');
 $my_data_upt = $gen_model->GetOneRow("ms_upt",array('kode_upt'=>$upt)); 
?>
<style type="text/css">
	.table th {
	   text-align: center;
	   font-weight:bold;
	}
	.datatable>tbody>tr>td
	{
		white-space: nowrap;
	} 
</style>

<input type="hidden" name="filter_provinsi" id="filter_provinsi" value="<?php echo $wilayah['id_prov'] ?>" >
<input type="hidden" name="SPP_Pencarian" id="SPP_Pencarian" >
<label class="loc-lbl">UPT <?php echo $wilayah['nama_prov'] ?></label>
<div class="row">
		<div class="col-md-12">
			<?php 
				if($_SESSION['group_user']=="grp_171026194411_1142"){
			?>
		 <select class="form_control" onchange="get_detail_spp()" id="mydata_upt_spp" style="width: 100%;">
		    <?php 
		       $data_upt = $gen_model->GetWhere('ms_upt',array('id_prov'=>$wilayah['id_prov']));
		       echo '<option value="">Pilih UPT</option>';
		        while($list = $data_upt->FetchRow()){
		          foreach($list as $key=>$val){
		                      $key=strtolower($key);
		                      $$key=$val;
		                    }  
		      ?>
		    <option <?php echo ($kode_upt==$upt ? 'selected' : '') ?> value="<?php echo $kode_upt ?>"><?php echo $nama_upt ?></option>
		    <?php }  ?>
		 </select><br/>
		 	<?php } else { ?>
	        		<label style="margin-top: 0px;color:black;background-color:white" class="loc-lbl"><?php echo $my_data_upt['nama_upt'] ?></label>
		 	<?php } ?>
		</div>
	</div>
<script type="text/javascript">
 function get_detail_spp(){
    var  map_id = "<?php echo $map_id ?>";
    var  upt    = $("#mydata_upt_spp").val();
    if(upt){
         detail_spp(map_id,upt);
    }
 }
</script>	
<input type="hidden" value="<?php echo $map_id ?>" name="data_map_id_pengunjung" class="data_map_id_pengunjung">
<input type="hidden" value="<?php echo $map_id ?>" name="data_map_id_spp" class="data_map_id_spp">
<input type="hidden" value="<?php echo $upt ?>"    name="data_upt_spp" class="data_upt_spp">
<input type="hidden" name="filter_provinsi" id="filter_provinsi_spp" value="<?php echo $wilayah['id_prov'] ?>" >
<input type="hidden" name="filter_upt" id="filter_upt_spp" value="<?php echo $upt  ?>" >

<div class="row" id="chart_detail_spp_1">
	<div class="col-sm-12">
	  <div class="card-set">
	    <div class="headset"><i class="fa fa-chart-pie"></i> Grafik Jumlah Per Metode</div>
	    <div id="pl_spp_jumlah_per_metode"></div>
	  </div>
	</div><br/>
</div>
<?php 
	// $check_waiting = "SELECT  count(*) as total
	// 				FROM tr_spp as spp
	// 				left outer join tr_data_sims as sm
	// 					on sm.no_spp=spp.no_spp
	// 				INNER JOIN ms_company AS cp 
	// 					on cp.company_id = sm.kode_perusahaan
	// 				left outer join ms_user as us_crt
	// 					on us_crt.kode_user=spp.created_by
	// 				left outer join ms_user as us_updt
	// 					on us_updt.kode_user=spp.last_update_by
	// 				left outer join ms_provinsi as pv
	// 					on pv.id_prov=spp.id_prov
	// 				left outer join ms_upt as upt
	// 					on upt.kode_upt=spp.kode_upt
	// 				where 1=1  and spp.id_prov='".$wilayah['id_prov']."' and spp.kode_upt='".$upt."' and spp.status in ('0','1')";
	// $total_waiting = $db->GetOne($check_waiting);	
	// if($total_waiting!="0"){			
?>

<!-- Tabel Persetujuan-->
	<div class="cnt-header row" style="padding-right: 0px;">
		<h6 class="table-label">Menunggu Persetujuan</h6>
			<?php  if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT ?>
				<a style="cursor:pointer;color:white" onclick="verif_data('kode_spp','pelayanan_spp/verifikasi','no_refresh','refresh_table')" class="btn btn-success btn-sm pull-right"><i class="fa fa-check"></i> Verifikasi Data</a>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<a style="cursor:pointer;color:white" data-toggle="modal" title="Reject" data-target="#Reject_SPP"  class="btn btn-danger btn-sm pull-right"><i class="fa fa-times"></i> Reject Data</a>
			<?php } ?>
	</div>

	<script type="text/javascript">
		 $("#checkAll_SPP").click(function () {
		     $('input:checkbox').not(this).prop('checked', this.checked);
		 });
	</script>

	<div class="table table-responsive pelayanan-top" id="filter_pelayanan_detail">
		<table id="tb_spp_waiting" class="display table table-striped pelayanan-table">
			<thead>
				<tr>
					  <th class="text-center"><input type="checkbox" id="checkAll_SPP"></th>
					  <th class="text-center">Created Date</th>
					  <th class="text-center">Status</th>
					  <th class="text-center">No. Client</th>
	                  <th class="text-center">Nama Pemegang ISR / Waba</th>
	                  <th class="text-center">Service</th>
	                  <th class="text-center">No. Aplikasi</th>
	                  <th class="text-center">No. SPP</th>
	                  <th class="text-center">Jenis SPP</th>
	                  <th class="text-center">Nilai BHP (Rp)</th>
	                  <th class="text-center">Tanggal Jatuh Tempo</th>
	                  <th class="text-center">Tanggal Pengiriman</th>
	                  <th class="text-center">Metode</th>
	                  <th class="text-center">Keterangan</th>
	                  <th class="text-center">Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<script type="text/javascript">
		var dTable_spp_waiting;
					dTable_spp_waiting = $('#tb_spp_waiting').DataTable( {
						"bProcessing": true,
						"bServerSide": true,
						"bJQueryUI": false,
						"responsive": false,
						"retrieve": true,
						"autoWidth": false,
						"sAjaxSource": "<?php echo $basepath ?>pelayanan_spp/rest_waiting&id_prov=<?php echo $wilayah['id_prov'] ?>&upt=<?php echo $upt ?>", 
						"sServerMethod": "POST",
						"scrollX": true,
						"scrollY": "350px",
	  					"scrollCollapse": true,
	  					"order": [[ 1, "desc" ]],
						"columnDefs": [
						{ "orderable": false, "targets": 0, "searchable": false},
						{ "orderable": true, "targets": 1, "searchable": true},
						{ "orderable": true, "targets": 2, "searchable": true},
						{ "orderable": true, "targets": 3, "searchable": true},
						{ "orderable": true, "targets": 4, "searchable": true },
						{ "orderable": true, "targets": 5, "searchable": true },
						{ "orderable": true, "targets": 6, "searchable": true },
						{ "orderable": true, "targets": 7, "searchable": true },
						{ "orderable": true, "targets": 8, "searchable": true },
						{ "orderable": true, "targets": 9, "searchable": true },
						{ "orderable": true, "targets": 10, "searchable": true },
						{ "orderable": true, "targets": 11, "searchable": true },
						{ "orderable": true, "targets": 12, "searchable": true },
						{ "orderable": true, "targets": 13, "searchable": true },
						{ "orderable": false, "targets": 14, "searchable": false, "width":170}
						]
					} );
	</script>
	<?php // } ?>

	<script type="text/javascript">
		function print_laporan_spp_detail(tipe){
			var param = $("#SPP_Pencarian").val();

			if(!param){
				var prov = " and spp.id_prov ='<?php echo $wilayah['id_prov'] ?>' ";
				param += prov;
				
				var upt = " and spp.kode_upt='<?php echo $upt ?>' ";
				param += upt;

				param = btoa(param);
			}


		    var form = document.createElement("form");
		    form.setAttribute("method", "POST");
		    if(tipe=="print"){
		  	 form.setAttribute("target", "_blank");
		    }
		    form.setAttribute("action", "<?php echo $basepath ?>pelayanan_spp/cetak_detail");
		           
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", "param");
            hiddenField.setAttribute("value", param);

            var hiddenField2 = document.createElement("input");
            hiddenField2.setAttribute("type", "hidden");
            hiddenField2.setAttribute("name", "tipe");
            hiddenField2.setAttribute("value", tipe);

            form.appendChild(hiddenField);
            form.appendChild(hiddenField2);

		    document.body.appendChild(form);
		    form.submit();
		}
	</script>

	<!-- Tabel SPP Pengunjung -->
	<div class="cnt-header row" style="margin-top: 30px; margin-bottom: 20px;">
		<h6 class="table-label" style="display:block; width:100%; clear: both;"><i class="fa fa-portrait"></i> Data SPP</h6>
		<!--<span style="float:left"><?php echo $my_data_upt['nama_upt'] ?>
        		<?php echo (!empty($my_data_upt['alamat']) ? '<br/>'.$my_data_upt['alamat'] : '')  ?>
        </span>-->
		<div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">

			<?php  if($akses_add_tab_spp=="1"){  ?>
				<a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Tambah"  onclick="call_modal_from_map('Tambah SPP','Form_SPP','<?php echo $map_id ?>','<?php echo $upt ?>','do_add','pelayanan/form')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
			<?php } ?>

			<a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Cetak" onclick="print_laporan_spp_detail('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

			<?php  if($akses_add_tab_spp=="1"){  ?>
				<a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Import" data-target="#Import_SPPDetail"><i class="fa fa-upload"></i></a>
			<?php } ?>

			<a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Filterisasi" data-target="#Filter_SPPDetail"  ><i class="fa fa-filter"></i></a>

			<div class="btn-group" role="group" data-tooltip="tooltip" data-placement="bottom" title="Export">
				<button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fa fa-file-pdf"></i>
				</button>
				<div class="dropdown-menu" aria-labelledby="red">
					<a class="dropdown-item"  onclick="print_laporan_spp_detail('pdf')" href="javascript:void(0)">Export to PDF</a>
					<a class="dropdown-item"   onclick="print_laporan_spp_detail('xls')" href="javascript:void(0)">Export to XLS</a>
					<a class="dropdown-item" onclick="print_laporan_spp_detail('doc')"   href="javascript:void(0)">Export to DOCS</a>
				</div>
			</div>
		</div>
		<hr style="width:100%; display: block; float: left;">
	</div>



	<div class="table table-responsive pelayanan-top" id="filter_spp_detail">
		<table id="tb_spp" class="display table table-striped pelayanan-table" style="width: 100%;">
			<thead>
				<tr>
					  <th class="text-center">No. Client</th>
	                  <th class="text-center">Nama Pemegang ISR / Waba</th>
	                  <th class="text-center">Service</th>
	                  <th class="text-center">No. Aplikasi</th>
	                  <th class="text-center">No. SPP</th>
	                  <th class="text-center">Jenis SPP</th>
	                  <th class="text-center">Nilai BHP (Rp)</th>
	                  <th class="text-center">Tanggal Jatuh Tempo</th>
	                  <th class="text-center">Tanggal Pengiriman</th>
	                  <th class="text-center">Metode</th>
	                  <th class="text-center">Keterangan</th>
	                  <th class="text-center">Aksi</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<script type="text/javascript">
	$('[data-tooltip="tooltip"]').tooltip();	
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});
		var dTable1_spp; 
			dTable1_spp = $('#tb_spp').DataTable( {
				"bProcessing": true,
				"bServerSide": true,
				"bJQueryUI": false,
				"searching": false,
				"autoWidth": false,
				"retrieve": true,
				"sAjaxSource": "<?php echo $basepath ?>pelayanan_spp/rest_provinsi&id_prov=<?php echo $wilayah['id_prov'] ?>&upt=<?php echo $upt ?>", 
				"sServerMethod": "POST",
				"scrollX": true,
				"scrollY": "350px",
				"scrollCollapse": true,
                "order": [[ 0, "desc" ]],
				"columnDefs": [
					{ "orderable": true, "targets": 0, "searchable": true},
					{ "orderable": true, "targets": 1, "searchable": true},
					{ "orderable": true, "targets": 2, "searchable": true},
					{ "orderable": true, "targets": 3, "searchable": true},
					{ "orderable": true, "targets": 4, "searchable": true },
					{ "orderable": true, "targets": 5, "searchable": true },
					{ "orderable": true, "targets": 6, "searchable": true },
					{ "orderable": true, "targets": 7, "searchable": true },
					{ "orderable": true, "targets": 8, "searchable": true },
					{ "orderable": true, "targets": 9, "searchable": true },
					{ "orderable": true, "targets": 10, "searchable": true },
					{ "orderable": false, "targets": 11, "searchable": false, "width":170}
				]
			} );
	</script>

	<!--<div class="cnt-heaeder" style="margin-top: 30px; padding-bottom: 36px;">
        <h6 class="table-label">Resume UPT : <?php echo $wilayah['nama_prov'] ?></h6>
    </div>
    <div class="table table-responsive pelayanan-top">
        <table id="tableDataDetail" class="table table-striped pelayanan-table" style="width: 100%;">
          <thead>
            <tr>
              <th rowspan="2" scope="col">Perizinan</th>
              <th rowspan="2" scope="col">Jumlah Pengunjung</th>
              <th colspan="3" scope="col" style="text-align: center;">Tujuan</th>
            </tr>
            <tr>
              <th scope="col">Pengaduan Gangguan</th>
              <th scope="col">Konsultasi</th>
              <th scope="col">Lain-Lain</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
        <hr>
    </div>-->
    <script type="text/javascript">
    		// var dTable3;
      //         $(document).ready(function() {
      //           dTable3 = $('#tableDataDetail').DataTable( {
      //             "bProcessing": true,
      //             "bServerSide": true,
      //             "bJQueryUI": false,
      //             "responsive": false,
      //             "autoWidth": false,
      //             "sAjaxSource": "<?php echo $basepath ?>pelayanan/pl_akumulasi_detail&id_prov=<?php echo $wilayah['id_prov'] ?>", 
      //             "sServerMethod": "POST",
      //             "scrollX": true,
      //             "scrollY": "350px",
      //               "scrollCollapse": true,
      //             "columnDefs": [
      //             { "orderable": true, "targets": 0, "searchable": true},
      //             { "orderable": false, "targets": 1, "searchable": false},
      //             { "orderable": false, "targets": 2, "searchable": false},
      //             { "orderable": false, "targets": 3, "searchable": false},
      //             { "orderable": false, "targets": 4, "searchable": false }
      //             ]
      //           } );
      //         });
    </script>

<script type="text/javascript">
	var chart_detail_spp_1 = [
	  <?php 
	  		$spp_no_1=0;
	        $chart_detail_1="";
	        $sql = "select count(*) as total,mtd.metode
                   from tr_spp as spp 
                   inner join ms_metode as mtd 
                      on mtd.id_metode=spp.metode
                   where spp.status='3' and spp.id_prov='".$wilayah['id_prov']."' and spp.kode_upt='".$upt."'  GROUP BY mtd.metode";
	        $get_chart_tujuan  = $db->Execute($sql);
	        while($list = $get_chart_tujuan->FetchRow()){
	          foreach($list as $key=>$val){
	                            $key=strtolower($key);
	                            $$key=$val;
	          }  
	          $chart_detail_1 .='{"name":"'.$metode.'","y":'.$total.'},';
	          $spp_no_1++;
	        } 
	        echo trim($chart_detail_1,',');
	  ?>
	];
	pie_chart('pl_spp_jumlah_per_metode',chart_detail_spp_1);
	<?php 
        if($spp_no_1==0){
          echo " $('#chart_detail_spp_1').hide(); ";
        }
     ?>
</script>