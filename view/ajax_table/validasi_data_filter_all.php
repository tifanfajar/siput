<table id="tb_validasi_data" class="display table table-striped pelayanan-table datatable">
                <thead>
                  <tr>
                    <th class="text-center" rowspan="2">Tanggal Buat</th>
                    <th class="text-center" rowspan="2">Provinsi</th>
                    <th class="text-center" rowspan="2">UPT</th>
                    <th class="text-center" rowspan="2">DATA SIMS</th>
                    <th class="text-center" rowspan="2">Data Sampling</th>
                    <th class="text-center" colspan="3">Data Hasil Inspeksi</th>
                    <th class="text-center" colspan="3">Tindak Lanjut Hasil Inspeksi</th>
                    <th class="text-center" rowspan="2">Capaian (% Valid)</th>
                    <th class="text-center" rowspan="2">Lampiran</th>
                    <th class="text-center" rowspan="2">Keterangan</th>
                    <th class="text-center" rowspan="2">Aksi</th>
                  </tr>
                  <tr>
                    <th  class="text-center">Sesuai ISR</th>
                    <th  class="text-center">Tidak Sesuai</th>
                    <th  class="text-center">Tidak Aktif</th> 
                    <th  class="text-center">Proses ISR</th>
                    <th  class="text-center">Sudah di tindaklanjuti</th>
                    <th  class="text-center">Belum di tindaklanjuti</th>
                  </tr> 
                  <tr>
                    <th  class="text-center">1</th>
                    <th  class="text-center">2</th>
                    <th  class="text-center">3</th> 
                    <th  class="text-center">4</th>
                    <th  class="text-center">5</th>
                    <th  class="text-center">6</th>
                    <th  class="text-center">7</th>
                    <th  class="text-center">8</th>
                    <th  class="text-center">9</th>
                    <th  class="text-center">10</th>
                    <th  class="text-center">11</th>
                    <th  class="text-center">12</th>
                    <th  class="text-center">13</th>
                    <th  class="text-center">14</th>
                    <th  class="text-center">15</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
               <script type="text/javascript">
              var dTable1;
              $(document).ready(function() {
                dTable1 = $('#tb_validasi_data').DataTable( {
                  "bProcessing": true,
                  "searching": false,
                  "bServerSide": true,
                  "bJQueryUI": false,
                  "retrieve": true,
                  "responsive": false,
                  "autoWidth": false,
                  "sAjaxSource": "<?php echo $basepath ?>validasi_data/rest_filter&param=<?php echo $param ?>", 
                  "sServerMethod": "POST",
                  "scrollX": true,
                  "scrollY": "350px",
                    "scrollCollapse": true,
                  "columnDefs": [
                  { "orderable": true, "targets": 0, "searchable": true},
                  { "orderable": true, "targets": 1, "searchable": true},
                  { "orderable": true, "targets": 2, "searchable": true},
                  { "orderable": true, "targets": 3, "searchable": true},
                  { "orderable": true, "targets": 4, "searchable": true },
                  { "orderable": true, "targets": 5, "searchable": true },
                  { "orderable": true, "targets": 6, "searchable": true },
                  { "orderable": true, "targets": 7, "searchable": true },
                  { "orderable": true, "targets": 8, "searchable": true },
                  { "orderable": true, "targets": 9, "searchable": true },
                  { "orderable": true, "targets": 10, "searchable": true },
                  { "orderable": true, "targets": 11, "searchable": true },
                  { "orderable": false, "targets": 12, "searchable": false },
                  { "orderable": true, "targets": 13, "searchable": true },
                  { "orderable": false, "targets": 14, "searchable": false, "width":200}
                  ]
                } );
              });
            </script>