<table id="tb_revoke" class="display table table-striped pelayanan-table" style="width: 100%;">
		<thead>
			<tr>
				<th class="text-center">Provinsi</th>
                <th class="text-center">UPT</th>
                <th class="text-center">No. Client</th>
                <th class="text-center">Nama Pemegang ISR / Waba</th>
                <th class="text-center">Service</th>
                <th class="text-center">No. Aplikasi</th>
                <th class="text-center">Tanggal ISR Dicabut</th>
                <th class="text-center">Tanggal Pengiriman</th>
                <th class="text-center">Metode</th>
                <th class="text-center">Keterangan</th>
                <th class="text-center">Aksi</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	var dTable1; 
	$(document).ready(function() {
		dTable1 = $('#tb_revoke').DataTable( {
			"bProcessing": true,
			"bServerSide": true,
			"bJQueryUI": false,
			"searching": false,
			"responsive": false,
			"autoWidth": false,
			"sAjaxSource": "<?php echo $basepath ?>pelayanan_revoke/rest_filter&param=<?php echo $param ?>",
			"sServerMethod": "POST",
			"scrollX": true,
			"scrollY": "350px",
			"scrollCollapse": true,
			"columnDefs": [
			  { "orderable": true, "targets": 0, "searchable": true},
	          { "orderable": true, "targets": 1, "searchable": true},
	          { "orderable": true, "targets": 2, "searchable": true},
	          { "orderable": true, "targets": 3, "searchable": true},
	          { "orderable": true, "targets": 4, "searchable": true },
	          { "orderable": true, "targets": 5, "searchable": true },
	          { "orderable": true, "targets": 6, "searchable": true },
	          { "orderable": true, "targets": 7, "searchable": true },
	          { "orderable": true, "targets": 8, "searchable": true },
	          { "orderable": true, "targets": 9, "searchable": true },
	          { "orderable": false, "targets": 10, "searchable": false, "width":200}
			]
		} );
	});
</script>
