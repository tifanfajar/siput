<title>Perusahaan - <?php echo $web['judul']?></title>


<style type="text/css">
  #example_paginate{position: relative; left: 0%; top: 5px;}
  .pagination li a{margin:5px 15px; }
  .dataTables_info{margin-bottom: 15px;}
  .datatable>tbody>tr>td
  {
    white-space: nowrap;
  } 
  .table th {
     text-align: center;
     font-weight:bold;
  }
</style>
<body class="pelayanan-cnt">


  <?php  include "view/top_menu.php";  ?>
  
  <!-- Main Container -->
  <div class="container-fluid animated fadeInUp" style="z-index: 3;">
    <div class="row">
  <?php  
    include "view/menu.php";  
   ?>
  <!-- Content -->
    <div class="container-fluid main-content" >
      <div class="row">


        <ul class="nav nav-tabs main-nav-tab" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="materi-tab"  onclick="refresh_table()" data-toggle="materi" href="#materi" role="tab" aria-controls="materi" aria-selected="false">Perusahaan</a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent" style="padding-top:0px">


             <!-- Tab Perusahaan -->
           <div class="tab-pane fade show active" id="materi" role="tabpanel" aria-labelledby="materi-tab">

            <div id="filter_upt" class="table table-responsive pelayanan-top">

               <table id="example" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
                    <th>No Klien Licence</th>
                    <th>Nama Perusahaan</th>
                    <th>No Telp</th>
                    <th>No Hp</th>
                    <th>Provinsi</th>
                    <th>Kota/Kabupaten</th>
                    <th>Alamat</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            <script type="text/javascript">
              var dTable;
                $(document).ready(function() {
                  dTable = $('#example').DataTable( {
                    "bProcessing": true,
                    "bServerSide": true,
                    "bJQueryUI": false,
                    "responsive": false,
                    "autoWidth": false,
                    "sAjaxSource": "<?php echo $basepath ?>company/rest", 
                    "sServerMethod": "POST",
                    "scrollX": true,
                    "scrollY": "350px",
                      "scrollCollapse": true,
                    "columnDefs": [
                    { "orderable": true, "targets": 0, "searchable": true},
                    { "orderable": true, "targets": 1, "searchable": true},
                    { "orderable": true, "targets": 2, "searchable": true},
                    { "orderable": true, "targets": 3, "searchable": true},
                    { "orderable": true, "targets": 4, "searchable": true },
                    { "orderable": true, "targets": 5, "searchable": true },
                    { "orderable": true, "targets": 6, "searchable": true }
                    ]
                  } );
                });
            </script>
            </div>  

          </div>



        </div>
      </div>
    </div>
    </div>
  </div>