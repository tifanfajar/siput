<!-- Javascript & Jquery -->
<script src="<?php echo $basepath ?>assets/js/popper.min.js"></script>
<script src="<?php echo $basepath ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo $basepath ?>assets/js/all.js"></script>


<!-- Highchart -->
<script src="<?php echo $basepath ?>assets/vendors/highchart/highstock.js"></script>
<script src="<?php echo $basepath ?>assets/vendors/highchart/highcharts-more.js"></script>

<!-- Highchart Map -->
<script src="<?php echo $basepath ?>assets/vendors/highchart_map/js/map.js"></script>
<script src="<?php echo $basepath ?>assets/vendors/highchart_map/js/data.js"></script>
<script src="<?php echo $basepath ?>assets/vendors/highchart_map/js/exporting.js"></script>
<script src="<?php echo $basepath ?>assets/vendors/highchart_map/js/id-all.js"></script>


<script type="text/javascript">
$(".tgl").datepicker({
   format: 'dd/mm/yyyy',
});
function val_checkbox(checkboxName) {
    var checkboxes = document.querySelectorAll('input[name="' + checkboxName + '"]:checked'), values = [];
    Array.prototype.forEach.call(checkboxes, function(el) {
        values.push("'"+el.value+"'");
    });
    return values;
}
function count_checkbox(checkboxName) {
    var checkboxes = document.querySelectorAll('input[name="' + checkboxName + '"]:checked'), values = [];
    var ct=0;
    Array.prototype.forEach.call(checkboxes, function(el) {
        values.push(el.value);
        ct++;
    });
    return ct;
}
function call_modal_from_map(title,form_url,map_id,upt,activity,controller){
    $("#call_modal").modal({backdrop:'static',keyboard: false});
    $.ajax({
        url: '<?php echo $basepath ?>'+controller,
        data: 'form_url='+form_url+'&map_id='+map_id+'&upt='+upt+'&activity='+activity,
        type: 'POST',
        dataType: 'html',
        success: function(result_data) {
            $("#modal_body").html(result_data);
            $("#modal-title").html(title);
        }
    });
} 
function call_modal_default(title,form_url,activity,controller){
    $("#call_modal").modal({backdrop:'static',keyboard: false});
    $.ajax({
        url: '<?php echo $basepath ?>'+controller,
        data: "form_url="+form_url+"&activity="+activity,
        type: 'POST',
        dataType: 'html',
        success: function(result_data) {
            $("#modal_body").html(result_data);
            $("#modal-title").html(title);
        }
    });
} 
function call_modal_default_no_full(title,form_url,activity,controller){
    $("#call_modal_no_full").modal({backdrop:'static',keyboard: false});
    $.ajax({
        url: '<?php echo $basepath ?>'+controller,
        data: "form_url="+form_url+"&activity="+activity,
        type: 'POST',
        dataType: 'html',
        success: function(result_data) {
            $("#modal_body_no_full").html(result_data);
            $("#modal-title_no_full").html(title);
        }
    });
} 
function ajax_table_map(div_id,map_id,upt,link_url){
    $.ajax({
        url: '<?php echo $basepath ?>'+link_url,
        data: 'div_id='+div_id+'&map_id='+map_id+'&upt='+upt,
        type: 'POST',
        dataType: 'html',
        success: function(result_data) {
            $("#"+div_id).html(result_data);
        }
    });
}
function verif_data(checkboxes_name,link_url,after_controller,fct_after){
    var total =  count_checkbox(checkboxes_name);
    if(total!=0){
      var get_data  =  val_checkbox(checkboxes_name);
        swal({
              title: 'Persetujuan Data',
              text: 'Apakah anda ingin melakukan persetujuan data ini ?',
              type: 'warning',      // warning,info,success,error
              showCancelButton: true,
              showLoaderOnConfirm: true,
              preConfirm: function(){
                $.ajax({
                    url: '<?php echo $basepath ?>'+link_url,
                    type: 'POST',
                    data: 'kode='+get_data+'&total='+total,
                    success: function(data) {
                      if(data=="OK"){
                        swal({
                          title: 'Success',
                          text: "Data berhasil diverifikasi",
                          type: 'success',
                          showCancelButton: false,
                          showLoaderOnConfirm: false,
                        }).then(function() {
                           if(after_controller=='no_refresh'){
                               window[fct_after]();   
                            }
                            else if(after_controller!=''){
                               window.location = '<?php echo $basepath ?>'+after_controller;
                            }
                            else {
                              location.reload();
                            } 
                        });
                      }
                      else if(data=="NOT_LOGIN"){
                        swal({
                            title: 'Error',
                            text: "Session tidak valid, silahkan login kembali",
                            type: 'error',
                            showCancelButton: false,
                            showLoaderOnConfirm: false,
                         }).then(function() {
                             location.reload();
                          });
                      }
                      else{
                        swal({
                            title: 'Error',
                            html: data,
                            type: 'error',
                            showCancelButton: false,
                            showLoaderOnConfirm: false,
                        });
                      }
                    }
                });
               }
            });
    }
    else {
      swal({
              title: 'Error',
              html: 'Harap checklist data terlebih dahulu',
              type: 'error',
              showCancelButton: false,
              showLoaderOnConfirm: false,
          });
    }
  }
  function reject_data(checkboxes_name,link_url,after_controller,fct_after,id_ket){
    var total      =  count_checkbox(checkboxes_name);
    var keterangan =  $("#"+id_ket).val();
    if(total!=0){
      var get_data  =  val_checkbox(checkboxes_name);
        swal({
              title: 'Reject Data',
              text: 'Apakah anda ingin melakukan reject data ini ?',
              type: 'warning',      // warning,info,success,error
              showCancelButton: true,
              showLoaderOnConfirm: true,
              preConfirm: function(){
                $.ajax({
                    url: '<?php echo $basepath ?>'+link_url,
                    type: 'POST',
                    data: 'kode='+get_data+'&ket='+keterangan+'&total='+total,
                    success: function(data) {
                      if(data=="OK"){
                        swal({
                          title: 'Success',
                          text: "Data berhasil direject",
                          type: 'success',
                          showCancelButton: false,
                          showLoaderOnConfirm: false,
                        }).then(function() {
                           if(after_controller=='no_refresh'){
                               window[fct_after]();   
                            }
                            else if(after_controller!=''){
                               window.location = '<?php echo $basepath ?>'+after_controller;
                            }
                            else {
                              location.reload();
                            } 
                        });
                      }
                      else if(data=="NOT_LOGIN"){
                        swal({
                            title: 'Error',
                            text: "Session tidak valid, silahkan login kembali",
                            type: 'error',
                            showCancelButton: false,
                            showLoaderOnConfirm: false,
                         }).then(function() {
                             location.reload();
                          });
                      }
                      else{
                        swal({
                            title: 'Error',
                            html: data,
                            type: 'error',
                            showCancelButton: false,
                            showLoaderOnConfirm: false,
                        });
                      }
                    }
                });
               }
            });
    }
    else {
      swal({
              title: 'Error',
              html: 'Harap checklist data terlebih dahulu',
              type: 'error',
              showCancelButton: false,
              showLoaderOnConfirm: false,
          });
    }
  }
function act_delete(header_text,content_text,type_icon,link_url,after_controller,fct_after,parameter_data,id_data){
  swal({
            title: header_text,
            text: content_text,
            type: type_icon,      // warning,info,success,error
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function(){
              $.ajax({
                  url: '<?php echo $basepath ?>'+link_url,
                  type: 'POST',
                  data: parameter_data+'='+id_data,
                  success: function(data) {
                    if(data=="OK"){
                      swal({
                        title: 'Success',
                        type: 'success',
                        showCancelButton: false,
                        showLoaderOnConfirm: false,
                      }).then(function() {
                         if(after_controller=='no_refresh'){
                               window[fct_after]();   
                          }
                          else if(after_controller!=''){
                             window.location = '<?php echo $basepath ?>'+after_controller;
                          }
                          else {
                            location.reload();
                          }  
                      });
                    }
                    else if(data=="NOT_LOGIN"){
                      swal({
                          title: 'Error',
                          text: "Session tidak valid, silahkan login kembali",
                          type: 'error',
                          showCancelButton: false,
                          showLoaderOnConfirm: false,
                       }).then(function() {
                           location.reload();
                        });
                    }
                    else{
                      swal({
                          title: 'Error',
                          html: data,
                          type: 'error',
                          showCancelButton: false,
                          showLoaderOnConfirm: false,
                      });
                    }
                  }
              });
             }
          });
}
function do_act(form_id,act_controller,after_controller,header_text,content_text,type_icon,fct_after){
                  swal({
                    title: header_text,
                    text: content_text,
                    type: type_icon,      // warning,info,success,error
                    showCancelButton: true,
                    showLoaderOnConfirm: true,
                    preConfirm: function(){
                      $.ajax({
                          url: act_controller, 
                          type: 'POST',
                          data: new FormData($('#'+form_id)[0]),  // Form ID
                          processData: false,
                          contentType: false,
                          success: function(data) {
                              var data_trim = $.trim(data);
                              if(data_trim=="OK")
                              {
                                  swal({
                                      title: 'Success',
                                      type: 'success',
                                      showCancelButton: false,
                                      showLoaderOnConfirm: false,
                                    }).then(function() {
                                        if(after_controller=='no_refresh'){
                                             window[fct_after]();   
                                        }
                                        else if(after_controller!=''){
                                           window.location = '<?php echo $basepath ?>'+after_controller;
                                        }
                                        else {
                                          location.reload();
                                        } 
                                });
                              } 
                              else if(data_trim=="NOT_LOGIN")
                              {
                                    swal({
                                      title: 'Error',
                                      text: "Session tidak valid, silahkan login kembali",
                                      type: 'error',
                                      showCancelButton: false,
                                      showLoaderOnConfirm: false,
                                   }).then(function() {
                                       location.reload();
                                    });
                              }
                              else
                              {
                                  swal({
                                      title: 'Error',
                                      html: data_trim,
                                      type: 'error',
                                      showCancelButton: false,
                                      showLoaderOnConfirm: false,
                                    });
                              }
                          }
                      });
                   }
      });   
}
function show_map(id,call_funct){
      var data = [
        <?php 
        //Show Provinsi in Map
        $prov = "";
        $data_ps = $gen_model->GetWhere('ms_provinsi');
          while($list = $data_ps->FetchRow()){
            foreach($list as $key=>$val){
                        $key=strtolower($key);
                        $$key=$val;
                      }  
             $prov .="['".$id_map."','".$color_map."'],\r";          
          }
          echo rtrim($prov,',');
        ?>
  ];

    // Create the chart
    Highcharts.mapChart(id, {
        chart: {
          map: 'countries/id/id-all',
          //backgroundColor:'#FFF'  //Single Color
          backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                    stops: [
                        [0, '#cddee4'],
                        [1, '#4b96af']
                    ]
                }
        },

        title: {
            text: ' '
        },
        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'top'
            }
        },
        legend: {
          enabled: false
        },
        credits: {
            enabled: false
        },
      plotOptions:{
        series:{
          point:{
            events:{
              click: function(){
                //detail_loket_pelayanan(this.options['hc-key']);
				window[call_funct](this.options['hc-key']);
              }
            }
          }
        }
      },
        series: [
      {
        data:  data,
        name: 'Provinsi',
        keys: ['hc-key', 'color'],
        tooltip: {
               headerFormat: '',
               pointFormat: '{point.name}'
            },
            states: {
                hover: {
                    color: '#BADA55'
                }
            }
        }
      ]
    });
}
function myFunction() {
    var element = document.getElementById("myDIV");
    element.classList.toggle("white-left-show");
  }

function pie_chart(id,mydata){
    Highcharts.chart(id, {
      exporting: { enabled: false },
      chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie'
      },
      title: {
          text: ''
      },
      credits: {
            enabled: false
      },
      tooltip: {
          pointFormat: '{series.name}: <b>{point.y:.0f}</b>'
      },
      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.y:.0f}',
                  style: {
                      color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                  }
              }
          }
      },
      title: false,
      subtitle:false,
      series: [{
          name: 'Total',
          colorByPoint: true,
          data: mydata
      }]
  });
}


</script>
</body>
</html>
