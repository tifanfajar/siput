<?php 
   if($_SESSION['group_user']=="grp_171026194411_1142"){  //Admin
?>
<title>Dashboard - <?php echo $web['judul']?></title>
<body class="dash-back">

  <?php 
    include "view/top_menu.php"; 
  ?>
  <style type="text/css">
	
  #tb_dashboard th {
       text-align: center;   
       font-weight:bold;
    }
  #tb_dashboard>tbody>tr>td {
        white-space: nowrap;
    }
      .headcol {
            background-color: #fcfefc;
            position:sticky; 
            width:5em; 
            left:0;
            top:auto;
            border-right: 0px none black; 
            border-top-width:3px; /*only relevant for first row*/
            margin-top:-3px; /*compensate for top border*/
			z-index:4;
        }
		.headcol2 {
            background-color: #fcfefc;
            position:sticky; 
            width:5em; 
            left:0;
            top:auto;
            border-right: 0px none black; 
            border-top-width:3px; /*only relevant for first row*/
            margin-top:-3px; /*compensate for top border*/
			z-index:2;
        }
		.head-fixed{
			position:unset !important;
		}
		marquee{background-color:rgba(0, 0, 0, 0.66) !important;}
  </style>
  <!-- Content -->
  
  <?php if(!empty($web['running_text'])){ ?>
      <marquee style="margin-bottom:25px;background-color:#<?php echo $web['running_text_color_bg'] ?>;">
          <span style="color:#<?php echo $web['running_text_color'] ?>;font-size:<?php echo $web['running_text_size'] ?>"><?php echo $web['running_text'] ?></span>
      </marquee>
    <?php } ?>
  
  <div class="container container-mobile animated fadeInDown" >
    <div class="row">
      <div class="col-md-12">
        <h5 class="judul-aplikasi"><?php echo $web['judul']?></h5>
      </div>
      
      
      <script type="text/javascript">
        function call_upt(){
            var prov = $("#upt_provinsi").val();
                $.ajax({
                     type: "POST",
                     dataType: "html",
                     url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
                     data: "provinsi="+prov,
                     success: function(msg){
                        if(msg){
                          $("#kode_upt").html(msg);        
                        }
                     }
                  }); 
          }
      </script>

        <!-- Tabel -->
                <div style="width:100%;">
                      <form>
						  <div class="row">
							<div class="col-md-2 form-group form-box col-xs-12">
							  <span class="label">Provinsi</span> 
							  <select class="form-control form-control-sm" style="width:100%" onchange="call_upt()" id="upt_provinsi" name="id_prov" >
								<option value="">Pilih Provinsi</option>
								<?php 
								$data_shf = $gen_model->GetWhere('ms_provinsi');
								while($list = $data_shf->FetchRow()){
								foreach($list as $key=>$val){
								$key=strtolower($key);
								$$key=$val;
							  }  
							  ?>
							  <option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
							  <?php } ?>
							</select>
						  </div>
						  <div class="col-md-2 form-group form-box col-xs-12">
							<span class="label">UPT</span> 
							<select class="form-control form-control-sm"  id="kode_upt" name="loket_upt"  style="width:100%">
							  <option value="">Pilih UPT</option>
							</select>
						  </div>
						  <div class="col-md-2 form-group form-box col-xs-12">
							<span class="label">Tanggal Awal</span> 
							<input readonly style="background-color:white"  class="form-control form-control-sm tgl" id="tgl_awal" name="tgl_awal"   placeholder="" type="text" >
						  </div>
						  <div class="col-md-2 form-group form-box col-xs-12">
							<span class="label">Tanggal Akhir</span> 
							<input readonly style="background-color:white" class="form-control form-control-sm tgl"  id="tgl_akhir" name="tgl_akhir"   placeholder="" type="text" >
						  </div>
						  <div class="col-md-4 form-group form-box col-xs-12" >
							<span class="label"><br/></span> 
							<button type="reset" class="btn btn-danger btn-sm float-right" style="margin-left: 10px; padding:.25rem .5rem;"><i class="fa fa-eraser"></i> Reset</button>
							<button type="button" onclick="cari()" class="btn btn-primary btn-sm float-right" style="padding:.25rem .5rem;"><i class="fa fa-search"></i> Cari</button>&nbsp;&nbsp;
						  </div>
						</div>
						<div class="row">
						</div>
						</form>
            </div>
              <style type="text/css">
                .table-cont{
                    /**make table can scroll**/
                    max-height: 400px;
                    overflow: auto;
                  }
				  .table{font-size:13px;}
				  .table thead th{background-color:#f2f2f2 !important;}
              </style>
              <script type="text/javascript">
                 function cari(){
                      var upt_provinsi = $("#upt_provinsi").val();
                      var kode_upt     = $("#kode_upt").val();
                      var tgl_awal     = $("#tgl_awal").val();
                      var tgl_akhir    = $("#tgl_akhir").val();
                       $.ajax({
                           type: "POST",
                           dataType: "html",
                           url: "<?php echo $basepath ?>dashboard/rest_filter",
                           data: "provinsi="+upt_provinsi+"&upt="+kode_upt+"&tgl_awal="+tgl_awal+"&tgl_akhir="+tgl_akhir,
                           success: function(msg){
                              if(msg){
                                $("#table-cont").html(msg);        
                              }
                           }
                        }); 
                 }
                 // Code goes here
                    'use strict'
                    window.onload = function(){
                      var tableCont = document.querySelector('#table-cont')
                      /**
                       * scroll handle
                       * @param {event} e -- scroll event
                       */
                      function scrollHandle (e){
                        var scrollTop = this.scrollTop;
                        this.querySelector('thead').style.transform = 'translateY(' + scrollTop + 'px)';
                      }
                      
                      tableCont.addEventListener('scroll',scrollHandle)
                    }
                    $(".tgl").datepicker({
                       format: 'dd/mm/yyyy',
                    });
              </script>
                    <div class='table-cont' id='table-cont'>
                    <table id="tb_dashboard" class="table table-bordered table-cont">
                      <thead style="z-index:100">
                        <tr>
                          <th rowspan="2" class="align-middle headcol head-fixed">Provinsi</th>
                          <th rowspan="2" class="align-middle headcol2 head-fixed">Nama UPT</th>
                          <?php 
                               $sql_men="select * from ms_menu_tab where tab_id between '1' and '12' order by urutan asc";
                                $result_men= $db->Execute($sql_men);
                                while($men=$result_men->FetchRow()){ ?>
                                   <th colspan="4"><?php echo $men['tab'] ?></th>
                           <?php } ?>
                        </tr>
                        <tr>
                           <?php 
                               $sql_men="select * from ms_menu_tab where tab_id  between '1' and '12'  order by urutan asc";
                                $result_men= $db->Execute($sql_men);
                                while($men=$result_men->FetchRow()){ ?>
                                    <th>Approve</th>
                                    <th>Reject</th>
                                    <th>Waiting Verification</th>
                                    <th>Waiting Update</th>
                           <?php } ?>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                          $data_prov_upt = $db->SelectLimit("select pv.id_prov,pv.nama_prov,upt.kode_upt,upt.nama_upt
                                                              from ms_provinsi as pv
                                                                  inner join ms_upt as upt
                                                                      on pv.id_prov=upt.id_prov order by pv.id_prov asc "); 
                          while($aRow = $data_prov_upt->FetchRow()){ ?>
                        <tr>
                            <td class="headcol"><?php echo $aRow['nama_prov'] ?></td>
                            <td class="headcol2"><?php echo $aRow['nama_upt'] ?></td>
                            <?php 
                               $sql_men="select * from ms_menu_tab where tab_id  between '1' and '12'  order by urutan asc";
                                $result_men= $db->Execute($sql_men);
                                while($men=$result_men->FetchRow()){ 

                                      $approve = $gen_model->GetOne('count(*) as total ',$men['table'],array('status'=>'3','id_prov'=>$aRow['id_prov'],'kode_upt'=>$aRow['kode_upt']));
                                      $reject = $gen_model->GetOne('count(*) as total ',$men['table'],array('status'=>'1','id_prov'=>$aRow['id_prov'],'kode_upt'=>$aRow['kode_upt']));
                                      $verif = $gen_model->GetOne('count(*) as total ',$men['table'],array('status'=>'0','id_prov'=>$aRow['id_prov'],'kode_upt'=>$aRow['kode_upt']));
                                      $update = $gen_model->GetOne('count(*) as total ',$men['table'],array('status'=>'4','id_prov'=>$aRow['id_prov'],'kode_upt'=>$aRow['kode_upt']));   

                                      $approve = (!empty($approve) ? $approve : '0');
                                      $reject =  (!empty($reject) ? $reject : '0');
                                      $verif =   (!empty($verif) ? $verif : '0');
                                      $update =  (!empty($update) ? $update : '0');
                                  ?>
                                    <td align="right"><?php echo $approve ?></td>
                                    <td align="right"><?php echo $reject ?></td>
                                    <td align="right"><?php echo $verif ?></td>
                                    <td align="right"><?php echo $update ?></td>
                           <?php } ?>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
             

      <div class="col-md-12"><br/><br/><br/>
        <small class="copy">Copyright &copy; <?php echo date("Y") ?> Direktorat Operasi Sumber Daya [OPS11]</small>
      </div>

    </div>
  </div>
  <!-- End Content -->
  
</body>
<?php  } else {
    echo "<script>window.location = '".$basepath."home';</script>";
} ?>