<title>Data SIMS - <?php echo $web['judul']?></title>


<style type="text/css">
  .table th {
     text-align: center;
     font-weight:bold;
  }
  .datatable>tbody>tr>td
  {
    white-space: nowrap;
  } 
</style>
<body class="pelayanan-cnt">
  <?php  include "view/top_menu.php";  ?>
  
  <!-- Main Container -->
  <div class="container-fluid animated fadeInUp" style="z-index: 3;">
    <div class="row">
  <?php  
    include "view/menu.php";  
   ?>
  <!-- Content -->
      
      <div class="container-fluid main-content">
      <div class="row">
          
  <div class="cnt-header row" style="margin-bottom: 20px;">
    <h6 class="table-label" style="display:block; width:100%; clear: both;"><i class="fa fa-file"></i> SIMS</h6>
    <input type="hidden" name="SimsPencarian" id="SimsPencarian" >
    <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">

      <a href="javascript:void(0)" onclick="print_laporan_detail('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

      <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" title="Filterisasi" data-target="#Filter_Sims"  ><i class="fa fa-filter"></i></a>

      <div class="btn-group" role="group">
        <button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-file-pdf"></i>
        </button>
        <div class="dropdown-menu" aria-labelledby="red">
          <a class="dropdown-item"  onclick="print_laporan_detail('pdf')" href="javascript:void(0)">Download PDF</a>
          <a class="dropdown-item"   onclick="print_laporan_detail('xls')" href="javascript:void(0)">Download XLS</a>
          <a class="dropdown-item" onclick="print_laporan_detail('doc')"   href="javascript:void(0)">Download DOCS</a>
        </div>
      </div>
    </div>
    <!--<hr style="width:100%; display: block; float: left;">-->
  </div>
  <script type="text/javascript">
    function print_laporan_detail(tipe){
      var filter_nama_perusahaan  = $("#filter_nama_perusahaan").val();
      var filter_kode_perusahaan  = $("#filter_kode_perusahaan").val();


      var filter_date_from        = $("#filter_date_from").val();
      var filter_date_to          = $("#filter_date_to").val();

      var filter_no_spp           = $("#filter_no_spp").val();
      var filter_no_client        = $("#filter_no_client").val();
      var filter_request_reff     = $("#filter_request_reff").val();
      var filter_licence_state    = $("#filter_licence_state").val();
      var filter_tagihan          = $("#filter_tagihan").val();
      var filter_tgl_begin        = $("#filter_tgl_begin").val();
      var filter_status           = $("#filter_status").val();
      var filter_status_izin      = $("#filter_status_izin").val();
      var filter_kategori_spp     = $("#filter_kategori_spp").val();
      var filter_service          = $("#filter_service").val();
      var filter_subservice       = $("#filter_subservice").val();

      var param="";
      if(filter_no_spp){
        param += " and sm.no_spp='"+filter_no_spp+"' ";
      }if(filter_request_reff){
        param += " and sm.request_reff='"+filter_request_reff+"' ";
      }if(filter_licence_state){
        param += " and sm.licence_state='"+filter_licence_state+"' ";
      }if(filter_tagihan){
        param += " and sm.tagihan='"+filter_tagihan+"' ";
      }if(filter_tgl_begin){
        param += " and sm.tgl_begin like '"+filter_tgl_begin+"%' ";
      }if(filter_status){
        param += " and sm.status='"+filter_status+"' ";
      }if(filter_status_izin){
        param += " and sm.status_izin='"+filter_status_izin+"' ";
      }if(filter_kategori_spp){ 
        param += " and sm.kategori_spp='"+filter_kategori_spp+"' ";
      }if(filter_service){
        param += " and sm.service='"+filter_service+"' ";
      }if(filter_subservice){
        param += " and sm.subservice='"+filter_subservice+"' ";
      }if(filter_nama_perusahaan && filter_kode_perusahaan){
        param += " and sm.kode_perusahaan='"+filter_kode_perusahaan+"' ";
      }

      if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
        }
        else {
           param += "&filter_date_from="+filter_date_from.trim()+"&filter_date_to="+filter_date_to.trim();
           param += " and sm.created_date BETWEEN '"+date_to_default(filter_date_from.trim())+"' AND '"+date_to_default(filter_date_to.trim())+"' ";
        }
      }
      else if(filter_date_from){
          param += " and sm.created_date like '"+date_to_default(filter_date_from)+"%' ";
      }
      else if(filter_date_to){
          param += " and sm.created_date like '"+date_to_default(filter_date_to)+"%' ";
      }


        var form = document.createElement("form");
        form.setAttribute("method", "POST");
        if(tipe=="print"){
         form.setAttribute("target", "_blank");
        }
        form.setAttribute("action", "<?php echo $basepath ?>sims/cetak");
               
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", "param");
            hiddenField.setAttribute("value", btoa(param));

            var hiddenField2 = document.createElement("input");
            hiddenField2.setAttribute("type", "hidden");
            hiddenField2.setAttribute("name", "tipe");
            hiddenField2.setAttribute("value", tipe);

            form.appendChild(hiddenField);
            form.appendChild(hiddenField2);

        document.body.appendChild(form);
        form.submit();
    }
  </script>

        <div class="table table-responsive pelayanan-top" id="table_filter_sims">
          <table id="tb_sims" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
                    <th>No SPP</th>
                    <th>No Klien Licence</th>
                    <th>Perusahaan</th>
                    <th>Request Reff</th>
                    <th>Licence State</th>
                    <th>Tagihan</th>
                    <th>Status</th>
                    <th>Tanggal Begin</th>
                    <th>Status Izin</th>
                    <th>Kategori SPP</th>
                    <th>Service</th>
                    <th>Sub Service</th>
                    <th>Created Date</th>
                    <th>Last Update</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
              <hr>
              
            </div>
      
      </div>
    </div>


    </div>
  </div>

<script type="text/javascript">

  $(".tgl").datepicker({
     format: 'dd/mm/yyyy',
  });
var dTable;
  $(document).ready(function() {
    dTable = $('#tb_sims').DataTable( {
      "bProcessing": true,
      "bServerSide": true,
      "bJQueryUI": false,
      "searching": false,
      "responsive": false,
      "autoWidth": false,
      "sAjaxSource": "<?php echo $basepath ?>sims/rest&only=view", 
      "sServerMethod": "POST",
      "scrollX": true,
      "scrollY": "350px",
        "scrollCollapse": true,
      "columnDefs": [
      { "orderable": true, "targets": 0, "searchable": true},
      { "orderable": true, "targets": 1, "searchable": true},
      { "orderable": true, "targets": 2, "searchable": true},
      { "orderable": true, "targets": 3, "searchable": true},
      { "orderable": true, "targets": 4, "searchable": true},
      { "orderable": true, "targets": 5, "searchable": true},
      { "orderable": true, "targets": 6, "searchable": true},
      { "orderable": true, "targets": 7, "searchable": true},
      { "orderable": true, "targets": 8, "searchable": true},
      { "orderable": true, "targets": 9, "searchable": true},
      { "orderable": true, "targets": 10, "searchable": true},
      { "orderable": true, "targets": 11, "searchable": true},
      { "orderable": true, "targets": 12, "searchable": true},
      { "orderable": true, "targets": 13, "searchable": true},
      { "orderable": false, "targets": 14, "searchable": false, "width":170}
      ]
    } );
  });
function do_detail_sims(param_id){
                call_modal_default('Detail SIMS','Form_SIMS','do_detail','sims/form');
                 setTimeout(function(){ 
                     $.ajax({
                          url: '<?php echo $basepath ?>sims/edit/'+param_id,
                          type: 'POST',
                          dataType: 'JSON',
                          success: function(data) {
                              $("#kode_sims").val(data.kode_sims);
                              $("#no_spp").val(data.no_spp);
                              $("#no_client").val(data.no_klien_licence);
                              $("#no_aplikasi").val(data.no_aplikasi);
                              $("#kode_perusahaan").val(data.kode_perusahaan);
                              $("#group_user").val(data.group_user);
                              $("#nama_perusahaan").val(data.cmp);
                              $("#request_reff").val(data.request_reff);
                              $("#licence_state").val(data.licence_state);
                              $("#tagihan").val(data.tagihan);
                              $("#status").val(data.status);
                              $("#tgl_begin").val(data.tgl_begin);
                              $("#status").val(data.status);
                              $("#status_izin").val(data.status_izin);
                              $("#kategori_spp").val(data.kategori_spp);
                              $("#service").val(data.service);
                              $("#subservice").val(data.subservice);
                          }
                      });
                 }, 1000);
              }
</script>

<!--  Start Filter -->
<div id="Filter_Sims" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_sims_filter" method="POST">

            <div class="row">  
              <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label">No SPP</span> 
                <input  class="form-control"  id="filter_no_spp" name="filter_no_spp"  maxlength="50" type="text"  >
              </div>
              <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label">No Client License</span>
                <input class="form-control" id="filter_no_client" name="filter_no_client"  maxlength="50" type="text"  >
              </div>

              <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label">No Aplikasi</span>
                <input class="form-control"  id="filter_no_aplikasi" name="filter_no_aplikasi" maxlength="50" type="text"  >
              </div>
            </div>

            <div class="row">  
                <div class="col-md-8 form-group form-box col-xs-12">
                  <span class="label">Perusahaan</span> 
                  <div class="input-group">
                    <input class="form-control" id="filter_company" name="filter_nama_perusahaan"   placeholder="" type="text"  >
                    <div class="input-group-btn">
                      <button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
                  </div>
                </div>
                <input  id="filter_kode_perusahaan" name="filter_kode_perusahaan" type="hidden" >
              </div>
              <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label">Request Reff</span>
                <input class="form-control"  id="filter_request_reff" name="filter_request_reff" maxlength="50"   placeholder="" type="text"  >
              </div>
            </div>

            <div class="row">  
                <div class="col-md-4 form-group form-box col-xs-12">
                  <span class="label">License State</span>
                  <input class="form-control"  id="filter_licence_state" name="filter_licence_state"  maxlength="50"  placeholder="" type="text"  >
                </div>
                <div class="col-md-4 form-group form-box col-xs-12">
                  <span class="label">Tagihan</span>
                  <input class="form-control"  id="filter_tagihan" name="filter_tagihan"  onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);"   type="text"  >
                </div>
                <div class="col-md-4 form-group form-box col-xs-12">
                  <span class="label">Tgl Begin</span>
                  <input class="form-control tgl"  id="filter_tgl_begin" name="filter_tgl_begin"  type="text"  >
                </div>
            </div>

            <div class="row">  
                <div class="col-md-4 form-group form-box col-xs-12">
                  <span class="label">Status</span>
                  <input class="form-control"  id="filter_status" name="filter_status" maxlength="50"  type="text"  >
                </div>
                <div class="col-md-4 form-group form-box col-xs-12">
                  <span class="label">Status Izin</span>
                  <input class="form-control"  id="filter_status_izin" name="filter_status_izin"  maxlength="50" placeholder="" type="text"  >
                </div>
                <div class="col-md-4 form-group form-box col-xs-12">
                  <span class="label">Kategori SPP</span>
                  <input class="form-control"  id="filter_kategori_spp" name="filter_kategori_spp"  maxlength="50" placeholder="" type="text"  >
                </div>
            </div>

            <div class="row">  
              <div class="col-md-6 form-group form-box col-xs-12">
                <span class="label">Service</span>
                <input class="form-control"  id="filter_service" name="filter_service" maxlength="50"  placeholder="" type="text"  >
              </div>
              <div class="col-md-6 form-group form-box col-xs-12">
                <span class="label">Sub Service</span>
                <input class="form-control"  id="filter_subservice" name="filter_subservice" maxlength="50"  placeholder="" type="text"  >
              </div> 
           </div> 

           <div class="row">
              <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label">Created Date</span><br/>
                <span class="label" style="font-size: 13px;">Mulai</span> 
                <input class="form-control tgl"  id="filter_date_from" name="filter_date_from" maxlength="50"  placeholder="" type="text"  >
              </div>
              <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label"></span><br/>
                <span class="label" style="font-size: 13px;">Akhir</span> 
                <input class="form-control tgl"  id="filter_date_to" name="filter_date_to" maxlength="50"  placeholder="" type="text"  >
              </div> 
           </div> 

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_sims_filter')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_sims()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
  $('#filter_company').typeahead({
      hint: true,
      highlight: true,
      source: function (query, process) {
          $.ajax({
              url: '<?php echo $basepath ?>ajax/reqCompany',
              type: 'POST',
              dataType: 'JSON',
              data: 'query='+$('#filter_company').val(),
              success: function(data) {
                  var results = data.map(function(item) {
                      var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
                      return someItem;
                  });
                  return process(results);
              }
          });
      },
      afterSelect: function(item) {
          this.$element[0].value = item.company_name
      },
      updater: function(item) {
          $('#filter_kode_perusahaan').val(item.company_id);
          return item;
      }
    });
  function filter_sims() {
      $("#table_filter_sims").html("");

      var filter_nama_perusahaan  = $("#filter_nama_perusahaan").val();
      var filter_kode_perusahaan  = $("#filter_kode_perusahaan").val();


      var filter_date_from        = $("#filter_date_from").val();
      var filter_date_to          = $("#filter_date_to").val();

      var filter_no_spp           = $("#filter_no_spp").val();
      var filter_no_client        = $("#filter_no_client").val();
      var filter_request_reff     = $("#filter_request_reff").val();
      var filter_licence_state    = $("#filter_licence_state").val();
      var filter_tagihan          = $("#filter_tagihan").val();
      var filter_tgl_begin        = $("#filter_tgl_begin").val();
      var filter_status           = $("#filter_status").val();
      var filter_status_izin      = $("#filter_status_izin").val();
      var filter_kategori_spp     = $("#filter_kategori_spp").val();
      var filter_service          = $("#filter_service").val();
      var filter_subservice       = $("#filter_subservice").val();

      var param="";
      if(filter_no_spp){
        param += "&no_spp="+filter_no_spp.trim();
      }if(filter_request_reff){
        param += "&request_reff="+filter_request_reff.trim();
      }if(filter_licence_state){
        param += "&licence_state="+filter_licence_state.trim();
      }if(filter_tagihan){
        param += "&tagihan="+filter_tagihan.trim();
      }if(filter_tgl_begin){
        param += "&tgl_begin="+filter_tgl_begin.trim();
      }if(filter_status){
        param += "&status="+filter_status.trim();
      }if(filter_status_izin){
        param += "&status_izin="+filter_status_izin.trim();
      }if(filter_kategori_spp){
        param += "&kategori_spp="+filter_kategori_spp.trim();
      }if(filter_service){
        param += "&service="+filter_service.trim();
      }if(filter_subservice){
        param += "&subservice="+filter_subservice.trim();
      }if(filter_nama_perusahaan && filter_kode_perusahaan){
        param += "&kode_perusahaan="+filter_kode_perusahaan.trim();
      }

      param += "&only=view";

      if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += "&filter_date_from="+filter_date_from.trim()+"&filter_date_to="+filter_date_to.trim();
        }
      }
      else if(filter_date_from){
          param += "&filter_date_from="+filter_date_from.trim();
      }
      else if(filter_date_to){
          param += "&filter_date_to="+filter_date_to.trim();
      }

       $.ajax({
              url: '<?php echo $basepath ?>sims/table_sims_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#table_filter_sims').html(data);
              }
          });
      $('#Filter_Sims').modal('hide');
   } 
</script>
<!-- End Filter -->


<!-- Start Import -->
<div id="Import_Sims" class="modal fade" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_Sims" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>

         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_sims.xls" download>Download Sample File</a><br/><br/>
         <span style="font-size: 14px;">File Import Membutuhkan Kode Perusahaan</span><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>company">List Data Perusahaan</a><br/>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        <form>
      </div>
    </div>
</div>

<script type="text/javascript">
  $("#DoImport_Sims").on("submit", function (event) {
    event.preventDefault();
        do_act('DoImport_Sims','sims/import','','Import File sims','Apakah anda ingin import file SIMS ini ?','info');
    });
</script>
<!-- End Import -->