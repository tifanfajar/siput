<!-- Reject Bahan Sosialisasi -->
<div id="Reject_Bahan_Sosialisasi" class="modal fade call_modal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle">Reject</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="pelayanan_reject" method="POST" autocomplete="off">
                <div class="row">
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Keterangan</span> 
                    <input type="text" name="keterangan" id="reject_ket_bahan_sosialisasi" class="form-control">
                  </div>
                 </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="reject_data('kode_bhn_sosialisasi','sosialisasi_bimtek_bahan_sosialisasi/reject','no_refresh','refresh_table','reject_ket_bahan_sosialisasi')" class="btn btn-danger btn-sm">Reject</button>
        </div>
      </div>
    </div>
</div>

<!-- Reject Rencana Sosialisasi -->
<div id="Reject_Rencana_Sosialisasi" class="modal fade call_modal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle">Reject</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="pelayanan_reject" method="POST" autocomplete="off">
                <div class="row">
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Keterangan</span> 
                    <input type="text" name="keterangan" id="reject_ket_rencana_sosialisasi" class="form-control">
                  </div>
                 </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="reject_data('kode_rencana_sosialisasi','sosialisasi_bimtek_rencana_sosialisasi/reject','no_refresh','refresh_table','reject_ket_rencana_sosialisasi')" class="btn btn-danger btn-sm">Reject</button>
        </div>
      </div>
    </div>
</div>

<!-- Reject Monev Sosialisasi -->
<div id="Reject_Monev_Sosialisasi" class="modal fade call_modal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle">Reject</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="pelayanan_reject" method="POST" autocomplete="off">
                <div class="row">
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Keterangan</span> 
                    <input type="text" name="keterangan" id="reject_ket_monev_sosialisasi" class="form-control">
                  </div>
                 </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="reject_data('kode_monev_sosialisasi','sosialisasi_bimtek_monev_sosialisasi/reject','no_refresh','refresh_table','reject_ket_monev_sosialisasi')" class="btn btn-danger btn-sm">Reject</button>
        </div>
      </div>
    </div>
</div>

<!-- Reject Galeri -->
<div id="Reject_Galeri" class="modal fade call_modal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle">Reject</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="pelayanan_reject" method="POST" autocomplete="off">
                <div class="row">
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Keterangan</span> 
                    <input type="text" name="keterangan" id="reject_ket_galeri" class="form-control">
                  </div>
                 </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="reject_data('kode_galeri','sosialisasi_bimtek_galeri/reject','no_refresh','refresh_table','reject_ket_galeri')" class="btn btn-danger btn-sm">Reject</button>
        </div>
      </div>
    </div>
</div>

            <!-- Modal Filter -->
<!-- Filter Bahan Sosialisasi All -->
<div id="Filter_BhnSosialisasiAll" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_bhn_sosialisasi_filter_all" method="POST">
                <div class="row">
               
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Materi</span> 
                    <select class="form-control" id="filter_materi_all" name="filter_materi_bhn_so_all" required>
                    <option value="">Pilih Materi</option>
                    <?php 
                      $data_mtr = $gen_model->GetWhere('ms_materi');
                        while($list = $data_mtr->FetchRow()){
                          foreach($list as $key=>$val){
                                      $key=strtolower($key);
                                      $$key=$val;
                                    }  
                      ?>
                    <option value="<?php echo $kode_materi ?>"><?php echo $jenis_materi ?></option>
                    <?php } ?>
                  </select>
                  </div>
                 </div>

                <div class="row"> 
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">Judul</span> 
                    <input type="text" name="filter_judul_all"  id="filter_judul_bhn_so_all" class="form-control">
                  </div>
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">Author</span> 
                    <input type="text" name="filter_author_all"  id="filter_author_bhn_so_all" class="form-control">
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">Deskripsi</span> 
                    <textarea name="filter_desc_all"  id="filter_desc_bhn_all" class="form-control"></textarea>
                  </div>
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">Keterangan</span> 
                    <textarea name="filter_ket_all"  id="filter_ket_bhn_all" class="form-control"></textarea>
                  </div>
                </div>
    
                 <div class="row">
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_from_bhn_so_all" name="filter_date_from_bhn_so_all" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_to_bhn_so_all" name="filter_date_to_bhn_so_all" maxlength="50"  placeholder="" type="text"  >
                  </div> 
              </div> 


                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" >
                    <span class="label">Provinsi</span> 
                    <select class="form-control" onchange="call_upt_fltr()" id="filter_provinsi_all" name="id_prov" >
                      <option value="">Pilih Provinsi</option>
                      <?php 
                        $data_shf = $gen_model->GetWhere('ms_provinsi');
                          while($list = $data_shf->FetchRow()){
                            foreach($list as $key=>$val){
                                        $key=strtolower($key);
                                        $$key=$val;
                                      }  
                        ?>
                      <option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
                    <span class="label">Nama UPT</span> 
                    <select class="form-control"  id="filter_upt_all" name="loket_upt" >
                      <option value="">Pilih UPT</option>
                    </select>
                  </div> 
                </div> 
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_bhn_sosialisasi_filter_all')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_bhn_sosialisasi_all()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
  function call_upt_fltr(){
    var prov = $("#filter_provinsi_all").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#filter_upt_all").html(msg);        
                }
             }
          }); 
  }
     function filter_bhn_sosialisasi_all() {
      $("#filter_bhn_so_all").html("");
      var filter_author     = $("#filter_author_bhn_so_all").val();
      var filter_judul      = $("#filter_judul_bhn_so_all").val();
      var filter_materi     = $("#filter_materi_bhn_so_all").val();
      var filter_provinsi   = $("#filter_provinsi_all").val();
      var filter_upt        = $("#filter_upt_all").val();
      var filter_date_from  = $("#filter_date_from_bhn_so_all").val();
      var filter_date_to    = $("#filter_date_to_bhn_so_all").val();
      var filter_desc       = $("#filter_desc_bhn_all").val();
      var filter_ket        = $("#filter_ket_bhn_all").val();

      var param="";
      if(filter_author){
        param += " and bhn.author like '%"+filter_author.trim()+"%' ";
      } if(filter_judul){
        param += " and bhn.judul like '%"+filter_judul.trim()+"%' ";
      }if(filter_materi){
        param += " and bhn.materi like '%"+filter_materi.trim()+"%' ";
      }if(filter_provinsi){
        param += " and (bhn.id_prov ='"+filter_provinsi.trim()+"' or bhn.id_prov is null) ";
      }if(filter_upt){
        param += " and (bhn.kode_upt ='"+filter_upt.trim()+"'  or bhn.kode_upt is null) ";
      }if(filter_desc){
        param += " and bhn.deskripsi like '%"+filter_desc.trim()+"%' ";
      }if(filter_ket){
        param += " and bhn.keterangan like '%"+filter_ket.trim()+"%' ";
      }

      if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(bhn.created_date, '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(bhn.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(bhn.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_to.trim())+"' ";
      }

      $("#BhnSosialisasiPencarian_all").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>sosialisasi_bimtek_bahan_sosialisasi/table_all_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_bhn_so_all').html(data);
              }
          });
      $('#Filter_BhnSosialisasiAll').modal('hide');
   }  
</script>


<!-- Filter Bahan Sosialisasi Detail -->
<div id="Filter_BhnSosialisasiDetail" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

         <div class="modal-body">
            <form id="form_bhn_sosialisasi_filter" method="POST">
                <div class="row">
                

                  <div class="col-md-12 form-group form-box col-xs-6">
                    <span class="label">Materi</span> 
                    <select class="form-control"  id="filter_materi" name="filter_materi_bhn_so" required <?php echo $readonly ?>>
                  <option value="">Pilih Materi</option>
                  <?php 
                    $data_mtr = $gen_model->GetWhere('ms_materi');
                      while($list = $data_mtr->FetchRow()){
                        foreach($list as $key=>$val){
                                    $key=strtolower($key);
                                    $$key=$val;
                                  }  
                    ?>
                  <option value="<?php echo $kode_materi ?>"><?php echo $jenis_materi ?></option>
                  <?php } ?>
                </select>
                  </div>
                 </div>

                <div class="row"> 
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">Judul</span> 
                    <input type="text" name="filter_judul"  id="filter_judul_bhn_so" class="form-control">
                  </div>
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">Author</span> 
                    <input type="text" name="filter_author"  id="filter_author_bhn_so" class="form-control">
                  </div>
                </div>

                 <div class="row"> 
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">Deskripsi</span> 
                    <textarea name="filter_desc"  id="filter_desc_bhn_so" class="form-control"></textarea>
                  </div>
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">Keterangan</span> 
                    <textarea name="filter_ket"  id="filter_ket_bhn_so" class="form-control"></textarea>
                  </div>
                </div>

                 <div class="row">
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_from_bhn_so" name="filter_date_from_bhn_so" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_to_bhn_so" name="filter_date_to_bhn_so" maxlength="50"  placeholder="" type="text"  >
                  </div> 
              </div> 
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_bhn_sosialisasi_filter')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_bhn_sosialisasi()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
  function filter_bhn_sosialisasi() {
      $("#filter_bhn_so_detail").html("");
      var filter_tgl        = $("#filter_tgl_bhn_so").val();
      var filter_author     = $("#filter_author_bhn_so").val();
      var filter_judul      = $("#filter_judul_bhn_so").val();
      var filter_materi     = $("#filter_materi_bhn_so").val();
      var filter_provinsi   = $("#filter_provinsi_bhn_so").val();
      var filter_upt        = $("#filter_upt_bhn_so").val();
      var filter_date_from  = $("#filter_date_from_bhn_so").val();
      var filter_date_to    = $("#filter_date_to_bhn_so").val();
      var filter_desc       = $("#filter_desc_bhn_so").val();
      var filter_ket        = $("#filter_ket_bhn_so").val();

      var param="";
      if(filter_author){
        param += " and bhn.author like '%"+filter_author.trim()+"%' ";
      } if(filter_judul){
        param += " and bhn.judul like '%"+filter_judul.trim()+"%' ";
      }if(filter_materi){
        param += " and bhn.materi like '%"+filter_materi.trim()+"%' ";
      }if(filter_provinsi){
        //param += " and (bhn.id_prov is null or bhn.id_prov ='"+filter_provinsi.trim()+"') ";
      }if(filter_upt){
        //param += " and (bhn.kode_upt is null or bhn.kode_upt ='"+filter_upt.trim()+"')  ";
      }if(filter_desc){
        param += " and bhn.deskripsi like '%"+filter_desc.trim()+"%' ";
      }if(filter_ket){
        param += " and bhn.keterangan like '%"+filter_ket.trim()+"%' ";
      }

       if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(bhn.created_date, '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(bhn.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(bhn.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_to.trim())+"' ";
      }

      $("#BhnSosialisasiPencarian").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>sosialisasi_bimtek_bahan_sosialisasi/table_detail_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_bhn_so_detail').html(data);
              }
          });
      $('#Filter_BhnSosialisasiDetail').modal('hide');
   } 
</script>

<!-- Filter Rencana Sosialisasi All -->
<div id="Filter_RencanaSosialisasiAll" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_rencana_sosialisasi_filter_all" method="POST">
                <div class="row">
                  <div class="col-md-12 form-group form-box col-xs-6" >
                    <span class="label">Jenis Kegiatan</span>
                    <select class="form-control"  id="filter_kegiatan_rcn_so_all" name="kegiatan" >
                      <option value="">Pilih Jenis Kegiatan</option>
                      <?php 
                        $data_mtr = $gen_model->GetWhere('ms_kegiatan');
                          while($list = $data_mtr->FetchRow()){
                            foreach($list as $key=>$val){
                                        $key=strtolower($key);
                                        $$key=$val;
                                      }  
                        ?>
                      <option value="<?php echo $kode_kegiatan ?>"><?php echo $kegiatan ?></option>
                      <?php } ?>
                    </select>
                 </div>
                 </div>

               

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Tempat</span>
                    <input  class="form-control" maxlength="100" id="filter_tempat_rcn_so_all" name="tempat"   placeholder="" type="text" >
                  </div>
                </div>

                 <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Tema</span>
                    <input  class="form-control" maxlength="100" id="filter_tema_rcn_so_all" name="tema"   placeholder="" type="text"  >
                  </div>
                </div>


           
               <div class="row">
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Tanggal Pelaksanaan</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_pl_date_from_rcn_so_all" name="filter_pl_date_from_rcn_so_all" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_pl_date_to_rcn_so_all" name="filter_pl_date_to_rcn_so_all" maxlength="50"  placeholder="" type="text"  >
                  </div> 
             
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_from_rcn_so_all" name="filter_date_from_rcn_so_all" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_to_rcn_so_all" name="filter_date_to_rcn_so_all" maxlength="50"  placeholder="" type="text"  >
                  </div> 
              </div> 

   

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" >
                    <span class="label">Provinsi</span> 
                    <select class="form-control" onchange="call_upt_fltr_rcn()" id="filter_provinsi_rcn_all" name="id_prov" >
                      <option value="">Pilih Provinsi</option>
                      <?php 
                        $data_shf = $gen_model->GetWhere('ms_provinsi');
                          while($list = $data_shf->FetchRow()){
                            foreach($list as $key=>$val){
                                        $key=strtolower($key);
                                        $$key=$val;
                                      }  
                        ?>
                      <option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
                    <span class="label">Nama UPT</span> 
                    <select class="form-control"  id="filter_upt_rcn_all" name="loket_upt" >
                      <option value="">Pilih UPT</option>
                    </select>
                  </div> 
                </div> 
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_rencana_sosialisasi_filter_all')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_rencana_sosialisasi_all()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
  function call_upt_fltr_rcn(){
    var prov = $("#filter_provinsi_rcn_all").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#filter_upt_rcn_all").html(msg);        
                }
             }
          }); 
  }
    function filter_rencana_sosialisasi_all() {
      $("#filter_rencana_so_all").html("");
      var filter_tema            = $("#filter_tema_rcn_so_all").val();
      var filter_tempat          = $("#filter_tempat_rcn_so_all").val();
      var filter_provinsi        = $("#filter_provinsi_rcn_all").val();
      var filter_upt             = $("#filter_upt_rcn_all").val();
      var filter_kegiatan        = $("#filter_kegiatan_rcn_so_all").val();

      var filter_date_from       = $("#filter_date_from_rcn_so_all").val();
      var filter_date_to         = $("#filter_date_to_rcn_so_all").val();
      var filter_plk_date_from   = $("#filter_pl_date_from_rcn_so_all").val();
      var filter_plk_date_to     = $("#filter_pl_date_to_rcn_so_all").val();

      var param="";
      if(filter_kegiatan){
        param += " and rcn.jenis_kegiatan ='"+filter_kegiatan.trim()+"' ";
      }if(filter_tema){
        param += " and rcn.tema like '%"+filter_tema.trim()+"%' ";
      }if(filter_tempat){
        param += " and rcn.tempat like '%"+filter_tempat.trim()+"%' ";
      }if(filter_provinsi){
        param += " and rcn.id_prov ='"+filter_provinsi.trim()+"' ";
      }if(filter_upt){
        param += " and rcn.kode_upt ='"+filter_upt.trim()+"' ";
      }
      
       //Tanggal Pelaksanaan
      if(filter_plk_date_from && filter_plk_date_to){
        date1 = new Date(filter_plk_date_from); 
        date2 = new Date(filter_plk_date_to);

        var date1   = ((new Date(filter_plk_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_plk_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(rcn.tgl_pelaksanaan, '%Y-%m-%d') between '"+date_to_default(filter_plk_date_from.trim())+"' and  '"+date_to_default(filter_plk_date_to.trim())+"'";
        }
      }
      else if(filter_plk_date_from){
           param += " and DATE_FORMAT(rcn.tgl_pelaksanaan, '%Y-%m-%d')='"+date_to_default(filter_plk_date_from.trim())+"' ";
      }
      else if(filter_plk_date_to){
          param += " and DATE_FORMAT(rcn.tgl_pelaksanaan, '%Y-%m-%d')='"+date_to_default(filter_plk_date_to.trim())+"' ";
      }

      //Created Date
      if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += "  and DATE_FORMAT(rcn.created_date, '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(rcn.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(rcn.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_to.trim())+"' ";
      }



      $("#RencanaSosialisasiPencarian_all").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>sosialisasi_bimtek_rencana_sosialisasi/table_all_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_rencana_so_all').html(data);
              }
          });
      $('#Filter_RencanaSosialisasiAll').modal('hide');
   }  
</script>

<!-- Filter Rencana Sosialisasi Detail -->
<div id="Filter_RencanaSosialisasiDetail" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_rencana_sosialisasi_filter" method="POST">
                <div class="row">
                  
                  <div class="col-md-12 form-group form-box col-xs-6" >
                    <span class="label">Jenis Kegiatan</span>
                    <select class="form-control"  id="filter_kegiatan_rcn_so" name="kegiatan" >
                      <option value="">Pilih Jenis Kegiatan</option>
                      <?php 
                        $data_mtr = $gen_model->GetWhere('ms_kegiatan');
                          while($list = $data_mtr->FetchRow()){
                            foreach($list as $key=>$val){
                                        $key=strtolower($key);
                                        $$key=$val;
                                      }  
                        ?>
                      <option value="<?php echo $kode_kegiatan ?>"><?php echo $kegiatan ?></option>
                      <?php } ?>
                    </select>
                 </div>
                 </div>
                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Tempat</span>
                    <input  class="form-control" maxlength="100" id="filter_tempat_rcn_so" name="tempat"   placeholder="" type="text" >
                  </div>
                </div>

                 <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Tema</span>
                    <input  class="form-control" maxlength="100" id="filter_tema_rcn_so" name="tema"   placeholder="" type="text"  >
                  </div>
                </div>

                

               <div class="row">
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Tanggal Pelaksanaan</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_pl_date_from_rcn_so" name="filter_pl_date_from_rcn_so" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_pl_date_to_rcn_so" name="filter_pl_date_to_rcn_so" maxlength="50"  placeholder="" type="text"  >
                  </div> 
             
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_from_rcn_so" name="filter_date_from_rcn_so" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_to_rcn_so" name="filter_date_to_rcn_so" maxlength="50"  placeholder="" type="text"  >
                  </div> 
              </div> 
   
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_rencana_sosialisasi_filter')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_rencana_sosialisasi_detail()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
    function filter_rencana_sosialisasi_detail() {
      $("#filter_rcn_so_detail").html("");
      var filter_tema            = $("#filter_tema_rcn_so").val();
      var filter_tempat          = $("#filter_tempat_rcn_so").val();
      var filter_provinsi        = $("#filter_provinsi_rencana_so").val();
      var filter_upt             = $("#filter_upt_rencana_so").val();
      var filter_kegiatan        = $("#filter_kegiatan_rcn_so").val();

      var filter_date_from       = $("#filter_date_from_rcn_so").val();
      var filter_date_to         = $("#filter_date_to_rcn_so").val();
      var filter_plk_date_from   = $("#filter_pl_date_from_rcn_so").val();
      var filter_plk_date_to     = $("#filter_pl_date_to_rcn_so").val();


      var param="";
      if(filter_kegiatan){
        param += " and rcn.jenis_kegiatan ='"+filter_kegiatan.trim()+"' ";
      }if(filter_tema){
        param += " and rcn.tema like '%"+filter_tema.trim()+"%' ";
      }if(filter_tempat){
        param += " and rcn.tempat like '%"+filter_tempat.trim()+"%' ";
      }if(filter_provinsi){
        //param += " and rcn.id_prov ='"+filter_provinsi.trim()+"' ";
      }if(filter_upt){
        //param += " and rcn.kode_upt ='"+filter_upt.trim()+"' ";
      }


       //Tanggal Pelaksanaan
      if(filter_plk_date_from && filter_plk_date_to){
        date1 = new Date(filter_plk_date_from); 
        date2 = new Date(filter_plk_date_to);

        var date1   = ((new Date(filter_plk_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_plk_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(rcn.tgl_pelaksanaan, '%Y-%m-%d') between '"+date_to_default(filter_plk_date_from.trim())+"' and  '"+date_to_default(filter_plk_date_to.trim())+"'";
        }
      }
      else if(filter_plk_date_from){
           param += " and DATE_FORMAT(rcn.tgl_pelaksanaan, '%Y-%m-%d')='"+date_to_default(filter_plk_date_from.trim())+"' ";
      }
      else if(filter_plk_date_to){
          param += " and DATE_FORMAT(rcn.tgl_pelaksanaan, '%Y-%m-%d')='"+date_to_default(filter_plk_date_to.trim())+"' ";
      }

      //Created Date
      if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += "  and DATE_FORMAT(rcn.created_date, '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(rcn.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(rcn.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_to.trim())+"' ";
      }

      $("#RencanaSosialisasiPencarian").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>sosialisasi_bimtek_rencana_sosialisasi/table_detail_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_rcn_so_detail').html(data);
              }
          });
      $('#Filter_RencanaSosialisasiDetail').modal('hide');
   }  
</script>


<!-- Filter Monev Sosialisasi All -->
<div id="Filter_MonevSosialisasiAll" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_monev_sosialisasi_filter_all" method="POST">
                <div class="row">

                  <div class="col-md-12 form-group form-box col-xs-6" >
                    <span class="label">Jenis Kegiatan</span>
                    <select class="form-control"  id="filter_kegiatan_mnv_so_all" name="kegiatan" >
                      <option value="">Pilih Jenis Kegiatan</option>
                      <?php 
                        $data_mtr = $gen_model->GetWhere('ms_kegiatan');
                          while($list = $data_mtr->FetchRow()){
                            foreach($list as $key=>$val){
                                        $key=strtolower($key);
                                        $$key=$val;
                                      }  
                        ?>
                      <option value="<?php echo $kode_kegiatan ?>"><?php echo $kegiatan ?></option>
                      <?php } ?>
                    </select>
                 </div>
                 </div>

               

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Tempat</span>
                    <input  class="form-control" maxlength="100" id="filter_tempat_mnv_so_all" name="tempat"   placeholder="" type="text" >
                  </div>
                </div>

                 <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Tema</span>
                    <input  class="form-control" maxlength="100" id="filter_tema_mnv_so_all" name="tema"   placeholder="" type="text"  >
                  </div>
                </div>

               <div class="row">
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Tanggal Pelaksanaan</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_pl_date_from_mnv_all" name="filter_pl_date_from_mnv_all" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_pl_date_to_mnv_all" name="filter_pl_date_to_mnv_all" maxlength="50"  placeholder="" type="text"  >
                  </div> 
             
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_from_mnv_all" name="filter_date_from_mnv_all" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_to_mnv_all" name="filter_date_to_mnv_all" maxlength="50"  placeholder="" type="text"  >
                  </div> 
              </div> 

   

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" >
                    <span class="label">Provinsi</span> 
                    <select class="form-control" onchange="call_upt_fltr_mnv()" id="filter_provinsi_mnv_all" name="id_prov" >
                      <option value="">Pilih Provinsi</option>
                      <?php 
                        $data_shf = $gen_model->GetWhere('ms_provinsi');
                          while($list = $data_shf->FetchRow()){
                            foreach($list as $key=>$val){
                                        $key=strtolower($key);
                                        $$key=$val;
                                      }  
                        ?>
                      <option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
                    <span class="label">Nama UPT</span> 
                    <select class="form-control"  id="filter_upt_mnv_all" name="loket_upt" >
                      <option value="">Pilih UPT</option>
                    </select>
                  </div> 
                </div> 
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_monev_sosialisasi_filter_all')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_monev_sosialisasi_all()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
  function call_upt_fltr_mnv(){
    var prov = $("#filter_provinsi_mnv_all").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#filter_upt_mnv_all").html(msg);        
                }
             }
          }); 
  }
    function filter_monev_sosialisasi_all() {
      $("#filter_monev_so_all").html("");
      var filter_kegiatan        = $("#filter_kegiatan_mnv_so_all").val();
      var filter_tema            = $("#filter_tema_mnv_so_all").val();
      var filter_tempat          = $("#filter_tempat_mnv_so_all").val();
      var filter_provinsi        = $("#filter_provinsi_mnv_all").val();
      var filter_upt             = $("#filter_upt_mnv_all").val();
     
      var filter_date_from       = $("#filter_date_from_mnv_all").val();
      var filter_date_to         = $("#filter_date_to_mnv_all").val();
      var filter_plk_date_from   = $("#filter_pl_date_from_mnv_all").val();
      var filter_plk_date_to     = $("#filter_pl_date_to_mnv_all").val();

      var param="";
      if(filter_kegiatan){
        param += " and mnv.jenis_kegiatan ='"+filter_kegiatan.trim()+"' ";
      }if(filter_tema){
        param += " and mnv.tema like '%"+filter_tema.trim()+"%' ";
      }if(filter_tempat){
        param += " and mnv.tempat like '%"+filter_tempat.trim()+"%' ";
      }if(filter_provinsi){
        param += " and mnv.id_prov ='"+filter_provinsi.trim()+"' ";
      }if(filter_upt){
        param += " and mnv.kode_upt ='"+filter_upt.trim()+"' ";
      }

        //Tanggal Pelaksanaan
      if(filter_plk_date_from && filter_plk_date_to){
        date1 = new Date(filter_plk_date_from); 
        date2 = new Date(filter_plk_date_to);

        var date1   = ((new Date(filter_plk_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_plk_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(mnv.tgl_pelaksanaan, '%Y-%m-%d') between '"+date_to_default(filter_plk_date_from.trim())+"' and  '"+date_to_default(filter_plk_date_to.trim())+"'";
        }
      }
      else if(filter_plk_date_from){
           param += " and DATE_FORMAT(mnv.tgl_pelaksanaan, '%Y-%m-%d')='"+date_to_default(filter_plk_date_from.trim())+"' ";
      }
      else if(filter_plk_date_to){
          param += " and DATE_FORMAT(mnv.tgl_pelaksanaan, '%Y-%m-%d')='"+date_to_default(filter_plk_date_to.trim())+"' ";
      }

      //Created Date
      if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(mnv.created_date, '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(mnv.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(mnv.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_to.trim())+"' ";
      }

      $("#MonevSosialisasiPencarian_all").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>sosialisasi_bimtek_monev_sosialisasi/table_all_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_monev_so_all').html(data);
              }
          });
      $('#Filter_MonevSosialisasiAll').modal('hide');
   }  
</script>

<!-- Filter Monev Sosialisasi Detail -->
<div id="Filter_MonevSosialisasiDetail" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_monev_sosialisasi_filter" method="POST">
                <div class="row">
                 
                  <div class="col-md-12 form-group form-box col-xs-6" >
                    <span class="label">Jenis Kegiatan</span>
                    <select class="form-control"  id="filter_kegiatan_mnv_so" name="kegiatan" >
                      <option value="">Pilih Jenis Kegiatan</option>
                      <?php 
                        $data_mtr = $gen_model->GetWhere('ms_kegiatan');
                          while($list = $data_mtr->FetchRow()){
                            foreach($list as $key=>$val){
                                        $key=strtolower($key);
                                        $$key=$val;
                                      }  
                        ?>
                      <option value="<?php echo $kode_kegiatan ?>"><?php echo $kegiatan ?></option>
                      <?php } ?>
                    </select>
                 </div>
                 </div>

                <div class="row"> 
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Tanggal Pelaksanaan</span>
                    <input  class="form-control tgl" id="filter_tgl_pelaksanaan_mnv_so" name="tgl_pelaksanaan"   placeholder="" type="text" >
                  </div>
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Tempat</span>
                    <input  class="form-control" maxlength="100" id="filter_tempat_mnv_so" name="tempat"   placeholder="" type="text" >
                  </div>
                </div>

                 <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Tema</span>
                    <input  class="form-control" maxlength="100" id="filter_tema_mnv_so" name="tema"   placeholder="" type="text"  >
                  </div>
                </div>

                

              <div class="row">
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Tanggal Pelaksanaan</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_pl_date_from_mnv" name="filter_pl_date_from_mnv" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_pl_date_to_mnv" name="filter_pl_date_to_mnv" maxlength="50"  placeholder="" type="text"  >
                  </div> 
             
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_from_mnv" name="filter_date_from_mnv" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_to_mnv" name="filter_date_to_mnv" maxlength="50"  placeholder="" type="text"  >
                  </div> 
              </div> 
   
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_monev_sosialisasi_filter')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_monev_sosialisasi_detail()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
    function filter_monev_sosialisasi_detail() {
      $("#filter_mnv_so_detail").html("");
      var filter_kegiatan        = $("#filter_kegiatan_mnv_so").val();
      var filter_tema            = $("#filter_tema_mnv_so").val();
      var filter_tempat          = $("#filter_tempat_mnv_so").val();
      var filter_provinsi        = $("#filter_provinsi_monev_so").val();
      var filter_upt             = $("#filter_upt_monev_so").val();

      var filter_date_from       = $("#filter_date_from_mnv").val();
      var filter_date_to         = $("#filter_date_to_mnv").val();
      var filter_plk_date_from   = $("#filter_pl_date_from_mnv").val();
      var filter_plk_date_to     = $("#filter_pl_date_to_mnv").val();


      var param="";
      if(filter_kegiatan){
        param += " and mnv.jenis_kegiatan ='"+filter_kegiatan.trim()+"' ";
      }if(filter_tema){
        param += " and mnv.tema like '%"+filter_tema.trim()+"%' ";
      }if(filter_tempat){
        param += " and mnv.tempat like '%"+filter_tempat.trim()+"%' ";
      }if(filter_provinsi){
        //param += " and mnv.id_prov ='"+filter_provinsi.trim()+"' ";
      }if(filter_upt){
        //param += " and mnv.kode_upt ='"+filter_upt.trim()+"' ";
      }

      //Tanggal Pelaksanaan
      if(filter_plk_date_from && filter_plk_date_to){
        date1 = new Date(filter_plk_date_from); 
        date2 = new Date(filter_plk_date_to);

        var date1   = ((new Date(filter_plk_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_plk_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(mnv.tgl_pelaksanaan, '%Y-%m-%d') between '"+date_to_default(filter_plk_date_from.trim())+"' and  '"+date_to_default(filter_plk_date_to.trim())+"'";
        }
      }
      else if(filter_plk_date_from){
           param += " and DATE_FORMAT(mnv.tgl_pelaksanaan, '%Y-%m-%d')='"+date_to_default(filter_plk_date_from.trim())+"' ";
      }
      else if(filter_plk_date_to){
          param += " and DATE_FORMAT(mnv.tgl_pelaksanaan, '%Y-%m-%d')='"+date_to_default(filter_plk_date_to.trim())+"' ";
      }

      //Created Date
      if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(mnv.created_date, '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(mnv.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(mnv.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_to.trim())+"' ";
      }

      $("#MonevSosialisasiPencarian").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>sosialisasi_bimtek_monev_sosialisasi/table_detail_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_mnv_so_detail').html(data);
              }
          });
      $('#Filter_MonevSosialisasiDetail').modal('hide');
   }  
</script>

<!-- Filter Galeri All -->
<div id="Filter_GaleriAll" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_galeri_filter_all" method="POST">
                <div class="row">
                 
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Kegiatan</span>
                    <input  class="form-control" maxlength="255" id="filter_kegiatan_galeri_all" name="kegiatan"   placeholder="" type="text" >
                  </div>
                 </div>


               <div class="row">
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_from_galeri_all" name="filter_date_from_galeri_all" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_to_galeri_all" name="filter_date_to_galeri_all" maxlength="50"  placeholder="" type="text"  >
                  </div> 
              </div> 

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" >
                    <span class="label">Provinsi</span> 
                    <select class="form-control" onchange="call_upt_fltr_glr()" id="filter_galeri_provinsi_all" name="id_prov" >
                      <option value="">Pilih Provinsi</option>
                      <?php 
                        $data_shf = $gen_model->GetWhere('ms_provinsi');
                          while($list = $data_shf->FetchRow()){
                            foreach($list as $key=>$val){
                                        $key=strtolower($key);
                                        $$key=$val;
                                      }  
                        ?>
                      <option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
                    <span class="label">Nama UPT</span> 
                    <select class="form-control"  id="filter_galeri_upt_all" name="loket_upt" >
                      <option value="">Pilih UPT</option>
                    </select>
                  </div> 
                </div> 
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_galeri_filter_all')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_galeri_all()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
  function call_upt_fltr_glr(){
    var prov = $("#filter_galeri_provinsi_all").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#filter_galeri_upt_all").html(msg);        
                }
             }
          }); 
  }
    function filter_galeri_all() {
      $("#filter_galeri_all").html("");
      var filter_tgl        = $("#filter_tgl_galeri_all").val();
      var filter_kegiatan   = $("#filter_kegiatan_galeri_all").val();
      var filter_provinsi   = $("#filter_galeri_provinsi_all").val();
      var filter_upt        = $("#filter_galeri_upt_all").val();
      var filter_date_from       = $("#filter_date_from_galeri_all").val();
      var filter_date_to         = $("#filter_date_to_galeri_all").val();

      var param="";
      if(filter_kegiatan){
        param += " and gl.kegiatan like '%"+filter_kegiatan.trim()+"%' ";
      }if(filter_provinsi){
        param += " and gl.id_prov ='"+filter_provinsi.trim()+"' ";
      }if(filter_upt){
        param += " and gl.kode_upt ='"+filter_upt.trim()+"' ";
      }

       if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(gl.created_date, '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(gl.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(gl.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_to.trim())+"' ";
      }

      $("#GaleriPencarian_all").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>sosialisasi_bimtek_galeri/table_all_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_galeri_all').html(data);
              }
          });
      $('#Filter_GaleriAll').modal('hide');
   }  
</script>

<!-- Filter Galeri Detail -->
<div id="Filter_GaleriDetail" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_galeri_filter" method="POST">
                <div class="row">
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Kegiatan</span>
                    <input  class="form-control" maxlength="255" id="filter_kegiatan_galeri" name="kegiatan"   placeholder="" type="text" >
                  </div>
                 </div>

                 <div class="row">
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_from_galeri" name="filter_date_from_galeri" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_to_galeri" name="filter_date_to_galeri" maxlength="50"  placeholder="" type="text"  >
                  </div> 
              </div> 

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_galeri_filter')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_galeri_detail()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
    function filter_galeri_detail() {

      $("#filter_galeri_detail").html("");
      var filter_tgl        = $("#filter_tgl_galeri").val();
      var filter_kegiatan   = $("#filter_kegiatan_galeri").val();
      var filter_provinsi   = $("#filter_provinsi_galeri").val();
      var filter_upt        = $("#filter_upt_galeri").val();
      var filter_date_from  = $("#filter_date_from_galeri").val();
      var filter_date_to    = $("#filter_date_to_galeri").val();


      var param="";
      if(filter_kegiatan){
        param += " and gl.kegiatan like '%"+filter_kegiatan.trim()+"%' ";
      }if(filter_provinsi){
        //param += " and gl.id_prov ='"+filter_provinsi.trim()+"' ";
      }if(filter_upt){
        //param += " and gl.kode_upt ='"+filter_upt.trim()+"' ";
      }

      if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(gl.created_date, '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(gl.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(gl.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_to.trim())+"' ";
      }

      $("#GaleriPencarian").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>sosialisasi_bimtek_galeri/table_detail_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_galeri_detail').html(data);
              }
          });
      $('#Filter_GaleriDetail').modal('hide');
   }  
</script>

<!-- Import -->

<!-- Import Bahan Sosialisasi All -->
<div id="Import_BhnSosialisasiAll" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_BhnSosialisasiAll" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_bahan_sosialisasi_all.xls" download>Download Sample File</a><br/><br/>
          <span style="font-size: 14px;">File Import Membutuhkan Kode UPT, Kode Provinsi, & Kode Materi</span><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>materi">List Materi</a><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>upt">List Data UPT & Provinsi</a>
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<!-- Import Bahan Sosialisasi Detail -->
<div id="Import_BhnSosialisasi_Detail" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_BhnSosialisasi_Detail" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
          <input type="hidden" id="my_id_prov_bhn_so" name="id_prov">
          <input type="hidden" id="my_id_upt_bhn_so" name="id_upt">
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_bahan_sosialisasi.xls" download>Download Sample File</a><br/><br/>
          <span style="font-size: 14px;">File Import Membutuhkan Kode Materi</span><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>materi">List Materi</a>
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>


<!-- Import Rencana Sosialisasi All -->
<div id="Import_RencanaSosialisasiAll" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_RencanaSosialisasiAll" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_rencana_sosialisasi_all.xls" download>Download Sample File</a><br/><br/>
          <span style="font-size: 14px;">File Import Membutuhkan Kode UPT, Kode Provinsi, & Kode Kegiatan</span><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>kegiatan">List Kegiatan</a><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>upt">List Data UPT & Provinsi</a>
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<!-- Import Rencana Sosialisasi Detail -->
<div id="Import_RencanaSosialisasi_Detail" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_RencanaSosialisasi_Detail" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
          <input type="hidden" id="my_id_prov_rcn_so" name="id_prov">
          <input type="hidden" id="my_id_upt_rcn_so" name="id_upt">
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_rencana_sosialisasi.xls" download>Download Sample File</a><br/><br/>
          <span style="font-size: 14px;">File Import Membutuhkan Kode Kegiatan</span><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>kegiatan">List Kegiatan</a>
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>


<!-- Import Monev Sosialisasi All -->
<div id="Import_MonevSosialisasiAll" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_MonevSosialisasiAll" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_monev_sosialisasi_all.xls" download>Download Sample File</a><br/><br/>
          <span style="font-size: 14px;">File Import Membutuhkan Kode UPT, Kode Provinsi, Kode Kegiatan</span><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>kegiatan">List Kegiatan</a><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>upt">List Data UPT & Provinsi</a>
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<!-- Import Monev Sosialisasi Detail -->
<div id="Import_MonevSosialisasi_Detail" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_MonevSosialisasi_Detail" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
          <input type="hidden" id="my_id_prov_monev_so" name="id_prov">
          <input type="hidden" id="my_id_upt_monev_so" name="id_upt">
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_monev_sosialisasi.xls" download>Download Sample File</a><br/><br/>
           <span style="font-size: 14px;">File Import Membutuhkan Kode Kegiatan</span><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>kegiatan">List Kegiatan</a>
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>


<!-- Import Galeri All -->
<div id="Import_GaleriAll" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_GaleriAll" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_galeri_all.xls" download>Download Sample File</a><br/><br/>
          <span style="font-size: 14px;">File Import Membutuhkan Kode UPT & Kode Provinsi</span><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>upt">List Data UPT & Provinsi</a>
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<!-- Import Galeri Detail -->
<div id="Import_Galeri_Detail" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_Galeri_Detail" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
          <input type="hidden" id="my_id_prov_galeri_so" name="id_prov">
          <input type="hidden" id="my_id_upt_galeri_so" name="id_upt">
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_galeri.xls" download>Download Sample File</a><br/>
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<script type="text/javascript">
  //Import Bahan Sosialisasi
   $("#DoImport_BhnSosialisasiAll").on("submit", function (event) {
    event.preventDefault();
        do_act('DoImport_BhnSosialisasiAll','sosialisasi_bimtek_bahan_sosialisasi/import_all','no_refresh','Import File Bahan Sosialisasi','Apakah anda ingin import file Bahan Sosialisasi ini ?','info','refresh_table');
    });

   //Import Bahan Sosialisasi Detail
   $("#DoImport_BhnSosialisasi_Detail").on("submit", function (event) {
    event.preventDefault();
        var fltr = $("#filter_provinsi_bhn_so").val();
        var fltr_upt = $("#filter_upt_bhn_so").val();
        $("#my_id_prov_bhn_so").val(fltr);
        $("#my_id_upt_bhn_so").val(fltr_upt);
        do_act('DoImport_BhnSosialisasi_Detail','sosialisasi_bimtek_bahan_sosialisasi/import','no_refresh','Import File Bahan Sosialisasi','Apakah anda ingin import file Bahan Sosialisasi ini ?','info','refresh_table');
    });


   //Import Rencana Sosialisasi
   $("#DoImport_RencanaSosialisasiAll").on("submit", function (event) {
    event.preventDefault();
        do_act('DoImport_RencanaSosialisasiAll','sosialisasi_bimtek_rencana_sosialisasi/import_all','no_refresh','Import File Rencana Sosialisasi','Apakah anda ingin import file Rencana Sosialisasi ini ?','info','refresh_table');
    });

  //Import Rencana Sosialisasi Detail
   $("#DoImport_RencanaSosialisasi_Detail").on("submit", function (event) {
    event.preventDefault();
        var fltr = $("#filter_provinsi_rencana_so").val();
        var fltr_upt = $("#filter_upt_rencana_so").val();
        $("#my_id_prov_rcn_so").val(fltr);
        $("#my_id_upt_rcn_so").val(fltr_upt);
        do_act('DoImport_RencanaSosialisasi_Detail','sosialisasi_bimtek_rencana_sosialisasi/import','no_refresh','Import File Rencana Sosialisasi','Apakah anda ingin import file Rencana Sosialisasi ini ?','info','refresh_table');
    });


   //Import Monev Sosialisasi
   $("#DoImport_MonevSosialisasiAll").on("submit", function (event) {
    event.preventDefault();
        do_act('DoImport_MonevSosialisasiAll','sosialisasi_bimtek_monev_sosialisasi/import_all','no_refresh','Import File Monev Sosialisasi','Apakah anda ingin import file Monev Sosialisasi ini ?','info','refresh_table');
    });

  //Import Monev Sosialisasi Detail
   $("#DoImport_MonevSosialisasi_Detail").on("submit", function (event) {
    event.preventDefault();
        var fltr = $("#filter_provinsi_monev_so").val();
        var fltr_upt = $("#filter_upt_monev_so").val();
        $("#my_id_prov_monev_so").val(fltr);
        $("#my_id_upt_monev_so").val(fltr_upt);
        do_act('DoImport_MonevSosialisasi_Detail','sosialisasi_bimtek_monev_sosialisasi/import','no_refresh','Import File Monev Sosialisasi','Apakah anda ingin import file Monev Sosialisasi ini ?','info','refresh_table');
    });


   //Import Galeri
   $("#DoImport_GaleriAll").on("submit", function (event) {
    event.preventDefault();
        do_act('DoImport_GaleriAll','sosialisasi_bimtek_galeri/import_all','no_refresh','Import File Galeri','Apakah anda ingin import file Galeri ini ?','info','refresh_table');
    });

  //Import Galeri Detail
   $("#DoImport_Galeri_Detail").on("submit", function (event) {
    event.preventDefault();
        var fltr = $("#filter_provinsi_galeri").val();
        var fltr_upt = $("#filter_upt_galeri").val();
        $("#my_id_prov_galeri_so").val(fltr);
        $("#my_id_upt_galeri_so").val(fltr_upt);
        do_act('DoImport_Galeri_Detail','sosialisasi_bimtek_galeri/import','no_refresh','Import File Galeri','Apakah anda ingin import file Galeri ini ?','info','refresh_table');
    });
</script>