<!-- Reject Validasi Data -->
<div id="Reject_ValidasiData" class="modal fade call_modal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle">Reject</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="pelayanan_reject" method="POST" autocomplete="off">
                <div class="row">
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Keterangan</span> 
                    <input type="text" name="keterangan" id="reject_ket_validasi_data" class="form-control">
                  </div>
                 </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="reject_data('kode_validasi_data','validasi_data/reject','no_refresh','refresh_table','reject_ket_validasi_data')" class="btn btn-danger btn-sm">Reject</button>
        </div>
      </div>
    </div>
</div>

            <!-- Modal Filter -->
<!-- Filter Validasi Data All -->
<div id="Filter_ValidasiDataAll" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_validasi_data_filter_all" method="POST">
                <div class="row">
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">No SPP</span> 
                    <input  class="form-control" maxlength="255" id="filter_data_sims_all" name="data_sims"   placeholder="" type="text" >
                  </div>
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Data Sampling</span> 
                    <input  class="form-control" maxlength="255" id="filter_data_sampling_all" name="data_sampling"   placeholder="" type="text" >
                  </div>
                </div>

                <div class="col-md-12 form-group form-box col-xs-12">
                    <center style="font-weight:bold;font-weight:50px">Data Hasil Inspeksi</center>
                </div>
                <div class="row">
                    <div class="col-md-3 form-group form-box col-xs-12">
                      <span class="label">Sesuai ISR</span>
                      <input  class="form-control" maxlength="255" id="filter_sesuai_isr_all" name="sesuai_isr"   placeholder="" type="text" >
                    </div>
                    <div class="col-md-3 form-group form-box col-xs-12">
                      <span class="label">Tidak Sesuai ISR</span> 
                      <input  class="form-control" maxlength="255" id="filter_tdk_sesuai_isr_all" name="tdk_sesuai_isr"   placeholder="" type="text" >
                    </div>
                    <div class="col-md-3 form-group form-box col-xs-12">
                      <span class="label">Tidak Aktif</span> 
                      <input  class="form-control" maxlength="255" id="filter_tidak_aktif_all" name="tidak_aktif"   placeholder="" type="text" >
                    </div>
                    <div class="col-md-3 form-group form-box col-xs-12">
                      <span class="label">Proses ISR</span> 
                      <input  class="form-control" maxlength="255" id="filter_proses_isr_all" name="proses_isr"   placeholder="" type="text" >
                    </div>
                </div>

                <div class="col-md-12 form-group form-box col-xs-12">
                    <center style="font-weight:bold;font-weight:50px">Tindak Lanjut Hasil Inspeksi</center>
                </div>
                <div class="row">
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Sudah Ditindaklanjuti</span> 
                    <input  class="form-control" maxlength="255" id="filter_sdh_ditindaklanjuti_all" name="sdh_ditindaklanjuti"   placeholder="" type="text" >
                  </div>
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Belum Ditindaklanjuti</span> 
                    <input  class="form-control" maxlength="255" id="filter_blm_ditindaklanjuti_all" name="blm_ditindaklanjuti"   placeholder="" type="text" >
                  </div>
                </div>

                <div class="row">  
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Capaian</span> 
                    <input  class="form-control" maxlength="255" id="filter_capaian_all" name="capaian"   placeholder="" type="text" >
                  </div>

                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Keterangan</span> 
                    <textarea  class="form-control"  id="filter_keterangan_all" name="keterangan" ></textarea>
                  </div>
                </div>
                
                 <div class="row">
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_from_all" name="filter_date_from" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_to_all" name="filter_date_to" maxlength="50"  placeholder="" type="text"  >
                  </div> 
               </div> 


                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" >
                    <span class="label">Provinsi</span> 
                    <select class="form-control" onchange="call_upt_fltr()" id="filter_provinsi_all" name="id_prov" >
                      <option value="">Pilih Provinsi</option>
                      <?php 
                        $data_shf = $gen_model->GetWhere('ms_provinsi');
                          while($list = $data_shf->FetchRow()){
                            foreach($list as $key=>$val){
                                        $key=strtolower($key);
                                        $$key=$val;
                                      }  
                        ?>
                      <option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
                    <span class="label">Nama UPT</span> 
                    <select class="form-control"  id="filter_upt_all" name="loket_upt" >
                      <option value="">Pilih UPT</option>
                    </select>
                  </div> 
                </div> 
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_validasi_data_filter_all')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_validasi_data_all()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
  function call_upt_fltr(){
    var prov = $("#filter_provinsi_all").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#filter_upt_all").html(msg);        
                }
             }
          }); 
  }
    function filter_validasi_data_all() {
      $("#filter_validasi_data_all").html("");
      var filter_data_sims          = $("#filter_data_sims_all").val();
      var filter_data_sampling      = $("#filter_data_sampling_all").val();
      var filter_sesuai_isr         = $("#filter_sesuai_isr_all").val();
      var filter_tdk_sesuai_isr     = $("#filter_tdk_sesuai_isr_all").val();
      var filter_tidak_aktif        = $("#filter_tidak_aktif_all").val();
      var filter_proses_isr         = $("#filter_proses_isr_all").val();
      var filter_sdh_ditindaklanjuti   = $("#filter_sdh_ditindaklanjuti_all").val();
      var filter_blm_ditindaklanjuti   = $("#filter_blm_ditindaklanjuti_all").val();
      var filter_capaian               = $("#filter_capaian_all").val();
      var filter_keterangan            = $("#filter_keterangan_all").val();
      var filter_date_from             = $("#filter_date_from_all").val();
      var filter_date_to               = $("#filter_date_to_all").val();
      var filter_provinsi              = $("#filter_provinsi_all").val();
      var filter_upt                   = $("#filter_upt_all").val();

      var param="";
      if(filter_data_sims){
        param += " and vld.data_sims like '%"+filter_data_sims.trim()+"%' ";
      }if(filter_data_sampling){
        param += " and vld.data_sampling like '%"+filter_data_sampling.trim()+"%' ";
      }if(filter_sesuai_isr){
        param += " and vld.sesuai_isr like '%"+filter_sesuai_isr.trim()+"%' ";
      }if(filter_tdk_sesuai_isr){
        param += " and vld.tdk_sesuai_isr like '%"+filter_tdk_sesuai_isr.trim()+"%' ";
      }if(filter_tidak_aktif){
        param += " and vld.tidak_aktif like '%"+filter_tidak_aktif.trim()+"%' ";
      }if(filter_proses_isr){
        param += " and vld.proses_isr like '%"+filter_proses_isr.trim()+"%' ";
      }if(filter_sdh_ditindaklanjuti){
        param += " and vld.sudah_ditindaklanjuti like '%"+filter_sdh_ditindaklanjuti.trim()+"%' ";
      }if(filter_blm_ditindaklanjuti){
        param += " and vld.belum_ditindaklanjuti like '%"+filter_blm_ditindaklanjuti.trim()+"%' ";
      }if(filter_capaian){
        param += " and vld.capaian like '%"+filter_capaian.trim()+"%' ";
      }if(filter_keterangan){
        param += " and vld.keterangan like '%"+filter_keterangan.trim()+"%' ";
      }if(filter_provinsi){
        param += " and vld.id_prov ='"+filter_provinsi.trim()+"' ";
      }if(filter_upt){
        param += " and vld.kode_upt ='"+filter_upt.trim()+"' ";
      }


       if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(vld.created_date, '%Y-%m-%d')  between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(vld.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(vld.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_to.trim())+"' ";
      }
      
      $("#ValidasiDataPencarian_all").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>validasi_data/table_all_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_validasi_data_all').html(data);
              }
          });
      $('#Filter_ValidasiDataAll').modal('hide');
   }  
</script>


<!-- Filter Validasi Data Detail -->
<div id="Filter_ValidasiDataDetail" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_validasi_data_filter" method="POST">
                <div class="row">
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Data SIMS</span> 
                    <input  class="form-control" maxlength="255" id="filter_data_sims" name="data_sims"   placeholder="" type="text" >
                  </div>
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Data Sampling</span> 
                    <input  class="form-control" maxlength="255" id="filter_data_sampling" name="data_sampling"   placeholder="" type="text" >
                  </div>
                </div>

                <div class="col-md-12 form-group form-box col-xs-12">
                    <center style="font-weight:bold;font-weight:50px">Data Hasil Inspeksi</center>
                </div>
                <div class="row">
                    <div class="col-md-3 form-group form-box col-xs-12">
                      <span class="label">Sesuai ISR</span>
                      <input  class="form-control" maxlength="255" id="filter_sesuai_isr" name="sesuai_isr"   placeholder="" type="text" >
                    </div>
                    <div class="col-md-3 form-group form-box col-xs-12">
                      <span class="label">Tidak Sesuai ISR</span> 
                      <input  class="form-control" maxlength="255" id="filter_tdk_sesuai_isr" name="tdk_sesuai_isr"   placeholder="" type="text" >
                    </div>
                    <div class="col-md-3 form-group form-box col-xs-12">
                      <span class="label">Tidak Aktif</span> 
                      <input  class="form-control" maxlength="255" id="filter_tidak_aktif" name="tidak_aktif"   placeholder="" type="text" >
                    </div>
                    <div class="col-md-3 form-group form-box col-xs-12">
                      <span class="label">Proses ISR</span> 
                      <input  class="form-control" maxlength="255" id="filter_proses_isr" name="proses_isr"   placeholder="" type="text" >
                    </div>
                </div>

                <div class="col-md-12 form-group form-box col-xs-12">
                    <center style="font-weight:bold;font-weight:50px">Tindak Lanjut Hasil Inspeksi</center>
                </div>
                <div class="row">
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Sudah Ditindaklanjuti</span> 
                    <input  class="form-control" maxlength="255" id="filter_sdh_ditindaklanjuti" name="sdh_ditindaklanjuti"   placeholder="" type="text" >
                  </div>
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Belum Ditindaklanjuti</span> 
                    <input  class="form-control" maxlength="255" id="filter_blm_ditindaklanjuti" name="blm_ditindaklanjuti"   placeholder="" type="text" >
                  </div>
                </div>

                <div class="row">  
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Capaian</span> 
                    <input  class="form-control" maxlength="255" id="filter_capaian" name="capaian"   placeholder="" type="text" >
                  </div>

                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Keterangan</span> 
                    <textarea  class="form-control"  id="filter_keterangan" name="keterangan" ></textarea>
                  </div>
                </div>
                
                 <div class="row">
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_from" name="filter_date_from" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_to" name="filter_date_to" maxlength="50"  placeholder="" type="text"  >
                  </div> 
               </div> 

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_validasi_data_filter')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_validasi_data()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
    function filter_validasi_data() {
      $("#filter_validasi_data").html("");
      var filter_data_sims          = $("#filter_data_sims").val();
      var filter_data_sampling      = $("#filter_data_sampling").val();
      var filter_sesuai_isr         = $("#filter_sesuai_isr").val();
      var filter_tdk_sesuai_isr     = $("#filter_tdk_sesuai_isr").val();
      var filter_tidak_aktif        = $("#filter_tidak_aktif").val();
      var filter_proses_isr         = $("#filter_proses_isr").val();
      var filter_sdh_ditindaklanjuti   = $("#filter_sdh_ditindaklanjuti").val();
      var filter_blm_ditindaklanjuti   = $("#filter_blm_ditindaklanjuti").val();
      var filter_capaian               = $("#filter_capaian").val();
      var filter_keterangan            = $("#filter_keterangan").val();
      var filter_date_from             = $("#filter_date_from").val();
      var filter_date_to               = $("#filter_date_to").val();
      var filter_provinsi              = $("#filter_provinsi_validasi_data").val();
      var filter_upt                   = $("#filter_upt_validasi_data").val();

      var param="";
      if(filter_data_sims){
        param += " and vld.data_sims like '%"+filter_data_sims.trim()+"%' ";
      }if(filter_data_sampling){
        param += " and vld.data_sampling like '%"+filter_data_sampling.trim()+"%' ";
      }if(filter_sesuai_isr){
        param += " and vld.sesuai_isr like '%"+filter_sesuai_isr.trim()+"%' ";
      }if(filter_tdk_sesuai_isr){
        param += " and vld.tdk_sesuai_isr like '%"+filter_tdk_sesuai_isr.trim()+"%' ";
      }if(filter_tidak_aktif){
        param += " and vld.tidak_aktif like '%"+filter_tidak_aktif.trim()+"%' ";
      }if(filter_proses_isr){
        param += " and vld.proses_isr like '%"+filter_proses_isr.trim()+"%' ";
      }if(filter_sdh_ditindaklanjuti){
        param += " and vld.sudah_ditindaklanjuti like '%"+filter_sdh_ditindaklanjuti.trim()+"%' ";
      }if(filter_blm_ditindaklanjuti){
        param += " and vld.belum_ditindaklanjuti like '%"+filter_blm_ditindaklanjuti.trim()+"%' ";
      }if(filter_capaian){
        param += " and vld.capaian like '%"+filter_capaian.trim()+"%' ";
      }if(filter_keterangan){
        param += " and vld.keterangan like '%"+filter_keterangan.trim()+"%' ";
      }if(filter_provinsi){
        param += " and (vld.id_prov ='"+filter_provinsi.trim()+"' or vld.id_prov is null)";
      }if(filter_upt){
        param += " and (vld.kode_upt ='"+filter_upt.trim()+"' or vld.kode_upt is null)";
      }


       if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(vld.created_date, '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(vld.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(vld.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_to.trim())+"' ";
      }
      
      $("#ValidasiDataPencarian").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>validasi_data/table_detail_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_validasi_data').html(data);
              }
          });
      $('#Filter_ValidasiDataDetail').modal('hide');
   }  
</script>


<!-- Import Validasi Data All -->
<div id="Import_ValidasiDataAll" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_ValidasiDataAll" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_validasi_data_all.xls" download>Download Sample File</a><br/><br/>
          <span style="font-size: 14px;">File Import Membutuhkan Kode UPT & Kode Provinsi</span><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>upt">List Data UPT & Provinsi</a>
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<!-- Import Validasi Data Detail -->
<div id="Import_ValidasiData_Detail" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_ValidasiData_Detail" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
          <input type="hidden" id="my_id_prov_validasi_data" name="id_prov">
          <input type="hidden" id="my_id_upt_validasi_data" name="id_upt">
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_validasi_data.xls" download>Download Sample File</a><br/>
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<script type="text/javascript">
   //Import Validasi Data
   $("#DoImport_ValidasiDataAll").on("submit", function (event) {
    event.preventDefault();
        do_act('DoImport_ValidasiDataAll','validasi_data/import_all','no_refresh','Import File Validasi Data','Apakah anda ingin import file Validasi Data ini ?','info','refresh_table');
    });

  //Import Validasi Data Detail
   $("#DoImport_ValidasiData_Detail").on("submit", function (event) {
    event.preventDefault();
        var fltr = $("#filter_provinsi_validasi_data").val();
        var fltr_upt = $("#filter_upt_validasi_data").val();
        $("#my_id_prov_validasi_data").val(fltr);
        $("#my_id_upt_validasi_data").val(fltr_upt);
        do_act('DoImport_ValidasiData_Detail','validasi_data/import','no_refresh','Import File Validasi Data','Apakah anda ingin import file Validasi Data ini ?','info','refresh_table');
    });
</script>