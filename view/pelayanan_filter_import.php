<!-- Modal Reject -->

<!-- Reject Loket Pelayanan -->
<div id="Reject_LoketPelayananDetail" class="modal fade call_modal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle">Reject</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="pelayanan_reject" method="POST" autocomplete="off">
                <div class="row">
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Keterangan</span> 
                    <input type="text" name="keterangan" id="reject_ket_pelayanan" class="form-control">
                  </div>
                 </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="reject_data('kode_pelayanan','pelayanan_loket/reject','no_refresh','refresh_table','reject_ket_pelayanan')" class="btn btn-danger btn-sm">Reject</button>
        </div>
      </div>
    </div>
</div>

<!-- Reject SPP -->
<div id="Reject_SPP" class="modal fade call_modal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle">Reject</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="spp_reject" method="POST" autocomplete="off">
                <div class="row">
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Keterangan</span> 
                    <input type="text"  name="keterangan" id="reject_ket_spp" class="form-control">
                  </div>
                 </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="reject_data('kode_spp','pelayanan_spp/reject','no_refresh','refresh_table','reject_ket_spp')" class="btn btn-danger btn-sm">Reject</button>
        </div>
      </div>
    </div>
</div>

<!-- Reject ISR -->
<div id="Reject_ISR" class="modal fade call_modal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle">Reject</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="spp_reject" method="POST" autocomplete="off">
                <div class="row">
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Keterangan</span> 
                    <input type="text"  name="keterangan" id="reject_ket_isr" class="form-control">
                  </div>
                 </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="reject_data('kode_isr','pelayanan_isr/reject','no_refresh','refresh_table','reject_ket_isr')" class="btn btn-danger btn-sm">Reject</button>
        </div>
      </div>
    </div>
</div>

<!-- Reject Revoke -->
<div id="Reject_Revoke" class="modal fade call_modal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle">Reject</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="spp_reject" method="POST" autocomplete="off">
                <div class="row">
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Keterangan</span> 
                    <input type="text"  name="keterangan" id="reject_ket_revoke" class="form-control">
                  </div>
                 </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="reject_data('kode_revoke','pelayanan_revoke/reject','no_refresh','refresh_table','reject_ket_revoke')" class="btn btn-danger btn-sm">Reject</button>
        </div>
      </div>
    </div>
</div>

<!-- Reject Piutang -->
<div id="Reject_Piutang" class="modal fade call_modal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle">Reject</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="spp_reject" method="POST" autocomplete="off">
                <div class="row">
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Keterangan</span> 
                    <input type="text"  name="keterangan" id="reject_ket_piutang" class="form-control">
                  </div>
                 </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="reject_data('kode_piutang','pelayanan_piutang/reject','no_refresh','refresh_table','reject_ket_piutang')" class="btn btn-danger btn-sm">Reject</button>
        </div>
      </div>
    </div>
</div>

<!-- Reject Unar -->
<div id="Reject_Unar" class="modal fade call_modal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle">Reject</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="spp_reject" method="POST" autocomplete="off">
                <div class="row">
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Keterangan</span> 
                    <input type="text"  name="keterangan" id="reject_ket_unar" class="form-control">
                  </div>
                 </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="reject_data('kode_unar','pelayanan_unar/reject','no_refresh','refresh_table','reject_ket_unar')" class="btn btn-danger btn-sm">Reject</button>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
  function company_loket(){
     $("#modal_loket_perusahaan").modal({backdrop:'static',keyboard: false});
      $.ajax({
          url: '<?php echo $basepath ?>pelayanan/company_loket',
          type: 'POST',
          dataType: 'html',
          success: function(result_data) {
              $("#company_table").html(result_data);
          }
      });
  }
  function company_loket_add(){
     $("#modal_loket_perusahaan_crud").modal({backdrop:'static',keyboard: false});
     $("#company_header").html("Tambah Perusahaan");
      $.ajax({
          url: '<?php echo $basepath ?>pelayanan/company_loket_form_data',
         data: "activity=do_add",
          type: 'POST',
          dataType: 'html',
          success: function(result_data) {
              $("#company_loket_form").html(result_data);
          }
      });
  } 
  function company_loket_edit(param_id){
      $("#modal_loket_perusahaan_crud").modal({backdrop:'static',keyboard: false});
      $("#company_header").html("Ubah Perusahaan");
      $.ajax({
          url: '<?php echo $basepath ?>pelayanan/company_loket_form_data',
         data: "activity=do_edit",
          type: 'POST',
          dataType: 'html',
          success: function(result_data) {
              $("#company_loket_form").html(result_data);

                 $.ajax({
                  url: '<?php echo $basepath ?>company/edit_cp_loket/'+param_id,
                  type: 'POST',
                  dataType: 'JSON',
                  success: function(data) {
                      $("#company_name").val(data.company_name);
                      $("#co_no_hp").val(data.no_hp);
                      $("#co_no_tlp").val(data.no_tlp);
                      $("#co_provinsi").val(data.id_provinsi);
                       setTimeout(function(){ 
                          call_upt();
                           
                       }, 2000);
                      $("#co_alamat").val(data.alamat);
                      $("#cmp_id_temp").val(data.company_id);
                      setTimeout(function(){ 
                        $("#co_kabkot").val(data.id_kabkot);
                      }, 3000);
                  }
              });

          }
      });
  }
  function company_loket_detail(param_id){
      $("#modal_loket_perusahaan_crud").modal({backdrop:'static',keyboard: false});
      $("#company_header").html("Detail Perusahaan");
      $.ajax({
          url: '<?php echo $basepath ?>pelayanan/company_loket_form_data',
         data: "activity=do_detail",
          type: 'POST',
          dataType: 'html',
          success: function(result_data) {
              $("#company_loket_form").html(result_data);

                 $.ajax({
                  url: '<?php echo $basepath ?>company/edit_cp_loket/'+param_id,
                  type: 'POST',
                  dataType: 'JSON',
                  success: function(data) {
                      $("#company_name").val(data.company_name);
                      $("#co_no_hp").val(data.no_hp);
                      $("#co_no_tlp").val(data.no_tlp);
                      $("#co_provinsi").val(data.id_provinsi);
                       setTimeout(function(){ 
                          call_upt();
                           
                       }, 2000);
                      $("#co_alamat").val(data.alamat);
                      $("#cmp_id_temp").val(data.company_id);
                      setTimeout(function(){ 
                        $("#co_kabkot").val(data.id_kabkot);
                      }, 3000);
                  }
              });

          }
      });
  }
  function company_loket_delete(parameter_data,param_id){
      act_delete('Hapus Perusahaan','Anda ingin menghapus Perusahaan ini ? ','warning','company/delete_cp_loket','no_refresh','refresh_table_cp_loket',window.atob(parameter_data),window.atob(param_id));
  }
  function refresh_table_cp_loket(){
   setTimeout(function(){ 
      $('#tb_company_loket').DataTable().ajax.reload();
    }, 1000);
  }
  function close_form_company_loket(){
    $("#modal_loket_perusahaan_crud").modal('hide');
    $('#tb_company_loket').DataTable().ajax.reload();
  }
</script>
<div style="z-index: 9999999;" id="modal_loket_perusahaan" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Perusahaan</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
           <div id="company_table">
           </div>
        </div>
      </div>
    </div>
</div>

<div style="z-index: 99999999;" id="modal_loket_perusahaan_crud" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> <span id="company_header">Tambah Perusahaan</span></h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" id="company_loket_form">
        </div>
      </div>
    </div>
</div>


<!-- Modal Filter -->
<!-- Filter Loket Pelayanan All -->
<div id="Filter_LoketPelayananAll" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_pelayanan_filter_all" method="POST">
                <div class="row">

                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">Nama</span> 
                    <input type="text" name="filter_nama_all" id="filter_nama_all" class="form-control">
                  </div>

                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">Jabatan</span> 
                    <input type="text" name="filter_jabatan_all"  id="filter_jabatan_all" class="form-control">
                  </div>
                 </div>

                <div class="row"> 
                  

                  <div class="col-md-12 form-group form-box col-xs-6">
                    <span class="label">Perusahaan</span> 
                    <div class="input-group">
                      <input class="form-control" id="filter_company_all" name="filter_company_all"   placeholder="" type="text"  >
                      <div class="input-group-btn">
                        <button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
                      </div>
                    </div>
                    <input type="hidden" name="filter_kode_perusahaan_all"  id="filter_kode_perusahaan_all">
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">No.Tlp</span> 
                    <input type="text"  name="filter_no_tlp_all" id="filter_no_tlp_all" class="form-control">
                  </div>

                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">No.Hp</span> 
                    <input type="text" name="filter_no_hp_all" id="filter_no_hp_all" class="form-control">
                  </div>
                </div>

                 <div class="row">
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Tanggal Pelayanan</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_pl_date_from_all" name="filter_pl_date_from_all" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_pl_date_to_all" name="filter_pl_date_to_all" maxlength="50"  placeholder="" type="text"  >
                  </div> 
             
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_from_all" name="filter_date_from_all" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_to_all" name="filter_date_to_all" maxlength="50"  placeholder="" type="text"  >
                  </div> 
              </div> 


                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" >
                    <span class="label">Provinsi</span> 
                    <select class="form-control" onchange="call_upt_fltr()" id="filter_provinsi_all" name="id_prov" >
                      <option value="">Pilih Provinsi</option>
                      <?php 
                        $data_shf = $gen_model->GetWhere('ms_provinsi');
                          while($list = $data_shf->FetchRow()){
                            foreach($list as $key=>$val){
                                        $key=strtolower($key);
                                        $$key=$val;
                                      }  
                        ?>
                      <option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
                    <span class="label">Nama UPT</span> 
                    <select class="form-control"  id="filter_upt_all" name="loket_upt" >
                      <option value="">Pilih UPT</option>
                    </select>
                  </div> 
                </div> 
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_pelayanan_filter_all')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_pelayanan_all()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
  $('#filter_company_all').typeahead({
      hint: true,
      highlight: true,
      source: function (query, process) {
          $.ajax({
              url: '<?php echo $basepath ?>ajax/reqCompany',
              type: 'POST',
              dataType: 'JSON',
              data: 'query='+$('#filter_company_all').val(),
              success: function(data) {
                  var results = data.map(function(item) {
                      var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
                      return someItem;
                  });
                  return process(results);
              }
          });
      },
      afterSelect: function(item) {
          this.$element[0].value = item.company_name
      },
      updater: function(item) {
          $('#filter_kode_perusahaan_all').val(item.company_id);
          return item;
      }
    });
  function call_upt_fltr(){
    var prov = $("#filter_provinsi_all").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#filter_upt_all").html(msg);        
                }
             }
          }); 
  }
    function filter_pelayanan_all() {
      $("#filter_pelayanan_all").html("");
      var filter_nama     = $("#filter_nama_all").val();
      var filter_jabatan  = $("#filter_jabatan_all").val();
      var filter_kode_perusahaan  = $("#filter_kode_perusahaan_all").val();
      var filter_provinsi = $("#filter_provinsi_all").val();
      var filter_upt      = $("#filter_upt_all").val();
      var filter_no_tlp   = $("#filter_no_tlp_all").val();
      var filter_no_hp    = $("#filter_no_hp_all").val();

      var filter_date_from  = $("#filter_date_from_all").val();
      var filter_date_to    = $("#filter_date_to_all").val();

      var filter_pl_date_from  = $("#filter_pl_date_from_all").val();
      var filter_pl_date_to    = $("#filter_pl_date_to_all").val();

      var param="";
      if(filter_nama){
        param += " and pl.nama_pengunjung like '%"+filter_nama.trim()+"%' ";
      } if(filter_jabatan){
        param += " and pl.jabatan_pengunjung like '%"+filter_jabatan.trim()+"%' ";
      }if(filter_kode_perusahaan){
        param += " and pl.kode_perusahaan='"+filter_kode_perusahaan.trim()+"' ";
      }if(filter_provinsi){
        param += " and pl.id_prov ='"+filter_provinsi.trim()+"' ";
      }if(filter_upt){
        param += " and pl.kode_upt ='"+filter_upt.trim()+"' ";
      }if(filter_upt){
        param += " and pl.kode_upt ='"+filter_upt.trim()+"' ";
      }if(filter_no_tlp){
        param += " and pl.no_tlp like '%"+filter_no_tlp.trim()+"%' ";
      }if(filter_no_hp){
        param += " and pl.no_hp like '%"+filter_no_hp.trim()+"%' ";
      }

      //Tanggal Pelayanan
      if(filter_pl_date_from && filter_pl_date_to){
        date1 = new Date(filter_pl_date_from); 
        date2 = new Date(filter_pl_date_to);

        var date1   = ((new Date(filter_pl_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_pl_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(pl.tgl_pelayanan, '%Y-%m-%d')   between '"+date_to_default(filter_pl_date_from.trim())+"' and  '"+date_to_default(filter_pl_date_to.trim())+"'";
        }
      }
      else if(filter_pl_date_from){
           param += " and DATE_FORMAT(pl.tgl_pelayanan, '%Y-%m-%d')='"+date_to_default(filter_pl_date_from.trim())+"' ";
      }
      else if(filter_pl_date_to){
          param += " and DATE_FORMAT(pl.tgl_pelayanan, '%Y-%m-%d')='"+date_to_default(filter_pl_date_to.trim())+"' ";
      }

      //Created Date
      if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(pl.created_date, '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(pl.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(pl.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_to.trim())+"' ";
      }

      $("#LoketPelayananPencarian_all").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>pelayanan_loket/table_all_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_pelayanan_all').html(data);
              }
          });
      $('#Filter_LoketPelayananAll').modal('hide');
   }  
</script>

<!-- Filter Loket Pelayanan Detail -->
<div id="Filter_LoketPelayananDetail" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_pelayanan_filter" method="POST">
                <div class="row">
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">Nama</span> 
                    <input type="text" name="filter_nama" id="filter_nama" class="form-control">
                  </div>
                   <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">Jabatan</span> 
                    <input type="text" name="filter_jabatan"  id="filter_jabatan" class="form-control">
                  </div>
                 </div>

                <div class="row"> 
                 
                  <div class="col-md-12 form-group form-box col-xs-6">
                    <span class="label">Perusahaan</span> 
                    <div class="input-group">
                      <input class="form-control" id="filter_company" name="filter_company"   placeholder="" type="text"  >
                      <div class="input-group-btn">
                        <button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
                      </div>
                    </div>
                    <input type="hidden" name="filter_kode_perusahaan"  id="filter_kode_perusahaan">
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">No.Tlp</span> 
                    <input type="text"  name="filter_no_tlp" id="filter_no_tlp" class="form-control">
                  </div>

                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">No.Hp</span> 
                    <input type="text" name="filter_no_hp" id="filter_no_hp" class="form-control">
                  </div>
                </div> 

                <div class="row">
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Tanggal Pelayanan</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_pl_date_from" name="filter_pl_date_from" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_pl_date_to" name="filter_pl_date_to" maxlength="50"  placeholder="" type="text"  >
                  </div> 
             
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_from" name="filter_date_from" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_to" name="filter_date_to" maxlength="50"  placeholder="" type="text"  >
                  </div> 
              </div> 

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_pelayanan_filter')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_pelayanan_detail()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
  $('#filter_company').typeahead({
      hint: true,
      highlight: true,
      source: function (query, process) {
          $.ajax({
              url: '<?php echo $basepath ?>ajax/reqCompany',
              type: 'POST',
              dataType: 'JSON',
              data: 'query='+$('#filter_company').val(),
              success: function(data) {
                  var results = data.map(function(item) {
                      var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
                      return someItem;
                  });
                  return process(results);
              }
          });
      },
      afterSelect: function(item) {
          this.$element[0].value = item.company_name
      },
      updater: function(item) {
          $('#filter_kode_perusahaan').val(item.company_id);
          return item;
      }
    });
  function filter_pelayanan_detail() {
      $("#filter_pelayanan_detail").html("");
      var filter_nama     = $("#filter_nama").val();
      var filter_jabatan  = $("#filter_jabatan").val();
      var filter_kode_perusahaan  = $("#filter_kode_perusahaan").val();
      var filter_provinsi = $("#filter_provinsi").val();
      var filter_upt      = $("#filter_upt").val();
      var filter_no_tlp   = $("#filter_no_tlp").val();
      var filter_no_hp    = $("#filter_no_hp").val();

      var filter_date_from  = $("#filter_date_from").val();
      var filter_date_to    = $("#filter_date_to").val();
      var filter_pl_date_from  = $("#filter_pl_date_from").val();
      var filter_pl_date_to    = $("#filter_pl_date_to").val();

      var param="";
      if(filter_nama){
        param += " and pl.nama_pengunjung like '%"+filter_nama.trim()+"%' ";
      } if(filter_jabatan){
        param += " and pl.jabatan_pengunjung like '%"+filter_jabatan.trim()+"%' ";
      }if(filter_kode_perusahaan){
        param += " and pl.kode_perusahaan='"+filter_kode_perusahaan.trim()+"' ";
      }if(filter_provinsi){
        param += " and pl.id_prov ='"+filter_provinsi.trim()+"' ";
      }if(filter_upt){
        param += " and pl.kode_upt ='"+filter_upt.trim()+"' ";
      }if(filter_no_tlp){
        param += " and pl.no_tlp like '%"+filter_no_tlp.trim()+"%' ";
      }if(filter_no_hp){
        param += " and pl.no_hp like '%"+filter_no_hp.trim()+"%' ";
      }

      //Tanggal Pelayanan
      if(filter_pl_date_from && filter_pl_date_to){
        date1 = new Date(filter_pl_date_from); 
        date2 = new Date(filter_pl_date_to);

        var date1   = ((new Date(filter_pl_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_pl_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(pl.tgl_pelayanan, '%Y-%m-%d') between '"+date_to_default(filter_pl_date_from.trim())+"' and  '"+date_to_default(filter_pl_date_to.trim())+"'";
        }
      }
      else if(filter_pl_date_from){
           param += " and DATE_FORMAT(pl.tgl_pelayanan, '%Y-%m-%d')='"+date_to_default(filter_pl_date_from.trim())+"' ";
      }
      else if(filter_pl_date_to){
          param += " and DATE_FORMAT(pl.tgl_pelayanan, '%Y-%m-%d')='"+date_to_default(filter_pl_date_to.trim())+"' ";
      }

      //Created Date
      if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(pl.created_date, '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(pl.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(pl.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_to.trim())+"' ";
      }

      $("#LoketPelayananPencarian").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>pelayanan_loket/table_detail_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_pelayanan_detail').html(data);
              }
          });
      $('#Filter_LoketPelayananDetail').modal('hide');
   } 
</script>


<!-- Filter SPP All -->
<div id="Filter_SPPAll" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_spp_filter_all" method="POST">
                  <div class="row">
                    <div class="col-md-6 form-group form-box col-xs-6">
                      <span class="label">No Spp</span> 
                      <input type="text" name="filter_no_spp_all"  id="filter_no_spp_all" class="form-control">
                    </div>

                    <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">Service</span> 
                    <input type="text" name="filter_service_spp_all"  id="filter_service_spp_all" class="form-control">
                  </div>
                 </div>

                   <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-6">
                      <span class="label">Perusahaan</span> 
                      <div class="input-group">
                        <input class="form-control" id="filter_company_spp_all" name="filter_company_spp_all"   placeholder="" type="text"  >
                        <div class="input-group-btn">
                          <button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                      </div>
                      <input type="hidden" name="filter_kode_perusahaan_spp_all"  id="filter_kode_perusahaan_spp_all">
                    </div>
                 </div>

                <div class="row"> 
                  
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">No Aplikasi</span> 
                    <input type="text" name="filter_no_aplikasi_spp_all"  id="filter_no_aplikasi_spp_all" class="form-control">
                  </div>

                   <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">No Client License</span> 
                    <input type="text" name="filter_no_client_spp_all"  id="filter_no_client_spp_all" class="form-control">
                  </div>

                </div>  

                <div class="row"> 
                 
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">Jenis SPP</span> 
                    <input type="text" name="filter_jenis_spp_spp_all"  id="filter_jenis_spp_spp_all" class="form-control">
                  </div>

                   <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">Nilai BHP (Rp)</span> 
                    <input class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="filter_nilai_bhp_spp_all" name="filter_nilai_bhp_spp_all"  placeholder="" type="text" >
                  </div>

                </div> 

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-6">
                    <span class="label">Metode</span> 
                     <select class="form-control" id="filter_metode_spp_all" name="filter_metode_spp_all">
                        <option value="">Pilih Metode</option>
                       <?php 
                        $data_mtd = $db->SelectLimit("select * from ms_metode"); 
                        while($list = $data_mtd->FetchRow()){
                              foreach($list as $key=>$val){
                                          $key=strtolower($key);
                                          $$key=$val;
                                        }  
                      ?>  
                      <option value="<?php echo $id_metode ?>"><?php echo $metode ?></option>
                      <?php } ?>
                      </select>
                  </div>
                </div> 

           <div class="row">
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Tanggal Pengiriman</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_pgr_date_spp_from_all" name="filter_pgr_date_spp_from_all" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_pgr_date_spp_to_all" name="filter_pgr_date_spp_to_all" maxlength="50"  placeholder="" type="text"  >
                  </div> 
             
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_spp_from_all" name="filter_date_spp_from_all" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_spp_to_all" name="filter_date_spp_to_all" maxlength="50"  placeholder="" type="text"  >
                  </div> 
              </div> 


                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12"  >
                    <span class="label">Provinsi</span> 
                    <select class="form-control" onchange="call_upt_fltr_spp()" id="filter_provinsi_spp_all" name="id_prov" >
                      <option value="">Pilih Provinsi</option>
                      <?php 
                        $data_shf = $gen_model->GetWhere('ms_provinsi');
                          while($list = $data_shf->FetchRow()){
                            foreach($list as $key=>$val){
                                        $key=strtolower($key);
                                        $$key=$val;
                                      }  
                        ?>
                      <option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12" >
                    <span class="label">Nama UPT</span> 
                    <select class="form-control"  id="filter_upt_spp_all" name="loket_upt" >
                      <option value="">Pilih UPT</option>
                    </select>
                  </div> 
                </div> 

              

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-6">
                    <span class="label">Keterangan</span> 
                    <textarea class="form-control" id="filter_keterangan_spp_all" name="filter_keterangan_spp_all"  rows="3" ></textarea>
                  </div>
                </div> 


            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_spp_filter_all')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_spp_all()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
   $('#filter_company_spp_all').typeahead({
      hint: true,
      highlight: true,
      source: function (query, process) {
          $.ajax({
              url: '<?php echo $basepath ?>ajax/reqCompany',
              type: 'POST',
              dataType: 'JSON',
              data: 'query='+$('#filter_company_spp_all').val(),
              success: function(data) {
                  var results = data.map(function(item) {
                      var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
                      return someItem;
                  });
                  return process(results);
              }
          });
      },
      afterSelect: function(item) {
          this.$element[0].value = item.company_name
      },
      updater: function(item) {
          $('#filter_kode_perusahaan_spp_all').val(item.company_id);
          return item;
      }
    });
  function call_upt_fltr_spp(){
    var prov = $("#filter_provinsi_spp_all").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#filter_upt_spp_all").html(msg);        
                }
             }
          }); 
  }
  function filter_spp_all() {
      $("#filter_spp_all").html("");
      var filter_spp              = $("#filter_no_spp_all").val();
      var filter_service          = $("#filter_service_spp_all").val();
      var filter_jabatan          = $("#filter_jabatan_all").val();
      var filter_kode_perusahaan  = $("#filter_kode_perusahaan_spp_all").val();
      var filter_no_aplikasi      = $("#filter_no_aplikasi_spp_all").val();
      var filter_no_client        = $("#filter_no_client_spp_all").val();
      var filter_jenis            = $("#filter_jenis_spp_spp_all").val();
      var filter_nilai_bhp        = $("#filter_nilai_bhp_spp_all").val();
      var filter_metode           = $("#filter_metode_spp_all").val();
      var filter_provinsi         = $("#filter_provinsi_spp_all").val();
      var filter_upt              = $("#filter_upt_spp_all").val();
      var filter_keterangan       = $("#filter_keterangan_spp_all").val();

      var filter_date_from        = $("#filter_date_spp_from_all").val();
      var filter_date_to          = $("#filter_date_spp_to_all").val();
      var filter_pgr_date_spp_from     = $("#filter_pgr_date_spp_from_all").val();
      var filter_pgr_date_spp_to       = $("#filter_pgr_date_spp_to_all").val();


      var param="";
      if(filter_spp){
        param += " and spp.no_spp like '%"+filter_spp.trim()+"%' ";
      } if(filter_service){
        param += " and spp.service like '%"+filter_service.trim()+"%' ";
      }if(filter_kode_perusahaan){
        param += " and spp.kode_perusahaan like '%"+filter_kode_perusahaan.trim()+"%' ";
      }if(filter_no_aplikasi){
        param += " and spp.no_aplikasi like '%"+filter_no_aplikasi.trim()+"%' ";
      }if(filter_no_client){
        param += " and spp.no_klien_licence like '%"+filter_no_client.trim()+"%' ";
      }if(filter_jenis){
        param += " and spp.status_izin  like '%"+filter_jenis.trim()+"%' ";
      }if(filter_nilai_bhp){
        param += " and spp.tagihan like '%"+remove_char(filter_nilai_bhp.trim(),'.')+"%' "; 
      }if(filter_metode){
        param += " and mtd.id_metode like '%"+filter_metode.trim()+"%' ";
      }if(filter_provinsi){
        param += " and spp.id_prov like '%"+filter_provinsi.trim()+"%' ";
      }if(filter_upt){
        param += " and spp.kode_upt like '%"+filter_upt.trim()+"%' ";
      }if(filter_keterangan){
        param += " and spp.keterangan like '%"+filter_keterangan.trim()+"%' ";
      }


      //Tanggal Pengiriman
      if(filter_pgr_date_spp_from && filter_pgr_date_spp_to){
        date1 = new Date(filter_pgr_date_spp_from); 
        date2 = new Date(filter_pgr_date_spp_to);

        var date1   = ((new Date(filter_pgr_date_spp_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_pgr_date_spp_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(spp.tgl_terima_waba , '%Y-%m-%d') between '"+date_to_default(filter_pgr_date_spp_from.trim())+"' and  '"+date_to_default(filter_pgr_date_spp_to.trim())+"'";
        }
      }
      else if(filter_pgr_date_spp_from){
           param += " and DATE_FORMAT(spp.tgl_terima_waba , '%Y-%m-%d')='"+date_to_default(filter_pgr_date_spp_from.trim())+"' ";
      }
      else if(filter_pgr_date_spp_to){
          param += " and DATE_FORMAT(spp.tgl_terima_waba , '%Y-%m-%d')='"+date_to_default(filter_pgr_date_spp_to.trim())+"' ";
      }

      //Created Date
      if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(spp.created_date , '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(spp.created_date , '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(spp.created_date , '%Y-%m-%d')'"+date_to_default(filter_date_to.trim())+"' ";
      }

      $("#SPPencarian_all").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>pelayanan_spp/table_all_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_spp_all').html(data);
              }
          });
      $('#Filter_SPPAll').modal('hide');
  }  
</script>

<!-- Filter SPP Detail -->
<div id="Filter_SPPDetail" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_spp_filter" method="POST">
                  <div class="row">
                    <div class="col-md-6 form-group form-box col-xs-6">
                      <span class="label">No Spp</span> 
                      <input type="text" name="filter_no_spp_spp"  id="filter_spp_no_spp" class="form-control">
                    </div>

                    <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">Service</span> 
                    <input type="text" name="filter_service_spp"  id="filter_service_spp" class="form-control">
                  </div>
                 </div>

                   <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-6">
                      <span class="label">Perusahaan</span> 
                      <div class="input-group">
                        <input class="form-control" id="filter_company_spp" name="filter_company_spp"   placeholder="" type="text"  >
                        <div class="input-group-btn">
                          <button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                      </div>
                      <input type="hidden" name="filter_kode_perusahaan_spp"  id="filter_kode_perusahaan_spp">
                    </div>
                 </div>

                <div class="row"> 
                  
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">No Aplikasi</span> 
                    <input type="text" name="filter_no_aplikasi_spp"  id="filter_no_aplikasi_spp" class="form-control">
                  </div>

                   <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">No Client License</span> 
                    <input type="text" name="filter_no_client_spp"  id="filter_no_client_spp" class="form-control">
                  </div>

                </div>  

                <div class="row"> 
                 
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">Jenis SPP</span> 
                    <input type="text" name="filter_jenis_spp_spp"  id="filter_jenis_spp_spp" class="form-control">
                  </div>

                   <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">Nilai BHP (Rp)</span> 
                    <input class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="filter_nilai_bhp_spp" name="filter_nilai_bhp_spp"  placeholder="" type="text" >
                  </div>

                </div> 

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-6">
                    <span class="label">Metode</span> 
                     <select class="form-control" id="filter_metode_spp" name="filter_metode_spp">
                        <option value="">Pilih Metode</option>
                        <?php 
                          $data_mtd = $db->SelectLimit("select * from ms_metode"); 
                          while($list = $data_mtd->FetchRow()){
                                foreach($list as $key=>$val){
                                            $key=strtolower($key);
                                            $$key=$val;
                                          }  
                        ?>  
                        <option value="<?php echo $id_metode ?>"><?php echo $metode ?></option>
                        <?php } ?>
                      </select>
                  </div>
                </div> 


           <div class="row">
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Tanggal Pengiriman</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_pgr_date_spp_from" name="filter_pgr_date_spp_from" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_pgr_date_spp_to" name="filter_pgr_date_spp_to" maxlength="50"  placeholder="" type="text"  >
                  </div> 
             
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_spp_from" name="filter_date_spp_from" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_spp_to" name="filter_date_spp_to" maxlength="50"  placeholder="" type="text"  >
                  </div> 
              </div> 

              

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-6">
                    <span class="label">Keterangan</span> 
                    <textarea class="form-control" id="filter_keterangan_spp" name="filter_keterangan_spp"  rows="3" ></textarea>
                  </div>
                </div> 


            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_spp_filter')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_spp_detail()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
  $('#filter_company_spp').typeahead({
      hint: true,
      highlight: true,
      source: function (query, process) {
          $.ajax({
              url: '<?php echo $basepath ?>ajax/reqCompany',
              type: 'POST',
              dataType: 'JSON',
              data: 'query='+$('#filter_company_spp').val(),
              success: function(data) {
                  var results = data.map(function(item) {
                      var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
                      return someItem;
                  });
                  return process(results);
              }
          });
      },
      afterSelect: function(item) {
          this.$element[0].value = item.company_name
      },
      updater: function(item) {
          $('#filter_kode_perusahaan_spp').val(item.company_id);
          return item;
      }
    });
  $('#filter_company').typeahead({
      hint: true,
      highlight: true,
      source: function (query, process) {
          $.ajax({
              url: '<?php echo $basepath ?>ajax/reqCompany',
              type: 'POST',
              dataType: 'JSON',
              data: 'query='+$('#filter_company').val(),
              success: function(data) {
                  var results = data.map(function(item) {
                      var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
                      return someItem;
                  });
                  return process(results);
              }
          });
      },
      afterSelect: function(item) {
          this.$element[0].value = item.company_name
      },
      updater: function(item) {
          $('#filter_kode_perusahaan').val(item.company_id);
          return item;
      }
    });
  function filter_spp_detail() {
      $("#filter_spp_detail").html("");
      var filter_spp              = $("#filter_no_spp_spp").val();
      var filter_service          = $("#filter_service_spp").val();
      var filter_kode_perusahaan  = $("#filter_kode_perusahaan_spp").val();
      var filter_no_aplikasi      = $("#filter_no_aplikasi_spp").val();
      var filter_no_client        = $("#filter_no_client_spp").val();
      var filter_jenis            = $("#filter_jenis_spp_spp").val();
      var filter_nilai_bhp        = $("#filter_nilai_bhp_spp").val();      var filter_metode           = $("#filter_metode_spp").val();
      var filter_provinsi         = $("#filter_provinsi_spp").val();
      var filter_upt              = $("#filter_upt_spp").val();
      var filter_keterangan       = $("#filter_keterangan_spp").val();

      var filter_date_from        = $("#filter_date_spp_from").val();
      var filter_date_to          = $("#filter_date_spp_to").val();
      var filter_pgr_date_spp_from     = $("#filter_pgr_date_spp_from").val();
      var filter_pgr_date_spp_to       = $("#filter_pgr_date_spp_to").val();


      var param="";
      if(filter_spp){
        param += " and spp.no_spp like '%"+filter_spp.trim()+"%' ";
      } if(filter_service){
        param += " and spp.service like '%"+filter_service.trim()+"%' ";
      }if(filter_kode_perusahaan){
        param += " and spp.kode_perusahaan like '%"+filter_kode_perusahaan.trim()+"%' ";
      }if(filter_no_aplikasi){
        param += " and spp.no_aplikasi like '%"+filter_no_aplikasi.trim()+"%' ";
      }if(filter_no_client){
        param += " and spp.no_klien_licence like '%"+filter_no_client.trim()+"%' ";
      }if(filter_jenis){
        param += " and spp.status_izin  like '%"+filter_jenis.trim()+"%' ";
      }if(filter_nilai_bhp){
        param += " and spp.tagihan like '%"+remove_char(filter_nilai_bhp.trim(),'.')+"%' "; 
      }if(filter_metode){
        param += " and mtd.id_metode like '%"+filter_metode.trim()+"%' ";
      }if(filter_provinsi){
        param += " and spp.id_prov like '%"+filter_provinsi.trim()+"%' ";
      }if(filter_upt){
        param += " and spp.kode_upt like '%"+filter_upt.trim()+"%' ";
      }if(filter_keterangan){
        param += " and spp.keterangan like '%"+filter_keterangan.trim()+"%' ";
      }

       //Tanggal Pengiriman
      if(filter_pgr_date_spp_from && filter_pgr_date_spp_to){
        date1 = new Date(filter_pgr_date_spp_from); 
        date2 = new Date(filter_pgr_date_spp_to);

        var date1   = ((new Date(filter_pgr_date_spp_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_pgr_date_spp_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(spp.tgl_terima_waba , '%Y-%m-%d') between '"+date_to_default(filter_pgr_date_spp_from.trim())+"' and  '"+date_to_default(filter_pgr_date_spp_to.trim())+"'";
        }
      }
      else if(filter_pgr_date_spp_from){
           param += " and DATE_FORMAT(spp.tgl_terima_waba , '%Y-%m-%d')='"+date_to_default(filter_pgr_date_spp_from.trim())+"' ";
      }
      else if(filter_pgr_date_spp_to){
          param += " and DATE_FORMAT(spp.tgl_terima_waba , '%Y-%m-%d')='"+date_to_default(filter_pgr_date_spp_to.trim())+"' ";
      }

      //Created Date
      if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(spp.created_date , '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(spp.created_date , '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(spp.created_date , '%Y-%m-%d')'"+date_to_default(filter_date_to.trim())+"' ";
      }
      
      $("#SPP_Pencarian").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>pelayanan_spp/table_detail_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_spp_detail').html(data);
              }
          });
      $('#Filter_SPPDetail').modal('hide');
   } 
</script>

<!-- Filter ISR All -->
<div id="Filter_ISRAll" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_isr_filter_all" method="POST">
                  <div class="row">
                    <div class="col-md-6 form-group form-box col-xs-6">
                      <span class="label">No Spp</span> 
                      <input type="text"   id="filter_no_spp_isr_all" class="form-control">
                    </div>

                    <div class="col-md-6 form-group form-box col-xs-12">
                      <span class="label">Perusahaan</span> 
                      <div class="input-group">
                        <input class="form-control" id="filter_company_isr_all"   placeholder="" type="text"  >
                        <div class="input-group-btn">
                          <button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                      </div>
                      <input type="hidden"   id="filter_kode_perusahaan_isr_all">
                    </div>
                 </div>

                <div class="row"> 
                  
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">No Aplikasi</span> 
                    <input type="text" id="filter_no_aplikasi_isr_all" class="form-control">
                  </div>


                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Service</span> 
                    <input  class="form-control"  id="filter_service_isr_all"    placeholder="" type="text"  >
                  </div>

                </div>  

            
                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Metode</span> 
                     <select class="form-control" id="filter_metode_isr_all">
                        <option value="">Pilih Metode</option>
                        <?php 
                            $data_mtd = $db->SelectLimit("select * from ms_metode"); 
                            while($list = $data_mtd->FetchRow()){
                                  foreach($list as $key=>$val){
                                              $key=strtolower($key);
                                              $$key=$val;
                                            }  
                          ?>  
                          <option value="<?php echo $id_metode ?>"><?php echo $metode ?></option>
                          <?php } ?>
                      </select>
                  </div>
                </div> 


                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12"  >
                    <span class="label">Provinsi</span> 
                    <select class="form-control" onchange="call_upt_fltr_isr()" id="filter_provinsi_isr_all" >
                      <option value="">Pilih Provinsi</option>
                      <?php 
                        $data_shf = $gen_model->GetWhere('ms_provinsi');
                          while($list = $data_shf->FetchRow()){
                            foreach($list as $key=>$val){
                                        $key=strtolower($key);
                                        $$key=$val;
                                      }  
                        ?>
                      <option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12" >
                    <span class="label">Nama UPT</span> 
                    <select class="form-control"  id="filter_upt_isr_all" >
                      <option value="">Pilih UPT</option>
                    </select>
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-6">
                    <span class="label">Keterangan</span> 
                    <textarea class="form-control" id="filter_keterangan_isr_all" rows="3" ></textarea>
                  </div>
                </div> 


              <div class="row">
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Tanggal Pengiriman</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_pgr_date_isr_from_all" name="filter_pgr_date_isr_from_all" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_pgr_date_isr_to_all" name="filter_pgr_date_isr_to_all" maxlength="50"  placeholder="" type="text"  >
                  </div> 
             
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_isr_from_all" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_isr_to_all"  maxlength="50"  placeholder="" type="text"  >
                  </div>
              </div> 

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_isr_filter_all')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_isr_all()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
   $('#filter_company_isr_all').typeahead({
      hint: true,
      highlight: true,
      source: function (query, process) {
          $.ajax({
              url: '<?php echo $basepath ?>ajax/reqCompany',
              type: 'POST',
              dataType: 'JSON',
              data: 'query='+$('#filter_company_isr_all').val(),
              success: function(data) {
                  var results = data.map(function(item) {
                      var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
                      return someItem;
                  });
                  return process(results);
              }
          });
      },
      afterSelect: function(item) {
          this.$element[0].value = item.company_name
      },
      updater: function(item) {
          $('#filter_kode_perusahaan_isr_all').val(item.company_id);
          return item;
      }
    });
  function call_upt_fltr_isr(){
    var prov = $("#filter_provinsi_isr_all").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#filter_upt_isr_all").html(msg);        
                }
             }
          }); 
  }
  function filter_isr_all() {
      $("#filter_isr_all").html("");
      var filter_spp              = $("#filter_no_spp_isr_all").val();
      var filter_service          = $("#filter_service_isr_all").val();
      var filter_kode_perusahaan  = $("#filter_kode_perusahaan_isr_all").val();
      var filter_no_aplikasi      = $("#filter_no_aplikasi_isr_all").val();
      var filter_metode           = $("#filter_metode_isr_all").val();
      var filter_provinsi         = $("#filter_provinsi_isr_all").val();
      var filter_upt              = $("#filter_upt_isr_all").val();
      var filter_keterangan       = $("#filter_keterangan_isr_all").val();

      var filter_date_from        = $("#filter_date_isr_from_all").val();
      var filter_date_to          = $("#filter_date_isr_to_all").val();
      var filter_pgr_date_from     = $("#filter_pgr_date_isr_from_all").val();
      var filter_pgr_date_to       = $("#filter_pgr_date_isr_to_all").val();



      var param="";
      if(filter_spp){
        param += " and isr.no_spp like '%"+filter_spp.trim()+"%' ";
      } if(filter_service){
        param += " and isr.service like '%"+filter_service.trim()+"%' ";
      }if(filter_kode_perusahaan){
        param += " and isr.no_client ='"+filter_kode_perusahaan.trim()+"' ";
      }if(filter_no_aplikasi){
        param += " and isr.no_aplikasi like '%"+filter_no_aplikasi.trim()+"%' ";
      }if(filter_metode){
        param += " and mtd.id_metode ='"+filter_metode.trim()+"' ";
      }if(filter_provinsi){
        param += " and isr.id_prov ='"+filter_provinsi.trim()+"' ";
      }if(filter_upt){
        param += " and isr.kode_upt ='"+filter_upt.trim()+"' ";
      }if(filter_keterangan){
        param += " and isr.keterangan like '%"+filter_keterangan.trim()+"%' ";
      }

        //Tanggal Pengiriman
      if(filter_pgr_date_from && filter_pgr_date_to){
        date1 = new Date(filter_pgr_date_from); 
        date2 = new Date(filter_pgr_date_to);

        var date1   = ((new Date(filter_pgr_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_pgr_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(isr.tgl_terima_waba , '%Y-%m-%d') between '"+date_to_default(filter_pgr_date_from.trim())+"' and  '"+date_to_default(filter_pgr_date_to.trim())+"'";
        }
      }
      else if(filter_pgr_date_from){
           param += " and DATE_FORMAT(isr.tgl_terima_waba , '%Y-%m-%d')='"+date_to_default(filter_pgr_date_from.trim())+"' ";
      }
      else if(filter_pgr_date_to){
          param += " and DATE_FORMAT(isr.tgl_terima_waba , '%Y-%m-%d')='"+date_to_default(filter_pgr_date_to.trim())+"' ";
      }

      //Created Date
      if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(isr.created_date , '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(isr.created_date , '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(isr.created_date , '%Y-%m-%d')'"+date_to_default(filter_date_to.trim())+"' ";
      }

      $("#ISRPencarian_all").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>pelayanan_isr/table_all_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_isr_all').html(data);
              }
          });
      $('#Filter_ISRAll').modal('hide');
   }  
</script>


<!-- Filter ISR Detail -->
<div id="Filter_ISRDetail" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_isr_filter" method="POST">
                  <div class="row">
                    <div class="col-md-6 form-group form-box col-xs-6">
                      <span class="label">No Spp</span> 
                      <input type="text"   id="filter_no_spp_isr" class="form-control">
                    </div>

                    <div class="col-md-6 form-group form-box col-xs-12">
                      <span class="label">Perusahaan</span> 
                      <div class="input-group">
                        <input class="form-control" id="filter_company_isr"   placeholder="" type="text"  >
                        <div class="input-group-btn">
                          <button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                      </div>
                      <input type="hidden"   id="filter_kode_perusahaan_isr">
                    </div>
                 </div>

                <div class="row"> 
                  
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">No Aplikasi</span> 
                    <input type="text" id="filter_no_aplikasi_isr" class="form-control">
                  </div>


                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Service</span> 
                    <input  class="form-control"  id="filter_service_isr"    placeholder="" type="text"  >
                  </div>

                </div>  

            
                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-6">
                    <span class="label">Metode</span> 
                     <select class="form-control" id="filter_metode_isr">
                        <option value="">Pilih Metode</option>
                        <?php 
                          $data_mtd = $db->SelectLimit("select * from ms_metode"); 
                          while($list = $data_mtd->FetchRow()){
                                foreach($list as $key=>$val){
                                            $key=strtolower($key);
                                            $$key=$val;
                                          }  
                        ?>  
                        <option value="<?php echo $id_metode ?>"><?php echo $metode ?></option>
                        <?php } ?>
                      </select>
                  </div>
                </div> 

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-6">
                    <span class="label">Keterangan</span> 
                    <textarea class="form-control" id="filter_keterangan_isr" rows="3" ></textarea>
                  </div>
                </div> 


             <div class="row">
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Tanggal Pengiriman</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_pgr_date_isr_from" name="filter_pgr_date_isr_from" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_pgr_date_isr_to" name="filter_pgr_date_isr_to" maxlength="50"  placeholder="" type="text"  >
                  </div> 
             
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_isr_from" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_isr_to"  maxlength="50"  placeholder="" type="text"  >
                  </div>
              </div> 

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_isr_filter')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_isr()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
   $('#filter_company_isr').typeahead({
      hint: true,
      highlight: true,
      source: function (query, process) {
          $.ajax({
              url: '<?php echo $basepath ?>ajax/reqCompany',
              type: 'POST',
              dataType: 'JSON',
              data: 'query='+$('#filter_company_isr').val(),
              success: function(data) {
                  var results = data.map(function(item) {
                      var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
                      return someItem;
                  });
                  return process(results);
              }
          });
      },
      afterSelect: function(item) {
          this.$element[0].value = item.company_name
      },
      updater: function(item) {
          $('#filter_kode_perusahaan_isr').val(item.company_id);
          return item;
      }
    });
  function filter_isr() {
      $("#filter_isr_detail").html("");
      var filter_spp              = $("#filter_no_spp_isr").val();
      var filter_service          = $("#filter_service_isr").val();
      var filter_kode_perusahaan  = $("#filter_kode_perusahaan_isr").val();
      var filter_no_aplikasi      = $("#filter_no_aplikasi_isr").val();
      var filter_metode           = $("#filter_metode_isr").val();
      var filter_provinsi         = $("#filter_provinsi_isr").val();
      var filter_upt              = $("#filter_upt_isr").val();
      var filter_keterangan       = $("#filter_keterangan_isr").val();


      var filter_date_from        = $("#filter_date_isr_from").val();
      var filter_date_to          = $("#filter_date_isr_to").val();
      var filter_pgr_date_from     = $("#filter_pgr_date_isr_from").val();
      var filter_pgr_date_to       = $("#filter_pgr_date_isr_to").val();


      var param="";
      if(filter_spp){
        param += " and isr.no_spp like '%"+filter_spp.trim()+"%' ";
      } if(filter_service){
        param += " and isr.service like '%"+filter_service.trim()+"%' ";
      }if(filter_kode_perusahaan){
        param += " and isr.no_client ='"+filter_kode_perusahaan.trim()+"' ";
      }if(filter_no_aplikasi){
        param += " and isr.no_aplikasi like '%"+filter_no_aplikasi.trim()+"%' ";
      }if(filter_metode){
        param += " and mtd.id_metode ='"+filter_metode.trim()+"' ";
      }if(filter_provinsi){
        param += " and isr.id_prov ='"+filter_provinsi.trim()+"' ";
      }if(filter_upt){
        param += " and isr.kode_upt ='"+filter_upt.trim()+"' ";
      }if(filter_keterangan){
        param += " and isr.keterangan like '%"+filter_keterangan.trim()+"%' ";
      }

          //Tanggal Pengiriman
      if(filter_pgr_date_from && filter_pgr_date_to){
        date1 = new Date(filter_pgr_date_from); 
        date2 = new Date(filter_pgr_date_to);

        var date1   = ((new Date(filter_pgr_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_pgr_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(isr.tgl_terima_waba , '%Y-%m-%d') between '"+date_to_default(filter_pgr_date_from.trim())+"' and  '"+date_to_default(filter_pgr_date_to.trim())+"'";
        }
      }
      else if(filter_pgr_date_from){
           param += " and DATE_FORMAT(isr.tgl_terima_waba , '%Y-%m-%d')='"+date_to_default(filter_pgr_date_from.trim())+"' ";
      }
      else if(filter_pgr_date_to){
          param += " and DATE_FORMAT(isr.tgl_terima_waba , '%Y-%m-%d')='"+date_to_default(filter_pgr_date_to.trim())+"' ";
      }

      //Created Date
      if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(isr.created_date , '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(isr.created_date , '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(isr.created_date , '%Y-%m-%d')'"+date_to_default(filter_date_to.trim())+"' ";
      }

      $("#ISR_Pencarian").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>pelayanan_isr/table_detail_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_isr_detail').html(data);
              }
          });
      $('#Filter_ISRDetail').modal('hide');
   }  
</script>


<!-- Filter Revoke All -->
<div id="Filter_RevokeAll" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_revoke_filter_all" method="POST">
                  <div class="row">
                    <div class="col-md-6 form-group form-box col-xs-6">
                      <span class="label">No Spp</span> 
                      <input type="text"   id="filter_no_spp_revoke_all" class="form-control">
                    </div>

                    <div class="col-md-6 form-group form-box col-xs-12">
                      <span class="label">Perusahaan</span> 
                      <div class="input-group">
                        <input class="form-control" id="filter_company_revoke_all"   placeholder="" type="text"  >
                        <div class="input-group-btn">
                          <button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                      </div>
                      <input type="hidden"   id="filter_kode_perusahaan_revoke_all">
                    </div>
                 </div>

                <div class="row"> 
                  
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">No Aplikasi</span> 
                    <input type="text" id="filter_no_aplikasi_revoke_all" class="form-control">
                  </div>


                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Service</span> 
                    <input  class="form-control"  id="filter_service_revoke_all"    placeholder="" type="text"  >
                  </div>

                </div>  

            
                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Metode</span> 
                     <select class="form-control" id="filter_metode_revoke_all">
                        <option value="">Pilih Metode</option>
                        <?php 
                          $data_mtd = $db->SelectLimit("select * from ms_metode"); 
                          while($list = $data_mtd->FetchRow()){
                                foreach($list as $key=>$val){
                                            $key=strtolower($key);
                                            $$key=$val;
                                          }  
                        ?>  
                        <option value="<?php echo $id_metode ?>"><?php echo $metode ?></option>
                        <?php } ?>
                      </select>
                  </div>
                </div> 


                <div class="row">

                   <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Tanggal ISR Dicabut</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_tgl_from_isr_dicabut_revoke_all" name="filter_tgl_from_isr_dicabut_revoke_all" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_tgl_to_isr_dicabut_revoke_all" name="filter_tgl_to_isr_dicabut_revoke_all" maxlength="50"  placeholder="" type="text"  >
                  </div>                   

                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Tanggal Pengiriman</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_pgr_date_rvk_from_all" name="filter_pgr_date_rvk_from_all" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_pgr_date_rvk_to_all" name="filter_pgr_date_rvk_to_all" maxlength="50"  placeholder="" type="text"  >
                  </div> 

              </div> 

               <div class="row">
             
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_rvk_from_all" name="filter_date_rvk_from_all" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_rvk_to_all" name="filter_date_rvk_to_all" maxlength="50"  placeholder="" type="text"  >
                  </div> 
              </div> 


                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12"  >
                    <span class="label">Provinsi</span> 
                    <select class="form-control" onchange="call_upt_fltr_revoke_all()" id="filter_provinsi_revoke_all" >
                      <option value="">Pilih Provinsi</option>
                      <?php 
                        $data_shf = $gen_model->GetWhere('ms_provinsi');
                          while($list = $data_shf->FetchRow()){
                            foreach($list as $key=>$val){
                                        $key=strtolower($key);
                                        $$key=$val;
                                      }  
                        ?>
                      <option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12" >
                    <span class="label">Nama UPT</span> 
                    <select class="form-control"  id="filter_upt_revoke_all" >
                      <option value="">Pilih UPT</option>
                    </select>
                  </div> 
                </div> 

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-6">
                    <span class="label">Keterangan</span> 
                    <textarea class="form-control" id="filter_keterangan_revoke_all" rows="3" ></textarea>
                  </div>
                </div> 


            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_revoke_filter_all')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_revoke_all()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
   $('#filter_company_revoke_all').typeahead({
      hint: true,
      highlight: true,
      source: function (query, process) {
          $.ajax({
              url: '<?php echo $basepath ?>ajax/reqCompany',
              type: 'POST',
              dataType: 'JSON',
              data: 'query='+$('#filter_company_revoke_all').val(),
              success: function(data) {
                  var results = data.map(function(item) {
                      var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
                      return someItem;
                  });
                  return process(results);
              }
          });
      },
      afterSelect: function(item) {
          this.$element[0].value = item.company_name
      },
      updater: function(item) {
          $('#filter_kode_perusahaan_revoke_all').val(item.company_id);
          return item;
      }
    });
  function call_upt_fltr_revoke_all(){
    var prov = $("#filter_provinsi_revoke_all").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#filter_upt_revoke_all").html(msg);        
                }
             }
          }); 
  }
  function filter_revoke_all() {
      $("#filter_revoke_all").html("");
      var filter_spp              = $("#filter_no_spp_revoke_all").val();
      var filter_service          = $("#filter_service_revoke_all").val();
      var filter_kode_perusahaan  = $("#filter_kode_perusahaan_revoke_all").val();
      var filter_no_aplikasi      = $("#filter_no_aplikasi_revoke_all").val();
      var filter_metode           = $("#filter_metode_revoke_all").val();
      var filter_provinsi         = $("#filter_provinsi_revoke_all").val();
      var filter_upt              = $("#filter_upt_revoke_all").val();
      var filter_keterangan       = $("#filter_keterangan_revoke_all").val();


      var filter_date_from             = $("#filter_date_rvk_from_all").val();
      var filter_date_to               = $("#filter_date_rvk_to_all").val();

      var filter_isr_cabut_date_from     = $("#filter_tgl_from_isr_dicabut_revoke_all").val();
      var filter_isr_cabut_date_to       = $("#filter_tgl_to_isr_dicabut_revoke_all").val();

      var filter_pgr_date_rvk_from     = $("#filter_pgr_date_rvk_from_all").val();
      var filter_pgr_date_rvk_to       = $("#filter_pgr_date_rvk_to_all").val();

      var param="";
      if(filter_spp){
        param += " and rvk.no_spp like '%"+filter_spp.trim()+"%' ";
      } if(filter_service){
        param += " and rvk.service like '%"+filter_service.trim()+"%' ";
      }if(filter_kode_perusahaan){
        param += " and rvk.no_client ='"+filter_kode_perusahaan.trim()+"' ";
      }if(filter_no_aplikasi){
        param += " and rvk.no_aplikasi like '%"+filter_no_aplikasi.trim()+"%' ";
      }if(filter_metode){
        param += " and mtd.id_metode ='"+filter_metode.trim()+"' ";
      }if(filter_provinsi){
        param += " and rvk.id_prov ='"+filter_provinsi.trim()+"' ";
      }if(filter_upt){
        param += " and rvk.kode_upt ='"+filter_upt.trim()+"' ";
      }if(filter_keterangan){
        param += " and rvk.keterangan like '%"+filter_keterangan.trim()+"%' ";
      }

     //Tanggal ISR dicabut
      if(filter_isr_cabut_date_from && filter_isr_cabut_date_to){
        date1 = new Date(filter_isr_cabut_date_from); 
        date2 = new Date(filter_isr_cabut_date_to);

        var date1   = ((new Date(filter_isr_cabut_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_isr_cabut_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(rvk.tgl_isr_dicabut , '%Y-%m-%d') between '"+date_to_default(filter_isr_cabut_date_from.trim())+"' and  '"+date_to_default(filter_isr_cabut_date_to.trim())+"'";
        }
      }
      else if(filter_isr_cabut_date_from){
           param += " and DATE_FORMAT(rvk.tgl_isr_dicabut , '%Y-%m-%d')='"+date_to_default(filter_isr_cabut_date_from.trim())+"' ";
      }
      else if(filter_isr_cabut_date_to){
          param += " and DATE_FORMAT(rvk.tgl_isr_dicabut , '%Y-%m-%d')='"+date_to_default(filter_isr_cabut_date_to.trim())+"' ";
      }

      //Tanggal Pengiriman
      if(filter_pgr_date_rvk_from && filter_pgr_date_rvk_to){
        date1 = new Date(filter_pgr_date_rvk_from); 
        date2 = new Date(filter_pgr_date_rvk_to);

        var date1   = ((new Date(filter_pgr_date_rvk_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_pgr_date_rvk_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(rvk.tgl_terima_waba , '%Y-%m-%d') between '"+date_to_default(filter_pgr_date_rvk_from.trim())+"' and  '"+date_to_default(filter_pgr_date_rvk_to.trim())+"'";
        }
      }
      else if(filter_pgr_date_rvk_from){
           param += " and DATE_FORMAT(rvk.tgl_terima_waba , '%Y-%m-%d')='"+date_to_default(filter_pgr_date_rvk_from.trim())+"' ";
      }
      else if(filter_pgr_date_rvk_to){
          param += " and DATE_FORMAT(rvk.tgl_terima_waba , '%Y-%m-%d')='"+date_to_default(filter_pgr_date_rvk_to.trim())+"' ";
      }

      //Created Date
      if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(rvk.created_date , '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(rvk.created_date , '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(rvk.created_date , '%Y-%m-%d')'"+date_to_default(filter_date_to.trim())+"' ";
      }

      $("#RevokePencarian_all").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>pelayanan_revoke/table_all_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_revoke_all').html(data);
              }
          });
      $('#Filter_RevokeAll').modal('hide');
  }  
</script>


<!-- Filter Revoke Detail -->
<div id="Filter_RevokeDetail" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_revoke_filter" method="POST">
                  <div class="row">
                    <div class="col-md-6 form-group form-box col-xs-6">
                      <span class="label">No Spp</span> 
                      <input type="text"   id="filter_no_spp_revoke" class="form-control">
                    </div>

                    <div class="col-md-6 form-group form-box col-xs-12">
                      <span class="label">Perusahaan</span> 
                      <div class="input-group">
                        <input class="form-control" id="filter_company_revoke"   placeholder="" type="text"  >
                        <div class="input-group-btn">
                          <button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                      </div>
                      <input type="hidden"   id="filter_kode_perusahaan_revoke">
                    </div>
                 </div>

                <div class="row"> 
                  
                  <div class="col-md-6 form-group form-box col-xs-6">
                    <span class="label">No Aplikasi</span> 
                    <input type="text" id="filter_no_aplikasi_revoke" class="form-control">
                  </div>


                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Service</span> 
                    <input  class="form-control"  id="filter_service_revoke"    placeholder="" type="text"  >
                  </div>

                </div>  

            
                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Metode</span> 
                     <select class="form-control" id="filter_metode_revoke">
                        <option value="">Pilih Metode</option>
                       <?php 
                          $data_mtd = $db->SelectLimit("select * from ms_metode"); 
                          while($list = $data_mtd->FetchRow()){
                                foreach($list as $key=>$val){
                                            $key=strtolower($key);
                                            $$key=$val;
                                          }  
                        ?>  
                        <option value="<?php echo $id_metode ?>"><?php echo $metode ?></option>
                        <?php } ?>
                      </select>
                  </div>
                </div> 

                <div class="row">
                   <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Tanggal ISR Dicabut</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_tgl_from_isr_dicabut_revoke" name="filter_tgl_from_isr_dicabut_revoke" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_tgl_to_isr_dicabut_revoke" name="filter_tgl_to_isr_dicabut_revoke" maxlength="50"  placeholder="" type="text"  >
                  </div>                   

                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Tanggal Pengiriman</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_pgr_date_rvk_from" name="filter_pgr_date_rvk_from" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_pgr_date_rvk_to" name="filter_pgr_date_rvk_to" maxlength="50"  placeholder="" type="text"  >
                  </div> 
              </div> 

               <div class="row">
             
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_rvk_from" name="filter_date_rvk_from" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-3 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_rvk_to" name="filter_date_rvk_to" maxlength="50"  placeholder="" type="text"  >
                  </div> 
              </div> 

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-6">
                    <span class="label">Keterangan</span> 
                    <textarea class="form-control" id="filter_keterangan_revoke" rows="3" ></textarea>
                  </div>
                </div> 

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_revoke_filter')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_revoke()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
   $('#filter_company_revoke').typeahead({
      hint: true,
      highlight: true,
      source: function (query, process) {
          $.ajax({
              url: '<?php echo $basepath ?>ajax/reqCompany',
              type: 'POST',
              dataType: 'JSON',
              data: 'query='+$('#filter_company_revoke').val(),
              success: function(data) {
                  var results = data.map(function(item) {
                      var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
                      return someItem;
                  });
                  return process(results);
              }
          });
      },
      afterSelect: function(item) {
          this.$element[0].value = item.company_name
      },
      updater: function(item) {
          $('#filter_kode_perusahaan_revoke').val(item.company_id);
          return item;
      }
    });
  function filter_revoke() {
      $("#filter_revoke_detail").html("");
      var filter_spp              = $("#filter_no_spp_revoke").val();
      var filter_service          = $("#filter_service_revoke").val();
      var filter_kode_perusahaan  = $("#filter_kode_perusahaan_revoke").val();
      var filter_no_aplikasi      = $("#filter_no_aplikasi_revoke").val();
      var filter_metode           = $("#filter_metode_revoke").val();
      var filter_provinsi         = $("#filter_provinsi_revoke").val();
      var filter_upt              = $("#filter_upt_revoke").val();
      var filter_keterangan       = $("#filter_keterangan_revoke").val();

      var filter_date_from             = $("#filter_date_rvk_from").val();
      var filter_date_to               = $("#filter_date_rvk_to").val();

      var filter_isr_cabut_date_from     = $("#filter_tgl_from_isr_dicabut_revoke").val();
      var filter_isr_cabut_date_to       = $("#filter_tgl_to_isr_dicabut_revoke").val();

      var filter_pgr_date_rvk_from     = $("#filter_pgr_date_rvk_from").val();
      var filter_pgr_date_rvk_to       = $("#filter_pgr_date_rvk_to").val();


      var param="";
      if(filter_spp){
        param += " and rvk.no_spp like '%"+filter_spp.trim()+"%' ";
      } if(filter_service){
        param += " and rvk.service like '%"+filter_service.trim()+"%' ";
      }if(filter_kode_perusahaan){
        param += " and rvk.no_client ='"+filter_kode_perusahaan.trim()+"' ";
      }if(filter_no_aplikasi){
        param += " and rvk.no_aplikasi like '%"+filter_no_aplikasi.trim()+"%' ";
      }if(filter_metode){
        param += " and mtd.id_metode ='"+filter_metode.trim()+"' ";
      }if(filter_provinsi){
        param += " and rvk.id_prov ='"+filter_provinsi.trim()+"' ";
      }if(filter_upt){
        param += " and rvk.kode_upt ='"+filter_upt.trim()+"' ";
      }if(filter_keterangan){
        param += " and rvk.keterangan like '%"+filter_keterangan.trim()+"%' ";
      }

       //Tanggal ISR dicabut
      if(filter_isr_cabut_date_from && filter_isr_cabut_date_to){
        date1 = new Date(filter_isr_cabut_date_from); 
        date2 = new Date(filter_isr_cabut_date_to);

        var date1   = ((new Date(filter_isr_cabut_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_isr_cabut_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(rvk.tgl_isr_dicabut , '%Y-%m-%d') between '"+date_to_default(filter_isr_cabut_date_from.trim())+"' and  '"+date_to_default(filter_isr_cabut_date_to.trim())+"'";
        }
      }
      else if(filter_isr_cabut_date_from){
           param += " and DATE_FORMAT(rvk.tgl_isr_dicabut , '%Y-%m-%d')='"+date_to_default(filter_isr_cabut_date_from.trim())+"' ";
      }
      else if(filter_isr_cabut_date_to){
          param += " and DATE_FORMAT(rvk.tgl_isr_dicabut , '%Y-%m-%d')='"+date_to_default(filter_isr_cabut_date_to.trim())+"' ";
      }

      //Tanggal Pengiriman
      if(filter_pgr_date_rvk_from && filter_pgr_date_rvk_to){
        date1 = new Date(filter_pgr_date_rvk_from); 
        date2 = new Date(filter_pgr_date_rvk_to);

        var date1   = ((new Date(filter_pgr_date_rvk_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_pgr_date_rvk_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(rvk.tgl_terima_waba , '%Y-%m-%d') between '"+date_to_default(filter_pgr_date_rvk_from.trim())+"' and  '"+date_to_default(filter_pgr_date_rvk_to.trim())+"'";
        }
      }
      else if(filter_pgr_date_rvk_from){
           param += " and DATE_FORMAT(rvk.tgl_terima_waba , '%Y-%m-%d')='"+date_to_default(filter_pgr_date_rvk_from.trim())+"' ";
      }
      else if(filter_pgr_date_rvk_to){
          param += " and DATE_FORMAT(rvk.tgl_terima_waba , '%Y-%m-%d')='"+date_to_default(filter_pgr_date_rvk_to.trim())+"' ";
      }

      //Created Date
      if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(rvk.created_date , '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(rvk.created_date , '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(rvk.created_date , '%Y-%m-%d')'"+date_to_default(filter_date_to.trim())+"' ";
      }

      $("#Revoke_Pencarian").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>pelayanan_revoke/table_detail_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_revoke_detail').html(data);
              }
          });
      $('#Filter_RevokeDetail').modal('hide');
   }  
</script>


<!-- Filter Piutang All -->
<div id="Filter_PiutangAll" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_piutang_filter_all" method="POST">
                  
                  <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-12">
                      <span class="label">Wajib Bayar</span> 
                      <div class="input-group">
                        <input class="form-control" id="filter_piutang_nama_perusahaan_all" name="nama_perusahaan"   placeholder="" type="text"  >
                        <input  id="filter_piutang_kode_perusahaan_all" name="nama_perusahaan"   type="hidden">
                        <div class="input-group-btn">
                          <button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 form-group form-box col-xs-12">
                      <span class="label">Nilai Penyerahan</span> 
                      <input  class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="filter_piutang_nilai_penyerahan_all" name="nilai_penyerahan"   placeholder="" type="text"  >
                    </div>
                    <div class="col-md-4 form-group form-box col-xs-12">
                      <span class="label">Tahun Pelimpahan</span> 
                      <input class="form-control thn" id="filter_piutang_thn_pelimpahan_all" name="thn_pelimpahan"  readonly  maxlength="4" type="text"  style="background-color:white" >
                    </div>
                    <div class="col-md-4 form-group form-box col-xs-12">
                      <span class="label">Tahapan Pengurusan</span> 
                      <input class="form-control" maxlength="255" id="filter_piutang_thp_pengurusan_all" name="thp_pengurusan"  type="text"  >
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-12">
                      <span class="label">Nama KPKNL</span> 
                      <input  class="form-control" maxlength="255" id="filter_piutang_nama_kpknl_all" name="nama_kpknl"   placeholder="" type="text"  >
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-12">
                        <center style="font-weight:bold;font-weight:50px">Pemindah Bukuan</center>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-4 form-group form-box col-xs-12">
                      <span class="label">Lunas</span>
                      <input class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="filter_piutang_lunas_all" name="lunas"  type="text" >
                    </div>
                    <div class="col-md-4 form-group form-box col-xs-12">
                      <span class="label">Angsuran</span> 
                      <input class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="filter_piutang_angsuran_all" name="angsuran"  type="text" >
                    </div>
                    <div class="col-md-4 form-group form-box col-xs-12">
                      <span class="label">Tanggal</span> 
                      <input class="form-control tgl" id="filter_piutang_tanggal_piutang_all" name="tanggal_piutang"  style="background-color:white" readonly type="text" >
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-12">
                        <hr/>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-4 form-group form-box col-xs-12">
                      <span class="label">PSBDT</span> 
                      <input class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="filter_piutang_psbdt_all" name="psbdt"  type="text" >
                    </div>
                    <div class="col-md-4 form-group form-box col-xs-12">
                      <span class="label">Pembatalan</span> 
                      <input class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="filter_piutang_pembatalan_all" name="pembatalan"  type="text" >
                    </div>
                    <div class="col-md-4 form-group form-box col-xs-12">
                      <span class="label">Sisa Piutang</span> 
                      <input class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="filter_piutang_sisa_piutang_all" type="text" >
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-12">
                      <span class="label">Keterangan</span>
                      <textarea class="form-control" id="filter_piutang_keterangan_all"   rows="3" ></textarea>
                    </div>
                  </div>


                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12"  >
                    <span class="label">Provinsi</span> 
                    <select class="form-control" onchange="call_upt_fltr_piutang_all()" id="filter_provinsi_piutang_all" >
                      <option value="">Pilih Provinsi</option>
                      <?php 
                        $data_shf = $gen_model->GetWhere('ms_provinsi');
                          while($list = $data_shf->FetchRow()){
                            foreach($list as $key=>$val){
                                        $key=strtolower($key);
                                        $$key=$val;
                                      }  
                        ?>
                      <option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12" >
                    <span class="label">Nama UPT</span> 
                    <select class="form-control"  id="filter_upt_piutang_all" >
                      <option value="">Pilih UPT</option>
                    </select>
                  </div> 
                </div> 

               <div class="row">
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_piutang_from_all" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_piutang_to_all"  maxlength="50"  placeholder="" type="text"  >
                  </div> 
               </div> 

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_piutang_filter_all')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_piutang_all()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
   $('#filter_piutang_nama_perusahaan_all').typeahead({
      hint: true,
      highlight: true,
      source: function (query, process) {
          $.ajax({
              url: '<?php echo $basepath ?>ajax/reqCompany',
              type: 'POST',
              dataType: 'JSON',
              data: 'query='+$('#filter_piutang_nama_perusahaan_all').val(),
              success: function(data) {
                  var results = data.map(function(item) {
                      var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
                      return someItem;
                  });
                  return process(results);
              }
          });
      },
      afterSelect: function(item) {
          this.$element[0].value = item.company_name
      },
      updater: function(item) {
          $('#filter_piutang_kode_perusahaan_all').val(item.company_id);
          return item;
      }
    });
  function call_upt_fltr_piutang_all(){
    var prov = $("#filter_provinsi_piutang_all").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#filter_upt_piutang_all").html(msg);        
                }
             }
          }); 
  }
  function filter_piutang_all() {
      $("#filter_piutang_all").html("");
      var filter_thn_pelimpahan      = $("#filter_piutang_thn_pelimpahan_all").val();
      var filter_tgl_piutang         = $("#filter_piutang_tanggal_piutang_all").val();
      var filter_kode_perusahaan     = $("#filter_piutang_kode_perusahaan_all").val();
      var filter_nilai_penyerahan    = $("#filter_piutang_nilai_penyerahan_all").val();
      var filter_tahapan_pengurusan  = $("#filter_piutang_thp_pengurusan_all").val();
      var filter_nama_kpknl          = $("#filter_piutang_nama_kpknl_all").val();
      var filter_lunas               = $("#filter_piutang_lunas_all").val();
      var filter_angsuran            = $("#filter_piutang_angsuran_all").val();
      var filter_psbdt               = $("#filter_piutang_psbdt_all").val();
      var filter_pembatalan          = $("#filter_piutang_pembatalan_all").val();
      var filter_sisa_piutang        = $("#filter_piutang_sisa_piutang_all").val();
      var filter_provinsi            = $("#filter_provinsi_piutang_all").val();
      var filter_upt                 = $("#filter_upt_piutang_all").val();
      var filter_keterangan          = $("#filter_piutang_keterangan_all").val();
      var filter_date_from           = $("#filter_date_piutang_from_all").val();
      var filter_date_to             = $("#filter_date_piutang_to_all").val();

      var param="";
      if(filter_tgl_piutang){
        param += " and pt.tanggal='"+date_to_default(filter_tgl_piutang.trim())+"' ";
      }if(filter_thn_pelimpahan){
        param += " and pt.thn_pelimpahan='"+filter_thn_pelimpahan.trim()+"' ";
      }if(filter_kode_perusahaan){
        param += " and pt.no_client ='"+filter_kode_perusahaan.trim()+"' ";
      }if(filter_nilai_penyerahan){
        param += " and pt.nilai_penyerahan like '%"+remove_char(filter_nilai_penyerahan.trim(),".")+"%' ";
      }if(filter_tahapan_pengurusan){
        param += " and pt.tahapan_pengurusan like '%"+filter_tahapan_pengurusan.trim()+"%' ";
      }if(filter_nama_kpknl){
        param += " and pt.nama_kpknl like '%"+filter_nama_kpknl.trim()+"%' ";
      }if(filter_lunas){
        param += " and pt.lunas like '%"+remove_char(filter_lunas.trim(),".")+"%' ";
      }if(filter_angsuran){
        param += " and pt.angsuran like '%"+remove_char(filter_angsuran.trim(),".")+"%' ";
      }if(filter_psbdt){
        param += " and pt.psbdt like '%"+remove_char(filter_psbdt.trim(),".")+"%' ";
      }if(filter_pembatalan){
        param += " and pt.pembatalan like '%"+remove_char(filter_pembatalan.trim(),".")+"%' ";
      }if(filter_sisa_piutang){
        param += " and pt.sisa_piutang like '%"+remove_char(filter_sisa_piutang.trim(),".")+"%' ";
      }if(filter_provinsi){
        param += " and pt.id_prov ='"+filter_provinsi.trim()+"' ";
      }if(filter_upt){
        param += " and pt.kode_upt ='"+filter_upt.trim()+"' ";
      }if(filter_keterangan){
        param += " and pt.keterangan like '%"+filter_keterangan.trim()+"%' ";
      }

        if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(pt.created_date , '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(pt.created_date , '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(pt.created_date , '%Y-%m-%d')='"+date_to_default(filter_date_to.trim())+"' ";
      }

      $("#PiutangPencarian_all").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>pelayanan_piutang/table_all_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_piutang_all').html(data);
              }
          });
      $('#Filter_PiutangAll').modal('hide');
  }  
</script>


<!-- Filter Piutang Detail -->
<div id="Filter_PiutangDetail" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_piutang_filter" method="POST">
                  <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-12">
                      <span class="label">Wajib Bayar</span> 
                      <div class="input-group">
                        <input class="form-control" id="filter_piutang_nama_perusahaan" name="nama_perusahaan"   placeholder="" type="text"  >
                        <input  id="filter_piutang_kode_perusahaan" name="nama_perusahaan"   type="hidden">
                        <div class="input-group-btn">
                          <button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-4 form-group form-box col-xs-12">
                      <span class="label">Nilai Penyerahan</span> 
                      <input  class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="filter_piutang_nilai_penyerahan" name="nilai_penyerahan"   placeholder="" type="text"  >
                    </div>
                    <div class="col-md-4 form-group form-box col-xs-12">
                      <span class="label">Tahun Pelimpahan</span> 
                      <input class="form-control thn" id="filter_piutang_thn_pelimpahan" name="thn_pelimpahan"  readonly  maxlength="4" type="text"  style="background-color:white" >
                    </div>
                    <div class="col-md-4 form-group form-box col-xs-12">
                      <span class="label">Tahapan Pengurusan</span> 
                      <input class="form-control" maxlength="255" id="filter_piutang_thp_pengurusan" name="thp_pengurusan"  type="text"  >
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-12">
                      <span class="label">Nama KPKNL</span> 
                      <input  class="form-control" maxlength="255" id="filter_piutang_nama_kpknl" name="nama_kpknl"   placeholder="" type="text"  >
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-12">
                        <center style="font-weight:bold;font-weight:50px">Pemindah Bukuan</center>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-4 form-group form-box col-xs-12">
                      <span class="label">Lunas</span>
                      <input class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="filter_piutang_lunas" name="lunas"  type="text" >
                    </div>
                    <div class="col-md-4 form-group form-box col-xs-12">
                      <span class="label">Angsuran</span> 
                      <input class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="filter_piutang_angsuran" name="angsuran"  type="text" >
                    </div>
                    <div class="col-md-4 form-group form-box col-xs-12">
                      <span class="label">Tanggal</span> 
                      <input class="form-control tgl" id="filter_piutang_tanggal_piutang" name="tanggal_piutang"  style="background-color:white" readonly type="text" >
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-12">
                        <hr/>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-4 form-group form-box col-xs-12">
                      <span class="label">PSBDT</span> 
                      <input class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="filter_piutang_psbdt" name="psbdt"  type="text" >
                    </div>
                    <div class="col-md-4 form-group form-box col-xs-12">
                      <span class="label">Pembatalan</span> 
                      <input class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="filter_piutang_pembatalan" name="pembatalan"  type="text" >
                    </div>
                    <div class="col-md-4 form-group form-box col-xs-12">
                      <span class="label">Sisa Piutang</span> 
                      <input class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="filter_piutang_sisa_piutang" type="text" >
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-12">
                      <span class="label">Keterangan</span>
                      <textarea class="form-control" id="filter_piutang_keterangan"   rows="3" ></textarea>
                    </div>
                  </div>

               <div class="row">
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_piutang_from" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_piutang_to"  maxlength="50"  placeholder="" type="text"  >
                  </div> 
               </div> 

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_piutang_filter')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_piutang()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
   $('#filter_piutang_nama_perusahaan').typeahead({
      hint: true,
      highlight: true,
      source: function (query, process) {
          $.ajax({
              url: '<?php echo $basepath ?>ajax/reqCompany',
              type: 'POST',
              dataType: 'JSON',
              data: 'query='+$('#filter_company_revoke').val(),
              success: function(data) {
                  var results = data.map(function(item) {
                      var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
                      return someItem;
                  });
                  return process(results);
              }
          });
      },
      afterSelect: function(item) {
          this.$element[0].value = item.company_name
      },
      updater: function(item) {
          $('#filter_piutang_kode_perusahaan').val(item.company_id);
          return item;
      }
    });
  function filter_piutang() {
        $("#filter_piutang_detail").html("");
        var filter_thn_pelimpahan      = $("#filter_piutang_thn_pelimpahan").val();
        var filter_tgl_piutang         = $("#filter_piutang_tanggal_piutang").val();
        var filter_kode_perusahaan     = $("#filter_piutang_kode_perusahaan").val();
        var filter_nilai_penyerahan    = $("#filter_piutang_nilai_penyerahan").val();
        var filter_tahapan_pengurusan  = $("#filter_piutang_thp_pengurusan").val();
        var filter_nama_kpknl          = $("#filter_piutang_nama_kpknl").val();
        var filter_lunas               = $("#filter_piutang_lunas").val();
        var filter_angsuran            = $("#filter_piutang_angsuran").val();
        var filter_psbdt               = $("#filter_piutang_psbdt").val();
        var filter_pembatalan          = $("#filter_piutang_pembatalan").val();
        var filter_sisa_piutang        = $("#filter_piutang_sisa_piutang").val();
        var filter_provinsi            = $("#filter_provinsi_piutang").val();
        var filter_upt                 = $("#filter_upt_piutang").val();
        var filter_keterangan          = $("#filter_piutang_keterangan").val();
        var filter_date_from           = $("#filter_date_piutang_from").val();
        var filter_date_to             = $("#filter_date_piutang_to").val();

        var param="";
        if(filter_tgl_piutang){
          param += " and pt.tanggal='"+date_to_default(filter_tgl_piutang.trim())+"' ";
        }if(filter_thn_pelimpahan){
          param += " and pt.thn_pelimpahan='"+filter_thn_pelimpahan.trim()+"' ";
        }if(filter_kode_perusahaan){
          param += " and pt.no_client ='"+filter_kode_perusahaan.trim()+"' ";
        }if(filter_nilai_penyerahan){
          param += " and pt.nilai_penyerahan like '%"+remove_char(filter_nilai_penyerahan.trim(),".")+"%' ";
        }if(filter_tahapan_pengurusan){
          param += " and pt.tahapan_pengurusan like '%"+filter_tahapan_pengurusan.trim()+"%' ";
        }if(filter_lunas){
          param += " and pt.lunas like '%"+remove_char(filter_lunas.trim(),".")+"%' ";
        }if(filter_angsuran){
          param += " and pt.angsuran like '%"+remove_char(filter_angsuran.trim(),".")+"%' ";
        }if(filter_psbdt){
          param += " and pt.psbdt like '%"+remove_char(filter_psbdt.trim(),".")+"%' ";
        }if(filter_pembatalan){
          param += " and pt.pembatalan like '%"+remove_char(filter_pembatalan.trim(),".")+"%' ";
        }if(filter_sisa_piutang){
          param += " and pt.sisa_piutang like '%"+remove_char(filter_sisa_piutang.trim(),".")+"%' ";
        }if(filter_keterangan){
          param += " and pt.keterangan like '%"+filter_keterangan.trim()+"%' ";
        }if(filter_nama_kpknl){
          param += " and (pt.nama_kpknl like '"+filter_nama_kpknl.trim()+"' or pt.nama_kpknl is null)";
        }if(filter_provinsi){
          param += " and (pt.id_prov ='"+filter_provinsi.trim()+"' or pt.id_prov is null)";
        }if(filter_upt){
          param += " and (pt.kode_upt ='"+filter_upt.trim()+"' or pt.kode_upt is null)";
        }

          if(filter_date_from && filter_date_to){
          date1 = new Date(filter_date_from); 
          date2 = new Date(filter_date_to);

          var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
          var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
          if(date1 > date2){
            swal({
                  title: 'Error',
                  html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                  type: 'error',
                  showCancelButton: false,
                  showLoaderOnConfirm: false,
              });
          }
          else {
             param += " and DATE_FORMAT(pt.created_date , '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
          }
        }
        else if(filter_date_from){
             param += " and DATE_FORMAT(pt.created_date , '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
        }
        else if(filter_date_to){
            param += " and DATE_FORMAT(pt.created_date , '%Y-%m-%d')='"+date_to_default(filter_date_to.trim())+"' ";
        }

        $("#Piutang_Pencarian").val(btoa(param));
         $.ajax({
                url: '<?php echo $basepath ?>pelayanan_piutang/table_detail_filter',
                type: 'POST',
                data: 'param='+btoa(param),
                success: function(data) {
                    $('#filter_piutang_detail').html(data);
                }
            });
        $('#Filter_PiutangDetail').modal('hide');
   }  
</script>

<!-- Filter Unar All -->
<div id="Filter_UnarAll" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_unar_filter_all" method="POST">

                  <div class="row">   
                    <div class="col-md-12 form-group form-box col-xs-12">
                      <center style="font-weight:bold;font-weight:50px">Tanggal Pelaksanaan UNAR</center>
                    </div>
                  </div>

                  <div class="row">  
                    <div class="col-md-6 form-group form-box col-xs-12">
                      <span class="label">Tanggal Pelaksanaan Ujian</span> 
                      <input class="form-control tgl" id="filter_unar_tgl_ujian_all" name="tgl_unar"  readonly style="background-color:white" type="text"   >
                    </div>
                    <div class="col-md-6 form-group form-box col-xs-12">
                      <span class="label">Lokasi Ujian</span>
                      <input  class="form-control" maxlength="255" id="filter_unar_lokasi_ujian_all" name="lokasi_ujian"   placeholder="" type="text"   >
                    </div>
                  </div>

                  <div class="row"> 
                    <div class="col-md-12 form-group form-box col-xs-12">
                        <center style="font-weight:bold;font-weight:50px">Jumlah Peserta</center>
                    </div>
                  </div>

                  <div class="row"> 
                      <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">YD</span>
                        <input onkeypress='return only_number(event);' class="form-control" id="filter_unar_jumlah_yd_all" name="jumlah_yd"  min="0"  type="number"  >
                      </div>
                      <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">YB</span> 
                        <input onkeypress='return only_number(event);' class="form-control" id="filter_unar_jumlah_yb_all" name="jumlah_yb" min="0"   type="number"  >
                      </div>
                      <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">YC</span>
                        <input onkeypress='return only_number(event);' class="form-control" id="filter_unar_jumlah_yc_all" name="jumlah_yc"  min="0" type="number"  >
                      </div>
                  </div>


                  <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-12">
                        <center style="font-weight:bold;font-weight:50px">Lulus</center>
                    </div>
                  </div>

                  <div class="row">
                      <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">YD</span>
                        <input onkeypress='return only_number(event);' class="form-control" id="filter_unar_lulus_yd_all" name="lulus_yd" min="0"  type="number"  >
                      </div>
                      <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">YB</span> 
                        <input onkeypress='return only_number(event);' class="form-control" id="filter_unar_lulus_yb_all" name="lulus_yb" min="0"  type="number"  >
                      </div>
                      <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">YC</span>
                        <input onkeypress='return only_number(event);' class="form-control" id="filter_unar_lulus_yc_all" name="lulus_yc" min="0"  type="number"  >
                      </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-12">
                        <center style="font-weight:bold;font-weight:50px">Tidak Lulus</center>
                    </div>
                  </div>

                  <div class="row">
                      <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">YD</span>
                        <input onkeypress='return only_number(event);' class="form-control" id="filter_unar_tdk_lulus_yd_all" name="tdk_lulus_yd" min="0"  type="number"  >
                      </div>
                      <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">YB</span> 
                        <input onkeypress='return only_number(event);' class="form-control" id="filter_unar_tdk_lulus_yb_all" name="tdk_lulus_yb" min="0"   type="number"  >
                      </div>
                      <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">YC</span>
                        <input onkeypress='return only_number(event);' class="form-control" id="filter_unar_tdk_lulus_yc_all" name="tdk_lulus_yc" min="0"  type="number"  >
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-12 form-group form-box col-xs-12">
                        <span class="label">Keterangan</span>
                        <textarea class="form-control" id="filter_unar_keterangan_all" name="keterangan"  rows="3" ></textarea>
                      </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" >
                      <span class="label">Provinsi</span>  
                      <select class="form-control" onchange="call_upt_filter_unar_all()" id="filter_unar_upt_provinsi_all" name="id_prov"  >
                        <option value="">Pilih Provinsi</option>
                        <?php 
                          $data_shf = $gen_model->GetWhere('ms_provinsi');
                            while($list = $data_shf->FetchRow()){
                              foreach($list as $key=>$val){
                                          $key=strtolower($key);
                                          $$key=$val;
                                        }  
                          ?>
                        <option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>

                  <div class="row">
                      <div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
                        <span class="label">Kabupaten / Kota</span>  
                        <select class="form-control"  id="filter_unar_kabkot_all" name="kabkot"  >
                          <option value="">Pilih Kabupaten / Kota</option>
                        </select>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
                        <span class="label">Nama UPT</span>  
                        <select class="form-control"  id="filter_unar_kode_upt_all" name="loket_upt"  >
                          <option value="">Pilih UPT</option>
                        </select>
                      </div>
                  </div>


               <div class="row">
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_unar_date_from_all" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_unar_date_to_all"  maxlength="50"  placeholder="" type="text"  >
                  </div> 
               </div> 

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_unar_filter_all')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_unar_all()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
  function call_upt_filter_unar_all(){
    var prov = $("#filter_unar_upt_provinsi_all").val();

        //Kabkot
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_kabkot",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#filter_unar_kabkot_all").html(msg);        
                }
             }
          }); 

        //Upt
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#filter_unar_kode_upt_all").html(msg);        
                }
             }
          }); 
  }
  function filter_unar_all() {
      $("#filter_unar_all").html("");
      var filter_tgl_ujian           = $("#filter_unar_tgl_ujian_all").val();
      var filter_lokasi              = $("#filter_unar_lokasi_ujian_all").val();
      var filter_jumlah_yd           = $("#filter_unar_jumlah_yd_all").val();
      var filter_jumlah_yb           = $("#filter_unar_jumlah_yb_all").val();
      var filter_jumlah_yc           = $("#filter_unar_jumlah_yc_all").val();
      var filter_lulus_yd            = $("#filter_unar_lulus_yd_all").val();
      var filter_lulus_yb            = $("#filter_unar_lulus_yb_all").val();
      var filter_lulus_yc            = $("#filter_unar_lulus_yc_all").val();
      var filter_tdk_lulus_yd        = $("#filter_unar_tdk_lulus_yd_all").val();
      var filter_tdk_lulus_yb        = $("#filter_unar_tdk_lulus_yb_all").val();
      var filter_tdk_lulus_yc        = $("#filter_unar_tdk_lulus_yc_all").val();
      var filter_provinsi            = $("#filter_unar_upt_provinsi_all").val();
      var filter_kabkot              = $("#filter_unar_kabkot_all").val();
      var filter_upt                 = $("#filter_unar_kode_upt_all").val();
      var filter_keterangan          = $("#filter_unar_keterangan_all").val();
      var filter_date_from           = $("#filter_unar_date_from_all").val();
      var filter_date_to             = $("#filter_unar_date_to_all").val();

      var param="";
      if(filter_tgl_ujian){
        param += " and un.tgl_ujian ='"+date_to_default(filter_tgl_ujian.trim())+"' ";
      }if(filter_lokasi){
        param += " and un.lokasi_ujian like '%"+filter_lokasi.trim()+"%' ";
      }if(filter_jumlah_yd){
        param += " and un.jumlah_yd like '%"+filter_jumlah_yd.trim()+"%' ";
      }if(filter_jumlah_yb){
        param += " and un.jumlah_yb like '%"+filter_jumlah_yb.trim()+"%' ";
      }if(filter_jumlah_yc){
        param += " and un.jumlah_yc like '%"+filter_jumlah_yc.trim()+"%' ";
      }if(filter_lulus_yd){
        param += " and un.lulus_yd like '%"+filter_lulus_yd.trim()+"%' ";
      }if(filter_lulus_yb){
        param += " and un.lulus_yb like '%"+filter_lulus_yb.trim()+"%' ";
      }if(filter_lulus_yc){
        param += " and un.lulus_yc like '%"+filter_lulus_yc.trim()+"%' ";
      }if(filter_tdk_lulus_yd){
        param += " and un.tdk_lulus_yd like '%"+filter_tdk_lulus_yd.trim()+"%' ";
      }if(filter_tdk_lulus_yb){
        param += " and un.tdk_lulus_yb like '%"+filter_tdk_lulus_yb.trim()+"%' ";
      }if(filter_tdk_lulus_yc){
        param += " and un.tdk_lulus_yc like '%"+filter_tdk_lulus_yc.trim()+"%' ";
      }if(filter_provinsi){
        param += " and un.id_prov ='"+filter_provinsi.trim()+"' ";
      }if(filter_kabkot){
        param += " and un.id_kabkot ='"+filter_kabkot.trim()+"' ";
      }if(filter_upt){
        param += " and un.kode_upt ='"+filter_upt.trim()+"' ";
      }if(filter_keterangan){
        param += " and un.keterangan like '%"+filter_keterangan.trim()+"%' ";
      }

        if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(un.created_date , '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(un.created_date , '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(un.created_date , '%Y-%m-%d')='"+date_to_default(filter_date_to.trim())+"' ";
      }

      $("#UnarPencarian_all").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>pelayanan_unar/table_all_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_unar_all').html(data);
              }
          });
      $('#Filter_UnarAll').modal('hide');
  }  
</script>

<!-- Filter Unar Detail -->
<div id="Filter_UnarDetail" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_unar_filter" method="POST">
                <div class="row">   
                    <div class="col-md-12 form-group form-box col-xs-12">
                      <center style="font-weight:bold;font-weight:50px">Tanggal Pelaksanaan UNAR</center>
                    </div>
                  </div>

                  <div class="row">  
                    <div class="col-md-6 form-group form-box col-xs-12">
                      <span class="label">Tanggal Pelaksanaan Ujian</span> 
                      <input class="form-control tgl" id="filter_unar_tgl_ujian_all" name="tgl_unar"  readonly style="background-color:white" type="text"   >
                    </div>
                    <div class="col-md-6 form-group form-box col-xs-12">
                      <span class="label">Lokasi Ujian</span>
                      <input  class="form-control" maxlength="255" id="filter_unar_lokasi_ujian" name="lokasi_ujian"   placeholder="" type="text"   >
                    </div>
                  </div>

                  <div class="row"> 
                    <div class="col-md-12 form-group form-box col-xs-12">
                        <center style="font-weight:bold;font-weight:50px">Jumlah Peserta</center>
                    </div>
                  </div>

                  <div class="row"> 
                      <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">YD</span>
                        <input onkeypress='return only_number(event);' class="form-control" id="filter_unar_jumlah_yd" name="jumlah_yd"  min="0"  type="number"  >
                      </div>
                      <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">YB</span> 
                        <input onkeypress='return only_number(event);' class="form-control" id="filter_unar_jumlah_yb" name="jumlah_yb" min="0"   type="number"  >
                      </div>
                      <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">YC</span>
                        <input onkeypress='return only_number(event);' class="form-control" id="filter_unar_jumlah_yc" name="jumlah_yc"  min="0" type="number"  >
                      </div>
                  </div>


                  <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-12">
                        <center style="font-weight:bold;font-weight:50px">Lulus</center>
                    </div>
                  </div>

                  <div class="row">
                      <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">YD</span>
                        <input onkeypress='return only_number(event);' class="form-control" id="filter_unar_lulus_yd" name="lulus_yd" min="0"  type="number"  >
                      </div>
                      <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">YB</span> 
                        <input onkeypress='return only_number(event);' class="form-control" id="filter_unar_lulus_yb" name="lulus_yb" min="0"  type="number"  >
                      </div>
                      <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">YC</span>
                        <input onkeypress='return only_number(event);' class="form-control" id="filter_unar_lulus_yc" name="lulus_yc" min="0"  type="number"  >
                      </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12 form-group form-box col-xs-12">
                        <center style="font-weight:bold;font-weight:50px">Tidak Lulus</center>
                    </div>
                  </div>

                  <div class="row">
                      <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">YD</span>
                        <input onkeypress='return only_number(event);' class="form-control" id="filter_unar_tdk_lulus_yd" name="tdk_lulus_yd" min="0"  type="number"  >
                      </div>
                      <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">YB</span> 
                        <input onkeypress='return only_number(event);' class="form-control" id="filter_unar_tdk_lulus_yb" name="tdk_lulus_yb" min="0"   type="number"  >
                      </div>
                      <div class="col-md-4 form-group form-box col-xs-12">
                        <span class="label">YC</span>
                        <input onkeypress='return only_number(event);' class="form-control" id="filter_unar_tdk_lulus_yc" name="tdk_lulus_yc" min="0"  type="number"  >
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-12 form-group form-box col-xs-12">
                        <span class="label">Keterangan</span>
                        <textarea class="form-control" id="filter_unar_keterangan" name="keterangan"  rows="3" ></textarea>
                      </div>
                  </div>

               

                  <div class="row">
                      <div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
                        <span class="label">Kabupaten / Kota</span>  
                        <select class="form-control"  id="filter_unar_kabkot" name="kabkot"  >
                          <option value="">Pilih Kabupaten / Kota</option>
                        </select>
                      </div>
                  </div>
               <div class="row">
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_unar_date_from" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_unar_date_to"  maxlength="50"  placeholder="" type="text"  >
                  </div> 
               </div> 
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_unar_filter')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_unar()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
 function filter_unar() {
      $("#filter_unar_detail").html("");
      var filter_tgl_ujian           = $("#filter_unar_tgl_ujian").val();
      var filter_lokasi              = $("#filter_unar_lokasi_ujian").val();
      var filter_jumlah_yd           = $("#filter_unar_jumlah_yd").val();
      var filter_jumlah_yb           = $("#filter_unar_jumlah_yb").val();
      var filter_jumlah_yc           = $("#filter_unar_jumlah_yc").val();
      var filter_lulus_yd            = $("#filter_unar_lulus_yd").val();
      var filter_lulus_yb            = $("#filter_unar_lulus_yb").val();
      var filter_lulus_yc            = $("#filter_unar_lulus_yc").val();
      var filter_tdk_lulus_yd        = $("#filter_unar_tdk_lulus_yd").val();
      var filter_tdk_lulus_yb        = $("#filter_unar_tdk_lulus_yb").val();
      var filter_tdk_lulus_yc        = $("#filter_unar_tdk_lulus_yc").val();
      var filter_provinsi            = $("#filter_provinsi_unar").val();
      var filter_kabkot              = $("#filter_unar_kabkot").val();
      var filter_upt                 = $("#filter_upt_unar").val();
      var filter_keterangan          = $("#filter_unar_keterangan").val();
      var filter_date_from           = $("#filter_unar_date_from").val();
      var filter_date_to             = $("#filter_unar_date_to").val();

      var param="";
      if(filter_tgl_ujian){
        param += " and un.tgl_ujian ='"+date_to_default(filter_tgl_ujian.trim())+"' ";
      }if(filter_lokasi){
        param += " and un.lokasi_ujian like '%"+filter_lokasi.trim()+"%' ";
      }if(filter_jumlah_yd){
        param += " and un.jumlah_yd like '%"+filter_jumlah_yd.trim()+"%' ";
      }if(filter_jumlah_yb){
        param += " and un.jumlah_yb like '%"+filter_jumlah_yb.trim()+"%' ";
      }if(filter_jumlah_yc){
        param += " and un.jumlah_yc like '%"+filter_jumlah_yc.trim()+"%' ";
      }if(filter_lulus_yd){
        param += " and un.lulus_yd like '%"+filter_lulus_yd.trim()+"%' ";
      }if(filter_lulus_yb){
        param += " and un.lulus_yb like '%"+filter_lulus_yb.trim()+"%' ";
      }if(filter_lulus_yc){
        param += " and un.lulus_yc like '%"+filter_lulus_yc.trim()+"%' ";
      }if(filter_tdk_lulus_yd){
        param += " and un.tdk_lulus_yd like '%"+filter_tdk_lulus_yd.trim()+"%' ";
      }if(filter_tdk_lulus_yb){
        param += " and un.tdk_lulus_yb like '%"+filter_tdk_lulus_yb.trim()+"%' ";
      }if(filter_tdk_lulus_yc){
        param += " and un.tdk_lulus_yc like '%"+filter_tdk_lulus_yc.trim()+"%' ";
      }if(filter_keterangan){
        param += " and un.keterangan like '%"+filter_keterangan.trim()+"%' ";
      }if(filter_kabkot){
        param += " and (un.id_kabkot ='"+filter_kabkot.trim()+"' or un.id_kabkot is null)";
      }if(filter_provinsi){
        param += " and (un.id_prov ='"+filter_provinsi.trim()+"' or un.id_prov is null)";
      }if(filter_upt){
        param += " and (un.kode_upt ='"+filter_upt.trim()+"' or un.kode_upt is null)";
      }

        if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(un.created_date , '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(un.created_date , '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(un.created_date , '%Y-%m-%d')='"+date_to_default(filter_date_to.trim())+"' ";
      }

      $("#UnarPencarian").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>pelayanan_unar/table_detail_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_unar_detail').html(data);
              }
          });
      $('#Filter_UnarDetail').modal('hide');
  }  
</script>


<!-- Modal Import -->

<!-- Import Loket Pelayanan All -->
<div id="Import_LoketPelayananAll" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_LoketPelayanan_All" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>

         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_loket_pelayanan_all.xls" download>Download Sample File</a><br/><br/>
         <span style="font-size: 14px;">File Import Membutuhkan Kode UPT, Kode Provinsi, Kode Perusahaan, Kode Group/Sub Perizinan & Kode Tujuan </span><br/><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>company">List Data Perusahaan</a><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>upt">List Data UPT & Provinsi</a><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>grp_layanan">List Data Group/Sub Perizinan</a><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>tujuan">List Data Tujuan</a>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<!-- Import Loket Pelayanan Detail Import -->
<div id="Import_LoketPelayananDetail" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_LoketPelayananDetail" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="hidden" id="my_id_prov" name="id_prov">
          <input type="hidden" id="my_id_upt" name="id_upt">
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>

         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_loket_pelayanan.xls" download>Download Sample File</a><br/><br/>
         <span style="font-size: 14px;">File Import Membutuhkan Kode Perusahaan, Kode Group/Sub Perizinan & Kode Tujuan </span><br/><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>company">List Data Perusahaan</a><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>grp_layanan">List Data Group/Sub Perizinan</a><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>tujuan">List Data Tujuan</a>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<!-- Import SPP All Import -->
<div id="Import_SPPAll" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_SPP_All" autocomplete="off" enctype="multipart/form-data">
        <input type="hidden" id="replace_import_spp_all" name="replace_import_spp_all">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>

         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_spp_all.xls" download>Download Sample File</a><br/><br/>
         <span style="font-size: 14px;">File Import Membutuhkan Kode UPT, Kode Provinsi, Kode Metode & No SPP</span><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>upt">List Data UPT & Provinsi</a><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>metode">List Metode</a><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>kategori_spp">List Kategori SPP</a>
        </div>
        <div class="modal-footer">
          <button type="submit"  class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<!-- Import SPP Detail Import -->
<div id="Import_SPPDetail" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_SPP_Detail" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
          <input type="hidden" id="replace_import_spp_detail_all" name="replace_import_spp_detail_all">
          <input type="hidden" id="my_id_prov_spp" name="id_prov">
          <input type="hidden" id="my_id_upt_spp" name="loket_upt">
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_spp.xls" download>Download Sample File</a><br/><br/>
         <span style="font-size: 14px;">File Import Membutuhkan No SPP & Kode Metode</span><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>metode">List Metode</a><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>kategori_spp">List Kategori SPP</a>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<!-- Import ISR All -->
<div id="Import_ISRAll" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_ISR_ALL" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_isr_all.xls" download>Download Sample File</a><br/><br/>
         <span style="font-size: 14px;">File Import Membutuhkan No SPP dari SPP, Kode UPT, Kode Provinsi, Kode Metode</span><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>upt">List Data UPT & Provinsi</a><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>metode">List Metode</a>
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<!-- Import ISR Detail -->
<div id="Import_ISRDetail" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_ISR_Detail" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
          <input type="hidden" id="my_id_prov_isr" name="id_prov">
          <input type="hidden" id="my_id_upt_isr" name="id_upt">
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_isr.xls" download>Download Sample File</a><br/><br/>
         <span style="font-size: 14px;">File Import Membutuhkan No SPP dari SPP, Kode Metode</span><br/>
          - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>metode">List Metode</a>
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<!-- Import Revoke All -->
<div id="Import_RevokeAll" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_Revoke_All" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_revoke_all.xls" download>Download Sample File</a><br/><br/>
         <span style="font-size: 14px;">File Import Membutuhkan No SPP dari SPP, Kode Provinsi, Kode Metode</span><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>upt">List Data UPT & Provinsi</a><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>metode">List Metode</a>
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<!-- Import Revoke Detail -->
<div id="Import_RevokeDetail" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_Revoke_Detail" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
          <input type="hidden" id="my_id_prov_revoke" name="id_prov">
          <input type="hidden" id="my_id_upt_revoke" name="id_upt">
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_revoke.xls" download>Download Sample File</a><br/><br/>
         <span style="font-size: 14px;">File Import Membutuhkan No SPP dari SPP, Kode Metode</span><br/>
          - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>metode">List Metode</a>
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<!-- Import Piutang All -->
<div id="Import_PiutangAll" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_Piutang_All" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_penanganan_piutang_all.xls" download>Download Sample File</a><br/><br/>
         <span style="font-size: 14px;">File Import Membutuhkan Kode Perusahaan, Kode Kabupaten/Kota</span><br/>
          - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>company">List Data Perusahaan</a><br/>
          - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>kabkot">List Data Kabupaten / Kota</a>
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<!-- Import Piutang Detail -->
<div id="Import_PiutangDetail" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_Piutang_Detail" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
          <input type="hidden" id="my_id_prov_piutang" name="id_prov">
          <input type="hidden" id="my_id_upt_piutang" name="id_upt">
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_penanganan_piutang.xls" download>Download Sample File</a><br/><br/>
         <span style="font-size: 14px;">File Import Membutuhkan Kode Perusahaan & Kode Kabupaten/Kota</span><br/>
          - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>company">List Data Perusahaan</a><br/>
         </div>
        <div id="table_kabkot_piutang"></div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>


<!-- Import UNAR All -->
<div id="Import_UnarAll" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_Unar_All" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_unar_all.xls" download>Download Sample File</a><br/><br/>
         <span style="font-size: 14px;">File Import Membutuhkan Kode UPT, Provinsi, Kode Kabupaten/Kota</span><br/>
          - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>upt">List Data UPT & Provinsi</a><br/>
          - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>kabkot">List Data Kabupaten / Kota</a>
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<!-- Import UNAR Detail -->
<div id="Import_UnarDetail" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_Unar_Detail" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
          <input type="hidden" id="my_id_prov_unar" name="id_prov">
          <input type="hidden" id="my_id_upt_unar" name="loket_upt">
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_unar.xls" download>Download Sample File</a><br/><br/>
         <span style="font-size: 14px;">File Import Membutuhkan Kode Kabupaten/Kota</span><br/>
          
            <div id="table_kabkot"></div>
            
          
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>


<script type="text/javascript">
   //Import Loket Pelayanan
  $("#DoImport_LoketPelayananDetail").on("submit", function (event) {
    event.preventDefault();
        var fltr = $("#filter_provinsi").val();
        var fltr_upt = $("#filter_upt").val();
        $("#my_id_prov").val(fltr);
        $("#my_id_upt").val(fltr_upt);
        do_act('DoImport_LoketPelayananDetail','pelayanan_loket/import','no_refresh','Import File Loket Pelayanan','Apakah anda ingin import file loket pelayanan ini ?','info','refresh_table');
    });

  $("#DoImport_LoketPelayanan_All").on("submit", function (event) {
    event.preventDefault();
        do_act('DoImport_LoketPelayanan_All','pelayanan_loket/import_all','no_refresh','Import File Loket Pelayanan','Apakah anda ingin import file loket pelayanan ini ?','info','refresh_table');
    });

  

  //Import SPP
  $("#DoImport_SPP_All").on("submit", function (event) {
    event.preventDefault();
        do_import_confirm('DoImport_SPP_All','pelayanan_spp/import_all_check','no_refresh','Import File SPP','Apakah anda ingin import file spp ini ?','info','import_confrim_spp_all','replace_import_spp_all');
    });

     $("#DoImport_SPP_Detail").on("submit", function (event) {
        var fltr = $("#filter_provinsi_spp").val();
        var fltr_upt = $("#filter_upt_spp").val();
        $("#my_id_prov_spp").val(fltr);
        $("#my_id_upt_spp").val(fltr_upt);
     event.preventDefault();
        do_import_confirm('DoImport_SPP_Detail','pelayanan_spp/import_detail_check','no_refresh','Import File SPP','Apakah anda ingin import file spp ini ?','info','import_confrim_spp_detail','replace_import_spp_detail_all');
    }); 

function import_confrim_spp_all(form_id,act_controller,after_controller){
   $.ajax({
      url: 'pelayanan_spp/import_all', 
      type: 'POST',
      data: new FormData($('#DoImport_SPP_All')[0]),  // Form ID
      processData: false,
      contentType: false,
      beforeSend: function() {
        call_loading('show');
      },
      success: function(data) {
        call_loading('none');
        swal({
            title: 'Success',
            type: 'success',
            showCancelButton: false,
            showLoaderOnConfirm: false,
          }).then(function() {
             refresh_table(); 
             $("#Import_SPPAll").modal('hide');  
        });
      }
    });
}
function import_confrim_spp_detail(form_id,act_controller,after_controller){
   $.ajax({
      url: 'pelayanan_spp/import_detail', 
      type: 'POST',
      data: new FormData($('#DoImport_SPP_Detail')[0]),  // Form ID
      processData: false,
      contentType: false,
      beforeSend: function() {
        call_loading('show');
      },
      success: function(data) {
        call_loading('none');
        swal({
            title: 'Success',
            type: 'success',
            showCancelButton: false,
            showLoaderOnConfirm: false,
          }).then(function() {
             refresh_table(); 
             $("#Import_SPPDetail").modal('hide');  
        });
      }
    });
}

 //Import ISR
  $("#DoImport_ISR_ALL").on("submit", function (event) {
    event.preventDefault();
        do_act('DoImport_ISR_ALL','pelayanan_isr/import_all','no_refresh','Import File ISR','Apakah anda ingin import file ISR ini ?','info','refresh_table');
    });

     $("#DoImport_ISR_Detail").on("submit", function (event) {
        var fltr = $("#filter_provinsi_isr").val();
        var fltr_upt = $("#filter_upt_isr").val();
        $("#my_id_prov_isr").val(fltr);
        $("#my_id_upt_isr").val(fltr_upt);
     event.preventDefault();
        do_act('DoImport_ISR_Detail','pelayanan_isr/import_detail','no_refresh','Import File ISR','Apakah anda ingin import file ISR ini ?','info','refresh_table');
    }); 

    //Import Revoke
  $("#DoImport_Revoke_All").on("submit", function (event) {
    event.preventDefault();
        do_act('DoImport_Revoke_All','pelayanan_revoke/import_all','no_refresh','Import File Revoke','Apakah anda ingin import file revoke ini ?','info','refresh_table');
    });

     $("#DoImport_Revoke_Detail").on("submit", function (event) {
        var fltr = $("#filter_provinsi_revoke").val();
        var fltr_upt = $("#filter_upt_revoke").val();
        $("#my_id_prov_revoke").val(fltr);
        $("#my_id_upt_revoke").val(fltr_upt);
     event.preventDefault();
        do_act('DoImport_Revoke_Detail','pelayanan_revoke/import_detail','no_refresh','Import File Revoke','Apakah anda ingin import file revoke ini ?','info','refresh_table');
    });  

  //Import Penanganan Piutang
   $("#DoImport_Piutang_All").on("submit", function (event) {
    event.preventDefault();
        do_act('DoImport_Piutang_All','pelayanan_piutang/import_all','no_refresh','Import File Penanganan Piutang','Apakah anda ingin import file Penanganan Piutang ini ?','info','refresh_table');
    });

   $("#DoImport_Piutang_Detail").on("submit", function (event) {
    event.preventDefault();
        var fltr = $("#filter_provinsi").val();
        var fltr_upt = $("#filter_upt_piutang").val();
        $("#my_id_prov_piutang").val(fltr);
        $("#my_id_upt_piutang").val(fltr_upt);
        do_act('DoImport_Piutang_Detail','pelayanan_piutang/import_detail','no_refresh','Import File Penanganan Piutang','Apakah anda ingin import file Penanganan Piutang ini ?','info','refresh_table');
    });

  //Import Unar
   $("#DoImport_Unar_All").on("submit", function (event) {
    event.preventDefault();
        do_act('DoImport_Unar_All','pelayanan_unar/import_all','no_refresh','Import File UNAR','Apakah anda ingin import file unar ini ?','info','refresh_table');
    });

   $("#DoImport_Unar_Detail").on("submit", function (event) {
    event.preventDefault();
        var fltr = $("#filter_provinsi").val();
        var fltr_upt = $("#filter_upt_unar").val();
        $("#my_id_prov_unar").val(fltr);
        $("#my_id_upt_unar").val(fltr_upt);
        do_act('DoImport_Unar_Detail','pelayanan_unar/import_detail','no_refresh','Import File UNAR','Apakah anda ingin import file unar ini ?','info','refresh_table');
    });
   $(".thn").datepicker({
     format: 'yyyy',
     viewMode: "years", 
       minViewMode: "years"
  });
</script>
