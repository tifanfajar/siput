<?php  
 $check_akses = $gen_model->GetOne("fua_read","tr_function_access",array('id_menu'=>5,'kode_function_access'=>$_SESSION['group_user']));
 if(empty($check_akses)){
    echo '<meta http-equiv="refresh" content="0;url='.$basepath.'home">';
 }
 else { ?>
<title>Monev Kinerja - <?php echo str_replace('<br/>','',$web['judul']) ?></title>


<style type="text/css">
  #example_paginate{position: relative; left: 40%; top: 5px;}
  .pagination li a{margin:5px 15px; }
  .dataTables_info{margin-bottom: 15px;}
  .datatable>tbody>tr>td
  {
    white-space: nowrap;
  } 
  .table th {
     text-align: center;
     font-weight:bold;
  }
</style>
<body class="pelayanan-cnt">


  <?php  
  include "view/top_menu.php";  
  $akses_add_tab_monev_kinerja = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>12,'kode_function_access'=>$_SESSION['group_user'])); 
  $akses_add_tab_monev_kinerja      = (!empty($akses_add_tab_monev_kinerja) ? $akses_add_tab_monev_kinerja : '0');
  ?>
  
  <!-- Main Container -->
  <div class="container-fluid animated fadeInUp" style="z-index: 3;">
    <div class="row">
  <?php  
    include "view/menu.php";  
   ?>
  <!-- Content -->
    <div class="container-fluid main-content" id="div_content" >
      <div class="row">


        <ul class="nav nav-tabs main-nav-tab" id="myTab" role="tablist">
          <?php 
              $data_tb = $db->SelectLimit("select * from ms_menu_tab where men_id='5' order by urutan asc"); 
              $no_tab = 1;
              $men_active =""; 
              $total_tab = 0;
              while($list = $data_tb->FetchRow()){
                  foreach($list as $key=>$val){
                        $key=strtolower($key);
                        $$key=$val;
                      } 
                      $whr_tab = array();
                      $whr_tab['kode_function_access'] = $_SESSION['group_user'];
                      $whr_tab['id_tab'] = $tab_id;
                      $check_tab = $gen_model->GetOne("fua_read","tr_function_access_tab",$whr_tab);
                     if(!empty($check_tab)){ 
                      $active_tab ="";   
                      if($no_tab==1) {
                           $active_tab ="active";
                           $men_active = $tab_url;   
                      }
                      ?>  
                      <li class="nav-item">
                        <a class="nav-link <?php echo $active_tab ?>" onclick="refresh_table()" id="<?php echo $tab_url ?>-tab" data-toggle="tab" href="#<?php echo $tab_url ?>" role="tab" aria-controls="<?php echo $tab_url ?>" aria-selected="true"><?php echo $tab ?></a>
                      </li>
                   <?php $no_tab++;  
                   $total_tab++;  
                 } 
              } 

              function active_tab($list_tab){
                    global $men_active;
                    if($men_active==$list_tab){
                       return "show active";
                    }
              }
              ?>
        </ul>


        <?php  if($total_tab!=0){  ?>
        <div class="tab-content" id="myTabContent">
          <!-- Tab Bahan Sosialisasi -->
          <div class="tab-pane fade <?php echo active_tab("monev_kinerja") ?>" id="monev_kinerja" role="tabpanel" aria-labelledby="home-tab">
           
            <!-- Peta -->
            <label class="judul-peta if_upt monev_kinerja_utama"><i class="fa fa-map-marked-alt"></i> Peta Lokasi UPT</label>
            <hr class="if_upt monev_kinerja_utama">
            <div class="peta-cnt" id="peta_monev_kinerja"></div>

            <!-- Grafik Chart -->
            <div class="row if_upt monev_kinerja_utama" >
                <div class="col-sm-12" id="div_monev_kinerja_grafik_1">
                  <div class="card-set">
                    <div class="headset"><i class="fa fa-chart-pie"></i> Grafik Jumlah Per Provinsi</div>
                    <div class="img-chart">
                      <!-- <img src="images/chart.jpg" alt=""> -->
                      <div id="monev_kinerja_grafik" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                    </div>
                  </div>
                </div><br/>
            </div>
            <div class="row">
                <div class="col-sm-12" id="monev_kinerja_table_data_utama"></div>
            </div>
            <input type="hidden" name="MonevKinerjaPencarian_all" id="MonevKinerjaPencarian_all" >
            <script type="text/javascript">
              function print_laporan_monev_kinerja_all(tipe){
                  var param = $("#MonevKinerjaPencarian_all").val();

                    var form = document.createElement("form");
                    form.setAttribute("method", "POST");
                    if(tipe=="print"){
                     form.setAttribute("target", "_blank");
                    }
                    form.setAttribute("action", "<?php echo $basepath ?>monev_kinerja/cetak_all");
                           
                        var hiddenField = document.createElement("input");
                        hiddenField.setAttribute("type", "hidden");
                        hiddenField.setAttribute("name", "param");
                        hiddenField.setAttribute("value", param);

                        var hiddenField2 = document.createElement("input");
                        hiddenField2.setAttribute("type", "hidden");
                        hiddenField2.setAttribute("name", "tipe");
                        hiddenField2.setAttribute("value", tipe);

                        form.appendChild(hiddenField);
                        form.appendChild(hiddenField2);

                    document.body.appendChild(form);
                    form.submit();
                }
            </script>
            <!-- Tabel -->
            <div class="cnt-heaeder monev_kinerja_utama" style="margin-top: 30px; margin-bottom: 20px;">
              <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-portrait"></i> Monev Kinerja</h6>
              
              <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">
              
                <?php  if($akses_add_tab_monev_kinerja="1"){  ?>
                  <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Tambah" onclick="call_modal_default('Tambah Monev Kinerja','Form_Monev_Kinerja_ALL','do_add','monev_kinerja/form_all')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                 <?php } ?>

                <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Cetak"  onclick="print_laporan_monev_kinerja_all('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

                <?php  if($akses_add_tab_monev_kinerja="1"){  ?>
                  <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Import" data-target="#Import_MonevKinerjaAll"><i class="fa fa-upload"></i></a>
                <?php } ?>
                
                <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Filterisasi" data-target="#Filter_MonevKinerjaAll"  ><i class="fa fa-filter"></i></a>

                <div class="btn-group" role="group" data-tooltip="tooltip" data-placement="bottom" title="Export">
                  <button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-file-pdf"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="red">
                    <a class="dropdown-item"  onclick="print_laporan_monev_kinerja_all('pdf')" href="javascript:void(0)">Export to PDF</a>
                    <a class="dropdown-item"   onclick="print_laporan_monev_kinerja_all('xls')" href="javascript:void(0)">Export to XLS</a>
                    <a class="dropdown-item" onclick="print_laporan_monev_kinerja_all('doc')"   href="javascript:void(0)">Export to DOCS</a>
                  </div>
                </div>
              </div>
              <hr style="width: 100%; float: left; display: block;">
            </div>

            <div id="filter_monev_kinerja_all" class="table table-responsive pelayanan-top monev_kinerja_utama">
              <table id="tb_monev_kinerja" class="display table table-striped pelayanan-table datatable">
                <thead>
                  <tr>
                    <th class="text-center">Tanggal Buat</th>
                    <th class="text-center">Provinsi</th>
                    <th class="text-center">UPT</th>
                    <th class="text-center">Kinerja</th>
                    <th class="text-center">Indikator Kinerja Kegiatan</th>
                    <th class="text-center">Target</th>
                    <th class="text-center">Renaksi Kinerja Target Uraian</th>
                    <th class="text-center">Pagu Anggaran</th>
                    <th class="text-center">Realisasi</th>
                    <th class="text-center">Data Dukung</th>
                    <th class="text-center">Keterangan</th>
                    <th class="text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            <script type="text/javascript">
              var dTable1;
              $(document).ready(function() {
                dTable1 = $('#tb_monev_kinerja').DataTable( {
                  "bProcessing": true,
                  "searching": false,
                  "bServerSide": true,
                  "bJQueryUI": false,
                  "retrieve": true,
                  "responsive": false,
                  "autoWidth": false,
                  "sAjaxSource": "<?php echo $basepath ?>monev_kinerja/rest", 
                  "sServerMethod": "POST",
                  "scrollX": true,
                  "scrollY": "350px",
                    "scrollCollapse": true,
                    "order": [[ 0, "desc" ]],
                  "columnDefs": [
                  { "orderable": true, "targets": 0, "searchable": true},
                  { "orderable": true, "targets": 1, "searchable": true},
                  { "orderable": true, "targets": 2, "searchable": true},
                  { "orderable": true, "targets": 3, "searchable": true},
                  { "orderable": true, "targets": 4, "searchable": true },
                  { "orderable": true, "targets": 5, "searchable": true },
                  { "orderable": true, "targets": 6, "searchable": true },
                  { "orderable": true, "targets": 7, "searchable": true },
                  { "orderable": true, "targets": 8, "searchable": true },
                  { "orderable": true, "targets": 9, "searchable": true },
                  { "orderable": true, "targets": 10, "searchable": true },
                  { "orderable": false, "targets": 11, "searchable": false, "width":200}
                  ]
                } );
              });
            </script>
            </div>    
          </div>
        </div>
        <?php } else { 
        echo "<div class='row'>
                <div class='col-md-12'>
                    <h3>Anda tidak mempunyai akses apapun pada halaman ini</h3>
                </div>
              </div>"; 
        echo "<script>
                var  height_main =  $('.pelayanan-cnt').height();
                     $('#div_content').height(height_main);
                     $('#myTab').remove();
              </script>";
       }?>
      </div>
    </div>
    </div>
  </div>

<script type="text/javascript">

function pilih_upt_monev_kinerja(id_map) {
  call_lokasi_upt(id_map,'monev_kinerja_table_data_utama');
  $('.monev_kinerja_utama').hide();
}
function  call_lokasi_upt(id_map,div_id){
   $.ajax({
         type: "POST",
         dataType: "html",
         url: "<?php echo $basepath ?>monev_kinerja/pilih_upt",
         data: "map_id="+id_map+"&div_id="+div_id,
         success: function(msg){
            if(msg){
              $("#"+div_id).html(msg);
            }
         }
      }); 
}

//Start Chart Monev Kinerja
    var chart_monev_kinerja_kunjungan = [
      <?php 
            $monev_kinerja_no = 0;
            $chart_kunjungan="";
            $sql = "SELECT  pv.nama_prov,COUNT(kode_monev_kinerja) as total
                from tr_monev_kinerja as mnv
                  inner join ms_provinsi as pv 
                      on pv.id_prov=mnv.id_prov  where  mnv.status='3' GROUP BY pv.nama_prov";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_kunjungan .='{"name":"'.$nama_prov.'","y":'.$total.'},';
              $monev_kinerja_no++;
            } 
            echo trim($chart_kunjungan,',');
      ?>
    ];

     <?php 
        if($monev_kinerja_no==0){
          echo " $('#div_monev_kinerja_grafik_1').hide(); ";
        }
     ?>
    //End Chart Monev Kinerja

<?php  
//Kepala UPT atau Operator UPT
if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){ 
   $wilayah   = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$_SESSION['upt_provinsi'])); ?>
    window.onload =  function(){  
     $(".if_upt").hide();
       detail_monev_kinerja('<?php echo $wilayah['id_map'] ?>','<?php echo $_SESSION['kode_upt'] ?>');
    }; 
<?php }
else { ?>

   //Map and chart
    window.onload =  function(){ 
      show_map('peta_monev_kinerja','pilih_upt_monev_kinerja'); 
      pie_chart('monev_kinerja_grafik',chart_monev_kinerja_kunjungan);       
    };

<?php } ?>

//Start Monev Kinerja
function after_crud_monev_kinerja(){
    var map_id = $(".data_map_id_monev_kinerja").val();
    var upt = $(".data_upt_monev_kinerja").val();
    detail_monev_kinerja(map_id,upt);
    $('#call_modal').modal('hide');
}
function detail_monev_kinerja(id_map,upt){
  $(".data_map_id_monev_kinerja").val(id_map);
  $(".data_upt_monev_kinerja").val(upt);
  $('.monev_kinerja_utama').hide();
  ajax_table_map('monev_kinerja_table_data_utama',id_map,upt,'monev_kinerja/table');
  //$('html, body').animate({
       // scrollTop: $("#pelayanan_table_data_utama").offset().top;
  //}, 1000);
  refresh_table();
}     
function do_edit_all(param_id){
  call_modal_default('Ubah Monev Kinerja','Form_Monev_Kinerja_ALL','do_edit','monev_kinerja/form_all');
   setTimeout(function(){ 
       $.ajax({
            url: '<?php echo $basepath ?>monev_kinerja/edit/'+param_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $("#kode_monev_kinerja").val(data.kode_monev_kinerja);
                $("#kinerja").val(data.kinerja);
                $("#indikator_kinerja").val(data.indikator_kinerja);
                $("#target").val(data.target);
                $("#pagu_anggaran").val(data.pagu_anggaran);
                $("#data_dukung").val(data.data_dukung);
                $("#keterangan").val(data.keterangan);
                $("#upt_provinsi").val(data.id_prov);
                $('#lampiran').attr('href',data.lampiran);
                     $.ajax({
                         type: "POST",
                         dataType: "html",
                         url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
                         data: "provinsi="+data.id_prov,
                         success: function(msg){
                            if(msg){
                              $("#kode_upt").html(msg);

                              setTimeout(function(){ 
                                   $("#kode_upt").val(data.kode_upt);        
                              }, 500);
                             
                            }
                         }
                      }); 
                     
                //Renaksi
                var str = data.renaksi_kinerja;
                var str_array = str.split('|');
                var html = '<table class="table table-bordered" style="width:90%">'
                var count =1;
                for(var i = 0; i < str_array.length; i++) {
                   str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
                      var zz = count;
                        if(count<10){
                          zz = "0"+zz; 
                        }
                        html+='<tr>';
                         html+='<th style="width:10%" align="right">B'+zz+'</th>';
                           html+='<td><input  class="form-control" value="'+str_array[i]+'"   name="renaksi[]"   placeholder="" type="text" ></td>';
                         html+='</tr>';
                      count++; 
                }
                html+='</table>';
                $("#table_renaksi").append(html);

                //Realisasi
                var str = data.realisasi;
                var str_array = str.split('|');
                var html = '<table class="table table-bordered" style="width:90%">'
                var countk =1;
                for(var iz = 0; iz < str_array.length; iz++) {
                   str_array[iz] = str_array[iz].replace(/^\s*/, "").replace(/\s*$/, "");
                      var zz = countk;
                        if(countk<10){
                          zz = "0"+zz; 
                        }
                        html+='<tr>';
                         html+='<th style="width:10%" align="right">B'+zz+'</th>';
                           html+='<td><input  class="form-control" value="'+str_array[iz]+'"   name="realisasi[]"   placeholder="" type="text" ></td>';
                         html+='</tr>';
                      countk++; 
                }
                html+='</table>';
                $("#table_realisasi").append(html);     
            }
        });
   }, 1000);
}
function do_edit(param_id,map_id,upt){
  call_modal_from_map('Ubah Monev Kinerja','Form_Monev_Kinerja',map_id,upt,'do_edit','monev_kinerja/form');
   setTimeout(function(){ 
       $.ajax({
            url: '<?php echo $basepath ?>monev_kinerja/edit/'+param_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $("#kode_monev_kinerja").val(data.kode_monev_kinerja);
                $("#kinerja").val(data.kinerja);
                $("#indikator_kinerja").val(data.indikator_kinerja);
                $("#target").val(data.target);
                $("#pagu_anggaran").val(data.pagu_anggaran);
                $("#data_dukung").val(data.data_dukung);
                $("#keterangan").val(data.keterangan);
                //Renaksi
                var str = data.renaksi_kinerja;
                var str_array = str.split('|');
                var html = '<table class="table table-bordered" style="width:90%">'
                var count =1;
                for(var i = 0; i < str_array.length; i++) {
                   str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
                      var zz = count;
                        if(count<10){
                          zz = "0"+zz; 
                        }
                        html+='<tr>';
                         html+='<th style="width:10%" align="right">B'+zz+'</th>';
                           html+='<td><input  class="form-control" value="'+str_array[i]+'"   name="renaksi[]"   placeholder="" type="text" ></td>';
                         html+='</tr>';
                      count++; 
                }
                html+='</table>';
                $("#table_renaksi").append(html);

                //Realisasi
                var str = data.realisasi;
                var str_array = str.split('|');
                var html = '<table class="table table-bordered" style="width:90%">'
                var countk =1;
                for(var iz = 0; iz < str_array.length; iz++) {
                   str_array[iz] = str_array[iz].replace(/^\s*/, "").replace(/\s*$/, "");
                      var zz = countk;
                        if(countk<10){
                          zz = "0"+zz; 
                        }
                        html+='<tr>';
                         html+='<th style="width:10%" align="right">B'+zz+'</th>';
                           html+='<td><input  class="form-control" value="'+str_array[iz]+'"   name="realisasi[]"   placeholder="" type="text" ></td>';
                         html+='</tr>';
                      countk++; 
                }
                html+='</table>';
                $("#table_realisasi").append(html);
            }
        });
   }, 1000);
}
function do_detail(param_id,map_id,upt){
   call_modal_from_map('Detail Monev Kinerja','Form_Monev_Kinerja',map_id,upt,'do_detail','monev_kinerja/form');
   setTimeout(function(){ 
       $.ajax({
            url: '<?php echo $basepath ?>monev_kinerja/edit/'+param_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $("#kode_monev_kinerja").val(data.kode_monev_kinerja);
                $("#kinerja").val(data.kinerja);
                $("#indikator_kinerja").val(data.indikator_kinerja);
                $("#target").val(data.target);
                $("#pagu_anggaran").val(data.pagu_anggaran);
                $("#data_dukung").val(data.data_dukung);
                $("#keterangan").val(data.keterangan);

                //Renaksi
                var str = data.renaksi_kinerja;
                var str_array = str.split('|');
                var html = '<table class="table table-bordered" style="width:90%">'
                var count =1;
                for(var i = 0; i < str_array.length; i++) {
                   str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
                      var zz = count;
                        if(count<10){
                          zz = "0"+zz; 
                        }
                        html+='<tr>';
                         html+='<th style="width:10%" align="right">B'+zz+'</th>';
                           html+='<td><input readonly class="form-control" value="'+str_array[i]+'"   name="renaksi[]"   placeholder="" type="text" ></td>';
                         html+='</tr>';
                      count++; 
                }
                html+='</table>';
                $("#table_renaksi").append(html);

                //Realisasi
                var str = data.realisasi;
                var str_array = str.split('|');
                var html = '<table class="table table-bordered" style="width:90%">'
                var countk =1;
                for(var iz = 0; iz < str_array.length; iz++) {
                   str_array[iz] = str_array[iz].replace(/^\s*/, "").replace(/\s*$/, "");
                      var zz = countk;
                        if(countk<10){
                          zz = "0"+zz; 
                        }
                        html+='<tr>';
                         html+='<th style="width:10%" align="right">B'+zz+'</th>';
                           html+='<td><input readonly class="form-control" value="'+str_array[iz]+'"   name="realisasi[]"   placeholder="" type="text" ></td>';
                         html+='</tr>';
                      countk++; 
                }
                html+='</table>';
                $("#table_realisasi").append(html);
            }
        });
   }, 1000);
}
function do_delete(parameter_data,param_id){
    act_delete('Hapus Monev Kinerja','Anda ingin menghapus Monev Kinerja ? ','warning','monev_kinerja/delete','no_refresh','after_crud_monev_kinerja',window.atob(parameter_data),window.atob(param_id));
}
function do_delete_all(parameter_data,param_id){
    act_delete('Hapus Monev Kinerja','Anda ingin menghapus Monev Kinerja ? ','warning','monev_kinerja/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
}
//End Monev Kinerja


function refresh_table(){
  setTimeout(function(){ 
    //Bahan Sosialisasi
    $('#tb_monev_kinerja').DataTable().ajax.reload();
    $('#tb_monev_kinerja_waiting').DataTable().ajax.reload();
  }, 500);
  $("#call_modal").modal("hide");
  $(".call_modal").modal("hide");
}
$('[data-tooltip="tooltip"]').tooltip();
</script>
<?php } ?>