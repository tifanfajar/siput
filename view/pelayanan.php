<?php  
 $check_akses = $gen_model->GetOne("fua_read","tr_function_access",array('id_menu'=>2,'kode_function_access'=>$_SESSION['group_user']));
 if(empty($check_akses)){
    echo '<meta http-equiv="refresh" content="0;url='.$basepath.'home">';
 }
 else { ?>
<title>Pelayanan - <?php echo str_replace('<br/>','',$web['judul']) ?></title>


<style type="text/css">
  #example_paginate{position: relative; left: 40%; top: 5px;}
  .pagination li a{margin:5px 15px; }
  .dataTables_info{margin-bottom: 15px;}
  .datatable>tbody>tr>td
  {
    white-space: nowrap;
  } 
  .table th {
     text-align: center;
     font-weight:bold;
  }
</style>
<body class="pelayanan-cnt">


  <?php  
  include "view/top_menu.php"; 
  $akses_add_tab_loket = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>1,'kode_function_access'=>$_SESSION['group_user'])); 
  $akses_add_tab_spp = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>2,'kode_function_access'=>$_SESSION['group_user'])); 
  $akses_add_tab_isr = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>3,'kode_function_access'=>$_SESSION['group_user'])); 
  $akses_add_tab_revoke = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>4,'kode_function_access'=>$_SESSION['group_user'])); 
  $akses_add_tab_piutang = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>5,'kode_function_access'=>$_SESSION['group_user'])); 
  $akses_add_tab_unar = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>6,'kode_function_access'=>$_SESSION['group_user'])); 


  $akses_add_tab_loket    = (!empty($akses_add_tab_loket) ? $akses_add_tab_loket : '0');
  $akses_add_tab_spp      = (!empty($akses_add_tab_spp) ? $akses_add_tab_spp : '0');
  $akses_add_tab_isr      = (!empty($akses_add_tab_isr) ? $akses_add_tab_isr : '0');
  $akses_add_tab_revoke   = (!empty($akses_add_tab_revoke) ? $akses_add_tab_revoke : '0');
  $akses_add_tab_piutang  = (!empty($akses_add_tab_piutang) ? $akses_add_tab_piutang : '0');
  $akses_add_tab_unar     = (!empty($akses_add_tab_unar) ? $akses_add_tab_unar : '0');

  ?>
  
  <!-- Main Container -->
  <div class="container-fluid animated fadeInUp" style="z-index: 3;">
    <div class="row">
  <?php  
    include "view/menu.php";  
   ?>
  <!-- Content -->
    <div class="container-fluid main-content" >
      <div class="row">


        <ul class="nav nav-tabs main-nav-tab" id="myTab" role="tablist">
          <?php 
              $data_tb = $db->SelectLimit("select * from ms_menu_tab where men_id='2' order by urutan asc"); 
              $no_tab = 1;
              $men_active =""; 
              $total_tab = 0;
              while($list = $data_tb->FetchRow()){
                  foreach($list as $key=>$val){
                        $key=strtolower($key);
                        $$key=$val;
                      } 
                      $whr_tab = array();
                      $whr_tab['kode_function_access'] = $_SESSION['group_user'];
                      $whr_tab['id_tab'] = $tab_id;
                      $check_tab = $gen_model->GetOne("fua_read","tr_function_access_tab",$whr_tab);
                     if(!empty($check_tab)){ 
                      $active_tab ="";   
                      if($no_tab==1) {
                           $active_tab ="active";
                           $men_active = $tab_url;   
                      }
                      ?>  
                      <li class="nav-item">
                        <a class="nav-link <?php echo $active_tab ?>" onclick="refresh_table()" id="<?php echo $tab_url ?>-tab" data-toggle="tab" href="#<?php echo $tab_url ?>" role="tab" aria-controls="<?php echo $tab_url ?>" aria-selected="true"><?php echo $tab ?></a>
                      </li>
                   <?php $no_tab++; 
                   $total_tab++;     
                 } 
              } 

              function active_tab($list_tab){
                    global $men_active;
                    if($men_active==$list_tab){
                       return "show active";
                    }
              }
              ?>
        </ul>


         <?php  if($total_tab!=0){  ?>
        <div class="tab-content" id="myTabContent">
          <!-- Tab Pelayanan -->
          <div class="tab-pane fade <?php echo active_tab("loket_pelayanan") ?>" id="loket_pelayanan" role="tabpanel" aria-labelledby="home-tab">
           
            <!-- Peta -->
            <label class="judul-peta if_upt pelayanan_utama"><i class="fa fa-map-marked-alt"></i> Peta Lokasi UPT</label>
            <hr class="if_upt pelayanan_utama">
            <div class="peta-cnt" id="peta_loket_pelayanan"></div>

            <!-- Grafik Chart -->
            <div class="row if_upt pelayanan_utama">
                <div class="col-sm-6" id="div_loket_pelayanan_grafik_1">
                  <div class="card-set">
                    <div class="headset"><i class="fa fa-chart-pie"></i> Grafik Jumlah Per Tujuan</div>
                    <div class="img-chart">
                      <!-- <img src="images/chart.jpg" alt=""> -->
                      <div id="loket_pelayanan_grafik_1" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6" id="div_loket_pelayanan_grafik_2">
                  <div class="card-set">
                    <div class="headset"><i class="fa fa-chart-pie"></i> Grafik Jumlah Pelayanan Per Provinsi</div>
                    <div class="img-chart">
                      <!-- <img src="images/chart.jpg" alt=""> -->
                      <div id="loket_pelayanan_grafik_2" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                    </div>
                  </div>
                </div> <br/>
            </div>

            <div class="row">
                <div class="col-sm-12" id="pelayanan_table_data_utama"></div>
            </div>

            <input type="hidden" name="LoketPelayananPencarian_all" id="LoketPelayananPencarian_all" >
            <script type="text/javascript">
              function print_laporan_loket_all(tipe){
                  var param = $("#LoketPelayananPencarian_all").val();

                    var form = document.createElement("form");
                    form.setAttribute("method", "POST");
                    if(tipe=="print"){
                     form.setAttribute("target", "_blank");
                    }
                    form.setAttribute("action", "<?php echo $basepath ?>pelayanan_loket/cetak_all");
                           
                        var hiddenField = document.createElement("input");
                        hiddenField.setAttribute("type", "hidden");
                        hiddenField.setAttribute("name", "param");
                        hiddenField.setAttribute("value", param);

                        var hiddenField2 = document.createElement("input");
                        hiddenField2.setAttribute("type", "hidden");
                        hiddenField2.setAttribute("name", "tipe");
                        hiddenField2.setAttribute("value", tipe);

                        form.appendChild(hiddenField);
                        form.appendChild(hiddenField2);

                    document.body.appendChild(form);
                    form.submit();
                }
            </script>
            <!-- Tabel -->
            <div class="cnt-heaeder pelayanan_utama" style="margin-top: 30px; margin-bottom: 20px;">
              <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-portrait"></i> Resume Loket Pelayanan</h6>
              
              <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">
                
                <?php  if($akses_add_tab_loket=="1"){  ?>
                   <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Tambah" onclick="call_modal_default('Tambah Profil & Data Pengunjung UPT','Form_Pelayanan_Loket_ALL','do_add','pelayanan/form_all')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                <?php } ?>

                <a  data-tooltip="tooltip" data-placement="bottom" title="Cetak" href="javascript:void(0)" onclick="print_laporan_loket_all('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

                <?php  if($akses_add_tab_loket=="1"){  ?>
                   <a href="" class="btn btn-secondary btn-sm btn-table"  data-tooltip="tooltip" data-toggle="modal" data-placement="bottom" title="Import" data-target="#Import_LoketPelayananAll"><i class="fa fa-upload"></i></a>
                <?php } ?>

                <a href=""  data-tooltip="tooltip" data-placement="bottom" title="Filterisasi" class="btn btn-secondary btn-sm btn-table" data-toggle="modal"  data-target="#Filter_LoketPelayananAll"  ><i class="fa fa-filter"></i></a>

                <div class="btn-group" role="group"  data-tooltip="tooltip" data-placement="bottom" title="Export">
                  <button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-file-pdf"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="red">
                    <a class="dropdown-item"  onclick="print_laporan_loket_all('pdf')" href="javascript:void(0)">Export to PDF</a>
                    <a class="dropdown-item"   onclick="print_laporan_loket_all('xls')" href="javascript:void(0)">Export to XLS</a>
                    <a class="dropdown-item" onclick="print_laporan_loket_all('doc')"   href="javascript:void(0)">Export to DOCS</a>
                  </div>
                </div>
              </div>
              <hr style="width: 100%; float: left; display: block;">
            </div>

            <div id="filter_pelayanan_all" class="table table-responsive pelayanan-top pelayanan_utama">
              <table id="tb_loket" class="display table table-striped pelayanan-table datatable">
                <thead>
                  <tr>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Provinsi</th>
                    <th class="text-center">UPT</th>
                    <th class="text-center">Nama</th>
                    <th class="text-center">Jabatan</th>
                    <th class="text-center">Perusahaan</th>
                    <th class="text-center">No.Tlp</th>
                    <th class="text-center">No.Hp</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Tujuan</th>
                    <th class="text-center">Perizinan</th>
                    <th class="text-center">Keterangan</th>
                    <th class="text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            
            </div>
            <!-- Tabel -->
            <div class="pelayanan_utama if_upt">
              <div class="cnt-heaeder" style="margin-top: 30px; padding-bottom: 36px;">
                <h6 class="table-label if_upt"><i class="fa fa-reply-all"></i> Resume UPT</h6>
              </div>
              <?php   $count_tj = $gen_model->getOne("count(*)","ms_tujuan"); ?>
              <div class="table table-responsive pelayanan-top if_upt">
                <table id="tb_loket_akumulasi" class="table table-striped pelayanan-table" style="width: 100%;">
                  <thead>
                    <tr>
                      <th rowspan="2" scope="col">Provinsi</th>
                      <th rowspan="2" scope="col">Nama UPT</th>
                      <th colspan="<?php echo $count_tj ?>" scope="col" style="text-align: center;">Tujuan</th>
                      <th rowspan="2" scope="col">Total Pengunjung</th>
                    </tr>
                    <tr>
                      <?php 
                          $data_tj = $db->SelectLimit("select * from ms_tujuan"); 
                          while($list = $data_tj->FetchRow()){
                            foreach($list as $key=>$val){
                                  $key=strtolower($key);
                                  $$key=$val;
                                }  
                        ?>  
                      <th scope="col"><?php echo $tujuan ?></th>
                    <?php } ?>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
                <hr>
              </div>
            </div>

              <div class="cnt-heaeder" style="margin-top: 30px; margin-bottom: 20px;">
                  <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-file"></i> Group Perizinan</h6>
                  
                  <?php 
                    $akses_tab_grp = $gen_model->GetOneRow("tr_function_access_tab",array('id_tab'=>19,'kode_function_access'=>$_SESSION['group_user']));
                    if(!empty($akses_tab_grp['fua_add'])){  ?>
                        <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">
                               <a  data-tooltip="tooltip" data-placement="bottom" title="Tambah" href="javascript:void(0)"  onclick="call_modal_default_no_full('Tambah Group Perizinan','Form_Perizinan','do_add','grp_layanan/form')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                        </div>
                  <?php } ?>
                  <hr style="width: 100%; float: left; display: block;">
                </div>

                <div id="" class="table table-responsive pelayanan-top">
                   <table id="tb_grp_perizinan" class="display table table-striped pelayanan-table">
                    <thead>
                      <tr>
                        <th>Kode Group Perizinan</th>
                        <th>Group Perizinan</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>
                  <script type="text/javascript">
                    var dTable;
                    $(document).ready(function() {
                      dTable = $('#tb_grp_perizinan').DataTable( {
                        "bProcessing": true,
                        "bServerSide": true,
                        "bJQueryUI": false,
                        "responsive": false,
                        "autoWidth": false,
                        "sAjaxSource": "<?php echo $basepath ?>grp_layanan/rest_grp", 
                        "sServerMethod": "POST",
                        "scrollX": true,
                        "searching": true,
                        "scrollY": "350px",
                          "scrollCollapse": true,
                        "columnDefs": [
                        { "orderable": true, "targets": 0, "searchable": true},
                        { "orderable": true, "targets": 1, "searchable": true},
                        { "orderable": false, "targets": 2, "searchable": false, "width":170}
                        ]
                      } );
                    });
                  </script>
                </div>

          </div>

          <!-- Tab SPP -->
          <div class="tab-pane fade <?php echo active_tab("spp") ?>" id="spp" role="tabpanel" aria-labelledby="spp-tab">
            <!-- Peta -->
            <label class="judul-peta if_upt spp_utama"><i class="fa fa-map-marked-alt"></i> Peta Lokasi UPT</label>
            <hr class="spp_utama">
            <div class="peta-cnt" id="peta_spp"></div>

            <!-- Grafik Chart -->
            <div class="row if_upt spp_utama" >
                <div class="col-sm-6" id="div_spp_grafik_1">
                  <div class="card-set">
                    <div class="headset"><i class="fa fa-chart-pie"></i> Grafik Jumlah Per Metode</div>
                    <div class="img-chart">
                      <div id="spp_grafik_1" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6" id="div_spp_grafik_2">
                  <div class="card-set">
                    <div class="headset"><i class="fa fa-chart-pie"></i> Grafik Jumlah SPP Per Provinsi</div>
                    <div class="img-chart">
                      <div id="spp_grafik_2" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                    </div>
                  </div>
                </div>
            </div> <br/>

            <div class="row">
                <div class="col-sm-12" id="spp_table_data_utama"></div>
            </div>

            <input type="hidden" name="SPPencarian_all" id="SPPencarian_all" >
            <script type="text/javascript">
              function print_laporan_spp_all(tipe){
                  var param = $("#SPPencarian_all").val();

                    var form = document.createElement("form");
                    form.setAttribute("method", "POST");
                    if(tipe=="print"){
                     form.setAttribute("target", "_blank");
                    }
                    form.setAttribute("action", "<?php echo $basepath ?>pelayanan_spp/cetak_all");
                           
                        var hiddenField = document.createElement("input");
                        hiddenField.setAttribute("type", "hidden");
                        hiddenField.setAttribute("name", "param");
                        hiddenField.setAttribute("value", param);

                        var hiddenField2 = document.createElement("input");
                        hiddenField2.setAttribute("type", "hidden");
                        hiddenField2.setAttribute("name", "tipe");
                        hiddenField2.setAttribute("value", tipe);

                        form.appendChild(hiddenField);
                        form.appendChild(hiddenField2);

                    document.body.appendChild(form);
                    form.submit();
                }
            </script>

            <!-- Tabel -->
            <div class="cnt-heaeder spp_utama" style="margin-top: 30px; margin-bottom: 20px;">
              <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-portrait"></i> Resume SPP</h6>
              <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">
               
               <?php  if($akses_add_tab_spp=="1"){  ?>
                    <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Tambah" onclick="call_modal_default('Tambah SPP','Form_SPP_ALL','do_add','pelayanan/form_all')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                <?php } ?>

                <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Cetak" onclick="print_laporan_spp_all('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

                <?php  if($akses_add_tab_spp=="1"){  ?>
                <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Import" data-target="#Import_SPPAll"><i class="fa fa-upload"></i></a>
                <?php } ?>
                
                <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom"  title="Filterisasi" data-target="#Filter_SPPAll"  ><i class="fa fa-filter"></i></a>

                <div class="btn-group" role="group" data-tooltip="tooltip" data-placement="bottom" title="Export">
                  <button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-file-pdf"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="red">
                    <a class="dropdown-item"  onclick="print_laporan_spp_all('pdf')" href="javascript:void(0)">Export to PDF</a>
                    <a class="dropdown-item"   onclick="print_laporan_spp_all('xls')" href="javascript:void(0)">Export to XLS</a>
                    <a class="dropdown-item" onclick="print_laporan_spp_all('doc')"   href="javascript:void(0)">Export to DOCS</a>
                  </div>
                </div>
              </div>
              <hr style="width: 100%; float: left; display: block;">
            </div>

            <div id="filter_spp_all" class="table table-responsive pelayanan-top spp_utama">
              <table id="tb_spp" class="display table table-striped pelayanan-table datatable">
                <thead>
                  <tr>
                      <th class="text-center">Provinsi</th>
                      <th class="text-center">UPT</th>
                      <th class="text-center">No. Client</th>
                      <th class="text-center">Nama Pemegang ISR / Waba</th>
                      <th class="text-center">Service</th>
                      <th class="text-center">No. Aplikasi</th>
                      <th class="text-center">No. SPP</th>
                      <th class="text-center">Jenis SPP</th>
                      <th class="text-center">Nilai BHP (Rp)</th>
                      <th class="text-center">Tanggal Jatuh Tempo</th>
                      <th class="text-center">Tanggal Pengiriman</th>
                      <th class="text-center">Metode</th>
                      <th class="text-center">Keterangan</th>
                      <th class="text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- Tabel -->
            <!--<div class="if_upt spp_utama">
              <div class="cnt-heaeder" style="margin-top: 30px; padding-bottom: 36px;">
                <h6 class="table-label if_upt spp_utama"><i class="fa fa-reply-all"></i> Resume UPT</h6>
              </div>
              <div class="table table-responsive pelayanan-top if_upt spp_utama">
                <table id="tableDataspp" class="table table-striped pelayanan-table" style="width: 100%;">
                  <thead>
                    <tr>
                      <th rowspan="2" scope="col">Nama UPT</th>
                      <th rowspan="2" scope="col">Perizinan</th>
                      <th rowspan="2" scope="col">Jumlah Pengunjung</th>
                      <th colspan="4" scope="col" style="text-align: center;">Tujuan</th>
                    </tr>
                    <tr>
                      <th scope="col">Pengaduan Gangguan</th>
                      <th scope="col">Konsultasi</th>
                      <th scope="col">Aksestensi</th>
                      <th scope="col">Lain-Lain</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
                <hr>
              </div>
            </div>-->


           <!-- <script type="text/javascript">
              var dTable;
              $(document).ready(function() {
                dTable = $('#tableDataspp').DataTable( {
                  "bProcessing": true,
                  "bServerSide": true,
                  "bJQueryUI": false,
                  "responsive": false,
                  "autoWidth": false,
                  "sAjaxSource": "<?php echo $basepath ?>pelayanan_spp/akumulasi", 
                  "sServerMethod": "POST",
                  "scrollX": true,
                  "scrollY": "350px",
                    "scrollCollapse": true,
                  "columnDefs": [
                  { "orderable": true, "targets": 0, "searchable": true},
                  { "orderable": false, "targets": 1, "searchable": false},
                  { "orderable": false, "targets": 2, "searchable": false},
                  { "orderable": false, "targets": 3, "searchable": false},
                  { "orderable": false, "targets": 4, "searchable": false },
                  { "orderable": false, "targets": 5, "searchable": false },
                  { "orderable": false, "targets": 6, "searchable": false }
                  ]
                } );
              });
            </script>-->
          </div>
          
      
          <!-- Tab ISR -->
          <div class="tab-pane fade <?php echo active_tab("isr") ?>" id="isr" role="tabpanel" aria-labelledby="isr-tab">
            <!-- Peta -->
            <label class="judul-peta if_upt isr_utama"><i class="fa fa-map-marked-alt"></i> Peta Lokasi UPT</label>
            <hr class="isr_utama">
            <div class="peta-cnt" id="peta_isr"></div>

            <!-- Grafik Chart -->
            <div class="row if_upt isr_utama" >
                <div class="col-sm-6" id="div_isr_grafik_1">
                  <div class="card-set">
                    <div class="headset"><i class="fa fa-chart-pie"></i> Grafik Jumlah Per Metode</div>
                    <div class="img-chart">
                      <!-- <img src="images/chart.jpg" alt=""> -->
                      <div id="isr_grafik_1" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6" id="div_isr_grafik_2">
                  <div class="card-set">
                    <div class="headset"><i class="fa fa-chart-pie"></i> Grafik Jumlah ISR Per Provinsi</div>
                    <div class="img-chart">
                      <!-- <img src="images/chart.jpg" alt=""> -->
                      <div id="isr_grafik_2" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                    </div>
                  </div>
                </div>
            </div> <br/>

            <div class="row">
                <div class="col-sm-12" id="isr_table_data_utama"></div>
            </div>

            <input type="hidden" name="ISRPencarian_all" id="ISRPencarian_all" >
            <script type="text/javascript">
              function print_laporan_isr_all(tipe){
                  var param = $("#ISRPencarian_all").val();

                    var form = document.createElement("form");
                    form.setAttribute("method", "POST");
                    if(tipe=="print"){
                     form.setAttribute("target", "_blank");
                    }
                    form.setAttribute("action", "<?php echo $basepath ?>pelayanan_isr/cetak_all");
                           
                        var hiddenField = document.createElement("input");
                        hiddenField.setAttribute("type", "hidden");
                        hiddenField.setAttribute("name", "param");
                        hiddenField.setAttribute("value", param);

                        var hiddenField2 = document.createElement("input");
                        hiddenField2.setAttribute("type", "hidden");
                        hiddenField2.setAttribute("name", "tipe");
                        hiddenField2.setAttribute("value", tipe);

                        form.appendChild(hiddenField);
                        form.appendChild(hiddenField2);

                    document.body.appendChild(form);
                    form.submit();
                }
            </script>

            <!-- Tabel -->
            <div class="cnt-heaeder isr_utama" style="margin-top: 30px; margin-bottom: 20px;">
              <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-portrait"></i> Resume ISR</h6>
              <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">
              
               <?php  if($akses_add_tab_isr=="1"){  ?>
                  <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Tambah"  onclick="call_modal_default('Tambah ISR','Form_ISR_ALL','do_add','pelayanan/form_all')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                <?php } ?>

                <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Cetak" onclick="print_laporan_isr_all('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

               <?php  if($akses_add_tab_isr=="1"){  ?>
                  <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Import" data-target="#Import_ISRAll"><i class="fa fa-upload"></i></a>
                <?php } ?>
                
                <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Filterisasi" data-target="#Filter_ISRAll"  ><i class="fa fa-filter"></i></a>

                <div class="btn-group" role="group" data-tooltip="tooltip" data-placement="bottom" title="Export">
                  <button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-file-pdf"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="red">
                    <a class="dropdown-item"  onclick="print_laporan_isr_all('pdf')" href="javascript:void(0)">Export to PDF</a>
                    <a class="dropdown-item"   onclick="print_laporan_isr_all('xls')" href="javascript:void(0)">Export to XLS</a>
                    <a class="dropdown-item" onclick="print_laporan_isr_all('doc')"   href="javascript:void(0)">Export to DOCS</a>
                  </div>
                </div>
              </div>
              <hr style="width: 100%; float: left; display: block;">
            </div>

            <div id="filter_isr_all" class="table table-responsive pelayanan-top isr_utama">
              <table id="tb_isr" class="display table table-striped pelayanan-table datatable">
                <thead>
                  <tr>
                    <th class="text-center">Provinsi</th>
                    <th class="text-center">UPT</th>
                    <th class="text-center">No. Client</th>
                    <th class="text-center">Nama Pemegang ISR / Waba</th>
                    <th class="text-center">Service</th>
                    <th class="text-center">No. Aplikasi</th>
                    <th class="text-center">Tanggal Pengiriman</th>
                    <th class="text-center">Metode</th>
                    <th class="text-center">Keterangan</th>
                    <th class="text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>  


           <!-- Tab Revoke -->
          <div class="tab-pane fade <?php echo active_tab("revoe") ?>" id="revoke" role="tabpanel" aria-labelledby="revoke-tab">
            <!-- Peta -->
            <label class="judul-peta if_upt revoke_utama"><i class="fa fa-map-marked-alt"></i> Peta Lokasi UPT</label>
            <hr class="revoke_utama">
            <div class="peta-cnt" id="peta_revoke"></div>

            <!-- Grafik Chart -->
            <div class="row if_upt revoke_utama">
                <div class="col-sm-6"  id="div_revoke_grafik_1">
                  <div class="card-set">
                    <div class="headset"><i class="fa fa-chart-pie"></i> Grafik Jumlah Per Metode</div>
                    <div class="img-chart">
                      <!-- <img src="images/chart.jpg" alt=""> -->
                      <div id="revoke_grafik_1" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6" id="div_revoke_grafik_2">
                  <div class="card-set">
                    <div class="headset"><i class="fa fa-chart-pie"></i> Grafik Jumlah Revoke Per Provinsi</div>
                    <div class="img-chart">
                      <!-- <img src="images/chart.jpg" alt=""> -->
                      <div id="revoke_grafik_2" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                    </div>
                  </div>
                </div>
            </div> <br/>

            <div class="row">
                <div class="col-sm-12" id="revoke_table_data_utama"></div>
            </div>

            <input type="hidden" name="RevokePencarian_all" id="RevokePencarian_all" >
            <script type="text/javascript">
              function print_laporan_revoke_all(tipe){
                  var param = $("#RevokePencarian_all").val();

                    var form = document.createElement("form");
                    form.setAttribute("method", "POST");
                    if(tipe=="print"){
                     form.setAttribute("target", "_blank");
                    }
                    form.setAttribute("action", "<?php echo $basepath ?>pelayanan_revoke/cetak_all");
                           
                        var hiddenField = document.createElement("input");
                        hiddenField.setAttribute("type", "hidden");
                        hiddenField.setAttribute("name", "param");
                        hiddenField.setAttribute("value", param);

                        var hiddenField2 = document.createElement("input");
                        hiddenField2.setAttribute("type", "hidden");
                        hiddenField2.setAttribute("name", "tipe");
                        hiddenField2.setAttribute("value", tipe);

                        form.appendChild(hiddenField);
                        form.appendChild(hiddenField2);

                    document.body.appendChild(form);
                    form.submit();
                }
            </script>

            <!-- Tabel -->
            <div class="cnt-heaeder revoke_utama" style="margin-top: 30px; margin-bottom: 20px;">
              <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-portrait"></i> Resume Revoke</h6>
              <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">
               
               <?php  if($akses_add_tab_revoke=="1"){  ?>
                 <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Tambah" onclick="call_modal_default('Tambah Revoke','Form_Revoke_ALL','do_add','pelayanan/form_all')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
               <?php } ?> 

                <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Cetak" onclick="print_laporan_revoke_all('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

                <?php  if($akses_add_tab_revoke=="1"){  ?>
                   <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Import" data-target="#Import_RevokeAll"><i class="fa fa-upload"></i></a>
                 <?php } ?> 
                
                <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Filterisasi" data-target="#Filter_RevokeAll"  ><i class="fa fa-filter"></i></a>

                <div class="btn-group" role="group" data-tooltip="tooltip" data-placement="bottom" title="Export">
                  <button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-file-pdf"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="red">
                    <a class="dropdown-item"  onclick="print_laporan_revoke_all('pdf')" href="javascript:void(0)">Export to PDF</a>
                    <a class="dropdown-item"  onclick="print_laporan_revoke_all('xls')" href="javascript:void(0)">Export to XLS</a>
                    <a class="dropdown-item"  onclick="print_laporan_revoke_all('doc')"   href="javascript:void(0)">Export to DOCS</a>
                  </div>
                </div>
              </div>
              <hr style="width: 100%; float: left; display: block;">
            </div>

            <div id="filter_revoke_all" class="table table-responsive pelayanan-top revoke_utama">
              <table id="tb_revoke" class="display table table-striped pelayanan-table datatable">
                <thead>
                  <tr>
                    <th class="text-center">Provinsi</th>
                    <th class="text-center">UPT</th>
                    <th class="text-center">No. Client</th>
                    <th class="text-center">Nama Pemegang ISR / Waba</th>
                    <th class="text-center">Service</th>
                    <th class="text-center">No. Aplikasi</th>
                    <th class="text-center">Tanggal ISR Dicabut</th>
                    <th class="text-center">Tanggal Pengiriman</th>
                    <th class="text-center">Metode</th>
                    <th class="text-center">Keterangan</th>
                    <th class="text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>

          <!-- Tab Penanganan Piutang -->
          <div class="tab-pane fade <?php echo active_tab("penanganan_piutang") ?>" id="penanganan_piutang" role="tabpanel" aria-labelledby="penanganan_piutang-tab">
            <!-- Peta -->
            <label class="judul-peta if_upt piutang_utama"><i class="fa fa-map-marked-alt"></i> Peta Lokasi UPT</label>
            <hr class="piutang_utama">
            <div class="peta-cnt" id="peta_penanganan_piutang"></div>

            <!-- Grafik Chart -->
            <div class="row if_upt piutang_utama" >
                <div class="col-sm-6" id="div_piutang_grafik_1">
                  <div class="card-set">
                    <div class="headset"><i class="fa fa-chart-pie"></i> Grafik Jumlah Per KPKNL</div>
                    <div class="img-chart">
                      <!-- <img src="images/chart.jpg" alt=""> -->
                      <div id="piutang_grafik_1" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6" id="div_piutang_grafik_2">
                  <div class="card-set">
                    <div class="headset"><i class="fa fa-chart-pie"></i> Grafik Jumlah Penanganan Piutang Per Provinsi</div>
                    <div class="img-chart">
                      <!-- <img src="images/chart.jpg" alt=""> -->
                      <div id="piutang_grafik_2" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                    </div>
                  </div>
                </div>
            </div> <br/>

            <div class="row">
                <div class="col-sm-12" id="piutang_table_data_utama"></div>
            </div>

            <input type="hidden" name="PiutangPencarian_all" id="PiutangPencarian_all" >
            <script type="text/javascript">
              function print_laporan_piutang_all(tipe){
                  var param = $("#PiutangPencarian_all").val();

                    var form = document.createElement("form");
                    form.setAttribute("method", "POST");
                    if(tipe=="print"){
                     form.setAttribute("target", "_blank");
                    }
                    form.setAttribute("action", "<?php echo $basepath ?>pelayanan_piutang/cetak_all");
                           
                        var hiddenField = document.createElement("input");
                        hiddenField.setAttribute("type", "hidden");
                        hiddenField.setAttribute("name", "param");
                        hiddenField.setAttribute("value", param);

                        var hiddenField2 = document.createElement("input");
                        hiddenField2.setAttribute("type", "hidden");
                        hiddenField2.setAttribute("name", "tipe");
                        hiddenField2.setAttribute("value", tipe);

                        form.appendChild(hiddenField);
                        form.appendChild(hiddenField2);

                    document.body.appendChild(form);
                    form.submit();
                }
            </script>

            <!-- Tabel -->
            <div class="cnt-heaeder piutang_utama" style="margin-top: 30px; margin-bottom: 20px;">
              <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-portrait"></i> Resume Penanganan Piutang</h6>
              <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">
                
                <?php  if($akses_add_tab_piutang=="1"){  ?>
                    <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Tambah"  onclick="call_modal_default('Tambah Penanganan Piutang','Form_Piutang_ALL','do_add','pelayanan/form_all')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                <?php } ?>

                <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Cetak" onclick="print_laporan_piutang_all('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

                <?php  if($akses_add_tab_piutang=="1"){  ?>
                  <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Import" data-target="#Import_PiutangAll"><i class="fa fa-upload"></i></a>
                <?php } ?>
                
                <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Filterisasi" data-target="#Filter_PiutangAll"  ><i class="fa fa-filter"></i></a>

                <div class="btn-group" role="group" data-tooltip="tooltip" data-placement="bottom" title="Export">
                  <button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-file-pdf"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="red">
                    <a class="dropdown-item"  onclick="print_laporan_piutang_all('pdf')" href="javascript:void(0)">Export to PDF</a>
                    <a class="dropdown-item"  onclick="print_laporan_piutang_all('xls')" href="javascript:void(0)">Export to XLS</a>
                    <a class="dropdown-item"  onclick="print_laporan_piutang_all('doc')"   href="javascript:void(0)">Export to DOCS</a>
                  </div>
                </div>
              </div>
              <hr style="width: 100%; float: left; display: block;">
            </div>

            <div id="filter_piutang_all" class="table table-responsive pelayanan-top piutang_utama">
              <table id="tb_piutang" class="display table table-striped pelayanan-table datatable">
                <thead>
                  <tr>
                    <th class="text-center" rowspan="2">Tanggal Buat</th>
                    <th class="text-center" rowspan="2">Provinsi</th>
                    <th class="text-center" rowspan="2">UPT</th>
                    <th class="text-center" rowspan="2">Wajib Bayar</th>
                    <th class="text-center" rowspan="2">No. Client</th>
                    <th class="text-center" rowspan="2">Nilai Penyerahan</th>
                    <th class="text-center" rowspan="2">Tahun Pelimpahan</th>
                    <th class="text-center" rowspan="2">Nama KPKNL</th>
                    <th class="text-center" rowspan="2">Tahapan Pengurusan</th>
                    <th class="text-center" colspan="3">Pemindah Bukuan</th>
                    <th class="text-center" rowspan="2">PSBDT</th>
                    <th class="text-center" rowspan="2">Pembatalan</th>
                    <th class="text-center" rowspan="2">Sisa Piutang</th>
                    <th class="text-center" rowspan="2">Ket</th>
                    <th class="text-center" rowspan="2">Aksi</th>
                  </tr>
                  <tr>
                    <th class="text-center">Lunas</th>
                    <th class="text-center">Angsuran</th>
                    <th class="text-center">Tanggal</th>
                  </tr>
                  <tr>
                    <th class="text-center">1</th>
                    <th class="text-center">2</th>
                    <th class="text-center">3</th>
                    <th class="text-center">4</th>
                    <th class="text-center">5</th>
                    <th class="text-center">6</th>
                    <th class="text-center">7</th>
                    <th class="text-center">8</th>
                    <th class="text-center">9</th>
                    <th class="text-center">10</th>
                    <th class="text-center">11</th>
                    <th class="text-center">12</th>
                    <th class="text-center">13</th>
                    <th class="text-center">14</th>
                    <th class="text-center">15</th>
                    <th class="text-center">16</th>
                    <th class="text-center">17</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>

          <!-- Tab UNAR -->
          <div class="tab-pane fade <?php echo active_tab("unar") ?>" id="unar" role="tabpanel" aria-labelledby="unar-tab">
            <!-- Peta -->
            <label class="judul-peta if_upt unar_utama"><i class="fa fa-map-marked-alt"></i> Peta Lokasi UPT</label>
            <hr class="unar_utama">
            <div class="peta-cnt" id="peta_unar"></div>

            <!-- Grafik Chart -->
            <div class="row if_upt unar_utama">
                <div class="col-sm-6" id="div_unar_grafik_1">
                  <div class="card-set">
                    <div class="headset"><i class="fa fa-chart-pie"></i> Grafik Jumlah Per Lulus/Tidak Lulus</div>
                    <div class="img-chart">
                      <!-- <img src="images/chart.jpg" alt=""> -->
                      <div id="unar_grafik_1" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6" id="div_unar_grafik_2">
                  <div class="card-set">
                    <div class="headset"><i class="fa fa-chart-pie"></i> Grafik Jumlah Unar Per Provinsi</div>
                    <div class="img-chart">
                      <!-- <img src="images/chart.jpg" alt=""> -->
                      <div id="unar_grafik_2" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                    </div>
                  </div>
                </div>
            </div> <br/>

            <div class="row">
                <div class="col-sm-12" id="unar_table_data_utama"></div>
            </div>

            <input type="hidden" name="UnarPencarian_all" id="UnarPencarian_all" >
            <script type="text/javascript">
              function print_laporan_unar_all(tipe){
                  var param = $("#UnarPencarian_all").val();

                    var form = document.createElement("form");
                    form.setAttribute("method", "POST");
                    if(tipe=="print"){
                     form.setAttribute("target", "_blank");
                    }
                    form.setAttribute("action", "<?php echo $basepath ?>pelayanan_unar/cetak_all");
                           
                        var hiddenField = document.createElement("input");
                        hiddenField.setAttribute("type", "hidden");
                        hiddenField.setAttribute("name", "param");
                        hiddenField.setAttribute("value", param);

                        var hiddenField2 = document.createElement("input");
                        hiddenField2.setAttribute("type", "hidden");
                        hiddenField2.setAttribute("name", "tipe");
                        hiddenField2.setAttribute("value", tipe);

                        form.appendChild(hiddenField);
                        form.appendChild(hiddenField2);

                    document.body.appendChild(form);
                    form.submit();
                }
            </script>

            <!-- Tabel -->
            <div class="cnt-heaeder unar_utama" style="margin-top: 30px; margin-bottom: 20px;">
              <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-portrait"></i> Resume Unar</h6>
              <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">
                
                <?php  if($akses_add_tab_unar=="1"){  ?>
                    <a href="javascript:void(0)"  data-tooltip="tooltip" data-placement="bottom" title="Tambah" onclick="call_modal_default('Tambah Unar','Form_Unar_ALL','do_add','pelayanan/form_all')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                <?php } ?>

                <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Cetak" onclick="print_laporan_unar_all('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

                <?php  if($akses_add_tab_unar=="1"){  ?>
                  <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Import" data-target="#Import_UnarAll"><i class="fa fa-upload"></i></a>
                <?php } ?>
                
                <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Filterisasi" data-target="#Filter_UnarAll"  ><i class="fa fa-filter"></i></a>

                <div class="btn-group" role="group" data-tooltip="tooltip" data-placement="bottom" title="Export">
                  <button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-file-pdf"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="red">
                    <a class="dropdown-item"  onclick="print_laporan_unar_all('pdf')" href="javascript:void(0)">Export to PDF</a>
                    <a class="dropdown-item"  onclick="print_laporan_unar_all('xls')" href="javascript:void(0)">Export to XLS</a>
                    <a class="dropdown-item"  onclick="print_laporan_unar_all('doc')"   href="javascript:void(0)">Export to DOCS</a>
                  </div>
                </div>
              </div>
              <hr style="width: 100%; float: left; display: block;">
            </div>

            <div id="filter_unar_all" class="table table-responsive pelayanan-top unar_utama">
              <table id="tb_unar" class="display table table-striped pelayanan-table datatable">
                <thead>
                 <tr>
                    <th class="text-center" rowspan="2">Tanggal Buat</th>
                    <th class="text-center" rowspan="2">Provinsi</th>
                    <th class="text-center" rowspan="2">UPT</th>
                    <th class="text-center" rowspan="2">Tanggal Pelaksanaan Ujian</th>
                    <th class="text-center" rowspan="2">Lokasi Ujian</th>
                    <th class="text-center" rowspan="2">Kab/Kota</th>
                    <th class="text-center" colspan="3">Jumlah Peserta</th>
                    <th class="text-center" colspan="3">Lulus</th>
                    <th class="text-center" colspan="3">Tidak Lulus</th>
                    <th class="text-center" rowspan="2">Ket</th>
                    <th class="text-center" rowspan="2">Aksi</th>
                  </tr>
                  <tr>
                    <th  class="text-center">YD</th>
                    <th  class="text-center">YC</th>
                    <th  class="text-center">YB</th>

                    <th  class="text-center">YD</th>
                    <th  class="text-center">YC</th>
                    <th  class="text-center">YB</th>

                    <th  class="text-center">YD</th>
                    <th  class="text-center">YC</th>
                    <th  class="text-center">YB</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <?php } else { 
          echo "<div class='row'>
                  <div class='col-md-12'>
                      <h3>Anda tidak mempunyai akses apapun pada halaman ini</h3>
                  </div>
                </div>"; 
          echo "<script>
                  var  height_main =  $('.pelayanan-cnt').height();
                       $('#div_content').height(height_main);
                       $('#myTab').remove();
                </script>";
         } ?>
      </div>
    </div>
    </div>
  </div>

<script type="text/javascript">
function pilih_upt_loket_pelayanan(id_map) {
  call_lokasi_upt(id_map,'pelayanan_table_data_utama');
  $('.pelayanan_utama').hide();
}
function pilih_upt_detail_spp(id_map) {
  call_lokasi_upt(id_map,'spp_table_data_utama');
  $('.spp_utama').hide();
}
function pilih_upt_detail_isr(id_map) {
  call_lokasi_upt(id_map,'isr_table_data_utama');
  $('.isr_utama').hide();
}
function pilih_upt_detail_revoke(id_map) {
  call_lokasi_upt(id_map,'revoke_table_data_utama');
  $('.revoke_utama').hide();
}
function pilih_upt_detail_piutang(id_map) {
  call_lokasi_upt(id_map,'piutang_table_data_utama');
  $('.piutang_utama').hide();
}
function pilih_upt_detail_unar(id_map) {
  call_lokasi_upt(id_map,'unar_table_data_utama');
  $('.unar_utama').hide();
}
function  call_lokasi_upt(id_map,div_id){
   $.ajax({
         type: "POST",
         dataType: "html",
         url: "<?php echo $basepath ?>pelayanan/pilih_upt",
         data: "map_id="+id_map+"&div_id="+div_id,
         success: function(msg){
            if(msg){
              $("#"+div_id).html(msg);
            }
         }
      }); 
}

<?php  
//Kepala UPT atau Operator UPT
if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){  
  $wilayah   = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$_SESSION['upt_provinsi'])); 
  ?>
   window.onload =  function(){
           $(".if_upt").hide();
           detail_loket_pelayanan('<?php echo $wilayah['id_map'] ?>','<?php echo $_SESSION['kode_upt'] ?>');
           detail_spp('<?php echo $wilayah['id_map'] ?>','<?php echo $_SESSION['kode_upt'] ?>');
            setTimeout(function(){ 
               detail_isr('<?php echo $wilayah['id_map'] ?>','<?php echo $_SESSION['kode_upt'] ?>');
               detail_revoke('<?php echo $wilayah['id_map'] ?>','<?php echo $_SESSION['kode_upt'] ?>');
               detail_piutang('<?php echo $wilayah['id_map'] ?>','<?php echo $_SESSION['kode_upt'] ?>');
               detail_unar('<?php echo $wilayah['id_map'] ?>','<?php echo $_SESSION['kode_upt'] ?>');
            }, 3000);
    };
<?php } else { ?>


    //Start Chart Loket Pelayanan
    var chart_tujuan = [
      <?php 
            $loket_no_1 = 0;
            //Chart by tujuan
            $chart_tujuan="";
            $sql = "select count(*) as total,tj.tujuan
                   from tr_pelayanan as tr 
                   inner join ms_tujuan as tj 
                      on tj.kode_tujuan=tr.tujuan
                   where tr.status='3' GROUP BY tr.tujuan";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_tujuan .='{"name":"'.$tujuan.'","y":'.(empty($total) ? '0' : $total).'},';
              $loket_no_1++;
            } 
            echo trim($chart_tujuan,',');
      ?>
    ];

    var chart_kunjungan = [
      <?php 
            $loket_no_2 = 0;
            //Chart by kunjungan
            $chart_kunjungan="";
            $sql = "SELECT  pv.nama_prov,COUNT(kode_pelayanan) as total
                from tr_pelayanan as pl
                  inner join ms_provinsi as pv 
                      on pv.id_prov=pl.id_prov  where  pl.status='3' GROUP BY pv.nama_prov";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_kunjungan .='{"name":"'.$nama_prov.'","y":'.(empty($total) ? '0' : $total).'},';
              $loket_no_2++;
            } 
            echo trim($chart_kunjungan,',');
      ?>
    ];

    <?php 
        if($loket_no_1==0){
          echo " $('#div_loket_pelayanan_grafik_1').hide(); ";
        }
        if($loket_no_2==0){
          echo " $('#div_loket_pelayanan_grafik_2').hide(); ";   
        }
     ?>
    //End Chart Loket Pelayanan

    //Start Chart SPP
    var chart_spp_metode = [
      <?php 
            $spp_no_1 = 0;
            //Chart by metode
            $chart_metode="";
            $sql = "select count(*) as total,mtd.metode
                   from tr_spp as spp 
                   inner join ms_metode as mtd 
                      on mtd.id_metode=spp.metode
                   where spp.status='3' GROUP BY mtd.metode";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_metode .='{"name":"'.$metode.'","y":'.(empty($total) ? '0' : $total).'},';
              $spp_no_1++;
            } 
            echo trim($chart_metode,',');
      ?>
    ];

    var chart_spp_kunjungan = [
      <?php 
            $spp_no_2 = 0;
            //Chart by kunjungan
            $chart_kunjungan="";
            $sql = "SELECT  pv.nama_prov,COUNT(kode_spp) as total
                from tr_spp as spp
                  inner join ms_provinsi as pv 
                      on pv.id_prov=spp.id_prov  where  spp.status='3' GROUP BY pv.nama_prov";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_kunjungan .='{"name":"'.$nama_prov.'","y":'.(empty($total) ? '0' : $total).'},';
              $spp_no_2++;
            } 
            echo trim($chart_kunjungan,',');
      ?>
    ];

     <?php 
        if($spp_no_1==0){
          echo " $('#div_spp_grafik_1').hide(); ";
        }
        if($spp_no_2==0){
          echo " $('#div_spp_grafik_2').hide(); ";   
        }
     ?>
     //End Chart SPP

     //Start Chart ISR
    var chart_isr_metode = [
      <?php 
            $isr_no_1 = 0;
            //Chart by metode
            $chart_metode="";
            $sql = "select count(*) as total,mtd.metode
                   from tr_isr as isr 
                   inner join ms_metode as mtd 
                      on mtd.id_metode=isr.metode
                   where isr.status='3' GROUP BY mtd.metode";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_metode .='{"name":"'.$metode.'","y":'.(empty($total) ? '0' : $total).'},';
              $isr_no_1++;
            } 
            echo trim($chart_metode,',');
      ?>
    ];

    var chart_isr_kunjungan = [
      <?php 
            $isr_no_2 = 0;
            //Chart by kunjungan
            $chart_kunjungan="";
            $sql = "SELECT  pv.nama_prov,COUNT(kode_isr) as total
                from tr_isr as isr
                  inner join ms_provinsi as pv 
                      on pv.id_prov=isr.id_prov  where  isr.status='3' GROUP BY pv.nama_prov";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_kunjungan .='{"name":"'.$nama_prov.'","y":'.(empty($total) ? '0' : $total).'},';
              $isr_no_2++;
            } 
            echo trim($chart_kunjungan,',');
      ?>
    ];

      <?php 
        if($isr_no_1==0){
          echo " $('#div_isr_grafik_1').hide(); ";
        }
        if($isr_no_2==0){
          echo " $('#div_isr_grafik_2').hide(); ";   
        }
     ?>

     //End Chart ISR 

     

     //Start Chart Revoke
    var chart_revoke_metode = [
      <?php 
            $revoke_no_1 = 0;
            //Chart by metode
            $chart_metode="";
            $sql = "select count(*) as total,mtd.metode
                   from tr_revoke as rvk 
                   inner join ms_metode as mtd 
                      on mtd.id_metode=rvk.metode
                   where rvk.status='3' GROUP BY mtd.metode";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_metode .='{"name":"'.$metode.'","y":'.(empty($total) ? '0' : $total).'},';
              $revoke_no_1++;
            } 
            echo trim($chart_metode,',');
      ?>
    ];

    var chart_revoke_kunjungan = [
      <?php 
            $revoke_no_2 = 0;
            //Chart by kunjungan
            $chart_kunjungan="";
            $sql = "SELECT  pv.nama_prov,COUNT(kode_revoke) as total
                from tr_revoke as rvk
                  inner join ms_provinsi as pv 
                      on pv.id_prov=rvk.id_prov  where  rvk.status='3' GROUP BY pv.nama_prov";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_kunjungan .='{"name":"'.$nama_prov.'","y":'.(empty($total) ? '0' : $total).'},';
              $revoke_no_2++;
            } 
            echo trim($chart_kunjungan,',');
      ?>
    ];

    <?php 
        if($revoke_no_1==0){
          echo " $('#div_revoke_grafik_1').hide(); ";
        }
        if($revoke_no_2==0){
          echo " $('#div_revoke_grafik_2').hide(); ";   
        }
     ?>

     //End Chart Revoke


     //Start Chart Piutang
    var chart_piutang_kpknl = [
      <?php 
            $piutang_no_1 = 0;
            //Chart by kpknl
            $chart_kpknl="";
            $sql = "select count(*) as total,kb.nama_kab_kot
                   from tr_penanganan_piutang as pt 
                   inner join ms_kabupaten_kota as kb 
                      on kb.id_kabkot=pt.nama_kpknl
                   where pt.status='3' GROUP BY kb.nama_kab_kot";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_kpknl .='{"name":"KPKNL '.$nama_kab_kot.'","y":'.(empty($total) ? '0' : $total).'},';
              $piutang_no_1++;
            } 
            echo trim($chart_kpknl,',');
      ?>
    ];

    var chart_piutang_kunjungan = [
      <?php 
            $piutang_no_2 = 0;
            //Chart by kunjungan
            $chart_kunjungan="";
            $sql = "SELECT  pv.nama_prov,COUNT(kode_piutang) as total
                from tr_penanganan_piutang as pt
                  inner join ms_provinsi as pv 
                      on pv.id_prov=pt.id_prov  where  pt.status='3' GROUP BY pv.nama_prov";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_kunjungan .='{"name":"'.$nama_prov.'","y":'.(empty($total) ? '0' : $total).'},';
              $piutang_no_2++;
            } 
            echo trim($chart_kunjungan,',');
      ?>
    ];

     <?php 
        if($piutang_no_1==0){
          echo " $('#div_piutang_grafik_1').hide(); ";
        }
        if($piutang_no_2==0){
          echo " $('#div_piutang_grafik_2').hide(); ";   
        }
     ?>
     //End Chart Piutang 


     //Start Chart Unar
    var chart_unar_lulus_tdk_lulus = [
      <?php 
            $unar_no_1 = 0;
            //Chart by Lulus / Tidak Lulus
            $chart_unar_lulus_tdk_lulus="";
            $sql = "select sum(lulus_yd+lulus_yc+lulus_yb) as lulus,sum(tdk_lulus_yd+tdk_lulus_yc+tdk_lulus_yb)  as tdk_lulus  
                    from tr_unar where status='3' ";
            $get_chart_unar_1  = $db->Execute($sql);
            while($list = $get_chart_unar_1->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_unar_lulus_tdk_lulus .='{"name":"Lulus","y":'.(empty($lulus) ? '0' : $lulus).'},';
              $chart_unar_lulus_tdk_lulus .='{"name":"Tidak Lulus","y":'.(empty($tdk_lulus) ? '0' : $tdk_lulus).'},';
        if($lulus!=0 and $tdk_lulus!=0){
         $unar_no_1++;
        }
            } 
            echo trim($chart_unar_lulus_tdk_lulus,',');
      ?>
    ];
  

    var chart_unar_kunjungan = [
      <?php 
            $unar_no_2 = 0;
            //Chart by kunjungan
            $chart_kunjungan="";
            $sql = "SELECT  pv.nama_prov,COUNT(kode_unar) as total
                from tr_unar as un
                  inner join ms_provinsi as pv 
                      on pv.id_prov=un.id_prov  where  un.status='3' GROUP BY pv.nama_prov";
            $get_chart_unar_2  = $db->Execute($sql);
            while($list = $get_chart_unar_2->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_kunjungan .='{"name":"'.$nama_prov.'","y":'.(empty($total) ? '0' : $total).'},';
              $unar_no_2++;
            } 
            echo trim($chart_kunjungan,',');
      ?>
    ];

     <?php 
        if($unar_no_1==0){
          echo " $('#div_unar_grafik_1').hide(); ";
        }
        if($unar_no_2==0){
          echo " $('#div_unar_grafik_2').hide(); ";   
        }
     ?>
     //End Chart Unar

    //Map and chart
    window.onload =  function(){
      show_map('peta_loket_pelayanan','pilih_upt_loket_pelayanan');  
      show_map('peta_spp','pilih_upt_detail_spp');  
      show_map('peta_isr','pilih_upt_detail_isr');  
      show_map('peta_revoke','pilih_upt_detail_revoke');  
      show_map('peta_penanganan_piutang','pilih_upt_detail_piutang'); 
      show_map('peta_unar','pilih_upt_detail_unar');  

      pie_chart('loket_pelayanan_grafik_1',chart_tujuan);
      pie_chart('loket_pelayanan_grafik_2',chart_kunjungan); 

      pie_chart('spp_grafik_1',chart_spp_metode);
      pie_chart('spp_grafik_2',chart_spp_kunjungan);

      pie_chart('isr_grafik_1',chart_isr_metode);
      pie_chart('isr_grafik_2',chart_isr_kunjungan);

      pie_chart('revoke_grafik_1',chart_revoke_metode);
      pie_chart('revoke_grafik_2',chart_revoke_kunjungan);

      pie_chart('piutang_grafik_1',chart_piutang_kpknl);
      pie_chart('piutang_grafik_2',chart_piutang_kunjungan); 

      pie_chart('unar_grafik_1',chart_unar_lulus_tdk_lulus);
      pie_chart('unar_grafik_2',chart_unar_kunjungan);

   };
          

<?php } ?>
  
    function do_delete_grp(parameter_data,param_id){
        act_delete('Hapus Group Perizinan','Anda ingin menghapus Group Perizinan ? ','warning','grp_layanan/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
    }
    function do_edit_grp(param_id){
      call_modal_default_no_full('Ubah Group Perizinan','Form_Perizinan','do_edit','grp_layanan/form');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>grp_layanan/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#id_jenis").val(data.id_jenis);
                    $("#jenis").val(data.jenis);
                }
            });
       }, 1000);
    } 

      //Loket
      var dTable_utama_loket;
        $(document).ready(function() {
          dTable_utama_loket = $('#tb_loket').DataTable( {
            "bProcessing": true,
            "searching": false,
            "bServerSide": true,
            "bJQueryUI": false,
            "retrieve": true,
            "responsive": false,
            "autoWidth": false,
            "sAjaxSource": "<?php echo $basepath ?>pelayanan_loket/rest", 
            "sServerMethod": "POST",
            "scrollX": true,
            "scrollY": "350px",
              "scrollCollapse": true,
           "order": [[ 0, "desc" ]],
            "columnDefs": [
            { "orderable": true, "targets": 0, "searchable": true},
            { "orderable": true, "targets": 1, "searchable": true},
            { "orderable": true, "targets": 2, "searchable": true},
            { "orderable": true, "targets": 3, "searchable": true},
            { "orderable": true, "targets": 4, "searchable": true },
            { "orderable": true, "targets": 5, "searchable": true },
            { "orderable": true, "targets": 6, "searchable": true },
            { "orderable": true, "targets": 7, "searchable": true },
            { "orderable": true, "targets": 8, "searchable": true },
            { "orderable": true, "targets": 9, "searchable": true },
            { "orderable": true, "targets": 10, "searchable": true },
            { "orderable": true, "targets": 11, "searchable": true },
            { "orderable": false, "targets": 12, "searchable": false, "width":200}
            ]
          } );
      });

      //Loket Akumulasi
      var dTable_utama_loket_akumulasi;
         $(document).ready(function() {
          dTable_utama_loket_akumulasi = $('#tb_loket_akumulasi').DataTable( {
            "bProcessing": true,
            "bServerSide": true,
            "bJQueryUI": false,
            "responsive": false,
            "autoWidth": false,
            "sAjaxSource": "<?php echo $basepath ?>pelayanan_loket/akumulasi", 
            "sServerMethod": "POST",
            "scrollX": true,
            "scrollY": "350px",
              "scrollCollapse": true,
            "columnDefs": [
            { "orderable": true, "targets": 0, "searchable": true},
            { "orderable": true, "targets": 1, "searchable": true},
            <?php
            $no = 2;
            $dt="";
            for ($x=1;$x <= $count_tj; $x++) {
               $dt .= '{ "orderable": false, "targets": '.$no.', "searchable": false},'.PHP_EOL;
               $no++;

            }
            echo $dt;
            ?>

            { "orderable": false, "targets":  <?php echo $no; ?>, "searchable": false}
            ]
          } );
      });

      //SPP
      var dTable_utama_spp;
        $(document).ready(function() {
          dTable_utama_spp = $('#tb_spp').DataTable( {
            "bProcessing": true,
            "bServerSide": true,
            "searching": false,
            "bJQueryUI": false,
            "retrieve": true,
            "responsive": false,
            "autoWidth": false,
            "sAjaxSource": "<?php echo $basepath ?>pelayanan_spp/rest", 
            "sServerMethod": "POST",
            "scrollX": true,
            "scrollY": "350px",
              "scrollCollapse": true,
              "order": [[ 0, "desc" ]],
            "columnDefs": [
            { "orderable": true, "targets": 0, "searchable": true},
            { "orderable": true, "targets": 1, "searchable": true},
            { "orderable": true, "targets": 2, "searchable": true},
            { "orderable": true, "targets": 3, "searchable": true},
            { "orderable": true, "targets": 4, "searchable": true },
            { "orderable": true, "targets": 5, "searchable": true },
            { "orderable": true, "targets": 6, "searchable": true },
            { "orderable": true, "targets": 7, "searchable": true },
            { "orderable": true, "targets": 8, "searchable": true },
            { "orderable": true, "targets": 9, "searchable": true },
            { "orderable": true, "targets": 10, "searchable": true },
            { "orderable": true, "targets": 11, "searchable": true },
            { "orderable": true, "targets": 12, "searchable": true },
            { "orderable": false, "targets": 13, "searchable": false, "width":200}
            ]
          } );
        });

      //ISR
      var dTable_utama_isr;
        $(document).ready(function() {
          dTable_utama_isr = $('#tb_isr').DataTable( {
            "bProcessing": true,
            "bServerSide": true,
            "searching": false,
            "bJQueryUI": false,
            "retrieve": true,
            "responsive": false,
            "autoWidth": false,
            "sAjaxSource": "<?php echo $basepath ?>pelayanan_isr/rest", 
            "sServerMethod": "POST",
            "scrollX": true,
            "scrollY": "350px",
              "scrollCollapse": true,
              "order": [[ 0, "desc" ]],
            "columnDefs": [
            { "orderable": true, "targets": 0, "searchable": true},
            { "orderable": true, "targets": 1, "searchable": true},
            { "orderable": true, "targets": 2, "searchable": true},
            { "orderable": true, "targets": 3, "searchable": true},
            { "orderable": true, "targets": 4, "searchable": true },
            { "orderable": true, "targets": 5, "searchable": true },
            { "orderable": true, "targets": 6, "searchable": true },
            { "orderable": true, "targets": 7, "searchable": true },
            { "orderable": true, "targets": 8, "searchable": true },
            { "orderable": false, "targets": 9, "searchable": false, "width":200}
            ]
          } );
        });

      //Revoke
      var dTable_utama_revoke;
       $(document).ready(function() {
        dTable_utama_revoke = $('#tb_revoke').DataTable( {
          "bProcessing": true,
          "bServerSide": true,
          "searching": false,
          "bJQueryUI": false,
          "retrieve": true,
          "responsive": false,
          "autoWidth": false,
          "sAjaxSource": "<?php echo $basepath ?>pelayanan_revoke/rest", 
          "sServerMethod": "POST",
          "scrollX": true,
          "scrollY": "350px",
            "scrollCollapse": true,
            "order": [[ 0, "desc" ]],
          "columnDefs": [
          { "orderable": true, "targets": 0, "searchable": true},
          { "orderable": true, "targets": 1, "searchable": true},
          { "orderable": true, "targets": 2, "searchable": true},
          { "orderable": true, "targets": 3, "searchable": true},
          { "orderable": true, "targets": 4, "searchable": true },
          { "orderable": true, "targets": 5, "searchable": true },
          { "orderable": true, "targets": 6, "searchable": true },
          { "orderable": true, "targets": 7, "searchable": true },
          { "orderable": true, "targets": 8, "searchable": true },
          { "orderable": true, "targets": 9, "searchable": true },
          { "orderable": false, "targets": 10, "searchable": false, "width":200}
          ]
        } );
      });

      //Penanganan Piutang
      var dTable_utama_piutang;
       $(document).ready(function() {
        dTable_utama_piutang = $('#tb_piutang').DataTable( {
          "bProcessing": true,
          "bServerSide": true,
          "searching": false,
          "bJQueryUI": false,
          "retrieve": true,
          "responsive": false,
          "autoWidth": false,
          "sAjaxSource": "<?php echo $basepath ?>pelayanan_piutang/rest", 
          "sServerMethod": "POST",
          "scrollX": true,
          "scrollY": "350px",
           "scrollCollapse": true,
           "order": [[ 0, "desc" ]],
          "columnDefs": [
          { "orderable": true, "targets": 0, "searchable": true},
          { "orderable": true, "targets": 1, "searchable": true},
          { "orderable": true, "targets": 2, "searchable": true},
          { "orderable": true, "targets": 3, "searchable": true},
          { "orderable": true, "targets": 4, "searchable": true },
          { "orderable": true, "targets": 5, "searchable": true },
          { "orderable": true, "targets": 6, "searchable": true },
          { "orderable": true, "targets": 7, "searchable": true },
          { "orderable": true, "targets": 8, "searchable": true },
          { "orderable": true, "targets": 9, "searchable": true },
          { "orderable": true, "targets": 10, "searchable": true },
          { "orderable": true, "targets": 11, "searchable": true },
          { "orderable": true, "targets": 12, "searchable": true },
          { "orderable": true, "targets": 13, "searchable": true },
          { "orderable": true, "targets": 14, "searchable": true },
          { "orderable": true, "targets": 15, "searchable": true },
          { "orderable": false, "targets": 16, "searchable": false, "width":200}
          ]
        } );
      });

      
      //Unar
      var dTable_utama_unar;
       $(document).ready(function() {
        dTable_utama_unar = $('#tb_unar').DataTable( {
          "bProcessing": true,
          "bServerSide": true,
          "searching": false,
          "bJQueryUI": false,
          "retrieve": true,
          "responsive": false,
          "autoWidth": false,
          "sAjaxSource": "<?php echo $basepath ?>pelayanan_unar/rest", 
          "sServerMethod": "POST",
          "scrollX": true,
          "scrollY": "350px",
           "scrollCollapse": true,
           "order": [[ 0, "desc" ]],
          "columnDefs": [
          { "orderable": true, "targets": 0, "searchable": true},
          { "orderable": true, "targets": 1, "searchable": true},
          { "orderable": true, "targets": 2, "searchable": true},
          { "orderable": true, "targets": 3, "searchable": true},
          { "orderable": true, "targets": 4, "searchable": true },
          { "orderable": true, "targets": 5, "searchable": true },
          { "orderable": true, "targets": 6, "searchable": true },
          { "orderable": true, "targets": 7, "searchable": true },
          { "orderable": true, "targets": 8, "searchable": true },
          { "orderable": true, "targets": 9, "searchable": true },
          { "orderable": true, "targets": 10, "searchable": true },
          { "orderable": true, "targets": 11, "searchable": true },
          { "orderable": true, "targets": 12, "searchable": true },
          { "orderable": true, "targets": 13, "searchable": true },
          { "orderable": true, "targets": 14, "searchable": true },
          { "orderable": true, "targets": 15, "searchable": true },
          { "orderable": false, "targets": 16, "searchable": false, "width":200}
          ]
        } );
      }); 


    //Start Loket Pelayanan
    function after_crud_loket_pelayanan(){
        var map_id = $(".data_map_id_pengunjung").val();
        var upt = $(".data_upt_pengunjung").val();
        detail_loket_pelayanan(map_id,upt);
        $('#call_modal').modal('hide');
    }
    function detail_loket_pelayanan(id_map,upt){
      $(".data_map_id_pengunjung").val(id_map);
      $(".data_upt_pengunjung").val(upt);
      $('.pelayanan_utama').hide();
      ajax_table_map('pelayanan_table_data_utama',id_map,upt,'pelayanan/table');
      //$('html, body').animate({
           // scrollTop: $("#pelayanan_table_data_utama").offset().top;
      //}, 1000);
      refresh_table();
    }     
    function do_edit_pl_all(param_id){
      call_modal_default('Ubah Profile & Data Pengunjung UPT','Form_Pelayanan_Loket_ALL','do_edit','pelayanan/form_all');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>pelayanan_loket/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_pelayanan").val(data.kode_pelayanan);
                    $('#tgl_pelayanan').datepicker('update',data.tgl_pelayanan);
                    $("#nama_pengunjung").val(data.nama_pengunjung);
                    $("#kode_perusahaan").val(data.kode_perusahaan);
                    $("#nama_perusahaan").val(data.nama_perusahaan);
                    $("#alamat_perusahaan").val(data.alamat_perusahaan);
                    $("#jabatan_pengunjung").val(data.jabatan_pengunjung);
                    $("#no_tlp").val(data.no_tlp);
                    $("#no_hp").val(data.no_hp);
                    $("#email").val(data.email);
                    $("#tujuan").val(data.tujuan);
                    $("#keterangan").val(data.keterangan);
                    $("#perizinan").val(data.jenis_perizinan);
                    $("#upt_provinsi").val(data.id_prov);

                      $.ajax({
                        url: '<?php echo $basepath ?>pelayanan/get_group_perizinan',
                        data: 'jenis='+data.jenis_perizinan,
                        type: 'POST',
                        dataType: 'html',
                        success: function(result_data) {
                            $("#group_perizinan").html(result_data);
                            $("#group_perizinan").val(data.group_perizinan);
                        }
                    });
                     $.ajax({
                         type: "POST",
                         dataType: "html",
                         url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
                         data: "provinsi="+data.id_prov,
                         success: function(msg){
                            if(msg){
                              $("#kode_upt").html(msg);

                              setTimeout(function(){ 
                                   $("#kode_upt").val(data.kode_upt);        
                              }, 500);
                             
                            }
                         }
                      }); 
                }
            });
       }, 1000);
    }
    function do_edit_pl(param_id,map_id,upt){
      call_modal_from_map('Ubah Profile & Data Pengunjung UPT','Form_Pelayanan_Loket',map_id,upt,'do_edit','pelayanan/form');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>pelayanan_loket/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_pelayanan").val(data.kode_pelayanan);
                    $('#tgl_pelayanan').datepicker('update',data.tgl_pelayanan);
                    $("#nama_pengunjung").val(data.nama_pengunjung);
                    $("#kode_perusahaan").val(data.kode_perusahaan);
                    $("#nama_perusahaan").val(data.nama_perusahaan);
                    $("#alamat_perusahaan").val(data.alamat_perusahaan);
                    $("#jabatan_pengunjung").val(data.jabatan_pengunjung);
                    $("#no_tlp").val(data.no_tlp);
                    $("#no_hp").val(data.no_hp);
                    $("#loket_upt").val(data.kode_upt);
                    $("#email").val(data.email);
                    $("#tujuan").val(data.tujuan);
                    $("#keterangan").val(data.keterangan);
                    $("#perizinan").val(data.jenis_perizinan);

                      $.ajax({
                        url: '<?php echo $basepath ?>pelayanan/get_group_perizinan',
                        data: 'jenis='+data.jenis_perizinan,
                        type: 'POST',
                        dataType: 'html',
                        success: function(result_data) {
                            $("#group_perizinan").html(result_data);
                            $("#group_perizinan").val(data.group_perizinan);
                        }
                    });

                }
            });
       }, 1000);
    }
    function do_detail_pl(param_id,map_id,upt){
      call_modal_from_map('Detail Profile & Data Pengunjung UPT','Form_Pelayanan_Loket',map_id,upt,'do_detail','pelayanan/form');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>pelayanan_loket/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_pelayanan").val(data.kode_pelayanan);
                    $('#tgl_pelayanan').val(data.tgl_pelayanan);
                    $("#nama_pengunjung").val(data.nama_pengunjung);
                    $("#kode_perusahaan").val(data.kode_perusahaan);
                    $("#nama_perusahaan").val(data.nama_perusahaan);
                    $("#alamat_perusahaan").val(data.alamat_perusahaan);
                    $("#jabatan_pengunjung").val(data.jabatan_pengunjung);
                    $("#no_tlp").val(data.no_tlp);
                    $("#no_hp").val(data.no_hp);
                    $("#email").val(data.email);
                    $("#tujuan").val(data.tujuan);
                    $("#keterangan").val(data.keterangan);
                    $("#perizinan").val(data.jenis_perizinan);

                      $.ajax({
                        url: '<?php echo $basepath ?>pelayanan/get_group_perizinan',
                        data: 'jenis='+data.jenis_perizinan,
                        type: 'POST',
                        dataType: 'html',
                        success: function(result_data) {
                            $("#group_perizinan").html(result_data);
                            $("#group_perizinan").val(data.group_perizinan);
                        }
                    });

                }
            });
       }, 1000);
    }
    function do_delete_pl(parameter_data,param_id){
        act_delete('Hapus Profil & Data Pengunjung','Anda ingin menghapus Profil & Data Pengunjung ? ','warning','pelayanan_loket/delete','no_refresh','after_crud_loket_pelayanan',window.atob(parameter_data),window.atob(param_id));
    }
    function do_delete_pl_all(parameter_data,param_id){
        act_delete('Hapus Profil & Data Pengunjung','Anda ingin menghapus Profil & Data Pengunjung ? ','warning','pelayanan_loket/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
    }
    //End Loket Pelayanan



    //Start SPP
    function after_crud_spp(){
        var map_id = $(".data_map_id_spp").val();
        var upt    = $(".data_upt_spp").val();
        detail_spp(map_id,upt);
        $('#call_modal').modal('hide');
    }
    function detail_spp(id_map,upt){
      $(".data_map_id_spp").val(id_map);
      $(".data_upt_spp").val(upt);
      $('.spp_utama').hide();
      ajax_table_map('spp_table_data_utama',id_map,upt,'pelayanan/table');
      //$('html, body').animate({
            scrollTop: $("#spp_table_data_utama").offset().top;
      //}, 1000);
      refresh_table();
    }
    function do_edit_spp(param_id,map_id,upt){
      call_modal_from_map('Ubah SPP','Form_SPP',map_id,upt,'do_edit','pelayanan/form');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>pelayanan_spp/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_spp").val(data.kode_spp);
                    $("#no_spp").val(data.no_spp);
                    $('#tgl_jatuh_tempo').datepicker('update',data.tgl_jatuh_tempo);
                    $('#tgl_terima_waba').datepicker('update',data.tgl_terima_waba);
                    $("#nomor_spp").val(data.no_spp);
                    $("#kode_perusahaan").val(data.no_klien_licence);
                    $("#service").val(data.service);
                    $("#no_aplikasi").val(data.no_aplikasi);
                    $("#kategori_spp").val(data.kategori_spp);
                    $("#nilai_bhp").val(data.tagihan);
                    $("#metode").val(data.metode);
                    $("#keterangan").val(data.keterangan);
                    $("#nama_perusahaan").val(data.company_name);
                    //$('#lampiran_spp').attr('href',data.lampiran);

                      $.ajax({
                             type: "POST",
                             dataType: "html",
                             url: "<?php echo $basepath ?>pelayanan_spp/get_files",
                             data: "aksi=do_edit&kode_spp="+param_id,
                             success: function(msg){
                                if(msg){
                                  $("#add_data").after(msg);
                                }
                             }
                          }); 
                }
            });
       }, 1000);
    }
    function do_edit_spp_all(param_id){
      call_modal_default('Ubah SPP','Form_SPP_ALL','do_edit','pelayanan/form_all');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>pelayanan_spp/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_spp").val(data.kode_spp);
                    $("#no_spp").val(data.no_spp);
                    $('#tgl_terima_waba').val(data.tgl_terima_waba);
                    $('#tgl_jatuh_tempo').val(data.tgl_jatuh_tempo);
                    $("#nomor_spp").val(data.no_spp);
                    $("#kode_perusahaan").val(data.no_klien_licence);
                    $("#service").val(data.service);
                    $("#no_aplikasi").val(data.no_aplikasi);
                    $("#kategori_spp").val(data.kategori_spp);
                    $("#nilai_bhp").val(data.tagihan);
                    $("#metode").val(data.metode);
                    $("#upt_provinsi").val(data.id_prov);
                    $("#keterangan").val(data.keterangan);
                    $("#nama_perusahaan").val(data.company_name);
                    //$('#lampiran_spp').attr('href',data.lampiran);
                     $.ajax({
                             type: "POST",
                             dataType: "html",
                             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
                             data: "provinsi="+data.id_prov,
                             success: function(msg){
                                if(msg){
                                  $("#kode_upt").html(msg);

                                  setTimeout(function(){ 
                                       $("#kode_upt").val(data.kode_upt);        
                                  }, 500);
                                 
                                }
                             }
                          }); 

                     $.ajax({
                             type: "POST",
                             dataType: "html",
                             url: "<?php echo $basepath ?>pelayanan_spp/get_files",
                             data: "aksi=do_edit&kode_spp="+param_id,
                             success: function(msg){
                                if(msg){
                                  $("#add_data").after(msg);
                                }
                             }
                          }); 

                }
            });
       }, 1000);
    }
    function do_detail_spp(param_id,map_id,upt){
      call_modal_from_map('Detail SPP','Form_SPP',map_id,upt,'do_detail','pelayanan/form');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>pelayanan_spp/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_spp").val(data.kode_spp);
                    $("#no_spp").val(data.no_spp);
                    $('#tgl_jatuh_tempo').val(data.tgl_jatuh_tempo);
                    $('#tgl_terima_waba').val(data.tgl_terima_waba);
                    $("#nomor_spp").val(data.no_spp);
                    $("#kode_perusahaan").val(data.no_klien_licence);
                    $("#service").val(data.service);
                    $("#no_aplikasi").val(data.no_aplikasi);
                    $("#kategori_spp").val(data.kategori_spp);
                    $("#nilai_bhp").val(data.tagihan);
                    $("#metode").val(data.metode);
                    $("#keterangan").val(data.keterangan);
                    $("#nama_perusahaan").val(data.company_name);
                    $('#lampiran_spp').attr('href',data.lampiran);
                }
            });
       }, 1000);
    } 
    function do_delete_spp(parameter_data,param_id){
        act_delete('Hapus SPP','Anda ingin menghapus SPP ? ','warning','pelayanan_spp/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
    }
    function do_delete_spp_all(parameter_data,param_id){
        act_delete('Hapus SPP','Anda ingin menghapus SPP ? ','warning','pelayanan_spp/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
    }
    //End SPP


    //Start ISR
    function after_crud_isr(){
        var map_id = $(".data_map_id_isr").val();
        var upt    = $(".data_upt_isr").val();
        detail_isr(map_id,upt);
        $('#call_modal').modal('hide');
    }
    function detail_isr(id_map,upt){
      $(".data_map_id_isr").val(id_map);
      $(".data_upt_isr").val(upt);
      $('.isr_utama').hide();
      ajax_table_map('isr_table_data_utama',id_map,upt,'pelayanan/table');
      //$('html, body').animate({
            scrollTop: $("#isr_table_data_utama").offset().top
      //}, 1000);
    }
    function do_detail_isr(param_id,map_id,upt){
      call_modal_from_map('Detail ISR','Form_ISR',map_id,upt,'do_detail','pelayanan/form');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>pelayanan_isr/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_isr").val(data.kode_isr);
                    $("#no_spp").val(data.no_spp);
                    $('#tgl_terima_waba').val(data.tgl_terima_waba);
                    $("#nomor_spp").val(data.no_spp);
                    $("#no_client").val(data.no_client);
                    $("#nama_pemegang").val(data.client_name);
                    $("#service").val(data.service);
                    $("#no_aplikasi").val(data.no_aplikasi);
                    $("#jenis_spp").val(data.jenis_spp);
                    $("#nilai_bhp").val(data.tagihan);
                    $("#metode").val(data.metode);
                    $("#keterangan").val(data.keterangan);
                }
            });
       }, 1000);
    }
    function do_edit_isr_all(param_id){
       call_modal_default('Ubah ISR','Form_ISR_ALL','do_edit','pelayanan/form_all');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>pelayanan_isr/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_isr").val(data.kode_isr);
                    $("#no_spp").val(data.no_spp);
                    $('#tgl_terima_waba').datepicker('update',data.tgl_terima_waba);
                    $("#nomor_spp").val(data.no_spp);
                    $("#no_client").val(data.no_client);
                    $("#nama_pemegang").val(data.client_name);
                    $("#service").val(data.service);
                    $("#no_aplikasi").val(data.no_aplikasi);
                    $("#jenis_spp").val(data.jenis_spp);
                    $("#nilai_bhp").val(data.tagihan);
                    $("#metode").val(data.metode);
                    $("#upt_provinsi").val(data.id_prov);
                    $("#keterangan").val(data.keterangan);

                     $.ajax({
                             type: "POST",
                             dataType: "html",
                             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
                             data: "provinsi="+data.id_prov,
                             success: function(msg){
                                if(msg){
                                  $("#kode_upt").html(msg);

                                  setTimeout(function(){ 
                                       $("#kode_upt").val(data.kode_upt);        
                                  }, 500);
                                 
                                }
                             }
                          }); 

                }
            });
       }, 1000);
    }
    function do_edit_isr(param_id,map_id,upt){
      call_modal_from_map('Ubah ISR','Form_ISR',map_id,upt,'do_edit','pelayanan/form');
      setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>pelayanan_isr/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_isr").val(data.kode_isr);
                    $("#no_spp").val(data.no_spp);
                    $('#tgl_terima_waba').datepicker('update',data.tgl_terima_waba);
                    $("#nomor_spp").val(data.no_spp);
                    $("#no_client").val(data.no_client);
                    $("#nama_pemegang").val(data.client_name);
                    $("#service").val(data.service);
                    $("#no_aplikasi").val(data.no_aplikasi);
                    $("#jenis_spp").val(data.jenis_spp);
                    $("#nilai_bhp").val(data.tagihan);
                    $("#metode").val(data.metode);
                    $("#keterangan").val(data.keterangan);
                }
            });
       }, 1000);
    } 
    function do_delete_isr(parameter_data,param_id){
        act_delete('Hapus ISR','Anda ingin menghapus ISR ? ','warning','pelayanan_isr/delete','no_refresh','after_crud_isr',window.atob(parameter_data),window.atob(param_id));
    }
    function do_delete_isr_all(parameter_data,param_id){
        act_delete('Hapus ISR','Anda ingin menghapus ISR ? ','warning','pelayanan_isr/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
    }
    //End ISR


    //Start Revoke
    function after_crud_revoke(){
        var map_id = $(".data_map_id_revoke").val();
        var upt    = $(".data_upt_revoke").val();
        detail_revoke(map_id,upt);
        $('#call_modal').modal('hide');
    }
    function detail_revoke(id_map,upt){
      $(".data_map_id_revoke").val(id_map);
      $(".data_upt_revoke").val(upt);
      $('.revoke_utama').hide();
      ajax_table_map('revoke_table_data_utama',id_map,upt,'pelayanan/table');
      //$('html, body').animate({
            scrollTop: $("#revoke_table_data_utama").offset().top
      //}, 1000);
    }
    function do_detail_revoke(param_id,map_id,upt){
      call_modal_from_map('Detail Revoke','Form_Revoke',map_id,upt,'do_detail','pelayanan/form');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>pelayanan_revoke/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_revoke").val(data.kode_revoke);
                    $("#no_spp").val(data.no_spp);
                    $('#tgl_terima_waba').val(data.tgl_terima_waba);
                    $('#tgl_isr_dicabut').val(data.tgl_isr_dicabut);
                    $("#nomor_spp").val(data.no_spp);
                    $("#no_client").val(data.no_client);
                    $("#nama_pemegang").val(data.client_name);
                    $("#service").val(data.service);
                    $("#no_aplikasi").val(data.no_aplikasi);
                    $("#jenis_spp").val(data.jenis_spp);
                    $("#nilai_bhp").val(data.tagihan);
                    $("#metode").val(data.metode);
                    $("#keterangan").val(data.keterangan);
                }
            });
       }, 1000);
    }
    function do_edit_revoke_all(param_id){
       call_modal_default('Ubah Revoke','Form_Revoke_ALL','do_edit','pelayanan/form_all');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>pelayanan_revoke/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_revoke").val(data.kode_revoke);
                    $("#no_spp").val(data.no_spp);
                    $('#tgl_terima_waba').datepicker('update',data.tgl_terima_waba);
                    $('#tgl_isr_dicabut').datepicker('update',data.tgl_isr_dicabut);
                    $("#nomor_spp").val(data.no_spp);
                    $("#no_client").val(data.no_client);
                    $("#nama_pemegang").val(data.client_name);
                    $("#service").val(data.service);
                    $("#no_aplikasi").val(data.no_aplikasi);
                    $("#jenis_spp").val(data.jenis_spp);
                    $("#nilai_bhp").val(data.tagihan);
                    $("#metode").val(data.metode);
                    $("#upt_provinsi").val(data.id_prov);
                    $("#keterangan").val(data.keterangan);

                     $.ajax({
                             type: "POST",
                             dataType: "html",
                             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
                             data: "provinsi="+data.id_prov,
                             success: function(msg){
                                if(msg){
                                  $("#kode_upt").html(msg);

                                  setTimeout(function(){ 
                                       $("#kode_upt").val(data.kode_upt);        
                                  }, 500);
                                 
                                }
                             }
                          }); 

                }
            });
       }, 1000);
    }
    function do_edit_revoke(param_id,map_id,upt){
      call_modal_from_map('Ubah Revoke','Form_Revoke',map_id,upt,'do_edit','pelayanan/form');
      setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>pelayanan_revoke/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_revoke").val(data.kode_revoke);
                    $("#no_spp").val(data.no_spp);
                    $('#tgl_terima_waba').datepicker('update',data.tgl_terima_waba);
                    $('#tgl_isr_dicabut').datepicker('update',data.tgl_isr_dicabut);
                    $("#nomor_spp").val(data.no_spp);
                    $("#no_client").val(data.no_client);
                    $("#nama_pemegang").val(data.client_name);
                    $("#service").val(data.service);
                    $("#no_aplikasi").val(data.no_aplikasi);
                    $("#jenis_spp").val(data.jenis_spp);
                    $("#nilai_bhp").val(data.tagihan);
                    $("#metode").val(data.metode);
                    $("#keterangan").val(data.keterangan);
                }
            });
       }, 1000);
    } 
    function do_delete_revoke(parameter_data,param_id){
        act_delete('Hapus Revoke','Anda ingin menghapus Revoke ? ','warning','pelayanan_revoke/delete','no_refresh','after_crud_revoke',window.atob(parameter_data),window.atob(param_id));
    }
    function do_delete_revoke_all(parameter_data,param_id){
        act_delete('Hapus Revoke','Anda ingin menghapus Revoke ? ','warning','pelayanan_revoke/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
    }
    //End Revoke


    //Start Penanganan Piutang
    function after_crud_piutang(){
        var map_id = $(".data_map_id_piutang").val();
        var upt    = $(".data_upt_piutang").val();
        detail_piutang(map_id,upt);
        $('#call_modal').modal('hide');
    }
    function detail_piutang(id_map,upt){
      $(".data_map_id_piutang").val(id_map);
      $(".data_upt_piutang").val(upt);
      $('.piutang_utama').hide();
      ajax_table_map('piutang_table_data_utama',id_map,upt,'pelayanan/table');
      //$('html, body').animate({
            scrollTop: $("#piutang_table_data_utama").offset().top
      //}, 1000);
    }
    function do_detail_piutang(param_id,map_id,upt){
      call_modal_from_map('Detail Penanganan Piutang','Form_Piutang',map_id,upt,'do_detail','pelayanan/form');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>pelayanan_piutang/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_piutang").val(data.kode_piutang);
                    $("#nama_perusahaan").val(data.client_name);
                    $("#nilai_penyerahan").val(data.nilai_penyerahan);
                    $('#tanggal_piutang').val(data.tanggal);
                    $("#thn_pelimpahan").val(data.thn_pelimpahan);
                    $("#no_client").val(data.no_client);
                    $("#thp_pengurusan").val(data.tahapan_pengurusan);
                    $("#lunas").val(data.lunas);
                    $("#angsuran").val(data.angsuran);
                    $("#psbdt").val(data.psbdt);
                    $("#pembatalan").val(data.pembatalan);
                    $("#sisa_piutang").val(data.sisa_piutang);
                    $("#keterangan").val(data.keterangan);

                      $.ajax({
                       type: "POST",
                       dataType: "html",
                       url: "<?php echo $basepath ?>daftar_pengguna/get_kpknl",
                       data: "provinsi="+data.id_prov,
                       success: function(msg_data){
                          if(msg_data){
                              $("#nama_kpknl").html(msg_data);

                              setTimeout(function(){ 
                                   $("#nama_kpknl").val(data.nama_kpknl);        
                              }, 500);
                             
                            }
                       }
                    }); 
                }
            });
       }, 1000);
    }
    function do_edit_piutang_all(param_id){
       call_modal_default('Ubah Penanganan Piutang','Form_Piutang_ALL','do_edit','pelayanan/form_all');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>pelayanan_piutang/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_piutang").val(data.kode_piutang);
                    $("#nama_perusahaan").val(data.client_name);
                    $("#nilai_penyerahan").val(data.nilai_penyerahan);
                    $('#tanggal_piutang').datepicker('update',data.tanggal);
                    $("#thn_pelimpahan").val(data.thn_pelimpahan);
                    $("#no_client").val(data.no_client);
                    $("#thp_pengurusan").val(data.tahapan_pengurusan);
                    $("#lunas").val(data.lunas);
                    $("#angsuran").val(data.angsuran);
                    $("#psbdt").val(data.psbdt);
                    $("#pembatalan").val(data.pembatalan);
                    $("#sisa_piutang").val(data.sisa_piutang);
                    $("#upt_provinsi").val(data.id_prov);
                    $("#keterangan").val(data.keterangan);

                     $.ajax({
                             type: "POST",
                             dataType: "html",
                             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
                             data: "provinsi="+data.id_prov,
                             success: function(msg){
                                if(msg){
                                  $("#kode_upt").html(msg);

                                  setTimeout(function(){ 
                                       $("#kode_upt").val(data.kode_upt);        
                                  }, 500);
                                 
                                }
                             }
                          });


                      $.ajax({
                       type: "POST",
                       dataType: "html",
                       url: "<?php echo $basepath ?>daftar_pengguna/get_kpknl",
                       data: "provinsi="+data.id_prov,
                       success: function(msg_data){
                          if(msg_data){
                            $("#nama_kpknl").html(msg_data);        
                          }
                       }
                    }); 


                    $.ajax({
                       type: "POST",
                       dataType: "html",
                       url: "<?php echo $basepath ?>daftar_pengguna/get_kpknl",
                       data: "provinsi="+data.id_prov,
                       success: function(msg_data){
                          if(msg_data){
                              $("#nama_kpknl").html(msg_data);

                              setTimeout(function(){ 
                                   $("#nama_kpknl").val(data.nama_kpknl);        
                              }, 500);
                             
                            }
                       }
                    }); 


                }
            });
       }, 1000);
    }
    function do_edit_piutang(param_id,map_id,upt){
      call_modal_from_map('Ubah Penanganan Piutang','Form_Piutang',map_id,upt,'do_edit','pelayanan/form');
      setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>pelayanan_piutang/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_piutang").val(data.kode_piutang);
                    $("#nama_perusahaan").val(data.client_name);
                    $("#nilai_penyerahan").val(data.nilai_penyerahan);
                    $('#tanggal_piutang').datepicker('update',data.tanggal);
                    $("#thn_pelimpahan").val(data.thn_pelimpahan);
                    $("#no_client").val(data.no_client);
                    $("#thp_pengurusan").val(data.tahapan_pengurusan);
                    $("#nama_kpknl").val(data.nama_kpknl);
                    $("#lunas").val(data.lunas);
                    $("#angsuran").val(data.angsuran);
                    $("#psbdt").val(data.psbdt);
                    $("#pembatalan").val(data.pembatalan);
                    $("#sisa_piutang").val(data.sisa_piutang);
                    $("#keterangan").val(data.keterangan);

                    $.ajax({
                       type: "POST",
                       dataType: "html",
                       url: "<?php echo $basepath ?>daftar_pengguna/get_kpknl",
                       data: "provinsi="+data.id_prov,
                       success: function(msg_data){
                          if(msg_data){
                              $("#nama_kpknl").html(msg_data);

                              setTimeout(function(){ 
                                   $("#nama_kpknl").val(data.nama_kpknl);        
                              }, 500);
                             
                            }
                       }
                    }); 

                }
            });
       }, 1000);
    } 
    function do_delete_piutang(parameter_data,param_id){
        act_delete('Hapus Penanganan Piutang','Anda ingin menghapus Penanganan Piutang  ? ','warning','pelayanan_piutang/delete','no_refresh','after_crud_piutang',window.atob(parameter_data),window.atob(param_id));
    }
    function do_delete_piutang_all(parameter_data,param_id){
        act_delete('Hapus Penanganan Piutang','Anda ingin menghapus Penanganan Piutang ? ','warning','pelayanan_piutang/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
    }
    //End Penanganan Piutang


    //Start UNAR
    function after_crud_unar(){
        var map_id = $(".data_map_id_unar").val();
        var upt    = $(".data_upt_unar").val();
        detail_unar(map_id,upt);
        $('#call_modal').modal('hide');
    }
    function detail_unar(id_map,upt){
      $(".data_map_id_unar").val(id_map);
      $(".data_upt_unar").val(upt);
      $('.unar_utama').hide();
      ajax_table_map('unar_table_data_utama',id_map,upt,'pelayanan/table');
      //$('html, body').animate({
            scrollTop: $("#unar_table_data_utama").offset().top
      //}, 1000);
    }
    function do_detail_unar(param_id,map_id,upt){
      call_modal_from_map('Detail UNAR','Form_Unar',map_id,upt,'do_detail','pelayanan/form');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>pelayanan_unar/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_unar").val(data.kode_unar);
                    $('#tgl_unar').val(data.tanggal);
                    $("#lokasi_ujian").val(data.lokasi_ujian);
                    $("#jumlah_yd").val(data.jumlah_yd);
                    $("#jumlah_yb").val(data.jumlah_yb);
                    $("#jumlah_yc").val(data.jumlah_yc);
                    $("#lulus_yd").val(data.lulus_yd);
                    $("#lulus_yb").val(data.lulus_yb);
                    $("#lulus_yc").val(data.lulus_yc);
                    $("#tdk_lulus_yd").val(data.tdk_lulus_yd);
                    $("#tdk_lulus_yb").val(data.tdk_lulus_yb);
                    $("#tdk_lulus_yc").val(data.tdk_lulus_yc);
                    $("#keterangan").val(data.keterangan);

                    //Kabupaten Kota
                     $.ajax({
                             type: "POST",
                             dataType: "html",
                             url: "<?php echo $basepath ?>daftar_pengguna/get_kabkot",
                             data: "provinsi="+data.id_prov,
                             success: function(msg){
                                if(msg){
                                  $("#kabkot").html(msg);

                                  setTimeout(function(){ 
                                       $("#kabkot").val(data.id_kabkot);        
                                  }, 500);
                                 
                                }
                             }
                          }); 
                     
                }
            });
       }, 1000);
    }
    function do_edit_unar_all(param_id){
       call_modal_default('Ubah Unar','Form_Unar_ALL','do_edit','pelayanan/form_all');
       setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>pelayanan_unar/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $("#kode_unar").val(data.kode_unar);
                    $('#tgl_unar').datepicker('update',data.tanggal);
                    $("#lokasi_ujian").val(data.lokasi_ujian);
                    $("#jumlah_yd").val(data.jumlah_yd);
                    $("#jumlah_yb").val(data.jumlah_yb);
                    $("#jumlah_yc").val(data.jumlah_yc);
                    $("#lulus_yd").val(data.lulus_yd);
                    $("#lulus_yb").val(data.lulus_yb);
                    $("#lulus_yc").val(data.lulus_yc);
                    $("#tdk_lulus_yd").val(data.tdk_lulus_yd);
                    $("#tdk_lulus_yb").val(data.tdk_lulus_yb);
                    $("#tdk_lulus_yc").val(data.tdk_lulus_yc);
                    $("#upt_provinsi").val(data.id_prov);
                    $("#keterangan").val(data.keterangan);

                    //Kabupaten Kota
                     $.ajax({
                             type: "POST",
                             dataType: "html",
                             url: "<?php echo $basepath ?>daftar_pengguna/get_kabkot",
                             data: "provinsi="+data.id_prov,
                             success: function(msg){
                                if(msg){
                                  $("#kabkot").html(msg);

                                  setTimeout(function(){ 
                                       $("#kabkot").val(data.id_kabkot);        
                                  }, 500);
                                 
                                }
                             }
                          }); 

                     //UPT
                     $.ajax({
                             type: "POST",
                             dataType: "html",
                             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
                             data: "provinsi="+data.id_prov,
                             success: function(msg){
                                if(msg){
                                  $("#kode_upt").html(msg);

                                  setTimeout(function(){ 
                                       $("#kode_upt").val(data.kode_upt);        
                                  }, 500);
                                 
                                }
                             }
                          }); 
                }
            });
       }, 1000);
    }
    function do_edit_unar(param_id,map_id,upt){
      call_modal_from_map('Ubah Unar','Form_Unar',map_id,upt,'do_edit','pelayanan/form');
      setTimeout(function(){ 
           $.ajax({
                url: '<?php echo $basepath ?>pelayanan_unar/edit/'+param_id,
                type: 'POST',
                dataType: 'JSON',
                success: function(data) {
                     $("#kode_unar").val(data.kode_unar);
                    $('#tgl_unar').datepicker('update',data.tanggal);
                    $("#lokasi_ujian").val(data.lokasi_ujian);
                    $("#jumlah_yd").val(data.jumlah_yd);
                    $("#jumlah_yb").val(data.jumlah_yb);
                    $("#jumlah_yc").val(data.jumlah_yc);
                    $("#lulus_yd").val(data.lulus_yd);
                    $("#lulus_yb").val(data.lulus_yb);
                    $("#lulus_yc").val(data.lulus_yc);
                    $("#tdk_lulus_yd").val(data.tdk_lulus_yd);
                    $("#tdk_lulus_yb").val(data.tdk_lulus_yb);
                    $("#tdk_lulus_yc").val(data.tdk_lulus_yc);
                    $("#keterangan").val(data.keterangan);

                    //Kabupaten Kota
                     $.ajax({
                             type: "POST",
                             dataType: "html",
                             url: "<?php echo $basepath ?>daftar_pengguna/get_kabkot",
                             data: "provinsi="+data.id_prov,
                             success: function(msg){
                                if(msg){
                                  $("#kabkot").html(msg);

                                  setTimeout(function(){ 
                                       $("#kabkot").val(data.id_kabkot);        
                                  }, 500);
                                 
                                }
                             }
                          }); 
                }
            });
       }, 1000);
    } 
    function do_delete_unar(parameter_data,param_id){
        act_delete('Hapus Data Unar','Anda ingin menghapus Data Unar  ? ','warning','pelayanan_unar/delete','no_refresh','after_crud_unar',window.atob(parameter_data),window.atob(param_id));
    }
    function do_delete_unar_all(parameter_data,param_id){
        act_delete('Hapus Data Unar','Anda ingin menghapus Data Unar ? ','warning','pelayanan_unar/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
    }
    //End UNAR

 
function refresh_table(){
 setTimeout(function(){ 

    //Loket Pelayanan
    $('#tb_loket').DataTable().ajax.reload();
    //$('#tb_loket_waiting').DataTable().ajax.reload();
    
    //SPP
    $('#tb_spp').DataTable().ajax.reload();
    $('#tb_spp_waiting').DataTable().ajax.reload(); 

    //ISR
    $('#tb_isr').DataTable().ajax.reload();
    $('#tb_isr_waiting').DataTable().ajax.reload();

    //REVOKE
    $('#tb_revoke').DataTable().ajax.reload();
    $('#tb_revoke_waiting').DataTable().ajax.reload(); 

    //Penanganan Piutang
    $('#tb_piutang').DataTable().ajax.reload();
    $('#tb_piutang_waiting').DataTable().ajax.reload();

    //UNAR
    $('#tb_unar').DataTable().ajax.reload();
    $('#tb_unar_waiting').DataTable().ajax.reload();

    //Loket Akumulasi
    $('#tb_loket_akumulasi').DataTable().ajax.reload();
    $('#tb_loket_akumulasi_detail').DataTable().ajax.reload();
  }, 1000);
  $("#call_modal").modal("hide");
  $(".call_modal").modal("hide");
}

 
</script>
<?php } ?>