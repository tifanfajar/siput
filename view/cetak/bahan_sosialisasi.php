<?php 
	$sWhere = " where 1=1 ";
	if($tipe=="xls"){
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=Bahan-Sosialisasi-".date('Ymdhis').".xls");
	}
	else if($tipe=="doc"){
		header("Content-Type: application/vnd.ms-word");
		header("Content-Disposition: attachment; filename=Bahan-Sosialisasi-".date('Ymdhis').".doc");
	}
	else if($tipe=="pdf"){
		error_reporting(null);
		ini_set('max_execution_time', 300); 
		require_once 'lib/mpdf/vendor/autoload.php';
		$mydata='<style>table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}</style>
		
			<h4 style="text-align:center">Bahan Sosialisasi</h4>
		<table style="width:100%">
			<tr>
				<th>Tanggal</th>
				<th>Provinsi</th>
				<th>UPT</th>
				<th>Materi</th>
				<th>Judul</th>
				<th>Author</th>
				<th>Deskripsi</th>
				<th>keterangan</th>
			</tr>';
				$rResultpdf = $md_sosialisasi_bimtek->getBahanSosialisasiFilter($sWhere,'','',$param);
				while($pdf = $rResultpdf->FetchRow()){ 

						$nama_prov = $pdf['nama_prov'];
					    $nama_upt  = $pdf['nama_upt'];

					    if(empty($nama_prov)){
					      $nama_prov = "<center>Semua Provinsi</center>";
					    }  
					    if(empty($nama_upt)){
					      $nama_upt = "<center>Semua UPT</center>";
					    }   
					$mydata .='<tr>
							<td>'.$gen_controller->get_date_indonesia($pdf['created_date']).'</td>
							<td>'.$nama_prov.'</td>
							<td>'.$nama_upt.'</td>
							<td>'.$pdf['jenis_materi'].'</td>
							<td>'.$pdf['judul'].'</td>
							<td>'.$pdf['author'].'</td>
							<td>'.$pdf['deskripsi'].'</td>
							<td>'.$pdf['keterangan'].'</td>
						</tr>';
			}
		$mydata .='</table>';

		$filename = "Bahan Sosialisasi ".date('Ymdhis');
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
		$mpdf->AddPage('A4-L');
		$mpdf->WriteHTML($mydata);
		$mpdf->Output($filename,'I');

	}

	if($tipe!="pdf"){ ?>
		<title>Bahan Sosialisasi</title>
		<style>
			table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}
			@media print {
					body {transform: scale(1.0);}
					-webkit-print-color-adjust: exact; 
					body {-webkit-print-color-adjust: exact;}
			}
			@page { size: auto;  margin: 0mm; }
		</style>
		<center>
			<h4>Bahan Sosialisasi</h4>
			<?php  if($tipe=="print"){ ?>
				<button id="printPageButton1" class="no-print" onclick="document.getElementById('printPageButton1').style.visibility='hidden';javascript:window.print();document.getElementById('printPageButton1').style.visibility='visible';" type="button">Print</button><br/>
			<?php } ?>
		</center>
		<table style="width:100%">
			<tr>
				<th>Tanggal</th>
				<th>Provinsi</th>
				<th>UPT</th>
				<th>Materi</th>
				<th>Judul</th>
				<th>Author</th>
				<th>Deskripsi</th>
				<th>keterangan</th>
			</tr>
			<?php
				$rResult = $md_sosialisasi_bimtek->getBahanSosialisasiFilter($sWhere,'','',$param);
				while($aRow = $rResult->FetchRow()){ 
						$nama_prov = $aRow['nama_prov'];
					    $nama_upt  = $aRow['nama_upt'];

					    if(empty($nama_prov)){
					      $nama_prov = "<center>Semua Provinsi</center>";
					    }  
					    if(empty($nama_upt)){
					      $nama_upt = "<center>Semua UPT</center>";
					    }   
				?>
			<tr>
				<td><?php echo $gen_controller->get_date_indonesia($aRow['created_date']) ?></td>
				<td><?php echo $nama_prov ?></td>
				<td><?php echo $nama_upt ?></td>
				<td><?php echo $aRow['jenis_materi'] ?></td>
				<td><?php echo $aRow['judul'] ?></td>
				<td><?php echo $aRow['author'] ?></td>
				<td><?php echo $aRow['deskripsi'] ?></td>
				<td><?php echo $aRow['keterangan'] ?></td>
			</tr>
		<?php } ?>
		</table>
<?php } ?>