<?php 
	$sWhere = " where 1=1 ";
	if($tipe=="xls"){
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=Penanganan-Piutang-".date('Ymdhis').".xls");
	}
	else if($tipe=="doc"){
		header("Content-Type: application/vnd.ms-word");
		header("Content-Disposition: attachment; filename=Penanganan-Piutang-".date('Ymdhis').".doc");
	}
	else if($tipe=="pdf"){
		error_reporting(null);
		ini_set('max_execution_time', 300); 
		require_once 'lib/mpdf/vendor/autoload.php';
		$mydata='<style>table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}</style>
		
			<h4 style="text-align:center">Penanganan Piutang</h4>
		<table style="width:100%">
				  <tr>
                    <th class="text-center" rowspan="2">Tanggal Buat</th>
                    <th class="text-center" rowspan="2">Provinsi</th>
                    <th class="text-center" rowspan="2">UPT</th>
                    <th class="text-center" rowspan="2">Wajib Bayar</th>
                    <th class="text-center" rowspan="2">No. Client</th>
                    <th class="text-center" rowspan="2">Nilai Penyerahan</th>
                    <th class="text-center" rowspan="2">Tahun Pelimpahan</th>
                    <th class="text-center" rowspan="2">Nama KPKNL</th>
                    <th class="text-center" rowspan="2">Tahapan Pengurusan</th>
                    <th class="text-center" colspan="3">Pemindah Bukuan</th>
                    <th class="text-center" rowspan="2">PSBDT</th>
                    <th class="text-center" rowspan="2">Pembatalan</th>
                    <th class="text-center" rowspan="2">Sisa Piutang</th>
                    <th class="text-center" rowspan="2">Ket</th>
                  </tr>
                  <tr>
                    <th class="text-center">Lunas</th>
                    <th class="text-center">Angsuran</th>
                    <th class="text-center">Tanggal</th>
                  </tr>
                  <tr>
                    <th class="text-center">1</th>
                    <th class="text-center">2</th>
                    <th class="text-center">3</th>
                    <th class="text-center">4</th>
                    <th class="text-center">5</th>
                    <th class="text-center">6</th>
                    <th class="text-center">7</th>
                    <th class="text-center">8</th>
                    <th class="text-center">9</th>
                    <th class="text-center">10</th>
                    <th class="text-center">11</th>
                    <th class="text-center">12</th>
                    <th class="text-center">13</th>
                    <th class="text-center">14</th>
                    <th class="text-center">15</th>
                    <th class="text-center">16</th>
                  </tr>';
				$rResultpdf = $md_pelayanan->getPiutangFilter($sWhere,'','',$param);
				while($pdf = $rResultpdf->FetchRow()){ 

					$nama_prov   = $pdf['id_prov'];
				      $nama_upt    = $pdf['kode_upt'];
				      $nama_kpknl  = $pdf['nama_kpknl'];
					 if($nama_prov=="Only_Admin"){
				        $nama_prov = "<center>Hanya Admin</center>";
				      }  
				      else if(empty($nama_prov)){
				        $nama_prov = "<center>Semua Provinsi</center>";
				      } 
				      else {
				        $nama_prov  = $pdf['nama_prov'];
				      } 

				      if($nama_upt=="Only_Admin"){
				        $nama_upt = "<center>Hanya Admin</center>";
				      }  
				      else if(empty($nama_upt)){
				        $nama_upt = "<center>Semua UPT</center>";
				      } 
				      else {
				        $nama_upt  = $pdf['nama_upt'];
				      }   
				      if($nama_kpknl=="Only_Admin"){
				        $nama_kpknl = "<center>Hanya Admin</center>";
				      }  
				      else if(empty($nama_kpknl)){
				        $nama_kpknl = "<center>Semua KPKNL</center>";
				      } 
				      else {
				        $nama_kpknl  = "KPKNL ".$gen_model->getOne('nama_kab_kot','ms_kabupaten_kota',array('id_kabkot'=>$pdf['nama_kpknl']));
				      }  
					$mydata .='<tr>
							<td>'.$gen_controller->get_date_indonesia($pdf['created_date']).'</td>
							<td>'.$nama_prov.'</td>
							<td>'.$nama_upt.'</td>
							<td>'.$pdf['company_name'].'</td>
							<td>'.$pdf['no_client'].'</td>
							<td align="right">'.int_to_rp($pdf['nilai_penyerahan']).'</td>
							<td align="center">'.$pdf['thn_pelimpahan'].'</td>
							<td>'.$nama_kpknl.'</td>
							<td>'.$pdf['tahapan_pengurusan'].'</td>
							<td align="right">'.int_to_rp($pdf['lunas']).'</td>
							<td align="right">'.int_to_rp($pdf['angsuran']).'</td>
							<td>'.$gen_controller->get_date_indonesia($pdf['tanggal']).'</td>
							<td align="right">'.int_to_rp($pdf['psbdt']).'</td>
							<td align="right">'.int_to_rp($pdf['pembatalan']).'</td>
							<td align="right">'.int_to_rp($pdf['sisa_piutang']).'</td>
							<td>'.$pdf['keterangan'].'</td>
						</tr>';
			}
		$mydata .='</table>';

		$filename = "Penanganan Piutang ".date('Ymdhis');
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
		$mpdf->AddPage('A4-L');
		$mpdf->WriteHTML($mydata);
		$mpdf->Output($filename,'I');

	}

	if($tipe!="pdf"){ ?>
		<title>Penanganan Piutang</title>
		<style>
			table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}
			@media print {
					body {transform: scale(1.0);}
					-webkit-print-color-adjust: exact; 
					body {-webkit-print-color-adjust: exact;}
			}
			@page { size: auto;  margin: 0mm; }
		</style>
		<center>
			<h4>Penanganan Piutang</h4>
			<?php  if($tipe=="print"){ ?>
				<button id="printPageButton1" class="no-print" onclick="document.getElementById('printPageButton1').style.visibility='hidden';javascript:window.print();document.getElementById('printPageButton1').style.visibility='visible';" type="button">Print</button><br/>
			<?php } ?>
		</center>
		<table style="width:100%">
			<tr>
                    <th class="text-center" rowspan="2">Tanggal Buat</th>
                    <th class="text-center" rowspan="2">Provinsi</th>
                    <th class="text-center" rowspan="2">UPT</th>
                    <th class="text-center" rowspan="2">Wajib Bayar</th>
                    <th class="text-center" rowspan="2">No. Client</th>
                    <th class="text-center" rowspan="2">Nilai Penyerahan</th>
                    <th class="text-center" rowspan="2">Tahun Pelimpahan</th>
                    <th class="text-center" rowspan="2">Nama KPKNL</th>
                    <th class="text-center" rowspan="2">Tahapan Pengurusan</th>
                    <th class="text-center" colspan="3">Pemindah Bukuan</th>
                    <th class="text-center" rowspan="2">PSBDT</th>
                    <th class="text-center" rowspan="2">Pembatalan</th>
                    <th class="text-center" rowspan="2">Sisa Piutang</th>
                    <th class="text-center" rowspan="2">Ket</th>
                  </tr>
                  <tr>
                    <th class="text-center">Lunas</th>
                    <th class="text-center">Angsuran</th>
                    <th class="text-center">Tanggal</th>
                  </tr>
                  <tr>
                    <th class="text-center">1</th>
                    <th class="text-center">2</th>
                    <th class="text-center">3</th>
                    <th class="text-center">4</th>
                    <th class="text-center">5</th>
                    <th class="text-center">6</th>
                    <th class="text-center">7</th>
                    <th class="text-center">8</th>
                    <th class="text-center">9</th>
                    <th class="text-center">10</th>
                    <th class="text-center">11</th>
                    <th class="text-center">12</th>
                    <th class="text-center">13</th>
                    <th class="text-center">14</th>
                    <th class="text-center">15</th>
                    <th class="text-center">16</th>
                  </tr>
			<?php
				$rResult = $md_pelayanan->getPiutangFilter($sWhere,'','',$param);
				while($aRow = $rResult->FetchRow()){ 

					 $nama_prov   = $aRow['id_prov'];
				      $nama_upt    = $aRow['kode_upt'];
				      $nama_kpknl  = $aRow['nama_kpknl'];
					 if($nama_prov=="Only_Admin"){
				        $nama_prov = "<center>Hanya Admin</center>";
				      }  
				      else if(empty($nama_prov)){
				        $nama_prov = "<center>Semua Provinsi</center>";
				      } 
				      else {
				        $nama_prov  = $aRow['nama_prov'];
				      } 

				      if($nama_upt=="Only_Admin"){
				        $nama_upt = "<center>Hanya Admin</center>";
				      }  
				      else if(empty($nama_upt)){
				        $nama_upt = "<center>Semua UPT</center>";
				      } 
				      else {
				        $nama_upt  = $aRow['nama_upt'];
				      }   
				      if($nama_kpknl=="Only_Admin"){
				        $nama_kpknl = "<center>Hanya Admin</center>";
				      }  
				      else if(empty($nama_kpknl)){
				        $nama_kpknl = "<center>Semua KPKNL</center>";
				      } 
				      else {
				        $nama_kpknl  = "KPKNL ".$gen_model->getOne('nama_kab_kot','ms_kabupaten_kota',array('id_kabkot'=>$aRow['nama_kpknl']));
				      }  
				?>
			<tr>
				<td><?php echo $gen_controller->get_date_indonesia($aRow['created_date']) ?></td>
				<td><?php echo $nama_prov ?></td>
				<td><?php echo $nama_upt ?></td>
				<td><?php echo $aRow['company_name'] ?></td>
				<td><?php echo $aRow['no_client'] ?></td>
				<td align="right"><?php echo int_to_rp($aRow['nilai_penyerahan']) ?></td>
				<td align="center"><?php echo $aRow['thn_pelimpahan'] ?></td>
				<td><?php echo $nama_kpknl ?></td>
				<td><?php echo $aRow['tahapan_pengurusan'] ?></td>
				<td align="right"><?php echo int_to_rp($aRow['lunas']) ?></td>
				<td align="right"><?php echo int_to_rp($aRow['angsuran']) ?></td>
				<td><?php echo $gen_controller->get_date_indonesia($aRow['tanggal']) ?></td>
				<td align="right"><?php echo int_to_rp($aRow['psbdt']) ?></td>
				<td align="right"><?php echo int_to_rp($aRow['pembatalan']) ?></td>
				<td align="right"><?php echo int_to_rp($aRow['sisa_piutang']) ?></td>
				<td><?php echo $aRow['keterangan'] ?></td>
			</tr>
		<?php } ?>
		</table>
<?php } ?>