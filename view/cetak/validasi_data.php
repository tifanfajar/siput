<?php 
	$sWhere = " where 1=1 ";
	if($tipe=="xls"){
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=Validasi-Data-".date('Ymdhis').".xls");
	}
	else if($tipe=="doc"){
		header("Content-Type: application/vnd.ms-word");
		header("Content-Disposition: attachment; filename=Validasi-Data-".date('Ymdhis').".doc");
	}
	else if($tipe=="pdf"){
		error_reporting(null);
		ini_set('max_execution_time', 300); 
		require_once 'lib/mpdf/vendor/autoload.php';
		$mydata='<style>table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}</style>
		
			<h4 style="text-align:center">Validasi Data</h4>
		<table style="width:100%">
			<tr>
                    <th rowspan="2">Tanggal Buat</th>
                    <th rowspan="2">Provinsi</th>
                    <th rowspan="2">UPT</th>
                    <th rowspan="2">Data SIMS</th>
                    <th rowspan="2">Data Sampling</th>
                    <th colspan="3">Data Hasil Inspeksi</th>
                    <th colspan="3">Tindak Lanjut Hasil Inspeksi</th>
                    <th rowspan="2">Capaian (% Valid)</th>
                    <th rowspan="2">Keterangan</th>
                  </tr>
                  <tr>
                    <th>Sesuai ISR</th>
                    <th>Tidak Sesuai</th>
                    <th>Tidak Aktif</th> 
                    <th>Proses ISR</th>
                    <th>Sudah di tindaklanjuti</th>
                    <th>Belum di tindaklanjuti</th>
                  </tr> 
                  <tr>
                    <th>1</th>
                    <th>2</th>
                    <th>3</th> 
                    <th>4</th>
                    <th>5</th>
                    <th>6</th>
                    <th>7</th>
                    <th>8</th>
                    <th>9</th>
                    <th>10</th>
                    <th>11</th>
                    <th>12</th>
                    <th>13</th>
                  </tr>';
				$rResultpdf = $md_validasi->getValidasiDataFilter($sWhere,'','',$param);
				while($pdf = $rResultpdf->FetchRow()){ 

					$nama_prov = $pdf['id_prov'];
				    $nama_upt  = $pdf['kode_upt'];

				    if($nama_prov=="Only_Admin"){
				      $nama_prov = "<center>Hanya Admin</center>";
				    }  
				    else if(empty($nama_prov)){
				      $nama_prov = "<center>Semua Provinsi</center>";
				    } 
				    else {
				      $nama_prov  = $pdf['nama_prov'];
				    } 

				    if($nama_upt=="Only_Admin"){
				      $nama_upt = "<center>Hanya Admin</center>";
				    }  
				    else if(empty($nama_upt)){
				      $nama_upt = "<center>Semua UPT</center>";
				    } 
				    else {
				      $nama_upt  = $pdf['nama_upt'];
				    }   		

					$mydata .='<tr>
							<td>'.$gen_controller->get_date_indonesia($pdf['created_date']).'</td>
							<td>'.$nama_prov.'</td>
							<td>'.$nama_upt.'</td>
							<td>'.$pdf['data_sims'].'</td>
							<td>'.$pdf['data_sampling'].'</td>
							<td>'.$pdf['sesuai_isr'].'</td>
							<td>'.$pdf['tdk_sesuai_isr'].'</td>
							<td>'.$pdf['tidak_aktif'].'</td>
							<td>'.$pdf['proses_isr'].'</td>
							<td>'.$pdf['sudah_ditindaklanjuti'].'</td>
							<td>'.$pdf['belum_ditindaklanjuti'].'</td>
							<td>'.$pdf['capaian'].'</td>
							<td>'.$pdf['keterangan'].'</td>
						</tr>';
			}
		$mydata .='</table>';

		$filename = "Validasi Data ".date('Ymdhis');
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
		$mpdf->AddPage('A4-L');
		$mpdf->WriteHTML($mydata);
		$mpdf->Output($filename,'I');

	}

	if($tipe!="pdf"){ ?>
		<title>Validasi Data</title>
		<style>
			table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}
			@media print {
					body {transform: scale(1.0);}
					-webkit-print-color-adjust: exact; 
					body {-webkit-print-color-adjust: exact;}
			}
			@page { size: auto;  margin: 0mm; }
		</style>
		<center>
			<h4>Validasi Data</h4>
			<?php  if($tipe=="print"){ ?>
				<button id="printPageButton1" class="no-print" onclick="document.getElementById('printPageButton1').style.visibility='hidden';javascript:window.print();document.getElementById('printPageButton1').style.visibility='visible';" type="button">Print</button><br/>
			<?php } ?>
		</center>
		<table style="width:100%">
				  <tr>
                    <th class="text-center" rowspan="2">Tanggal Buat</th>
                    <th class="text-center" rowspan="2">Provinsi</th>
                    <th class="text-center" rowspan="2">UPT</th>
                    <th class="text-center" rowspan="2">Data SIMS</th>
                    <th class="text-center" rowspan="2">Data Sampling</th>
                    <th class="text-center" colspan="3">Data Hasil Inspeksi</th>
                    <th class="text-center" colspan="3">Tindak Lanjut Hasil Inspeksi</th>
                    <th class="text-center" rowspan="2">Capaian (% Valid)</th>
                    <th class="text-center" rowspan="2">Keterangan</th>
                  </tr>
                  <tr>
                    <th  class="text-center">Sesuai ISR</th>
                    <th  class="text-center">Tidak Sesuai</th>
                    <th  class="text-center">Tidak Aktif</th> 
                    <th  class="text-center">Proses ISR</th>
                    <th  class="text-center">Sudah di tindaklanjuti</th>
                    <th  class="text-center">Belum di tindaklanjuti</th>
                  </tr> 
                  <tr>
                    <th  class="text-center">1</th>
                    <th  class="text-center">2</th>
                    <th  class="text-center">3</th> 
                    <th  class="text-center">4</th>
                    <th  class="text-center">5</th>
                    <th  class="text-center">6</th>
                    <th  class="text-center">7</th>
                    <th  class="text-center">8</th>
                    <th  class="text-center">9</th>
                    <th  class="text-center">10</th>
                    <th  class="text-center">11</th>
                    <th  class="text-center">12</th>
                    <th  class="text-center">13</th>
                  </tr>
			<?php
				$rResult = $md_validasi->getValidasiDataFilter($sWhere,'','',$param);
				while($aRow = $rResult->FetchRow()){ 

					$nama_prov = $aRow['id_prov'];
				    $nama_upt  = $aRow['kode_upt'];

				    if($nama_prov=="Only_Admin"){
				      $nama_prov = "<center>Hanya Admin</center>";
				    }  
				    else if(empty($nama_prov)){
				      $nama_prov = "<center>Semua Provinsi</center>";
				    } 
				    else {
				      $nama_prov  = $aRow['nama_prov'];
				    } 

				    if($nama_upt=="Only_Admin"){
				      $nama_upt = "<center>Hanya Admin</center>";
				    }  
				    else if(empty($nama_upt)){
				      $nama_upt = "<center>Semua UPT</center>";
				    } 
				    else {
				      $nama_upt  = $aRow['nama_upt'];
				    }   
					?>
			<tr>
				<td><?php echo $gen_controller->get_date_indonesia($aRow['created_date']) ?></td>
				<td><?php echo $nama_prov ?></td>
				<td><?php echo $nama_upt ?></td>
				<td><?php echo $aRow['data_sims'] ?></td>
				<td><?php echo $aRow['data_sampling'] ?></td>
				<td><?php echo $aRow['sesuai_isr'] ?></td>
				<td><?php echo $aRow['tdk_sesuai_isr'] ?></td>
				<td><?php echo $aRow['tidak_aktif'] ?></td>
				<td><?php echo $aRow['proses_isr'] ?></td>
				<td><?php echo $aRow['sudah_ditindaklanjuti'] ?></td>
				<td><?php echo $aRow['belum_ditindaklanjuti'] ?></td>
				<td><?php echo $aRow['capaian'] ?></td>
				<td><?php echo $aRow['keterangan'] ?></td>
			</tr>
		<?php } ?>
		</table>
<?php } ?>