<?php 
	$sWhere = " where 1=1 ";
	if($tipe=="xls"){
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=DaftarPengguna-".date('Ymdhis').".xls");
	}
	else if($tipe=="doc"){
		header("Content-Type: application/vnd.ms-word");
		header("Content-Disposition: attachment; filename=DaftarPengguna-".date('Ymdhis').".doc");
	}
	else if($tipe=="pdf"){
		error_reporting(null);
		ini_set('max_execution_time', 300); 
		require_once 'lib/mpdf/vendor/autoload.php';
		$mydata='<style>table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}</style>
		
			<h4 style="text-align:center">Daftar Pengguna</h4>
		<table style="width:100%">
			<tr>
				<th>Username</th>
				<th>Nama</th>
				<th>Level</th>
				<th>Status</th>
				<th>Provinsi</th>
				<th>UPT</th>
				<th>No.Tlp</th>
				<th>No.Hp</th>
				<th>Email</th>
				<th>Tanggal Buat</th>
				<th>Tanggal Ubah</th>
			</tr>';
			$sts = "";
			    if($pdf['status']=="0"){
			      $sts = "Non Aktif";
			    }
			    else if($pdf['status']=="1"){
			      $sts = "Aktif";
			    }
			    else if($pdf['status']=="2"){
			      $sts = "Blokir";
			    }
				$rResultpdf = $md_user->getDataUserFilter($sWhere,'','',$param);
				while($pdf = $rResultpdf->FetchRow()){ 
						$mydata .='<tr>
							<td>'.$pdf['username'].'</td>
							<td>'.$pdf['nama_lengkap'].'</td>
							<td>'.$pdf['group_name'].'</td>
							<td>'.$sts.'</td>
							<td>'.$pdf['nama_prov'].'</td>
							<td>'.$pdf['nama_upt'].'</td>
							<td>'.$pdf['no_tlp'].'</td>
							<td>'.$pdf['no_hp'].'</td>
							<td>'.$pdf['email'].'</td>
							<td>'.$gen_controller->get_date_indonesia($pdf['created_date'])." ".substr($pdf['created_date'],11,8).'</td>
							<td>'.$gen_controller->get_date_indonesia($pdf['last_update'])." ".substr($pdf['last_update'],11,8).'</td>
						</tr>';
			}
		$mydata .='</table>';

		$filename = "Daftar Pengguna ".date('Ymdhis');
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
		$mpdf->AddPage('A4-L');
		$mpdf->WriteHTML($mydata);
		$mpdf->Output($filename,'I');

	}

	if($tipe!="pdf"){ ?>
		<title>SPP</title>
		<style>
			table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}
			@media print {
					body {transform: scale(1.0);}
					-webkit-print-color-adjust: exact; 
					body {-webkit-print-color-adjust: exact;}
			}
			@page { size: auto;  margin: 0mm; }
		</style>
		<center>
			<h4>Daftar Pengguna</h4>
			<?php  if($tipe=="print"){ ?>
				<button id="printPageButton1" class="no-print" onclick="document.getElementById('printPageButton1').style.visibility='hidden';javascript:window.print();document.getElementById('printPageButton1').style.visibility='visible';" type="button">Print</button><br/>
			<?php } ?>
		</center>
		<table style="width:100%">
			<tr>
				<th>Username</th>
				<th>Nama</th>
				<th>Level</th>
				<th>Status</th>
				<th>Provinsi</th>
				<th>UPT</th>
				<th>No.Tlp</th>
				<th>No.Hp</th>
				<th>Email</th>
				<th>Tanggal Buat</th>
				<th>Tanggal Ubah</th>
			</tr>
			<?php
				$rResult = $md_user->getDataUserFilter($sWhere,'','',$param);
				while($aRow = $rResult->FetchRow()){ 

						 if($aRow['status']=="0"){
						      $sts = "Non Aktif";
						    }
						    else if($aRow['status']=="1"){
						      $sts = "Aktif";
						    }
						    else if($aRow['status']=="2"){
						      $sts = "Blokir";
						    }
					?>
			<tr>
				<td><?php echo $aRow['username'] ?></td>
				<td><?php echo $aRow['nama_lengkap'] ?></td>
				<td><?php echo $aRow['group_name'] ?></td>
				<td><?php echo $sts ?></td>
				<td><?php echo $aRow['nama_prov'] ?></td>
				<td><?php echo $aRow['nama_upt'] ?></td>
				<td><?php echo $aRow['no_tlp'] ?></td>
				<td><?php echo $aRow['no_hp'] ?></td>
				<td><?php echo $aRow['email'] ?></td>
				<td><?php echo $gen_controller->get_date_indonesia($aRow['created_date'])." ".substr($aRow['created_date'],11,8) ?></td>
				<td><?php echo $gen_controller->get_date_indonesia($aRow['last_update'])." ".substr($aRow['last_update'],11,8) ?></td>
			</tr>
		<?php } ?>
		</table>
<?php } ?>