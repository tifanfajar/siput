<?php 
	$sWhere = " where 1=1 ";
	if($tipe=="xls"){
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=Loket-Pelayanan-".date('Ymdhis').".xls");
	}
	else if($tipe=="doc"){
		header("Content-Type: application/vnd.ms-word");
		header("Content-Disposition: attachment; filename=Loket-Pelayanan-".date('Ymdhis').".doc");
	}
	else if($tipe=="pdf"){
		error_reporting(null);
		ini_set('max_execution_time', 300); 
		require_once 'lib/mpdf/vendor/autoload.php';
		$mydata='<style>table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}</style>
		
			<h4 style="text-align:center">Profil & Data Pengunjung</h4>
		<table style="width:100%">
			<tr>
				<th>Tanggal</th>
				<th>Provinsi</th>
				<th>UPT</th>
				<th>Nama</th>
				<th>Jabatan</th>
				<th>Perusahaan</th>
				<th>No.Tlp</th>
				<th>No.Hp</th>
				<th>Email</th>
				<th>Tujuan</th>
				<th>Perizinan</th>
			</tr>';
				$rResultpdf = $md_pelayanan->getLoketPelayananFilter($sWhere,'','',$param);
				while($pdf = $rResultpdf->FetchRow()){ 
					$mydata .='<tr>
							<td>'.$gen_controller->get_date_indonesia($pdf['tgl_pelayanan']).'</td>
							<td>'.$pdf['nama_prov'].'</td>
							<td>'.$pdf['nama_upt'].'</td>
							<td>'.$pdf['nama_pengunjung'].'</td>
							<td>'.$pdf['jabatan_pengunjung'].'</td>
							<td>'.$pdf['company_name'].'</td>
							<td>'.$pdf['no_tlp'].'</td>
							<td>'.$pdf['no_hp'].'</td>
							<td>'.$pdf['email'].'</td>
							<td>'.$pdf['nama_tj'].'</td>
							<td>'.$pdf['jenis'].'</td>
						</tr>';
			}
		$mydata .='</table>';

		$filename = "Loket Pelayanan ".date('Ymdhis');
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
		$mpdf->AddPage('A4-L');
		$mpdf->WriteHTML($mydata);
		$mpdf->Output($filename,'I');

	}

	if($tipe!="pdf"){ ?>
		<title>Loket Pelayanan</title>
		<style>
			table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}
			@media print {
					body {transform: scale(1.0);}
					-webkit-print-color-adjust: exact; 
					body {-webkit-print-color-adjust: exact;}
			}
			@page { size: auto;  margin: 0mm; }
		</style>
		<center>
			<h4>Profil & Data Pengunjung</h4>
			<?php  if($tipe=="print"){ ?>
				<button id="printPageButton1" class="no-print" onclick="document.getElementById('printPageButton1').style.visibility='hidden';javascript:window.print();document.getElementById('printPageButton1').style.visibility='visible';" type="button">Print</button><br/>
			<?php } ?>
		</center>
		<table style="width:100%">
			<tr>
				<th>Tanggal</th>
				<th>Provinsi</th>
				<th>UPT</th>
				<th>Nama</th>
				<th>Jabatan</th>
				<th>Perusahaan</th>
				<th>No.Tlp</th>
				<th>No.Hp</th>
				<th>Email</th>
				<th>Tujuan</th>
				<th>Perizinan</th>
			</tr>
			<?php
				$rResult = $md_pelayanan->getLoketPelayananFilter($sWhere,'','',$param);
				while($aRow = $rResult->FetchRow()){ ?>
			<tr>
				<td><?php echo $gen_controller->get_date_indonesia($aRow['tgl_pelayanan']) ?></td>
				<td><?php echo $aRow['nama_prov'] ?></td>
				<td><?php echo $aRow['nama_upt'] ?></td>
				<td><?php echo $aRow['nama_pengunjung'] ?></td>
				<td><?php echo $aRow['jabatan_pengunjung'] ?></td>
				<td><?php echo $aRow['company_name'] ?></td>
				<td><?php echo $aRow['no_tlp'] ?></td>
				<td><?php echo $aRow['no_hp'] ?></td>
				<td><?php echo $aRow['email'] ?></td>
				<td><?php echo $aRow['nama_tj'] ?></td>
				<td><?php echo $aRow['jenis'] ?></td>
			</tr>
		<?php } ?>
		</table>
<?php } ?>