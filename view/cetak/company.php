<?php 
	$sWhere = " where 1=1 ";
	if($tipe=="xls"){
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=Company-".date('Ymdhis').".xls");
	}
	else if($tipe=="doc"){
		header("Content-Type: application/vnd.ms-word");
		header("Content-Disposition: attachment; filename=Company-".date('Ymdhis').".doc");
	}
	else if($tipe=="pdf"){
		error_reporting(null);
		ini_set('max_execution_time', 300); 
		require_once 'lib/mpdf/vendor/autoload.php';
		$mydata='<style>table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}</style>
		
			<h4 style="text-align:center">Company</h4>
		<table style="width:100%">
			<tr>
				<th>No Klien Licence</th>
                <th>Nama Perusahaan</th>
                <th>No Telp</th>
                <th>No Hp</th>
                <th>Provinsi</th>
                <th>Kota/Kabupaten</th>
                <th>Alamat</th>
			</tr>';
				$rResultpdf = $md_company->getCompanyFilter($sWhere,'','',$param);
				while($pdf = $rResultpdf->FetchRow()){ 
					$mydata .='<tr>
							<td>'.$pdf['company_id'].'</td>
							<td>'.$pdf['company_name'].'</td>
							<td>'.$pdf['no_tlp_company'].'</td>
							<td>'.$pdf['no_hp_company'].'</td>
							<td>'.$pdf['nama_prov'].'</td>
							<td>'.$pdf['nama_kab_kot'].'</td>
							<td>'.$pdf['alamat'].'</td>
						</tr>';
			}
		$mydata .='</table>';

		$filename = "Company ".date('Ymdhis');
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
		$mpdf->AddPage('A4-L');
		$mpdf->WriteHTML($mydata);
		$mpdf->Output($filename,'I');

	}

	if($tipe!="pdf"){ ?>
		<title>Company</title>
		<style>
			table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}
			@media print {
					body {transform: scale(1.0);}
					-webkit-print-color-adjust: exact; 
					body {-webkit-print-color-adjust: exact;}
			}
			@page { size: auto;  margin: 0mm; }
		</style>
		<center>
			<h4>Company</h4>
			<?php  if($tipe=="print"){ ?>
				<button id="printPageButton1" class="no-print" onclick="document.getElementById('printPageButton1').style.visibility='hidden';javascript:window.print();document.getElementById('printPageButton1').style.visibility='visible';" type="button">Print</button><br/>
			<?php } ?>
		</center>
		<table style="width:100%">
			<tr>
				<th>No Klien Licence</th>
                <th>Nama Perusahaan</th>
                <th>No Telp</th>
                <th>No Hp</th>
                <th>Provinsi</th>
                <th>Kota/Kabupaten</th>
                <th>Alamat</th>
			</tr>
			<?php
				$rResult = $md_company->getCompanyFilter($sWhere,'','',$param);
				while($aRow = $rResult->FetchRow()){ ?>
			<tr>
				<td><?php echo $aRow['company_id'] ?></td>
				<td><?php echo $aRow['company_name'] ?></td>
				<td><?php echo $aRow['no_tlp_company'] ?></td>
				<td><?php echo $aRow['no_hp_company'] ?></td>
				<td><?php echo $aRow['nama_prov'] ?></td>
				<td><?php echo $aRow['nama_kab_kot'] ?></td>
				<td><?php echo $aRow['alamat'] ?></td>
			</tr>
		<?php } ?>
		</table>
<?php } ?>