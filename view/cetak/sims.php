<?php 
	$sWhere = " where 1=1 ";
	if($tipe=="xls"){
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=Data-Sims-".date('Ymdhis').".xls");
	}
	else if($tipe=="doc"){
		header("Content-Type: application/vnd.ms-word");
		header("Content-Disposition: attachment; filename=Data-Sims-".date('Ymdhis').".doc");
	}
	else if($tipe=="pdf"){
		error_reporting(null);
		ini_set('max_execution_time', 300); 
		require_once 'lib/mpdf/vendor/autoload.php';
		$mydata='<style>table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}</style>
		
			<h4 style="text-align:center">Data SIMS</h4>
		<table style="width:100%">
			<tr>
				<th>No SPP</th>
	            <th>No Klien Licence</th>
	            <th>Perusahaan</th>
	            <th>Request Reff</th>
	            <th>Licence State</th>
	            <th>Tagihan</th>
	            <th>Status</th>
	            <th>Tanggal Begin</th>
	            <th>Status Izin</th>
	            <th>Kategori SPP</th>
	            <th>Service</th>
	            <th>Sub Service</th>
	            <th>Created Date</th>
	            <th>Last Update</th>
			</tr>';
				$rResultpdf = $md_sims->getSimsFilter($sWhere,'','',$param);
				while($pdf = $rResultpdf->FetchRow()){ 
					$mydata .='<tr>
							<td>'.$pdf['no_spp'].'</td>
							<td>'.$pdf['no_klien_licence'].'</td>
							<td>'.$pdf['company_name'].'</td>
							<td>'.$pdf['request_reff'].'</td>
							<td>'.$pdf['licence_state'].'</td>
							<td>'.int_to_rp($pdf['tagihan']).'</td>
							<td>'.$pdf['status'].'</td>
							<td>'.$gen_controller->get_date_indonesia($pdf['tgl_begin']).'</td>
							<td>'.$pdf['status_izin'].'</td>
							<td>'.$pdf['kategori_spp'].'</td>
							<td>'.$pdf['service'].'</td>
							<td>'.$pdf['subservice'].'</td>
							<td>'.$gen_controller->get_date_indonesia($pdf['created_date']).'</td>
							<td>'.$gen_controller->get_date_indonesia($pdf['last_update']).'</td>
						</tr>';
			}
		$mydata .='</table>';

		$filename = "Data SIMS ".date('Ymdhis');
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
		$mpdf->AddPage('A4-L');
		$mpdf->WriteHTML($mydata);
		$mpdf->Output($filename,'I');

	}

	if($tipe!="pdf"){ ?>
		<title>Data SIMS</title>
		<style>
			table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}
			@media print {
					body {transform: scale(1.0);}
					-webkit-print-color-adjust: exact; 
					body {-webkit-print-color-adjust: exact;}
			}
			@page { size: auto;  margin: 0mm; }
		</style>
		<center>
			<h4>Data SIMS</h4>
			<?php  if($tipe=="print"){ ?>
				<button id="printPageButton1" class="no-print" onclick="document.getElementById('printPageButton1').style.visibility='hidden';javascript:window.print();document.getElementById('printPageButton1').style.visibility='visible';" type="button">Print</button><br/>
			<?php } ?>
		</center>
		<table style="width:100%">
			<tr>
				<th>No SPP</th>
	            <th>No Klien Licence</th>
	            <th>Perusahaan</th>
	            <th>Request Reff</th>
	            <th>Licence State</th>
	            <th>Tagihan</th>
	            <th>Status</th>
	            <th>Tanggal Begin</th>
	            <th>Status Izin</th>
	            <th>Kategori SPP</th>
	            <th>Service</th>
	            <th>Sub Service</th>
	            <th>Created Date</th>
	            <th>Last Update</th>
			</tr>
			<?php
				$rResult = $md_sims->getSimsFilter($sWhere,'','',$param);
				while($aRow = $rResult->FetchRow()){ ?>
			<tr>
				<td><?php echo $aRow['no_spp'] ?></td>
				<td><?php echo $aRow['no_klien_licence'] ?></td>
				<td><?php echo $aRow['company_name'] ?></td>
				<td><?php echo $aRow['request_reff'] ?></td>
				<td><?php echo $aRow['licence_state'] ?></td>
				<td><?php echo int_to_rp($aRow['tagihan']) ?></td>
				<td><?php echo $aRow['status'] ?></td>
				<td><?php echo$gen_controller->get_date_indonesia($aRow['tgl_begin']) ?></td>
				<td><?php echo $aRow['status_izin'] ?></td>
				<td><?php echo $aRow['kategori_spp'] ?></td>
				<td><?php echo $aRow['service'] ?></td>
				<td><?php echo $aRow['subservice'] ?></td>
				<td><?php echo $gen_controller->get_date_indonesia($aRow['created_date']) ?></td>
				<td><?php echo $gen_controller->get_date_indonesia($aRow['last_update']) ?></td>
			</tr>
		<?php } ?>
		</table>
<?php } ?>