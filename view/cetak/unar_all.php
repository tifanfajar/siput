<?php 
	$sWhere = " where 1=1 ";
	if($tipe=="xls"){
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=Unar-".date('Ymdhis').".xls");
	}
	else if($tipe=="doc"){
		header("Content-Type: application/vnd.ms-word");
		header("Content-Disposition: attachment; filename=Unar-".date('Ymdhis').".doc");
	}
	else if($tipe=="pdf"){
		error_reporting(null);
		ini_set('max_execution_time', 300); 
		require_once 'lib/mpdf/vendor/autoload.php';
		$mydata='<style>table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}</style>
		
			<h4 style="text-align:center">Unar</h4>
		<table style="width:100%">
				  <tr>
                    <th class="text-center" rowspan="2">Tanggal Buat</th>
                    <th class="text-center" rowspan="2">Provinsi</th>
                    <th class="text-center" rowspan="2">UPT</th>
                    <th class="text-center" rowspan="2">Tanggal Pelaksanaan Ujian</th>
                    <th class="text-center" rowspan="2">Lokasi Ujian</th>
                    <th class="text-center" rowspan="2">Kab/Kota</th>
                    <th class="text-center" colspan="3">Jumlah Peserta</th>
                    <th class="text-center" colspan="3">Lulus</th>
                    <th class="text-center" colspan="3">Tidak Lulus</th>
                    <th class="text-center" rowspan="2">Ket</th>
                  </tr>
                  <tr>
                    <th  class="text-center">YD</th>
                    <th  class="text-center">YC</th>
                    <th  class="text-center">YB</th>

                    <th  class="text-center">YD</th>
                    <th  class="text-center">YC</th>
                    <th  class="text-center">YB</th>

                    <th  class="text-center">YD</th>
                    <th  class="text-center">YC</th>
                    <th  class="text-center">YB</th>
                  </tr>';
				$rResultpdf = $md_pelayanan->getUnarFilter($sWhere,'','',$param);
				while($pdf = $rResultpdf->FetchRow()){ 

					$nama_prov = $pdf['id_prov'];
				    $nama_upt  = $pdf['kode_upt'];
				    $nama_kab_kot  = $pdf['id_kabkot'];

				    if($nama_prov=="Only_Admin"){
				      $nama_prov = "<center>Hanya Admin</center>";
				    }  
				    else if(empty($nama_prov)){
				      $nama_prov = "<center>Semua Provinsi</center>";
				    } 
				    else {
				      $nama_prov  = $pdf['nama_prov'];
				    } 

				    if($nama_upt=="Only_Admin"){
				      $nama_upt = "<center>Hanya Admin</center>";
				    }  
				    else if(empty($nama_upt)){
				      $nama_upt = "<center>Semua UPT</center>";
				    } 
				    else {
				      $nama_upt  = $pdf['nama_upt'];
				    }   

				    if($nama_kab_kot=="Only_Admin"){
				      $nama_kab_kot = "<center>Hanya Admin</center>";
				    }  
				    else if(empty($nama_kab_kot)){
				      $nama_kab_kot = "<center>Semua Kab/Kota</center>";
				    } 
				    else {
				      $nama_kab_kot  = $pdf['nama_kab_kot'];
				    }  
					
					$mydata .='<tr>
							<td>'.$gen_controller->get_date_indonesia($pdf['created_date']).'</td>
							<td>'.$nama_prov.'</td>
							<td>'.$nama_upt.'</td>
							<td>'.$gen_controller->get_date_indonesia($pdf['tgl_ujian']).'</td>
							<td>'.$pdf['lokasi_ujian'].'</td>
							<td>'.$nama_kab_kot.'</td>
							<td>'.$pdf['jumlah_yd'].'</td>
							<td>'.$pdf['jumlah_yc'].'</td>
							<td>'.$pdf['jumlah_yb'].'</td>
							<td>'.$pdf['lulus_yd'].'</td>
							<td>'.$pdf['lulus_yc'].'</td>
							<td>'.$pdf['lulus_yb'].'</td>
							<td>'.$pdf['tdk_lulus_yd'].'</td>
							<td>'.$pdf['tdk_lulus_yc'].'</td>
							<td>'.$pdf['tdk_lulus_yb'].'</td>
							<td>'.$pdf['keterangan'].'</td>
						</tr>';
			}
		$mydata .='</table>';

		$filename = "Unar ".date('Ymdhis');
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
		$mpdf->AddPage('A4-L');
		$mpdf->WriteHTML($mydata);
		$mpdf->Output($filename,'I');

	}

	if($tipe!="pdf"){ ?>
		<title>Unar</title>
		<style>
			table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}
			@media print {
					body {transform: scale(1.0);}
					-webkit-print-color-adjust: exact; 
					body {-webkit-print-color-adjust: exact;}
			}
			@page { size: auto;  margin: 0mm; }
		</style>
		<center>
			<h4>Unar</h4>
			<?php  if($tipe=="print"){ ?>
				<button id="printPageButton1" class="no-print" onclick="document.getElementById('printPageButton1').style.visibility='hidden';javascript:window.print();document.getElementById('printPageButton1').style.visibility='visible';" type="button">Print</button><br/>
			<?php } ?>
		</center>
		<table style="width:100%">
				<tr>
                    <th class="text-center" rowspan="2">Tanggal Buat</th>
                    <th class="text-center" rowspan="2">Provinsi</th>
                    <th class="text-center" rowspan="2">UPT</th>
                    <th class="text-center" rowspan="2">Tanggal Pelaksanaan Ujian</th>
                    <th class="text-center" rowspan="2">Lokasi Ujian</th>
                    <th class="text-center" rowspan="2">Kab/Kota</th>
                    <th class="text-center" colspan="3">Jumlah Peserta</th>
                    <th class="text-center" colspan="3">Lulus</th>
                    <th class="text-center" colspan="3">Tidak Lulus</th>
                    <th class="text-center" rowspan="2">Ket</th>
                  </tr>
                  <tr>
                    <th  class="text-center">YD</th>
                    <th  class="text-center">YC</th>
                    <th  class="text-center">YB</th>

                    <th  class="text-center">YD</th>
                    <th  class="text-center">YC</th>
                    <th  class="text-center">YB</th>

                    <th  class="text-center">YD</th>
                    <th  class="text-center">YC</th>
                    <th  class="text-center">YB</th>
                  </tr>
			<?php
				$rResult = $md_pelayanan->getUnarFilter($sWhere,'','',$param);
				while($aRow = $rResult->FetchRow()){ 

					$nama_prov = $aRow['id_prov'];
				    $nama_upt  = $aRow['kode_upt'];
				    $nama_kab_kot  = $aRow['id_kabkot'];

				    if($nama_prov=="Only_Admin"){
				      $nama_prov = "<center>Hanya Admin</center>";
				    }  
				    else if(empty($nama_prov)){
				      $nama_prov = "<center>Semua Provinsi</center>";
				    } 
				    else {
				      $nama_prov  = $aRow['nama_prov'];
				    } 

				    if($nama_upt=="Only_Admin"){
				      $nama_upt = "<center>Hanya Admin</center>";
				    }  
				    else if(empty($nama_upt)){
				      $nama_upt = "<center>Semua UPT</center>";
				    } 
				    else {
				      $nama_upt  = $aRow['nama_upt'];
				    }   

				    if($nama_kab_kot=="Only_Admin"){
				      $nama_kab_kot = "<center>Hanya Admin</center>";
				    }  
				    else if(empty($nama_kab_kot)){
				      $nama_kab_kot = "<center>Semua Kab/Kota</center>";
				    } 
				    else {
				      $nama_kab_kot  = $aRow['nama_kab_kot'];
				    }  
				?>
			<tr>
				<td><?php echo $gen_controller->get_date_indonesia($aRow['created_date']) ?></td>
				<td><?php echo $nama_prov ?></td>
				<td><?php echo $nama_upt ?></td>
				<td><?php echo $gen_controller->get_date_indonesia($aRow['tgl_ujian']) ?></td>
				<td><?php echo $aRow['lokasi_ujian'] ?></td>
				<td><?php echo $nama_kab_kot ?></td>
				<td><?php echo $aRow['jumlah_yd'] ?></td>
				<td><?php echo $aRow['jumlah_yc'] ?></td>
				<td><?php echo $aRow['jumlah_yb'] ?></td>
				<td><?php echo $aRow['lulus_yd'] ?></td>
				<td><?php echo $aRow['lulus_yc'] ?></td>
				<td><?php echo $aRow['lulus_yb'] ?></td>
				<td><?php echo $aRow['tdk_lulus_yd'] ?></td>
				<td><?php echo $aRow['tdk_lulus_yc'] ?></td>
				<td><?php echo $aRow['tdk_lulus_yb'] ?></td>
				<td><?php echo $aRow['keterangan'] ?></td>
			</tr>
		<?php } ?>
		</table>
<?php } ?>