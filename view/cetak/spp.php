<?php 
	$sWhere = " where 1=1 ";
	if($tipe=="xls"){
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=SPP-".date('Ymdhis').".xls");
	}
	else if($tipe=="doc"){
		header("Content-Type: application/vnd.ms-word");
		header("Content-Disposition: attachment; filename=SPP-".date('Ymdhis').".doc");
	}
	else if($tipe=="pdf"){
		error_reporting(null);
		ini_set('max_execution_time', 300); 
		require_once 'lib/mpdf/vendor/autoload.php';
		$mydata='<style>table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}</style>
		
			<h4 style="text-align:center">SPP</h4>
		<table style="width:100%">
			<tr>
                <th>No. Client</th>
                <th>Nama Pemegang ISR / Waba</th>
                <th>Service</th>
                <th>No. Aplikasi</th>
                <th>No. SPP</th>
                <th>Jenis SPP</th>
                <th>Nilai BHP (Rp)</th>
                <th>Tanggal Jatuh Tempo</th>
                <th>Tanggal Diterima Waba</th>
                <th>Metode</th>
                <th>Keterangan</th>
			</tr>';
				$rResultpdf = $md_pelayanan->getSPPFilter($sWhere,'','',$param);
				while($pdf = $rResultpdf->FetchRow()){ 
				

						$mydata .='<tr>
							<td>'.$pdf['no_klien_licence'].'</td>
							<td>'.$pdf['company_name'].'</td>
							<td>'.$pdf['service'].'</td>
							<td>'.$pdf['no_aplikasi'].'</td>
							<td>'.$pdf['no_spp'].'</td>
							<td>'.$pdf['kategori_spp'].'</td>
							<td>'.int_to_rp($pdf['tagihan']).'</td>
							<td>'.$gen_controller->get_date_indonesia($pdf['tgl_jatuh_tempo']).'</td>
							<td>'.$gen_controller->get_date_indonesia($pdf['tgl_terima_waba']).'</td>
							<td>'.$pdf['nama_metode'].'</td>
							<td>'.$pdf['keterangan'].'</td>
						</tr>';
			}
		$mydata .='</table>';

		$filename = "SPP ".date('Ymdhis');
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
		$mpdf->AddPage('A4-L');
		$mpdf->WriteHTML($mydata);
		$mpdf->Output($filename,'I');

	}

	if($tipe!="pdf"){ ?>
		<title>SPP</title>
		<style>
			table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}
			@media print {
					body {transform: scale(1.0);}
					-webkit-print-color-adjust: exact; 
					body {-webkit-print-color-adjust: exact;}
			}
			@page { size: auto;  margin: 0mm; }
		</style>
		<center>
			<h4>SPP</h4>
			<?php  if($tipe=="print"){ ?>
				<button id="printPageButton1" class="no-print" onclick="document.getElementById('printPageButton1').style.visibility='hidden';javascript:window.print();document.getElementById('printPageButton1').style.visibility='visible';" type="button">Print</button><br/>
			<?php } ?>
		</center>
		<table style="width:100%">
			<tr>
                <th>No. Client</th>
                <th>Nama Pemegang ISR / Waba</th>
                <th>Service</th>
                <th>No. Aplikasi</th>
                <th>No. SPP</th>
                <th>Jenis SPP</th>
                <th>Nilai BHP (Rp)</th>
                <th>Tanggal Jatuh Tempo</th>
                <th>Tanggal Diterima Waba</th>
                <th>Metode</th>
                <th>Keterangan</th>
			</tr>
			<?php
				$rResult = $md_pelayanan->getSPPFilter($sWhere,'','',$param);
				while($aRow = $rResult->FetchRow()){ ?>
			<tr>
				<td><?php echo $aRow['no_klien_licence'] ?></td>
				<td><?php echo $aRow['company_name'] ?></td>
				<td><?php echo $aRow['service'] ?></td>
				<td><?php echo $aRow['no_aplikasi'] ?></td>
				<td><?php echo $aRow['no_spp'] ?></td>
				<td><?php echo $aRow['kategori_spp'] ?></td>
				<td><?php echo int_to_rp($aRow['tagihan']) ?></td>
				<td><?php echo $gen_controller->get_date_indonesia($aRow['tgl_jatuh_tempo']) ?></td>
				<td><?php echo $gen_controller->get_date_indonesia($aRow['tgl_terima_waba']) ?></td>
				<td><?php echo $aRow['nama_metode'] ?></td>
				<td><?php echo $aRow['keterangan'] ?></td>
			</tr>
		<?php } ?>
		</table>
<?php } ?>