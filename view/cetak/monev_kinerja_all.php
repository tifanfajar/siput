<?php 
	$sWhere = " where 1=1 ";
	if($tipe=="xls"){
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=Monev-Kinerja-".date('Ymdhis').".xls");
	}
	else if($tipe=="doc"){
		header("Content-Type: application/vnd.ms-word");
		header("Content-Disposition: attachment; filename=Monev-Kinerja-".date('Ymdhis').".doc");
	}
	else if($tipe=="pdf"){
		error_reporting(null);
		ini_set('max_execution_time', 300); 
		require_once 'lib/mpdf/vendor/autoload.php';
		$mydata='<style>table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}</style>
		
			<h4 style="text-align:center">Monev Kinerja</h4>
		<table style="width:100%">
			<tr>
				<th>Tanggal Buat</th>
                <th>Provinsi</th>
                <th>UPT</th>
                <th>Kinerja</th>
                <th>Indikator Kinerja Kegiatan</th>
                <th>Target</th>
                <th>Renaksi Kinerja Target Uraian</th>
                <th>Pagu Anggaran</th>
                <th>Realisasi</th>
                <th>Data Dukung</th>
                <th>Keterangan</th>
			</tr>';
				$rResultpdf = $md_monev->getMonevKinerjaFilter($sWhere,'','',$param);
				while($pdf = $rResultpdf->FetchRow()){ 

					//Renaksi
				     $renaksi   = "";
				     $renaksi_exp=explode("|",$pdf['renaksi_kinerja']);
				     $i = 1;
				     foreach($renaksi_exp as $var) {
				      $zz = $i;
				      if($i<10){
				        $zz = "0".$i; 
				      }
				      if($i<=12){
				        $renaksi .= "B".$zz." : ".$var. "<br/>";
				          $i++;
				      }
				     } 
				     
				     //Realisasi
				     $realisasi = "";
				     $realisasi_exp=explode("|",$pdf['realisasi']);
				     $iz = 1;
				     foreach($realisasi_exp as $var) {
				      $zz = $iz;
				      if($iz<10){
				        $zz = "0".$iz; 
				      }
				      if($iz<=12){
				        $realisasi .= "B".$zz." : ".$var. "<br/>";
				         $iz++;
				      }
				     }

				    $nama_prov = $pdf['id_prov'];
				    $nama_upt  = $pdf['kode_upt'];

				    if($nama_prov=="Only_Admin"){
				      $nama_prov = "<center>Hanya Admin</center>";
				    }  
				    else if(empty($nama_prov)){
				      $nama_prov = "<center>Semua Provinsi</center>";
				    } 
				    else {
				      $nama_prov  = $pdf['nama_prov'];
				    } 

				    if($nama_upt=="Only_Admin"){
				      $nama_upt = "<center>Hanya Admin</center>";
				    }  
				    else if(empty($nama_upt)){
				      $nama_upt = "<center>Semua UPT</center>";
				    } 
				    else {
				      $nama_upt  = $pdf['nama_upt'];
				    }  
					
					$mydata .='<tr>
							<td>'.$gen_controller->get_date_indonesia($pdf['created_date']).'</td>
							<td>'.$nama_prov.'</td>
							<td>'.$nama_upt.'</td>
							<td>'.$pdf['kinerja'].'</td>
							<td>'.$pdf['indikator_kinerja'].'</td>
							<td>'.$pdf['target'].'</td>
							<td>'.$renaksi.'</td>
							<td>'.$pdf['pagu_anggaran'].'</td>
							<td>'.$realisasi.'</td>
							<td>'.$pdf['data_dukung'].'</td>
							<td>'.$pdf['keterangan'].'</td>
						</tr>';
			}
		$mydata .='</table>';

		$filename = "Monev Kinerja ".date('Ymdhis');
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
		$mpdf->AddPage('A4-L');
		$mpdf->WriteHTML($mydata);
		$mpdf->Output($filename,'I');

	}

	if($tipe!="pdf"){ ?>
		<title>Monev Kinerja</title>
		<style>
			table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}
			@media print {
					body {transform: scale(1.0);}
					-webkit-print-color-adjust: exact; 
					body {-webkit-print-color-adjust: exact;}
			}
			@page { size: auto;  margin: 0mm; }
		</style>
		<center>
			<h4>Monev Kinerja</h4>
			<?php  if($tipe=="print"){ ?>
				<button id="printPageButton1" class="no-print" onclick="document.getElementById('printPageButton1').style.visibility='hidden';javascript:window.print();document.getElementById('printPageButton1').style.visibility='visible';" type="button">Print</button><br/>
			<?php } ?>
		</center>
		<table style="width:100%">
			<tr>
				<th>Tanggal Buat</th>
                <th>Provinsi</th>
                <th>UPT</th>
                <th>Kinerja</th>
                <th>Indikator Kinerja Kegiatan</th>
                <th>Target</th>
                <th>Renaksi Kinerja Target Uraian</th>
                <th>Pagu Anggaran</th>
                <th>Realisasi</th>
                <th>Data Dukung</th>
                <th>Keterangan</th>
			</tr>
			<?php
				$rResult = $md_monev->getMonevKinerjaFilter($sWhere,'','',$param);
				while($aRow = $rResult->FetchRow()){ 

					//Renaksi
				     $renaksi   = "";
				     $renaksi_exp=explode("|",$aRow['renaksi_kinerja']);
				     $i = 1;
				     foreach($renaksi_exp as $var) {
				      $zz = $i;
				      if($i<10){
				        $zz = "0".$i; 
				      }
				      if($i<=12){
				        $renaksi .= "B".$zz." : ".$var. "<br/>";
				          $i++;
				      }
				     } 
				     
				     //Realisasi
				     $realisasi = "";
				     $realisasi_exp=explode("|",$aRow['realisasi']);
				     $iz = 1;
				     foreach($realisasi_exp as $var) {
				      $zz = $iz;
				      if($iz<10){
				        $zz = "0".$iz; 
				      }
				      if($iz<=12){
				        $realisasi .= "B".$zz." : ".$var. "<br/>";
				         $iz++;
				      }
				     }


				    $nama_prov = $aRow['id_prov'];
				    $nama_upt  = $aRow['kode_upt'];

				    if($nama_prov=="Only_Admin"){
				      $nama_prov = "<center>Hanya Admin</center>";
				    }  
				    else if(empty($nama_prov)){
				      $nama_prov = "<center>Semua Provinsi</center>";
				    } 
				    else {
				      $nama_prov  = $aRow['nama_prov'];
				    } 

				    if($nama_upt=="Only_Admin"){
				      $nama_upt = "<center>Hanya Admin</center>";
				    }  
				    else if(empty($nama_upt)){
				      $nama_upt = "<center>Semua UPT</center>";
				    } 
				    else {
				      $nama_upt  = $aRow['nama_upt'];
				    }  
				?>
			<tr>
				<td><?php echo $gen_controller->get_date_indonesia($aRow['created_date']) ?></td>
				<td><?php echo $nama_prov ?></td>
				<td><?php echo $nama_upt ?></td>
				<td><?php echo $aRow['kinerja'] ?></td>
				<td><?php echo $aRow['indikator_kinerja'] ?></td>
				<td><?php echo $aRow['target'] ?></td>
				<td><?php echo $renaksi ?></td>
				<td><?php echo $aRow['pagu_anggaran'] ?></td>
				<td><?php echo $realisasi ?></td>
				<td><?php echo $aRow['data_dukung'] ?></td>
				<td><?php echo $aRow['keterangan'] ?></td>
			</tr>
		<?php } ?>
		</table>
<?php } ?>