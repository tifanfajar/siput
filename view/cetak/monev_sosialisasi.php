<?php 
	$sWhere = " where 1=1 ";
	if($tipe=="xls"){
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=Monev-Sosialisasi-".date('Ymdhis').".xls");
	}
	else if($tipe=="doc"){
		header("Content-Type: application/vnd.ms-word");
		header("Content-Disposition: attachment; filename=Monev-Sosialisasi-".date('Ymdhis').".doc");
	}
	else if($tipe=="pdf"){
		error_reporting(null);
		ini_set('max_execution_time', 300); 
		require_once 'lib/mpdf/vendor/autoload.php';
		$mydata='<style>table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}</style>
		
			<h4 style="text-align:center">Monev Sosialisasi</h4>
		<table style="width:100%">
			<tr>
				<th>Tanggal Buat</th>
                <th>Provinsi</th>
                <th>UPT</th>
                <th>Jenis Kegiatan</th>
                <th>Tanggal Pelaksanaan</th>
                <th>Tempat</th>
                <th>Tema</th>
                <th>Target Peserta</th>
                <th>Jumlah Peserta</th>
                <th>Anggaran</th>
                <th>Narasumber</th>
                <th>Keterangan</th>
			</tr>';
				$rResultpdf = $md_sosialisasi_bimtek->getMonevSosialisasiFilter($sWhere,'','',$param);
				while($pdf = $rResultpdf->FetchRow()){ 
						$nama_prov = $pdf['nama_prov'];
					    $nama_upt  = $pdf['nama_upt'];

					    if(empty($nama_prov)){
					      $nama_prov = "<center>Semua Provinsi</center>";
					    }  
					    if(empty($nama_upt)){
					      $nama_upt = "<center>Semua UPT</center>";
					    }   
					$mydata .='<tr>
							<td>'.$gen_controller->get_date_indonesia($pdf['created_date']).'</td>
							<td>'.$nama_prov.'</td>
							<td>'.$nama_upt.'</td>
							<td>'.$pdf['kegiatan'].'</td>
							<td>'.$gen_controller->get_date_indonesia($pdf['tgl_pelaksanaan']).'</td>
							<td>'.$pdf['tempat'].'</td>
							<td>'.$pdf['tema'].'</td>
							<td>'.$pdf['target_peserta'].'</td>
							<td>'.$pdf['jumlah_peserta'].'</td>
							<td>'.int_to_rp($pdf['anggaran']).'</td>
							<td>'.str_replace('|', ', ', $pdf['narasumber']).'</td>
							<td>'.$pdf['keterangan'].'</td>
						</tr>';
			} 
		$mydata .='</table>';

		$filename = "Monev Sosialisasi ".date('Ymdhis');
		$mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
		$mpdf->AddPage('A4-L');
		$mpdf->WriteHTML($mydata);
		$mpdf->Output($filename,'I');

	}

	if($tipe!="pdf"){ ?>
		<title>Monev Sosialisasi</title>
		<style>
			table {
		    	border-collapse: collapse;
			}
			table, th, td {
			    border: 1px solid black;
			}
			@media print {
					body {transform: scale(1.0);}
					-webkit-print-color-adjust: exact; 
					body {-webkit-print-color-adjust: exact;}
			}
			@page { size: auto;  margin: 0mm; }
		</style>
		<center>
			<h4>Monev Sosialisasi</h4>
			<?php  if($tipe=="print"){ ?>
				<button id="printPageButton1" class="no-print" onclick="document.getElementById('printPageButton1').style.visibility='hidden';javascript:window.print();document.getElementById('printPageButton1').style.visibility='visible';" type="button">Print</button><br/>
			<?php } ?>
		</center>
		<table style="width:100%">
			<tr>
				<th>Tanggal Buat</th>
                <th>Provinsi</th>
                <th>UPT</th>
                <th>Jenis Kegiatan</th>
                <th>Tanggal Pelaksanaan</th>
                <th>Tempat</th>
                <th>Tema</th>
                <th>Target Peserta</th>
                <th>Jumlah Peserta</th>
                <th>Anggaran</th>
                <th>Narasumber</th>
                <th>Keterangan</th>
			</tr>
			<?php
				$rResult = $md_sosialisasi_bimtek->getMonevSosialisasiFilter($sWhere,'','',$param);
				while($aRow = $rResult->FetchRow()){ 
						$nama_prov = $aRow['nama_prov'];
					    $nama_upt  = $aRow['nama_upt'];

					    if(empty($nama_prov)){
					      $nama_prov = "<center>Semua Provinsi</center>";
					    }  
					    if(empty($nama_upt)){
					      $nama_upt = "<center>Semua UPT</center>";
					    }   
					?>
			<tr>
				<td><?php echo $gen_controller->get_date_indonesia($aRow['created_date']) ?></td>
				<td><?php echo $nama_prov ?></td>
				<td><?php echo $nama_upt ?></td>
				<td><?php echo $aRow['kegiatan'] ?></td>
				<td><?php echo $gen_controller->get_date_indonesia($aRow['tgl_pelaksanaan']) ?></td>
				<td><?php echo $aRow['tempat'] ?></td>
				<td><?php echo $aRow['tema'] ?></td>
				<td><?php echo $aRow['target_peserta'] ?></td>
				<td><?php echo $aRow['jumlah_peserta'] ?></td>
				<td><?php echo int_to_rp($aRow['anggaran']) ?></td>
				<td><?php echo str_replace('|', ', ', $aRow['narasumber']) ?></td>
				<td><?php echo $aRow['keterangan'] ?></td>
			</tr>
		<?php } ?>
		</table>
<?php } ?>