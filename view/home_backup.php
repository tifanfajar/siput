<title>Dashboard - <?php echo $web['judul']?></title>
<body class="dash-back">

  <?php 
    include "view/top_menu.php"; 
  ?>
 <?php if(!empty($web['running_text'])){ ?>
      <marquee class="animated fadeInDown" style="margin-bottom:25px;background-color:#<?php echo $web['running_text_color_bg'] ?>;">
          <span style="color:#<?php echo $web['running_text_color'] ?>;font-size:<?php echo $web['running_text_size'] ?>"><?php echo $web['running_text'] ?></span>
      </marquee>
    <?php } ?>
  <!-- Content -->
  <div class="container container-mobile animated fadeInDown">
    <div class="row">
      <div class="col-md-12">
        <h5 class="judul-aplikasi"><?php echo $web['judul']?></h5>
      </div>
     
      <!-- Button -->
      <?php 
          $check_pelayanan = $gen_model->GetOne("fua_read","tr_function_access",array('id_menu'=>2,'kode_function_access'=>$_SESSION['group_user'])); 
          if(!empty($check_pelayanan)){ ?>
            <div class="col-md-4 btn-cnt">
              <a href="<?php echo $basepath ?>pelayanan" class="btn-dash1"></a>
              <h5 class="bnt-text">Pelayanan</h5>
            </div>
        <?php } 
         $check_bimtek = $gen_model->GetOne("fua_read","tr_function_access",array('id_menu'=>3,'kode_function_access'=>$_SESSION['group_user'])); 
         if(!empty($check_bimtek)){ ?>
          <div class="col-md-4 btn-cnt">
            <a href="<?php echo $basepath ?>sosialisasi_bimtek" class="btn-dash2"></a>
            <h5 class="bnt-text">Sosialisasi & Bimtek</h5>
          </div>
      <?php } 

       $check_validasi_data = $gen_model->GetOne("fua_read","tr_function_access",array('id_menu'=>4,'kode_function_access'=>$_SESSION['group_user'])); 
     if(!empty($check_bimtek)){ ?>
          <div class="col-md-4 btn-cnt">
            <a href="<?php echo $basepath ?>validasi_data" class="btn-dash5"></a>
            <h5 class="bnt-text">Validasi Data</h5>
          </div>
    <?php } 


      $check_monev_kinerja = $gen_model->GetOne("fua_read","tr_function_access",array('id_menu'=>5,'kode_function_access'=>$_SESSION['group_user'])); 
       if(!empty($check_monev_kinerja)){ ?>
      <div class="col-md-4 btn-cnt">
        <a href="<?php echo $basepath ?>monev_kinerja" class="btn-dash6"></a>
        <h5 class="bnt-text">Monev Kinerja</h5>
      </div>
    <?php } 

      $check_daftar_pengguna = $gen_model->GetOne("fua_read","tr_function_access",array('id_menu'=>6,'kode_function_access'=>$_SESSION['group_user'])); 
      if(!empty($check_daftar_pengguna)){ ?>
      <div class="col-md-4 btn-cnt">
        <a href="<?php echo $basepath ?>setting" class="btn-dash3"></a>
        <h5 class="bnt-text">Setting</h5>
      </div>
    <?php } ?>
     


      <div class="col-md-12"><br/>
        <table align="center">
        <?php if(!empty($web['alamat'])){ ?>
          <tr title="Address">
            <td><small><i class="fa fa-map-marker"></i></small> &nbsp;</td>
            <td><small><?php echo $web['alamat'] ?></small></td>
          </tr>
        <?php } 
        if(!empty($web['no_tlp'])){ ?>
          <tr title="Contact">
            <td><small><i class="fa fa-phone"></i></small> &nbsp;</td>
            <td>
                <small>
                  <?php echo $web['no_tlp'].(!empty($web['no_hp']) ? ' - '.$web['no_hp'] : '') ?>
                </small>
            </td>
          </tr>
        <?php } 
        if(!empty($web['no_fax'])){ ?>
          <tr title="Faximilie">
            <td><small><i class="fa fa-print"></i></small> &nbsp;</td>
            <td>
                <small>
                  <?php echo $web['no_fax'] ?>
                </small>
            </td>
          </tr>
        <?php  } 
        if(!empty($web['email'])){ ?>
          <tr title="Faximilie">
            <td><small><i class="fa fa-envelope"></i></small> &nbsp;</td>
            <td>
                <small>
                  <a style="text-decoration:none" href="mailto:<?php echo $web['email'] ?>"><?php echo $web['email'] ?></a>
                </small>
            </td>
          </tr>
        <?php } ?>
        </table>
        <small class="copy">Copyright &copy; <?php echo date("Y") ?> Direktorat Operasi Sumber Daya [OPS11]</small>
      </div>

    </div>
  </div>
  <!-- End Content -->
  
</body>