<title>Kegiatan - <?php echo $web['judul']?></title>


<style type="text/css">
  #example_paginate{position: relative; left: 40%; top: 5px;}
  .pagination li a{margin:5px 15px; }
  .dataTables_info{margin-bottom: 15px;}
  .datatable>tbody>tr>td
  {
    white-space: nowrap;
  } 
  .table th {
     text-align: center;
     font-weight:bold;
  }
</style>
<body class="pelayanan-cnt">


  <?php  include "view/top_menu.php";  ?>
  
  <!-- Main Container -->
  <div class="container-fluid animated fadeInUp" style="z-index: 3;">
    <div class="row">
  <?php  
    include "view/menu.php";  
   ?>
  <!-- Content -->
    <div class="container-fluid main-content" >
      <div class="row">


        <ul class="nav nav-tabs main-nav-tab" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="materi-tab"  onclick="refresh_table()" data-toggle="materi" href="#materi" role="tab" aria-controls="materi" aria-selected="false">Kegiatan</a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent" style="padding-top:0px">


             <!-- Tab Kegiatan -->
           <div class="tab-pane fade show active" id="materi" role="tabpanel" aria-labelledby="materi-tab">

            <div id="filter_upt" class="table table-responsive pelayanan-top">
               <table id="tb_upt" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
                    <th>Kode Kegiatan</th>
                    <th>Jenis Kegiatan</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            <script type="text/javascript">
              var dTable;
              $(document).ready(function() {
                dTable = $('#tb_upt').DataTable( {
                  "bProcessing": true,
                  "bServerSide": true,
                  "bJQueryUI": false,
                  "responsive": false,
                  "autoWidth": false,
                  "bLengthChange": false,
                  "bPaginate": false,
                  "sAjaxSource": "<?php echo $basepath ?>kegiatan/rest", 
                  "sServerMethod": "POST",
                  "scrollX": true,
                  "scrollY": "350px",
                    "scrollCollapse": true,
                  "columnDefs": [
                    { "orderable": true, "targets": 0, "searchable": true},
                    { "orderable": true, "targets": 1, "searchable": true}
                  ]
                } );
              });
            </script>
            </div>  
          </div>



        </div>
      </div>
    </div>
    </div>
  </div>

<script type="text/javascript">

function refresh_table(){
  setTimeout(function(){ 
    $('#tb_upt').DataTable().ajax.reload();
  }, 300);
  $("#call_modal").modal("hide");
  $(".call_modal").modal("hide");
}

 
</script>