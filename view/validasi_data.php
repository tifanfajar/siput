<?php  
 $check_akses = $gen_model->GetOne("fua_read","tr_function_access",array('id_menu'=>5,'kode_function_access'=>$_SESSION['group_user']));
 if(empty($check_akses)){
    echo '<meta http-equiv="refresh" content="0;url='.$basepath.'home">';
 }
 else { ?>
<title>Validasi Data - <?php echo str_replace('<br/>','',$web['judul']) ?></title>


<style type="text/css">
  #example_paginate{position: relative; left: 40%; top: 5px;}
  .pagination li a{margin:5px 15px; }
  .dataTables_info{margin-bottom: 15px;}
  .datatable>tbody>tr>td
  {
    white-space: nowrap;
  } 
  .table th {
     text-align: center;
     font-weight:bold;
  }
</style>
<body class="pelayanan-cnt">


  <?php 
   include "view/top_menu.php"; 
   $akses_add_tab_validasi_data = $gen_model->GetOne("fua_add","tr_function_access_tab",array('id_tab'=>11,'kode_function_access'=>$_SESSION['group_user'])); 
   $akses_add_tab_validasi_data      = (!empty($akses_add_tab_validasi_data) ? $akses_add_tab_validasi_data : '0');
   ?>
  
  <!-- Main Container -->
  <div class="container-fluid animated fadeInUp"  style="z-index: 3;">
    <div class="row">
  <?php  
    include "view/menu.php";  
   ?>
  <!-- Content -->
    <div class="container-fluid main-content" id="div_content" >
      <div class="row">
        <ul class="nav nav-tabs main-nav-tab" id="myTab" role="tablist">
          <?php 
              $data_tb = $db->SelectLimit("select * from ms_menu_tab where men_id='4' order by urutan asc"); 
              $no_tab = 1;
              $men_active =""; 
              $total_tab = 0;
              while($list = $data_tb->FetchRow()){
                  foreach($list as $key=>$val){
                        $key=strtolower($key);
                        $$key=$val;
                      } 
                      $whr_tab = array();
                      $whr_tab['kode_function_access'] = $_SESSION['group_user'];
                      $whr_tab['id_tab'] = $tab_id;
                      $check_tab = $gen_model->GetOne("fua_read","tr_function_access_tab",$whr_tab);
                     if(!empty($check_tab)){ 
                      $active_tab ="";   
                      if($no_tab==1) {
                           $active_tab ="active";
                           $men_active = $tab_url;   
                      }
                      ?>  
                      <li class="nav-item">
                        <a class="nav-link <?php echo $active_tab ?>" onclick="refresh_table()" id="<?php echo $tab_url ?>-tab" data-toggle="tab" href="#<?php echo $tab_url ?>" role="tab" aria-controls="<?php echo $tab_url ?>" aria-selected="true"><?php echo $tab ?></a>
                      </li>
                   <?php $no_tab++;    
                   $total_tab++;
                 } 
                 
              } 
              function active_tab($list_tab){
                    global $men_active;
                    if($men_active==$list_tab){
                       return "show active";
                    }
              }
              ?>
        </ul>


      <?php  if($total_tab!=0){  ?>
        <div class="tab-content" id="myTabContent">
          <!-- Tab Bahan Sosialisasi -->
          <div class="tab-pane fade <?php echo active_tab("validasi_data") ?>" id="validasi_data" role="tabpanel" aria-labelledby="home-tab">
           
            <!-- Peta -->
            <label class="judul-peta if_upt validasi_data_utama"><i class="fa fa-map-marked-alt"></i> Peta Lokasi UPT</label>
            <hr class="if_upt validasi_data_utama">
            <div class="peta-cnt" id="peta_validasi_Data"></div>

            <!-- Grafik Chart -->
            <div class="row if_upt validasi_data_utama" >
                <div class="col-sm-12" id="div_validasi_data_grafik_1">
                  <div class="card-set">
                    <div class="headset"><i class="fa fa-chart-pie"></i> Rekapitulasi Laporan UPT</div>
                    <div class="img-chart">
                      <!-- <img src="images/chart.jpg" alt=""> -->
                      <div id="validasi_data_grafik" style="min-width: 310px; height: 400px; max-width: 100%; margin: 0 auto"></div>
                    </div>
                  </div>
                </div>
                 <br/>
            </div>

            <div class="row">
                <div class="col-sm-12" id="validasi_data_table_data_utama"></div>
            </div>
            <input type="hidden" name="ValidasiDataPencarian_all" id="ValidasiDataPencarian_all" >
            <script type="text/javascript">
              function print_laporan_validasi_all(tipe){
                  var param = $("#ValidasiDataPencarian_all").val();

                    var form = document.createElement("form");
                    form.setAttribute("method", "POST");
                    if(tipe=="print"){
                     form.setAttribute("target", "_blank");
                    }
                    form.setAttribute("action", "<?php echo $basepath ?>validasi_data/cetak_all");
                           
                        var hiddenField = document.createElement("input");
                        hiddenField.setAttribute("type", "hidden");
                        hiddenField.setAttribute("name", "param");
                        hiddenField.setAttribute("value", param);

                        var hiddenField2 = document.createElement("input");
                        hiddenField2.setAttribute("type", "hidden");
                        hiddenField2.setAttribute("name", "tipe");
                        hiddenField2.setAttribute("value", tipe);

                        form.appendChild(hiddenField);
                        form.appendChild(hiddenField2);

                    document.body.appendChild(form);
                    form.submit();
                }
            </script>
            <!-- Tabel -->
            <div class="cnt-heaeder validasi_data_utama" style="margin-top: 30px; margin-bottom: 20px;">
              <h6 class="table-label" style="width: 100%; display: block;"><i class="fa fa-portrait"></i> Validasi Data</h6>
              
              <div class="btn-group col-sm-12 pull-right" role="group" aria-label="Basic example">
               
                <?php  if($akses_add_tab_validasi_data="1"){  ?>
                    <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Tambah" onclick="call_modal_default('Tambah Validasi Data','Form_Validasi_Data_ALL','do_add','validasi_data/form_all')"   class="btn btn-primary btn-sm pull-right btn-table left-trash btn-intable"><i class="fa fa-file"></i></a>
                <?php } ?>

                <a href="javascript:void(0)" data-tooltip="tooltip" data-placement="bottom" title="Cetak" onclick="print_laporan_validasi_all('print')" class="btn btn-secondary btn-sm btn-table"><i class="fa fa-print"></i></a>

                <?php  if($akses_add_tab_validasi_data="1"){  ?>
                    <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Import" data-target="#Import_ValidasiDataAll"><i class="fa fa-upload"></i></a>
                <?php } ?>
                
                <a href="" class="btn btn-secondary btn-sm btn-table" data-toggle="modal" data-tooltip="tooltip" data-placement="bottom" title="Filterisasi" data-target="#Filter_ValidasiDataAll"  ><i class="fa fa-filter"></i></a>

                <div class="btn-group" role="group" data-tooltip="tooltip" data-placement="bottom" title="Export">
                  <button id="red" type="button" class="btn btn-secondary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-file-pdf"></i>
                  </button>
                  <div class="dropdown-menu" aria-labelledby="red">
                    <a class="dropdown-item"  onclick="print_laporan_validasi_all('pdf')" href="javascript:void(0)">Export to PDF</a>
                    <a class="dropdown-item"   onclick="print_laporan_validasi_all('xls')" href="javascript:void(0)">Export to XLS</a>
                    <a class="dropdown-item" onclick="print_laporan_validasi_all('doc')"   href="javascript:void(0)">Export to DOCS</a>
                  </div>
                </div>
              </div>
              <hr style="width: 100%; float: left; display: block;">
            </div>

            <div id="filter_validasi_data_all" class="table table-responsive pelayanan-top validasi_data_utama">
              <table id="tb_validasi_data" class="display table table-striped pelayanan-table datatable">
                <thead>
                  <tr>
                    <th class="text-center" rowspan="2">Tanggal Buat</th>
                    <th class="text-center" rowspan="2">Provinsi</th>
                    <th class="text-center" rowspan="2">UPT</th>
                    <th class="text-center" rowspan="2">DATA SIMS</th>
                    <th class="text-center" rowspan="2">Data Sampling</th>
                    <th class="text-center" colspan="3">Data Hasil Inspeksi</th>
                    <th class="text-center" colspan="3">Tindak Lanjut Hasil Inspeksi</th>
                    <th class="text-center" rowspan="2">Capaian (% Valid)</th>
                    <th class="text-center" rowspan="2">Lampiran</th>
                    <th class="text-center" rowspan="2">Keterangan</th>
                    <th class="text-center" rowspan="2">Aksi</th>
                  </tr>
                  <tr>
                    <th  class="text-center">Sesuai ISR</th>
                    <th  class="text-center">Tidak Sesuai</th>
                    <th  class="text-center">Tidak Aktif</th> 
                    <th  class="text-center">Proses ISR</th>
                    <th  class="text-center">Sudah di tindaklanjuti</th>
                    <th  class="text-center">Belum di tindaklanjuti</th>
                  </tr> 
                  <tr>
                    <th  class="text-center">1</th>
                    <th  class="text-center">2</th>
                    <th  class="text-center">3</th> 
                    <th  class="text-center">4</th>
                    <th  class="text-center">5</th>
                    <th  class="text-center">6</th>
                    <th  class="text-center">7</th>
                    <th  class="text-center">8</th>
                    <th  class="text-center">9</th>
                    <th  class="text-center">10</th>
                    <th  class="text-center">11</th>
                    <th  class="text-center">12</th>
                    <th  class="text-center">13</th>
                    <th  class="text-center">14</th>
                    <th  class="text-center">15</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            <script type="text/javascript">
              var dTable1;
              $(document).ready(function() {
                dTable1 = $('#tb_validasi_data').DataTable( {
                  "bProcessing": true,
                  "searching": false,
                  "bServerSide": true,
                  "bJQueryUI": false,
                  "retrieve": true,
                  "responsive": false,
                  "autoWidth": false,
                  "sAjaxSource": "<?php echo $basepath ?>validasi_data/rest", 
                  "sServerMethod": "POST",
                  "scrollX": true,
                  "scrollY": "350px",
                    "scrollCollapse": true,
                    "order": [[ 0, "desc" ]],
                  "columnDefs": [
                  { "orderable": true, "targets": 0, "searchable": true},
                  { "orderable": true, "targets": 1, "searchable": true},
                  { "orderable": true, "targets": 2, "searchable": true},
                  { "orderable": true, "targets": 3, "searchable": true},
                  { "orderable": true, "targets": 4, "searchable": true },
                  { "orderable": true, "targets": 5, "searchable": true },
                  { "orderable": true, "targets": 6, "searchable": true },
                  { "orderable": true, "targets": 7, "searchable": true },
                  { "orderable": true, "targets": 8, "searchable": true },
                  { "orderable": true, "targets": 9, "searchable": true },
                  { "orderable": true, "targets": 10, "searchable": true },
                  { "orderable": true, "targets": 11, "searchable": true },
                  { "orderable": false, "targets": 12, "searchable": false },
                  { "orderable": true, "targets": 13, "searchable": true },
                  { "orderable": false, "targets": 14, "searchable": false, "width":200}
                  ]
                } );
              });
            </script>
            </div>    
          </div>
        </div>
      <?php } else { 
        echo "<div class='row'>
                <div class='col-md-12'>
                    <h3>Anda tidak mempunyai akses apapun pada halaman ini</h3>
                </div>
              </div>"; 
        echo "<script>
                var  height_main =  $('.pelayanan-cnt').height();
                     $('#div_content').height(height_main);
                     $('#myTab').remove();
              </script>";
      }?>
      </div>
    </div>
    </div>
  </div>

<script type="text/javascript">

function pilih_upt_validasi_data(id_map) {
  call_lokasi_upt(id_map,'validasi_data_table_data_utama');
  $('.validasi_data_utama').hide();
}
function  call_lokasi_upt(id_map,div_id){
   $.ajax({
         type: "POST",
         dataType: "html",
         url: "<?php echo $basepath ?>validasi_data/pilih_upt",
         data: "map_id="+id_map+"&div_id="+div_id,
         success: function(msg){
            if(msg){
              $("#"+div_id).html(msg);
            }
         }
      }); 
} 
    
    //Start Chart Validasi Data
    var chart_validasi_data_kunjungan = [
      <?php 
            $validasi_data_no = 0;
            //Chart by kunjungan
            $chart_kunjungan="";
            $sql = "SELECT  pv.nama_prov,COUNT(kode_validasi_data) as total
                from tr_validasi_data as vld
                  inner join ms_provinsi as pv 
                      on pv.id_prov=vld.id_prov  where  vld.status='3' GROUP BY pv.nama_prov";
            $get_chart_tujuan  = $db->Execute($sql);
            while($list = $get_chart_tujuan->FetchRow()){
              foreach($list as $key=>$val){
                                $key=strtolower($key);
                                $$key=$val;
              }  
              $chart_kunjungan .='{"name":"'.$nama_prov.'","y":'.$total.'},';
              $validasi_data_no++;
            } 
            echo trim($chart_kunjungan,',');
      ?>
    ];

     <?php 
        if($validasi_data_no==0){
          echo " $('#div_validasi_data_grafik_1').hide(); ";
        }
     ?>
    //End Chart Validasi Data

<?php  
//Kepala UPT atau Operator UPT
if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){ 
    $wilayah   = $gen_model->GetOneRow("ms_provinsi",array('id_prov'=>$_SESSION['upt_provinsi'])); ?>
     window.onload =  function(){ 
        $(".if_upt").hide();
        detail_validasi_data('<?php echo $wilayah['id_map'] ?>','<?php echo $_SESSION['kode_upt'] ?>');
     };

<?php } else { ?>

  //Map and chart
    window.onload =  function(){
      show_map('peta_validasi_Data','pilih_upt_validasi_data'); 
      pie_chart('validasi_data_grafik',chart_validasi_data_kunjungan);    
    };

<?php } ?>

//Start Validasi Data
function after_crud_validasi_data(){
    var map_id = $(".data_map_id_validasi_data").val();
    var upt = $(".data_upt_validasi_data").val();
    detail_validasi_data(map_id,upt);
    $('#call_modal').modal('hide');
}
function detail_validasi_data(id_map,upt){
  $(".data_map_id_validasi_data").val(id_map);
  $(".data_upt_validasi_data").val(upt);
  $('.validasi_data_utama').hide();
  ajax_table_map('validasi_data_table_data_utama',id_map,upt,'validasi_data/table');
  //$('html, body').animate({
       // scrollTop: $("#pelayanan_table_data_utama").offset().top;
  //}, 1000);
  refresh_table();
}     
function do_edit_all(param_id){
  call_modal_default('Ubah Validasi Data','Form_Validasi_Data_ALL','do_edit','validasi_data/form_all');
   setTimeout(function(){ 
       $.ajax({
            url: '<?php echo $basepath ?>validasi_data/edit/'+param_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $("#kode_validasi_data").val(data.kode_validasi_data);
                $("#data_sims").val(data.data_sims);
                $("#data_sampling").val(data.data_sampling);
                $("#sesuai_isr").val(data.sesuai_isr);
                $("#tdk_sesuai_isr").val(data.tdk_sesuai_isr);
                $("#tidak_aktif").val(data.tidak_aktif);
                $("#proses_isr").val(data.proses_isr);
                $("#sdh_ditindaklanjuti").val(data.sudah_ditindaklanjuti);
                $("#blm_ditindaklanjuti").val(data.belum_ditindaklanjuti);
                $("#capaian").val(data.capaian);
                $("#capaian").val(data.capaian);
                $("#keterangan").val(data.keterangan);
                $("#upt_provinsi").val(data.id_prov);
                $('#lampiran').attr('href',data.lampiran);
                     $.ajax({
                         type: "POST",
                         dataType: "html",
                         url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
                         data: "provinsi="+data.id_prov,
                         success: function(msg){
                            if(msg){
                              $("#kode_upt").html(msg);

                              setTimeout(function(){ 
                                   $("#kode_upt").val(data.kode_upt);        
                              }, 500);
                             
                            }
                         }
                      }); 

            }
        });
   }, 1000);
}
function do_edit(param_id,map_id,upt){
  call_modal_from_map('Ubah Validasi Data','Form_Validasi_Data',map_id,upt,'do_edit','validasi_data/form');
   setTimeout(function(){ 
       $.ajax({
            url: '<?php echo $basepath ?>validasi_data/edit/'+param_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $("#kode_validasi_data").val(data.kode_validasi_data);
                $("#data_sims").val(data.data_sims);
                $("#data_sampling").val(data.data_sampling);
                $("#sesuai_isr").val(data.sesuai_isr);
                $("#tdk_sesuai_isr").val(data.tdk_sesuai_isr);
                $("#tidak_aktif").val(data.tidak_aktif);
                $("#proses_isr").val(data.proses_isr);
                $("#sdh_ditindaklanjuti").val(data.sudah_ditindaklanjuti);
                $("#blm_ditindaklanjuti").val(data.belum_ditindaklanjuti);
                $("#capaian").val(data.capaian);
                $("#capaian").val(data.capaian);
                $("#keterangan").val(data.keterangan);
                $('#lampiran').attr('href',data.lampiran);
            }
        });
   }, 1000);
}
function do_detail(param_id,map_id,upt){
   call_modal_from_map('Detail Validasi Data','Form_Validasi_Data',map_id,upt,'do_detail','validasi_data/form');
   setTimeout(function(){ 
       $.ajax({
            url: '<?php echo $basepath ?>validasi_data/edit/'+param_id,
            type: 'POST',
            dataType: 'JSON',
            success: function(data) {
                $("#kode_validasi_data").val(data.kode_validasi_data);
                $("#data_sims").val(data.data_sims);
                $("#data_sampling").val(data.data_sampling);
                $("#sesuai_isr").val(data.sesuai_isr);
                $("#tdk_sesuai_isr").val(data.tdk_sesuai_isr);
                $("#tidak_aktif").val(data.tidak_aktif);
                $("#proses_isr").val(data.proses_isr);
                $("#sdh_ditindaklanjuti").val(data.sudah_ditindaklanjuti);
                $("#blm_ditindaklanjuti").val(data.belum_ditindaklanjuti);
                $("#capaian").val(data.capaian);
                $("#capaian").val(data.capaian);
                $("#keterangan").val(data.keterangan);
                $('#lampiran').attr('href',data.lampiran);
            }
        });
   }, 1000);
}
function do_delete(parameter_data,param_id){
    act_delete('Hapus Validasi Data','Anda ingin menghapus Validasi Data ? ','warning','validasi_data/delete','no_refresh','after_crud_validasi_data',window.atob(parameter_data),window.atob(param_id));
}
function do_delete_all(parameter_data,param_id){
    act_delete('Hapus Bahan Sosialisasi','Anda ingin menghapus Validasi Data ? ','warning','validasi_data/delete','no_refresh','refresh_table',window.atob(parameter_data),window.atob(param_id));
}
//End Validasi Data


function refresh_table(){
  setTimeout(function(){ 
    //Validasi Data
    $('#tb_validasi_data').DataTable().ajax.reload();
    $('#tb_validasi_data_waiting').DataTable().ajax.reload();
  }, 500);
  $("#call_modal").modal("hide");
  $(".call_modal").modal("hide");
}
$('[data-tooltip="tooltip"]').tooltip();
</script>
<?php } ?>