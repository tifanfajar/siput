<!-- Start User Filter -->
<div id="Filter_User" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_user_filter" method="POST">

            <div class="row">  
              <div class="col-md-6 form-group form-box col-xs-12">
                <span class="label">Username</span> 
                <input class="form-control" id="filter_usr_username" name="filter_username"  placeholder=""  type="text" >
              </div>
              <div class="col-md-6 form-group form-box col-xs-12">
                <span class="label">Nama Lengkap</span>
                <input class="form-control" maxlength="50" id="filter_usr_nama_lengkap" name="filter_nama_lengkap"  placeholder=""  type="text" >
              </div>
            </div>

            <div class="row">  
              <div class="col-md-6 form-group form-box col-xs-12">
                <span class="label">Status</span> 
                    <select  class="form-control"  id="filter_usr_status" name="filter_status">
                      <option value="">Pilih Status</option>
                      <option value="0">Non Aktif</option>
                      <option value="1">Aktif</option>
                      <option value="2">Blokir</option>
                    </select>
              </div>
              <div class="col-md-6 form-group form-box col-xs-12">
                <span class="label">Group User</span>
                  <select onchange="get_grp()" class="form-control"  id="filter_usr_group_user" name="filter_group_user">
                    <option value="">Pilih Group User</option>
                    <?php 
                        $whr_data = array();
                      $data_shf = $gen_model->GetWhere('ms_group');
                        while($list = $data_shf->FetchRow()){
                          foreach($list as $key=>$val){
                                      $key=strtolower($key);
                                      $$key=$val;
                                    }  
                      ?>
                    <option value="<?php echo $kode_group ?>"><?php echo $group_name ?></option>
                    <?php } ?>
                  </select>
              </div>
            </div> 


            <div class="row">  
              <div class="col-md-12 form-group form-box col-xs-12">
                <span class="label">Provinsi</span> 
                      <select class="form-control"  onchange="call_upt_user()" id="filter_usr_provinsi" name="filter_provinsi" >
                      <option value="">Pilih Provinsi</option>
                      <?php 
                          $whr_data = array();
                        $data_shf = $gen_model->GetWhere('ms_provinsi');
                          while($list = $data_shf->FetchRow()){
                            foreach($list as $key=>$val){
                                        $key=strtolower($key);
                                        $$key=$val;
                                      }  
                        ?>
                      <option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
                      <?php } ?>
                    </select>
              </div>
              <div class="col-md-12 form-group form-box col-xs-12">
                <span class="label">UPT</span> 
                    <select  class="form-control"  id="filter_usr_upt" name="filter_upt">
                      <option value="">Pilih UPT</option>
                    </select>
              </div>
            </div>

            <div class="row">  
              <div class="col-md-6 form-group form-box col-xs-12">
                <span class="label">No Telp</span>
                <input class="form-control" id="filter_usr_no_tlp" name="filter_no_tlp"  placeholder="" type="text">
              </div>
              <div class="col-md-6 form-group form-box col-xs-12">
                <span class="label">No HP</span> 
                <input class="form-control" id="filter_usr_no_hp" name="filter_no_hp"  placeholder="" type="text" >
              </div>
            </div> 

            <div class="row">  
              <div class="col-md-12 form-group form-box col-xs-12">
                <span class="label">Email</span> 
                <input class="form-control"  id="filter_usr_email" name="filter_email"  placeholder="" type="email" >
              </div>
            </div>

           <div class="row">
              <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label">Created Date</span><br/>
                <span class="label" style="font-size: 13px;">Mulai</span> 
                <input class="form-control tgl"  id="filter_usr_date_from" name="filter_date_from" maxlength="50"  placeholder="" type="text"  >
              </div>
              <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label"></span><br/>
                <span class="label" style="font-size: 13px;">Akhir</span> 
                <input class="form-control tgl"  id="filter_usr_date_to" name="filter_date_to" maxlength="50"  placeholder="" type="text"  >
              </div> 
           </div> 

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_user_filter')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_user()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
  function call_upt_user(){
    var prov = $("#filter_usr_provinsi").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#filter_usr_upt").html(msg);        
                }
             }
          }); 
  }
  function filter_user() {
      $("#filter_pengguna").html("");
      var filter_user             = $("#filter_usr_username").val();
      var filter_nama             = $("#filter_usr_nama_lengkap").val();
      var filter_status           = $("#filter_usr_status").val();
      var filter_group_user       = $("#filter_usr_group_user").val();
      var filter_provinsi         = $("#filter_usr_provinsi").val();
      var filter_upt              = $("#filter_usr_upt").val();
      var filter_no_tlp           = $("#filter_usr_no_tlp").val();
      var filter_no_hp            = $("#filter_usr_no_hp").val();
      var filter_email            = $("#filter_usr_email").val();
      var filter_date_from        = $("#filter_usr_date_from").val();
      var filter_date_to          = $("#filter_usr_date_to").val();

      var param="";
      if(filter_user){
        param += " and us.username like '%"+filter_user.trim()+"%'";
      }if(filter_nama){
        param += " and us.nama_lengkap like '%"+filter_nama.trim()+"%'";
      }if(filter_status){
        param += " and us.status='"+filter_status.trim()+"'";
      }if(filter_group_user){
        param += " and us.group_user='"+filter_group_user.trim()+"'";
      }if(filter_provinsi){
        param += " and us.upt_provinsi='"+filter_provinsi.trim()+"'";
      }if(filter_upt){
        param += " and us.kode_upt='"+filter_upt.trim()+"'";
      }if(filter_no_tlp){
        param += " and us.no_tlp  like '%"+filter_no_tlp.trim()+"%'";
      }if(filter_no_hp){
        param += " and us.no_hp  like '%"+filter_no_hp.trim()+"%'";
      }if(filter_email){
        param += " and us.email  like '%"+filter_email.trim()+"%'";
      }

      if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and us.created_date between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and us.created_date='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and us.created_date='"+date_to_default(filter_date_to.trim())+"' ";
      }
      $("#DaftarPenggunaPencarian").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>daftar_pengguna/table_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_pengguna').html(data);
              }
          });
      $('#Filter_User').modal('hide');
   } 
</script>
<!-- End User  Filter -->

<!--  Start SIMS Filter -->
<div id="Filter_Sims" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_sims_filter" method="POST">

            <div class="row">  
              <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label">No SPP</span> 
                <input  class="form-control"  id="filter_sims_no_spp" name="filter_sims_no_spp"  maxlength="50" type="text"  >
              </div>
              <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label">No Client License</span>
                <input class="form-control" id="filter_sims_no_client" name="filter_sims_no_client"  maxlength="50" type="text"  >
              </div>

              <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label">No Aplikasi</span>
                <input class="form-control"  id="filter_sims_no_aplikasi" name="filter_sims_no_aplikasi" maxlength="50" type="text"  >
              </div>
            </div>

            <div class="row">  
                <div class="col-md-8 form-group form-box col-xs-12">
                  <span class="label">Perusahaan</span> 
                  <div class="input-group">
                    <input class="form-control" id="filter_sims_company" name="filter_sims_company"   placeholder="" type="text"  >
                    <div class="input-group-btn">
                      <button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
                  </div>
                </div>
                <input  id="filter_sims_kode_perusahaan" name="filter_sims_kode_perusahaan" type="hidden" >
              </div>
              <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label">Request Reff</span>
                <input class="form-control"  id="filter_sims_request_reff" name="filter_sims_request_reff" maxlength="50"   placeholder="" type="text"  >
              </div>
            </div>

            <div class="row">  
                <div class="col-md-4 form-group form-box col-xs-12">
                  <span class="label">License State</span>
                  <input class="form-control"  id="filter_sims_licence_state" name="filter_sims_licence_state"  maxlength="50"  placeholder="" type="text"  >
                </div>
                <div class="col-md-4 form-group form-box col-xs-12">
                  <span class="label">Tagihan</span>
                  <input class="form-control"  id="filter_sims_tagihan" name="filter_sims_tagihan"  onkeydown="return numbersonly(this,event);" onkeyup="javascript:tandaPemisahTitik(this);"   type="text"  >
                </div>
                <div class="col-md-4 form-group form-box col-xs-12">
                  <span class="label">Tgl Begin</span>
                  <input class="form-control tgl"  id="filter_sims_tgl_begin" name="filter_sims_tgl_begin"  type="text"  >
                </div>
            </div>

            <div class="row">  
                <div class="col-md-4 form-group form-box col-xs-12">
                  <span class="label">Status</span>
                  <input class="form-control"  id="filter_sims_status" name="filter_sims_status" maxlength="50"  type="text"  >
                </div>
                <div class="col-md-4 form-group form-box col-xs-12">
                  <span class="label">Status Izin</span>
                  <input class="form-control"  id="filter_sims_status_izin" name="filter_sims_status_izin"  maxlength="50" placeholder="" type="text"  >
                </div>
                <div class="col-md-4 form-group form-box col-xs-12">
                  <span class="label">Kategori SPP</span>
                  <input class="form-control"  id="filter_sims_kategori_spp" name="filter_sims_kategori_spp"  maxlength="50" placeholder="" type="text"  >
                </div>
            </div>

            <div class="row">  
              <div class="col-md-6 form-group form-box col-xs-12">
                <span class="label">Service</span>
                <input class="form-control"  id="filter_sims_service" name="filter_sims_service" maxlength="50"  placeholder="" type="text"  >
              </div>
              <div class="col-md-6 form-group form-box col-xs-12">
                <span class="label">Sub Service</span>
                <input class="form-control"  id="filter_sims_subservice" name="filter_sims_subservice" maxlength="50"  placeholder="" type="text"  >
              </div> 
           </div> 

           <div class="row">
              <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label">Created Date</span><br/>
                <span class="label" style="font-size: 13px;">Mulai</span> 
                <input class="form-control tgl"  id="filter_sims_date_from" name="filter_sims_date_from" maxlength="50"  placeholder="" type="text"  >
              </div>
              <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label"></span><br/>
                <span class="label" style="font-size: 13px;">Akhir</span> 
                <input class="form-control tgl"  id="filter_sims_date_to" name="filter_sims_date_to" maxlength="50"  placeholder="" type="text"  >
              </div> 
           </div> 

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_sims_filter')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_sims()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
  $('#filter_sims_company').typeahead({
      hint: true,
      highlight: true,
      source: function (query, process) {
          $.ajax({
              url: '<?php echo $basepath ?>ajax/reqCompany',
              type: 'POST',
              dataType: 'JSON',
              data: 'query='+$('#filter_sims_company').val(),
              success: function(data) {
                  var results = data.map(function(item) {
                      var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
                      return someItem;
                  });
                  return process(results);
              }
          });
      },
      afterSelect: function(item) {
          this.$element[0].value = item.company_name
      },
      updater: function(item) {
          $('#filter_kode_perusahaan').val(item.company_id);
          return item;
      }
    });
  function filter_sims() {
      $("#filter_sims").html("");

      var filter_nama_perusahaan  = $("#filter_sims_company").val();
      var filter_kode_perusahaan  = $("#filter_sims_kode_perusahaan").val();


      var filter_date_from        = $("#filter_sims_date_from").val();
      var filter_date_to          = $("#filter_sims_date_to").val();

      var filter_no_spp           = $("#filter_sims_no_spp").val();
      var filter_no_client        = $("#filter_sims_no_client").val();
      var filter_request_reff     = $("#filter_sims_request_reff").val();
      var filter_licence_state    = $("#filter_sims_licence_state").val();
      var filter_tagihan          = $("#filter_sims_tagihan").val();
      var filter_tgl_begin        = $("#filter_sims_tgl_begin").val();
      var filter_status           = $("#filter_sims_status").val();
      var filter_status_izin      = $("#filter_sims_status_izin").val();
      var filter_kategori_spp     = $("#filter_sims_kategori_spp").val();
      var filter_service          = $("#filter_sims_service").val();
      var filter_subservice       = $("#filter_sims_subservice").val();

       var param="";
      if(filter_no_spp){
        param += "&no_spp="+filter_no_spp.trim();
      }if(filter_request_reff){
        param += "&request_reff="+filter_request_reff.trim();
      }if(filter_licence_state){
        param += "&licence_state="+filter_licence_state.trim();
      }if(filter_tagihan){
        param += "&tagihan="+filter_tagihan.trim();
      }if(filter_tgl_begin){
        param += "&tgl_begin="+filter_tgl_begin.trim();
      }if(filter_status){
        param += "&status="+filter_status.trim();
      }if(filter_status_izin){
        param += "&status_izin="+filter_status_izin.trim();
      }if(filter_kategori_spp){
        param += "&kategori_spp="+filter_kategori_spp.trim();
      }if(filter_service){
        param += "&service="+filter_service.trim();
      }if(filter_subservice){
        param += "&subservice="+filter_subservice.trim();
      }if(filter_nama_perusahaan && filter_kode_perusahaan){
        param += "&kode_perusahaan="+filter_kode_perusahaan.trim();
      }

      if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += "&filter_date_from="+filter_date_from.trim()+"&filter_date_to="+filter_date_to.trim();
        }
      }
      else if(filter_date_from){
          param += "&filter_date_from="+filter_date_from.trim();
      }
      else if(filter_date_to){
          param += "&filter_date_to="+filter_date_to.trim();
      }
       $.ajax({
              url: '<?php echo $basepath ?>sims/table_sims_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_sims').html(data);
              }
          });
      $('#Filter_Sims').modal('hide');
   } 
</script>
<!-- End SIMS Filter -->

<!-- Start Print SIMS -->
<script type="text/javascript">
   function print_laporan_sims(tipe){
                          var filter_nama_perusahaan  = $("#filter_sims_company").val();
                          var filter_kode_perusahaan  = $("#filter_sims_kode_perusahaan").val();


                          var filter_date_from        = $("#filter_sims_date_from").val();
                          var filter_date_to          = $("#filter_sims_date_to").val();

                          var filter_no_spp           = $("#filter_sims_no_spp").val();
                          var filter_no_client        = $("#filter_sims_no_client").val();
                          var filter_request_reff     = $("#filter_sims_request_reff").val();
                          var filter_licence_state    = $("#filter_sims_licence_state").val();
                          var filter_tagihan          = $("#filter_sims_tagihan").val();
                          var filter_tgl_begin        = $("#filter_sims_tgl_begin").val();
                          var filter_status           = $("#filter_sims_status").val();
                          var filter_status_izin      = $("#filter_sims_status_izin").val();
                          var filter_kategori_spp     = $("#filter_sims_kategori_spp").val();
                          var filter_service          = $("#filter_sims_service").val();
                          var filter_subservice       = $("#filter_sims_subservice").val();


                        var param="";
                        if(filter_no_spp){
                          param += " and sm.no_spp='"+filter_no_spp+"' ";
                        }if(filter_request_reff){
                          param += " and sm.request_reff='"+filter_request_reff+"' ";
                        }if(filter_licence_state){
                          param += " and sm.licence_state='"+filter_licence_state+"' ";
                        }if(filter_tagihan){
                          param += " and sm.tagihan='"+filter_tagihan+"' ";
                        }if(filter_tgl_begin){
                          param += " and sm.tgl_begin like '"+filter_tgl_begin+"%' ";
                        }if(filter_status){
                          param += " and sm.status='"+filter_status+"' ";
                        }if(filter_status_izin){
                          param += " and sm.status_izin='"+filter_status_izin+"' ";
                        }if(filter_kategori_spp){ 
                          param += " and sm.kategori_spp='"+filter_kategori_spp+"' ";
                        }if(filter_service){
                          param += " and sm.service='"+filter_service+"' ";
                        }if(filter_subservice){
                          param += " and sm.subservice='"+filter_subservice+"' ";
                        }if(filter_nama_perusahaan && filter_kode_perusahaan){
                          param += " and sm.kode_perusahaan='"+filter_kode_perusahaan+"' ";
                        }

                        if(filter_date_from && filter_date_to){
                          date1 = new Date(filter_date_from); 
                          date2 = new Date(filter_date_to);

                          var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
                          var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
                          if(date1 > date2){
                          }
                          else {
                             param += "&filter_date_from="+filter_date_from.trim()+"&filter_date_to="+filter_date_to.trim();
                             param += " and sm.created_date BETWEEN '"+date_to_default(filter_date_from.trim())+"' AND '"+date_to_default(filter_date_to.trim())+"' ";
                          }
                        }
                        else if(filter_date_from){
                            param += " and sm.created_date like '"+date_to_default(filter_date_from)+"%' ";
                        }
                        else if(filter_date_to){
                            param += " and sm.created_date like '"+date_to_default(filter_date_to)+"%' ";
                        }


                          var form = document.createElement("form");
                          form.setAttribute("method", "POST");
                          if(tipe=="print"){
                           form.setAttribute("target", "_blank");
                          }
                          form.setAttribute("action", "<?php echo $basepath ?>sims/cetak");
                                 
                              var hiddenField = document.createElement("input");
                              hiddenField.setAttribute("type", "hidden");
                              hiddenField.setAttribute("name", "param");
                              hiddenField.setAttribute("value", btoa(param));

                              var hiddenField2 = document.createElement("input");
                              hiddenField2.setAttribute("type", "hidden");
                              hiddenField2.setAttribute("name", "tipe");
                              hiddenField2.setAttribute("value", tipe);

                              form.appendChild(hiddenField);
                              form.appendChild(hiddenField2);

                          document.body.appendChild(form);
                          form.submit();
                }
</script>
<!-- End Print SIMS -->


<!-- Start Import SIMS -->
<div id="Import_Sims" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_Sims" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>

         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_sims.xls" download>Download Sample File</a><br/><br/>
         <span style="font-size: 14px;">File Import Membutuhkan Kode Perusahaan</span><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>company">List Data Perusahaan</a><br/>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        <form>
      </div>
    </div>
</div>

<script type="text/javascript">
  $("#DoImport_Sims").on("submit", function (event) {
    event.preventDefault();
        do_act('DoImport_Sims','sims/import','no_refresh','Import File sims','Apakah anda ingin import file SIMS ini ?','info');
    });
</script>
<!-- End Import SIMS -->

<!--  Start Perusahaan Filter -->
<div id="Filter_Perusahaan" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_perusahaan_filter" method="POST">

            <div class="row">  
              <div class="col-md-6 form-group form-box col-xs-12">
                  <span class="label">No Klien Licence</span> 
                  <input  class="form-control" maxlength="30" id="filter_company_id" name="company_id"   placeholder="" type="text" required >
                </div>

                <div class="col-md-6 form-group form-box col-xs-12">
                  <span class="label">Nama Perusahaan</span> 
                  <input  class="form-control" maxlength="255" id="filter_nama_perusahaan" name="nama_perusahaan"   placeholder="" type="text" required >
                </div>
            </div>

            <div class="row"> 
              <div class="col-md-6 form-group form-box col-xs-12">
                <span class="label">No Tlp</span> 
                <input  class="form-control" maxlength="25" id="filter_co_no_tlp" name="no_tlp"   placeholder="" type="text"  >
              </div>

              <div class="col-md-6 form-group form-box col-xs-12">
                <span class="label">No Hp</span> 
                <input  class="form-control" maxlength="25" id="filter_co_no_hp" name="no_hp"   placeholder="" type="text"  >
              </div>
            </div>

             <div class="row"> 
              <div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" >
                <span class="label">Provinsi</span>  
                <select class="form-control"  id="filter_co_provinsi" name="id_prov" onchange="call_kabkot_company()"  >
                  <option value="">Pilih Provinsi</option>
                  <?php 
                    $data_shf = $gen_model->GetWhere('ms_provinsi');
                      while($list = $data_shf->FetchRow()){
                        foreach($list as $key=>$val){
                                    $key=strtolower($key);
                                    $$key=$val;
                                  }  
                    ?>
                  <option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
             <div class="row"> 
              <div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
                <span class="label">Nama Kabupaten/Kota</span> 
                <select class="form-control"  id="filter_co_kabkot" name="kabkot"  >
                  <option value="">Pilih Kabupaten/Kota</option>
                </select>
              </div>
            </div>

             <div class="row"> 
              <div class="col-md-12 form-group form-box col-xs-12">
                <span class="label">Alamat</span> 
                <textarea  class="form-control"  id="filter_co_alamat" name="alamat" ></textarea>
              </div>
              </div>

           <div class="row">
              <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label">Created Date</span><br/>
                <span class="label" style="font-size: 13px;">Mulai</span> 
                <input class="form-control tgl"  id="filter_co_date_from" name="filter_co_date_from" maxlength="50"  placeholder="" type="text"  >
              </div>
              <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label"></span><br/>
                <span class="label" style="font-size: 13px;">Akhir</span> 
                <input class="form-control tgl"  id="filter_co_date_to" name="filter_co_date_to" maxlength="50"  placeholder="" type="text"  >
              </div> 
           </div> 

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_perusahaan_filter')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_perusahaan()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
  function call_kabkot_company(){
    var prov = $("#filter_co_provinsi").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_kabkot",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#filter_co_kabkot").html(msg);        
                }
             }
          }); 
  }
  function filter_perusahaan() {
      $("#filter_company").html("");

      var filter_nama_perusahaan  = $("#filter_nama_perusahaan").val();
      var filter_kode_perusahaan  = $("#filter_company_id").val();
      var filter_date_from        = $("#filter_co_date_from").val();
      var filter_date_to          = $("#filter_co_date_to").val();
      var filter_co_alamat        = $("#filter_co_alamat").val();
      var filter_provinsi         = $("#filter_co_provinsi").val();
      var filter_kabkot           = $("#filter_co_kabkot").val();
      var filter_no_tlp           = $("#filter_co_no_tlp").val();
      var filter_no_hp            = $("#filter_co_no_hp").val();

      var param="";
      if(filter_nama_perusahaan){
        param += " and cp.company_name like '%"+filter_nama_perusahaan.trim()+"%' ";
      }if(filter_kode_perusahaan){
        param += " and cp.company_id like '%"+filter_kode_perusahaan.trim()+"%' ";
      }if(filter_co_alamat){
        param += " and cp.alamat like '%"+filter_co_alamat.trim()+"%' ";
      }if(filter_no_tlp){
        param += " and cp.no_tlp_company like '%"+filter_no_tlp.trim()+"%' ";
      }if(filter_no_hp){
        param += " and cp.no_hp_company like '%"+filter_no_hp.trim()+"%' ";
      }if(filter_provinsi){
        param += " and cp.id_provinsi ='"+filter_provinsi.trim()+"' ";
      }if(filter_kabkot){
        param += " and cp.id_kabkot ='"+filter_upt.trim()+"' ";
      }


       if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and cp.created_date between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and cp.created_date='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and cp.created_date='"+date_to_default(filter_date_to.trim())+"' ";
      }
       $("#CompanyPencarian").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>company/table_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_company').html(data);
              }
          });
      $('#Filter_Perusahaan').modal('hide');
   } 
</script>
<!-- End Company Filter -->

<!--  Start UPT Filter -->
<div id="Filter_UPT" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_upt_filter" method="POST">

            <div class="row">  
                <div class="col-md-12 form-group form-box col-xs-12">
                  <span class="label">Nama UPT</span> 
                  <input  class="form-control" maxlength="255" id="filter_nama_upt" name="nama_perusahaan"   placeholder="" type="text" required >
                </div>
            </div>

            <div class="row"> 
              <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label">No Tlp</span> 
                <input  class="form-control" maxlength="150" id="filter_upt_no_tlp" name="no_tlp"   placeholder="" type="text"  >
              </div>

              <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label">No Fax</span> 
                <input  class="form-control" maxlength="150" id="filter_upt_no_fax" name="no_fax"   placeholder="" type="text"  >
              </div>

               <div class="col-md-4 form-group form-box col-xs-12">
                <span class="label">Email</span> 
                <input  class="form-control" maxlength="255" id="filter_upt_email" name="email"   placeholder="" type="text"  >
              </div>
            </div>

             <div class="row"> 
              <div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" >
                <span class="label">Provinsi</span>  
                <select class="form-control"  id="filter_upt_provinsi" name="id_prov" >
                  <option value="">Pilih Provinsi</option>
                  <?php 
                    $data_shf = $gen_model->GetWhere('ms_provinsi');
                      while($list = $data_shf->FetchRow()){
                        foreach($list as $key=>$val){
                                    $key=strtolower($key);
                                    $$key=$val;
                                  }  
                    ?>
                  <option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>

             <div class="row"> 
              <div class="col-md-12 form-group form-box col-xs-12">
                <span class="label">Alamat</span> 
                <textarea  class="form-control"  id="filter_upt_alamat" name="alamat" ></textarea>
              </div>
              </div>

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_upt_filter')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_upt()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
  function filter_upt() {
      $("#filter_upt").html("");

      var filter_nama_upt         = $("#filter_nama_upt").val();
      var filter_alamat           = $("#filter_upt_alamat").val();
      var filter_provinsi         = $("#filter_upt_provinsi").val();
      var filter_no_tlp           = $("#filter_upt_no_tlp").val();
      var filter_no_fax           = $("#filter_upt_no_fax").val();
      var filter_email            = $("#filter_upt_email").val();

      var param="";
      if(filter_nama_upt){
        param += " and upt.nama_upt like '%"+filter_nama_upt.trim()+"%' ";
      }if(filter_alamat){
        param += " and upt.alamat like '%"+filter_alamat.trim()+"%' ";
      }if(filter_no_tlp){
        param += " and upt.no_tlp like '%"+filter_no_tlp.trim()+"%' ";
      }if(filter_no_fax){
        param += " and upt.no_fax like '%"+filter_no_fax.trim()+"%' ";
      }if(filter_provinsi){
        param += " and upt.id_prov ='"+filter_provinsi.trim()+"' ";
      }if(filter_email){
        param += " and upt.email ='"+filter_email.trim()+"' ";
      }


       $("#UPTPencarian").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>upt/table_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_upt').html(data);
              }
          });
      $('#Filter_UPT').modal('hide');
   } 
</script>
<!-- End UPT Filter -->