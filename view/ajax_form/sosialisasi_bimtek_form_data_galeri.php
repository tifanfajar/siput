<?php  
$web = $gen_model->GetOneRow('ms_web'); 
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<div class="row">

		<input id="kode_galeri" name="kode_galeri"  type="hidden">
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Kegiatan</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="kegiatan_galeri" name="kegiatan"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Lampiran</span> 
			<?php if($activity=="do_add"){  ?> 
				<span class="required">*</span> 
			<?php } 
			if($activity!="do_detail"){  ?>
				<input  class="form-control"  id="lampiran" name="lampiran"  type="file" <?php echo $readonly ?> <?php if($activity=="do_add"){ echo "required"; } ?> >
			<?php } 
			if($activity=="do_detail" or  $activity=="do_edit"){ 
				echo '<br/><a style="text-decoration:none" id="lampiran_galeri" href="#" download>Download<a/>'; 
			    } ?>
		</div>

		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Deskripsi</span> 
			<textarea  class="form-control"  id="deskripsi_galeri" name="deskripsi" <?php echo $readonly ?>></textarea>
		</div>

		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Keterangan</span> 
			<textarea  class="form-control"  id="keterangan_galeri" name="keterangan" <?php echo $readonly ?>></textarea>
		</div>

		<input  value="<?php echo $wilayah['id_prov'] ?>" id="id_prov_galeri" name="id_prov" type="hidden" required>
		<input  value="<?php echo $map_id ?>" id="map_id_galeri" name="map_id" type="hidden" required>
		<input  value="<?php echo $upt ?>" id="kode_upt_galeri" name="loket_upt" type="hidden" required>
		
		
		<div class="col-12">
			<div class="headTitle"></div>
			<?php  if($activity!="do_detail"){ ?>
				<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php } 
			if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','sosialisasi_bimtek_galeri/add','no_refresh','Simpan Data Galeri','Apakah anda ingin menyimpan data Galeri ?','info','refresh_table');
    });
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','sosialisasi_bimtek_galeri/update','no_refresh','Ubah Data Galeri','Apakah anda ingin mengubah data Galeri ?','warning','refresh_table');
	});
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});
</script>
