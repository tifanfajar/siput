<?php  
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<input  id="id_jenis_sb" name="id_jenis_sb" type="hidden" required>
	<div class="row">
		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Perizinan</span> <span class="required">*</span>
			<select class="form-control" id="jenis" name="jenis"   placeholder="" required <?php echo $readonly ?>>
				<option value="">Pilih Perizinan</option>
				<?php 
					$data_perizinan = $db->SelectLimit("select DISTINCT(id_jenis),jenis from ms_group_layanan where status='1'"); 
					while($list = $data_perizinan->FetchRow()){
								foreach($list as $key=>$val){
		                        $key=strtolower($key);
		                        $$key=$val;
		                      }  
				?>	
				<option value="<?php echo $id_jenis ?>"><?php echo $jenis ?></option>
			<?php } ?>
			</select>
		</div>
		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Sub Perizinan</span> <span class="required">*</span>
			<input class="form-control" maxlength="50" id="jenis_sb" name="jenis_sb"  placeholder=""  type="text" required>
		</div>

		<div class="col-12">
			<div class="headTitle"></div>
			<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php  if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','grp_layanan/add_sb','no_refresh','Simpan Data Sub Group Perizinan','Apakah anda ingin menyimpan data Sub Group Perizinan ?','info','refresh_table');
    });
	
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','grp_layanan/update_sb','no_refresh','Ubah Data Sub Group Perizinan','Apakah anda ingin mengubah data Sub Group Perizinan ?','warning','refresh_table');
	});
</script>
