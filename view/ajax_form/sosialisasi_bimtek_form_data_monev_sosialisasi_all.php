<?php  
$web = $gen_model->GetOneRow('ms_web'); 
$readonly="";
if($activity=="do_detail" or $activity=="do_add"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<div class="row">

		<input id="kode_monev_sosialisasi" name="kode_monev_sosialisasi"  type="hidden">
		<?php if($activity!="do_detail"){  ?> 
			<div class="col-md-12 form-group form-box col-xs-6">
				<span class="label">Tanggal Pelaksanaan / Jenis Kegiatan / Tema</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php } ?>
				<input  class="form-control" id="suggest" name="suggest"  type="text" required <?php if($activity=="do_detail"){ echo "readonly"; } ?>>
			</div>
		<?php } ?>
		<div class="col-md-6 form-group form-box col-xs-6" id="div_upt_provinsi" >
			<span class="label">Jenis Kegiatan</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control"  id="kegiatan_mnv_so" name="kegiatan" required <?php echo $readonly ?>>
				<option value="">Pilih Jenis Kegiatan</option>
				<?php 
					$data_mtr = $gen_model->GetWhere('ms_kegiatan');
						while($list = $data_mtr->FetchRow()){
							foreach($list as $key=>$val){
	                        $key=strtolower($key);
	                        $$key=$val;
	                      }  
					?>
				<option value="<?php echo $kode_kegiatan ?>"><?php echo $kegiatan ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Tanggal Pelaksanaan</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control tgl" id="tgl_pelaksanaan_mnv_so" name="tgl_pelaksanaan"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Tema</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" id="tema_mnv_so" name="tema"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Tempat</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="100" id="tempat_mnv_so" name="tempat"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Target Peserta</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="50" id="target_peserta_mnv_so" name="target_peserta"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Jumlah Peserta</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control"  onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="jumlah_peserta_mnv_so" name="jumlah_peserta"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Anggaran</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control"  id="anggaran_mnv_so" name="anggaran"  onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);"  placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Lampiran</span> 
			<?php if($activity=="do_add"){  ?> 
				<span class="required">*</span> 
			<?php } 
			if($activity!="do_detail"){  ?>
				<input  class="form-control"  id="lampiran" name="lampiran"  type="file" <?php echo $readonly ?> <?php if($activity=="do_add"){ echo "required"; } ?> >
			<?php } 
			if($activity=="do_detail" or  $activity=="do_edit"){ 
				echo '<br/><a style="text-decoration:none" id="lampiran_mnv_so" href="#" download>Download<a/>'; 
			    } ?>
		</div>
		<div id="data_narasumber" style="width:100%"></div>
		<?php if($activity!="do_detail"){  ?>
			<div class="col-md-12 form-group form-box col-xs-12">
				<center>
					<button id='btn_tambah' <?php if($activity=="do_add"){  echo 'disabled'; }?> onclick="tambah_narasumber()" type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> &nbsp; Tambah</button>
				</center>
			</div>
		<?php } ?>
		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Keterangan</span> 
			<textarea  class="form-control"  id="keterangan_mnv_so" name="keterangan" <?php echo $readonly ?>></textarea>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" >
			<span class="label">Provinsi</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control" onchange="call_upt()" id="upt_provinsi" name="id_prov" required <?php echo $readonly ?>>
				<option value="">Pilih Provinsi</option>
				<option value="All">Semua Provinsi</option>
				<?php 
					$data_shf = $gen_model->GetWhere('ms_provinsi');
						while($list = $data_shf->FetchRow()){
							foreach($list as $key=>$val){
	                        $key=strtolower($key);
	                        $$key=$val;
	                      }  
					?>
				<option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
				<?php } ?>
			</select>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
			<span class="label">Nama UPT</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control"  id="kode_upt" name="loket_upt" required <?php echo $readonly ?>>
				<option value="">Pilih UPT</option>
			</select>
		</div>
		
		<div class="col-12">
			<div class="headTitle"></div>
			<?php  if($activity!="do_detail"){ ?>
				<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php } 
			if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	 		var Ids = 1;
	<?php  if($activity=="do_edit"){ ?>
			 setTimeout(function(){ 
               Ids = $('.dinamis_narasumber').length+1;  
             }, 1000);
	<?php }  ?>
	$('#suggest').typeahead({
			hint: true,
			highlight: true,
			source: function (query, process) {
			    $.ajax({
			        url: '<?php echo $basepath ?>ajax/reqDataRencanaSosialisasi',
			        type: 'POST',
			        dataType: 'JSON',
			        data: 'query='+$('#suggest').val(),
			        success: function(data) {
			            var results = data.map(function(item) {
			                var someItem = {name:item.label,myvalue:item.value,tgl_pelaksanaan:item.tgl_pelaksanaan,tema:item.tema,jenis_kegiatan:item.jenis_kegiatan,tempat:item.tempat,kode_upt:item.kode_upt,id_prov:item.id_prov,jumlah_peserta:item.jumlah_peserta,anggaran:item.anggaran,target_peserta:item.target_peserta,narasumber:item.narasumber,keterangan:item.keterangan};
			                return someItem;
			            });
			            return process(results);
			        }
			    });
			},
			afterSelect: function(item) {
		    	//this.$element[0].value = item.label
			},
			updater: function(item) {
			    $('#suggest').val(item.name);
			    $('#tgl_pelaksanaan_mnv_so').val(item.tgl_pelaksanaan);
			    $('#tema_mnv_so').val(item.tema);
			    $('#tempat_mnv_so').val(item.tempat);
			    $('#kegiatan_mnv_so').val(item.jenis_kegiatan);
			    $('#target_peserta_mnv_so').val(item.target_peserta);
			    $('#jumlah_peserta_mnv_so').val(item.jumlah_peserta);
			    $('#anggaran_mnv_so').val(item.anggaran);
			    $('#keterangan_mnv_so').val(item.keterangan);
			    $('#upt_provinsi').val(item.id_prov);

			    $.ajax({
                     type: "POST",
                     dataType: "html",
                     url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
                     data: "provinsi="+item.id_prov,
                     success: function(msg){
                        if(msg){
                          $("#kode_upt").html(msg);
                          setTimeout(function(){ 
                               $("#kode_upt").val(item.kode_upt);        
                          }, 500);
                         
                        }
                     }
                  }); 
			    $(':input').removeAttr('readonly');
			    $('#btn_tambah').prop("disabled", false);
			    $("#data_narasumber").html("");
			    var str = item.narasumber;
			    if(str){
			    	Ids = 1;
	                var str_array = str.split('|');
	                for(var i = 0; i < str_array.length; i++) {
	                   str_array[i] = str_array[i].replace(/^\s*/, "").replace(/\s*$/, "");
	                    var  html ='<div style="margin-right:0px;margin-left:0px" class="row dinamis_narasumber" id="narasumber_'+i+'" >';
	                         html+='<div class="col-md-10 form-group form-box col-xs-10">';
	                           html+='<span class="label">Narasumber</span> <span class="required">*</span>';
	                           html+='<input  class="form-control" value="'+str_array[i]+'"  name="narasumber[]"   placeholder="" type="text" required>';
	                         html+='</div>';
	                         html+='<div  class="col-md-2 form-group form-box col-xs-2">';
	                          html+='<br/><button type="button"  onClick="hapus_narasumber(\''+i+'\');"  class="btn btn-danger btn-sm">Hapus</button>';
	                         html+='</div></div>';
	                         $("#data_narasumber").append(html);
	                         Ids++;
	                }
                }
                else {

                	var i = 1;
                	 var  html ='<div style="margin-right:0px;margin-left:0px" class="row dinamis_narasumber" id="narasumber_'+i+'" >';
	                         html+='<div class="col-md-10 form-group form-box col-xs-10">';
	                           html+='<span class="label">Narasumber</span> <span class="required">*</span>';
	                           html+='<input  class="form-control"   name="narasumber[]"   placeholder="" type="text" required>';
	                         html+='</div>';
	                         html+='<div  class="col-md-2 form-group form-box col-xs-2">';
	                          html+='<br/><button type="button"  onClick="hapus_narasumber(\''+i+'\');"  class="btn btn-danger btn-sm">Hapus</button>';
	                         html+='</div></div>';
	                         $("#data_narasumber").append(html);
	                Ids = Ids+1;
                }
			    return item;
			}
		});
	<?php  if($activity=="do_edit"){ ?>
			 setTimeout(function(){ 
               Ids = $('.dinamis_narasumber').length+1;  
             }, 1000);
	<?php }  ?>
	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','sosialisasi_bimtek_monev_sosialisasi/add','no_refresh','Simpan Data Monev Sosialisasi','Apakah anda ingin menyimpan data Monev Sosialisasi ?','info','refresh_table');
    });
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','sosialisasi_bimtek_monev_sosialisasi/update','no_refresh','Ubah Data Monev Sosialisasi','Apakah anda ingin mengubah data Monev Sosialisasi ?','warning','refresh_table');
	});
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});
	function tambah_narasumber(){
		var  html ='<div style="margin-right:0px;margin-left:0px" class="row" id="narasumber_'+Ids+'" >';
					 html+='<div class="col-md-10 form-group form-box col-xs-10">';
						 html+='<span class="label">Narasumber</span> <span class="required">*</span>';
						 html+='<input  class="form-control"   name="narasumber[]"   placeholder="" type="text" required>';
					 html+='</div>';
					 html+='<div  class="col-md-2 form-group form-box col-xs-2">';
					 	html+='<br/><button type="button"  onClick="hapus_narasumber(\''+Ids+'\');"  class="btn btn-danger btn-sm">Hapus</button>';
					 html+='</div></div>';
				$("#data_narasumber").append(html);
				Ids++;
	}
	function hapus_narasumber(Ids){
		$('#narasumber_'+Ids).slideUp('slow');
		setTimeout(function(){ $('#narasumber_'+Ids).remove(); },500);
	}
	function call_upt(){
		var prov = $("#upt_provinsi").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#kode_upt").html(msg);        
                }
             }
          }); 
	}
</script>
