<?php  
$web = $gen_model->GetOneRow('ms_web'); 
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>_loket" autocomplete="off" enctype="multipart/form-data">
	<div class="row">
        <input  type="hidden"  id="cmp_id_temp" name="company_id_temp">
		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Nama Perusahaan</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="company_name" name="nama_perusahaan"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>

		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">No Tlp</span> 
			<input  class="form-control" maxlength="25" id="co_no_tlp" name="no_tlp"   placeholder="" type="text"  <?php echo $readonly ?>>
		</div>

		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">No Hp</span> 
			<input  class="form-control" maxlength="25" id="co_no_hp" name="no_hp"   placeholder="" type="text"  <?php echo $readonly ?>>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" >
			<span class="label">Provinsi</span>  
			<select class="form-control"  id="co_provinsi" name="id_prov" onchange="call_upt()"  <?php echo $readonly ?>>
				<option value="">Pilih Provinsi</option>
				<?php 
					$data_shf = $gen_model->GetWhere('ms_provinsi');
						while($list = $data_shf->FetchRow()){
							foreach($list as $key=>$val){
	                        $key=strtolower($key);
	                        $$key=$val;
	                      }  
					?>
				<option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
				<?php } ?>
			</select>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
			<span class="label">Nama Kabupaten/Kota</span> 
			<select class="form-control"  id="co_kabkot" name="kabkot"  <?php echo $readonly ?>>
				<option value="">Pilih Kabupaten/Kota</option>
			</select>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Alamat</span> 
			<textarea  class="form-control"  id="co_alamat" name="alamat" <?php echo $readonly ?>></textarea>
		</div>

		
		<div class="col-12">
			<div class="headTitle"></div>
			<?php  if($activity!="do_detail"){ ?>
				<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php } 
			if($activity=="do_add"){ ?>
			<!--<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>-->
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#do_add_loket").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add_loket','company/add_loket','no_refresh','Simpan Data Perusahaan','Apakah anda ingin menyimpan data Perusahaan ?','info','close_form_company_loket');
    });
	$("#do_edit_loket").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit_loket','company/update_loket','no_refresh','Ubah Data Perusahaan','Apakah anda ingin mengubah data Perusahaan ?','warning','close_form_company_loket');
	});
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});
	function call_upt(){
		var prov = $("#co_provinsi").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_kabkot",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#co_kabkot").html(msg);        
                }
             }
          }); 
	}
</script>
