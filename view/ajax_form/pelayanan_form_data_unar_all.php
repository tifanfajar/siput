<?php  
$web = $gen_model->GetOneRow('ms_web'); 
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>

<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<div class="row">


		<div class="col-md-12 form-group form-box col-xs-12">
				<center style="font-weight:bold;font-weight:50px">Tanggal Pelaksanaan UNAR</center>
		</div>



		<input id="kode_unar" name="kode_unar"  type="hidden">
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Tanggal Pelaksanaan Ujian</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input class="form-control <?php echo ($activity=="do_detail" ? '' : 'tgl' ) ?>" id="tgl_unar" name="tgl_unar"  readonly style="<?php echo ($activity=="do_detail" ? '' : 'background-color:white' ) ?>" type="text" required  >
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Lokasi Ujian</span><?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="lokasi_ujian" name="lokasi_ujian"   placeholder="" type="text" required  <?php echo $readonly ?>>
		</div>

	
		<div class="col-md-12 form-group form-box col-xs-12">
				<center style="font-weight:bold;font-weight:50px">Jumlah Peserta</center>
		</div>

		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">YD</span>
			<input onkeypress='return only_number_and_dot(event);' class="form-control" id="jumlah_yd" name="jumlah_yd"    type="number"  <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">YB</span> 
			<input onkeypress='return only_number_and_dot(event);' class="form-control" id="jumlah_yb" name="jumlah_yb"    type="number"  <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">YC</span>
			<input onkeypress='return only_number_and_dot(event);' class="form-control" id="jumlah_yc" name="jumlah_yc"   type="number"  <?php echo $readonly ?>>
		</div>


	
		<div class="col-md-12 form-group form-box col-xs-12">
				<center style="font-weight:bold;font-weight:50px">Lulus</center>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">YD</span>
			<input onkeypress='return only_number_and_dot(event);' class="form-control" id="lulus_yd" name="lulus_yd"   type="number"  <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">YB</span> 
			<input onkeypress='return only_number_and_dot(event);' class="form-control" id="lulus_yb" name="lulus_yb"   type="number"  <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">YC</span>
			<input onkeypress='return only_number_and_dot(event);' class="form-control" id="lulus_yc" name="lulus_yc"   type="number"  <?php echo $readonly ?>>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12">
				<center style="font-weight:bold;font-weight:50px">Tidak Lulus</center>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">YD</span>
			<input onkeypress='return only_number_and_dot(event);' class="form-control" id="tdk_lulus_yd" name="tdk_lulus_yd"   type="number"  <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">YB</span> 
			<input onkeypress='return only_number_and_dot(event);' class="form-control" id="tdk_lulus_yb" name="tdk_lulus_yb"    type="number"  <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">YC</span>
			<input onkeypress='return only_number_and_dot(event);' class="form-control" id="tdk_lulus_yc" name="tdk_lulus_yc"   type="number"  <?php echo $readonly ?>>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Keterangan</span>
			<textarea class="form-control" id="keterangan" name="keterangan"  rows="3" <?php echo $readonly ?>></textarea>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" >
			<span class="label">Provinsi</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control" onchange="call_upt()" id="upt_provinsi" name="id_prov" required <?php echo $readonly ?>>
				<option value="">Pilih Provinsi</option>
				<option value="All">Semua Provinsi</option>
				<option value="Only_Admin">Hanya Admin</option>
				<?php 
					$data_shf = $gen_model->GetWhere('ms_provinsi');
						while($list = $data_shf->FetchRow()){
							foreach($list as $key=>$val){
	                        $key=strtolower($key);
	                        $$key=$val;
	                      }  
					?>
				<option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
				<?php } ?>
			</select>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
			<span class="label">Kabupaten / Kota</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control"  id="kabkot" name="kabkot" required <?php echo $readonly ?>>
				<option value="">Pilih Kabupaten / Kota</option>
			</select>
		</div>


		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
			<span class="label">Nama UPT</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control"  id="kode_upt" name="loket_upt" required <?php echo $readonly ?>>
				<option value="">Pilih UPT</option>
			</select>
		</div>


		<div class="col-12">
			<div class="headTitle"></div>
			<?php  if($activity!="do_detail"){ ?>
				<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php } 

			if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','pelayanan_unar/add','no_refresh','Simpan Data UNAR','Apakah anda ingin menyimpan data UNAR ?','info','refresh_table');
    });
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','pelayanan_unar/update','no_refresh','Ubah Data UNAR','Apakah anda ingin mengubah data UNAR ?','warning','refresh_table');
	});
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});

	function call_upt(){
		var prov = $("#upt_provinsi").val();
     
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_kabkot",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#kabkot").html(msg);        
                }
             }
          }); 

           $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#kode_upt").html(msg);        
                }
             }
          }); 

	}
</script>
