<?php  
$web = $gen_model->GetOneRow('ms_web'); 
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<div class="row">
		<input id="kode_revoke" name="kode_revoke"  type="hidden" >
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">No SPP</span> 
			<input  class="form-control"  id="no_spp" name="no_spp"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Service</span> 
			<input  class="form-control"  id="service" name="service"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">No Aplikasi</span>
			<input class="form-control"  id="no_aplikasi" name="no_aplikasi"  placeholder="" type="text" required <?php echo $readonly ?>>
		</div>

		<div class="col-md-8 form-group form-box col-xs-12">
			<span class="label">Nama Perusahaan</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<div class="input-group">
				<input class="form-control" id="nama_perusahaan" name="nama_perusahaan"   placeholder="" type="text" required <?php echo $readonly ?>>
				<div class="input-group-btn">
					<button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
				</div>
			</div>
			<input  id="kode_perusahaan" name="kode_perusahaan" type="hidden" >
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Tanggal ISR Dicabut</span>
			<input class="form-control <?php echo ($activity=="do_detail" ? '' : 'tgl' ) ?>"  id="tgl_isr_dicabut" name="tgl_isr_dicabut"   type="text" readonly style="<?php echo ($activity=="do_detail" ? '' : 'background-color:white' ) ?>">
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Tanggal Pengiriman</span> <span class="required">*</span>
			<input class="form-control <?php echo ($activity=="do_detail" ? '' : 'tgl' ) ?>" id="tgl_terima_waba" name="tgl_terima_waba"  readonly style="<?php echo ($activity=="do_detail" ? '' : 'background-color:white' ) ?>" type="text" required>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Metode</span> <span class="required">*</span>
			<select class="form-control" id="metode" name="metode" required <?php echo $readonly ?>>
				<option value="">Pilih Metode</option>
				<?php 
					$data_mtd = $db->SelectLimit("select * from ms_metode where status='1'"); 
					while($list = $data_mtd->FetchRow()){
								foreach($list as $key=>$val){
		                        $key=strtolower($key);
		                        $$key=$val;
		                      }  
				?>	
				<option value="<?php echo $id_metode ?>"><?php echo $metode ?></option>
			<?php } ?>
			</select>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" >
			<span class="label">Provinsi</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control" onchange="call_upt()" id="upt_provinsi" name="id_prov" required <?php echo $readonly ?>>
				<option value="">Pilih Provinsi</option>
				<option value="All">Semua Provinsi</option>
				<option value="Only_Admin">Hanya Admin</option>
				<?php 
					$data_shf = $gen_model->GetWhere('ms_provinsi');
						while($list = $data_shf->FetchRow()){
							foreach($list as $key=>$val){
	                        $key=strtolower($key);
	                        $$key=$val;
	                      }  
					?>
				<option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
				<?php } ?>
			</select>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
			<span class="label">Nama UPT</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control"  id="kode_upt" name="loket_upt" required <?php echo $readonly ?>>
				<option value="">Pilih UPT</option>
			</select>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Keterangan</span>
			<textarea class="form-control" id="keterangan" name="keterangan"  rows="3" <?php echo $readonly ?>></textarea>
		</div>
		<div class="col-12">
			<span><b>Note : </b></span><br/>
			<p align="justify">
				<?php echo $web['note_pengisian'] ?>
			</p>
		</div>
		<div class="col-12">
			<div class="headTitle"></div>

			<?php  if($activity!="do_detail"){ ?>
				<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php } 

			if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	$('#nama_perusahaan').typeahead({
			hint: true,
			highlight: true,
			source: function (query, process) {
			    $.ajax({
			        url: '<?php echo $basepath ?>ajax/reqCompany',
			        type: 'POST',
			        dataType: 'JSON',
			        data: 'query='+$('#nama_perusahaan').val(),
			        success: function(data) {
			            var results = data.map(function(item) {
			                var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
			                return someItem;
			            });
			            return process(results);
			        }
			    });
			},
			afterSelect: function(item) {
		    	this.$element[0].value = item.company_name
			},
			updater: function(item) {
			    $('#kode_perusahaan').val(item.company_id);
			    $('#alamat_perusahaan').val(item.company_address);
			    return item;
			}
		});
	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','pelayanan_revoke/add','no_refresh','Simpan Data Revoke','Apakah anda ingin menyimpan data Revoke ?','info','refresh_table');
    });
	
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','pelayanan_revoke/update','no_refresh','Ubah Data Revoke','Apakah anda ingin mengubah data  Revoke ?','warning','refresh_table');
	});
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});
	function call_upt(){
		var prov = $("#upt_provinsi").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#kode_upt").html(msg);        
                }
             }
          }); 
	}
</script>
