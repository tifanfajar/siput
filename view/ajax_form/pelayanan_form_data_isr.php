<?php  
$web = $gen_model->GetOneRow('ms_web'); 
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<div class="row">
		<input id="kode_isr" name="kode_isr"  type="hidden" >
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">No SPP</span> <span class="required">*</span>
			<div class="input-group">
				<input class="form-control" id="nomor_spp" name="nomor_spp"   placeholder="" type="text" required <?php echo $readonly ?>>
				<div class="input-group-btn">
					<button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
				</div>
			</div>
			<input  id="no_spp" name="no_spp" type="hidden" required>
			<input  value="<?php echo $wilayah['id_prov'] ?>" id="id_prov" name="id_prov" type="hidden" required>
			<input  value="<?php echo $map_id ?>" id="map_id_pengunjung" name="map_id" type="hidden" required>
			<input  value="<?php echo $upt ?>" id="loket_upt" name="loket_upt" type="hidden" required>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Service</span> 
			<input  class="form-control"  id="service" name="service"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">No Aplikasi</span>
			<input class="form-control"  id="no_aplikasi" name="no_aplikasi"  placeholder="" type="text" required <?php echo $readonly ?>>
		</div>

		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">No Client License</span>
			<input class="form-control" id="no_client" name="no_client"  placeholder="" type="text" required readonly>
		</div>
		<div class="col-md-8 form-group form-box col-xs-12">
			<span class="label">Nama Pemegang ISR / Waba</span>
			<input class="form-control"  name="nama_pemegang"  id="nama_pemegang"  placeholder="" type="text" required readonly>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Tanggal Pengiriman</span> <span class="required">*</span>
			<input class="form-control <?php echo ($activity=="do_detail" ? '' : 'tgl' ) ?>" id="tgl_terima_waba" name="tgl_terima_waba"  readonly style="<?php echo ($activity=="do_detail" ? '' : 'background-color:white' ) ?>" type="text" required>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Metode</span> <span class="required">*</span>
			<select class="form-control" id="metode" name="metode" required <?php echo $readonly ?>>
				<option value="">Pilih Metode</option>
				<?php 
					$data_mtd = $db->SelectLimit("select * from ms_metode where status='1'"); 
					while($list = $data_mtd->FetchRow()){
								foreach($list as $key=>$val){
		                        $key=strtolower($key);
		                        $$key=$val;
		                      }  
				?>	
				<option value="<?php echo $id_metode ?>"><?php echo $metode ?></option>
			<?php } ?>
			</select>
		</div>
		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Keterangan</span>
			<textarea class="form-control" id="keterangan" name="keterangan"  rows="3" <?php echo $readonly ?>></textarea>
		</div>
		

		<div class="col-12">
			<span><b>Note : </b></span><br/>
			<p align="justify">
				<?php echo $web['note_pengisian'] ?>
			</p>
		</div>
		<div class="col-12">
			<div class="headTitle"></div>

			<?php  if($activity!="do_detail"){ ?>
				<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php } 

			if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','pelayanan_isr/add','no_refresh','Simpan Data ISR','Apakah anda ingin menyimpan data ISR ?','info','refresh_table');
    });
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','pelayanan_isr/update','no_refresh','Ubah Data ISR','Apakah anda ingin mengubah data ISR ?','warning','refresh_table');
	});
	
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});
	$('#nomor_spp').typeahead({
			hint: true,
			highlight: true,
			source: function (query, process) {
			    $.ajax({
			        url: '<?php echo $basepath ?>ajax/reqDataSims',
			        type: 'POST',
			        dataType: 'JSON',
			        data: 'query='+$('#nomor_spp').val(),
			        success: function(data) {
			            var results = data.map(function(item) {
			                var someItem = {name:item.label,myvalue:item.value,no_spp:item.no_spp,company_name:item.company_name,service:item.service,no_aplikasi:item.no_aplikasi,no_client:item.no_client,tagihan:item.tagihan,jenis_spp:item.jenis_spp};
			                return someItem;
			            });
			            return process(results);
			        }
			    });
			},
			afterSelect: function(item) {
		    	this.$element[0].value = item.no_spp
			},
			updater: function(item) {
			    $('#no_spp').val(item.no_spp);
			    $('#nama_pemegang').val(item.company_name);
			    $('#service').val(item.service);
			    $('#no_aplikasi').val(item.no_aplikasi);
			    $('#no_client').val(item.no_client);
			    return item;
			}
		});
</script>
