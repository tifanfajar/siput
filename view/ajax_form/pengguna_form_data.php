<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<div class="row">
		<input id="kode_pelayanan" name="kode_pelayanan"  type="hidden">
		
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Username</span> <span class="required">*</span>
			<input class="form-control" maxlength="20" id="username" name="username"  placeholder=""  type="text" required>
			<input  id="kode_user" name="kode_user"  type="hidden" >
		</div>

		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Password</span> 
				<?php if($activity=="do_edit") { } else { ?> 
					<span class="required">*</span>
				<?php } ?>
			<input class="form-control" id="password" name="password"  placeholder=""  type="password" <?php if($activity=="do_edit") { } else {  echo "required";  } ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Nama Lengkap</span> <span class="required">*</span>
			<input class="form-control" maxlength="50" id="nama_lengkap" name="nama_lengkap"  placeholder=""  type="text" required>
		</div>

		<?php if($activity=="do_edit") { ?> 
			<div class="col-md-6 form-group form-box col-xs-12">
				<span class="label">Status</span> <span class="required">*</span>
				<select  class="form-control"  id="status" name="status"   required>
					<option value="">Pilih Status</option>
					<option value="0">Non Aktif</option>
					<option value="1">Aktif</option>
					<option value="2">Blokir</option>
				</select>
			</div>
			<div class="col-md-12 form-group form-box col-xs-12">
				<span class="label">Group User</span> <span class="required">*</span>
				<select onchange="get_grp()" class="form-control"  id="group_user" name="group_user"   required>
					<option value="">Pilih Group User</option>
					<?php 
					  	$whr_data = array();
						$data_shf = $gen_model->GetWhere('ms_group');
							while($list = $data_shf->FetchRow()){
								foreach($list as $key=>$val){
		                        $key=strtolower($key);
		                        $$key=$val;
		                      }  
						?>
					<option value="<?php echo $kode_group ?>"><?php echo $group_name ?></option>
					<?php } ?>
				</select>
			</div>	
		<?php } else { ?> 
			<div class="col-md-6 form-group form-box col-xs-12">
				<span class="label">Group User</span> <span class="required">*</span>
				<select onchange="get_grp()" class="form-control"  id="group_user" name="group_user"   required>
					<option value="">Pilih Group User</option>
					<?php 
					  	$whr_data = array();
						$data_shf = $gen_model->GetWhere('ms_group');
							while($list = $data_shf->FetchRow()){
								foreach($list as $key=>$val){
		                        $key=strtolower($key);
		                        $$key=$val;
		                      }  
						?>
					<option value="<?php echo $kode_group ?>"><?php echo $group_name ?></option>
					<?php } ?>
				</select>
			</div>		
		<?php } ?>


		
		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" style="display:none">
			<span class="label">Provinsi</span>
			<select class="form-control" onchange="call_upt()" id="upt_provinsi" name="upt_provinsi" >
				<option value="">Pilih Provinsi</option>
				<?php 
				  	$whr_data = array();
					$data_shf = $gen_model->GetWhere('ms_provinsi');
						while($list = $data_shf->FetchRow()){
							foreach($list as $key=>$val){
	                        $key=strtolower($key);
	                        $$key=$val;
	                      }  
					?>
				<option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
				<?php } ?>
			</select>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt" style="display:none">
			<span class="label">Nama UPT</span>
			<select class="form-control"  id="kode_upt" name="kode_upt" >
				<option value="">Pilih UPT</option>
			</select>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">No Fax</span>
			<input class="form-control" maxlength="25" id="no_fax" name="no_fax"  placeholder="" type="text">
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">No Telp</span>
			<input class="form-control" maxlength="25" id="no_tlp" name="no_tlp"  placeholder="" type="text">
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">No HP</span> <span class="required">*</span>
			<input class="form-control" maxlength="25" id="no_hp" name="no_hp"  placeholder="" type="text" required>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Email</span> <span class="required">*</span>
			<input class="form-control"  maxlength="150" id="email" name="email"  placeholder="" type="text" >
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Photo</span>
			<input class="form-control"  id="photo" name="photo"  placeholder="" type="file">
		</div>
		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Alamat</span>
			<textarea class="form-control" id="alamat" name="alamat"  rows="3"></textarea>
		</div>
		<div class="col-12">
			<div class="headTitle"></div>
			<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php  if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	function get_grp(){
		var grp = $("#group_user").val();
		if(grp=="grp_171026194411_1143"  || grp=="grp_171026194411_1144"){
			$("#div_upt_provinsi").show();
			$("#div_upt").show();
		}
		else {
			$("#div_upt_provinsi").hide();
			$("#div_upt").hide();
		}
	}
	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','daftar_pengguna/do_add','no_refresh','Simpan Data Pengguna','Apakah anda ingin menyimpan data Pengguna ?','info','refresh_table');
    });
	
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','daftar_pengguna/do_update','no_refresh','Ubah Data Pengguna','Apakah anda ingin mengubah data Pengguna ?','warning','refresh_table');
	});

	function call_upt(){
		var prov = $("#upt_provinsi").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#kode_upt").html(msg);        
                }
             }
          }); 
	}
</script>
