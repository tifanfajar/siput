<?php  
$web = $gen_model->GetOneRow('ms_web'); 
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<div class="row">
        <input  type="hidden"  id="kode_upt" name="kode_upt">
		

		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Nama UPT</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="nama_upt" name="nama_upt"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>

		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">No Fax</span> 
			<input  class="form-control" maxlength="150" id="upt_no_fax" name="no_fax"   placeholder="" type="text"  <?php echo $readonly ?>>
		</div>

		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">No Tlp</span> 
			<input  class="form-control" maxlength="150" id="upt_no_tlp" name="no_tlp"   placeholder="" type="text"  <?php echo $readonly ?>>
		</div>

		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Email</span> 
			<input  class="form-control" maxlength="255" id="upt_email" name="email"   placeholder="" type="text"  <?php echo $readonly ?>>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" >
			<span class="label">Provinsi</span>  
			<select class="form-control"  id="upt_provinsi" name="id_prov"   <?php echo $readonly ?>>
				<option value="">Pilih Provinsi</option>
				<?php 
					$data_shf = $gen_model->GetWhere('ms_provinsi');
						while($list = $data_shf->FetchRow()){
							foreach($list as $key=>$val){
	                        $key=strtolower($key);
	                        $$key=$val;
	                      }  
					?>
				<option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
				<?php } ?>
			</select>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Alamat</span> 
			<textarea  class="form-control"  id="upt_alamat" name="alamat" <?php echo $readonly ?>></textarea>
		</div>
		
		<div class="col-12">
			<div class="headTitle"></div>
			<?php  if($activity!="do_detail"){ ?>
				<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php } 
			if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','upt/add','no_refresh','Simpan Data UPT','Apakah anda ingin menyimpan data UPT ?','info','refresh_table');
    });
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','upt/update','no_refresh','Ubah Data UPT','Apakah anda ingin mengubah data UPT ?','warning','refresh_table');
	});
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});
</script>
