
<?php  
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<div class="row">
		<input id="kode_pelayanan" name="kode_pelayanan"  type="hidden">
		<input id="tipe_pl" name="tipe_pl"  value="all" type="hidden">
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Tanggal Pelayanan</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input class="form-control <?php echo ($activity=="do_detail" ? '' : 'tgl' ) ?>" id="tgl_pelayanan" name="tgl_pelayanan"  readonly style="<?php echo ($activity=="do_detail" ? '' : 'background-color:white' ) ?>" type="text" required>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Nama Lengkap</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input class="form-control" maxlength="100" id="nama_pengunjung" name="nama_pengunjung"  placeholder=""  type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Nama Perusahaan</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<div class="input-group">
				<input class="form-control" id="nama_perusahaan" name="nama_perusahaan"   placeholder="" type="text" required <?php echo $readonly ?>>
				<div class="input-group-btn">
					<button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
				</div>
			</div>
			<br/>
				Perusahaan tidak terdaftar ? <a onclick="company_loket()" style="color:blue;cursor:pointer"> daftar disini</a>
			<input  id="kode_perusahaan" name="kode_perusahaan" type="hidden" >
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Alamat Perusahaan</span>
			<input class="form-control" readonly style="cursor:not-allowed;" id="alamat_perusahaan"  placeholder="" type="text">
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Jabatan</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="150" id="jabatan_pengunjung" name="jabatan_pengunjung"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">No Telp</span>
			<input class="form-control" maxlength="50" id="no_tlp" name="no_tlp"  placeholder="" type="text" <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">No HP</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input class="form-control" maxlength="50" id="no_hp" name="no_hp"  placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Email</span> 
			<input class="form-control"  maxlength="150" id="email" name="email"  placeholder="" type="email" <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Tujuan</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control"  id="tujuan" name="tujuan"   placeholder="" required <?php echo $readonly ?>>
				<option value="">Pilih Tujuan</option>
			<?php 
					$data_tj = $db->SelectLimit("select * from ms_tujuan where status='1' "); 
					while($list = $data_tj->FetchRow()){
								foreach($list as $key=>$val){
		                        $key=strtolower($key);
		                        $$key=$val;
		                      }  
				?>	
				<option value="<?php echo $kode_tujuan ?>"><?php echo $tujuan ?></option>
			<?php } ?>
			</select>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Perizinan</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<!--<select class="form-control" onchange="get_perizinan()" id="perizinan" name="perizinan"   placeholder="" required <?php echo $readonly ?>>-->
			<select class="form-control"id="perizinan" name="perizinan"   placeholder="" required <?php echo $readonly ?>>
				<option value="">Pilih Perizinan</option>

				<?php 
					$data_perizinan = $db->SelectLimit("select DISTINCT(id_jenis),jenis from ms_group_layanan where status='1'"); 
					while($list = $data_perizinan->FetchRow()){
								foreach($list as $key=>$val){
		                        $key=strtolower($key);
		                        $$key=$val;
		                      }  
				?>	
				<option value="<?php echo $id_jenis ?>"><?php echo $jenis ?></option>
			<?php } ?>
			</select>
		</div>
		<!--<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Group Perizinan</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control"  id="group_perizinan" name="group_perizinan"   placeholder=""  <?php echo $readonly ?>>
				<option value="">Pilih Group Perizinan</option>
			</select>
		</div>-->

		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" >
			<span class="label">Provinsi</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control" onchange="call_upt2()" id="upt_provinsi" name="id_prov" required <?php echo $readonly ?>>
				<option value="">Pilih Provinsi</option>
				<?php 
					$data_shf = $gen_model->GetWhere('ms_provinsi');
						while($list = $data_shf->FetchRow()){
							foreach($list as $key=>$val){
	                        $key=strtolower($key);
	                        $$key=$val;
	                      }  
					?>
				<option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
				<?php } ?>
			</select>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
			<span class="label">Nama UPT</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control"  id="kode_upt" name="loket_upt" required <?php echo $readonly ?>>
				<option value="">Pilih UPT</option>
			</select>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Keterangan</span>
			<textarea class="form-control" id="keterangan" name="keterangan"  rows="3" <?php echo $readonly ?>></textarea>
		</div>
		<div class="col-12">
			<div class="headTitle"></div>

			<?php  if($activity!="do_detail"){ ?>
				<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php } 

			if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
    function get_perizinan(){
    	var prz = $("#perizinan").val();
    	$.ajax({
	        url: '<?php echo $basepath ?>pelayanan/get_group_perizinan',
	        data: 'jenis='+prz,
	        type: 'POST',
	        dataType: 'html',
	        success: function(result_data) {
	            $("#group_perizinan").html(result_data);
	        }
	    });
    }

	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','pelayanan_loket/add','no_refresh','Simpan Data Pengunjung UPT','Apakah anda ingin menyimpan data pengunjung UPT ?','info','refresh_table');
    });
	
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','pelayanan_loket/update','no_refresh','Ubah Data Pengunjung UPT','Apakah anda ingin mengubah data pengunjung UPT ?','warning','refresh_table');
	});
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});
	$('#nama_perusahaan').typeahead({
			hint: true,
			highlight: true,
			source: function (query, process) {
			    $.ajax({
			        url: '<?php echo $basepath ?>ajax/reqCompanyLoket',
			        type: 'POST',
			        dataType: 'JSON',
			        data: 'query='+$('#nama_perusahaan').val(),
			        success: function(data) {
			            var results = data.map(function(item) {
			                var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
			                return someItem;
			            });
			            return process(results);
			        }
			    });
			},
			afterSelect: function(item) {
		    	this.$element[0].value = item.company_name
			},
			updater: function(item) {
			    $('#kode_perusahaan').val(item.company_id);
			    $('#alamat_perusahaan').val(item.company_address);
			    return item;
			}
		});
	function call_upt2(){
		var prov = $("#upt_provinsi").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#kode_upt").html(msg);        
                }
             }
          }); 
	}
</script>
