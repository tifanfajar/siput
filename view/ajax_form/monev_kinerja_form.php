<?php  
$web = $gen_model->GetOneRow('ms_web'); 
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<div class="row">

		<input id="kode_monev_kinerja" name="kode_monev_kinerja"  type="hidden">
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Kinerja</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="kinerja" name="kinerja"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Indikator Kinerja Kegiatan</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="indikator_kinerja" name="indikator_kinerja"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Target</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="target" name="target"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Pagu Anggaran</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="pagu_anggaran" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" name="pagu_anggaran"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>

		<?php if($activity=="do_add"){  ?>
			<div class="col-md-6 form-group form-box col-xs-12">
				<span class="label">Renaksi Kinerja Target Uraian</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php } ?>
				<table class="table table-bordered" style="width:90%">
					<?php  
						for ($x = 1; $x <= 12; $x++) { 
							$zz = $x;
						      if($x<10){
						        $zz = "0".$x; 
						      }
					?>
					<tr>
						<th style="width:10%" align="right"><?php echo "B".$zz; ?></th>
						<td><input  class="form-control"   name="renaksi[]"   placeholder="" type="text" ></td>
					</tr>
					<?php   } ?>
				</table>
			</div>
			<div class="col-md-6 form-group form-box col-xs-12">
				<span class="label">Realisasi</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php } ?>
				<table class="table table-bordered" style="width:90%">
					<?php  
						for ($xz = 1; $xz <= 12; $xz++) { 
							$zz = $xz;
						      if($xz<10){
						        $zz = "0".$xz; 
						      }
					?>
					<tr>
						<th style="width:10%" align="right"><?php echo "B".$zz; ?></th>
						<td><input  class="form-control"   name="realisasi[]"   placeholder="" type="text" ></td>
					</tr>
					<?php   } ?>
				</table>
			</div>
		<?php } else { ?>
			<div class="col-md-6 form-group form-box col-xs-12">
				<span class="label">Renaksi Kinerja Target Uraian</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php } ?>
				<div id="table_renaksi"></div>
			</div>
			<div class="col-md-6 form-group form-box col-xs-12">
				<span class="label">Realisasi</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php } ?>
				<div id="table_realisasi"></div>
			</div>
		<?php } ?>

		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Data Dukung</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php } ?>
			<input type="text"  class="form-control"  id="data_dukung" name="data_dukung" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Keterangan</span> 
			<textarea  class="form-control"  id="keterangan" name="keterangan" <?php echo $readonly ?>></textarea>
		</div>

		<input  value="<?php echo $wilayah['id_prov'] ?>" id="id_prov_galeri" name="id_prov" type="hidden" required>
		<input  value="<?php echo $map_id ?>" id="map_id_galeri" name="map_id" type="hidden" required>
		<input  value="<?php echo $upt ?>" id="kode_upt_galeri" name="loket_upt" type="hidden" required>
		
		
		<div class="col-12">
			<div class="headTitle"></div>
			<?php  if($activity!="do_detail"){ ?>
				<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php } 
			if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	var Ids = 1;
	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','monev_kinerja/add','no_refresh','Simpan Data Monev Kinerja','Apakah anda ingin menyimpan data Monev Kinerja ?','info','refresh_table');
    });
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','monev_kinerja/update','no_refresh','Ubah Data Monev Kinerja','Apakah anda ingin mengubah data Monev Kinerja ?','warning','refresh_table');
	});
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});
</script>
