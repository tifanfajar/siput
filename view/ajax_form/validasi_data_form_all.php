<?php  
$web = $gen_model->GetOneRow('ms_web'); 
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<div class="row">

		<input id="kode_validasi_data" name="kode_validasi_data"  type="hidden">
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">DATA SIMS</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" onkeypress="return only_number(event)"  name="data_sims"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Data Sampling</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" onkeypress="return only_number(event)"  id="data_sampling" name="data_sampling"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12">
				<center style="font-weight:bold;font-weight:50px">Data Hasil Inspeksi</center>
		</div>
		<div class="col-md-3 form-group form-box col-xs-12">
			<span class="label">Sesuai ISR</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" onkeypress="return only_number(event)"  id="sesuai_isr" name="sesuai_isr"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-3 form-group form-box col-xs-12">
			<span class="label">Tidak Sesuai ISR</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" onkeypress="return only_number(event)"  id="tdk_sesuai_isr" name="tdk_sesuai_isr"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-3 form-group form-box col-xs-12">
			<span class="label">Tidak Aktif</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" onkeypress="return only_number(event)"  id="tidak_aktif" name="tidak_aktif"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-3 form-group form-box col-xs-12">
			<span class="label">Proses ISR</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" onkeypress="return only_number(event)"  id="proses_isr" name="proses_isr"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12">
				<center style="font-weight:bold;font-weight:50px">Tindak Lanjut Hasil Inspeksi</center>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Sudah Ditindaklanjuti</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" onkeypress="return only_number(event)"  id="sdh_ditindaklanjuti" name="sdh_ditindaklanjuti"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Belum Ditindaklanjuti</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" onkeypress="return only_number(event)"  id="blm_ditindaklanjuti" name="blm_ditindaklanjuti"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>

		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Capaian</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" onkeypress="return only_number(event)"  id="capaian" name="capaian"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Lampiran</span> 
			<?php if($activity=="do_add"){  ?> 
				<span class="required">*</span> 
			<?php } 
			if($activity!="do_detail"){  ?>
				<input  class="form-control"  name="lampiran"  type="file" <?php echo $readonly ?> <?php if($activity=="do_add"){ echo "required"; } ?> >
			<?php } 
			if($activity=="do_detail" or  $activity=="do_edit"){ 
				echo '<br/><a style="text-decoration:none" id="lampiran" href="#" download>Download<a/>'; 
			    } ?>
		</div>


		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Keterangan</span> 
			<textarea  class="form-control"  id="keterangan" name="keterangan" <?php echo $readonly ?>></textarea>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" >
			<span class="label">Provinsi</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control" onchange="call_upt()" id="upt_provinsi" name="id_prov" required <?php echo $readonly ?>>
				<option value="">Pilih Provinsi</option>
				<option value="All">Semua Provinsi</option>
				<option value="Only_Admin">Hanya Admin</option>
				<?php 
					$data_shf = $gen_model->GetWhere('ms_provinsi');
						while($list = $data_shf->FetchRow()){
							foreach($list as $key=>$val){
	                        $key=strtolower($key);
	                        $$key=$val;
	                      }  
					?>
				<option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
				<?php } ?>
			</select>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
			<span class="label">Nama UPT</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control"  id="kode_upt" name="loket_upt" required <?php echo $readonly ?>>
				<option value="">Pilih UPT</option>
			</select>
		</div>
		
		<div class="col-12">
			<div class="headTitle"></div>
			<?php  if($activity!="do_detail"){ ?>
				<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php } 
			if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','validasi_data/add','no_refresh','Simpan Validasi Data','Apakah anda ingin menyimpan Validasi Data ?','info','refresh_table');
    });
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','validasi_data/update','no_refresh','Ubah Validasi Data','Apakah anda ingin mengubah Validasi Data ?','warning','refresh_table');
	});
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});
	function call_upt(){
		var prov = $("#upt_provinsi").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#kode_upt").html(msg);        
                }
             }
          }); 
	}
</script>
