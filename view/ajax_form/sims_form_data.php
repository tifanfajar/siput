<?php  
$web = $gen_model->GetOneRow('ms_web'); 
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<div class="row">
		<input id="kode_sims" name="kode_sims"  type="hidden" >
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">No SPP</span> 
			<input  class="form-control"  id="no_spp" name="no_spp"  maxlength="50" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">No Client License</span>
			<input class="form-control" id="no_client" name="no_client"  maxlength="50" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">No Aplikasi</span>
			<input class="form-control"  id="no_aplikasi" name="no_aplikasi" maxlength="50" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-8 form-group form-box col-xs-12">
			<span class="label">Perusahaan</span> <span class="required">*</span>
			<div class="input-group">
				<input class="form-control" id="nama_perusahaan" name="nama_perusahaan"   placeholder="" type="text" required <?php echo $readonly ?>>
				<div class="input-group-btn">
					<button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
				</div>
			</div>
			<input  id="kode_perusahaan" name="kode_perusahaan" type="hidden" required>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Request Reff</span>
			<input class="form-control"  id="request_reff" name="request_reff" maxlength="50"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">License State</span>
			<input class="form-control"  id="licence_state" name="licence_state"  maxlength="50"  placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Tagihan</span>
			<input class="form-control"  id="tagihan" name="tagihan"  onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);"   type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Tgl Begin</span>
			<input class="form-control tgl"  id="tgl_begin" name="tgl_begin"  type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Status</span>
			<input class="form-control"  id="status" name="status" maxlength="50"  type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Status Izin</span>
			<input class="form-control"  id="status_izin" name="status_izin"  maxlength="50" placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Kategori SPP</span>
			<input class="form-control"  id="kategori_spp" name="kategori_spp"  maxlength="50" placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Service</span>
			<input class="form-control"  id="service" name="service" maxlength="50"  placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Sub Service</span>
			<input class="form-control"  id="subservice" name="subservice" maxlength="50"  placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-12">
			<div class="headTitle"></div>

			<?php  if($activity!="do_detail"){ ?>
				<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php } 

			if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">

	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','sims/do_add','no_refresh','Simpan Data SIMS','Apakah anda ingin menyimpan data SIMS ?','info','refresh_table');
    });
	
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','sims/do_update','no_refresh','Ubah Data SIMS','Apakah anda ingin mengubah data SIMS ?','warning','refresh_table');
	});
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});

		$('#nama_perusahaan').typeahead({
			hint: true,
			highlight: true,
			source: function (query, process) {
			    $.ajax({
			        url: '<?php echo $basepath ?>ajax/reqCompany',
			        type: 'POST',
			        dataType: 'JSON',
			        data: 'query='+$('#nama_perusahaan').val(),
			        success: function(data) {
			            var results = data.map(function(item) {
			                var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
			                return someItem;
			            });
			            return process(results);
			        }
			    });
			},
			afterSelect: function(item) {
		    	this.$element[0].value = item.company_name
			},
			updater: function(item) {
			    $('#kode_perusahaan').val(item.company_id);
			    return item;
			}
		});
</script>
