<?php  
$web = $gen_model->GetOneRow('ms_web'); 
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<div class="row">
		<input id="kode_piutang" name="kode_piutang"  type="hidden">
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Wajib Bayar</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<div class="input-group">
				<input class="form-control" id="nama_perusahaan" name="nama_perusahaan"   placeholder="" type="text" required <?php echo $readonly ?>>
				<div class="input-group-btn">
					<button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
				</div>
			</div>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">No Client</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="100" id="no_client" name="no_client"   placeholder="" type="text" required readonly="" >
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Nilai Penyerahan</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="nilai_penyerahan" name="nilai_penyerahan"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Tahun Pelimpahan</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input class="form-control thn" id="thn_pelimpahan" name="thn_pelimpahan"  readonly style="<?php echo ($activity=="do_detail" ? '' : 'background-color:white' ) ?>" maxlength="4" type="text" required>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Tahapan Pengurusan</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input class="form-control" maxlength="255" id="thp_pengurusan" name="thp_pengurusan"  type="text" required <?php echo $readonly ?>>
		</div>
	
		<div class="col-md-12 form-group form-box col-xs-12">
				<center style="font-weight:bold;font-weight:50px">Pemindah Bukuan</center>
		</div>

		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Lunas</span>
			<input class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="lunas" name="lunas"  type="text" <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Angsuran</span> 
			<input class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="angsuran" name="angsuran"  type="text" <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Tanggal</span> 
			<input class="form-control <?php echo ($activity=="do_detail" ? '' : 'tgl' ) ?>" id="tanggal_piutang" name="tanggal_piutang"  readonly style="<?php echo ($activity=="do_detail" ? '' : 'background-color:white' ) ?>" type="text" <?php echo $readonly ?>>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12">
				<hr/>
		</div>

		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">PSBDT</span> 
			<input class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="psbdt" name="psbdt"  type="text" <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Pembatalan</span> 
			<input class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="pembatalan" name="pembatalan"  type="text" <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Sisa Piutang</span> 
			<input class="form-control" onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" id="sisa_piutang" name="sisa_piutang"  type="text" <?php echo $readonly ?>>
		</div>
		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Keterangan</span>
			<textarea class="form-control" id="keterangan" name="keterangan"  rows="3" <?php echo $readonly ?>></textarea>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12" id="div_upt_provinsi" >
			<span class="label">Provinsi</span> <span class="required">*</span>
			<select class="form-control" onchange="call_upt()" id="upt_provinsi" name="id_prov" required <?php echo $readonly ?>>
				<option value="">Pilih Provinsi</option>
				<option value="All">Semua Provinsi</option>
				<option value="Only_Admin">Hanya Admin</option>
				<?php 
					$data_shf = $gen_model->GetWhere('ms_provinsi');
						while($list = $data_shf->FetchRow()){
							foreach($list as $key=>$val){
	                        $key=strtolower($key);
	                        $$key=$val;
	                      }  
					?>
				<option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
				<?php } ?>
			</select>
		</div>

		<div class="col-md-6 form-group form-box col-xs-12" id="div_upt_provinsi" >
			<span class="label">Nama KPKNL</span> <span class="required">*</span>
			<select class="form-control"  id="nama_kpknl" name="nama_kpknl" required <?php echo $readonly ?>>
				<option value="">Pilih KPKNL</option>
			</select>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
			<span class="label">Nama UPT</span> <span class="required">*</span>
			<select class="form-control"  id="kode_upt" name="loket_upt" required <?php echo $readonly ?>>
				<option value="">Pilih UPT</option>
			</select>
		</div>

		<div class="col-12">
			<div class="headTitle"></div>
			<?php  if($activity!="do_detail"){ ?>
				<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php } 

			if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	function call_upt(){
		var prov = $("#upt_provinsi").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#kode_upt").html(msg);   

                  	  $.ajax({
			             type: "POST",
			             dataType: "html",
			             url: "<?php echo $basepath ?>daftar_pengguna/get_kpknl",
			             data: "provinsi="+prov,
			             success: function(msg_data){
			                if(msg_data){
			                  $("#nama_kpknl").html(msg_data);        
			                }
			             }
			          }); 

                }
             }
          }); 
	}
	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','pelayanan_piutang/add','no_refresh','Simpan Data Piutang','Apakah anda ingin menyimpan data Piutang ?','info','refresh_table');
    });
	
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','pelayanan_piutang/update','no_refresh','Ubah Data Piutang','Apakah anda ingin mengubah data  Piutang ?','warning','refresh_table');
	});
	
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});
	$(".thn").datepicker({
		 format: 'yyyy',
		 viewMode: "years", 
    	 minViewMode: "years"
	});
	$('#nama_perusahaan').typeahead({
			hint: true,
			highlight: true,
			source: function (query, process) {
			    $.ajax({
			        url: '<?php echo $basepath ?>ajax/reqCompany',
			        type: 'POST',
			        dataType: 'JSON',
			        data: 'query='+$('#nama_perusahaan').val(),
			        success: function(data) {
			            var results = data.map(function(item) {
			                var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
			                return someItem;
			            });
			            return process(results);
			        }
			    });
			},
			afterSelect: function(item) {
		    	this.$element[0].value = item.company_name
			},
			updater: function(item) {
			    $('#no_client').val(item.company_id);
			    return item;
			}
		});
</script>
