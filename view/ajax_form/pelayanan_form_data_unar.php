<?php  
$web = $gen_model->GetOneRow('ms_web'); 
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>

<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<div class="row">


		<div class="col-md-12 form-group form-box col-xs-12">
				<center style="font-weight:bold;font-weight:50px">Tanggal Pelaksanaan UNAR</center>
		</div>

		
		<input  value="<?php echo $wilayah['id_prov'] ?>" id="id_prov" name="id_prov" type="hidden" required>
		<input  value="<?php echo $map_id ?>" id="map_id_pengunjung" name="map_id" type="hidden" required>
		<input  value="<?php echo $upt ?>" id="loket_upt" name="loket_upt" type="hidden" required>
		<input id="kode_unar" name="kode_unar"  type="hidden">
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Tanggal Pelaksanaan Ujian</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input class="form-control <?php echo ($activity=="do_detail" ? '' : 'tgl' ) ?>" id="tgl_unar" name="tgl_unar"  readonly style="<?php echo ($activity=="do_detail" ? '' : 'background-color:white' ) ?>" type="text" required  >
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Lokasi Ujian</span><?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="lokasi_ujian" name="lokasi_ujian"   placeholder="" type="text" required  <?php echo $readonly ?>>
		</div>

	
		<div class="col-md-12 form-group form-box col-xs-12">
				<center style="font-weight:bold;font-weight:50px">Jumlah Peserta</center>
		</div>

		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">YD</span>
			<input onkeypress='return only_number_and_dot(event);' class="form-control" id="jumlah_yd" name="jumlah_yd"    type="number"  <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">YB</span> 
			<input onkeypress='return only_number_and_dot(event);' class="form-control" id="jumlah_yb" name="jumlah_yb"    type="number"  <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">YC</span>
			<input onkeypress='return only_number_and_dot(event);' class="form-control" id="jumlah_yc" name="jumlah_yc"   type="number"  <?php echo $readonly ?>>
		</div>


	
		<div class="col-md-12 form-group form-box col-xs-12">
				<center style="font-weight:bold;font-weight:50px">Lulus</center>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">YD</span>
			<input onkeypress='return only_number_and_dot(event);' class="form-control" id="lulus_yd" name="lulus_yd"   type="number"  <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">YB</span> 
			<input onkeypress='return only_number_and_dot(event);' class="form-control" id="lulus_yb" name="lulus_yb"   type="number"  <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">YC</span>
			<input onkeypress='return only_number_and_dot(event);' class="form-control" id="lulus_yc" name="lulus_yc"   type="number"  <?php echo $readonly ?>>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12">
				<center style="font-weight:bold;font-weight:50px">Tidak Lulus</center>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">YD</span>
			<input onkeypress='return only_number_and_dot(event);' class="form-control" id="tdk_lulus_yd" name="tdk_lulus_yd"   type="number"  <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">YB</span> 
			<input onkeypress='return only_number_and_dot(event);' class="form-control" id="tdk_lulus_yb" name="tdk_lulus_yb"    type="number"  <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">YC</span>
			<input onkeypress='return only_number_and_dot(event);' class="form-control" id="tdk_lulus_yc" name="tdk_lulus_yc"   type="number"  <?php echo $readonly ?>>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Keterangan</span>
			<textarea class="form-control" id="keterangan" name="keterangan"  rows="3" <?php echo $readonly ?>></textarea>
		</div>

		
		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
			<span class="label">Kabupaten / Kota</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control"  id="kabkot" name="kabkot" required <?php echo $readonly ?>>
				<option value="">Pilih Kabupaten / Kota</option>
			</select>
		</div>
	

		<div class="col-12">
			<div class="headTitle"></div>
			<?php  if($activity!="do_detail"){ ?>
				<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php } 

			if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','pelayanan_unar/add','no_refresh','Simpan Data UNAR','Apakah anda ingin menyimpan data UNAR ?','info','refresh_table');
    });
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','pelayanan_unar/update','no_refresh','Ubah Data UNAR','Apakah anda ingin mengubah data UNAR ?','warning','refresh_table');
	});
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});


        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_kabkot",
             data: "provinsi=<?php echo $wilayah['id_prov'] ?>",
             success: function(msg){
                if(msg){
                  $("#kabkot").html(msg);        
                }
             }
          }); 
</script>
