<?php  
$web = $gen_model->GetOneRow('ms_web'); 
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<div class="row">

		<input id="kode_validasi_data" name="kode_validasi_data"  type="hidden">
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">DATA SIMS</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="data_sims" name="data_sims"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Data Sampling</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="data_sampling" name="data_sampling"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12">
				<center style="font-weight:bold;font-weight:50px">Data Hasil Inspeksi</center>
		</div>
		<div class="col-md-3 form-group form-box col-xs-12">
			<span class="label">Sesuai ISR</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="sesuai_isr" name="sesuai_isr"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-3 form-group form-box col-xs-12">
			<span class="label">Tidak Sesuai ISR</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="tdk_sesuai_isr" name="tdk_sesuai_isr"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-3 form-group form-box col-xs-12">
			<span class="label">Tidak Aktif</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="tidak_aktif" name="tidak_aktif"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-3 form-group form-box col-xs-12">
			<span class="label">Proses ISR</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="proses_isr" name="proses_isr"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12">
				<center style="font-weight:bold;font-weight:50px">Tindak Lanjut Hasil Inspeksi</center>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Sudah Ditindaklanjuti</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="sdh_ditindaklanjuti" name="sdh_ditindaklanjuti"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Belum Ditindaklanjuti</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="blm_ditindaklanjuti" name="blm_ditindaklanjuti"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>

		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Capaian</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="capaian" name="capaian"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Lampiran</span> 
			<?php if($activity=="do_add"){  ?> 
				<span class="required">*</span> 
			<?php } 
			if($activity!="do_detail"){  ?>
				<input  class="form-control"  name="lampiran"  type="file" <?php echo $readonly ?> <?php if($activity=="do_add"){ echo "required"; } ?> >
			<?php } 
			if($activity=="do_detail" or  $activity=="do_edit"){ 
				echo '<br/><a style="text-decoration:none" id="lampiran" href="#" download>Download<a/>'; 
			    } ?>
		</div>


		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Keterangan</span> 
			<textarea  class="form-control"  id="keterangan" name="keterangan" <?php echo $readonly ?>></textarea>
		</div>

		<input  value="<?php echo $wilayah['id_prov'] ?>" id="id_prov_bhn_sosialisasi" name="id_prov" type="hidden" required>
		<input  value="<?php echo $map_id ?>" id="map_id_bhn_sosialisasi" name="map_id" type="hidden" required>
		<input  value="<?php echo $upt ?>" id="kode_upt_bhn_sosialisasi" name="loket_upt" type="hidden" required>
		
		<div class="col-12">
			<div class="headTitle"></div>
			<?php  if($activity!="do_detail"){ ?>
				<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php } 
			if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','validasi_data/add','no_refresh','Simpan Validasi Data','Apakah anda ingin menyimpan Validasi Data ?','info','refresh_table');
    });
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','validasi_data/update','no_refresh','Ubah Validasi Data','Apakah anda ingin mengubah Validasi Data ?','warning','refresh_table');
	});
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});
</script>
