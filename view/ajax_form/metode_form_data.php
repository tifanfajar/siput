<?php  
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<input  id="id_metode" name="id_metode" type="hidden" required>
	<div class="row">
		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Metode</span> <span class="required">*</span>
			<input class="form-control" maxlength="50" id="metode" name="metode"  placeholder=""  type="text" required>
		</div>

		<div class="col-12">
			<div class="headTitle"></div>
			<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php  if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','metode/add','no_refresh','Simpan Data Metode','Apakah anda ingin menyimpan data Metode ?','info','refresh_table');
    });
	
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','metode/update','no_refresh','Ubah Data Metode','Apakah anda ingin mengubah data Metode ?','warning','refresh_table');
	});
</script>
