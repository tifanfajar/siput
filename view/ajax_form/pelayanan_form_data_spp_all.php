<?php  
$web = $gen_model->GetOneRow('ms_web'); 
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<div class="row">
		<input id="kode_spp" name="kode_spp"  type="hidden" >
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">No SPP</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input class="form-control"  name="no_spp"  id="no_spp"  placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Nama Pemegang ISR / Waba</span><?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<div class="input-group">
				<input class="form-control" id="nama_perusahaan" name="nama_perusahaan"   placeholder="" type="text" required <?php echo $readonly ?>>
				<div class="input-group-btn">
					<button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
				</div>
			</div>
			<input  id="kode_perusahaan" name="kode_perusahaan" type="hidden" <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">No Aplikasi</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input class="form-control"  id="no_aplikasi" name="no_aplikasi"  placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Service</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control"  id="service" name="service"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Kategori SPP</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control" id="kategori_spp" name="kategori_spp" required <?php echo $readonly ?>>
				<option value="">Pilih Kategori SPP</option>
				<?php 
					$data_mtd = $db->SelectLimit("select * from ms_kategori_spp where status='1' order by kategori_spp asc"); 
					while($list = $data_mtd->FetchRow()){
								foreach($list as $key=>$val){
		                        $key=strtolower($key);
		                        $$key=$val;
		                      }  
				?>	
				<option value="<?php echo $kode_kategori_spp ?>"><?php echo $kategori_spp ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-md-2 form-group form-box col-xs-12">
			<span class="label">Nilai BHP (Rp)</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" class="form-control" id="nilai_bhp" name="nilai_bhp"  placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-2 form-group form-box col-xs-12">
			<span class="label">Tanggal Jatuh Tempo</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input class="form-control <?php echo ($activity=="do_detail" ? '' : 'tgl' ) ?>" id="tgl_jatuh_tempo" name="tgl_jatuh_tempo"  readonly style="<?php echo ($activity=="do_detail" ? '' : 'background-color:white' ) ?>" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-2 form-group form-box col-xs-12">
			<span class="label">Tanggal Pengiriman</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input class="form-control <?php echo ($activity=="do_detail" ? '' : 'tgl' ) ?>" id="tgl_terima_waba" name="tgl_terima_waba"  readonly style="<?php echo ($activity=="do_detail" ? '' : 'background-color:white' ) ?>" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-3 form-group form-box col-xs-12">
			<span class="label">Jenis Pengiriman</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control" id="metode" name="metode" required <?php echo $readonly ?>>
				<option value="">Pilih Jenis Pengiriman</option>
				<?php 
					$data_mtd = $db->SelectLimit("select * from ms_metode where status='1'"); 
					while($list = $data_mtd->FetchRow()){
								foreach($list as $key=>$val){
		                        $key=strtolower($key);
		                        $$key=$val;
		                      }  
				?>	
				<option value="<?php echo $id_metode ?>"><?php echo $metode ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-md-3 form-group form-box col-xs-12"></div>

		<?php  if($activity=="do_add"){ ?>
			<div class="col-md-4 form-group form-box col-xs-12"></div>
			<div class="col-md-4 form-group form-box col-xs-12">
				<span class="label">Bukti Kirim <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php } ?></span>  
				<?php if($activity!="do_detail"){  ?>
					<input  class="form-control"  id="lampiran" name="lampiran[]"  type="file" <?php echo $readonly ?> <?php if($activity=="do_add"){ echo "required"; } ?> >
				<?php } 
				if($activity=="do_detail" or  $activity=="do_edit"){ 
					echo '<br/><a style="text-decoration:none" id="lampiran_spp" href="#" download>Download<a/>'; 
				    } ?>
			</div>
			<div class="col-md-4 form-group form-box col-xs-12"></div>
		<?php } ?>	

			<div id="add_data"></div>


		<div class="col-md-4 form-group form-box col-xs-12"></div>
		<div class="col-md-3 form-group form-box col-xs-12">

		<?php if($activity!="do_detail"){  ?> 
			<center>
				<button type="button" onclick="tambah_file()" class="btn btn-primary btn-sm btn-simpan"><i class="fa fa-plus"></i> Tambah Bukti Kirim</button>
			</center>
		 <?php }?>	

		</div>

		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" >
			<span class="label">Provinsi</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control" onchange="call_upt()" id="upt_provinsi" name="id_prov" required <?php echo $readonly ?>>
				<option value="">Pilih Provinsi</option>
				<?php 
					$data_shf = $gen_model->GetWhere('ms_provinsi');
						while($list = $data_shf->FetchRow()){
							foreach($list as $key=>$val){
	                        $key=strtolower($key);
	                        $$key=$val;
	                      }  
					?>
				<option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
				<?php } ?>
			</select>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
			<span class="label">Nama UPT</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control"  id="kode_upt" name="loket_upt" required <?php echo $readonly ?>>
				<option value="">Pilih UPT</option>
			</select>
		</div>

	

		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Keterangan</span>
			<textarea class="form-control" id="keterangan" name="keterangan"  rows="3" <?php echo $readonly ?>></textarea>
		</div>
		<div class="col-12">
			<span><b>Note : </b></span><br/>
			<p align="justify">
				<?php echo $web['note_pengisian'] ?>
			</p>
		</div>
		<div class="col-12">
			<div class="headTitle"></div>

			<?php  if($activity!="do_detail"){ ?>
				<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php } 

			if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
   $("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','pelayanan_spp/add','no_refresh','Simpan Data SPP','Apakah anda ingin menyimpan data SPP ?','info','refresh_table');
    });
	
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','pelayanan_spp/update','no_refresh','Ubah Data SPP','Apakah anda ingin mengubah data SPP ?','warning','refresh_table');
	});
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});
	$('#nama_perusahaan').typeahead({
			hint: true,
			highlight: true,
			source: function (query, process) {
			    $.ajax({
			        url: '<?php echo $basepath ?>ajax/reqCompany',
			        type: 'POST',
			        dataType: 'JSON',
			        data: 'query='+$('#nama_perusahaan').val(),
			        success: function(data) {
			            var results = data.map(function(item) {
			                var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
			                return someItem;
			            });
			            return process(results);
			        }
			    });
			},
			afterSelect: function(item) {
		    	this.$element[0].value = item.company_name
			},
			updater: function(item) {
			    $('#kode_perusahaan').val(item.company_id);
			    $('#alamat_perusahaan').val(item.company_address);
			    return item;
			}
		});
	function call_upt(){
		var prov = $("#upt_provinsi").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#kode_upt").html(msg);        
                }
             }
          }); 
	}
	var Ids = 1;
	function tambah_file(){
		var  html ='<div class="class_'+Ids+' col-md-4 form-group form-box col-xs-12"></div>';
					 html+='<div class="class_'+Ids+' col-md-4 form-group form-box col-xs-12">';
						 html+='<span class="class_'+Ids+' label">Bukti Kirim <span class="required">*</span></span>';
						 html+='<input  class="class_'+Ids+' form-control"  id="lampiran" name="lampiran[]"  type="file" required>';
					 html+='</div>';
					 html+='<div class="class_'+Ids+' col-md-1 form-group form-box col-xs-12">';
					 	html+='<button type="button" onClick="hapus_file(\''+Ids+'\');" class="btn btn-danger btn-sm btn-batal">Hapus</button>';
					 html+='</div><div class="class_'+Ids+' col-md-3 form-group form-box col-xs-12"></div>';
				$("#add_data").after(html);
				Ids++;
	}
	function hapus_file(ids){
		$(".class_"+ids).remove();
	}
	function hapus_file_id(idsku,ids){
		 swal({
              title: 'Hapus Bukti Kirim',
              text: 'Apakah anda ingin hapus bukti kirim ini  ?',
              type: 'warning',      // warning,info,success,error
              showCancelButton: true,
              showLoaderOnConfirm: true,
              preConfirm: function(){
                $.ajax({
                    url: '<?php echo $basepath ?>pelayanan_spp/delete_file',
                    type: 'POST',
                    data: 'kode='+idsku,
                    success: function(data) {
                    	if(data=="OK"){
                    		 $(".class_up_"+ids).slideUp(1000, function() {
							    $(this).remove();
							});	

                    	}
                    }
                });
               }
            });
	}
</script>
