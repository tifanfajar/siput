<?php  
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<input  id="kode_materi" name="kode_materi" type="hidden" required>
	<div class="row">
		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Hak Akses</span> <span class="required">*</span>
			<input class="form-control" maxlength="50" id="hak_akses" name="hak_akses"  placeholder=""  type="text" required>
		</div>

		<div class="col-12">
			<div class="headTitle"></div>
			<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php  if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','hak_akses/add','no_refresh','Simpan Data Hak Akses','Apakah anda ingin menyimpan data Hak Akses ?','info','refresh_table');
    });
	
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','hak_akses/update','no_refresh','Ubah Data Hak Akses','Apakah anda ingin mengubah data Hak Akses ?','warning','refresh_table');
	});
</script>
