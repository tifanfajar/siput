<?php  
$web = $gen_model->GetOneRow('ms_web'); 
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<div class="row">

		<input id="kode_rencana_sosialisasi" name="kode_rencana_sosialisasi"  type="hidden">
		<div class="col-md-6 form-group form-box col-xs-6" id="div_upt_provinsi" >
			<span class="label">Jenis Kegiatan</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control"  id="kegiatan_rcn_so" name="kegiatan" required <?php echo $readonly ?>>
				<option value="">Pilih Jenis Kegiatan</option>
				<?php 
					$data_mtr = $gen_model->GetWhere('ms_kegiatan');
						while($list = $data_mtr->FetchRow()){
							foreach($list as $key=>$val){
	                        $key=strtolower($key);
	                        $$key=$val;
	                      }  
					?>
				<option value="<?php echo $kode_kegiatan ?>"><?php echo $kegiatan ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Tanggal Pelaksanaan</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control tgl" id="tgl_pelaksanaan_rcn_so" name="tgl_pelaksanaan"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Tema</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="100" id="tema_rcn_so" name="tema"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Tempat</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="100" id="tempat_rcn_so" name="tempat"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Target Peserta</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="50" id="target_peserta_rcn_so" name="target_peserta"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Jumlah Peserta</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control"  id="jumlah_peserta_rcn_so" name="jumlah_peserta"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Anggaran</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control"  id="anggaran_rcn_so" name="anggaran"  onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);"  placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Lampiran</span> 
			<?php if($activity=="do_add"){  ?> 
				<span class="required">*</span> 
			<?php } 
			if($activity!="do_detail"){  ?>
				<input  class="form-control"  id="lampiran" name="lampiran"  type="file" <?php echo $readonly ?> <?php if($activity=="do_add"){ echo "required"; } ?> >
			<?php } 
			if($activity=="do_detail" or  $activity=="do_edit"){ 
				echo '<br/><a style="text-decoration:none" id="lampiran_rcn_so" href="#" download>Download<a/>'; 
			    } ?>
		</div>
		<?php if($activity=="do_add"){  ?>
			<div class="col-md-10 form-group form-box col-xs-10 dinamis_narasumber">
				<span class="label">Narasumber</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
				<input  class="form-control"   name="narasumber[]"   placeholder="" type="text" required <?php echo $readonly ?>>
			</div>
		<?php } ?>
		<div id="data_narasumber" style="width:100%"></div>
		<?php if($activity!="do_detail"){  ?>
			<div class="col-md-12 form-group form-box col-xs-12">
				<center>
					<button onclick="tambah_narasumber()" type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> &nbsp; Tambah</button>
				</center>
			</div>
		<?php } ?>

		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Keterangan</span> 
			<textarea  class="form-control"  id="keterangan_rcn_so" name="keterangan" <?php echo $readonly ?>></textarea>
		</div>

		<input  value="<?php echo $wilayah['id_prov'] ?>" id="id_prov_galeri" name="id_prov" type="hidden" required>
		<input  value="<?php echo $map_id ?>" id="map_id_galeri" name="map_id" type="hidden" required>
		<input  value="<?php echo $upt ?>" id="kode_upt_galeri" name="loket_upt" type="hidden" required>
		
		<div class="col-12">
			<div class="headTitle"></div>
			<?php  if($activity!="do_detail"){ ?>
				<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php } 
			if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	var Ids = 1;
	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','sosialisasi_bimtek_rencana_sosialisasi/add','no_refresh','Simpan Data Rencana Sosialisasi','Apakah anda ingin menyimpan data Rencana Sosialisasi ?','info','refresh_table');
    });
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','sosialisasi_bimtek_rencana_sosialisasi/update','no_refresh','Ubah Data Rencana Sosialisasi','Apakah anda ingin mengubah data Rencana Sosialisasi ?','warning','refresh_table');
	});
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});
	function tambah_narasumber(){
		var  html ='<div style="margin-right:0px;margin-left:0px" class="row" id="narasumber_'+Ids+'" >';
					 html+='<div class="col-md-10 form-group form-box col-xs-10">';
						 html+='<span class="label">Narasumber</span> <span class="required">*</span>';
						 html+='<input  class="form-control"   name="narasumber[]"   placeholder="" type="text" required>';
					 html+='</div>';
					 html+='<div  class="col-md-2 form-group form-box col-xs-2">';
					 	html+='<br/><button type="button"  onClick="hapus_narasumber(\''+Ids+'\');"  class="btn btn-danger btn-sm">Hapus</button>';
					 html+='</div></div>';
				$("#data_narasumber").append(html);
				Ids++;
	}
	function hapus_narasumber(Ids){
		$('#narasumber_'+Ids).slideUp('slow');
		setTimeout(function(){ $('#slideAdd_'+Ids).remove(); }, 500);
	}
	function call_upt(){
		var prov = $("#upt_provinsi").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#kode_upt").html(msg);        
                }
             }
          }); 
	}
</script>
