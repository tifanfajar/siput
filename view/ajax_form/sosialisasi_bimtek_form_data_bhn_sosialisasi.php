<?php  
$web = $gen_model->GetOneRow('ms_web'); 
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<div class="row">

		<input id="kode_bhn_sosialisasi" name="kode_bhn_sosialisasi"  type="hidden">
		<div class="col-md-6 form-group form-box col-xs-6" id="" >
			<span class="label">Materi</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control"  id="materi_bhn_so" name="materi" required <?php echo $readonly ?>>
				<option value="">Pilih Materi</option>
				<?php 
					$data_mtr = $gen_model->GetWhere('ms_materi');
						while($list = $data_mtr->FetchRow()){
							foreach($list as $key=>$val){
	                        $key=strtolower($key);
	                        $$key=$val;
	                      }  
					?>
				<option value="<?php echo $kode_materi ?>"><?php echo $jenis_materi ?></option>
				<?php } ?>
			</select>
		</div>

		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Judul</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="255" id="judul_bhn_so" name="judul"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Author</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control" maxlength="100" id="author_bhn_so" name="author"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Lampiran</span> 
			<?php if($activity=="do_add"){  ?> 
				<span class="required">*</span> 
			<?php } 
			if($activity!="do_detail"){  ?>
				<input  class="form-control"  id="lampiran" name="lampiran"  type="file" <?php echo $readonly ?> <?php if($activity=="do_add"){ echo "required"; } ?> >
			<?php } 
			if($activity=="do_detail" or  $activity=="do_edit"){ 
				echo '<br/><a style="text-decoration:none" id="lampiran_bhn_so" href="#" download>Download<a/>'; 
			    } ?>
		</div>

		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Deskripsi</span> 
			<textarea  class="form-control"  id="deskripsi_bhn_so" name="deskripsi" <?php echo $readonly ?>></textarea>
		</div>

		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Keterangan</span> 
			<textarea  class="form-control"  id="keterangan_bhn_so" name="keterangan" <?php echo $readonly ?>></textarea>
		</div>

		<input  value="<?php echo $wilayah['id_prov'] ?>" id="id_prov_bhn_sosialisasi" name="id_prov" type="hidden" required>
		<input  value="<?php echo $map_id ?>" id="map_id_bhn_sosialisasi" name="map_id" type="hidden" required>
		<input  value="<?php echo $upt ?>" id="kode_upt_bhn_sosialisasi" name="loket_upt" type="hidden" required>
		
		<div class="col-12">
			<div class="headTitle"></div>
			<?php  if($activity!="do_detail"){ ?>
				<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php } 
			if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','sosialisasi_bimtek_bahan_sosialisasi/add','no_refresh','Simpan Data Bahan Sosialisasi','Apakah anda ingin menyimpan data Bahan Sosialisasi ?','info','refresh_table');
    });
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','sosialisasi_bimtek_bahan_sosialisasi/update','no_refresh','Ubah Data Bahan Sosialisasi','Apakah anda ingin mengubah data Bahan Sosialisasi ?','warning','refresh_table');
	});
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});
</script>
