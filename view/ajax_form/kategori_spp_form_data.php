<?php  
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<input  id="kode_kategori_spp" name="kode_kategori_spp" type="hidden" required>
	<div class="row">
		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Kategori SPP</span> <span class="required">*</span>
			<input class="form-control" maxlength="50" id="kgp" name="kgp"  placeholder=""  type="text" required>
		</div>

		<div class="col-12">
			<div class="headTitle"></div>
			<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php  if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','kategori_spp/add','no_refresh','Simpan Data Kategori SPP','Apakah anda ingin menyimpan data Kategori SPP ?','info','refresh_table');
    });
	
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','kategori_spp/update','no_refresh','Ubah Data Kategori SPP','Apakah anda ingin mengubah data Kategori SPP ?','warning','refresh_table');
	});
</script>
