
<form method="POST"  id="do_edit" autocomplete="off" enctype="multipart/form-data">
	<div class="row">

		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Password Lama</span> <span class="required">*</span>
			<input class="form-control"  id="old_pwd" name="old_pwd"   type="password" required>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Password Baru</span> <span class="required">*</span>
			<input class="form-control" id="new_pwd" name="new_pwd"   type="password" required>
		</div>

		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Konfirmasi Password Baru</span> <span class="required">*</span>
			<input class="form-control"  id="k_new_pwd" name="k_new_pwd"   type="password" required>
		</div>

		<div class="col-12">
			<div class="headTitle"></div>
			<button type="submit" class="btn btn-primary btn-sm btn-simpan">Ganti Password</button>
		</div>
	</div>
</form>
<script type="text/javascript">
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','ajax/reqPassword','auth/logout','Ganti Password','Apakah anda ingin mengganti password anda ?','warning','');
	});
</script>
