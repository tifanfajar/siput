<?php  
$web = $gen_model->GetOneRow('ms_web'); 
$readonly="";
if($activity=="do_detail"){ 
	$readonly=" readonly ";
 } ?>
<form method="POST"  id="<?php echo $activity ?>" autocomplete="off" enctype="multipart/form-data">
	<div class="row">
		<input id="kode_spp" name="kode_spp"  type="hidden" >
		<input  value="<?php echo $wilayah['id_prov'] ?>" id="id_prov" name="id_prov" type="hidden" required>
		<input  value="<?php echo $map_id ?>" id="map_id_pengunjung" name="map_id" type="hidden" required>
		<input  value="<?php echo $upt ?>" id="loket_upt" name="loket_upt" type="hidden" required>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">No SPP</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input class="form-control"  name="no_spp"  id="no_spp"  placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Nama Pemegang ISR / Waba</span><?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<div class="input-group">
				<input class="form-control" id="nama_perusahaan" name="nama_perusahaan"   placeholder="" type="text" required <?php echo $readonly ?>>
				<div class="input-group-btn">
					<button style="cursor:default" class="btn btn-default btn-lg" type="button"><i class="glyphicon glyphicon-search"></i></button>
				</div>
			</div>
			<input  id="kode_perusahaan" name="kode_perusahaan" type="hidden" <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">No Aplikasi</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input class="form-control"  id="no_aplikasi" name="no_aplikasi"  placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Service</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input  class="form-control"  id="service" name="service"   placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-6 form-group form-box col-xs-12">
			<span class="label">Kategori SPP</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input class="form-control" id="kategori_spp" name="kategori_spp"  placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-4 form-group form-box col-xs-12">
			<span class="label">Nilai BHP (Rp)</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input onkeydown="return numbersonly(this, event);" onkeyup="javascript:tandaPemisahTitik(this);" class="form-control" id="nilai_bhp" name="nilai_bhp"  placeholder="" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-2 form-group form-box col-xs-12">
			<span class="label">Tanggal Pengiriman</span>  <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<input class="form-control <?php echo ($activity=="do_detail" ? '' : 'tgl' ) ?>" id="tgl_terima_waba" name="tgl_terima_waba"  readonly style="<?php echo ($activity=="do_detail" ? '' : 'background-color:white' ) ?>" type="text" required <?php echo $readonly ?>>
		</div>
		<div class="col-md-3 form-group form-box col-xs-12">
			<span class="label">Jenis Pengiriman</span> <?php if($activity!="do_detail"){  ?> <span class="required">*</span> <?php }?>
			<select class="form-control" id="metode" name="metode" required <?php echo $readonly ?>>
				<option value="">Pilih Jenis Pengiriman</option>
				<?php 
					$data_mtd = $db->SelectLimit("select * from ms_metode where status='1'"); 
					while($list = $data_mtd->FetchRow()){
								foreach($list as $key=>$val){
		                        $key=strtolower($key);
		                        $$key=$val;
		                      }  
				?>	
				<option value="<?php echo $id_metode ?>"><?php echo $metode ?></option>
				<?php } ?>
			</select>
		</div>
		<div class="col-md-3 form-group form-box col-xs-12">
			<span class="label">Lampiran</span>  
			<?php if($activity!="do_detail"){  ?>
				<input  class="form-control"  id="lampiran" name="lampiran"  type="file" <?php echo $readonly ?> <?php if($activity=="do_add"){ echo "required"; } ?> >
			<?php } 
			if($activity=="do_detail" or  $activity=="do_edit"){ 
				echo '<br/><a style="text-decoration:none" id="lampiran_spp" href="#" download>Download<a/>'; 
			    } ?>
		</div>
		<div class="col-md-12 form-group form-box col-xs-12">
			<span class="label">Keterangan</span>
			<textarea class="form-control" id="keterangan" name="keterangan"  rows="3" <?php echo $readonly ?>></textarea>
		</div>
		<div class="col-12">
			<span><b>Note : </b></span><br/>
			<p align="justify">
				<?php echo $web['note_pengisian'] ?>
			</p>
		</div>
		<div class="col-12">
			<div class="headTitle"></div>

			<?php  if($activity!="do_detail"){ ?>
				<button type="submit" class="btn btn-primary btn-sm btn-simpan"><?php echo ($activity=="do_add" ? 'Simpan' : 'Ubah' ) ?></button>
			<?php } 

			if($activity=="do_add"){ ?>
			<button type="reset"  id="btn_batal" class="btn btn-default btn-sm btn-batal">Batal</button>
			<?php } ?>
		</div>
	</div>
</form>
<script type="text/javascript">
    function get_perizinan(){
    	var prz = $("#perizinan").val();
    	$.ajax({
	        url: '<?php echo $basepath ?>pelayanan/get_group_perizinan',
	        data: 'jenis='+prz,
	        type: 'POST',
	        dataType: 'html',
	        success: function(result_data) {
	            $("#group_perizinan").html(result_data);
	        }
	    });
    }

	$("#do_add").on("submit", function (event) {
		event.preventDefault();
        do_act('do_add','pelayanan_spp/add','no_refresh','Simpan Data SPP','Apakah anda ingin menyimpan data SPP ?','info','after_crud_spp');
    });
	
	$("#do_edit").on("submit", function (event) {
		event.preventDefault();
			do_act('do_edit','pelayanan_spp/update','no_refresh','Ubah Data SPP','Apakah anda ingin mengubah data SPP ?','warning','after_crud_spp');
	});
	$(".tgl").datepicker({
		 format: 'dd/mm/yyyy',
	});
	$('#nama_perusahaan').typeahead({
			hint: true,
			highlight: true,
			source: function (query, process) {
			    $.ajax({
			        url: '<?php echo $basepath ?>ajax/reqCompany',
			        type: 'POST',
			        dataType: 'JSON',
			        data: 'query='+$('#nama_perusahaan').val(),
			        success: function(data) {
			            var results = data.map(function(item) {
			                var someItem = {name:item.label,myvalue:item.value,company_id:item.company_id,company_name:item.company_name,company_address:item.company_address};
			                return someItem;
			            });
			            return process(results);
			        }
			    });
			},
			afterSelect: function(item) {
		    	this.$element[0].value = item.company_name
			},
			updater: function(item) {
			    $('#kode_perusahaan').val(item.company_id);
			    $('#alamat_perusahaan').val(item.company_address);
			    return item;
			}
		});
</script>
