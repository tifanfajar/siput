<title>Daftar Pengguna - <?php echo $web['judul']?></title>


<style type="text/css">
  .table th {
     text-align: center;
     font-weight:bold;
  }
  .datatable>tbody>tr>td
  {
    white-space: nowrap;
  } 
</style>
<link href="<?php echo $basepath ?>assets/vendors/easyui/themes/bootstrap/easyui.css" rel="stylesheet">
<link href="<?php echo $basepath ?>assets/vendors/easyui/themes/icon.css" rel="stylesheet">
<body class="pelayanan-cnt">
  <?php  include "view/top_menu.php";  ?>
  
  <!-- Main Container -->
  <div class="container-fluid animated fadeInUp" style="z-index: 3;">
    <div class="row">
  <?php  
    include "view/menu.php";  
    $grp = $gen_model->GetOneRow("ms_group",array('kode_group'=>$gen_controller->decrypt($id_parameter))); 
   ?>
  <!-- Content -->
      
    <div class="container-fluid main-content">
      <div class="row">
      
      <!--  -->
      <div class="col-12">
        <div class="headTitle">
          <h6>Edit Hak Akses</h6>
        </div>
      </div>

      <form  id="f1" method="post" style="width:100%">
      
        <div class="col-md-12 col-xs-12 form-group form-box">
          <span>Nama Hak Akses</span>
          <input name="hak_akses_text" class="form-control" value="<?php echo $grp['group_name'] ?>" readonly aria-describedby="emailHelp"  type="text">
          <input value="<?php echo $id_parameter ?>" name="kode_function_access"  aria-describedby="emailHelp"  type="hidden">
        </div>


        <div class="col-md-12 col-xs-12 form-group form-box">
          <span>Hak Akses</span>
           <table style="width:100%" id="tt" class="easyui-treegrid fixedtable"
              data-options="url:'<?php echo $basepath ?>/hak_akses/update/=<?php echo $id_parameter ?>',idField:'id',treeField:'title',onLoadSuccess:function(){$('.easyui-treegrid').treegrid('expandAll');}" width="100%">
              <thead>
                <tr>
                  <th field="title" width="90%">Menu</th>
                  <th field="read" width="10%"><center>Akses</center></th>
                </tr>
              </thead>
            </table>
        </div>

          <div class="col-12">
            <div class="headTitle">
            </div>
            <button type="submit" class="btn btn-primary btn-sm btn-simpan">Simpan</button>
            <a href="<?php echo $basepath ?>setting"><button type="button" class="btn btn-danger btn-sm btn-batal">Kembali</button></a>
          </div>

       </form>

      </div>
    </div>


    </div>
  </div>
<script src="<?php echo $basepath ?>assets/vendors/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript">
  $("#f1").on("submit", function (event) {
    event.preventDefault();
      do_act('f1','hak_akses/do_update','setting','Ubah Data Hak Akses','Apakah anda ingin mengubah data hak akses ?','warning');
  });

  function check_row( men_id ) {
      $('#read_' + men_id).prop('checked', true);
    }

    function uncheck_row( men_id ) {
      $('#read_' + men_id).prop('checked', false);
    }

</script>