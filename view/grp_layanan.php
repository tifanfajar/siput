<title>Group/Sub Perizinan - <?php echo $web['judul']?></title>


<style type="text/css">
  #example_paginate{position: relative; left: 0%; top: 5px;}
  .pagination li a{margin:5px 15px; }
  .dataTables_info{margin-bottom: 15px;}
  .datatable>tbody>tr>td
  {
    white-space: nowrap;
  } 
  .table th {
     text-align: center;
     font-weight:bold;
  }
</style>
<body class="pelayanan-cnt">


  <?php  include "view/top_menu.php";  ?>
  
  <!-- Main Container -->
  <div class="container-fluid animated fadeInUp" style="z-index: 3;">
    <div class="row">
  <?php  
    include "view/menu.php";  
   ?>
  <!-- Content -->
    <div class="container-fluid main-content" >
      <div class="row">


        <ul class="nav nav-tabs main-nav-tab" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="materi-tab"  onclick="refresh_table()" data-toggle="materi" href="#materi" role="tab" aria-controls="materi" aria-selected="false">List Group/Sub Perizinan</a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent" style="padding-top:0px">


             <!-- Tab List Group/Sub Perizinan -->
           <div class="tab-pane fade show active" id="materi" role="tabpanel" aria-labelledby="materi-tab">

            <div id="filter_upt" class="table table-responsive pelayanan-top">

               <table id="example" class="display table table-striped pelayanan-table">
                <thead>
                  <tr>
                    <th>Kode Group Perizinan</th>
                    <th>Group Perizinan</th>
                    <th>Kode Sub Group Perizinan</th>
                    <th>Sub Group Perizinan</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            <script type="text/javascript">
              var dTable;
                $(document).ready(function() {
                  dTable = $('#example').DataTable( {
                    "bProcessing": true,
                    "bServerSide": true,
                    "bJQueryUI": false,
                    "responsive": false,
                    "autoWidth": false,
                    "sAjaxSource": "<?php echo $basepath ?>grp_layanan/rest", 
                    "sServerMethod": "POST",
                    "scrollX": true,
                    "scrollY": "350px",
                      "scrollCollapse": true,
                    "columnDefs": [
                    { "orderable": true, "targets": 0, "searchable": true},
                    { "orderable": true, "targets": 1, "searchable": true},
                    { "orderable": true, "targets": 2, "searchable": true},
                    { "orderable": true, "targets": 3, "searchable": true}
                    ]
                  } );
                });
            </script>
            </div>  

          </div>



        </div>
      </div>
    </div>
    </div>
  </div>
