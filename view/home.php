<title>Dashboard - <?php echo str_replace('<br/>','',$web['judul']) ?></title>

    <style type="text/css">
	.judul-aplikasi{color:#fff !important; padding:0px !important;}
	html,body{
		background: #a7cfdf;
		background: -moz-linear-gradient(top, #00ffc9 0%,aquamarine 80%, #003a7d 100%);
		background: -webkit-linear-gradient(top, #00ffc9 0%,aquamarine 80%,#003a7d 100%);
		background: linear-gradient(to bottom, #0090ff 0%,aquamarine 80%,#002cdb 100%);
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#00ffc9 ', endColorstr='#003a7d ',GradientType=0 );
		height: 100%;
		overflow: hidden;
	}
      @media (max-width: 400px){
      .title-mobile{margin-left: 17% !important;}
    }
    .main-bar{position: fixed;z-index: 2000}
    .button-cr{
      position: relative;
      overflow: hidden;
      width: 300px;
      height: 60px;
      display: block;
      border-radius: 70px;
      background-color: red;
      background: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #00b5e5), color-stop(100%, #008db2));
      background: -webkit-linear-gradient(top, #00b5e5, #008db2);
      background: -moz-linear-gradient(top, #00b5e5, #008db2);
      background: -o-linear-gradient(top, #00b5e5, #008db2);
      background: linear-gradient(to bottom, #00b5e5, #008db2);
      background-color: #00a1cb;
      border-color: #007998;
      color: white;
      text-shadow: 0 -1px 1px rgba(0, 40, 50, 0.35);
      text-align: center;
      line-height: 3.2;
      font-size: 18px;
      font-weight: bold;
      text-decoration: none !important;
      transition: ease 0.4s;
      }
    .button-cr:hover{color: #FFD800 !important;}
    .button-wrap {
      -webkit-border-radius: 200px;
      -moz-border-radius: 200px;
      -ms-border-radius: 200px;
      -o-border-radius: 200px;
      border-radius: 200px;
      -webkit-box-shadow: inset 0px 1px 3px rgba(0, 0, 0, 0.04);
      -moz-box-shadow: inset 0px 1px 3px rgba(0, 0, 0, 0.04);
      box-shadow: inset 0px 1px 3px rgba(0, 0, 0, 0.04);
      padding: 10px;
    }
	marquee{background-color:rgba(0, 0, 0, 0.66) !important;}
	.copy{color:#fff !important;}
	.bnt-text{
		color:#fff;
		display: block;
		text-align: center;
		margin: 0 auto;
		font-weight: bold;
		width: 80%;
		padding-top: 0px !important;
		font-size: 14px;
		margin-top: 10px;
		text-transform: capitalize;
		padding: 5px;
	}
	.btn-cnt a {
		margin-bottom: 0px;
		text-align: center;
		color: #fff;
		line-height: 21;
	}
    </style>

<body class="dash-back">
  <div id="parallax" role="banner" class="animated fadeInDown">
    <div id="parallax-wrap">
      <div id="mechaGirl" class="parallax-layer bg6"></div>
      <div id="cacheObj" class="parallax-layer bg1"></div>
      <div id="tablette" class="parallax-layer bg2"></div>
      <div id="stylet" class="parallax-layer bg3"></div>
      <div id="lunette" class="parallax-layer bg4"></div>
      <div id="ampoule" class="parallax-layer bg5"></div>
     </div>
  </div>
  <?php 
    include "view/top_menu.php"; 
  ?>
 <?php if(!empty($web['running_text'])){ ?>
      <div class="container-fluid" style="padding: 0px; margin-top: 60px;">
        <marquee class="animated fadeInDown" style="margin-bottom:25px;background-color:#<?php echo $web['running_text_color_bg'] ?>;">
          <span style="color:#<?php echo $web['running_text_color'] ?>;font-size:<?php echo $web['running_text_size'] ?>"><?php echo $web['running_text'] ?></span>
        </marquee>
      </div>
    <?php } ?>

 <!-- Content -->
  <div class="container container-mobile animated fadeInDown" style="height: 79%;">
    <div class="row">
      <div class="col-md-12">
        <h5 class="judul-aplikasi"><?php echo $web['judul']?></h5> 
      </div>
      
      <?php 
          $check_pelayanan = $gen_model->GetOne("fua_read","tr_function_access",array('id_menu'=>2,'kode_function_access'=>$_SESSION['group_user'])); 
          if(!empty($check_pelayanan)){ ?>
      <div class="col-md-4 btn-cnt">
        <span class="button-wrap">
          <a href="<?php echo $basepath ?>pelayanan" class="btn-dash1">Pelayanan</a>
        </span>
      </div>

      <?php } 
         $check_bimtek = $gen_model->GetOne("fua_read","tr_function_access",array('id_menu'=>3,'kode_function_access'=>$_SESSION['group_user'])); 
         if(!empty($check_bimtek)){ ?>

      <div class="col-md-4 btn-cnt">
        <span class="button-wrap">
          <a href="<?php echo $basepath ?>sosialisasi_bimtek" class="btn-dash2">Sosialisasi & Bimtek</a>
        </span>
      </div>
       <?php } 

       $check_monev_kinerja = $gen_model->GetOne("fua_read","tr_function_access",array('id_menu'=>5,'kode_function_access'=>$_SESSION['group_user'])); 
     if(!empty($check_monev_kinerja)){ ?>

      <div class="col-md-4 btn-cnt">
        <span class="button-wrap">
          <a href="<?php echo $basepath ?>monev_kinerja" class="btn-dash3">Monev Kinerja</a>
        </span>
      </div>
      <?php } ?>
      <div class="col-md-2"></div>

     <?php 
      $check_validasi_data = $gen_model->GetOne("fua_read","tr_function_access",array('id_menu'=>4,'kode_function_access'=>$_SESSION['group_user'])); 
      if(!empty($check_validasi_data)){ 
     ?> 
      <div class="col-md-4 btn-cnt">
        <span class="button-wrap">
          <a href="<?php echo $basepath ?>validasi_data" class="btn-dash4">Validasi Data</a>
        </span>
      </div>
     <?php } 

     $check_setting = $gen_model->GetOne("fua_read","tr_function_access",array('id_menu'=>6,'kode_function_access'=>$_SESSION['group_user'])); 
      if(!empty($check_setting)){  ?>
      <div class="col-md-4 btn-cnt">
        <span class="button-wrap">
          <a href="<?php echo $basepath ?>setting" class="btn-dash5">Setting</a>
        </span>
      </div>
      <?php } ?>
      
      <div class="col-md-2"></div>

      <!-- Button -->
      <!-- <div class="col-md-4 btn-cnt">
        <a href="pelayanan.html" class="btn-dash1"></a>
        <h5 class="bnt-text">Pelayanan</h5>
      </div>
      <div class="col-md-4 btn-cnt">
        <a href="sosialisasi.html" class="btn-dash2"></a>
        <h5 class="bnt-text">Sosialisasi & Bimtek</h5>
      </div>
      <div class="col-md-4 btn-cnt">
        <a href="pelayanan.html" class="btn-dash3"></a>
        <h5 class="bnt-text">Penanganan Piutang</h5>
      </div>

      <div class="col-md-4 btn-cnt">
        <a href="pelayanan.html" class="btn-dash4"></a>
        <h5 class="bnt-text">UNAR</h5>
      </div>
      <div class="col-md-4 btn-cnt">
        <a href="validasiData.html" class="btn-dash5"></a>
        <h5 class="bnt-text">Validasi Data</h5>
      </div>
      <div class="col-md-4 btn-cnt">
        <a href="monevKinerja.html" class="btn-dash6"></a>
        <h5 class="bnt-text">Monev Kinerja</h5>
      </div> -->

      <div class="col-md-12">
        <small class="copy">&copy; <?php echo date("Y") ?> Direktorat Operasi Sumber Daya [OPS11]</small>
        <small class="copy" style="bottom: -10px; color: #999;">
          <?php if(!empty($web['alamat'])) { echo $web['alamat']." | ";  } 
                if(!empty($web['no_tlp'])) { echo $web['no_tlp']." | ";  } 
                if(!empty($web['no_hp']))  { echo $web['no_hp']." | ";  } 
                if(!empty($web['no_fax'])) { echo $web['no_fax']." | ";  } 
                if(!empty($web['email']))  { echo "<a style='text-decoration:none' href='mailto:".$web['email']."'>".$web['email']."</a>";  } 
          ?>
        </small>
      </div>

    </div>
  </div>
  <!-- End Content -->
  
</body>