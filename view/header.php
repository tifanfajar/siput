<?php 
$web = $gen_model->GetOneRow('ms_web'); 
$data_my_usr = $md_user->getDetailDataUser('kode_user',$_SESSION['kode_user']);
$my_usr = array();
while($list_usr = $data_my_usr->FetchRow()){
    foreach($list_usr as $key=>$val){
        $key=strtolower($key);
        $$key=$val;
        $my_usr[$key]=$val;
      }  
}

?>
<html lang="en">
  <head>

   <!-- CSS -->
  <link rel="stylesheet" href="<?php echo $basepath ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $basepath ?>assets/css/main.css">
  <link rel="stylesheet" href="<?php echo $basepath ?>assets/css/all.css">
  <link rel="stylesheet" href="<?php echo $basepath ?>assets/css/animate.css">
  <link rel="stylesheet" href="<?php echo $basepath ?>assets/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo $basepath ?>assets/css/bootstrap-glyphicons.css">
  <link rel="stylesheet" href="<?php echo $basepath ?>assets/css/loading.css">
  <link rel="shortcut icon" type="image/x-icon"  href="<?php echo $basepath ?>assets/images/logo.png" />
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 

  <!--<link rel="stylesheet" type="text/css" href="<?php echo $basepath ?>assets/css/parallax.css">-->

  <style type="text/css">
    .dot {
        border-radius: 100%;
        width: 25px;
        height: 5px;
        padding: 6px;
        text-align: center;
    }
  </style>
  
  <!-- Javascript & Jquery -->
  <script type="text/javascript" src="<?php echo $basepath ?>assets/vendors/DataTables/jQuery-3.3.1/jquery-3.3.1.min.js"></script>
  <!--<script src="<?php echo $basepath ?>assets/vendors/jquery/js/jquery.min.js"></script>-->

  <!--DataTables -->
  <link rel="stylesheet" href="<?php echo $basepath ?>assets/vendors/DataTables/datatables.min.css">
  <script type="text/javascript" src="<?php echo $basepath ?>assets/vendors/DataTables/datatables.js"></script>
  
  <!-- Datepicker -->
  <link rel="stylesheet" href="<?php echo $basepath ?>assets/vendors/datepicker/dist/css/bootstrap-datepicker.min.css">
  <script type="text/javascript" src="<?php echo $basepath ?>assets/vendors/datepicker/dist/js/bootstrap-datepicker.min.js"></script>

  <!-- SweetAlert -->
  <link rel="stylesheet" type="text/css" href="<?php echo $basepath ?>assets/vendors/sweetalert2/dist/sweetalert2.min.css">
  <script type="text/javascript" src="<?php echo $basepath ?>assets/vendors/sweetalert2/dist/sweetalert2.min.js"></script>
  <style type="text/css">
    .swal2-container {
      z-index: 9999999999;
    }
  </style>

  <!-- Rupiah Format -->
  <script type="text/javascript" src="<?php echo $basepath ?>assets/js/rupiah.js"></script>

  <!-- Select2 -->
  <script type="text/javascript" src="<?php echo $basepath ?>assets/js/select2.min.js"></script>
  <script type="text/javascript">

    function date_to_default(tgl){
      var newdate = tgl.split("/").reverse().join("-");
      return newdate;
    }

   
    $(".year").datepicker({
        format: "yyyy",
        viewMode: "years", 
        minViewMode: "years"
    });
    $(".select2").select2();
    function logout(){
        $.ajax({
            url: '<?php echo $basepath ?>auth/logout',
            type: 'POST',
            dataType: 'html',
            success: function(data) {
              location.reload();
            }
        });
    }
    function only_number(event) {
         var key = window.event ? event.keyCode : event.which;
          if (event.keyCode == 8 || event.keyCode == 46
           || event.keyCode == 37 || event.keyCode == 39) {
              return true;
          }
          else if ( key < 48 || key > 57 ) {
              return false;
          }
          else return true;
    }
    function only_number_and_dot(event) {
         var x = event.charCode;
          if(x== 46 || x== 45){  //allow dot and strip
            return true;
          }
          else if( x >= 48 && x <= 57 ){ // allow number
            return true;
          }
          else{
            return false;
          }
    }
    function clear_form(form_id){
        $('#'+form_id).find('input:text, input:password,input:hidden, select, textarea').val('');
        $('#'+form_id).find('input:radio, input:checkbox').prop('checked', false); 
    }
    function remove_char(char,symbol){
      var hasil = char.split(symbol).join("");
      return hasil;
    }
  </script>

  <!-- Autocomplete -->
  <script type="text/javascript" src="<?php echo $basepath ?>assets/vendors/typeahead/bootstrap3-typeahead.min.js"></script>
</head>

<!-- Loading -->
<div id="loading_bg" style="background-color: rgba(0, 0, 0, 0.53);width: 100%;height: 100%;position: fixed;
        z-index: 999999999;display:none;top:0;bottom:0;left:0;right:0">
    <div style="margin: auto;position: absolute;top: 0;right: 0;bottom: 0;left: 0;" class="lds-ripple">
      <div></div>
      <div></div>
    </div>
</div>

 <!-- Modal -->
<div id="call_modal" class="modal modal-full fade" role="dialog" style="z-index:99999">
  <div class="modal-dialog modal-lg fullScreenModal">
    <!-- Modal content-->
    <div class="modal-content" style="background-color: #f5f9fc;">
      <div class="modal-header">
         <h4 class="modal-title" id="modal-title">Header Modal</h4>
          <button   type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body" id="modal_body" style="height: 450px;overflow-y: auto;"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
  
  <div id="call_modal_no_full" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="background-color: #f5f9fc;">
      <div class="modal-header">
         <h4 class="modal-title" id="modal-title_no_full">Header Modal</h4>
          <button   type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body" id="modal_body_no_full"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>  

<div id="call_modal_notif" class="modal fade" role="dialog" style="z-index:2100">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="background-color: #f5f9fc;">
      <div class="modal-header">
         <h4 class="modal-title" id="modal-title_notif">Header Modal</h4>
          <button   type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body" id="modal_body_notif"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


 <!-- Modal -->
<div id="call_modal_notif_data" class="modal modal-full fade" role="dialog" style="z-index:2200">
  <div class="modal-dialog modal-lg fullScreenModal">
    <!-- Modal content-->
    <div class="modal-content" style="background-color: #f5f9fc;">
      <div class="modal-header">
         <h4 class="modal-title" id="modal-title_notif_data">Header Modal</h4>
          <button   type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body" id="modal_body_notif_data" style="height: 450px;overflow-y: auto;"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
  
  <div id="call_modal_pwd" class="modal fade" role="dialog">
  <div class="modal-dialog" style="width: 750px;">
    <!-- Modal content-->
    <div class="modal-content" style="background-color: #f5f9fc;">
      <div class="modal-header">
         <h4 class="modal-title" id="modal-title_pwd">Ganti Password</h4>
          <button   type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body" id="modal_body_pwd"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>  

<script type="text/javascript">
  function call_pwd(){
      $("#call_modal_pwd").modal({backdrop:'static',keyboard: false});
      $.ajax({
        url: '<?php echo $basepath ?>ajax/form_pwd',
        type: 'POST',
        dataType: 'html',
        success: function(result_data) {
            $("#modal_body_pwd").html(result_data);
        }
      });
  }
</script>
