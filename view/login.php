<?php $web = $gen_model->GetOneRow('ms_web');  ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo str_replace('<br/>','',$web['judul']) ?></title>

	  <link rel="stylesheet" href="<?php echo $basepath ?>assets/css/bootstrap.min.css">
	  <link rel="stylesheet" href="<?php echo $basepath ?>assets/css/main.css">
	  <link rel="stylesheet" href="<?php echo $basepath ?>assets/css//animate.css">
	  <link rel="shortcut icon" type="image/x-icon"  href="<?php echo $basepath ?>assets/images/logo.png" />
	  <link rel="stylesheet" type="text/css" href="<?php echo $basepath ?>assets/vendors/sweetalert2/dist/sweetalert2.min.css">
	<style>
		html body{overflow:hidden;}
	</style>
</head>
<body class="logbox">
	
	<div class="container">
		<div class="row">
			<h5 class="animated bounceInDown" style="margin-top:40px;"><?php echo $web['judul'] ?></h5>
			<div class="col-md-4 col-xs-12 cnt-login animated fadeInUp">
				<div class="img-logo">
					<img src="<?php echo $basepath ?>assets/images/logo-login.jpg" width="57" alt="">
				</div>
				<!--<span class="intro">Mohon isi informasi login dengan data yang valid untuk melanjutkan klik tombol Login</span>-->
				<br/><br/>
        <form method="POST" id="f1">
					<div class="form-group">
						<input type="text" class="form form-control" placeholder="Username" name="username" required>
					</div>
					<div class="form-group">
						<input type="password" class="form form-control" placeholder="Password" name="password" required>
					</div>
					<button type="submit" class="btn btn-white">Login</button>
				</form>
				<small>&copy; <?php echo date('Y') ?> Direktorat Operasi Sumber Daya [OPS11]</small>
			</div>
		</div>
	</div>

	<script src="<?php echo $basepath ?>assets/js/jquery-3.2.1.slim.min.js"></script>
  <script src="<?php echo $basepath ?>assets/js/bootstrap.min.js"></script>
  <script src="<?php echo $basepath ?>assets/js/popper.min.js"></script>
  <script type="text/javascript" src="<?php echo $basepath ?>assets/vendors/jquery/js/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo $basepath ?>assets/vendors/sweetalert2/dist/sweetalert2.min.js"></script>
  <script type="text/javascript">
      $("#f1").on("submit", function (event) {
        event.preventDefault();
        do_proses('f1','auth','home');
      });
      function do_proses(form_id,act_controller,after_controller){
      $.ajax({
        url: act_controller, 
        type: 'POST',
        data: new FormData($('#'+form_id)[0]),  // Form ID
        processData: false,
        contentType: false,
        success: function(data) {
          var data_trim = $.trim(data);
          if(data_trim=="OK"){
            swal({
              title: 'Success',
              type: 'success',
              showCancelButton: false,
              showLoaderOnConfirm: false,
              }).then(function() {
                 if(after_controller!=''){
                  window.location = '<?php echo $basepath ?>'+after_controller;
                 }
                else {
                  location.reload();
                } 
             });
          } 
          else {
              swal({
              title: 'Error',
              text: "Username / Password Salah",
              type: 'error',
              showCancelButton: false,
              showLoaderOnConfirm: false,
              });
          }
        }
      });
    }
  </script>
</body>
</html>