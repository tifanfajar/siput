<!-- Reject MonevKinerja -->
<div id="Reject_Monev_Kinerja" class="modal fade call_modal" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle">Reject</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="pelayanan_reject" method="POST" autocomplete="off">
                <div class="row">
                  <div class="col-md-12 form-group form-box col-xs-12">
                    <span class="label">Keterangan</span> 
                    <input type="text" name="reject_ket_monev_kinerja" id="reject_ket_monev_kinerja" class="form-control">
                  </div>
                 </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="reject_data('kode_monev_kinerja','monev_kinerja/reject','no_refresh','refresh_table','reject_ket_monev_kinerja')" class="btn btn-danger btn-sm">Reject</button>
        </div>
      </div>
    </div>
</div>

            <!-- Modal Filter -->
<!-- Filter Validasi Data All -->
<div id="Filter_MonevKinerjaAll" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_monev_kinerja_filter_all" method="POST">
               <div class="row">
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Kinerja</span> 
                    <input  class="form-control" maxlength="255" id="filter_kinerja_all" name="kinerja"   placeholder="" type="text" >
                  </div>
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Indikator Kinerja Kegiatan</span> 
                    <input  class="form-control" maxlength="255" id="filter_indikator_kinerja_all" name="indikator_kinerja"   placeholder="" type="text" >
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Target</span> 
                    <input  class="form-control" maxlength="255" id="filter_target_all" name="target"   placeholder="" type="text" >
                  </div>
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Pagu Anggaran</span> 
                    <input  class="form-control" maxlength="255" id="filter_pagu_anggaran_all" name="pagu_anggaran"   placeholder="" type="text" >
                  </div>
               </div>

              <div class="row">
                <div class="col-md-6 form-group form-box col-xs-12">
                  <span class="label">Data Dukung</span> 
                  <input type="text"  class="form-control"  id="filter_data_dukung_all" name="data_dukung" >
                </div>
                <div class="col-md-6 form-group form-box col-xs-12">
                  <span class="label">Keterangan</span> 
                  <textarea  class="form-control"  id="filter_keterangan_all" name="keterangan"></textarea>
                </div>
              </div>
                
               <div class="row">
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_from_all" name="filter_date_from" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_to_all" name="filter_date_to" maxlength="50"  placeholder="" type="text"  >
                  </div> 
              </div> 


                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12" id="div_upt_provinsi" >
                    <span class="label">Provinsi</span> 
                    <select class="form-control" onchange="call_upt_fltr()" id="filter_provinsi_all" name="id_prov" >
                      <option value="">Pilih Provinsi</option>
                      <?php 
                        $data_shf = $gen_model->GetWhere('ms_provinsi');
                          while($list = $data_shf->FetchRow()){
                            foreach($list as $key=>$val){
                                        $key=strtolower($key);
                                        $$key=$val;
                                      }  
                        ?>
                      <option value="<?php echo $id_prov ?>"><?php echo $nama_prov ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="row"> 
                  <div class="col-md-12 form-group form-box col-xs-12" id="div_upt" >
                    <span class="label">Nama UPT</span> 
                    <select class="form-control"  id="filter_upt_all" name="loket_upt" >
                      <option value="">Pilih UPT</option>
                    </select>
                  </div> 
                </div> 
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_monev_kinerja_filter_all')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_monev_kinerja_all()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
  function call_upt_fltr(){
    var prov = $("#filter_provinsi_all").val();
        $.ajax({
             type: "POST",
             dataType: "html",
             url: "<?php echo $basepath ?>daftar_pengguna/get_upt",
             data: "provinsi="+prov,
             success: function(msg){
                if(msg){
                  $("#filter_upt_all").html(msg);        
                }
             }
          }); 
  }
    function filter_monev_kinerja_all() {
      $("#filter_monev_kinerja_all").html("");
      var filter_kinerja            = $("#filter_kinerja_all").val();
      var filter_indikator_kinerja  = $("#filter_indikator_kinerja_all").val();
      var filter_target             = $("#filter_target_all").val();
      var filter_pagu_anggaran      = $("#filter_pagu_anggaran_all").val();
      var filter_data_dukung        = $("#filter_data_dukung_all").val();
      var filter_keterangan         = $("#filter_keterangan_all").val();
      var filter_date_from          = $("#filter_date_from_all").val();
      var filter_date_to            = $("#filter_date_to_all").val();
      var filter_provinsi           = $("#filter_provinsi_all").val();
      var filter_upt                = $("#filter_upt_all").val();

      var param="";
      if(filter_kinerja){
        param += " and mnv.kinerja like '%"+filter_kinerja.trim()+"%' ";
      }if(filter_indikator_kinerja){
        param += " and mnv.indikator_kinerja like '%"+filter_indikator_kinerja.trim()+"%' ";
      }if(filter_target){
        param += " and mnv.target like '%"+filter_target.trim()+"%' ";
      }if(filter_pagu_anggaran){
        param += " and mnv.pagu_anggaran like '%"+filter_pagu_anggaran.trim()+"%' ";
      }if(filter_data_dukung){
        param += " and mnv.data_dukung like '%"+filter_data_dukung.trim()+"%' ";
      }if(filter_keterangan){
        param += " and mnv.keterangan like '%"+filter_keterangan.trim()+"%' ";
      }if(filter_provinsi){
        param += " and mnv.id_prov ='"+filter_provinsi.trim()+"' ";
      }if(filter_upt){
        param += " and mnv.kode_upt ='"+filter_upt.trim()+"' ";
      }


       if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(mnv.created_date, '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(mnv.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(mnv.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_to.trim())+"' ";
      }
      
      $("#MonevKinerjaPencarian_all").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>monev_kinerja/table_all_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_monev_kinerja_all').html(data);
              }
          });
      $('#Filter_MonevKinerjaAll').modal('hide');
   }  
</script>


<!-- Filter Monev Kinerja Detail -->
<div id="Filter_MonevKinerjaDetail" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-search"></i> Filter Data</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <form id="form_monev_kinerja_filter" method="POST">
                
                <div class="row">
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Kinerja</span> 
                    <input  class="form-control" maxlength="255" id="filter_kinerja" name="kinerja"   placeholder="" type="text" >
                  </div>
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Indikator Kinerja Kegiatan</span> 
                    <input  class="form-control" maxlength="255" id="filter_indikator_kinerja" name="indikator_kinerja"   placeholder="" type="text" >
                  </div>
               </div>
               <div class="row">
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Target</span> 
                    <input  class="form-control" maxlength="255" id="filter_target" name="target"   placeholder="" type="text" >
                  </div>
                  <div class="col-md-6 form-group form-box col-xs-12">
                    <span class="label">Pagu Anggaran</span> 
                    <input  class="form-control" maxlength="255" id="filter_pagu_anggaran" name="pagu_anggaran"   placeholder="" type="text" >
                  </div>
               </div>

              <div class="row">
                <div class="col-md-6 form-group form-box col-xs-12">
                  <span class="label">Data Dukung</span> 
                  <input type="text"  class="form-control"  id="filter_data_dukung" name="data_dukung" >
                </div>
                <div class="col-md-6 form-group form-box col-xs-12">
                  <span class="label">Keterangan</span> 
                  <textarea  class="form-control"  id="filter_keterangan" name="keterangan"></textarea>
                </div>
              </div>
                
               <div class="row">
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label">Created Date</span><br/>
                    <span class="label" style="font-size: 13px;">Mulai</span> 
                    <input class="form-control tgl"  id="filter_date_from" name="filter_date_from" maxlength="50"  placeholder="" type="text"  >
                  </div>
                  <div class="col-md-4 form-group form-box col-xs-12">
                    <span class="label"></span><br/>
                    <span class="label" style="font-size: 13px;">Akhir</span> 
                    <input class="form-control tgl"  id="filter_date_to" name="filter_date_to" maxlength="50"  placeholder="" type="text"  >
                  </div> 
              </div> 

            </form>
        </div>
        <div class="modal-footer">
          <button type="button" onclick="clear_form('form_monev_kinerja_filter')"  class="btn btn-warning btn-sm">Reset</button>
          <button type="button" onclick="filter_monev_kinerja()" class="btn btn-primary btn-sm">Cari</button>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
    function filter_monev_kinerja() {
      $("#filter_monev_kinerja").html("");
      var filter_kinerja            = $("#filter_kinerja").val();
      var filter_indikator_kinerja  = $("#filter_indikator_kinerja").val();
      var filter_target             = $("#filter_target").val();
      var filter_pagu_anggaran      = $("#filter_pagu_anggaran").val();
      var filter_data_dukung        = $("#filter_data_dukung").val();
      var filter_keterangan         = $("#filter_keterangan").val();
      var filter_date_from          = $("#filter_date_from").val();
      var filter_date_to            = $("#filter_date_to").val();
      var filter_provinsi           = $("#filter_provinsi_monev_kinerja").val();
      var filter_upt                = $("#filter_upt_monev_kinerja").val();

      var param="";
      if(filter_kinerja){
        param += " and mnv.kinerja like '%"+filter_kinerja.trim()+"%' ";
      }if(filter_indikator_kinerja){
        param += " and mnv.indikator_kinerja like '%"+filter_indikator_kinerja.trim()+"%' ";
      }if(filter_target){
        param += " and mnv.target like '%"+filter_target.trim()+"%' ";
      }if(filter_pagu_anggaran){
        param += " and mnv.pagu_anggaran like '%"+filter_pagu_anggaran.trim()+"%' ";
      }if(filter_data_dukung){
        param += " and mnv.data_dukung like '%"+filter_data_dukung.trim()+"%' ";
      }if(filter_keterangan){
        param += " and mnv.keterangan like '%"+filter_keterangan.trim()+"%' ";
      }if(filter_provinsi){
        param += " and (mnv.id_prov ='"+filter_provinsi.trim()+"' or mnv.id_prov is null)";
      }if(filter_upt){
        param += " and (mnv.kode_upt ='"+filter_upt.trim()+"' or mnv.kode_upt is null)";
      }


       if(filter_date_from && filter_date_to){
        date1 = new Date(filter_date_from); 
        date2 = new Date(filter_date_to);

        var date1   = ((new Date(filter_date_from.split('/').reverse().join('/')).getTime())/1000);
        var date2   = ((new Date(filter_date_to.split('/').reverse().join('/')).getTime())/1000);
        if(date1 > date2){
          swal({
                title: 'Error',
                html: 'Tanggal Mulai lebih besar daripada Tanggal Akhir',
                type: 'error',
                showCancelButton: false,
                showLoaderOnConfirm: false,
            });
        }
        else {
           param += " and DATE_FORMAT(mnv.created_date, '%Y-%m-%d') between '"+date_to_default(filter_date_from.trim())+"' and  '"+date_to_default(filter_date_to.trim())+"'";
        }
      }
      else if(filter_date_from){
           param += " and DATE_FORMAT(mnv.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_from.trim())+"' ";
      }
      else if(filter_date_to){
          param += " and DATE_FORMAT(mnv.created_date, '%Y-%m-%d')='"+date_to_default(filter_date_to.trim())+"' ";
      }
      
      $("#MonevKinerjaPencarian").val(btoa(param));
       $.ajax({
              url: '<?php echo $basepath ?>monev_kinerja/table_detail_filter',
              type: 'POST',
              data: 'param='+btoa(param),
              success: function(data) {
                  $('#filter_monev_kinerja').html(data);
              }
          });
      $('#Filter_MonevKinerjaDetail').modal('hide');
   }  
</script>



<!-- Import Monev Kinerja All -->
<div id="Import_MonevKinerjaAll" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_MonevKinerjaAll" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_monev_kinerja_all.xls" download>Download Sample File</a><br/><br/>
          <span style="font-size: 14px;">File Import Membutuhkan Kode UPT & Kode Provinsi</span><br/>
         - <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>upt">List Data UPT & Provinsi</a>
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<!-- Import Monev Kinerja Detail -->
<div id="Import_MonevKinerja_Detail" class="modal fade call_modal" role="dialog">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h6 class="modal-title" id="exampleModalLongTitle"><i class="fa fa-download"></i> Import File</h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST"  id="DoImport_MonevKinerja_Detail" autocomplete="off" enctype="multipart/form-data">
        <div class="modal-body">
          <i style="display: block; margin-bottom: 10px; font-size: 14px;">Import data file dengan extensi (.xls)</i>
          <input type="file" id="lampiran" name="lampiran" required><br/><hr/>
          <input type="hidden" id="my_id_prov_monev_kinerja" name="id_prov">
          <input type="hidden" id="my_id_upt_monev_kinerja" name="id_upt">
         <a style="text-decoration:none" target="_blank" href="<?php echo $basepath ?>assets/sample_file/import_monev_kinerja.xls" download>Download Sample File</a><br/>
         </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary btn-sm" title="Import" >Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>

<script type="text/javascript">
   //Import Monev Kinerja
   $("#DoImport_MonevKinerjaAll").on("submit", function (event) {
    event.preventDefault();
        do_act('DoImport_MonevKinerjaAll','monev_kinerja/import_all','no_refresh','Import File Monev Kinerja','Apakah anda ingin import file Monev Kinerja ini ?','info','refresh_table');
    });

  //Import Monev Kinerja
   $("#DoImport_MonevKinerja_Detail").on("submit", function (event) {
    event.preventDefault();
        var fltr = $("#filter_provinsi_monev_kinerja").val();
        var fltr_upt = $("#filter_upt_monev_kinerja").val();
        $("#my_id_prov_monev_kinerja").val(fltr);
        $("#my_id_upt_monev_kinerja").val(fltr_upt);
        do_act('DoImport_MonevKinerja_Detail','monev_kinerja/import','no_refresh','Import File Monev Kinerja','Apakah anda ingin import file Monev Kinerja ini ?','info','refresh_table');
    });
</script>