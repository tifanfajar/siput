<title>Daftar Pengguna - <?php echo $web['judul']?></title>


<style type="text/css">
  .table th {
     text-align: center;
     font-weight:bold;
  }
  .datatable>tbody>tr>td
  {
    white-space: nowrap;
  } 
</style>
<link href="<?php echo $basepath ?>assets/vendors/easyui/themes/bootstrap/easyui.css" rel="stylesheet">
<link href="<?php echo $basepath ?>assets/vendors/easyui/themes/icon.css" rel="stylesheet">
<body class="pelayanan-cnt">
  <?php  include "view/top_menu.php";  ?>
  
  <!-- Main Container -->
  <div class="container-fluid animated fadeInUp" style="z-index: 3;">
    <div class="row">
  <?php  
    include "view/menu.php";  
    $grp = $gen_model->GetOneRow("ms_group",array('kode_group'=>$gen_controller->decrypt($id_parameter))); 
   ?>
  <!-- Content -->
      
    <div class="container-fluid main-content">
      <div class="row">
      
      <!--  -->
      <div class="col-12">
        <div class="headTitle">
          <h6>Edit Akses Tab</h6>
        </div>
      </div>

      <form  id="f1" method="post" style="width:100%">
      
        <div class="col-md-12 col-xs-12 form-group form-box">
          <span>Akses Tab</span>
          <input name="hak_akses_text" class="form-control" value="<?php echo $grp['group_name'] ?>" readonly aria-describedby="emailHelp"  type="text">
          <input value="<?php echo $id_parameter ?>" name="kode_function_access"  aria-describedby="emailHelp"  type="hidden">
        </div>


        <div class="col-md-12 col-xs-12 form-group form-box">
          <span>Akses Tab</span>
           <table style="width:100%" id="tt" class="easyui-treegrid fixedtable"
              data-options="url:'<?php echo $basepath ?>/akses_tab/update/=<?php echo $id_parameter ?>',idField:'id',treeField:'title',onLoadSuccess:function(){$('.easyui-treegrid').treegrid('expandAll');}" width="100%">
              <thead>
                <tr>
                  <th field="title" width="50%">Tab</th>
                  <th field="read" width="10%">Read</th>
                  <th field="add" width="10%">Add</th>
                  <th field="edit" width="10%">Edit</th>
                  <th field="delete" width="10%">Delete</th>
                  <th field="checkrow" width="5%"> </th>
                  <th field="uncheckrow" width="5%"> </th>
                </tr>
              </thead>
            </table>
        </div>

          <div class="col-12">
            <div class="headTitle">
            </div>
            <button type="submit" class="btn btn-primary btn-sm btn-simpan">Simpan</button>
            <a href="<?php echo $basepath ?>setting"><button type="button" class="btn btn-danger btn-sm btn-batal">Kembali</button></a>
          </div>

       </form>

      </div>
    </div>


    </div>
  </div>
<script src="<?php echo $basepath ?>assets/vendors/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript">
  $("#f1").on("submit", function (event) {
    event.preventDefault();
      do_act('f1','akses_tab/do_update','setting','Ubah Data Akses Tab','Apakah anda ingin mengubah data akses tab ?','warning');
  });

  function check_row( men_id ) {
      $('#read_' + men_id).prop('checked', true);
      $('#add_' + men_id).prop('checked', true);
      $('#edit_' + men_id).prop('checked', true);
      $('#delete_' + men_id).prop('checked', true);
    }

    function uncheck_row( men_id ) {
      $('#read_' + men_id).prop('checked', false);
      $('#add_' + men_id).prop('checked', false);
      $('#edit_' + men_id).prop('checked', false);
      $('#delete_' + men_id).prop('checked', false);

    }

</script>