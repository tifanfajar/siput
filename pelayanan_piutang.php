<?php 
//General Controller
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();

//Model Pelayanan
include "model/pelayanan.php";
$md_pelayanan  = new pelayanan();

//Model User
include "model/user.php";
$md_user      = new user();
session_start();

//Check Acces Menu and Tab For Notification
$id_tab  = 5;
$id_menu = 2; 
$grp_menu_ntf = get_grp_notif($id_menu,$id_tab);
$group_ntf    = array();
if(!empty($grp_menu_ntf)){
  $variableAry=explode(",",$grp_menu_ntf); 
  foreach($variableAry as $grp) {
    if(!empty($grp)){
     array_push($group_ntf,$grp);
    }
  }
}

//Check Access Tab
$akses_tab = $gen_model->GetOneRow("tr_function_access_tab",array('id_tab'=>$id_tab,'kode_function_access'=>$_SESSION['group_user'])); 
if(empty($akses_tab['fua_edit'])){
  $akses_tab['fua_edit'] = 0;
}
if(empty($akses_tab['fua_delete'])){
  $akses_tab['fua_delete'] = 0;
}


$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}

$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //Check Session
  $gen_controller->check_session();
  //View
  $gen_controller->response_code('404');
}
else if($act=="cetak_detail"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/piutang.php";
}
else if($act=="cetak_all"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/piutang_all.php";
}
else if($act=="table_detail_filter"){
   $param  = $_REQUEST['param'];
   include "view/ajax_table/pelayanan_table_data_piutang_filter.php";
}
else if($act=="table_all_filter"){
   $param  = $_REQUEST['param'];
   include "view/ajax_table/pelayanan_table_data_piutang_filter_all.php";
}
else if($act=="import_all"){
   //File
    $target = basename($_FILES['lampiran']['name']);
    move_uploaded_file($_FILES['lampiran']['tmp_name'], $target);
    $path_info = pathinfo($target);
    $ext =  strtolower($path_info['extension']); 
    
    if($ext!="xls"){
      echo "Format file salah";
      die();
    }

    require_once 'lib/excel_reader.php';
    $data = new Spreadsheet_Excel_Reader($_FILES['lampiran']['name'],false);
    $baris = $data->rowcount($sheet_index=0);
    for ($i=3; $i<=$baris; $i++) {
        $insert_data = array();
        $insert_data['id_prov']             = $data->val($i, 1);
        $insert_data['kode_upt']            = $data->val($i, 2);
        $insert_data['no_client']           = $data->val($i, 3);
        $insert_data['nilai_penyerahan']    = $data->val($i, 4);
        $insert_data['thn_pelimpahan']      = $data->val($i, 5);
        $insert_data['nama_kpknl']          = $data->val($i, 6);
        $insert_data['tahapan_pengurusan']  = $data->val($i, 7);
        $insert_data['lunas']               = $data->val($i, 8);
        $insert_data['angsuran']            = $data->val($i, 9);
        $insert_data['tanggal']             = $data->val($i, 10);
        $insert_data['psbdt']               = $data->val($i, 11);
        $insert_data['pembatalan']          = $data->val($i, 12);
        $insert_data['sisa_piutang']        = $data->val($i, 13);
        $insert_data['keterangan']          = $data->val($i, 14);
        $insert_data['created_date']        = $date_now_indo_full;
        $insert_data['last_update']         = $date_now_indo_full;
        $insert_data['created_by']          = $_SESSION['kode_user'];
        $insert_data['last_update_by']      = $_SESSION['kode_user'];

        
        if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
          $insert_data['status']              = "0";
        }
        else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
          $insert_data['status']              = "3";
        }
        else {
          $insert_data['status']              = "3";
        }
        if(!empty($data->val($i, 3))){
             $insert_data['kode_piutang']        = "pt_".date("ymdhis")."_".rand(1000,9999);
             $gen_model->Insert('tr_penanganan_piutang',$insert_data);
             //For Notification All
             $upt=$insert_data['kode_upt'];
             $upt_array[$upt][] =  array (
                                    'kode' => $insert_data['kode_piutang'],
                                    'prov' => $insert_data['id_prov'],
                                    'upt'  => $upt
                                  );
        }
    }
  
    $keys = array_keys($upt_array);
      for($i = 0; $i < count($upt_array); $i++) {
          $hitung_files = 0;
          $kode  = "";
          foreach($upt_array[$keys[$i]] as $key => $value) {
             $kode .= $value['kode'].",";
              $upt   = $value['upt'];
              $prov  = $value['prov'];
              $hitung_files++;
          }
          $kode = trim($kode,',');
          // echo "Id Prov : ".$prov."<br/>";
          // echo "UPT : ".$upt."<br/>";
          // echo "Kode : ".$kode."<br/>";
          // echo "Total : ".$hitung_files."<br/><br/>";

          //Notification
           if($hitung_files!=0){
               if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                      notifikasi('2','5',$prov,$upt,'grp_171026194411_1143','Penanganan Piutang','Ada '.$hitung_files.' data baru pada Penanganan Piutang yang harus di tinjau',$kode);
                    }
                    else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                      notifikasi('4','5',$prov,$upt,'grp_171026194411_1142','Penanganan Piutang','Ada '.$hitung_files.' data baru pada Penanganan Piutang yang sudah di setujui',$kode);
                    }
                    else {
                      foreach ($group_ntf as $id_group) {
                         notifikasi('1','5',$prov,$upt,$id_group,'Penanganan Piutang','Ada '.$hitung_files.' data baru pada Penanganan Piutang',$kode);
                      }
                    }
          }
      }
    echo "OK";
    
    unlink($_FILES['lampiran']['name']);
}
else if($act=="import_detail"){
   //File
    $target = basename($_FILES['lampiran']['name']);
    move_uploaded_file($_FILES['lampiran']['tmp_name'], $target);
    $path_info = pathinfo($target);
    $ext =  strtolower($path_info['extension']); 
    
    if($ext!="xls"){
      echo "Format file salah";
      die();
    }

    require_once 'lib/excel_reader.php';
    $data = new Spreadsheet_Excel_Reader($_FILES['lampiran']['name'],false);
    $baris = $data->rowcount($sheet_index=0);
    
    $kode="";
    $hitung_files=0;
    for ($i=3; $i<=$baris; $i++) {
        $insert_data = array();
        $insert_data['id_prov']             = $_POST['id_prov'];
        $insert_data['kode_upt']            = $_POST['id_upt'];
        $insert_data['no_client']           = $data->val($i, 1);
        $insert_data['nilai_penyerahan']    = $data->val($i, 2);
        $insert_data['thn_pelimpahan']      = $data->val($i, 3);
        $insert_data['nama_kpknl']          = $data->val($i, 4);
        $insert_data['tahapan_pengurusan']  = $data->val($i, 5);
        $insert_data['lunas']               = $data->val($i, 6);
        $insert_data['angsuran']            = $data->val($i, 7);
        $insert_data['tanggal']             = $data->val($i, 8);
        $insert_data['psbdt']               = $data->val($i, 9);
        $insert_data['pembatalan']          = $data->val($i, 10);
        $insert_data['sisa_piutang']        = $data->val($i, 11);
        $insert_data['keterangan']          = $data->val($i, 12);
        $insert_data['created_date']        = $date_now_indo_full;
        $insert_data['last_update']         = $date_now_indo_full;
        $insert_data['created_by']          = $_SESSION['kode_user'];
        $insert_data['last_update_by']      = $_SESSION['kode_user'];

        if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
          $insert_data['status']              = "0";
        }
        else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
          $insert_data['status']              = "3";
        }
        else {
          $insert_data['status']              = "3";
        }

        if(!empty($data->val($i, 1))){
             $insert_data['kode_piutang']        = "pt_".date("ymdhis")."_".rand(1000,9999);
             $gen_model->Insert('tr_penanganan_piutang',$insert_data);
             $kode .= $insert_data['kode_piutang'].",";
             $hitung_files++;
        }
       
    }
     $kode = rtrim($kode,",");
    //Notification
    if($hitung_files!=0){
         if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                notifikasi('2','5',$_POST['id_prov'],$_POST['id_upt'],'grp_171026194411_1143','Penanganan Piutang','Ada '.$hitung_files.' data baru pada Penanganan Piutang yang harus di tinjau',$kode);
              }
              else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                notifikasi('4','5',$_POST['id_prov'],$_POST['id_upt'],'grp_171026194411_1142','Penanganan Piutang','Ada '.$hitung_files.' data baru pada Penanganan Piutang yang sudah di setujui',$kode);
              }
              else {
                foreach ($group_ntf as $id_group) {
                   notifikasi('1','5',$_POST['id_prov'],$_POST['id_upt'],$id_group,'Penanganan Piutang','Ada '.$hitung_files.' data baru pada Penanganan Piutang',$kode);
                }
              }
    }
    echo "OK";
    
    unlink($_FILES['lampiran']['name']);
}
else if($act=="add"){

    if(!empty($_SESSION['kode_user'])){
      //Proses
      $insert_data = array();
      $insert_data['kode_piutang']        = "pt_".date("ymdhis")."_".rand(1000,9999);
      $insert_data['no_client']           = get($_POST['no_client']);
      $insert_data['id_prov']             = get($_POST['id_prov']);
      $insert_data['kode_upt']            = get($_POST['loket_upt']);
      $insert_data['nilai_penyerahan']    = (!empty($_POST['nilai_penyerahan']) ? rp_int($_POST['nilai_penyerahan']) : null);
      $insert_data['thn_pelimpahan']      = get($_POST['thn_pelimpahan']);
      $insert_data['nama_kpknl']          = get($_POST['nama_kpknl']);
      $insert_data['tahapan_pengurusan']  = get($_POST['thp_pengurusan']);
      $insert_data['lunas']               = (!empty($_POST['lunas']) ? rp_int($_POST['lunas']) : null);
      $insert_data['angsuran']            = (!empty($_POST['angsuran']) ? rp_int($_POST['angsuran']) : null);
      $insert_data['psbdt']               = (!empty($_POST['psbdt']) ? rp_int($_POST['psbdt']) : null);
      $insert_data['pembatalan']          = (!empty($_POST['pembatalan']) ? rp_int($_POST['pembatalan']) : null);
      $insert_data['sisa_piutang']        = (!empty($_POST['sisa_piutang']) ? rp_int($_POST['sisa_piutang']) : null);
      $insert_data['tanggal']             = (!empty($_POST['tanggal_piutang']) ? $gen_controller->date_indo_default($_POST['tanggal_piutang']) : null);
      $insert_data['keterangan']          = get($_POST['keterangan']);
      $insert_data['created_date']        = $date_now_indo_full;
      $insert_data['last_update']         = $date_now_indo_full;
      $insert_data['created_by']          = $_SESSION['kode_user'];
      $insert_data['last_update_by']      = $_SESSION['kode_user'];

      if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
        $insert_data['status']              = "0";
      }
      else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
        $insert_data['status']              = "3";
      }
      else {
        $insert_data['status']              = "3";
      }

        if($_POST['id_prov']=="All"){
          $insert_data['id_prov'] ="";
        }
        if($_POST['loket_upt']=="All"){
          $insert_data['kode_upt']="";
       }
       if($_POST['nama_kpknl']=="All"){
          $insert_data['nama_kpknl']="";
       }

       //Validation
      if($insert_data['no_client']!=""){
           if($gen_model->Insert('tr_penanganan_piutang',$insert_data)=="OK"){
                   //Notification
                  if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                    notifikasi('2','5',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1143','Penanganan Piutang','Ada 1 data baru pada Penanganan Piutang yang harus di tinjau',$insert_data['kode_piutang']);
                  }
                  else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                    notifikasi('4','5',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1142','Penanganan Piutang','Ada 1 data baru pada Penanganan Piutang yang sudah di setujui',$insert_data['kode_piutang']);
                  }
                  else {
                     foreach ($group_ntf as $id_group) {
                       notifikasi('1','5',$_POST['id_prov'],$_POST['loket_upt'],$id_group,'Penanganan Piutang','Ada 1 data baru pada Penanganan Piutang',$insert_data['kode_piutang']);
                    }
                  }
               echo "OK";
          }
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="edit" and $id_parameter!=""){
    $edit  = $gen_model->GetOneRow("tr_penanganan_piutang",array('kode_piutang'=>$id_parameter)); 
    foreach($edit as $key=>$val){
                  $key=strtolower($key);
                  $$key=$val;
    }
    $cp  = $gen_model->GetOneRow("ms_company",array('company_id'=>$no_client)); 
    $data = array(
      'kode_piutang'=>$kode_piutang,
      'created_date'=>$gen_controller->get_date_indonesia($created_date),
      'tanggal'=>$gen_controller->get_date_indonesia($tanggal),
      'no_client'=>$no_client,
      'client_name'=>$cp['company_name'],
      'thn_pelimpahan'=>$thn_pelimpahan,
      'nama_kpknl'=>$nama_kpknl,
      'tahapan_pengurusan'=>$tahapan_pengurusan,
      'nilai_penyerahan'=>int_to_rp($nilai_penyerahan),
      'lunas'=>int_to_rp($lunas),
      'angsuran'=>int_to_rp($angsuran),
      'psbdt'=>int_to_rp($psbdt),
      'pembatalan'=>int_to_rp($pembatalan),
      'sisa_piutang'=>int_to_rp($sisa_piutang),
      'kode_upt'=>$kode_upt,
      'id_prov'=>$id_prov,
      'keterangan'=>$keterangan
    );
    echo json_encode($data); 
}
else if($act=="update"){
    if(!empty($_SESSION['kode_user'])){
      //Proses
      $update_data = array();
      $update_data['no_client']           = get($_POST['no_client']);
      $update_data['id_prov']             = get($_POST['id_prov']);
      $update_data['kode_upt']            = get($_POST['loket_upt']);
      $update_data['nilai_penyerahan']    = (!empty($_POST['nilai_penyerahan']) ? rp_int($_POST['nilai_penyerahan']) : null);
      $update_data['thn_pelimpahan']      = get($_POST['thn_pelimpahan']);
      $update_data['nama_kpknl']          = get($_POST['nama_kpknl']);
      $update_data['tahapan_pengurusan']  = get($_POST['thp_pengurusan']);
      $update_data['lunas']               = (!empty($_POST['lunas']) ? rp_int($_POST['lunas']) : null);
      $update_data['angsuran']            = (!empty($_POST['angsuran']) ? rp_int($_POST['angsuran']) : null);
      $update_data['psbdt']               = (!empty($_POST['psbdt']) ? rp_int($_POST['psbdt']) : null);
      $update_data['pembatalan']          = (!empty($_POST['pembatalan']) ? rp_int($_POST['pembatalan']) : null);
      $update_data['sisa_piutang']        = (!empty($_POST['sisa_piutang']) ? rp_int($_POST['sisa_piutang']) : null);
      $update_data['tanggal']             = (!empty($_POST['tanggal_piutang']) ? $gen_controller->date_indo_default($_POST['tanggal_piutang']) : null);
      $update_data['keterangan']          = get($_POST['keterangan']);
      $update_data['last_update']         = $date_now_indo_full;
      $update_data['last_update_by']      = $_SESSION['kode_user'];


      if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
         $update_data['status']              = "0";
      }

        if($_POST['id_prov']=="All"){
          $update_data['id_prov'] ="";
        }
        if($_POST['loket_upt']=="All"){
          $update_data['kode_upt']="";
       }
       if($_POST['nama_kpknl']=="All"){
          $update_data['nama_kpknl']="";
       }

       //Paramater
      $where_data = array();
      $where_data['kode_piutang']       = $_POST['kode_piutang'];


       //Validation
      if(!empty($_POST['kode_piutang'])){
           if($gen_model->Update('tr_penanganan_piutang',$update_data,$where_data)=="OK"){
                   //Notification
                  if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                     notifikasi('2','5',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1143','Penanganan Piutang','Ada 1 data baru pada Penanganan Piutang yang harus di tinjau',$_POST['kode_piutang']);
                  }
               echo "OK";
          }
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="delete"){
    if(!empty($_SESSION['kode_user'])){

       $update_data['status']              = "2"; //Delete

       //Paramater
      $where_data = array();
      $where_data['kode_piutang']       = $_POST['kode_piutang'];


       //Validation
      if(!empty($_POST['kode_piutang'])){
            echo $gen_model->Update('tr_penanganan_piutang',$update_data,$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="verifikasi"){
    if(!empty($_SESSION['kode_user'])){
       $db->Execute("update tr_penanganan_piutang set status='3' where kode_piutang in(".$_REQUEST['kode'].") ");
       $total = $_REQUEST['total'];
        if(!empty($total)){
        //Notification
          if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
            $kode=str_replace("'","",$_REQUEST['kode']);
            notifikasi('4','5',$_SESSION['upt_provinsi'],$_SESSION['kode_upt'],'grp_171026194411_1142','Penanganan Piutang','Ada '.$total.' data baru pada Penanganan Piutang yang sudah di setujui',$kode);
          }
        }
       echo "OK";
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="reject"){
    if(!empty($_SESSION['kode_user'])){
       $db->Execute("update tr_penanganan_piutang set status='1',ket_sts_apr_reject='".$_REQUEST['ket']."' where kode_piutang in(".$_REQUEST['kode'].") ");
        $total = $_REQUEST['total'];
        if(!empty($total)){
        //Notification
          if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
            $kode=str_replace("'","",$_REQUEST['kode']);
              foreach ($group_ntf as $id_group) {
                      notifikasi('3','5',$_SESSION['upt_provinsi'],$_SESSION['kode_upt'],$id_group,'Penanganan Piutang','Ada '.$total.' data baru pada Penanganan Piutang yang di reject',$kode);
               }
          }
        }
       echo "OK";
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_waiting"){
  $prov = $_REQUEST['id_prov'];
  $upt  = $_REQUEST['upt'];
   $aColumns = array('pt.kode_piutang','pt.created_date','cp.company_name','pt.no_client','pt.nilai_penyerahan','pt.thn_pelimpahan','pt.nama_kpknl','pt.tahapan_pengurusan','pt.lunas','pt.angsuran','pt.tanggal','pt.psbdt','pt.pembatalan','pt.sisa_piutang','pt.keterangan','pt.kode_piutang'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

   $rResult              = $md_pelayanan->getPiutangWaiting($sWhere,$sOrder,$sLimit,$prov,$upt);
   $rResultFilterTotal   = $md_pelayanan->getCountPiutangWaiting($sWhere,$prov,$upt);




  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $wilayah ="";
    if(!empty($aRow['id_prov'])){
     $wilayah = $gen_model->GetOne("id_map","ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    }
    $get_kpknl = $gen_model->getOne('nama_kab_kot','ms_kabupaten_kota',array('id_kabkot'=>$aRow['nama_kpknl']));

    $param_id = $aRow['kode_piutang'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_piutang(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
    

     $checkbox="";
    if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
         if($aRow['status']!="4"){
             $checkbox = '<input type="checkbox" value="'.$param_id.'" name="kode_piutang">';
          }
    }
   
    $sts = "";
    if($aRow['status']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }

     //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_piutang(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_piutang(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
             }
        }
        else {
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_piutang(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_piutang(\''.base64_encode('kode_piutang').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_piutang(\''.base64_encode('kode_piutang').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
             }
        }
        else {
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_piutang(\''.base64_encode('kode_piutang').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }

    $edit_delete = $detail.$edit.$delete;
    $row = array();

    $row = array($checkbox,$gen_controller->get_date_indonesia($aRow['created_date']),$sts,$aRow['company_name'],$aRow['no_client'],int_to_rp($aRow['nilai_penyerahan']),$aRow['thn_pelimpahan'],"KPKNL ".$get_kpknl,$aRow['tahapan_pengurusan'],int_to_rp($aRow['lunas']),int_to_rp($aRow['angsuran']),$gen_controller->get_date_indonesia($aRow['tanggal']),int_to_rp($aRow['psbdt']),int_to_rp($aRow['pembatalan']),int_to_rp($aRow['sisa_piutang']),$aRow['keterangan'],"<center>".$edit_delete."</center>");


    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest"){
  $aColumns = array('pt.created_date','pv.nama_prov','upt.nama_upt','cp.company_name','pt.no_client','pt.nilai_penyerahan','pt.thn_pelimpahan','pt.nama_kpknl','pt.tahapan_pengurusan','pt.lunas','pt.angsuran','pt.tanggal','pt.psbdt','pt.pembatalan','pt.sisa_piutang','pt.keterangan','pt.kode_piutang'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getPiutang($sWhere,$sOrder,$sLimit);
  $rResultFilterTotal   = $md_pelayanan->getCountPiutang($sWhere);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $wilayah ="";
    if(!empty($aRow['id_prov'])){
     $wilayah = $gen_model->GetOne("id_map","ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    }
    $param_id = $aRow['kode_piutang'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_piutang_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

   
      $nama_prov   = $aRow['id_prov'];
      $nama_upt    = $aRow['kode_upt'];
      $nama_kpknl  = $aRow['nama_kpknl'];

      if($nama_prov=="Only_Admin"){
        $nama_prov = "<center>Hanya Admin</center>";
      }  
      else if(empty($nama_prov)){
        $nama_prov = "<center>Semua Provinsi</center>";
      } 
      else {
        $nama_prov  = $aRow['nama_prov'];
      } 

      if($nama_upt=="Only_Admin"){
        $nama_upt = "<center>Hanya Admin</center>";
      }  
      else if(empty($nama_upt)){
        $nama_upt = "<center>Semua UPT</center>";
      } 
      else {
        $nama_upt  = $aRow['nama_upt'];
      }   
      if($nama_kpknl=="Only_Admin"){
        $nama_kpknl = "<center>Hanya Admin</center>";
      }  
      else if(empty($nama_kpknl)){
        $nama_kpknl = "<center>Semua KPKNL</center>";
      } 
      else {
       // $nama_kpknl  = "KPKNL ".$gen_model->getOne('nama_kab_kot','ms_kabupaten_kota',array('id_kabkot'=>$aRow['nama_kpknl']));
        $nama_kpknl  = $gen_model->getOne('nama_kab_kot','ms_kabupaten_kota',array('id_kabkot'=>$aRow['nama_kpknl']));
      }  



    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_piutang_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_piutang_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
             }
        }
        else {
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_piutang_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_piutang_all(\''.base64_encode('kode_piutang').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_piutang_all(\''.base64_encode('kode_piutang').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
             }
        }
        else {
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_piutang_all(\''.base64_encode('kode_piutang').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$nama_prov,$nama_upt,$aRow['company_name'],$aRow['no_client'],int_to_rp($aRow['nilai_penyerahan']),$aRow['thn_pelimpahan'],$nama_kpknl,$aRow['tahapan_pengurusan'],int_to_rp($aRow['lunas']),int_to_rp($aRow['angsuran']),$gen_controller->get_date_indonesia($aRow['tanggal']),int_to_rp($aRow['psbdt']),int_to_rp($aRow['pembatalan']),int_to_rp($aRow['sisa_piutang']),$aRow['keterangan'],"<center>".$edit_delete."</center>");

    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_filter"){
  $param = $_REQUEST['param'];
  $aColumns = array('pt.created_date','pv.nama_prov','upt.nama_upt','cp.company_name','pt.no_client','pt.nilai_penyerahan','pt.thn_pelimpahan','pt.nama_kpknl','pt.tahapan_pengurusan','pt.lunas','pt.angsuran','pt.tanggal','pt.psbdt','pt.pembatalan','pt.sisa_piutang','pt.keterangan','pt.kode_piutang'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getPiutangFilter($sWhere,$sOrder,$sLimit,$param);
  $rResultFilterTotal   = $md_pelayanan->getCountPiutangFilter($sWhere,$param);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $wilayah ="";
    if(!empty($aRow['id_prov'])){
     $wilayah = $gen_model->GetOne("id_map","ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    }
    $param_id = $aRow['kode_piutang'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_piutang_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
     $get_kpknl = $gen_model->getOne('nama_kab_kot','ms_kabupaten_kota',array('id_kabkot'=>$aRow['nama_kpknl']));
    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['kode_upt']==$aRow['kode_upt']){
                if($_SESSION['group_user']=="grp_171026194411_1143"){
                    $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_piutang_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
                }
                else if($aRow['created_by']==$_SESSION['kode_user']){
                    $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_piutang_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
                 }
           }
        }
        else {
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_piutang_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['kode_upt']==$aRow['kode_upt']){
                if($_SESSION['group_user']=="grp_171026194411_1143"){
                    $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_piutang_all(\''.base64_encode('kode_piutang').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                }   
                else if($aRow['created_by']==$_SESSION['kode_user']){
                    $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_piutang_all(\''.base64_encode('kode_piutang').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                 }
             }
        }
        else {  
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_piutang_all(\''.base64_encode('kode_piutang').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }

    $nama_prov   = $aRow['id_prov'];
      $nama_upt    = $aRow['kode_upt'];
      $nama_kpknl  = $aRow['nama_kpknl'];

      if($nama_prov=="Only_Admin"){
        $nama_prov = "<center>Hanya Admin</center>";
      }  
      else if(empty($nama_prov)){
        $nama_prov = "<center>Semua Provinsi</center>";
      } 
      else {
        $nama_prov  = $aRow['nama_prov'];
      } 

      if($nama_upt=="Only_Admin"){
        $nama_upt = "<center>Hanya Admin</center>";
      }  
      else if(empty($nama_upt)){
        $nama_upt = "<center>Semua UPT</center>";
      } 
      else {
        $nama_upt  = $aRow['nama_upt'];
      }   
      if($nama_kpknl=="Only_Admin"){
        $nama_kpknl = "<center>Hanya Admin</center>";
      }  
      else if(empty($nama_kpknl)){
        $nama_kpknl = "<center>Semua KPKNL</center>";
      } 
      else {
        // $nama_kpknl  = "KPKNL ".$gen_model->getOne('nama_kab_kot','ms_kabupaten_kota',array('id_kabkot'=>$aRow['nama_kpknl']));
        $nama_kpknl  = $gen_model->getOne('nama_kab_kot','ms_kabupaten_kota',array('id_kabkot'=>$aRow['nama_kpknl']));
      }  

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$nama_prov,$nama_upt,$aRow['company_name'],$aRow['no_client'],int_to_rp($aRow['nilai_penyerahan']),$aRow['thn_pelimpahan'],$nama_kpknl,$aRow['tahapan_pengurusan'],int_to_rp($aRow['lunas']),int_to_rp($aRow['angsuran']),$gen_controller->get_date_indonesia($aRow['tanggal']),int_to_rp($aRow['psbdt']),int_to_rp($aRow['pembatalan']),int_to_rp($aRow['sisa_piutang']),$aRow['keterangan'],"<center>".$edit_delete."</center>");

    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_provinsi_filter"){
  $param = $_REQUEST['param'];
     $aColumns = array('pt.created_date','pv.nama_prov','upt.nama_upt','cp.company_name','pt.no_client','pt.nilai_penyerahan','pt.thn_pelimpahan','pt.nama_kpknl','pt.tahapan_pengurusan','pt.lunas','pt.angsuran','pt.tanggal','pt.psbdt','pt.pembatalan','pt.sisa_piutang','pt.keterangan','pt.kode_piutang'); //Kolom Pada Tabel


    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getPiutangFilter($sWhere,$sOrder,$sLimit,$param);
  $rResultFilterTotal   = $md_pelayanan->getCountPiutangFilter($sWhere,$param);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

   while($aRow = $rResult->FetchRow()){
    $wilayah ="";
    if(!empty($aRow['id_prov'])){
     $wilayah = $gen_model->GetOne("id_map","ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    }
    $param_id = $aRow['kode_piutang'];
    $get_kpknl = $gen_model->getOne('nama_kab_kot','ms_kabupaten_kota',array('id_kabkot'=>$aRow['nama_kpknl']));
    $detail = '<button title="Detail"  type="button" onclick="do_detail_piutang(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

     //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));


    $edit="";
    $delete="";

    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_piutang(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_piutang(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
             }
        }
        else {
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_piutang(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_piutang(\''.base64_encode('kode_piutang').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            } 
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_piutang(\''.base64_encode('kode_piutang').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
             }
        }
        else {
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_piutang(\''.base64_encode('kode_piutang').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }

     $nama_prov   = $aRow['id_prov'];
      $nama_upt    = $aRow['kode_upt'];
      $nama_kpknl  = $aRow['nama_kpknl'];

      if($nama_prov=="Only_Admin"){
        $nama_prov = "<center>Hanya Admin</center>";
      }  
      else if(empty($nama_prov)){
        $nama_prov = "<center>Semua Provinsi</center>";
      } 
      else {
        $nama_prov  = $aRow['nama_prov'];
      } 

      if($nama_upt=="Only_Admin"){
        $nama_upt = "<center>Hanya Admin</center>";
      }  
      else if(empty($nama_upt)){
        $nama_upt = "<center>Semua UPT</center>";
      } 
      else {
        $nama_upt  = $aRow['nama_upt'];
      }   
      if($nama_kpknl=="Only_Admin"){
        $nama_kpknl = "<center>Hanya Admin</center>";
      }  
      else if(empty($nama_kpknl)){
        $nama_kpknl = "<center>Semua KPKNL</center>";
      } 
      else {
        // $nama_kpknl  = "KPKNL ".$gen_model->getOne('nama_kab_kot','ms_kabupaten_kota',array('id_kabkot'=>$aRow['nama_kpknl']));
        $nama_kpknl  = $gen_model->getOne('nama_kab_kot','ms_kabupaten_kota',array('id_kabkot'=>$aRow['nama_kpknl']));
      }  

    $edit_delete = $detail.$edit.$delete;
    $row = array();

     $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$nama_prov,$nama_upt,$aRow['company_name'],$aRow['no_client'],int_to_rp($aRow['nilai_penyerahan']),$aRow['thn_pelimpahan'],$nama_kpknl,$aRow['tahapan_pengurusan'],int_to_rp($aRow['lunas']),int_to_rp($aRow['angsuran']),$gen_controller->get_date_indonesia($aRow['tanggal']),int_to_rp($aRow['psbdt']),int_to_rp($aRow['pembatalan']),int_to_rp($aRow['sisa_piutang']),$aRow['keterangan'],"<center>".$edit_delete."</center>");

    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_provinsi"){
  $prov = $_REQUEST['id_prov'];
  $upt = $_REQUEST['upt'];

  $aColumns = array('pt.created_date','pv.nama_prov','upt.nama_upt','cp.company_name','pt.no_client','pt.nilai_penyerahan','pt.thn_pelimpahan','pt.nama_kpknl','pt.tahapan_pengurusan','pt.lunas','pt.angsuran','pt.tanggal','pt.psbdt','pt.pembatalan','pt.sisa_piutang','pt.keterangan','pt.kode_piutang'); //Kolom Pada Tabel


    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getPiutang($sWhere,$sOrder,$sLimit,$prov,$upt);
  $rResultFilterTotal   = $md_pelayanan->getCountPiutang($sWhere,$prov,$upt);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $wilayah ="";
    if(!empty($aRow['id_prov'])){
     $wilayah = $gen_model->GetOne("id_map","ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    }
    $param_id = $aRow['kode_piutang'];
    $get_kpknl = $gen_model->getOne('nama_kab_kot','ms_kabupaten_kota',array('id_kabkot'=>$aRow['nama_kpknl']));
    $detail = '<button title="Detail"  type="button" onclick="do_detail_piutang(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

      //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));

    $edit="";
    $delete="";
     if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['kode_upt']==$aRow['kode_upt']){
                if($_SESSION['group_user']=="grp_171026194411_1143"){
                    $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_piutang(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
                } 
                else if($aRow['created_by']==$_SESSION['kode_user']){
                    $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_piutang(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
                 }
             }
        }
        else {
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_piutang(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
              if($_SESSION['kode_upt']==$aRow['kode_upt']){
                  if($_SESSION['group_user']=="grp_171026194411_1143"){
                      $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_piutang(\''.base64_encode('kode_piutang').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                  }
                  else if($aRow['created_by']==$_SESSION['kode_user']){
                      $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_piutang(\''.base64_encode('kode_piutang').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                   }
              }
        }
        else {
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_piutang(\''.base64_encode('kode_piutang').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }

      $nama_prov   = $aRow['id_prov'];
      $nama_upt    = $aRow['kode_upt'];
      $nama_kpknl  = $aRow['nama_kpknl'];

      if($nama_prov=="Only_Admin"){
        $nama_prov = "<center>Hanya Admin</center>";
      }  
      else if(empty($nama_prov)){
        $nama_prov = "<center>Semua Provinsi</center>";
      } 
      else {
        $nama_prov  = $aRow['nama_prov'];
      } 

      if($nama_upt=="Only_Admin"){
        $nama_upt = "<center>Hanya Admin</center>";
      }  
      else if(empty($nama_upt)){
        $nama_upt = "<center>Semua UPT</center>";
      } 
      else {
        $nama_upt  = $aRow['nama_upt'];
      }   
      if($nama_kpknl=="Only_Admin"){
        $nama_kpknl = "<center>Hanya Admin</center>";
      }  
      else if(empty($nama_kpknl)){
        $nama_kpknl = "<center>Semua KPKNL</center>";
      } 
      else {
        // $nama_kpknl  = "KPKNL ".$gen_model->getOne('nama_kab_kot','ms_kabupaten_kota',array('id_kabkot'=>$aRow['nama_kpknl']));
        $nama_kpknl  = $gen_model->getOne('nama_kab_kot','ms_kabupaten_kota',array('id_kabkot'=>$aRow['nama_kpknl']));
      }  

    $edit_delete = $detail.$edit.$delete;
    $row = array();

  $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$nama_prov,$nama_upt,$aRow['company_name'],$aRow['no_client'],int_to_rp($aRow['nilai_penyerahan']),$aRow['thn_pelimpahan'],$nama_kpknl,$aRow['tahapan_pengurusan'],int_to_rp($aRow['lunas']),int_to_rp($aRow['angsuran']),$gen_controller->get_date_indonesia($aRow['tanggal']),int_to_rp($aRow['psbdt']),int_to_rp($aRow['pembatalan']),int_to_rp($aRow['sisa_piutang']),$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else {
  $gen_controller->response_code(http_response_code());
}
?>