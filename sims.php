<?php 
//General Controller
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();

//Model Hak Akses
include "model/sims.php";
$md_sims      = new sims();

//Model User
include "model/user.php";
$md_user      = new user();
session_start();

//Check Access Tab
$akses_tab = $gen_model->GetOneRow("tr_function_access_tab",array('id_tab'=>15,'kode_function_access'=>$_SESSION['group_user'])); 
if(empty($akses_tab['fua_edit'])){
  $akses_tab['fua_edit'] = 0;
}
if(empty($akses_tab['fua_delete'])){
  $akses_tab['fua_delete'] = 0;
}

$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}

$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //Check Session
  $gen_controller->check_session();
  //View
  include "view/header.php";
  include "view/default_style.php";
  include "view/sims.php";
  include "view/footer.php";
}
else if($act=="form"){
   include "view/default_style.php"; 
   $form_url  = $_REQUEST['form_url'];
   $activity  = $_REQUEST['activity'];
   if($form_url=="Form_SIMS"){
      include "view/ajax_form/sims_form_data.php";
   }
}
else if($act=="cetak"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/sims.php";
}
else if($act=="table_sims_filter"){
   $param  = $_REQUEST['param'];
   include "view/ajax_table/sims_table_data_filter.php";
}
else if($act=="import"){
   //File
    $target = basename($_FILES['lampiran']['name']) ;
    move_uploaded_file($_FILES['lampiran']['tmp_name'], $target);
    $path_info = pathinfo($target);
    $ext =  strtolower($path_info['extension']); 
    
    if($ext!="xls"){
      echo "Format file salah";
      die();
    }

    require_once 'lib/excel_reader.php';
    $data = new Spreadsheet_Excel_Reader($_FILES['lampiran']['name'],false);
    $baris = $data->rowcount($sheet_index=0);
    
    for ($i=2; $i<=$baris; $i++) {
        $insert_data = array();
        $insert_data['kode_sims']           = "sims_".date("ymdhis")."_".rand(1000,9999);
        $insert_data['no_klien_licence']    = $data->val($i, 2);
        $insert_data['no_aplikasi']         = $data->val($i, 3);
        $insert_data['request_reff']        = $data->val($i, 4);
        $insert_data['licence_state']       = $data->val($i, 5);
        $insert_data['tagihan']             = rp_int(get($data->val($i, 6)));
        $insert_data['status']              = $data->val($i, 7);
        $insert_data['tgl_begin']           = $data->val($i, 8);
        $insert_data['status_izin']         = $data->val($i, 9);
        $insert_data['kategori_spp']        = $data->val($i, 10);
        $insert_data['service']             = $data->val($i, 11);
        $insert_data['subservice']          = $data->val($i, 12);
        $insert_data['created_date']        = $date_now_indo_full;
        $insert_data['last_update']         = $date_now_indo_full;
        $insert_data['created_by']          = $_SESSION['kode_user'];
        $insert_data['last_update_by']      = $_SESSION['kode_user'];

        if($_SESSION['group_user']=="grp_171026194411_1142"){  //Admin
          $insert_data['status']              = "3";
        }
        else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
          $insert_data['status']              = "3";
        }
        else {
          $insert_data['status']              = "0";
        }

          if(!empty($data->val($i, 1))){
              $count_spp = $db->GetOne("select count(*) as total from tr_data_sims where no_spp='".$data->val($i, 1)."'"); 
              if($count_spp>0){
                 //Paramater
                $where_data = array();
                $where_data['no_spp']     = get($data->val($i, 1));
                 $gen_model->Update('tr_data_sims',$insert_data,$where_data);
              }
              else {
                $insert_data['no_spp']      = get($data->val($i, 1));
                $gen_model->Insert('tr_data_sims',$insert_data);
              }
          }
    }
    echo "OK";
    
    unlink($_FILES['lampiran']['name']);
}
else if($act=="do_add"){
    if(!empty($_SESSION['kode_user'])){
      //Proses
      $insert_data = array();
      $insert_data['kode_sims']               = "sims_".date("ymdhis")."_".rand(1000,9999);
      $insert_data['no_spp']                  = get($_POST['no_spp']);
      $insert_data['no_klien_licence']        = get($_POST['no_client']);
      $insert_data['no_aplikasi']             = get($_POST['no_aplikasi']);
      $insert_data['kode_perusahaan']         = get($_POST['kode_perusahaan']);
      $insert_data['request_reff']            = get($_POST['request_reff']);
      $insert_data['licence_state']           = get($_POST['licence_state']);
      $insert_data['tagihan']                 = rp_int(get($_POST['tagihan']));
      $insert_data['status']                  = get($_POST['status']);
      $insert_data['tgl_begin']               = $gen_controller->date_indo_default($_POST['tgl_begin']);
      $insert_data['status_izin']             = get($_POST['status_izin']);
      $insert_data['kategori_spp']            = get($_POST['kategori_spp']);
      $insert_data['service']                 = get($_POST['service']);
      $insert_data['subservice']              = get($_POST['subservice']);
      $insert_data['created_date']            = $date_now_indo_full;
      $insert_data['last_update']             = $date_now_indo_full;
      $insert_data['created_by']              = $_SESSION['kode_user'];
      $insert_data['last_update_by']          = $_SESSION['kode_user'];

       //Validation
      if($insert_data['no_spp']!=""){

          $count_spp = $db->GetOne("select count(*) as total from tr_data_sims where no_spp='".$_POST['no_spp']."'"); 
          if($count_spp>0){
            echo 'No SPP sudah terdaftar di database';
          }
          else {
            echo $gen_model->Insert('tr_data_sims',$insert_data);
          }
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="edit" and $id_parameter!=""){
    $edit = $gen_model->GetOneRow('tr_data_sims',array('kode_sims'=>$id_parameter)); 
    foreach($edit as $key=>$val){
                  $key=strtolower($key);
                  $$key=$val;
    }
    $cmp = $gen_model->GetOne('company_name','ms_company',array('company_id'=>$kode_perusahaan));
    $data = array('kode_sims'=>$kode_sims,'no_spp'=>$no_spp,'no_klien_licence'=>$no_klien_licence,'no_aplikasi'=>$no_aplikasi,'kode_perusahaan'=>$kode_perusahaan,'request_reff'=>$request_reff,'licence_state'=>$licence_state,'tagihan'=>int_to_rp($tagihan),'status'=>$status,'tgl_begin'=>$gen_controller->get_date_indonesia($tgl_begin),'status_izin'=>$status_izin,'kategori_spp'=>$kategori_spp,'service'=>$service,'subservice'=>$subservice,'cmp'=>$cmp);
    echo json_encode($data); 
}
else if($act=="do_update"){
    if(!empty($_SESSION['kode_user'])){
      //Proses
      $update_data = array();
      $update_data['no_spp']                  = get($_POST['no_spp']);
      $update_data['no_klien_licence']        = get($_POST['no_client']);
      $update_data['no_aplikasi']             = get($_POST['no_aplikasi']);
      $update_data['kode_perusahaan']         = get($_POST['kode_perusahaan']);
      $update_data['request_reff']            = get($_POST['request_reff']);
      $update_data['licence_state']           = get($_POST['licence_state']);
      $update_data['tagihan']                 = rp_int(get($_POST['tagihan']));
      $update_data['status']                  = get($_POST['status']);
      $update_data['tgl_begin']               = $gen_controller->date_indo_default($_POST['tgl_begin']);
      $update_data['status_izin']             = get($_POST['status_izin']);
      $update_data['kategori_spp']            = get($_POST['kategori_spp']);
      $update_data['service']                 = get($_POST['service']);
      $update_data['subservice']              = get($_POST['subservice']);
      $update_data['last_update']             = $date_now_indo_full;
      $update_data['last_update_by']          = $_SESSION['kode_user'];
       //Paramater
          $where_data = array();
          $where_data['kode_sims']     = get($_POST['kode_sims']);

       //Validation
      if($update_data['no_spp']!=""){

          $count_spp = $db->GetOne("select count(*) as total from tr_data_sims where no_spp='".$_POST['no_spp']."' and kode_sims!='".$_POST['kode_sims']."' "); 
          if($count_spp>0){
            echo 'No SPP sudah terdaftar di database';
          }
          else {
            echo $gen_model->Update('tr_data_sims',$update_data,$where_data);
          }
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="delete"){
    if(!empty($_SESSION['kode_user'])){

       //Paramater
      $where_data = array();
      $where_data['kode_sims']       = $_POST['kode_sims'];


       //Validation
      if(!empty($_POST['kode_sims'])){
          echo $gen_model->Delete('tr_data_sims',$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest"){
  $aColumns = array('sm.no_spp','sm.no_klien_licence','cp.company_name','sm.request_reff','sm.licence_state','sm.tagihan','sm.status','sm.tgl_begin','sm.status_izin','sm.kategori_spp','sm.service','sm.subservice','sm.created_date','sm.last_update','sm.kode_sims'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_sims->getSims($sWhere,$sOrder,$sLimit);
  $rResultFilterTotal   = $md_sims->getCountSims($sWhere);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );



  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_sims'];

    $detail = '<button title="Detail"  type="button" onclick="do_detail_sims(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button> &nbsp; ';

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
      $edit = '<button  data-toggle="modal" data-target="#edit_modal" type="button" onclick="do_edit_sims(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
    }
    if($akses_tab['fua_delete']=="1"){ 
        $delete = '&nbsp; <button type="button" onclick="do_delete_sims(\''.base64_encode('kode_sims').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> </button>';
     }

    $edit_delete = $detail.$edit.$delete;

    if(!empty($_REQUEST['only'])){
         $edit_delete = $detail;
    }

    $row = array();
    $row = array($aRow['no_spp'],$aRow['no_klien_licence'],$aRow['company_name'],$aRow['request_reff'],$aRow['licence_state'],int_to_rp($aRow['tagihan']),$aRow['status'],"<center>".$gen_controller->get_date_indonesia($aRow['tgl_begin'])."</center>",$aRow['status_izin'],$aRow['kategori_spp'],$aRow['service'],$aRow['subservice'],"<center>".$gen_controller->get_date_indonesia($aRow['created_date'])."</center>","<center>".$gen_controller->get_date_indonesia($aRow['last_update'])."</center>","<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_filter"){
  $aColumns = array('sm.no_spp','sm.no_klien_licence','cp.company_name','sm.request_reff','sm.licence_state','sm.tagihan','sm.status','sm.tgl_begin','sm.status_izin','sm.kategori_spp','sm.service','sm.subservice','sm.created_date','sm.last_update','sm.kode_sims'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  
  $param = "";

    if(!empty($_REQUEST['no_spp'])){
      $param .= " and sm.no_spp='".$_REQUEST['no_spp']."' ";
    }
    if(!empty($_REQUEST['request_reff'])){
      $param .= " and sm.request_reff='".$_REQUEST['request_reff']."' ";
    }
    if(!empty($_REQUEST['licence_state'])){
      $param .= " and sm.licence_state='".$_REQUEST['licence_state']."' ";
    }
    if(!empty($_REQUEST['tagihan'])){
      $param .= " and sm.tagihan='".$_REQUEST['tagihan']."' ";
    }
    if(!empty($_REQUEST['tgl_begin'])){
      $param .= " and sm.tgl_begin like '".$_REQUEST['tgl_begin']."%' ";
    }
    if(!empty($_REQUEST['status'])){
      $param .= " and sm.status='".$_REQUEST['status']."' ";
    }
    if(!empty($_REQUEST['status_izin'])){
      $param .= " and sm.status_izin='".$_REQUEST['status_izin']."' ";
    }
    if(!empty($_REQUEST['kategori_spp'])){
      $param .= " and sm.kategori_spp='".$_REQUEST['kategori_spp']."' ";
    }
    if(!empty($_REQUEST['service'])){
      $param .= " and sm.service='".$_REQUEST['service']."' ";
    }
    if(!empty($_REQUEST['subservice'])){
      $param .= " and sm.subservice='".$_REQUEST['subservice']."' ";
    }
    if(!empty($_REQUEST['kode_perusahaan'])){
      $param .= " and sm.kode_perusahaan='".$_REQUEST['kode_perusahaan']."' ";
    }


      if(!empty($_REQUEST['filter_date_from']) && !empty($_REQUEST['filter_date_to'])){
        $param .= " and sm.created_date BETWEEN '".$gen_controller->date_indo_default($_REQUEST['filter_date_from'])."' AND '".$gen_controller->date_indo_default($_REQUEST['filter_date_to'])."' ";
      }
      else if(!empty($_REQUEST['filter_date_from'])){
         $param .= " and sm.created_date like '".$gen_controller->date_indo_default($_REQUEST['filter_date_from'])."%' ";
      }
      else if(!empty($_REQUEST['filter_date_to'])){
         $param .= " and sm.created_date like '".$gen_controller->date_indo_default($_REQUEST['filter_date_to'])."%' ";
      }
  
  $rResult              = $md_sims->getSimsFilter($sWhere,$sOrder,$sLimit,base64_encode($param));
  $rResultFilterTotal   = $md_sims->getCountSimsFilter($sWhere,base64_encode($param));


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){

    $param_id = $aRow['kode_sims'];

    $detail = '<button title="Detail"  type="button" onclick="do_detail_sims(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button> &nbsp; ';
   
    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
      $edit = '<button  data-toggle="modal" data-target="#edit_modal" type="button" onclick="do_edit_sims(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
    }
    if($akses_tab['fua_delete']=="1"){ 
        $delete = '&nbsp; <button type="button" onclick="do_delete_sims(\''.base64_encode('kode_sims').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> </button>';
     }


    $edit_delete = $detail.$edit.$delete;

    if(!empty($_REQUEST['only'])){
         $edit_delete = $detail;
    }
    
    $row = array();
    $row = array($aRow['no_spp'],$aRow['no_klien_licence'],$aRow['company_name'],$aRow['request_reff'],$aRow['licence_state'],int_to_rp($aRow['tagihan']),$aRow['status'],"<center>".$gen_controller->get_date_indonesia($aRow['tgl_begin'])."</center>",$aRow['status_izin'],$aRow['kategori_spp'],$aRow['service'],$aRow['subservice'],"<center>".$gen_controller->get_date_indonesia($aRow['created_date'])."</center>","<center>".$gen_controller->get_date_indonesia($aRow['last_update'])."</center>","<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}

else {
  $gen_controller->response_code(http_response_code());
}
?>