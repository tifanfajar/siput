<?php 
//General Controller
include "General_Controller.php";
$gen_controller  = new General_Controller();

//Model Global
include "model/General_Model.php";
$gen_model      = new General_Model();

//Model Pelayanan
include "model/pelayanan.php";
$md_pelayanan  = new pelayanan();

//Model User
include "model/user.php";
$md_user      = new user();
session_start();

//Check Acces Menu and Tab For Notification
$id_tab  = 6;
$id_menu = 2; 
$grp_menu_ntf = get_grp_notif($id_menu,$id_tab);
$group_ntf    = array();
if(!empty($grp_menu_ntf)){
  $variableAry=explode(",",$grp_menu_ntf); 
  foreach($variableAry as $grp) {
    if(!empty($grp)){
     array_push($group_ntf,$grp);
    }
  }
}



//Check Access Tab
$akses_tab = $gen_model->GetOneRow("tr_function_access_tab",array('id_tab'=>$id_tab,'kode_function_access'=>$_SESSION['group_user']));
if(empty($akses_tab['fua_edit'])){
  $akses_tab['fua_edit'] = 0;
}
if(empty($akses_tab['fua_delete'])){
  $akses_tab['fua_delete'] = 0;
}
 

$act="";
if(isset($_REQUEST['do_act'])){
    $act = $_REQUEST['do_act'];
}

$id_parameter="";
if(isset($_REQUEST['id_parameter'])){
        $id_parameter =$_REQUEST['id_parameter'];
}
if($act=="" or $act==null) {
  //Check Session
  $gen_controller->check_session();
  //View
  $gen_controller->response_code('404');
}
else if($act=="cetak_detail"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/unar.php";
}
else if($act=="cetak_all"){
   $param = $_REQUEST['param'];
   $tipe  = $_REQUEST['tipe'];
   include "view/cetak/unar_all.php";
}
else if($act=="table_detail_filter"){
   $param  = $_REQUEST['param'];
   include "view/ajax_table/pelayanan_table_data_unar_filter.php";
}
else if($act=="table_all_filter"){
   $param  = $_REQUEST['param'];
   include "view/ajax_table/pelayanan_table_data_unar_filter_all.php";
}
else if($act=="import_all"){
   //File
    $target = basename($_FILES['lampiran']['name']);
    move_uploaded_file($_FILES['lampiran']['tmp_name'], $target);
    $path_info = pathinfo($target);
    $ext =  strtolower($path_info['extension']); 
    
    if($ext!="xls"){
      echo "Format file salah";
      die();
    }

    require_once 'lib/excel_reader.php';
    $data = new Spreadsheet_Excel_Reader($_FILES['lampiran']['name'],false);
    $baris = $data->rowcount($sheet_index=0);
    for ($i=3; $i<=$baris; $i++) {
      //echo "tgl_ujian : ".$i." - ".$data->val($i, 3)."<br/>";
        $insert_data = array();
        $insert_data['id_prov']             = $data->val($i, 1);
        $insert_data['kode_upt']            = $data->val($i, 2);
        $insert_data['tgl_ujian']           = $data->val($i, 3);
        $insert_data['lokasi_ujian']        = $data->val($i, 4);
        $insert_data['id_kabkot']           = $data->val($i, 5);

        $insert_data['jumlah_yd']           = $data->val($i, 6);
        $insert_data['jumlah_yc']           = $data->val($i, 7);
        $insert_data['jumlah_yb']           = $data->val($i, 8);

        $insert_data['lulus_yd']            = $data->val($i, 9);
        $insert_data['lulus_yc']            = $data->val($i, 10);
        $insert_data['lulus_yb']            = $data->val($i, 11);

        $insert_data['tdk_lulus_yd']        = $data->val($i, 12);
        $insert_data['tdk_lulus_yc']        = $data->val($i, 13);
        $insert_data['tdk_lulus_yb']        = $data->val($i, 14);

       
        $insert_data['keterangan']          = $data->val($i, 15);
        $insert_data['created_date']        = $date_now_indo_full;
        $insert_data['last_update']         = $date_now_indo_full;
        $insert_data['created_by']          = $_SESSION['kode_user'];
        $insert_data['last_update_by']      = $_SESSION['kode_user'];

        if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
          $insert_data['status']              = "0";
        }
        else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
          $insert_data['status']              = "3";
        }
        else {
          $insert_data['status']              = "3";
        }
         $insert_data['kode_unar']        = "un_".date("ymdhis")."_".rand(1000,9999);
         if(!empty($data->val($i, 1))){
            $gen_model->Insert('tr_unar',$insert_data);

            //For Notification All
             $upt=$insert_data['kode_upt'];
             $upt_array[$upt][] =  array (
                                    'kode' => $insert_data['kode_unar'],
                                    'prov' => $insert_data['id_prov'],
                                    'upt'  => $upt
                                  );
         }
         
    }
   
     $keys = array_keys($upt_array);
      for($i = 0; $i < count($upt_array); $i++) {
          $hitung_files = 0;
          $kode  = "";
          foreach($upt_array[$keys[$i]] as $key => $value) {
             $kode .= $value['kode'].",";
              $upt   = $value['upt'];
              $prov  = $value['prov'];
              $hitung_files++;
          }
          $kode = trim($kode,',');
          // echo "Id Prov : ".$prov."<br/>";
          // echo "UPT : ".$upt."<br/>";
          // echo "Kode : ".$kode."<br/>";
          // echo "Total : ".$hitung_files."<br/><br/>";

          //Notification
           if($hitung_files!=0){
               if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                       notifikasi('2','6',$prov,$upt,'grp_171026194411_1143','UNAR','Ada '.$hitung_files.' data baru pada UNAR yang harus di tinjau',$kode);
                    }
                    else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                      notifikasi('4','6',$prov,$upt,'grp_171026194411_1142','UNAR','Ada '.$hitung_files.' data baru pada UNAR yang sudah di setujui',$kode);
                    }
                    else {
                     foreach ($group_ntf as $id_group) {
                         notifikasi('1','6',$prov,$upt,$id_group,'UNAR','Ada '.$hitung_files.' data baru pada UNAR',$kode);
                      }
                    }
          }
      }
    echo "OK";
    
    unlink($_FILES['lampiran']['name']);
}
else if($act=="import_detail"){
   //File
    $target = basename($_FILES['lampiran']['name']);
    move_uploaded_file($_FILES['lampiran']['tmp_name'], $target);
    $path_info = pathinfo($target);
    $ext =  strtolower($path_info['extension']); 
    
    if($ext!="xls"){
      echo "Format file salah";
      die();
    }

    require_once 'lib/excel_reader.php';
    $data = new Spreadsheet_Excel_Reader($_FILES['lampiran']['name'],false);
    $baris = $data->rowcount($sheet_index=0);
    $kode="";
    $hitung_files=0;
    for ($i=3; $i<=$baris; $i++) {
        $insert_data = array();
        $insert_data['id_prov']             = $_POST['id_prov'];
        $insert_data['kode_upt']            = $_POST['loket_upt'];
        $insert_data['tgl_ujian']           = $data->val($i, 1);
        $insert_data['lokasi_ujian']        = $data->val($i, 2);
        $insert_data['id_kabkot']           = $data->val($i, 3);

        $insert_data['jumlah_yd']           = $data->val($i, 4);
        $insert_data['jumlah_yc']           = $data->val($i, 5);
        $insert_data['jumlah_yb']           = $data->val($i, 6);

        $insert_data['lulus_yd']            = $data->val($i, 7);
        $insert_data['lulus_yc']            = $data->val($i, 8);
        $insert_data['lulus_yb']            = $data->val($i, 9);

        $insert_data['tdk_lulus_yd']        = $data->val($i, 10);
        $insert_data['tdk_lulus_yc']        = $data->val($i, 11);
        $insert_data['tdk_lulus_yb']        = $data->val($i, 12);
       
        $insert_data['keterangan']          = $data->val($i, 13);
        $insert_data['created_date']        = $date_now_indo_full;
        $insert_data['last_update']         = $date_now_indo_full;
        $insert_data['created_by']          = $_SESSION['kode_user'];
        $insert_data['last_update_by']      = $_SESSION['kode_user'];

        if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
          $insert_data['status']              = "0";
        }
        else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
          $insert_data['status']              = "3";
        }
        else {
          $insert_data['status']              = "3";
        }
         $insert_data['kode_unar']        = "un_".date("ymdhis")."_".rand(1000,9999);
         if(!empty($data->val($i, 1))){  
            $gen_model->Insert('tr_unar',$insert_data);
            $kode .= $insert_data['kode_unar'].",";
            $hitung_files++;
         }
    }
    $kode = rtrim($kode,",");
    //Notification
    if($hitung_files!=0){
         if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                notifikasi('2','6',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1143','UNAR','Ada '.$hitung_files.' data baru pada UNAR yang harus di tinjau',$kode);
              }
              else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                notifikasi('4','6',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1142','UNAR','Ada '.$hitung_files.' data baru pada UNAR yang sudah di setujui',$kode);
              }
              else {
                foreach ($group_ntf as $id_group) {
                   notifikasi('1','6',$_POST['id_prov'],$_POST['loket_upt'],$id_group,'UNAR','Ada '.$hitung_files.' data baru pada UNAR',$kode);
                }
              }
    }
    echo "OK";
    
    unlink($_FILES['lampiran']['name']);
}
else if($act=="add"){

    if(!empty($_SESSION['kode_user'])){
      //Proses
      $insert_data = array();
      $insert_data['kode_unar']        = "un_".date("ymdhis")."_".rand(1000,9999);
      $insert_data['id_prov']             = get($_POST['id_prov']);
      $insert_data['kode_upt']            = get($_POST['loket_upt']);
      $insert_data['tgl_ujian']           = (!empty($_POST['tgl_unar']) ? $gen_controller->date_indo_default($_POST['tgl_unar']) : null);
      $insert_data['lokasi_ujian']        = get($_POST['lokasi_ujian']);
      $insert_data['id_kabkot']           = get($_POST['kabkot']);

      $insert_data['jumlah_yd']           = (!empty($_POST['jumlah_yd']) ? $_POST['jumlah_yd'] : null);
      $insert_data['jumlah_yb']           = (!empty($_POST['jumlah_yb']) ?  $_POST['jumlah_yb'] : null);
      $insert_data['jumlah_yc']           = (!empty($_POST['jumlah_yc']) ? $_POST['jumlah_yc'] : null); 

      $insert_data['lulus_yd']            = (!empty($_POST['lulus_yd']) ? $_POST['lulus_yd'] : null);
      $insert_data['lulus_yb']            = (!empty($_POST['lulus_yb']) ? $_POST['lulus_yb'] : null);
      $insert_data['lulus_yc']            = (!empty($_POST['lulus_yc']) ? $_POST['lulus_yc'] : null);

      $insert_data['tdk_lulus_yd']        = (!empty($_POST['tdk_lulus_yd']) ? $_POST['tdk_lulus_yd'] : null);
      $insert_data['tdk_lulus_yb']        = (!empty($_POST['tdk_lulus_yb']) ?  $_POST['tdk_lulus_yb'] : null);
      $insert_data['tdk_lulus_yc']        = (!empty($_POST['tdk_lulus_yc']) ? $_POST['tdk_lulus_yc'] : null);
     
      $insert_data['keterangan']          = get($_POST['keterangan']);
      $insert_data['created_date']        = $date_now_indo_full;
      $insert_data['last_update']         = $date_now_indo_full;
      $insert_data['created_by']          = $_SESSION['kode_user'];
      $insert_data['last_update_by']      = $_SESSION['kode_user'];

      if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
        $insert_data['status']              = "0";
      }
      else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
        $insert_data['status']              = "3";
      }
      else {
        $insert_data['status']              = "3";
      }


        if($_POST['id_prov']=="All"){
          $insert_data['id_prov'] ="";
        }
        if($_POST['loket_upt']=="All"){
          $insert_data['kode_upt']="";
       }
       if($_POST['kabkot']=="All"){
          $insert_data['id_kabkot']="";
       }

       //Validation
      if($insert_data['lokasi_ujian']!=""){
           if($gen_model->Insert('tr_unar',$insert_data)=="OK"){
                   //Notification
                  if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
                    notifikasi('2','6s',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1143','UNAR','Ada 1 data baru pada UNAR yang harus di tinjau',$insert_data['kode_unar']);
                  }
                  else if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
                    notifikasi('4','6',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1142','UNAR','Ada 1 data baru pada UNAR yang sudah di setujui',$insert_data['kode_unar']);
                  }
                  else {
                    foreach ($group_ntf as $id_group) {
                       notifikasi('1','6',$_POST['id_prov'],$_POST['loket_upt'],$id_group,'UNAR','Ada 1 data baru pada UNAR',$insert_data['kode_unar']);
                    }
                  }
               echo "OK";
          }
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="edit" and $id_parameter!=""){
    $edit  = $gen_model->GetOneRow("tr_unar",array('kode_unar'=>$id_parameter)); 
    foreach($edit as $key=>$val){
                  $key=strtolower($key);
                  $$key=$val;
    }
    $data = array(
      'kode_unar'=>$kode_unar,
      'tanggal'=>$gen_controller->get_date_indonesia($tgl_ujian),
      'lokasi_ujian'=>$lokasi_ujian,
      'id_kabkot'=>$id_kabkot,
      'jumlah_yd'=>$jumlah_yd,
      'jumlah_yc'=>$jumlah_yc,
      'jumlah_yb'=>$jumlah_yb,
      'lulus_yd'=>$lulus_yd,
      'lulus_yc'=>$lulus_yc,
      'lulus_yb'=>$lulus_yb,
      'tdk_lulus_yd'=>$tdk_lulus_yd,
      'tdk_lulus_yc'=>$tdk_lulus_yc,
      'tdk_lulus_yb'=>$tdk_lulus_yb,
      'keterangan'=>$keterangan,
      'kode_upt'=>$kode_upt,
      'id_prov'=>$id_prov
    );
    echo json_encode($data); 
}
else if($act=="update"){
    if(!empty($_SESSION['kode_user'])){
      //Proses
      $update_data = array();
      $update_data['id_prov']             = get($_POST['id_prov']);
      $update_data['kode_upt']            = get($_POST['loket_upt']);
      $update_data['tgl_ujian']           = (!empty($_POST['tgl_unar']) ? $gen_controller->date_indo_default($_POST['tgl_unar']) : null);
      $update_data['lokasi_ujian']        = get($_POST['lokasi_ujian']);
      $update_data['id_prov']             = get($_POST['id_prov']);
      $update_data['id_kabkot']           = get($_POST['kabkot']);

      $update_data['jumlah_yd']           = (!empty($_POST['jumlah_yd']) ? $_POST['jumlah_yd'] : null);
      $update_data['jumlah_yb']           = (!empty($_POST['jumlah_yb']) ?  $_POST['jumlah_yb'] : null);
      $update_data['jumlah_yc']           = (!empty($_POST['jumlah_yc']) ? $_POST['jumlah_yc'] : null); 

      $update_data['lulus_yd']            = (!empty($_POST['lulus_yd']) ? $_POST['lulus_yd'] : null);
      $update_data['lulus_yb']            = (!empty($_POST['lulus_yb']) ? $_POST['lulus_yb'] : null);
      $update_data['lulus_yc']            = (!empty($_POST['lulus_yc']) ? $_POST['lulus_yc'] : null);

      $update_data['tdk_lulus_yd']        = (!empty($_POST['tdk_lulus_yd']) ? $_POST['tdk_lulus_yd'] : null);
      $update_data['tdk_lulus_yb']        = (!empty($_POST['tdk_lulus_yb']) ?  $_POST['tdk_lulus_yb'] : null);
      $update_data['tdk_lulus_yc']        = (!empty($_POST['tdk_lulus_yc']) ? $_POST['tdk_lulus_yc'] : null);
     
      $update_data['keterangan']          = get($_POST['keterangan']);
      $update_data['last_update']         = $date_now_indo_full;
      $update_data['last_update_by']      = $_SESSION['kode_user'];


      if($_SESSION['group_user']=="grp_171026194411_1144"){  //Operator
        $update_data['status']              = "0";
      }


        if($_POST['id_prov']=="All"){
          $update_data['id_prov'] ="";
        }
        if($_POST['loket_upt']=="All"){
          $update_data['kode_upt']="";
       }
       if($_POST['kabkot']=="All"){
          $update_data['id_kabkot']="";
       }

       //Paramater
      $where_data = array();
      $where_data['kode_unar']       = $_POST['kode_unar'];


       //Validation
      if(!empty($_POST['kode_unar'])){
           if($gen_model->Update('tr_unar',$update_data,$where_data)=="OK"){
                   //Notification
                  if($_SESSION['group_user']=="grp_171026194411_1142"){  //Operator
                      notifikasi('2','6s',$_POST['id_prov'],$_POST['loket_upt'],'grp_171026194411_1143','UNAR','Ada 1 data baru pada UNAR yang harus di tinjau',$_POST['kode_unar']);
                  }
               echo "OK";
          }
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'NOT_LOGIN';
    }
}
else if($act=="delete"){
    if(!empty($_SESSION['kode_user'])){

       $update_data['status']              = "2"; //Delete

       //Paramater
      $where_data = array();
      $where_data['kode_unar']       = $_POST['kode_unar'];


       //Validation
      if(!empty($_POST['kode_unar'])){
            echo $gen_model->Update('tr_unar',$update_data,$where_data);
      }
      else {
          echo 'Terjadi kesalahan';
      }
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="verifikasi"){
    if(!empty($_SESSION['kode_user'])){
       $db->Execute("update tr_unar set status='3' where kode_unar in(".$_REQUEST['kode'].") ");
        $total = $_REQUEST['total'];
        if(!empty($total)){
        //Notification
          if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
            $kode=str_replace("'","",$_REQUEST['kode']);
            notifikasi('4','6',$_SESSION['upt_provinsi'],$_SESSION['kode_upt'],'grp_171026194411_1142','UNAR','Ada '.$total.' data baru pada UNAR yang sudah di setujui',$kode);
          }
        }
       echo "OK";
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="reject"){
    if(!empty($_SESSION['kode_user'])){
       $db->Execute("update tr_unar set status='1',ket_sts_apr_reject='".$_REQUEST['ket']."' where kode_unar in(".$_REQUEST['kode'].") ");
       $total = $_REQUEST['total'];
        if(!empty($total)){
        //Notification
          if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
            $kode=str_replace("'","",$_REQUEST['kode']);
              foreach ($group_ntf as $id_group) {
                      notifikasi('3','6',$_SESSION['upt_provinsi'],$_SESSION['kode_upt'],$id_group,'UNAR','Ada '.$total.' data baru pada UNAR yang di reject',$kode);
               }
          }
        }
       echo "OK";
    }
    else {
      echo 'Session tidak valid, silahkan login kembali';
    }
}
else if($act=="rest_waiting"){
  $prov = $_REQUEST['id_prov'];
  $upt  = $_REQUEST['upt'];
  
     $aColumns = array('un.created_date','un.status','un.tgl_ujian','un.lokasi_ujian','kb.nama_kab_kot','un.jumlah_yd','un.jumlah_yc','un.jumlah_yb','un.lulus_yd','un.lulus_yc','un.lulus_yb','un.tdk_lulus_yd','un.tdk_lulus_yb','un.tdk_lulus_yc','un.keterangan','un.kode_unar'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

   $rResult              = $md_pelayanan->getUnarWaiting($sWhere,$sOrder,$sLimit,$prov,$upt);
   $rResultFilterTotal   = $md_pelayanan->getCountUnarWaiting($sWhere,$prov,$upt);

  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
    $wilayah ="";
    if(!empty($aRow['id_prov'])){
     $wilayah = $gen_model->GetOne("id_map","ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    }

    $param_id = $aRow['kode_unar'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_unar(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';
    
  

    $checkbox="";
    if($_SESSION['group_user']=="grp_171026194411_1143"){ //Kepala UPT
         if($aRow['status']!="4"){
             $checkbox = '<input type="checkbox" value="'.$param_id.'" name="kode_unar">';
          }
    }
   
    $sts = "";
    if($aRow['status']=="0"){
      $sts="<span style='color:orange;font-weight:bold'>Waiting Verification</span> ";
    }
    else if($aRow['status']=="1"){
      $sts="<span style='color:red;font-weight:bold'>Reject</span> <br/><center>".$aRow['ket_sts_apr_reject']."</center>";
    }
    else if($aRow['status']=="4"){
      $sts="<span style='font-weight:bold'>Waiting For Update</span>";
    }

     //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));


    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
              $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_unar(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
              $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_unar(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
             }
        }
        else {
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_unar(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                 $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_unar(\''.base64_encode('kode_unar').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
               $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_unar(\''.base64_encode('kode_unar').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
             }
        }
        else {
           $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_unar(\''.base64_encode('kode_unar').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($checkbox,$gen_controller->get_date_indonesia($aRow['created_date']),$sts,$gen_controller->get_date_indonesia($aRow['tgl_ujian']),$aRow['lokasi_ujian'],$aRow['nama_kab_kot'],$aRow['jumlah_yd'],$aRow['jumlah_yc'],$aRow['jumlah_yb'],$aRow['lulus_yd'],$aRow['lulus_yc'],$aRow['lulus_yb'],$aRow['tdk_lulus_yd'],$aRow['tdk_lulus_yc'],$aRow['tdk_lulus_yb'],$aRow['keterangan'],"<center>".$edit_delete."</center>");

    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest"){
    $aColumns = array('un.created_date','un.id_prov','upt.nama_upt','un.tgl_ujian','un.lokasi_ujian','kb.nama_kab_kot','un.jumlah_yd','un.jumlah_yc','un.jumlah_yb','un.lulus_yd','un.lulus_yc','un.lulus_yb','un.tdk_lulus_yd','un.tdk_lulus_yb','un.tdk_lulus_yc','un.keterangan','un.kode_unar'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getUnar($sWhere,$sOrder,$sLimit);
  $rResultFilterTotal   = $md_pelayanan->getCountUnar($sWhere);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
     $wilayah ="";
    if(!empty($aRow['id_prov'])){
     $wilayah = $gen_model->GetOne("id_map","ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    }
    $param_id = $aRow['kode_unar'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_unar_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_unar_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_unar_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
             }
        }
        else {
          $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_unar_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                 $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_unar_all(\''.base64_encode('kode_unar').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                 $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_unar_all(\''.base64_encode('kode_unar').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
             }
        }
        else {
           $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_unar_all(\''.base64_encode('kode_unar').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }

    $nama_prov = $aRow['id_prov'];
    $nama_upt  = $aRow['kode_upt'];
    $nama_kab_kot  = $aRow['id_kabkot'];

    if($nama_prov=="Only_Admin"){
      $nama_prov = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_prov)){
      $nama_prov = "<center>Semua Provinsi</center>";
    } 
    else {
      $nama_prov  = $aRow['nama_prov'];
    } 

    if($nama_upt=="Only_Admin"){
      $nama_upt = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_upt)){
      $nama_upt = "<center>Semua UPT</center>";
    } 
    else {
      $nama_upt  = $aRow['nama_upt'];
    }   

    if($nama_kab_kot=="Only_Admin"){
      $nama_kab_kot = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_kab_kot)){
      $nama_kab_kot = "<center>Semua Kab/Kota</center>";
    } 
    else {
      $nama_kab_kot  = $aRow['nama_kab_kot'];
    }  

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$nama_prov,$nama_upt,$gen_controller->get_date_indonesia($aRow['tgl_ujian']),$aRow['lokasi_ujian'],$nama_kab_kot,$aRow['jumlah_yd'],$aRow['jumlah_yc'],$aRow['jumlah_yb'],$aRow['lulus_yd'],$aRow['lulus_yc'],$aRow['lulus_yb'],$aRow['tdk_lulus_yd'],$aRow['tdk_lulus_yc'],$aRow['tdk_lulus_yb'],$aRow['keterangan'],"<center>".$edit_delete."</center>");
    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_filter"){
  $param = $_REQUEST['param'];
  $aColumns = array('un.created_date','un.id_prov','upt.nama_upt','un.tgl_ujian','un.lokasi_ujian','kb.nama_kab_kot','un.jumlah_yd','un.jumlah_yc','un.jumlah_yb','un.lulus_yd','un.lulus_yc','un.lulus_yb','un.tdk_lulus_yd','un.tdk_lulus_yb','un.tdk_lulus_yc','un.keterangan','un.kode_unar'); //Kolom Pada Tabel

    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getUnarFilter($sWhere,$sOrder,$sLimit,$param);
  $rResultFilterTotal   = $md_pelayanan->getCountUnarFilter($sWhere,$param);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
     $wilayah ="";
    if(!empty($aRow['id_prov'])){
     $wilayah = $gen_model->GetOne("id_map","ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    }
    $param_id = $aRow['kode_unar'];

   

    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_unar_all(\''.$param_id.'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

    $edit="";
    $delete="";
    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['kode_upt']==$aRow['kode_upt']){
              if($_SESSION['group_user']=="grp_171026194411_1143"){
                  $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_unar_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
              }
              else if($aRow['created_by']==$_SESSION['kode_user']){
                  $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_unar_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
               }
            }
        }
        else {
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_unar_all(\''.$param_id.'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['kode_upt']==$aRow['kode_upt']){
                if($_SESSION['group_user']=="grp_171026194411_1143"){
                      $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_unar_all(\''.base64_encode('kode_unar').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                }
                else if($aRow['created_by']==$_SESSION['kode_user']){
                    $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_unar_all(\''.base64_encode('kode_unar').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                 }
           }
        }
        else {
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_unar_all(\''.base64_encode('kode_unar').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }

    $nama_prov = $aRow['id_prov'];
    $nama_upt  = $aRow['kode_upt'];
    $nama_kab_kot  = $aRow['id_kabkot'];

    if($nama_prov=="Only_Admin"){
      $nama_prov = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_prov)){
      $nama_prov = "<center>Semua Provinsi</center>";
    } 
    else {
      $nama_prov  = $aRow['nama_prov'];
    } 

    if($nama_upt=="Only_Admin"){
      $nama_upt = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_upt)){
      $nama_upt = "<center>Semua UPT</center>";
    } 
    else {
      $nama_upt  = $aRow['nama_upt'];
    }   

    if($nama_kab_kot=="Only_Admin"){
      $nama_kab_kot = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_kab_kot)){
      $nama_kab_kot = "<center>Semua Kab/Kota</center>";
    } 
    else {
      $nama_kab_kot  = $aRow['nama_kab_kot'];
    }  

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$nama_prov,$nama_upt,$gen_controller->get_date_indonesia($aRow['tgl_ujian']),$aRow['lokasi_ujian'],$nama_kab_kot,$aRow['jumlah_yd'],$aRow['jumlah_yc'],$aRow['jumlah_yb'],$aRow['lulus_yd'],$aRow['lulus_yc'],$aRow['lulus_yb'],$aRow['tdk_lulus_yd'],$aRow['tdk_lulus_yc'],$aRow['tdk_lulus_yb'],$aRow['keterangan'],"<center>".$edit_delete."</center>");

    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_provinsi_filter"){
  $param = $_REQUEST['param'];
     $aColumns = array('un.created_date','un.id_prov','upt.nama_upt','un.tgl_ujian','un.lokasi_ujian','kb.nama_kab_kot','un.jumlah_yd','un.jumlah_yc','un.jumlah_yb','un.lulus_yd','un.lulus_yc','un.lulus_yb','un.tdk_lulus_yd','un.tdk_lulus_yb','un.tdk_lulus_yc','un.keterangan','un.kode_unar'); //Kolom Pada Tabel



    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getUnarFilter($sWhere,$sOrder,$sLimit,$param);
  $rResultFilterTotal   = $md_pelayanan->getCountUnarFilter($sWhere,$param);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

   while($aRow = $rResult->FetchRow()){
     $wilayah ="";
    if(!empty($aRow['id_prov'])){
     $wilayah = $gen_model->GetOne("id_map","ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    }
    $param_id = $aRow['kode_unar'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_unar(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

    //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));

    $edit="";
    $delete="";

    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_unar(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_unar(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
             }
        }
        else {
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_unar(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['group_user']=="grp_171026194411_1143"){
                    $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_unar(\''.base64_encode('kode_unar').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
            }
            else if($aRow['created_by']==$_SESSION['kode_user']){
                  $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_unar(\''.base64_encode('kode_unar').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
             }
        }
        else {
              $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_unar(\''.base64_encode('kode_unar').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }


    $nama_prov = $aRow['id_prov'];
    $nama_upt  = $aRow['kode_upt'];
    $nama_kab_kot  = $aRow['id_kabkot'];

    if($nama_prov=="Only_Admin"){
      $nama_prov = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_prov)){
      $nama_prov = "<center>Semua Provinsi</center>";
    } 
    else {
      $nama_prov  = $aRow['nama_prov'];
    } 

    if($nama_upt=="Only_Admin"){
      $nama_upt = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_upt)){
      $nama_upt = "<center>Semua UPT</center>";
    } 
    else {
      $nama_upt  = $aRow['nama_upt'];
    }   

    if($nama_kab_kot=="Only_Admin"){
      $nama_kab_kot = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_kab_kot)){
      $nama_kab_kot = "<center>Semua Kab/Kota</center>";
    } 
    else {
      $nama_kab_kot  = $aRow['nama_kab_kot'];
    }  

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$nama_prov,$nama_upt,$gen_controller->get_date_indonesia($aRow['tgl_ujian']),$aRow['lokasi_ujian'],$nama_kab_kot,$aRow['jumlah_yd'],$aRow['jumlah_yc'],$aRow['jumlah_yb'],$aRow['lulus_yd'],$aRow['lulus_yc'],$aRow['lulus_yb'],$aRow['tdk_lulus_yd'],$aRow['tdk_lulus_yc'],$aRow['tdk_lulus_yb'],$aRow['keterangan'],"<center>".$edit_delete."</center>");

    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else if($act=="rest_provinsi"){
  $prov = $_REQUEST['id_prov'];
  $upt = $_REQUEST['upt'];
   $aColumns = array('un.created_date','un.id_prov','upt.nama_upt','un.tgl_ujian','un.lokasi_ujian','kb.nama_kab_kot','un.jumlah_yd','un.jumlah_yc','un.jumlah_yb','un.lulus_yd','un.lulus_yc','un.lulus_yb','un.tdk_lulus_yd','un.tdk_lulus_yb','un.tdk_lulus_yc','un.keterangan','un.kode_unar'); //Kolom Pada Tabel



    // Input method (use $_GET, $_POST or $_REQUEST)
  $input =& $_POST;
  $iColumnCount = count($aColumns);


  $sLimit = $gen_controller->Paging($input);
  $sOrder = $gen_controller->Ordering($input, $aColumns );
  $sWhere = $gen_controller->Filtering($aColumns, $iColumnCount, $input);

  $aQueryColumns = array();
  foreach ($aColumns as $col) {
    if ($col != ' ') {
      $aQueryColumns[] = $col;
    }
  }

  $rResult              = $md_pelayanan->getUnar($sWhere,$sOrder,$sLimit,$prov,$upt);
  $rResultFilterTotal   = $md_pelayanan->getCountUnar($sWhere,$prov,$upt);


  $output = array(
    "sEcho"                => (empty($input['sEcho']) ? '0' : intval($input['sEcho'])),
    "iTotalRecords"        => $rResultFilterTotal,
    "iTotalDisplayRecords" => $rResultFilterTotal,
    "aaData"               => array(),
  );

  while($aRow = $rResult->FetchRow()){
     $wilayah ="";
    if(!empty($aRow['id_prov'])){
     $wilayah = $gen_model->GetOne("id_map","ms_provinsi",array('id_prov'=>$aRow['id_prov']));
    }
    $param_id = $aRow['kode_unar'];
    
    $detail = '<button title="Detail"  type="button" onclick="do_detail_unar(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-success btn-sm"><i class="fa fa-file"></i> </button>';

     //check group
    $grp_usr = $gen_model->GetOne("group_user","ms_user",array('kode_user'=>$aRow['created_by']));

    $edit="";
    $delete="";

    if($akses_tab['fua_edit']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['kode_upt']==$aRow['kode_upt']){
                 if($_SESSION['group_user']=="grp_171026194411_1143"){
                  $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_unar(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
                  }
                  else if($aRow['created_by']==$_SESSION['kode_user']){
                      $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_unar(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
                   }
            }
        }
        else {
            $edit = '&nbsp; <button title="Edit"  type="button" onclick="do_edit_unar(\''.$param_id.'\',\''.$wilayah.'\',\''.$aRow['kode_upt'].'\')" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> </button>';
        }
    }
    if($akses_tab['fua_delete']=="1"){ 
         //Kepala UPT atau Operator UPT
        if($_SESSION['group_user']=="grp_171026194411_1143" or $_SESSION['group_user']=="grp_171026194411_1144"){
            if($_SESSION['kode_upt']==$aRow['kode_upt']){
                 if($_SESSION['group_user']=="grp_171026194411_1143"){
                  $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_unar(\''.base64_encode('kode_unar').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                  }
                  else if($aRow['created_by']==$_SESSION['kode_user']){
                      $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_unar(\''.base64_encode('kode_unar').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
                   }
            }
        }
        else {
            $delete = '&nbsp; <button title="Hapus" type="button" onclick="do_delete_unar(\''.base64_encode('kode_unar').'\',\''.base64_encode($param_id).'\')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>';
        }
    }


    $nama_prov = $aRow['id_prov'];
    $nama_upt  = $aRow['kode_upt'];
    $nama_kab_kot  = $aRow['id_kabkot'];

    if($nama_prov=="Only_Admin"){
      $nama_prov = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_prov)){
      $nama_prov = "<center>Semua Provinsi</center>";
    } 
    else {
      $nama_prov  = $aRow['nama_prov'];
    } 

    if($nama_upt=="Only_Admin"){
      $nama_upt = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_upt)){
      $nama_upt = "<center>Semua UPT</center>";
    } 
    else {
      $nama_upt  = $aRow['nama_upt'];
    }   

    if($nama_kab_kot=="Only_Admin"){
      $nama_kab_kot = "<center>Hanya Admin</center>";
    }  
    else if(empty($nama_kab_kot)){
      $nama_kab_kot = "<center>Semua Kab/Kota</center>";
    } 
    else {
      $nama_kab_kot  = $aRow['nama_kab_kot'];
    }  

    $edit_delete = $detail.$edit.$delete;
    $row = array();
    $row = array($gen_controller->get_date_indonesia($aRow['created_date']),$nama_prov,$nama_upt,$gen_controller->get_date_indonesia($aRow['tgl_ujian']),$aRow['lokasi_ujian'],$nama_kab_kot,$aRow['jumlah_yd'],$aRow['jumlah_yc'],$aRow['jumlah_yb'],$aRow['lulus_yd'],$aRow['lulus_yc'],$aRow['lulus_yb'],$aRow['tdk_lulus_yd'],$aRow['tdk_lulus_yc'],$aRow['tdk_lulus_yb'],$aRow['keterangan'],"<center>".$edit_delete."</center>");


    $output['aaData'][] = $row;
  }
  echo json_encode($output);
}
else {
  $gen_controller->response_code(http_response_code());
}
?>