<?php 
//Show PHP Version
/*  
   phpinfo();  
   die();
*/

//Hide Error
//error_reporting(null);

include('lib/adodb/adodb.inc.php');
 
// define variabel database
$dbhostname		= "127.0.0.1";
$dbusername		= "root";
// $dbpassword		= "";
$dbpassword		= "Tifanfajar29";
$dbname			= "pelaporan_pelayanan";

$db = ADONewConnection('mysqli'); // eg. 'mysql' or 'oci8'

// $db->debug = true;
 
// Koneksi Ke Database
//$db->Connect($server, $user, $password, $database);

$status  =  $db->PConnect($dbhostname, $dbusername, $dbpassword, $dbname);
$db->SetFetchMode(ADODB_FETCH_ASSOC);


if (!$status ) {
	echo '<h1>Connection Failed</h1>' . $db->ErrorMsg();
}


//Hapus atau Berikan Komentar jika ingin menggunakan jam server
date_default_timezone_set("Asia/Jakarta");
$root_folder   = $_SERVER['DOCUMENT_ROOT']."/";
$basepath      = "http://".$_SERVER['HTTP_HOST']."/siput/";
$secret_key    = "app_absenkey";
$date_now_indo = date("Y-m-d");
$date_now_indo_full = date("Y-m-d H:i:s");

function get($var){
	return str_replace("'","''",$var);
}
function only_number($nmbr){
	if(!empty($nmbr)){
		return preg_replace('/[^0-9]/', '',$nmbr);
	}
}
function rp_int($var){
	$var_result =  str_replace(".","",$var);
	return str_replace(",","",$var_result);
}
function int_to_rp($var){
	if(is_numeric($var)){
		return number_format($var,0,".",".");
	}
	else {
		return $var;
	}	
}
function convertMonthToNumber($monthName) {
      return date('m', strtotime($monthName));
}
function get_grp_notif($id_menu,$id_tab){
	 global $db,$gen_model;
	 $rtr_grp ="";
	 $dt_grp = $db->SelectLimit("select fc.kode_function_access,fc.fua_read,fc_tab.fua_read as read_tab from tr_function_access as fc
		INNER JOIN tr_function_access_tab as fc_tab
				on fc_tab.kode_function_access=fc.kode_function_access
	where fc.id_menu='".$id_menu."' and fc.fua_read='1' and fc_tab.id_tab='".$id_tab."' and fc_tab.fua_read='1' 
AND fc.kode_function_access NOT IN (
						'grp_171026194411_1142',
						'grp_171026194411_1143'
					)
AND fc_tab.kode_function_access NOT IN (
						'grp_171026194411_1142',
						'grp_171026194411_1143'
					)"); 
		while($list = $dt_grp->FetchRow()){
			foreach($list as $key=>$val){
            $key=strtolower($key);
            $$key=$val;
          }  
			$rtr_grp .= $kode_function_access.",";
 		}
 	return $rtr_grp;
}

function notifikasi($tipe_notif,$jenis,$id_prov,$upt,$group,$url,$judul,$data_notif){
  global $db,$date_now_indo_full,$gen_model;
  	  $insert_data = array();
	  $insert_data['kode_notif']   			  = "ntf_".date("ymdhis")."_".rand(1000,9999);
	  $insert_data['status']            	  = "0";
	  $insert_data['jenis']           		  = $jenis;
	  $insert_data['tipe_notif']              = $tipe_notif;
	  $insert_data['id_prov']              	  = $id_prov;
	  $insert_data['kode_upt']                = $upt;
	  $insert_data['to_group_user']           = $group;
	  $insert_data['url']              		  = $url;
	  $insert_data['judul']              	  = $judul;
	  $insert_data['data_notif']              = $data_notif;

	  $insert_data['created_date']            = $date_now_indo_full;
      $insert_data['last_update']             = $date_now_indo_full;
      $insert_data['created_by']              = $_SESSION['kode_user'];
      $insert_data['last_update_by']          = $_SESSION['kode_user'];

      $gen_model->Insert('tr_notif',$insert_data);
}

class General_Controller {
	//example : check_session()
	function check_session(){
		global $basepath;
	   if(empty($_SESSION['username'])){
		  echo '<script>document.location="'.$basepath.'"</script>';
		}
	}
	//example : response_code('403')
	function response_code($respon_code){
		global $root_folder;
		  if($respon_code!="200"){
		        $error_pages = $root_folder.'ErrorDocument';
		        $pages_err   = scandir($error_pages, 0);
		        unset($pages_err[0], $pages_err[1]);
		        if(in_array($respon_code.'.php', $pages_err)){
		            include($error_pages.'/'.$respon_code.'.php');
		        }
		        else {
		            include "ErrorDocument/404.php"; 
		        }
		   }
		   else {
		        include "ErrorDocument/404.php"; 
		   }
	}


	//example : selisih_jam('08:00','08:30')
	function selisih_jam($jam_mulai,$jam_selesai){
		$start = strtotime($jam_mulai);
		$end   = strtotime($jam_selesai);
		$diff  = $end - $start;

		$hours = floor($diff / (60 * 60));
		$minutes = $diff - $hours * (60 * 60);
		if(empty($jam_mulai) or empty($jam_selesai)){
			return "";
		}
		else {
			return $hours .  ' Jam, ' . floor( $minutes / 60 ) . ' Menit';
		}
	}

	//example : count_time('08:00','08:30')
	function count_time($jam_mulai,$jam_selesai){
		if(empty($jam_mulai) or empty($jam_selesai)){
			return 0;
		}
		else {
		    $array1 = explode(':', $jam_mulai);
		    $array2 = explode(':', $jam_selesai);
		    $minutes1 = ($array1[0] * 60.0 + $array1[1]);
		    $minutes2 = ($array2[0] * 60.0 + $array2[1]);
			$hasil = $minutes2 - $minutes1;
			return $hasil;
		}
	}

	//example : convertMinsToHoursMins('180')
	function convertToHoursMins($time,$format) {
	    if ($time < 1) {
	        return;
	    }
	    $hours = floor($time / 60);
	    $minutes = ($time % 60);
	    if($format=="full"){
	    	return $hours .  ' Jam, ' .$minutes. ' Menit';
	    }
	    else {
	    	return $hours.','.$minutes;
	    }
	}

	//get_biaya mengajar 
	function biaya_mengajar($waktu,$format) {
		global $gen_model;
	    //get biaya mengajar
        $biaya = $gen_model->GetOneRow('ms_biaya_mengajar');
        $wk = explode(',', $waktu);
        $proses = 0;
        $proses += $wk[0] * $biaya['biaya_ngajar'];

        if($wk[1]>=$biaya['pembulatan']){
        	$proses += $biaya['biaya_ngajar']; 
        }
      
        if($format=="rp"){
        	return number_format($proses,0,',','.');
        }
        else {
        	return $proses;
        }
	}

	//example : bulan_indo('01')
    function bulan_indo($month){
    	$bulan = array(
                '01' => 'Januari',
                '02' => 'Februari',
                '03' => 'Maret',
                '04' => 'April',
                '05' => 'Mei',
                '06' => 'Juni',
                '07' => 'Juli',
                '08' => 'Agustus',
                '09' => 'September',
                '10' => 'Oktober',
                '11' => 'November',
                '12' => 'Desember',
        );
        return $bulan[$month];
    }       



	//example : redirect_alert('user','kata-kata')
	function redirect_alert($url,$alert){
	   global $basepath;
	   if($url=="window_back"){  //history back
	      echo '<script>history.back(alert("'.$alert.'"))</script>';
	   }
	   else {
	    echo '<script>alert("'.$alert.'");</script>
	         <script>document.location="'.$basepath.$url.'"</script>';
	   }
	}

	function getTimeToMicroseconds() {
	    $t = microtime(true);
	    $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
	    $d = new DateTime(date('Y-m-d H:i:s.' . $micro, $t));

	    return $d->format("u"); 
	}
	
	function redirect($url){
	   global $basepath;
	   if($url=="window_back"){  //history back
	      	echo '<script>history.back</script>';
	   }
	   else {
	    	echo '<script>document.location="'.$basepath.$url.'"</script>';
	   }
	}

	

	//example : get_client_ip()
	function get_client_ip() {
	    $ipaddress = '';
	    if (getenv('HTTP_CLIENT_IP'))
	        $ipaddress = getenv('HTTP_CLIENT_IP');
	    else if(getenv('HTTP_X_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	    else if(getenv('HTTP_X_FORWARDED'))
	        $ipaddress = getenv('HTTP_X_FORWARDED');
	    else if(getenv('HTTP_FORWARDED_FOR'))
	        $ipaddress = getenv('HTTP_FORWARDED_FOR');
	    else if(getenv('HTTP_FORWARDED'))
	       $ipaddress = getenv('HTTP_FORWARDED');
	    else if(getenv('REMOTE_ADDR'))
	        $ipaddress = getenv('REMOTE_ADDR');
	    else
	        $ipaddress = 'UNKNOWN';
	    return $ipaddress;
	}

	//example : date_indo_default('30/03/2018')
	function date_indo_default($date){  
		if($date==null or $date==""){
			return "null";
		}
		else {
			$tgl = explode("/",$date);
			return $tgl[2]."-".$tgl[1]."-".$tgl[0];  // yyyy-mm-dd
		}
	}

	//example : get_hari('2018-03-30')
	function get_hari($tanggal){  //Only format yyyy-mm-dd
		$day = date('D', strtotime($tanggal));
		$dayList = array(
			'Sun' => 'Minggu',
			'Mon' => 'Senin',
			'Tue' => 'Selasa',
			'Wed' => 'Rabu',
			'Thu' => 'Kamis',
			'Fri' => 'Jumat',
			'Sat' => 'Sabtu'
		);
		return $dayList[$day]; //output data => Day Name
	}

	//example : get_name_day('1')
	function get_name_day($day){ 
		$dayList = array(
			'1' => 'Senin',
			'2' => 'Selasa',
			'3' => 'Rabu',
			'4' => 'Kamis',
			'5' => 'Jumat',
			'6' => 'Sabtu',
			'7' => 'Minggu'
		);
		return $dayList[$day]; //output data => Day Name
	}

	//example : get_date_indonesia('2018-03-30','all')
	function get_date_indonesia($date,$jns=''){   //Only format yyyy-mm-dd  
	    $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	 
	    $tahun = substr($date, 0, 4);
	    $bulan = substr($date, 5, 2);
	    $tgl   = substr($date, 8, 2);
		
		if($jns=="all"){
			$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;  
			// output data =>  dd, month name yyyy
		}
		else if($jns=="month"){
			$result = $BulanIndo[(int)$bulan-1];   // output data => month name
		}
		else if($jns=="month_year"){
			$result = $BulanIndo[(int)$bulan-1] . " ". $tahun; // output data=> month name  yyyy
		}
		else {
			$newDate = date("d/m/Y", strtotime($date));
			if($date==null or $date==""){
				return "";
			}
			else {
				return $newDate;  // output data =>  dd/mm/yyyy
			}
		}
	    return($result);
	}

	//example : upload_file('temporary_file','path_file','file_name')
	function upload_file($tmp,$path,$file_name) {
	   copy($tmp,$path.$file_name);
	}

	//example : delete_file('path_file','file_name')
	function delete_file($path,$file_name) {
	   	$check = file_exists($path.$file_name);
	   	if($check=="1"){
	   		unlink($path.$file_name);   
	   	}
	}

	//example : array_to_string('$data',',')
	function array_to_string($data,$simbol) {
	       $all_data="";
		    foreach ($data as $dt){ 
		        $all_data .= $dt.$simbol; 
		    }
		   $all_data = rtrim($all_data,$simbol);
		   return $all_data;
	}

	//example : array_to_string('3,2,4','3',',')
	function array_to_string_checked($from,$to,$simbol) {
		$array_of_data = explode($simbol, $from); 
		if(in_array($to,$array_of_data)) return "checked";
	}

	//example : if_select('3','3')
	function if_select($from,$to,$output) {
		if($from==$to) {
			return $output;
		}
		else {
			return "";
		}
	}

	//example : encrypt('hello_world')
	function encrypt($string) {
	  global $secret_key;
	  $result = '';
	  for($i=0; $i<strlen($string); $i++) {
	    $char = substr($string, $i, 1);
	    $keychar = substr($secret_key, ($i % strlen($secret_key))-1, 1);
	    $char = chr(ord($char)+ord($keychar));
	    $result.=$char;
	  }
	  return base64_encode($result);
	 // return $string;
	}

	//example : decrypt('enkripsi_data')
	function decrypt($string) {
	  global $secret_key;	
	  $result = '';
	  $string = base64_decode($string);
	  for($i=0; $i<strlen($string); $i++) {
	    $char = substr($string, $i, 1);
	    $keychar = substr($secret_key, ($i % strlen($secret_key))-1, 1);
	    $char = chr(ord($char)-ord($keychar));
	    $result.=$char;
	  }
	  return $result;
	  // return $string;
	}

	function EXPORT_DATABASE($host,$user,$pass,$name,$tables=false,$backup_name=false){ 
		set_time_limit(999999999); 
		$mysqli = new mysqli($host,$user,$pass,$name); $mysqli->select_db($name); 
		$mysqli->query("SET NAMES 'utf8'");
		$queryTables = $mysqli->query('SHOW TABLES'); 
		while($row = $queryTables->fetch_row()) { 
			$target_tables[] = $row[0]; }	if($tables !== false) { $target_tables = array_intersect( $target_tables, $tables); 
		} 
		$content = "SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";\r\nSET time_zone = \"+00:00\";\r\n\r\n\r\n/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;\r\n/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;\r\n/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;\r\n/*!40101 SET NAMES utf8 */;\r\n--\r\n-- Database: `".$name."`\r\n--\r\n\r\n\r\n";
		foreach($target_tables as $table){
			if (empty($table)){ continue; } 
			$result	= $mysqli->query('SELECT * FROM `'.$table.'`');  	
			$fields_amount=$result->field_count;  
			$rows_num=$mysqli->affected_rows; 	
			$res = $mysqli->query('SHOW CREATE TABLE '.$table);	
			$TableMLine=$res->fetch_row(); 
			$content .= "\n\n".$TableMLine[1].";\n\n";   
			$TableMLine[1]=str_ireplace('CREATE TABLE `','CREATE TABLE IF NOT EXISTS `',$TableMLine[1]);
			for ($i = 0, $st_counter = 0; $i < $fields_amount;   $i++, $st_counter=0) {
				while($row = $result->fetch_row())	{ //when started (and every after 100 command cycle):
					if ($st_counter%100 == 0 || $st_counter == 0 )	{
							$content .= "\nINSERT INTO ".$table." VALUES";
						}
						$content .= "\n(";    
						for($j=0; $j<$fields_amount; $j++){ 
							$row[$j] = str_replace("\n","\\n", addslashes($row[$j]) ); if (isset($row[$j])) {
							$content .= '"'.$row[$j].'"' ;
							}  else{
								$content .= '""';
							}	   
							if ($j<($fields_amount-1)){
								$content.= ',';
							}   
						}        
						$content .=")";
					//every after 100 command cycle [or at last line] ....p.s. but should be inserted 1 cycle eariler
					if ((($st_counter+1)%100==0 && $st_counter!=0) || $st_counter+1==$rows_num){
						$content .= ";";
					} 
					else { 
						$content .= ",";
						}	$st_counter=$st_counter+1;
				}
			} 
			$content .="\n\n\n";
		}
		$content .= "\r\n\r\n/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;\r\n/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;\r\n/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;";
		$backup_name = $backup_name ? $backup_name : $name.'_('.date('H-i-s').'_'.date('d-m-Y').').sql';
		ob_get_clean(); header('Content-Type: application/octet-stream');  header("Content-Transfer-Encoding: Binary");  header('Content-Length: '. (function_exists('mb_strlen') ? mb_strlen($content, '8bit'): strlen($content)) );    header("Content-disposition: attachment; filename=\"".$backup_name."\""); 
		echo $content; exit;
	}


		//Start Property DataTables ServerSide
		function Paging( $input ){
			$sLimit = "";
			if ( isset( $input['iDisplayStart'] ) && $input['iDisplayLength'] != '-1' ) {
				$sLimit = " LIMIT ".intval( $input['iDisplayStart'] ).", ".intval( $input['iDisplayLength'] );
			}

			return $sLimit;
		}


		function Ordering( $input, $aColumns ){
				$aOrderingRules = array();
				if ( isset( $input['iSortCol_0'] ) ) {
					$iSortingCols = intval( $input['iSortingCols'] );
					for ( $i=0 ; $i<$iSortingCols ; $i++ ) {
						if ( $input[ 'bSortable_'.intval($input['iSortCol_'.$i]) ] == 'true' ) {
							$aOrderingRules[] =
							$aColumns[ intval( $input['iSortCol_'.$i] ) ]." "
							.($input['sSortDir_'.$i]==='asc' ? 'asc' : 'desc');
						}
					}
				}

				if (!empty($aOrderingRules)) {
					$sOrder = " ORDER BY ".implode(", ", $aOrderingRules);
					} else {
					$sOrder = "";
				}
				return $sOrder;
		}

		function Filtering( $aColumns, $iColumnCount, $input){
				if ( isset($input['sSearch']) && $input['sSearch'] != "" ) {
					$aFilteringRules = array();
					for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
						if ( isset($input['bSearchable_'.$i]) && $input['bSearchable_'.$i] == 'true' ) {
							$aFilteringRules[] = $aColumns[$i]." LIKE '%".addslashes( $input['sSearch'] )."%'";
						}
					}
					if (!empty($aFilteringRules)) {
						$aFilteringRules = array('('.implode(" OR ", $aFilteringRules).')');
					}
				}

				// Individual column filtering
				for ( $i=0 ; $i<$iColumnCount ; $i++ ) {
					if ( isset($input['bSearchable_'.$i]) && $input['bSearchable_'.$i] == 'true' && $input['sSearch_'.$i] != '' ) {
						$aFilteringRules[] = $aColumns[$i]."  LIKE '%".addslashes($input['sSearch_'.$i])."%'";
					}
				}

				if (!empty($aFilteringRules)) {
					$sWhere = " WHERE ".implode(" OR ", $aFilteringRules);
					} else {
					$sWhere = " WHERE 1=1 ";
				}
				return $sWhere;
		}
		//End Property DataTables ServerSide

}
?>